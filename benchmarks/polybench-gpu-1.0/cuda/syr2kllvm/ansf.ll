; ModuleID = '<stdin>'
source_filename = "/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/ansf.cu"
target datalayout = "e-i64:64-v16:16-v32:32-n16:32:64"
target triple = "nvptx64-nvidia-cuda"

%struct.CallSite_t = type { i32, i16, i16 }
%printf_args = type { i32 }
%printf_args.0 = type { i32 }
%printf_args.1 = type { i32, i32, i32, i32 }
%printf_args.2 = type { i8* }
%printf_args.3 = type { i8* }
%printf_args.4 = type { i8* }
%printf_args.5 = type { i8* }
%printf_args.6 = type { i32, i32 }
%printf_args.7 = type { i32, i32, i32, i32, i32 }
%struct.BBlog_t = type { i16, i16, i16, i16, i64, i32, i32, i32 }
%printf_args.8 = type { i32* }
%printf_args.9 = type { i32, i32 }
%printf_args.10 = type { i32, i32 }
%printf_args.11 = type { i32, i32 }
%printf_args.12 = type { i32, i32, i32, i32 }
%printf_args.13 = type { i64 }
%printf_args.14 = type { i64 }
%printf_args.15 = type { i32, i32, i64, i64, i64 }
%printf_args.16 = type { i32, i32, i64, i64, i64 }

@CTALB = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !0
@CTAUB = local_unnamed_addr addrspace(1) externally_initialized global i32 999, align 4, !dbg !33
@CONSTANCE = local_unnamed_addr addrspace(1) externally_initialized global i32 128, align 4, !dbg !35
@VERBOSE = local_unnamed_addr addrspace(1) externally_initialized global i8 0, align 1, !dbg !37
@ccnntt = addrspace(1) externally_initialized global i64 1, align 8, !dbg !40
@bbccnntt = addrspace(1) externally_initialized global i64 1, align 8, !dbg !42
@done1 = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !44
@done2 = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !46
@funcDic = addrspace(1) externally_initialized global [15 x [31 x i8]] zeroinitializer, align 1, !dbg !48
@dicHeight = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !54
@globalCallStack = local_unnamed_addr addrspace(1) externally_initialized global [10 x %struct.CallSite_t*] zeroinitializer, align 8, !dbg !56
@stackHeight = local_unnamed_addr addrspace(1) externally_initialized global i32* null, align 8, !dbg !68
@contextDic = addrspace(1) externally_initialized global [20 x [5 x %struct.CallSite_t]] zeroinitializer, align 4, !dbg !71
@cHeight = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !77
@.str = private unnamed_addr constant [35 x i8] c"height != 1 && \22stack height == 1\22\00", align 1
@.str.1 = private unnamed_addr constant [59 x i8] c"/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/ansf.cu\00", align 1
@__PRETTY_FUNCTION__._Z15updateCallStackiissi = private unnamed_addr constant [50 x i8] c"void updateCallStack(int, int, short, short, int)\00", align 1
@.str.2 = private unnamed_addr constant [20 x i8] c"first ever. tid=%d\0A\00", align 1
@.str.3 = private unnamed_addr constant [30 x i8] c"same caller different callee\0A\00", align 1
@.str.4 = private unnamed_addr constant [14 x i8] c"call squence\0A\00", align 1
@.str.5 = private unnamed_addr constant [50 x i8] c"(0==-1) && \22!! undefined things happeened here\5Cn\22\00", align 1
@.str.6 = private unnamed_addr constant [37 x i8] c" d::: current call stack height: %d\0A\00", align 1
@.str.7 = private unnamed_addr constant [30 x i8] c" %d: call site: %d, (%d, %d)\0A\00", align 1
@.str.8 = private unnamed_addr constant [15 x i8] c"d: caller: %s\0A\00", align 1
@.str.9 = private unnamed_addr constant [15 x i8] c"d: callee: %s\0A\00", align 1
@.str.10 = private unnamed_addr constant [15 x i8] c"d: return: %s\0A\00", align 1
@.str.11 = private unnamed_addr constant [18 x i8] c"d: undefined: %s\0A\00", align 1
@.str.12 = private unnamed_addr constant [11 x i8] c"id<cHeight\00", align 1
@__PRETTY_FUNCTION__._Z8cxtprinti = private unnamed_addr constant [19 x i8] c"void cxtprint(int)\00", align 1
@.str.13 = private unnamed_addr constant [41 x i8] c"d::: requested context id: %d out of %d\0A\00", align 1
@.str.14 = private unnamed_addr constant [47 x i8] c"d::::::: current context [%d][%d]: %d, %d, %d\0A\00", align 1
@.str.15 = private unnamed_addr constant [5 x i8] c"i<15\00", align 1
@__PRETTY_FUNCTION__._Z6cxtcpyP10CallSite_tS0_i = private unnamed_addr constant [45 x i8] c"void cxtcpy(CallSite_t *, CallSite_t *, int)\00", align 1
@.str.16 = private unnamed_addr constant [36 x i8] c"bbccnntt < 24*64*1024*1024/24 - 128\00", align 1
@__PRETTY_FUNCTION__.passBasicBlock = private unnamed_addr constant [43 x i8] c"void passBasicBlock(void *, int, int, int)\00", align 1
@buffer_oN_DeViCe = external addrspace(1) global [402653184 x i32], align 4
@.str.17 = private unnamed_addr constant [34 x i8] c"ccnntt < 24*64*1024*1024/24 - 128\00", align 1
@__PRETTY_FUNCTION__._Z10storeLinesPvssss = private unnamed_addr constant [52 x i8] c"void storeLines(void *, short, short, short, short)\00", align 1
@.str.18 = private unnamed_addr constant [22 x i8] c"d: buffer handle: %p\0A\00", align 1
@.str.19 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.20 = private unnamed_addr constant [24 x i8] c"d: load by CTA (%d,%d)\0A\00", align 1
@.str.21 = private unnamed_addr constant [25 x i8] c"d: store by CTA (%d,%d)\0A\00", align 1
@.str.22 = private unnamed_addr constant [23 x i8] c"d: !!! undefined !!! \0A\00", align 1
@.str.23 = private unnamed_addr constant [47 x i8] c"d: source line: %d\09 column: %d by CTA (%d,%d)\0A\00", align 1
@.str.24 = private unnamed_addr constant [49 x i8] c"d: Kernel Returns: collected [ %llu ] BB logs. \0A\00", align 1
@.str.25 = private unnamed_addr constant [57 x i8] c"size of function dic: %d %d %lu -> %lu , roundec to %lu\0A\00", align 1
@.str.26 = private unnamed_addr constant [56 x i8] c"size of context dic: %d %d %lu -> %lu , rounded to %lu\0A\00", align 1

; Function Attrs: nounwind
define void @_Z8mystrcpyPcS_(i8* nocapture %dst, i8* nocapture readonly %src) local_unnamed_addr #0 !dbg !984 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %dst, i64 0, metadata !988, metadata !991), !dbg !992
  tail call void @llvm.dbg.value(metadata i8* %src, i64 0, metadata !989, metadata !991), !dbg !993
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !990, metadata !991), !dbg !994
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !990, metadata !991), !dbg !994
  %0 = load i8, i8* %src, align 1, !dbg !995, !tbaa !997
  %cmp16 = icmp eq i8 %0, 0, !dbg !1000
  br i1 %cmp16, label %while.end, label %while.body.preheader, !dbg !1001

while.body.preheader:                             ; preds = %entry
  br label %while.body, !dbg !1003

while.body:                                       ; preds = %while.body.preheader, %while.body
  %arrayidx518 = phi i8* [ %arrayidx5, %while.body ], [ %dst, %while.body.preheader ]
  %1 = phi i8 [ %2, %while.body ], [ %0, %while.body.preheader ]
  %cnt.017 = phi i32 [ %inc, %while.body ], [ 0, %while.body.preheader ]
  store i8 %1, i8* %arrayidx518, align 1, !dbg !1003, !tbaa !997
  %inc = add nuw nsw i32 %cnt.017, 1, !dbg !1005
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !990, metadata !991), !dbg !994
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !990, metadata !991), !dbg !994
  %idxprom = sext i32 %inc to i64, !dbg !995
  %arrayidx = getelementptr inbounds i8, i8* %src, i64 %idxprom, !dbg !995
  %2 = load i8, i8* %arrayidx, align 1, !dbg !995, !tbaa !997
  %cmp = icmp ne i8 %2, 0, !dbg !1000
  %cmp1 = icmp slt i32 %inc, 30, !dbg !1006
  %3 = and i1 %cmp1, %cmp, !dbg !1008
  %arrayidx5 = getelementptr inbounds i8, i8* %dst, i64 %idxprom
  br i1 %3, label %while.body, label %while.end.loopexit, !dbg !1001, !llvm.loop !1009

while.end.loopexit:                               ; preds = %while.body
  br label %while.end, !dbg !1012

while.end:                                        ; preds = %while.end.loopexit, %entry
  %arrayidx5.lcssa = phi i8* [ %dst, %entry ], [ %arrayidx5, %while.end.loopexit ]
  store i8 0, i8* %arrayidx5.lcssa, align 1, !dbg !1012, !tbaa !997
  ret void, !dbg !1013
}

; Function Attrs: nounwind readonly
define zeroext i1 @_Z8mystrcmpPcS_(i8* nocapture readonly %dst, i8* nocapture readonly %src) local_unnamed_addr #1 !dbg !1014 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %dst, i64 0, metadata !1018, metadata !991), !dbg !1021
  tail call void @llvm.dbg.value(metadata i8* %src, i64 0, metadata !1019, metadata !991), !dbg !1022
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1020, metadata !991), !dbg !1023
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1020, metadata !991), !dbg !1023
  br label %while.body, !dbg !1024

while.cond:                                       ; preds = %if.end
  %inc = add nuw nsw i32 %cnt.023, 1, !dbg !1026
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1020, metadata !991), !dbg !1023
  %idxprom.1 = sext i32 %inc to i64, !dbg !1028
  %arrayidx.1 = getelementptr inbounds i8, i8* %dst, i64 %idxprom.1, !dbg !1028
  %0 = load i8, i8* %arrayidx.1, align 1, !dbg !1028, !tbaa !997
  %1 = getelementptr inbounds i8, i8* %src, i64 %idxprom.1, !dbg !1030
  %2 = load i8, i8* %1, align 1, !dbg !1030, !tbaa !997
  %3 = or i8 %2, %0, !dbg !1032
  %4 = icmp eq i8 %3, 0, !dbg !1032
  br i1 %4, label %cleanup, label %if.end.1, !dbg !1032

while.body:                                       ; preds = %while.cond.2, %entry
  %cnt.023 = phi i32 [ 0, %entry ], [ %inc.2, %while.cond.2 ]
  %idxprom = sext i32 %cnt.023 to i64, !dbg !1028
  %arrayidx = getelementptr inbounds i8, i8* %dst, i64 %idxprom, !dbg !1028
  %5 = load i8, i8* %arrayidx, align 1, !dbg !1028, !tbaa !997
  %6 = getelementptr inbounds i8, i8* %src, i64 %idxprom, !dbg !1030
  %7 = load i8, i8* %6, align 1, !dbg !1030, !tbaa !997
  %8 = or i8 %7, %5, !dbg !1032
  %9 = icmp eq i8 %8, 0, !dbg !1032
  br i1 %9, label %cleanup, label %if.end, !dbg !1032

if.end:                                           ; preds = %while.body
  %cmp12 = icmp eq i8 %5, %7, !dbg !1033
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1020, metadata !991), !dbg !1023
  br i1 %cmp12, label %while.cond, label %cleanup, !dbg !1035

cleanup:                                          ; preds = %while.cond.2, %if.end.2, %while.cond.1, %if.end.1, %while.cond, %if.end, %while.body
  %retval.0 = phi i1 [ true, %while.body ], [ false, %if.end ], [ true, %while.cond ], [ false, %if.end.1 ], [ true, %while.cond.1 ], [ false, %if.end.2 ], [ true, %while.cond.2 ]
  ret i1 %retval.0, !dbg !1036

if.end.1:                                         ; preds = %while.cond
  %cmp12.1 = icmp eq i8 %0, %2, !dbg !1033
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1020, metadata !991), !dbg !1023
  br i1 %cmp12.1, label %while.cond.1, label %cleanup, !dbg !1035

while.cond.1:                                     ; preds = %if.end.1
  %inc.1 = add nsw i32 %cnt.023, 2, !dbg !1026
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1020, metadata !991), !dbg !1023
  %idxprom.2 = sext i32 %inc.1 to i64, !dbg !1028
  %arrayidx.2 = getelementptr inbounds i8, i8* %dst, i64 %idxprom.2, !dbg !1028
  %10 = load i8, i8* %arrayidx.2, align 1, !dbg !1028, !tbaa !997
  %11 = getelementptr inbounds i8, i8* %src, i64 %idxprom.2, !dbg !1030
  %12 = load i8, i8* %11, align 1, !dbg !1030, !tbaa !997
  %13 = or i8 %12, %10, !dbg !1032
  %14 = icmp eq i8 %13, 0, !dbg !1032
  br i1 %14, label %cleanup, label %if.end.2, !dbg !1032

if.end.2:                                         ; preds = %while.cond.1
  %cmp12.2 = icmp eq i8 %10, %12, !dbg !1033
  %inc.2 = add nsw i32 %cnt.023, 3, !dbg !1026
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1020, metadata !991), !dbg !1023
  br i1 %cmp12.2, label %while.cond.2, label %cleanup, !dbg !1035

while.cond.2:                                     ; preds = %if.end.2
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1020, metadata !991), !dbg !1023
  %cmp.2 = icmp slt i32 %inc.2, 30, !dbg !1037
  br i1 %cmp.2, label %while.body, label %cleanup, !dbg !1024, !llvm.loop !1038
}

; Function Attrs: nounwind
define i32 @_Z9getFuncIDPc(i8* nocapture readonly %func) local_unnamed_addr #0 !dbg !1041 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %func, i64 0, metadata !1045, metadata !991), !dbg !1051
  %0 = load i32, i32* addrspacecast (i32 addrspace(1)* @dicHeight to i32*), align 4, !dbg !1052, !tbaa !1054
  %cmp = icmp eq i32 %0, 0, !dbg !1056
  br i1 %cmp, label %if.then, label %for.cond.preheader, !dbg !1057

for.cond.preheader:                               ; preds = %entry
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1046, metadata !991), !dbg !1058
  %cmp152 = icmp sgt i32 %0, 0, !dbg !1059
  br i1 %cmp152, label %for.body.preheader, label %for.end, !dbg !1061

for.body.preheader:                               ; preds = %for.cond.preheader
  br label %for.body, !dbg !1063

if.then:                                          ; preds = %entry
  tail call void @llvm.dbg.value(metadata i8* getelementptr ([15 x [31 x i8]], [15 x [31 x i8]]* addrspacecast ([15 x [31 x i8]] addrspace(1)* @funcDic to [15 x [31 x i8]]*), i64 0, i64 0, i64 0), i64 0, metadata !988, metadata !991), !dbg !1064
  tail call void @llvm.dbg.value(metadata i8* %func, i64 0, metadata !989, metadata !991), !dbg !1067
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !990, metadata !991), !dbg !1068
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !990, metadata !991), !dbg !1068
  %1 = load i8, i8* %func, align 1, !dbg !1069, !tbaa !997
  %cmp16.i = icmp eq i8 %1, 0, !dbg !1070
  br i1 %cmp16.i, label %return.sink.split, label %while.body.i.preheader, !dbg !1071

while.body.i.preheader:                           ; preds = %if.then
  br label %while.body.i, !dbg !1072

while.body.i:                                     ; preds = %while.body.i.preheader, %while.body.i
  %arrayidx518.i = phi i8* [ %arrayidx5.i, %while.body.i ], [ getelementptr ([15 x [31 x i8]], [15 x [31 x i8]]* addrspacecast ([15 x [31 x i8]] addrspace(1)* @funcDic to [15 x [31 x i8]]*), i64 0, i64 0, i64 0), %while.body.i.preheader ]
  %2 = phi i8 [ %3, %while.body.i ], [ %1, %while.body.i.preheader ]
  %cnt.017.i = phi i32 [ %inc.i, %while.body.i ], [ 0, %while.body.i.preheader ]
  store i8 %2, i8* %arrayidx518.i, align 1, !dbg !1072, !tbaa !997
  %inc.i = add nuw nsw i32 %cnt.017.i, 1, !dbg !1073
  tail call void @llvm.dbg.value(metadata i32 %inc.i, i64 0, metadata !990, metadata !991), !dbg !1068
  tail call void @llvm.dbg.value(metadata i32 %inc.i, i64 0, metadata !990, metadata !991), !dbg !1068
  %idxprom.i = sext i32 %inc.i to i64, !dbg !1069
  %arrayidx.i = getelementptr inbounds i8, i8* %func, i64 %idxprom.i, !dbg !1069
  %3 = load i8, i8* %arrayidx.i, align 1, !dbg !1069, !tbaa !997
  %cmp.i = icmp ne i8 %3, 0, !dbg !1070
  %cmp1.i = icmp slt i32 %inc.i, 30, !dbg !1074
  %4 = and i1 %cmp1.i, %cmp.i, !dbg !1075
  %arrayidx5.i50 = getelementptr [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 0, i64 %idxprom.i
  %arrayidx5.i = addrspacecast i8 addrspace(1)* %arrayidx5.i50 to i8*
  br i1 %4, label %while.body.i, label %return.sink.split.loopexit, !dbg !1071, !llvm.loop !1009

for.body:                                         ; preds = %for.body.preheader, %for.inc
  %i.053 = phi i32 [ %inc4, %for.inc ], [ 0, %for.body.preheader ]
  %idxprom = sext i32 %i.053 to i64, !dbg !1063
  tail call void @llvm.dbg.value(metadata i8* %func, i64 0, metadata !1019, metadata !991), !dbg !1076
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1020, metadata !991), !dbg !1078
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1020, metadata !991), !dbg !1078
  br label %while.body.i37, !dbg !1079

while.cond.i:                                     ; preds = %if.end.i
  %inc.i38 = or i32 %cnt.023.i, 1, !dbg !1080
  tail call void @llvm.dbg.value(metadata i32 %inc.i38, i64 0, metadata !1020, metadata !991), !dbg !1078
  %idxprom.i35.1 = sext i32 %inc.i38 to i64, !dbg !1081
  %arrayidx.i3649.1 = getelementptr inbounds [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 %idxprom, i64 %idxprom.i35.1, !dbg !1081
  %arrayidx.i36.1 = addrspacecast i8 addrspace(1)* %arrayidx.i3649.1 to i8*, !dbg !1081
  %5 = load i8, i8* %arrayidx.i36.1, align 1, !dbg !1081, !tbaa !997
  %6 = getelementptr inbounds i8, i8* %func, i64 %idxprom.i35.1, !dbg !1082
  %7 = load i8, i8* %6, align 1, !dbg !1082, !tbaa !997
  %8 = or i8 %7, %5, !dbg !1083
  %9 = icmp eq i8 %8, 0, !dbg !1083
  br i1 %9, label %return.loopexit, label %if.end.i.1, !dbg !1083

while.body.i37:                                   ; preds = %while.cond.i.1, %for.body
  %cnt.023.i = phi i32 [ 0, %for.body ], [ %inc.i38.1, %while.cond.i.1 ]
  %idxprom.i35 = sext i32 %cnt.023.i to i64, !dbg !1081
  %arrayidx.i3649 = getelementptr inbounds [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 %idxprom, i64 %idxprom.i35, !dbg !1081
  %arrayidx.i36 = addrspacecast i8 addrspace(1)* %arrayidx.i3649 to i8*, !dbg !1081
  %10 = load i8, i8* %arrayidx.i36, align 1, !dbg !1081, !tbaa !997
  %11 = getelementptr inbounds i8, i8* %func, i64 %idxprom.i35, !dbg !1082
  %12 = load i8, i8* %11, align 1, !dbg !1082, !tbaa !997
  %13 = or i8 %12, %10, !dbg !1083
  %14 = icmp eq i8 %13, 0, !dbg !1083
  br i1 %14, label %return.loopexit, label %if.end.i, !dbg !1083

if.end.i:                                         ; preds = %while.body.i37
  %cmp12.i = icmp eq i8 %10, %12, !dbg !1084
  tail call void @llvm.dbg.value(metadata i32 %inc.i38, i64 0, metadata !1020, metadata !991), !dbg !1078
  br i1 %cmp12.i, label %while.cond.i, label %for.inc, !dbg !1085

for.inc:                                          ; preds = %if.end.i.1, %if.end.i
  %inc4 = add nuw nsw i32 %i.053, 1, !dbg !1086
  tail call void @llvm.dbg.value(metadata i32 %inc4, i64 0, metadata !1046, metadata !991), !dbg !1058
  tail call void @llvm.dbg.value(metadata i32 %inc4, i64 0, metadata !1046, metadata !991), !dbg !1058
  %cmp1 = icmp slt i32 %inc4, %0, !dbg !1059
  br i1 %cmp1, label %for.body, label %for.end.loopexit, !dbg !1061, !llvm.loop !1088

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end, !dbg !1091

for.end:                                          ; preds = %for.end.loopexit, %for.cond.preheader
  %idxprom7 = sext i32 %0 to i64, !dbg !1091
  %arraydecay919 = getelementptr inbounds [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 %idxprom7, i64 0, !dbg !1091
  %arraydecay9 = addrspacecast i8 addrspace(1)* %arraydecay919 to i8*, !dbg !1091
  tail call void @llvm.dbg.value(metadata i8* %arraydecay9, i64 0, metadata !988, metadata !991), !dbg !1092
  tail call void @llvm.dbg.value(metadata i8* %func, i64 0, metadata !989, metadata !991), !dbg !1094
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !990, metadata !991), !dbg !1095
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !990, metadata !991), !dbg !1095
  %15 = load i8, i8* %func, align 1, !dbg !1096, !tbaa !997
  %cmp16.i22 = icmp eq i8 %15, 0, !dbg !1097
  br i1 %cmp16.i22, label %return.sink.split, label %while.body.i31.preheader, !dbg !1098

while.body.i31.preheader:                         ; preds = %for.end
  br label %while.body.i31, !dbg !1099

while.body.i31:                                   ; preds = %while.body.i31.preheader, %while.body.i31
  %arrayidx518.i23 = phi i8* [ %arrayidx5.i30, %while.body.i31 ], [ %arraydecay9, %while.body.i31.preheader ]
  %16 = phi i8 [ %17, %while.body.i31 ], [ %15, %while.body.i31.preheader ]
  %cnt.017.i24 = phi i32 [ %inc.i25, %while.body.i31 ], [ 0, %while.body.i31.preheader ]
  store i8 %16, i8* %arrayidx518.i23, align 1, !dbg !1099, !tbaa !997
  %inc.i25 = add nuw nsw i32 %cnt.017.i24, 1, !dbg !1100
  tail call void @llvm.dbg.value(metadata i32 %inc.i25, i64 0, metadata !990, metadata !991), !dbg !1095
  tail call void @llvm.dbg.value(metadata i32 %inc.i25, i64 0, metadata !990, metadata !991), !dbg !1095
  %idxprom.i26 = sext i32 %inc.i25 to i64, !dbg !1096
  %arrayidx.i27 = getelementptr inbounds i8, i8* %func, i64 %idxprom.i26, !dbg !1096
  %17 = load i8, i8* %arrayidx.i27, align 1, !dbg !1096, !tbaa !997
  %cmp.i28 = icmp ne i8 %17, 0, !dbg !1097
  %cmp1.i29 = icmp slt i32 %inc.i25, 30, !dbg !1101
  %18 = and i1 %cmp1.i29, %cmp.i28, !dbg !1102
  %arrayidx5.i3048 = getelementptr inbounds [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 %idxprom7, i64 %idxprom.i26
  %arrayidx5.i30 = addrspacecast i8 addrspace(1)* %arrayidx5.i3048 to i8*
  br i1 %18, label %while.body.i31, label %return.sink.split.loopexit58, !dbg !1098, !llvm.loop !1009

return.sink.split.loopexit:                       ; preds = %while.body.i
  br label %return.sink.split, !dbg !1103

return.sink.split.loopexit58:                     ; preds = %while.body.i31
  br label %return.sink.split, !dbg !1103

return.sink.split:                                ; preds = %return.sink.split.loopexit58, %return.sink.split.loopexit, %if.then, %for.end
  %arrayidx5.lcssa.i.sink = phi i8* [ %arraydecay9, %for.end ], [ getelementptr ([15 x [31 x i8]], [15 x [31 x i8]]* addrspacecast ([15 x [31 x i8]] addrspace(1)* @funcDic to [15 x [31 x i8]]*), i64 0, i64 0, i64 0), %if.then ], [ %arrayidx5.i, %return.sink.split.loopexit ], [ %arrayidx5.i30, %return.sink.split.loopexit58 ]
  %retval.3.ph = phi i32 [ %0, %for.end ], [ 0, %if.then ], [ 0, %return.sink.split.loopexit ], [ %0, %return.sink.split.loopexit58 ]
  store i8 0, i8* %arrayidx5.lcssa.i.sink, align 1, !dbg !1103, !tbaa !997
  %inc10 = add nsw i32 %0, 1
  store i32 %inc10, i32* addrspacecast (i32 addrspace(1)* @dicHeight to i32*), align 4, !tbaa !1054
  br label %return, !dbg !1104

return.loopexit:                                  ; preds = %while.cond.i.1, %while.cond.i, %while.body.i37
  br label %return, !dbg !1104

return:                                           ; preds = %return.loopexit, %return.sink.split
  %retval.3 = phi i32 [ %retval.3.ph, %return.sink.split ], [ %i.053, %return.loopexit ]
  ret i32 %retval.3, !dbg !1104

if.end.i.1:                                       ; preds = %while.cond.i
  %cmp12.i.1 = icmp eq i8 %5, %7, !dbg !1084
  %inc.i38.1 = add nsw i32 %cnt.023.i, 2, !dbg !1080
  tail call void @llvm.dbg.value(metadata i32 %inc.i38, i64 0, metadata !1020, metadata !991), !dbg !1078
  br i1 %cmp12.i.1, label %while.cond.i.1, label %for.inc, !dbg !1085

while.cond.i.1:                                   ; preds = %if.end.i.1
  tail call void @llvm.dbg.value(metadata i32 %inc.i38, i64 0, metadata !1020, metadata !991), !dbg !1078
  %cmp.i34.1 = icmp slt i32 %inc.i38.1, 30, !dbg !1105
  br i1 %cmp.i34.1, label %while.body.i37, label %return.loopexit, !dbg !1079, !llvm.loop !1038
}

; Function Attrs: convergent nounwind
define void @_Z15updateCallStackiissi(i32 %caller, i32 %callee, i16 signext %sline, i16 signext %scolm, i32 %tid) local_unnamed_addr #2 !dbg !1106 {
entry:
  %tmp = alloca %printf_args, align 8
  tail call void @llvm.dbg.value(metadata i32 %caller, i64 0, metadata !1110, metadata !991), !dbg !1122
  tail call void @llvm.dbg.value(metadata i32 %callee, i64 0, metadata !1111, metadata !991), !dbg !1123
  tail call void @llvm.dbg.value(metadata i16 %sline, i64 0, metadata !1112, metadata !991), !dbg !1124
  tail call void @llvm.dbg.value(metadata i16 %scolm, i64 0, metadata !1113, metadata !991), !dbg !1125
  tail call void @llvm.dbg.value(metadata i32 %tid, i64 0, metadata !1114, metadata !991), !dbg !1126
  %idxprom = sext i32 %tid to i64, !dbg !1127
  %arrayidx177 = getelementptr inbounds [10 x %struct.CallSite_t*], [10 x %struct.CallSite_t*] addrspace(1)* @globalCallStack, i64 0, i64 %idxprom, !dbg !1127
  %arrayidx = addrspacecast %struct.CallSite_t* addrspace(1)* %arrayidx177 to %struct.CallSite_t**, !dbg !1127
  %0 = load %struct.CallSite_t*, %struct.CallSite_t** %arrayidx, align 8, !dbg !1127, !tbaa !1128
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %0, i64 0, metadata !1115, metadata !991), !dbg !1130
  %1 = load i32*, i32** addrspacecast (i32* addrspace(1)* @stackHeight to i32**), align 8, !dbg !1131, !tbaa !1128
  %arrayidx2 = getelementptr inbounds i32, i32* %1, i64 %idxprom, !dbg !1131
  tail call void @llvm.dbg.value(metadata i32* %arrayidx2, i64 0, metadata !1116, metadata !991), !dbg !1132
  %2 = load i32, i32* %arrayidx2, align 4, !dbg !1133, !tbaa !1054
  switch i32 %2, label %if.end [
    i32 1, label %cond.false
    i32 0, label %if.then
  ], !dbg !1133

cond.false:                                       ; preds = %entry
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str, i64 0, i64 0), i32 121, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @__PRETTY_FUNCTION__._Z15updateCallStackiissi, i64 0, i64 0)) #9, !dbg !1134
  unreachable

if.then:                                          ; preds = %entry
  %3 = getelementptr inbounds %printf_args, %printf_args* %tmp, i64 0, i32 0, !dbg !1136
  store i32 %tid, i32* %3, align 8, !dbg !1136
  %4 = bitcast %printf_args* %tmp to i8*, !dbg !1136
  %5 = call i32 @vprintf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i64 0, i64 0), i8* nonnull %4) #10, !dbg !1136
  %id = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 0, !dbg !1139
  store i32 %caller, i32* %id, align 4, !dbg !1140, !tbaa !1141
  %sline6 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 1, !dbg !1144
  store i16 %sline, i16* %sline6, align 4, !dbg !1145, !tbaa !1146
  %scolm8 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 2, !dbg !1147
  store i16 %scolm, i16* %scolm8, align 2, !dbg !1148, !tbaa !1149
  %id10 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 1, i32 0, !dbg !1150
  store i32 %callee, i32* %id10, align 4, !dbg !1151, !tbaa !1141
  %sline12 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 1, i32 1, !dbg !1152
  store i16 -1, i16* %sline12, align 4, !dbg !1153, !tbaa !1146
  %scolm14 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 1, i32 2, !dbg !1154
  store i16 -1, i16* %scolm14, align 2, !dbg !1155, !tbaa !1149
  store i32 2, i32* %arrayidx2, align 4, !dbg !1156, !tbaa !1054
  br label %cleanup100, !dbg !1157

if.end:                                           ; preds = %entry
  %sub = add nsw i32 %2, -2, !dbg !1158
  %idxprom16 = sext i32 %sub to i64, !dbg !1159
  %id18 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom16, i32 0, !dbg !1160
  %6 = load i32, i32* %id18, align 4, !dbg !1160, !tbaa !1141
  tail call void @llvm.dbg.value(metadata i32 %6, i64 0, metadata !1118, metadata !991), !dbg !1161
  %sub20 = add nsw i32 %2, -1, !dbg !1162
  %idxprom21 = sext i32 %sub20 to i64, !dbg !1163
  %id23 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom21, i32 0, !dbg !1164
  %7 = load i32, i32* %id23, align 4, !dbg !1164, !tbaa !1141
  tail call void @llvm.dbg.value(metadata i32 %7, i64 0, metadata !1119, metadata !991), !dbg !1165
  %cmp24 = icmp eq i32 %6, %caller, !dbg !1166
  %cmp25 = icmp eq i32 %7, %callee, !dbg !1168
  %or.cond = and i1 %cmp24, %cmp25, !dbg !1170
  br i1 %or.cond, label %if.then26, label %if.else, !dbg !1170

if.then26:                                        ; preds = %if.end
  %sline30 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom16, i32 1, !dbg !1171
  store i16 %sline, i16* %sline30, align 4, !dbg !1173, !tbaa !1146
  %scolm34 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom16, i32 2, !dbg !1174
  store i16 %scolm, i16* %scolm34, align 2, !dbg !1175, !tbaa !1149
  br label %cleanup100, !dbg !1176

if.else:                                          ; preds = %if.end
  %cmp24.not = xor i1 %cmp24, true, !dbg !1177
  %or.cond178 = or i1 %cmp25, %cmp24.not, !dbg !1177
  br i1 %or.cond178, label %if.else51, label %if.then38, !dbg !1177

if.then38:                                        ; preds = %if.else
  %8 = tail call i32 @vprintf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.3, i64 0, i64 0), i8* null) #10, !dbg !1179
  %9 = load i32, i32* %arrayidx2, align 4, !dbg !1181, !tbaa !1054
  %sub39 = add nsw i32 %9, -1, !dbg !1182
  %idxprom40 = sext i32 %sub39 to i64, !dbg !1183
  %id42 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom40, i32 0, !dbg !1184
  store i32 %callee, i32* %id42, align 4, !dbg !1185, !tbaa !1141
  %10 = load i32, i32* %arrayidx2, align 4, !dbg !1186, !tbaa !1054
  %sub43 = add nsw i32 %10, -2, !dbg !1187
  %idxprom44 = sext i32 %sub43 to i64, !dbg !1188
  %sline46 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom44, i32 1, !dbg !1189
  store i16 %sline, i16* %sline46, align 4, !dbg !1190, !tbaa !1146
  %scolm50 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom44, i32 2, !dbg !1191
  store i16 %scolm, i16* %scolm50, align 2, !dbg !1192, !tbaa !1149
  br label %cleanup100, !dbg !1193

if.else51:                                        ; preds = %if.else
  %cmp52 = icmp eq i32 %7, %caller, !dbg !1194
  br i1 %cmp52, label %if.then53, label %for.cond.preheader, !dbg !1196

for.cond.preheader:                               ; preds = %if.else51
  br label %for.cond

if.then53:                                        ; preds = %if.else51
  %11 = tail call i32 @vprintf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.4, i64 0, i64 0), i8* null) #10, !dbg !1197
  %12 = load i32, i32* %arrayidx2, align 4, !dbg !1199, !tbaa !1054
  %sub54 = add nsw i32 %12, -1, !dbg !1200
  %idxprom55 = sext i32 %sub54 to i64, !dbg !1201
  %sline57 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom55, i32 1, !dbg !1202
  store i16 %sline, i16* %sline57, align 4, !dbg !1203, !tbaa !1146
  %scolm61 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom55, i32 2, !dbg !1204
  store i16 %scolm, i16* %scolm61, align 2, !dbg !1205, !tbaa !1149
  %idxprom62 = sext i32 %12 to i64, !dbg !1206
  %id64 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom62, i32 0, !dbg !1207
  store i32 %callee, i32* %id64, align 4, !dbg !1208, !tbaa !1141
  %13 = load i32, i32* %arrayidx2, align 4, !dbg !1209, !tbaa !1054
  %idxprom65 = sext i32 %13 to i64, !dbg !1210
  %sline67 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom65, i32 1, !dbg !1211
  store i16 -1, i16* %sline67, align 4, !dbg !1212, !tbaa !1146
  %scolm70 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom65, i32 2, !dbg !1213
  store i16 -1, i16* %scolm70, align 2, !dbg !1214, !tbaa !1149
  %inc = add nsw i32 %13, 1, !dbg !1215
  store i32 %inc, i32* %arrayidx2, align 4, !dbg !1215, !tbaa !1054
  br label %cleanup100, !dbg !1216

for.cond:                                         ; preds = %for.cond.preheader, %for.body
  %i.0.in = phi i32 [ %i.0, %for.body ], [ %2, %for.cond.preheader ]
  %i.0 = add nsw i32 %i.0.in, -1
  tail call void @llvm.dbg.value(metadata i32 %i.0, i64 0, metadata !1120, metadata !991), !dbg !1217
  %cmp76 = icmp sgt i32 %i.0.in, 0, !dbg !1218
  br i1 %cmp76, label %for.body, label %for.end, !dbg !1221

for.body:                                         ; preds = %for.cond
  %idxprom77 = sext i32 %i.0 to i64, !dbg !1223
  %id79 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom77, i32 0, !dbg !1226
  %14 = load i32, i32* %id79, align 4, !dbg !1226, !tbaa !1141
  %cmp80 = icmp eq i32 %14, %caller, !dbg !1227
  br i1 %cmp80, label %if.then81, label %for.cond, !dbg !1228, !llvm.loop !1229

if.then81:                                        ; preds = %for.body
  store i32 %i.0.in, i32* %arrayidx2, align 4, !dbg !1232, !tbaa !1054
  store i32 %callee, i32* %id79, align 4, !dbg !1234, !tbaa !1141
  %sline87 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom77, i32 1, !dbg !1235
  %scolm90 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom77, i32 2, !dbg !1236
  store i16 %sline, i16* %sline87, align 4, !dbg !1237, !tbaa !1146
  store i16 %scolm, i16* %scolm90, align 2, !dbg !1238, !tbaa !1149
  br label %cleanup100

for.end:                                          ; preds = %for.cond
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.5, i64 0, i64 0), i32 183, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @__PRETTY_FUNCTION__._Z15updateCallStackiissi, i64 0, i64 0)) #9, !dbg !1239
  unreachable

cleanup100:                                       ; preds = %if.then26, %if.then38, %if.then53, %if.then81, %if.then
  ret void, !dbg !1240
}

; Function Attrs: convergent inlinehint noreturn nounwind
define internal fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* %__message, i32 %__line, i8* %__function) unnamed_addr #3 !dbg !1242 {
entry:
  tail call void @__assertfail(i8* %__message, i8* getelementptr inbounds ([59 x i8], [59 x i8]* @.str.1, i64 0, i64 0), i32 %__line, i8* %__function, i64 1) #11, !dbg !1251
  unreachable, !dbg !1251
}

declare i32 @vprintf(i8*, i8*) local_unnamed_addr

; Function Attrs: nounwind
define void @_Z14printCallStacki(i32 %tid) local_unnamed_addr #0 !dbg !1252 {
entry:
  %tmp = alloca %printf_args.0, align 8
  %tmp12 = alloca %printf_args.1, align 8
  tail call void @llvm.dbg.value(metadata i32 %tid, i64 0, metadata !1254, metadata !991), !dbg !1259
  %idxprom = sext i32 %tid to i64, !dbg !1260
  %arrayidx27 = getelementptr inbounds [10 x %struct.CallSite_t*], [10 x %struct.CallSite_t*] addrspace(1)* @globalCallStack, i64 0, i64 %idxprom, !dbg !1260
  %arrayidx = addrspacecast %struct.CallSite_t* addrspace(1)* %arrayidx27 to %struct.CallSite_t**, !dbg !1260
  %0 = load %struct.CallSite_t*, %struct.CallSite_t** %arrayidx, align 8, !dbg !1260, !tbaa !1128
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %0, i64 0, metadata !1255, metadata !991), !dbg !1261
  %1 = load i32*, i32** addrspacecast (i32* addrspace(1)* @stackHeight to i32**), align 8, !dbg !1262, !tbaa !1128
  %arrayidx2 = getelementptr inbounds i32, i32* %1, i64 %idxprom, !dbg !1262
  %2 = load i32, i32* %arrayidx2, align 4, !dbg !1262, !tbaa !1054
  tail call void @llvm.dbg.value(metadata i32 %2, i64 0, metadata !1256, metadata !991), !dbg !1263
  %3 = getelementptr inbounds %printf_args.0, %printf_args.0* %tmp, i64 0, i32 0, !dbg !1264
  store i32 %2, i32* %3, align 8, !dbg !1264
  %4 = bitcast %printf_args.0* %tmp to i8*, !dbg !1264
  %5 = call i32 @vprintf(i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.6, i64 0, i64 0), i8* nonnull %4) #10, !dbg !1264
  %cmp = icmp sgt i32 %2, 0, !dbg !1265
  call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1257, metadata !991), !dbg !1267
  br i1 %cmp, label %for.body.lr.ph, label %cleanup, !dbg !1268

for.body.lr.ph:                                   ; preds = %entry
  %6 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp12, i64 0, i32 0
  %7 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp12, i64 0, i32 1
  %8 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp12, i64 0, i32 2
  %9 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp12, i64 0, i32 3
  %10 = bitcast %printf_args.1* %tmp12 to i8*
  %xtraiter = and i32 %2, 1, !dbg !1269
  %lcmp.mod = icmp eq i32 %xtraiter, 0, !dbg !1269
  br i1 %lcmp.mod, label %for.body.prol.loopexit, label %for.body.prol.preheader, !dbg !1269

for.body.prol.preheader:                          ; preds = %for.body.lr.ph
  br label %for.body.prol, !dbg !1269

for.body.prol:                                    ; preds = %for.body.prol.preheader
  %id.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 0, !dbg !1271
  %11 = load i32, i32* %id.prol, align 4, !dbg !1271, !tbaa !1141
  %sline.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 1, !dbg !1273
  %12 = load i16, i16* %sline.prol, align 4, !dbg !1273, !tbaa !1146
  %conv.prol = sext i16 %12 to i32, !dbg !1274
  %scolm.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 2, !dbg !1275
  %13 = load i16, i16* %scolm.prol, align 2, !dbg !1275, !tbaa !1149
  %conv11.prol = sext i16 %13 to i32, !dbg !1276
  store i32 0, i32* %6, align 8, !dbg !1277
  store i32 %11, i32* %7, align 4, !dbg !1277
  store i32 %conv.prol, i32* %8, align 8, !dbg !1277
  store i32 %conv11.prol, i32* %9, align 4, !dbg !1277
  %14 = call i32 @vprintf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %10) #10, !dbg !1277
  call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !1257, metadata !991), !dbg !1267
  call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !1257, metadata !991), !dbg !1267
  br label %for.body.prol.loopexit, !dbg !1269

for.body.prol.loopexit:                           ; preds = %for.body.lr.ph, %for.body.prol
  %i.029.unr = phi i32 [ 0, %for.body.lr.ph ], [ 1, %for.body.prol ]
  %15 = icmp eq i32 %2, 1, !dbg !1269
  br i1 %15, label %cleanup.loopexit, label %for.body.lr.ph.new, !dbg !1269

for.body.lr.ph.new:                               ; preds = %for.body.prol.loopexit
  br label %for.body, !dbg !1269

for.body:                                         ; preds = %for.body, %for.body.lr.ph.new
  %i.029 = phi i32 [ %i.029.unr, %for.body.lr.ph.new ], [ %inc.1, %for.body ]
  %idxprom5 = sext i32 %i.029 to i64, !dbg !1278
  %id = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5, i32 0, !dbg !1271
  %16 = load i32, i32* %id, align 4, !dbg !1271, !tbaa !1141
  %sline = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5, i32 1, !dbg !1273
  %17 = load i16, i16* %sline, align 4, !dbg !1273, !tbaa !1146
  %conv = sext i16 %17 to i32, !dbg !1274
  %scolm = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5, i32 2, !dbg !1275
  %18 = load i16, i16* %scolm, align 2, !dbg !1275, !tbaa !1149
  %conv11 = sext i16 %18 to i32, !dbg !1276
  store i32 %i.029, i32* %6, align 8, !dbg !1277
  store i32 %16, i32* %7, align 4, !dbg !1277
  store i32 %conv, i32* %8, align 8, !dbg !1277
  store i32 %conv11, i32* %9, align 4, !dbg !1277
  %19 = call i32 @vprintf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %10) #10, !dbg !1277
  %inc = add nuw nsw i32 %i.029, 1, !dbg !1279
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1257, metadata !991), !dbg !1267
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1257, metadata !991), !dbg !1267
  %idxprom5.1 = sext i32 %inc to i64, !dbg !1278
  %id.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5.1, i32 0, !dbg !1271
  %20 = load i32, i32* %id.1, align 4, !dbg !1271, !tbaa !1141
  %sline.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5.1, i32 1, !dbg !1273
  %21 = load i16, i16* %sline.1, align 4, !dbg !1273, !tbaa !1146
  %conv.1 = sext i16 %21 to i32, !dbg !1274
  %scolm.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5.1, i32 2, !dbg !1275
  %22 = load i16, i16* %scolm.1, align 2, !dbg !1275, !tbaa !1149
  %conv11.1 = sext i16 %22 to i32, !dbg !1276
  store i32 %inc, i32* %6, align 8, !dbg !1277
  store i32 %20, i32* %7, align 4, !dbg !1277
  store i32 %conv.1, i32* %8, align 8, !dbg !1277
  store i32 %conv11.1, i32* %9, align 4, !dbg !1277
  %23 = call i32 @vprintf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %10) #10, !dbg !1277
  %inc.1 = add nsw i32 %i.029, 2, !dbg !1279
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1257, metadata !991), !dbg !1267
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1257, metadata !991), !dbg !1267
  %exitcond.1 = icmp eq i32 %inc.1, %2, !dbg !1281
  br i1 %exitcond.1, label %cleanup.loopexit.unr-lcssa, label %for.body, !dbg !1269, !llvm.loop !1283

cleanup.loopexit.unr-lcssa:                       ; preds = %for.body
  br label %cleanup.loopexit, !dbg !1286

cleanup.loopexit:                                 ; preds = %for.body.prol.loopexit, %cleanup.loopexit.unr-lcssa
  br label %cleanup, !dbg !1286

cleanup:                                          ; preds = %cleanup.loopexit, %entry
  ret void, !dbg !1286
}

; Function Attrs: convergent nounwind
define i32 @_Z6holdonl(i64 %c) local_unnamed_addr #2 !dbg !1288 {
entry:
  tail call void @llvm.dbg.value(metadata i64 %c, i64 0, metadata !1295, metadata !991), !dbg !1298
  %0 = tail call i32 asm sideeffect "mov.u32 \09$0, %clock;", "=r"() #9, !dbg !1299, !srcloc !1304
  tail call void @llvm.dbg.value(metadata i32 %0, i64 0, metadata !1302, metadata !991) #10, !dbg !1305
  %conv = sext i32 %0 to i64, !dbg !1306
  tail call void @llvm.dbg.value(metadata i64 %conv, i64 0, metadata !1296, metadata !991), !dbg !1307
  tail call void @llvm.dbg.value(metadata i64 0, i64 0, metadata !1297, metadata !991), !dbg !1308
  tail call void @llvm.dbg.value(metadata i64 0, i64 0, metadata !1297, metadata !991), !dbg !1308
  %cmp7 = icmp sgt i64 %c, 0, !dbg !1309
  br i1 %cmp7, label %while.body.preheader, label %while.end, !dbg !1311

while.body.preheader:                             ; preds = %entry
  br label %while.body, !dbg !1312

while.body:                                       ; preds = %while.body.preheader, %while.body
  %1 = tail call i32 asm sideeffect "mov.u32 \09$0, %clock;", "=r"() #9, !dbg !1312, !srcloc !1304
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !1302, metadata !991) #10, !dbg !1314
  %conv2 = sext i32 %1 to i64, !dbg !1315
  %sub = sub nsw i64 %conv2, %conv, !dbg !1316
  tail call void @llvm.dbg.value(metadata i64 %sub, i64 0, metadata !1297, metadata !991), !dbg !1308
  tail call void @llvm.dbg.value(metadata i64 %sub, i64 0, metadata !1297, metadata !991), !dbg !1308
  %cmp = icmp slt i64 %sub, %c, !dbg !1309
  br i1 %cmp, label %while.body, label %while.end.loopexit, !dbg !1311, !llvm.loop !1317

while.end.loopexit:                               ; preds = %while.body
  %phitmp = trunc i64 %sub to i32, !dbg !1320
  br label %while.end, !dbg !1320

while.end:                                        ; preds = %while.end.loopexit, %entry
  %clock_offset.0.lcssa = phi i32 [ 0, %entry ], [ %phitmp, %while.end.loopexit ]
  ret i32 %clock_offset.0.lcssa, !dbg !1321
}

; Function Attrs: norecurse nounwind readnone
define void @InitKernel() local_unnamed_addr #4 !dbg !1322 {
entry:
  ret void, !dbg !1323
}

; Function Attrs: nounwind readnone
define void @callFunc(i8* nocapture %er, i8* nocapture %ee, i32 %sline, i32 %scolm) local_unnamed_addr #5 !dbg !1324 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %er, i64 0, metadata !1328, metadata !991), !dbg !1332
  tail call void @llvm.dbg.value(metadata i8* %ee, i64 0, metadata !1329, metadata !991), !dbg !1333
  tail call void @llvm.dbg.value(metadata i32 %sline, i64 0, metadata !1330, metadata !991), !dbg !1334
  tail call void @llvm.dbg.value(metadata i32 %scolm, i64 0, metadata !1331, metadata !991), !dbg !1335
  ret void, !dbg !1336
}

; Function Attrs: nounwind
define void @takeString(i8* %p, i32 %action) local_unnamed_addr #0 !dbg !1337 {
entry:
  %tmp = alloca %printf_args.2, align 8
  %tmp14 = alloca %printf_args.3, align 8
  %tmp18 = alloca %printf_args.4, align 8
  %tmp20 = alloca %printf_args.5, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1341, metadata !991), !dbg !1343
  tail call void @llvm.dbg.value(metadata i32 %action, i64 0, metadata !1342, metadata !991), !dbg !1344
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !1345, !range !1383
  %cmp = icmp eq i32 %0, 0, !dbg !1384
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !1385

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !1386, !range !1415
  %cmp2 = icmp eq i32 %1, 0, !dbg !1416
  br i1 %cmp2, label %lor.lhs.false3, label %return, !dbg !1417

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !1418, !range !1383
  %cmp5 = icmp eq i32 %2, 0, !dbg !1422
  br i1 %cmp5, label %lor.lhs.false6, label %return, !dbg !1423

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !1424, !range !1415
  %cmp8 = icmp eq i32 %3, 0, !dbg !1428
  br i1 %cmp8, label %if.end, label %return, !dbg !1429

if.end:                                           ; preds = %lor.lhs.false6
  %4 = load i8, i8* addrspacecast (i8 addrspace(1)* @VERBOSE to i8*), align 1, !dbg !1431, !tbaa !1433, !range !1435
  %tobool = icmp eq i8 %4, 0, !dbg !1431
  br i1 %tobool, label %return, label %if.then9, !dbg !1436

if.then9:                                         ; preds = %if.end
  switch i32 %action, label %if.else19 [
    i32 1, label %if.then11
    i32 2, label %if.then13
    i32 3, label %if.then17
  ], !dbg !1437

if.then11:                                        ; preds = %if.then9
  %5 = getelementptr inbounds %printf_args.2, %printf_args.2* %tmp, i64 0, i32 0, !dbg !1439
  store i8* %p, i8** %5, align 8, !dbg !1439
  %6 = bitcast %printf_args.2* %tmp to i8*, !dbg !1439
  %7 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.8, i64 0, i64 0), i8* nonnull %6) #10, !dbg !1439
  br label %return, !dbg !1439

if.then13:                                        ; preds = %if.then9
  %8 = getelementptr inbounds %printf_args.3, %printf_args.3* %tmp14, i64 0, i32 0, !dbg !1441
  store i8* %p, i8** %8, align 8, !dbg !1441
  %9 = bitcast %printf_args.3* %tmp14 to i8*, !dbg !1441
  %10 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.9, i64 0, i64 0), i8* nonnull %9) #10, !dbg !1441
  br label %return, !dbg !1441

if.then17:                                        ; preds = %if.then9
  %11 = getelementptr inbounds %printf_args.4, %printf_args.4* %tmp18, i64 0, i32 0, !dbg !1443
  store i8* %p, i8** %11, align 8, !dbg !1443
  %12 = bitcast %printf_args.4* %tmp18 to i8*, !dbg !1443
  %13 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.10, i64 0, i64 0), i8* nonnull %12) #10, !dbg !1443
  br label %return, !dbg !1443

if.else19:                                        ; preds = %if.then9
  %14 = getelementptr inbounds %printf_args.5, %printf_args.5* %tmp20, i64 0, i32 0, !dbg !1445
  store i8* %p, i8** %14, align 8, !dbg !1445
  %15 = bitcast %printf_args.5* %tmp20 to i8*, !dbg !1445
  %16 = call i32 @vprintf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.11, i64 0, i64 0), i8* nonnull %15) #10, !dbg !1445
  br label %return

return:                                           ; preds = %if.end, %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry, %if.then13, %if.else19, %if.then17, %if.then11
  ret void, !dbg !1446
}

; Function Attrs: convergent nounwind
define void @_Z8cxtprinti(i32 %id) local_unnamed_addr #2 !dbg !1447 {
entry:
  %tmp = alloca %printf_args.6, align 8
  %tmp22 = alloca %printf_args.7, align 8
  tail call void @llvm.dbg.value(metadata i32 %id, i64 0, metadata !1449, metadata !991), !dbg !1452
  %0 = load i32, i32* addrspacecast (i32 addrspace(1)* @cHeight to i32*), align 4, !dbg !1453, !tbaa !1054
  %cmp = icmp sgt i32 %0, %id, !dbg !1453
  br i1 %cmp, label %cond.end, label %cond.false, !dbg !1453

cond.false:                                       ; preds = %entry
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.12, i64 0, i64 0), i32 338, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @__PRETTY_FUNCTION__._Z8cxtprinti, i64 0, i64 0)) #9, !dbg !1454
  unreachable

cond.end:                                         ; preds = %entry
  %cmp1 = icmp slt i32 %id, 0, !dbg !1456
  br i1 %cmp1, label %return, label %if.end, !dbg !1458

if.end:                                           ; preds = %cond.end
  %1 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 0, !dbg !1459
  store i32 %id, i32* %1, align 8, !dbg !1459
  %2 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 1, !dbg !1459
  store i32 %0, i32* %2, align 4, !dbg !1459
  %3 = bitcast %printf_args.6* %tmp to i8*, !dbg !1459
  %4 = call i32 @vprintf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.13, i64 0, i64 0), i8* nonnull %3) #10, !dbg !1459
  call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1450, metadata !991), !dbg !1460
  %idxprom = sext i32 %id to i64, !dbg !1461
  call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1450, metadata !991), !dbg !1460
  %5 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 0
  %6 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 1
  %7 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 2
  %8 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 3
  %9 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 4
  %10 = bitcast %printf_args.7* %tmp22 to i8*
  br label %land.rhs, !dbg !1464

land.rhs:                                         ; preds = %if.end, %for.body
  %i.039 = phi i32 [ 0, %if.end ], [ %inc, %for.body ]
  %idxprom4 = sext i32 %i.039 to i64, !dbg !1461
  %arrayidx538 = getelementptr inbounds [20 x [5 x %struct.CallSite_t]], [20 x [5 x %struct.CallSite_t]] addrspace(1)* @contextDic, i64 0, i64 %idxprom, i64 %idxprom4, !dbg !1461
  %arrayidx5 = addrspacecast %struct.CallSite_t addrspace(1)* %arrayidx538 to %struct.CallSite_t*, !dbg !1461
  %id6 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %arrayidx5, i64 0, i32 0, !dbg !1466
  %11 = load i32, i32* %id6, align 4, !dbg !1466, !tbaa !1141
  %cmp7 = icmp eq i32 %11, -1, !dbg !1467
  br i1 %cmp7, label %return.loopexit, label %for.body, !dbg !1468

for.body:                                         ; preds = %land.rhs
  %sline = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %arrayidx5, i64 0, i32 1, !dbg !1470
  %12 = load i16, i16* %sline, align 4, !dbg !1470, !tbaa !1146
  %conv = sext i16 %12 to i32, !dbg !1472
  %scolm = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %arrayidx5, i64 0, i32 2, !dbg !1473
  %13 = load i16, i16* %scolm, align 2, !dbg !1473, !tbaa !1149
  %conv21 = sext i16 %13 to i32, !dbg !1474
  store i32 %id, i32* %5, align 8, !dbg !1475
  store i32 %i.039, i32* %6, align 4, !dbg !1475
  store i32 %11, i32* %7, align 8, !dbg !1475
  store i32 %conv, i32* %8, align 4, !dbg !1475
  store i32 %conv21, i32* %9, align 8, !dbg !1475
  %14 = call i32 @vprintf(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.14, i64 0, i64 0), i8* nonnull %10) #10, !dbg !1475
  %inc = add nuw nsw i32 %i.039, 1, !dbg !1476
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1450, metadata !991), !dbg !1460
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1450, metadata !991), !dbg !1460
  %cmp3 = icmp slt i32 %inc, 15, !dbg !1478
  br i1 %cmp3, label %land.rhs, label %return.loopexit, !dbg !1464, !llvm.loop !1479

return.loopexit:                                  ; preds = %land.rhs, %for.body
  br label %return, !dbg !1482

return:                                           ; preds = %return.loopexit, %cond.end
  ret void, !dbg !1482
}

; Function Attrs: convergent nounwind
define void @_Z6cxtcpyP10CallSite_tS0_i(%struct.CallSite_t* nocapture %dst, %struct.CallSite_t* nocapture readonly %src, i32 %height) local_unnamed_addr #2 !dbg !1483 {
entry:
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %dst, i64 0, metadata !1487, metadata !991), !dbg !1491
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %src, i64 0, metadata !1488, metadata !991), !dbg !1492
  tail call void @llvm.dbg.value(metadata i32 %height, i64 0, metadata !1489, metadata !991), !dbg !1493
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1490, metadata !991), !dbg !1494
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1490, metadata !991), !dbg !1494
  %cmp13 = icmp sgt i32 %height, 0, !dbg !1495
  br i1 %cmp13, label %for.body.preheader, label %cond.end, !dbg !1499

for.body.preheader:                               ; preds = %entry
  %0 = add i32 %height, -1, !dbg !1501
  %xtraiter = and i32 %height, 3, !dbg !1501
  %lcmp.mod = icmp eq i32 %xtraiter, 0, !dbg !1501
  br i1 %lcmp.mod, label %for.body.prol.loopexit, label %for.body.prol.preheader, !dbg !1501

for.body.prol.preheader:                          ; preds = %for.body.preheader
  br label %for.body.prol, !dbg !1501

for.body.prol:                                    ; preds = %for.body.prol, %for.body.prol.preheader
  %i.014.prol = phi i32 [ %inc.prol, %for.body.prol ], [ 0, %for.body.prol.preheader ]
  %prol.iter = phi i32 [ %prol.iter.sub, %for.body.prol ], [ %xtraiter, %for.body.prol.preheader ]
  %idxprom.prol = sext i32 %i.014.prol to i64, !dbg !1501
  %arrayidx.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom.prol, !dbg !1501
  %arrayidx2.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom.prol, !dbg !1502
  %1 = bitcast %struct.CallSite_t* %arrayidx.prol to i64*, !dbg !1503
  %2 = bitcast %struct.CallSite_t* %arrayidx2.prol to i64*, !dbg !1503
  %3 = load i64, i64* %1, align 4, !dbg !1503
  store i64 %3, i64* %2, align 4, !dbg !1503
  %inc.prol = add nuw nsw i32 %i.014.prol, 1, !dbg !1504
  tail call void @llvm.dbg.value(metadata i32 %inc.prol, i64 0, metadata !1490, metadata !991), !dbg !1494
  tail call void @llvm.dbg.value(metadata i32 %inc.prol, i64 0, metadata !1490, metadata !991), !dbg !1494
  %prol.iter.sub = add i32 %prol.iter, -1, !dbg !1499
  %prol.iter.cmp = icmp eq i32 %prol.iter.sub, 0, !dbg !1499
  br i1 %prol.iter.cmp, label %for.body.prol.loopexit.unr-lcssa, label %for.body.prol, !dbg !1499, !llvm.loop !1506

for.body.prol.loopexit.unr-lcssa:                 ; preds = %for.body.prol
  br label %for.body.prol.loopexit, !dbg !1501

for.body.prol.loopexit:                           ; preds = %for.body.preheader, %for.body.prol.loopexit.unr-lcssa
  %i.014.unr = phi i32 [ 0, %for.body.preheader ], [ %inc.prol, %for.body.prol.loopexit.unr-lcssa ]
  %4 = icmp ult i32 %0, 3, !dbg !1501
  br i1 %4, label %for.end, label %for.body.preheader.new, !dbg !1501

for.body.preheader.new:                           ; preds = %for.body.prol.loopexit
  br label %for.body, !dbg !1501

for.body:                                         ; preds = %for.body, %for.body.preheader.new
  %i.014 = phi i32 [ %i.014.unr, %for.body.preheader.new ], [ %inc.3, %for.body ]
  %idxprom = sext i32 %i.014 to i64, !dbg !1501
  %arrayidx = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom, !dbg !1501
  %arrayidx2 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom, !dbg !1502
  %5 = bitcast %struct.CallSite_t* %arrayidx to i64*, !dbg !1503
  %6 = bitcast %struct.CallSite_t* %arrayidx2 to i64*, !dbg !1503
  %7 = load i64, i64* %5, align 4, !dbg !1503
  store i64 %7, i64* %6, align 4, !dbg !1503
  %inc = add nuw nsw i32 %i.014, 1, !dbg !1504
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1490, metadata !991), !dbg !1494
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1490, metadata !991), !dbg !1494
  %idxprom.1 = sext i32 %inc to i64, !dbg !1501
  %arrayidx.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom.1, !dbg !1501
  %arrayidx2.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom.1, !dbg !1502
  %8 = bitcast %struct.CallSite_t* %arrayidx.1 to i64*, !dbg !1503
  %9 = bitcast %struct.CallSite_t* %arrayidx2.1 to i64*, !dbg !1503
  %10 = load i64, i64* %8, align 4, !dbg !1503
  store i64 %10, i64* %9, align 4, !dbg !1503
  %inc.1 = add nsw i32 %i.014, 2, !dbg !1504
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1490, metadata !991), !dbg !1494
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1490, metadata !991), !dbg !1494
  %idxprom.2 = sext i32 %inc.1 to i64, !dbg !1501
  %arrayidx.2 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom.2, !dbg !1501
  %arrayidx2.2 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom.2, !dbg !1502
  %11 = bitcast %struct.CallSite_t* %arrayidx.2 to i64*, !dbg !1503
  %12 = bitcast %struct.CallSite_t* %arrayidx2.2 to i64*, !dbg !1503
  %13 = load i64, i64* %11, align 4, !dbg !1503
  store i64 %13, i64* %12, align 4, !dbg !1503
  %inc.2 = add nsw i32 %i.014, 3, !dbg !1504
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1490, metadata !991), !dbg !1494
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1490, metadata !991), !dbg !1494
  %idxprom.3 = sext i32 %inc.2 to i64, !dbg !1501
  %arrayidx.3 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom.3, !dbg !1501
  %arrayidx2.3 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom.3, !dbg !1502
  %14 = bitcast %struct.CallSite_t* %arrayidx.3 to i64*, !dbg !1503
  %15 = bitcast %struct.CallSite_t* %arrayidx2.3 to i64*, !dbg !1503
  %16 = load i64, i64* %14, align 4, !dbg !1503
  store i64 %16, i64* %15, align 4, !dbg !1503
  %inc.3 = add nsw i32 %i.014, 4, !dbg !1504
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1490, metadata !991), !dbg !1494
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1490, metadata !991), !dbg !1494
  %exitcond.3 = icmp eq i32 %inc.3, %height, !dbg !1495
  br i1 %exitcond.3, label %for.end.unr-lcssa, label %for.body, !dbg !1499, !llvm.loop !1508

for.end.unr-lcssa:                                ; preds = %for.body
  br label %for.end, !dbg !1511

for.end:                                          ; preds = %for.body.prol.loopexit, %for.end.unr-lcssa
  %cmp3 = icmp slt i32 %height, 15, !dbg !1511
  br i1 %cmp3, label %cond.end, label %cond.false, !dbg !1511

cond.false:                                       ; preds = %for.end
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.15, i64 0, i64 0), i32 359, i8* getelementptr inbounds ([45 x i8], [45 x i8]* @__PRETTY_FUNCTION__._Z6cxtcpyP10CallSite_tS0_i, i64 0, i64 0)) #9, !dbg !1512
  unreachable

cond.end:                                         ; preds = %entry, %for.end
  %i.0.lcssa16 = phi i32 [ %height, %for.end ], [ 0, %entry ]
  %idxprom4 = sext i32 %i.0.lcssa16 to i64, !dbg !1514
  %id = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom4, i32 0, !dbg !1515
  store i32 -1, i32* %id, align 4, !dbg !1516, !tbaa !1141
  ret void, !dbg !1517
}

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture writeonly, i8* nocapture readonly, i64, i32, i1) #6

; Function Attrs: nounwind readonly
define zeroext i1 @_Z6cxtcmpP10CallSite_tS0_i(%struct.CallSite_t* nocapture readonly %dst, %struct.CallSite_t* nocapture readonly %src, i32 %height) local_unnamed_addr #1 !dbg !1518 {
entry:
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %dst, i64 0, metadata !1522, metadata !991), !dbg !1527
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %src, i64 0, metadata !1523, metadata !991), !dbg !1528
  tail call void @llvm.dbg.value(metadata i32 %height, i64 0, metadata !1524, metadata !991), !dbg !1529
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1525, metadata !991), !dbg !1530
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1525, metadata !991), !dbg !1530
  %cmp9 = icmp sgt i32 %height, 0, !dbg !1531
  br i1 %cmp9, label %for.body.preheader, label %cleanup, !dbg !1534

for.body.preheader:                               ; preds = %entry
  br label %for.body, !dbg !1536

for.cond:                                         ; preds = %for.body
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1525, metadata !991), !dbg !1530
  %cmp = icmp slt i32 %inc, %height, !dbg !1531
  br i1 %cmp, label %for.body, label %cleanup.loopexit, !dbg !1534, !llvm.loop !1538

for.body:                                         ; preds = %for.body.preheader, %for.cond
  %i.010 = phi i32 [ %inc, %for.cond ], [ 0, %for.body.preheader ]
  %idxprom = sext i32 %i.010 to i64, !dbg !1536
  %id = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom, i32 0, !dbg !1541
  %0 = load i32, i32* %id, align 4, !dbg !1541, !tbaa !1141
  %id3 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom, i32 0, !dbg !1542
  %1 = load i32, i32* %id3, align 4, !dbg !1542, !tbaa !1141
  %cmp4 = icmp eq i32 %0, %1, !dbg !1543
  %inc = add nuw nsw i32 %i.010, 1, !dbg !1544
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1525, metadata !991), !dbg !1530
  br i1 %cmp4, label %for.cond, label %cleanup.loopexit, !dbg !1546

cleanup.loopexit:                                 ; preds = %for.cond, %for.body
  %.ph = phi i1 [ true, %for.cond ], [ false, %for.body ]
  br label %cleanup, !dbg !1547

cleanup:                                          ; preds = %cleanup.loopexit, %entry
  %2 = phi i1 [ true, %entry ], [ %.ph, %cleanup.loopexit ]
  ret i1 %2, !dbg !1547
}

; Function Attrs: norecurse nounwind readnone
define i32 @getContextID() local_unnamed_addr #4 !dbg !1548 {
entry:
  ret i32 -1, !dbg !1549
}

; Function Attrs: convergent nounwind
define void @passBasicBlock(i8* nocapture readonly %p, i32 %action, i32 %sline, i32 %scolm) local_unnamed_addr #2 !dbg !1550 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1554, metadata !991), !dbg !1569
  tail call void @llvm.dbg.value(metadata i32 %action, i64 0, metadata !1555, metadata !991), !dbg !1570
  tail call void @llvm.dbg.value(metadata i32 %sline, i64 0, metadata !1556, metadata !991), !dbg !1571
  tail call void @llvm.dbg.value(metadata i32 %scolm, i64 0, metadata !1557, metadata !991), !dbg !1572
  %0 = load i64, i64* addrspacecast (i64 addrspace(1)* @bbccnntt to i64*), align 8, !dbg !1573, !tbaa !1574
  %cmp = icmp ult i64 %0, 67108736, !dbg !1573
  br i1 %cmp, label %cond.end, label %cond.false, !dbg !1573

cond.false:                                       ; preds = %entry
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.16, i64 0, i64 0), i32 437, i8* getelementptr inbounds ([43 x i8], [43 x i8]* @__PRETTY_FUNCTION__.passBasicBlock, i64 0, i64 0)) #9, !dbg !1576
  unreachable

cond.end:                                         ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !1578, !range !1415
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !1581, !range !1415
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #10, !dbg !1584, !range !1629
  %mul = mul nuw i32 %3, %2, !dbg !1630
  %add = add i32 %mul, %1, !dbg !1631
  %4 = load i32, i32* addrspacecast (i32 addrspace(1)* @CTALB to i32*), align 4, !dbg !1632, !tbaa !1054
  %cmp3 = icmp ult i32 %add, %4, !dbg !1633
  br i1 %cmp3, label %return, label %lor.lhs.false, !dbg !1634

lor.lhs.false:                                    ; preds = %cond.end
  %5 = load i32, i32* addrspacecast (i32 addrspace(1)* @CTAUB to i32*), align 4, !dbg !1635, !tbaa !1054
  %cmp9 = icmp ugt i32 %add, %5, !dbg !1637
  br i1 %cmp9, label %return, label %if.end, !dbg !1638

if.end:                                           ; preds = %lor.lhs.false
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1558, metadata !991), !dbg !1640
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1641, metadata !991), !dbg !1649
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1651, metadata !991), !dbg !1655
  %6 = atomicrmw add i64* addrspacecast (i64 addrspace(1)* @bbccnntt to i64*), i64 1 seq_cst, !dbg !1657
  tail call void @llvm.dbg.value(metadata i64 0, i64 0, metadata !1560, metadata !991), !dbg !1658
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1561, metadata !991), !dbg !1659
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1562, metadata !991), !dbg !1660
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1563, metadata !991), !dbg !1661
  tail call void @llvm.dbg.value(metadata i64 0, i64 0, metadata !1560, metadata !991), !dbg !1658
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1562, metadata !991), !dbg !1660
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1563, metadata !991), !dbg !1661
  %7 = load i8, i8* %p, align 1, !dbg !1662, !tbaa !997
  %cmp1288 = icmp eq i8 %7, 0, !dbg !1664
  br i1 %cmp1288, label %for.cond.cleanup, label %for.body.preheader, !dbg !1665

for.body.preheader:                               ; preds = %if.end
  br label %for.body, !dbg !1667

for.cond.cleanup.loopexit:                        ; preds = %cleanup
  br label %for.cond.cleanup, !dbg !1669

for.cond.cleanup:                                 ; preds = %for.cond.cleanup.loopexit, %if.end
  %key.0.lcssa = phi i64 [ 0, %if.end ], [ %key.1, %for.cond.cleanup.loopexit ]
  tail call void @llvm.dbg.value(metadata %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 0, metadata !1568, metadata !991), !dbg !1669
  %sext = shl i64 %6, 32, !dbg !1670
  %idxprom = ashr exact i64 %sext, 32, !dbg !1670
  %key27 = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 4, !dbg !1671
  store i64 %key.0.lcssa, i64* %key27, align 8, !dbg !1672, !tbaa !1673
  %8 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !1675, !range !1383
  %conv29 = trunc i32 %8 to i16, !dbg !1677
  %tidx = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 2, !dbg !1678
  store i16 %conv29, i16* %tidx, align 4, !dbg !1679, !tbaa !1680
  %9 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !1681, !range !1383
  %conv33 = trunc i32 %9 to i16, !dbg !1683
  %tidy = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 3, !dbg !1684
  store i16 %conv33, i16* %tidy, align 2, !dbg !1685, !tbaa !1686
  %conv37 = trunc i32 %1 to i16, !dbg !1687
  %bidx = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 0, !dbg !1688
  store i16 %conv37, i16* %bidx, align 8, !dbg !1689, !tbaa !1690
  %conv41 = trunc i32 %2 to i16, !dbg !1691
  %bidy = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 1, !dbg !1692
  store i16 %conv41, i16* %bidy, align 2, !dbg !1693, !tbaa !1694
  %sline46 = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 5, !dbg !1695
  store i32 %sline, i32* %sline46, align 8, !dbg !1696, !tbaa !1697
  %scolm49 = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 6, !dbg !1698
  store i32 %scolm, i32* %scolm49, align 4, !dbg !1699, !tbaa !1700
  %cid = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 7, !dbg !1701
  store i32 -1, i32* %cid, align 8, !dbg !1702, !tbaa !1703
  br label %return

for.body:                                         ; preds = %for.body.preheader, %cleanup
  %10 = phi i8 [ %13, %cleanup ], [ %7, %for.body.preheader ]
  %key.091 = phi i64 [ %key.1, %cleanup ], [ 0, %for.body.preheader ]
  %factor.090 = phi i64 [ %factor.1, %cleanup ], [ 1, %for.body.preheader ]
  %i.089 = phi i32 [ %inc, %cleanup ], [ 0, %for.body.preheader ]
  %.off = add i8 %10, -48, !dbg !1667
  %11 = icmp ugt i8 %.off, 75, !dbg !1667
  tail call void @llvm.dbg.value(metadata i64 %add23, i64 0, metadata !1560, metadata !991), !dbg !1658
  tail call void @llvm.dbg.value(metadata i64 %mul25, i64 0, metadata !1562, metadata !991), !dbg !1660
  br i1 %11, label %cleanup, label %if.end20, !dbg !1667

if.end20:                                         ; preds = %for.body
  %conv21 = sext i8 %10 to i64, !dbg !1704
  %mul22 = mul nsw i64 %factor.090, %conv21, !dbg !1705
  %add23 = add i64 %mul22, %key.091, !dbg !1706
  %12 = load i32, i32* addrspacecast (i32 addrspace(1)* @CONSTANCE to i32*), align 4, !dbg !1707, !tbaa !1054
  %conv24 = sext i32 %12 to i64, !dbg !1707
  %mul25 = mul nsw i64 %factor.090, %conv24, !dbg !1708
  br label %cleanup, !dbg !1709

cleanup:                                          ; preds = %for.body, %if.end20
  %factor.1 = phi i64 [ %mul25, %if.end20 ], [ %factor.090, %for.body ]
  %key.1 = phi i64 [ %add23, %if.end20 ], [ %key.091, %for.body ]
  tail call void @llvm.dbg.value(metadata i64 %key.1, i64 0, metadata !1560, metadata !991), !dbg !1658
  tail call void @llvm.dbg.value(metadata i64 %factor.1, i64 0, metadata !1562, metadata !991), !dbg !1660
  %inc = add nuw nsw i32 %i.089, 1, !dbg !1710
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1563, metadata !991), !dbg !1661
  tail call void @llvm.dbg.value(metadata i64 %key.1, i64 0, metadata !1560, metadata !991), !dbg !1658
  tail call void @llvm.dbg.value(metadata i64 %factor.1, i64 0, metadata !1562, metadata !991), !dbg !1660
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1563, metadata !991), !dbg !1661
  %idx.ext = sext i32 %inc to i64, !dbg !1712
  %add.ptr = getelementptr inbounds i8, i8* %p, i64 %idx.ext, !dbg !1712
  %13 = load i8, i8* %add.ptr, align 1, !dbg !1662, !tbaa !997
  %cmp12 = icmp eq i8 %13, 0, !dbg !1664
  br i1 %cmp12, label %for.cond.cleanup.loopexit, label %for.body, !dbg !1665, !llvm.loop !1713

return:                                           ; preds = %cond.end, %lor.lhs.false, %for.cond.cleanup
  ret void, !dbg !1716
}

; Function Attrs: convergent nounwind
define void @_Z10storeLinesPvssss(i8* %p, i16 signext %size, i16 signext %line, i16 signext %colmn, i16 signext %op) local_unnamed_addr #2 !dbg !1717 {
entry:
  %tmp = alloca %printf_args.8, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1721, metadata !991), !dbg !1729
  tail call void @llvm.dbg.value(metadata i16 %size, i64 0, metadata !1722, metadata !991), !dbg !1730
  tail call void @llvm.dbg.value(metadata i16 %line, i64 0, metadata !1723, metadata !991), !dbg !1731
  tail call void @llvm.dbg.value(metadata i16 %colmn, i64 0, metadata !1724, metadata !991), !dbg !1732
  tail call void @llvm.dbg.value(metadata i16 %op, i64 0, metadata !1725, metadata !991), !dbg !1733
  %0 = load i64, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !1734, !tbaa !1574
  %cmp = icmp ult i64 %0, 67108736, !dbg !1734
  br i1 %cmp, label %cond.end, label %cond.false, !dbg !1734

cond.false:                                       ; preds = %entry
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.17, i64 0, i64 0), i32 498, i8* getelementptr inbounds ([52 x i8], [52 x i8]* @__PRETTY_FUNCTION__._Z10storeLinesPvssss, i64 0, i64 0)) #9, !dbg !1735
  unreachable

cond.end:                                         ; preds = %entry
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1641, metadata !991), !dbg !1737
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1651, metadata !991), !dbg !1739
  %1 = atomicrmw add i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), i64 1 seq_cst, !dbg !1741
  %conv = trunc i64 %1 to i32, !dbg !1742
  tail call void @llvm.dbg.value(metadata i32 %conv, i64 0, metadata !1726, metadata !991), !dbg !1743
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !1727, metadata !991), !dbg !1744
  tail call void @llvm.dbg.value(metadata i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 0, metadata !1728, metadata !991), !dbg !1745
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !1746, !range !1415
  %conv2 = trunc i32 %2 to i16, !dbg !1748
  %mul = mul nsw i32 %conv, 12, !dbg !1749
  %idxprom = sext i32 %mul to i64, !dbg !1750
  %arrayidx = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom, !dbg !1750
  store i16 %conv2, i16* %arrayidx, align 2, !dbg !1751, !tbaa !1752
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !1753, !range !1415
  %conv4 = trunc i32 %3 to i16, !dbg !1755
  %add6 = or i32 %mul, 1, !dbg !1756
  %idxprom7 = sext i32 %add6 to i64, !dbg !1757
  %arrayidx8 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom7, !dbg !1757
  store i16 %conv4, i16* %arrayidx8, align 2, !dbg !1758, !tbaa !1752
  %4 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !1759, !range !1383
  %conv10 = trunc i32 %4 to i16, !dbg !1761
  %add12 = or i32 %mul, 2, !dbg !1762
  %idxprom13 = sext i32 %add12 to i64, !dbg !1763
  %arrayidx14 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom13, !dbg !1763
  store i16 %conv10, i16* %arrayidx14, align 2, !dbg !1764, !tbaa !1752
  %5 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !1765, !range !1383
  %conv16 = trunc i32 %5 to i16, !dbg !1767
  %add18 = or i32 %mul, 3, !dbg !1768
  %idxprom19 = sext i32 %add18 to i64, !dbg !1769
  %arrayidx20 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom19, !dbg !1769
  store i16 %conv16, i16* %arrayidx20, align 2, !dbg !1770, !tbaa !1752
  %6 = ptrtoint i8* %p to i64, !dbg !1771
  %add22 = mul i64 %1, 12884901888, !dbg !1772
  %sext = add i64 %add22, 4294967296, !dbg !1772
  %idxprom23 = ashr exact i64 %sext, 32, !dbg !1772
  %arrayidx24 = getelementptr inbounds i64, i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 %idxprom23, !dbg !1772
  store i64 %6, i64* %arrayidx24, align 8, !dbg !1773, !tbaa !1774
  %add26 = add nsw i32 %mul, 8, !dbg !1776
  %idxprom27 = sext i32 %add26 to i64, !dbg !1777
  %arrayidx28 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom27, !dbg !1777
  store i16 %size, i16* %arrayidx28, align 2, !dbg !1778, !tbaa !1752
  %add30 = add nsw i32 %mul, 9, !dbg !1779
  %idxprom31 = sext i32 %add30 to i64, !dbg !1780
  %arrayidx32 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom31, !dbg !1780
  store i16 %line, i16* %arrayidx32, align 2, !dbg !1781, !tbaa !1752
  %add34 = add nsw i32 %mul, 10, !dbg !1782
  %idxprom35 = sext i32 %add34 to i64, !dbg !1783
  %arrayidx36 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom35, !dbg !1783
  store i16 %colmn, i16* %arrayidx36, align 2, !dbg !1784, !tbaa !1752
  %add38 = add nsw i32 %mul, 11, !dbg !1785
  %idxprom39 = sext i32 %add38 to i64, !dbg !1786
  %arrayidx40 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom39, !dbg !1786
  store i16 %op, i16* %arrayidx40, align 2, !dbg !1787, !tbaa !1752
  %cmp42 = icmp slt i32 %conv, 5, !dbg !1788
  br i1 %cmp42, label %if.then, label %if.end, !dbg !1790

if.then:                                          ; preds = %cond.end
  %7 = getelementptr inbounds %printf_args.8, %printf_args.8* %tmp, i64 0, i32 0, !dbg !1791
  store i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 0), i32** %7, align 8, !dbg !1791
  %8 = bitcast %printf_args.8* %tmp to i8*, !dbg !1791
  %9 = call i32 @vprintf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.18, i64 0, i64 0), i8* nonnull %8) #10, !dbg !1791
  br label %if.end, !dbg !1791

if.end:                                           ; preds = %if.then, %cond.end
  ret void, !dbg !1792
}

; Function Attrs: nounwind
define void @_Z9dumpLinesv() local_unnamed_addr #0 !dbg !1793 {
entry:
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !1796, !range !1383
  %cmp = icmp eq i32 %0, 0, !dbg !1799
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !1800

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !1801, !range !1415
  %cmp2 = icmp eq i32 %1, 0, !dbg !1804
  br i1 %cmp2, label %lor.lhs.false3, label %return, !dbg !1805

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !1806, !range !1383
  %cmp5 = icmp eq i32 %2, 0, !dbg !1809
  br i1 %cmp5, label %lor.lhs.false6, label %return, !dbg !1810

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !1811, !range !1415
  %cmp8 = icmp eq i32 %3, 0, !dbg !1814
  br i1 %cmp8, label %for.cond.preheader, label %return, !dbg !1815

for.cond.preheader:                               ; preds = %lor.lhs.false6
  %4 = tail call i32 @vprintf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.19, i64 0, i64 0), i8* null) #10, !dbg !1817
  br label %return, !dbg !1818

return:                                           ; preds = %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry, %for.cond.preheader
  ret void, !dbg !1819
}

; Function Attrs: nounwind
define void @_Z6print1i(i32 %a) local_unnamed_addr #0 !dbg !1821 {
entry:
  %tmp = alloca %printf_args.9, align 8
  %tmp14 = alloca %printf_args.10, align 8
  tail call void @llvm.dbg.value(metadata i32 %a, i64 0, metadata !1823, metadata !991), !dbg !1824
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !1825, !range !1383
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !1828, !range !1383
  %add = add nuw nsw i32 %1, %0, !dbg !1831
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !1832, !range !1415
  %add3 = add nuw nsw i32 %add, %2, !dbg !1835
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !1836, !range !1415
  %add5 = sub nsw i32 0, %3, !dbg !1839
  %cmp = icmp eq i32 %add3, %add5, !dbg !1839
  br i1 %cmp, label %land.lhs.true, label %if.end17, !dbg !1840

land.lhs.true:                                    ; preds = %entry
  %4 = load i8, i8* addrspacecast (i8 addrspace(1)* @VERBOSE to i8*), align 1, !dbg !1841, !tbaa !1433, !range !1435
  %tobool = icmp eq i8 %4, 0, !dbg !1841
  br i1 %tobool, label %if.end17, label %if.then, !dbg !1843

if.then:                                          ; preds = %land.lhs.true
  switch i32 %a, label %if.else15 [
    i32 1, label %if.then7
    i32 2, label %if.then11
  ], !dbg !1845

if.then7:                                         ; preds = %if.then
  %5 = getelementptr inbounds %printf_args.9, %printf_args.9* %tmp, i64 0, i32 0, !dbg !1847
  store i32 %2, i32* %5, align 8, !dbg !1847
  %6 = getelementptr inbounds %printf_args.9, %printf_args.9* %tmp, i64 0, i32 1, !dbg !1847
  store i32 %3, i32* %6, align 4, !dbg !1847
  %7 = bitcast %printf_args.9* %tmp to i8*, !dbg !1847
  %8 = call i32 @vprintf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.20, i64 0, i64 0), i8* nonnull %7) #10, !dbg !1849
  br label %if.end17, !dbg !1847

if.then11:                                        ; preds = %if.then
  %9 = getelementptr inbounds %printf_args.10, %printf_args.10* %tmp14, i64 0, i32 0, !dbg !1851
  store i32 %2, i32* %9, align 8, !dbg !1851
  %10 = getelementptr inbounds %printf_args.10, %printf_args.10* %tmp14, i64 0, i32 1, !dbg !1851
  store i32 %3, i32* %10, align 4, !dbg !1851
  %11 = bitcast %printf_args.10* %tmp14 to i8*, !dbg !1851
  %12 = call i32 @vprintf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.21, i64 0, i64 0), i8* nonnull %11) #10, !dbg !1853
  br label %if.end17, !dbg !1851

if.else15:                                        ; preds = %if.then
  %13 = tail call i32 @vprintf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.22, i64 0, i64 0), i8* null) #10, !dbg !1855
  br label %if.end17

if.end17:                                         ; preds = %land.lhs.true, %if.then7, %if.else15, %if.then11, %entry
  ret void, !dbg !1856
}

; Function Attrs: nounwind
define void @_Z6print2v() local_unnamed_addr #0 !dbg !1857 {
entry:
  %tmp = alloca %printf_args.11, align 8
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !1858, !range !1383
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !1861, !range !1383
  %add = add nuw nsw i32 %1, %0, !dbg !1864
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !1865, !range !1415
  %add3 = add nuw nsw i32 %add, %2, !dbg !1868
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !1869, !range !1415
  %add5 = sub nsw i32 0, %3, !dbg !1872
  %cmp = icmp eq i32 %add3, %add5, !dbg !1872
  br i1 %cmp, label %land.lhs.true, label %if.end, !dbg !1873

land.lhs.true:                                    ; preds = %entry
  %4 = load i8, i8* addrspacecast (i8 addrspace(1)* @VERBOSE to i8*), align 1, !dbg !1874, !tbaa !1433, !range !1435
  %tobool = icmp eq i8 %4, 0, !dbg !1874
  br i1 %tobool, label %if.end, label %if.then, !dbg !1876

if.then:                                          ; preds = %land.lhs.true
  %5 = getelementptr inbounds %printf_args.11, %printf_args.11* %tmp, i64 0, i32 0, !dbg !1878
  store i32 %2, i32* %5, align 8, !dbg !1878
  %6 = getelementptr inbounds %printf_args.11, %printf_args.11* %tmp, i64 0, i32 1, !dbg !1878
  store i32 %3, i32* %6, align 4, !dbg !1878
  %7 = bitcast %printf_args.11* %tmp to i8*, !dbg !1878
  %8 = call i32 @vprintf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.21, i64 0, i64 0), i8* nonnull %7) #10, !dbg !1879
  br label %if.end, !dbg !1878

if.end:                                           ; preds = %land.lhs.true, %if.then, %entry
  ret void, !dbg !1880
}

; Function Attrs: nounwind
define void @_Z6print3ii(i32 %line, i32 %col) local_unnamed_addr #0 !dbg !1881 {
entry:
  %tmp = alloca %printf_args.12, align 8
  tail call void @llvm.dbg.value(metadata i32 %line, i64 0, metadata !1885, metadata !991), !dbg !1887
  tail call void @llvm.dbg.value(metadata i32 %col, i64 0, metadata !1886, metadata !991), !dbg !1888
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !1889, !range !1383
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !1892, !range !1383
  %add = add nuw nsw i32 %1, %0, !dbg !1895
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !1896, !range !1415
  %add3 = add nuw nsw i32 %add, %2, !dbg !1899
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !1900, !range !1415
  %add5 = sub nsw i32 0, %3, !dbg !1903
  %cmp = icmp eq i32 %add3, %add5, !dbg !1903
  br i1 %cmp, label %land.lhs.true, label %if.end, !dbg !1904

land.lhs.true:                                    ; preds = %entry
  %4 = load i8, i8* addrspacecast (i8 addrspace(1)* @VERBOSE to i8*), align 1, !dbg !1905, !tbaa !1433, !range !1435
  %tobool = icmp eq i8 %4, 0, !dbg !1905
  br i1 %tobool, label %if.end, label %if.then, !dbg !1907

if.then:                                          ; preds = %land.lhs.true
  %5 = getelementptr inbounds %printf_args.12, %printf_args.12* %tmp, i64 0, i32 0, !dbg !1909
  store i32 %line, i32* %5, align 8, !dbg !1909
  %6 = getelementptr inbounds %printf_args.12, %printf_args.12* %tmp, i64 0, i32 1, !dbg !1909
  store i32 %col, i32* %6, align 4, !dbg !1909
  %7 = getelementptr inbounds %printf_args.12, %printf_args.12* %tmp, i64 0, i32 2, !dbg !1909
  store i32 %2, i32* %7, align 8, !dbg !1909
  %8 = getelementptr inbounds %printf_args.12, %printf_args.12* %tmp, i64 0, i32 3, !dbg !1909
  store i32 %3, i32* %8, align 4, !dbg !1909
  %9 = bitcast %printf_args.12* %tmp to i8*, !dbg !1909
  %10 = call i32 @vprintf(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.23, i64 0, i64 0), i8* nonnull %9) #10, !dbg !1910
  br label %if.end, !dbg !1909

if.end:                                           ; preds = %land.lhs.true, %if.then, %entry
  ret void, !dbg !1911
}

; Function Attrs: convergent nounwind
define void @print5(i8* %p, i32 %bits, i32 %sline, i32 %scolm, i32 %op) local_unnamed_addr #2 !dbg !1912 {
entry:
  %tmp.i = alloca %printf_args.8, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1916, metadata !991), !dbg !1921
  tail call void @llvm.dbg.value(metadata i32 %bits, i64 0, metadata !1917, metadata !991), !dbg !1922
  tail call void @llvm.dbg.value(metadata i32 %sline, i64 0, metadata !1918, metadata !991), !dbg !1923
  tail call void @llvm.dbg.value(metadata i32 %scolm, i64 0, metadata !1919, metadata !991), !dbg !1924
  tail call void @llvm.dbg.value(metadata i32 %op, i64 0, metadata !1920, metadata !991), !dbg !1925
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !1926, !range !1415
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !1929, !range !1415
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #10, !dbg !1932, !range !1629
  %mul = mul nuw i32 %2, %1, !dbg !1935
  %add = add i32 %mul, %0, !dbg !1936
  %3 = load i32, i32* addrspacecast (i32 addrspace(1)* @CTALB to i32*), align 4, !dbg !1937, !tbaa !1054
  %cmp = icmp ult i32 %add, %3, !dbg !1938
  br i1 %cmp, label %return, label %lor.lhs.false, !dbg !1939

lor.lhs.false:                                    ; preds = %entry
  %4 = load i32, i32* addrspacecast (i32 addrspace(1)* @CTAUB to i32*), align 4, !dbg !1940, !tbaa !1054
  %cmp8 = icmp ugt i32 %add, %4, !dbg !1942
  br i1 %cmp8, label %return, label %if.end, !dbg !1943

if.end:                                           ; preds = %lor.lhs.false
  %5 = bitcast %printf_args.8* %tmp.i to i8*, !dbg !1945
  call void @llvm.lifetime.start(i64 8, i8* nonnull %5), !dbg !1945
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1721, metadata !991) #10, !dbg !1945
  tail call void @llvm.dbg.value(metadata i16 %conv, i64 0, metadata !1722, metadata !991) #10, !dbg !1947
  tail call void @llvm.dbg.value(metadata i16 %conv9, i64 0, metadata !1723, metadata !991) #10, !dbg !1948
  tail call void @llvm.dbg.value(metadata i16 %conv10, i64 0, metadata !1724, metadata !991) #10, !dbg !1949
  tail call void @llvm.dbg.value(metadata i16 %conv11, i64 0, metadata !1725, metadata !991) #10, !dbg !1950
  %6 = load i64, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !1951, !tbaa !1574
  %cmp.i = icmp ult i64 %6, 67108736, !dbg !1951
  br i1 %cmp.i, label %cond.end.i, label %cond.false.i, !dbg !1951

cond.false.i:                                     ; preds = %if.end
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.17, i64 0, i64 0), i32 498, i8* getelementptr inbounds ([52 x i8], [52 x i8]* @__PRETTY_FUNCTION__._Z10storeLinesPvssss, i64 0, i64 0)) #9, !dbg !1952
  unreachable

cond.end.i:                                       ; preds = %if.end
  %conv11 = trunc i32 %op to i16, !dbg !1953
  %conv10 = trunc i32 %scolm to i16, !dbg !1954
  %conv9 = trunc i32 %sline to i16, !dbg !1955
  %div = sdiv i32 %bits, 8, !dbg !1956
  %conv = trunc i32 %div to i16, !dbg !1957
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1641, metadata !991) #10, !dbg !1958
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1651, metadata !991) #10, !dbg !1960
  %7 = atomicrmw add i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), i64 1 seq_cst, !dbg !1962
  %conv.i = trunc i64 %7 to i32, !dbg !1963
  tail call void @llvm.dbg.value(metadata i32 %conv.i, i64 0, metadata !1726, metadata !991) #10, !dbg !1964
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !1727, metadata !991) #10, !dbg !1965
  tail call void @llvm.dbg.value(metadata i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 0, metadata !1728, metadata !991) #10, !dbg !1966
  %conv2.i = trunc i32 %0 to i16, !dbg !1967
  %mul.i = mul nsw i32 %conv.i, 12, !dbg !1968
  %idxprom.i = sext i32 %mul.i to i64, !dbg !1969
  %arrayidx.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom.i, !dbg !1969
  store i16 %conv2.i, i16* %arrayidx.i, align 2, !dbg !1970, !tbaa !1752
  %conv4.i = trunc i32 %1 to i16, !dbg !1971
  %add6.i = or i32 %mul.i, 1, !dbg !1972
  %idxprom7.i = sext i32 %add6.i to i64, !dbg !1973
  %arrayidx8.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom7.i, !dbg !1973
  store i16 %conv4.i, i16* %arrayidx8.i, align 2, !dbg !1974, !tbaa !1752
  %8 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !1975, !range !1383
  %conv10.i = trunc i32 %8 to i16, !dbg !1977
  %add12.i = or i32 %mul.i, 2, !dbg !1978
  %idxprom13.i = sext i32 %add12.i to i64, !dbg !1979
  %arrayidx14.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom13.i, !dbg !1979
  store i16 %conv10.i, i16* %arrayidx14.i, align 2, !dbg !1980, !tbaa !1752
  %9 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !1981, !range !1383
  %conv16.i = trunc i32 %9 to i16, !dbg !1983
  %add18.i = or i32 %mul.i, 3, !dbg !1984
  %idxprom19.i = sext i32 %add18.i to i64, !dbg !1985
  %arrayidx20.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom19.i, !dbg !1985
  store i16 %conv16.i, i16* %arrayidx20.i, align 2, !dbg !1986, !tbaa !1752
  %10 = ptrtoint i8* %p to i64, !dbg !1987
  %add22.i = mul i64 %7, 12884901888, !dbg !1988
  %sext.i = add i64 %add22.i, 4294967296, !dbg !1988
  %idxprom23.i = ashr exact i64 %sext.i, 32, !dbg !1988
  %arrayidx24.i = getelementptr inbounds i64, i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 %idxprom23.i, !dbg !1988
  store i64 %10, i64* %arrayidx24.i, align 8, !dbg !1989, !tbaa !1774
  %add26.i = add nsw i32 %mul.i, 8, !dbg !1990
  %idxprom27.i = sext i32 %add26.i to i64, !dbg !1991
  %arrayidx28.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom27.i, !dbg !1991
  store i16 %conv, i16* %arrayidx28.i, align 2, !dbg !1992, !tbaa !1752
  %add30.i = add nsw i32 %mul.i, 9, !dbg !1993
  %idxprom31.i = sext i32 %add30.i to i64, !dbg !1994
  %arrayidx32.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom31.i, !dbg !1994
  store i16 %conv9, i16* %arrayidx32.i, align 2, !dbg !1995, !tbaa !1752
  %add34.i = add nsw i32 %mul.i, 10, !dbg !1996
  %idxprom35.i = sext i32 %add34.i to i64, !dbg !1997
  %arrayidx36.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom35.i, !dbg !1997
  store i16 %conv10, i16* %arrayidx36.i, align 2, !dbg !1998, !tbaa !1752
  %add38.i = add nsw i32 %mul.i, 11, !dbg !1999
  %idxprom39.i = sext i32 %add38.i to i64, !dbg !2000
  %arrayidx40.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom39.i, !dbg !2000
  store i16 %conv11, i16* %arrayidx40.i, align 2, !dbg !2001, !tbaa !1752
  %cmp42.i = icmp slt i32 %conv.i, 5, !dbg !2002
  br i1 %cmp42.i, label %if.then.i, label %_Z10storeLinesPvssss.exit, !dbg !2003

if.then.i:                                        ; preds = %cond.end.i
  %11 = getelementptr inbounds %printf_args.8, %printf_args.8* %tmp.i, i64 0, i32 0, !dbg !2004
  store i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 0), i32** %11, align 8, !dbg !2004
  %12 = call i32 @vprintf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.18, i64 0, i64 0), i8* nonnull %5) #10, !dbg !2004
  br label %_Z10storeLinesPvssss.exit, !dbg !2004

_Z10storeLinesPvssss.exit:                        ; preds = %cond.end.i, %if.then.i
  call void @llvm.lifetime.end(i64 8, i8* nonnull %5), !dbg !2005
  br label %return, !dbg !2006

return:                                           ; preds = %entry, %lor.lhs.false, %_Z10storeLinesPvssss.exit
  ret void, !dbg !2007
}

; Function Attrs: nounwind
define void @RetKernel() local_unnamed_addr #0 !dbg !2008 {
entry:
  %tmp = alloca %printf_args.13, align 8
  %tmp19 = alloca %printf_args.14, align 8
  %tmp23 = alloca %printf_args.15, align 8
  %tmp24 = alloca %printf_args.16, align 8
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !2018, !range !1383
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !2020, !range !1383
  %add = add nuw nsw i32 %1, %0, !dbg !2023
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !2024, !range !1415
  %add3 = add nuw nsw i32 %add, %2, !dbg !2027
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !2028, !range !1415
  %add5 = sub nsw i32 0, %3, !dbg !2031
  %cmp = icmp eq i32 %add3, %add5, !dbg !2031
  br i1 %cmp, label %if.then, label %if.end, !dbg !2032

if.then:                                          ; preds = %entry
  tail call void @llvm.dbg.value(metadata %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 0, metadata !2010, metadata !991), !dbg !2033
  %4 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #10, !dbg !2034, !range !2062
  %conv = trunc i32 %4 to i16, !dbg !2063
  store i16 %conv, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), align 8, !dbg !2064, !tbaa !1690
  %5 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.y() #10, !dbg !2065, !range !2062
  %conv8 = trunc i32 %5 to i16, !dbg !2068
  store i16 %conv8, i16* getelementptr (%struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 0, i32 1), align 2, !dbg !2069, !tbaa !1694
  %6 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #10, !dbg !2070, !range !1629
  %conv11 = trunc i32 %6 to i16, !dbg !2072
  store i16 %conv11, i16* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 1) to i16*), align 4, !dbg !2073, !tbaa !1680
  %7 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.y() #10, !dbg !2074, !range !1629
  %conv14 = trunc i32 %7 to i16, !dbg !2077
  store i16 %conv14, i16* getelementptr (%struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 0, i32 3), align 2, !dbg !2078, !tbaa !1686
  %8 = load i64, i64* addrspacecast (i64 addrspace(1)* @bbccnntt to i64*), align 8, !dbg !2079, !tbaa !1574
  store i64 %8, i64* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 2) to i64*), align 8, !dbg !2080, !tbaa !1673
  store i32 0, i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 4), align 8, !dbg !2081, !tbaa !1697
  store i32 0, i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 5), align 4, !dbg !2082, !tbaa !1700
  %9 = getelementptr inbounds %printf_args.13, %printf_args.13* %tmp, i64 0, i32 0, !dbg !2083
  store i64 %8, i64* %9, align 8, !dbg !2083
  %10 = bitcast %printf_args.13* %tmp to i8*, !dbg !2083
  %11 = call i32 @vprintf(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.24, i64 0, i64 0), i8* nonnull %10) #10, !dbg !2083
  %12 = load i64, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !2084, !tbaa !1574
  %13 = getelementptr inbounds %printf_args.14, %printf_args.14* %tmp19, i64 0, i32 0, !dbg !2085
  store i64 %12, i64* %13, align 8, !dbg !2085
  %14 = bitcast %printf_args.14* %tmp19 to i8*, !dbg !2085
  %15 = call i32 @vprintf(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.24, i64 0, i64 0), i8* nonnull %14) #10, !dbg !2085
  call void @llvm.dbg.value(metadata i64 1024, i64 0, metadata !2015, metadata !991), !dbg !2086
  call void @llvm.dbg.value(metadata i64 3072, i64 0, metadata !2016, metadata !991), !dbg !2087
  %16 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 0, !dbg !2088
  store i32 15, i32* %16, align 8, !dbg !2088
  %17 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 1, !dbg !2088
  store i32 31, i32* %17, align 4, !dbg !2088
  %18 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 2, !dbg !2088
  store i64 1, i64* %18, align 8, !dbg !2088
  %19 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 3, !dbg !2088
  store i64 465, i64* %19, align 8, !dbg !2088
  %20 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 4, !dbg !2088
  store i64 1024, i64* %20, align 8, !dbg !2088
  %21 = bitcast %printf_args.15* %tmp23 to i8*, !dbg !2088
  %22 = call i32 @vprintf(i8* getelementptr inbounds ([57 x i8], [57 x i8]* @.str.25, i64 0, i64 0), i8* nonnull %21) #10, !dbg !2088
  %23 = getelementptr inbounds %printf_args.16, %printf_args.16* %tmp24, i64 0, i32 0, !dbg !2089
  store i32 15, i32* %23, align 8, !dbg !2089
  %24 = getelementptr inbounds %printf_args.16, %printf_args.16* %tmp24, i64 0, i32 1, !dbg !2089
  store i32 15, i32* %24, align 4, !dbg !2089
  %25 = getelementptr inbounds %printf_args.16, %printf_args.16* %tmp24, i64 0, i32 2, !dbg !2089
  store i64 8, i64* %25, align 8, !dbg !2089
  %26 = getelementptr inbounds %printf_args.16, %printf_args.16* %tmp24, i64 0, i32 3, !dbg !2089
  store i64 1800, i64* %26, align 8, !dbg !2089
  %27 = getelementptr inbounds %printf_args.16, %printf_args.16* %tmp24, i64 0, i32 4, !dbg !2089
  store i64 3072, i64* %27, align 8, !dbg !2089
  %28 = bitcast %printf_args.16* %tmp24 to i8*, !dbg !2089
  %29 = call i32 @vprintf(i8* getelementptr inbounds ([56 x i8], [56 x i8]* @.str.26, i64 0, i64 0), i8* nonnull %28) #10, !dbg !2089
  call void @llvm.dbg.value(metadata i8* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i32 0, i64 402652928) to i8*), i64 0, metadata !2017, metadata !991), !dbg !2090
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 402652928) to i8*), i8* addrspacecast (i8 addrspace(1)* getelementptr inbounds ([15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 0, i64 0) to i8*), i64 465, i32 1, i1 false) #10, !dbg !2091
  call void @llvm.dbg.value(metadata i8* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i32 0, i64 402652416) to i8*), i64 0, metadata !2017, metadata !991), !dbg !2090
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 402652416) to i8*), i8* addrspacecast (i8 addrspace(1)* bitcast ([20 x [5 x %struct.CallSite_t]] addrspace(1)* @contextDic to i8 addrspace(1)*) to i8*), i64 1800, i32 1, i1 false) #10, !dbg !2100
  store i64 1, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !2102, !tbaa !1574
  store i64 1, i64* addrspacecast (i64 addrspace(1)* @bbccnntt to i64*), align 8, !dbg !2103, !tbaa !1574
  br label %if.end, !dbg !2104

if.end:                                           ; preds = %if.then, %entry
  ret void, !dbg !2105
}

; Function Attrs: convergent noreturn nounwind
declare void @__assertfail(i8*, i8*, i32, i8*, i64) local_unnamed_addr #7

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.x() #8

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #8

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.y() #8

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #8

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #8

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #8

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.y() #8

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.y() #8

; Function Attrs: nounwind readnone
declare void @llvm.dbg.value(metadata, i64, metadata, metadata) #8

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #6

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #6

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readonly "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { convergent nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { convergent inlinehint noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { norecurse nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind }
attributes #7 = { convergent noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind readnone }
attributes #9 = { convergent nounwind }
attributes #10 = { nounwind }
attributes #11 = { convergent noreturn nounwind }

!llvm.dbg.cu = !{!2}
!llvm.module.flags = !{!975, !976, !977}
!llvm.ident = !{!978}
!nvvm.internalize.after.link = !{}
!nvvmir.version = !{!979}
!nvvm.annotations = !{!980, !981, !980, !982, !982, !982, !982, !983, !983, !982}

!0 = !DIGlobalVariableExpression(var: !1)
!1 = distinct !DIGlobalVariable(name: "CTALB", scope: !2, file: !3, line: 15, type: !8, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C_plus_plus, file: !3, producer: "clang version 5.0.0 (trunk 294196)", isOptimized: true, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, retainedTypes: !5, globals: !32, imports: !79)
!3 = !DIFile(filename: "/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/ansf.cu", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!4 = !{}
!5 = !{!6, !8, !9, !24, !25, !26, !15, !27, !29, !30}
!6 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !7, size: 64)
!7 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!8 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!9 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !10, size: 64)
!10 = !DIDerivedType(tag: DW_TAG_typedef, name: "BBlog_t", file: !11, line: 37, baseType: !12)
!11 = !DIFile(filename: "/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/types.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!12 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "BBlog_t", file: !11, line: 27, size: 256, elements: !13, identifier: "_ZTS7BBlog_t")
!13 = !{!14, !16, !17, !18, !19, !21, !22, !23}
!14 = !DIDerivedType(tag: DW_TAG_member, name: "bidx", scope: !12, file: !11, line: 28, baseType: !15, size: 16)
!15 = !DIBasicType(name: "short", size: 16, encoding: DW_ATE_signed)
!16 = !DIDerivedType(tag: DW_TAG_member, name: "bidy", scope: !12, file: !11, line: 29, baseType: !15, size: 16, offset: 16)
!17 = !DIDerivedType(tag: DW_TAG_member, name: "tidx", scope: !12, file: !11, line: 30, baseType: !15, size: 16, offset: 32)
!18 = !DIDerivedType(tag: DW_TAG_member, name: "tidy", scope: !12, file: !11, line: 31, baseType: !15, size: 16, offset: 48)
!19 = !DIDerivedType(tag: DW_TAG_member, name: "key", scope: !12, file: !11, line: 32, baseType: !20, size: 64, offset: 64)
!20 = !DIBasicType(name: "long long unsigned int", size: 64, encoding: DW_ATE_unsigned)
!21 = !DIDerivedType(tag: DW_TAG_member, name: "sline", scope: !12, file: !11, line: 33, baseType: !8, size: 32, offset: 128)
!22 = !DIDerivedType(tag: DW_TAG_member, name: "scolm", scope: !12, file: !11, line: 34, baseType: !8, size: 32, offset: 160)
!23 = !DIDerivedType(tag: DW_TAG_member, name: "cid", scope: !12, file: !11, line: 35, baseType: !8, size: 32, offset: 192)
!24 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !15, size: 64)
!25 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !26, size: 64)
!26 = !DIBasicType(name: "long int", size: 64, encoding: DW_ATE_signed)
!27 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !28, size: 64)
!28 = !DIDerivedType(tag: DW_TAG_volatile_type, baseType: !29)
!29 = !DIBasicType(name: "long long int", size: 64, encoding: DW_ATE_signed)
!30 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !31, size: 64)
!31 = !DIBasicType(name: "unsigned char", size: 8, encoding: DW_ATE_unsigned_char)
!32 = !{!0, !33, !35, !37, !40, !42, !44, !46, !48, !54, !56, !68, !71, !77}
!33 = !DIGlobalVariableExpression(var: !34)
!34 = distinct !DIGlobalVariable(name: "CTAUB", scope: !2, file: !3, line: 16, type: !8, isLocal: false, isDefinition: true)
!35 = !DIGlobalVariableExpression(var: !36)
!36 = distinct !DIGlobalVariable(name: "CONSTANCE", scope: !2, file: !3, line: 17, type: !8, isLocal: false, isDefinition: true)
!37 = !DIGlobalVariableExpression(var: !38)
!38 = distinct !DIGlobalVariable(name: "VERBOSE", scope: !2, file: !3, line: 22, type: !39, isLocal: false, isDefinition: true)
!39 = !DIBasicType(name: "bool", size: 8, encoding: DW_ATE_boolean)
!40 = !DIGlobalVariableExpression(var: !41)
!41 = distinct !DIGlobalVariable(name: "ccnntt", scope: !2, file: !3, line: 35, type: !20, isLocal: false, isDefinition: true)
!42 = !DIGlobalVariableExpression(var: !43)
!43 = distinct !DIGlobalVariable(name: "bbccnntt", scope: !2, file: !3, line: 36, type: !20, isLocal: false, isDefinition: true)
!44 = !DIGlobalVariableExpression(var: !45)
!45 = distinct !DIGlobalVariable(name: "done1", scope: !2, file: !3, line: 42, type: !8, isLocal: false, isDefinition: true)
!46 = !DIGlobalVariableExpression(var: !47)
!47 = distinct !DIGlobalVariable(name: "done2", scope: !2, file: !3, line: 43, type: !8, isLocal: false, isDefinition: true)
!48 = !DIGlobalVariableExpression(var: !49)
!49 = distinct !DIGlobalVariable(name: "funcDic", scope: !2, file: !3, line: 46, type: !50, isLocal: false, isDefinition: true)
!50 = !DICompositeType(tag: DW_TAG_array_type, baseType: !7, size: 3720, elements: !51)
!51 = !{!52, !53}
!52 = !DISubrange(count: 15)
!53 = !DISubrange(count: 31)
!54 = !DIGlobalVariableExpression(var: !55)
!55 = distinct !DIGlobalVariable(name: "dicHeight", scope: !2, file: !3, line: 47, type: !8, isLocal: false, isDefinition: true)
!56 = !DIGlobalVariableExpression(var: !57)
!57 = distinct !DIGlobalVariable(name: "globalCallStack", scope: !2, file: !3, line: 49, type: !58, isLocal: false, isDefinition: true)
!58 = !DICompositeType(tag: DW_TAG_array_type, baseType: !59, size: 640, elements: !66)
!59 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !60, size: 64)
!60 = !DIDerivedType(tag: DW_TAG_typedef, name: "CallSite_t", file: !11, line: 12, baseType: !61)
!61 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "CallSite_t", file: !11, line: 8, size: 64, elements: !62, identifier: "_ZTS10CallSite_t")
!62 = !{!63, !64, !65}
!63 = !DIDerivedType(tag: DW_TAG_member, name: "id", scope: !61, file: !11, line: 9, baseType: !8, size: 32)
!64 = !DIDerivedType(tag: DW_TAG_member, name: "sline", scope: !61, file: !11, line: 10, baseType: !15, size: 16, offset: 32)
!65 = !DIDerivedType(tag: DW_TAG_member, name: "scolm", scope: !61, file: !11, line: 11, baseType: !15, size: 16, offset: 48)
!66 = !{!67}
!67 = !DISubrange(count: 10)
!68 = !DIGlobalVariableExpression(var: !69)
!69 = distinct !DIGlobalVariable(name: "stackHeight", scope: !2, file: !3, line: 50, type: !70, isLocal: false, isDefinition: true)
!70 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !8, size: 64)
!71 = !DIGlobalVariableExpression(var: !72)
!72 = distinct !DIGlobalVariable(name: "contextDic", scope: !2, file: !3, line: 56, type: !73, isLocal: false, isDefinition: true)
!73 = !DICompositeType(tag: DW_TAG_array_type, baseType: !60, size: 6400, elements: !74)
!74 = !{!75, !76}
!75 = !DISubrange(count: 20)
!76 = !DISubrange(count: 5)
!77 = !DIGlobalVariableExpression(var: !78)
!78 = distinct !DIGlobalVariable(name: "cHeight", scope: !2, file: !3, line: 57, type: !8, isLocal: false, isDefinition: true)
!79 = !{!80, !86, !91, !93, !95, !97, !99, !103, !105, !107, !109, !111, !113, !115, !117, !119, !121, !123, !125, !127, !129, !131, !135, !137, !139, !141, !145, !149, !151, !153, !157, !161, !163, !165, !167, !169, !171, !173, !175, !177, !181, !185, !187, !189, !193, !195, !197, !199, !201, !203, !207, !209, !211, !216, !223, !227, !229, !231, !235, !237, !239, !243, !245, !247, !251, !253, !255, !257, !259, !261, !263, !265, !267, !269, !274, !276, !278, !282, !284, !286, !288, !290, !292, !294, !296, !300, !304, !306, !308, !313, !315, !317, !319, !321, !323, !325, !329, !335, !339, !343, !348, !351, !355, !359, !374, !378, !382, !386, !390, !394, !396, !400, !404, !408, !416, !420, !424, !428, !432, !437, !443, !447, !451, !453, !461, !465, !473, !475, !477, !481, !485, !489, !493, !497, !502, !503, !504, !505, !508, !509, !510, !511, !512, !513, !514, !517, !519, !521, !523, !525, !527, !529, !531, !534, !536, !538, !540, !542, !544, !546, !548, !550, !552, !554, !556, !558, !560, !562, !564, !566, !568, !570, !572, !574, !576, !578, !580, !582, !584, !586, !588, !590, !592, !594, !596, !598, !602, !603, !605, !607, !609, !611, !613, !615, !617, !619, !621, !623, !625, !627, !629, !631, !636, !638, !642, !650, !655, !659, !663, !667, !671, !673, !675, !679, !685, !689, !695, !701, !703, !707, !711, !715, !719, !726, !728, !732, !736, !740, !742, !746, !750, !754, !756, !758, !762, !770, !774, !778, !782, !784, !790, !792, !798, !802, !806, !810, !814, !818, !822, !824, !826, !830, !834, !838, !840, !844, !848, !850, !852, !856, !860, !864, !868, !869, !870, !871, !875, !878, !882, !887, !890, !892, !894, !896, !898, !900, !902, !904, !906, !908, !910, !912, !914, !917, !919, !926, !928, !929, !931, !933, !935, !937, !941, !943, !945, !947, !949, !951, !953, !955, !957, !961, !965, !967, !971}
!80 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !83, line: 201)
!81 = !DINamespace(name: "std", scope: null, file: !82, line: 195)
!82 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_math_forward_declares.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!83 = !DISubprogram(name: "abs", linkageName: "_ZL3absx", scope: !82, file: !82, line: 44, type: !84, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!84 = !DISubroutineType(types: !85)
!85 = !{!29, !29}
!86 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !87, line: 202)
!87 = !DISubprogram(name: "acos", linkageName: "_ZL4acosf", scope: !82, file: !82, line: 46, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!88 = !DISubroutineType(types: !89)
!89 = !{!90, !90}
!90 = !DIBasicType(name: "float", size: 32, encoding: DW_ATE_float)
!91 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !92, line: 203)
!92 = !DISubprogram(name: "acosh", linkageName: "_ZL5acoshf", scope: !82, file: !82, line: 48, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!93 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !94, line: 204)
!94 = !DISubprogram(name: "asin", linkageName: "_ZL4asinf", scope: !82, file: !82, line: 50, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!95 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !96, line: 205)
!96 = !DISubprogram(name: "asinh", linkageName: "_ZL5asinhf", scope: !82, file: !82, line: 52, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!97 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !98, line: 206)
!98 = !DISubprogram(name: "atan", linkageName: "_ZL4atanf", scope: !82, file: !82, line: 56, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!99 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !100, line: 207)
!100 = !DISubprogram(name: "atan2", linkageName: "_ZL5atan2ff", scope: !82, file: !82, line: 54, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!101 = !DISubroutineType(types: !102)
!102 = !{!90, !90, !90}
!103 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !104, line: 208)
!104 = !DISubprogram(name: "atanh", linkageName: "_ZL5atanhf", scope: !82, file: !82, line: 58, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!105 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !106, line: 209)
!106 = !DISubprogram(name: "cbrt", linkageName: "_ZL4cbrtf", scope: !82, file: !82, line: 60, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!107 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !108, line: 210)
!108 = !DISubprogram(name: "ceil", linkageName: "_ZL4ceilf", scope: !82, file: !82, line: 62, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!109 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !110, line: 211)
!110 = !DISubprogram(name: "copysign", linkageName: "_ZL8copysignff", scope: !82, file: !82, line: 64, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!111 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !112, line: 212)
!112 = !DISubprogram(name: "cos", linkageName: "_ZL3cosf", scope: !82, file: !82, line: 66, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!113 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !114, line: 213)
!114 = !DISubprogram(name: "cosh", linkageName: "_ZL4coshf", scope: !82, file: !82, line: 68, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!115 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !116, line: 214)
!116 = !DISubprogram(name: "erf", linkageName: "_ZL3erff", scope: !82, file: !82, line: 72, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!117 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !118, line: 215)
!118 = !DISubprogram(name: "erfc", linkageName: "_ZL4erfcf", scope: !82, file: !82, line: 70, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!119 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !120, line: 216)
!120 = !DISubprogram(name: "exp", linkageName: "_ZL3expf", scope: !82, file: !82, line: 76, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!121 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !122, line: 217)
!122 = !DISubprogram(name: "exp2", linkageName: "_ZL4exp2f", scope: !82, file: !82, line: 74, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!123 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !124, line: 218)
!124 = !DISubprogram(name: "expm1", linkageName: "_ZL5expm1f", scope: !82, file: !82, line: 78, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!125 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !126, line: 219)
!126 = !DISubprogram(name: "fabs", linkageName: "_ZL4fabsf", scope: !82, file: !82, line: 80, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!127 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !128, line: 220)
!128 = !DISubprogram(name: "fdim", linkageName: "_ZL4fdimff", scope: !82, file: !82, line: 82, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!129 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !130, line: 221)
!130 = !DISubprogram(name: "floor", linkageName: "_ZL5floorf", scope: !82, file: !82, line: 84, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!131 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !132, line: 222)
!132 = !DISubprogram(name: "fma", linkageName: "_ZL3fmafff", scope: !82, file: !82, line: 86, type: !133, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!133 = !DISubroutineType(types: !134)
!134 = !{!90, !90, !90, !90}
!135 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !136, line: 223)
!136 = !DISubprogram(name: "fmax", linkageName: "_ZL4fmaxff", scope: !82, file: !82, line: 88, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!137 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !138, line: 224)
!138 = !DISubprogram(name: "fmin", linkageName: "_ZL4fminff", scope: !82, file: !82, line: 90, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!139 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !140, line: 225)
!140 = !DISubprogram(name: "fmod", linkageName: "_ZL4fmodff", scope: !82, file: !82, line: 92, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!141 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !142, line: 226)
!142 = !DISubprogram(name: "fpclassify", linkageName: "_ZL10fpclassifyf", scope: !82, file: !82, line: 94, type: !143, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!143 = !DISubroutineType(types: !144)
!144 = !{!8, !90}
!145 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !146, line: 227)
!146 = !DISubprogram(name: "frexp", linkageName: "_ZL5frexpfPi", scope: !82, file: !82, line: 96, type: !147, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!147 = !DISubroutineType(types: !148)
!148 = !{!90, !90, !70}
!149 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !150, line: 228)
!150 = !DISubprogram(name: "hypot", linkageName: "_ZL5hypotff", scope: !82, file: !82, line: 98, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!151 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !152, line: 229)
!152 = !DISubprogram(name: "ilogb", linkageName: "_ZL5ilogbf", scope: !82, file: !82, line: 100, type: !143, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!153 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !154, line: 230)
!154 = !DISubprogram(name: "isfinite", linkageName: "_ZL8isfinitef", scope: !82, file: !82, line: 102, type: !155, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!155 = !DISubroutineType(types: !156)
!156 = !{!39, !90}
!157 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !158, line: 231)
!158 = !DISubprogram(name: "isgreater", linkageName: "_ZL9isgreaterff", scope: !82, file: !82, line: 106, type: !159, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!159 = !DISubroutineType(types: !160)
!160 = !{!39, !90, !90}
!161 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !162, line: 232)
!162 = !DISubprogram(name: "isgreaterequal", linkageName: "_ZL14isgreaterequalff", scope: !82, file: !82, line: 105, type: !159, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!163 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !164, line: 233)
!164 = !DISubprogram(name: "isinf", linkageName: "_ZL5isinff", scope: !82, file: !82, line: 108, type: !155, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!165 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !166, line: 234)
!166 = !DISubprogram(name: "isless", linkageName: "_ZL6islessff", scope: !82, file: !82, line: 112, type: !159, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!167 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !168, line: 235)
!168 = !DISubprogram(name: "islessequal", linkageName: "_ZL11islessequalff", scope: !82, file: !82, line: 111, type: !159, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!169 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !170, line: 236)
!170 = !DISubprogram(name: "islessgreater", linkageName: "_ZL13islessgreaterff", scope: !82, file: !82, line: 114, type: !159, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!171 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !172, line: 237)
!172 = !DISubprogram(name: "isnan", linkageName: "_ZL5isnanf", scope: !82, file: !82, line: 116, type: !155, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!173 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !174, line: 238)
!174 = !DISubprogram(name: "isnormal", linkageName: "_ZL8isnormalf", scope: !82, file: !82, line: 118, type: !155, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!175 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !176, line: 239)
!176 = !DISubprogram(name: "isunordered", linkageName: "_ZL11isunorderedff", scope: !82, file: !82, line: 120, type: !159, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!177 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !178, line: 240)
!178 = !DISubprogram(name: "labs", linkageName: "_ZL4labsl", scope: !82, file: !82, line: 121, type: !179, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!179 = !DISubroutineType(types: !180)
!180 = !{!26, !26}
!181 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !182, line: 241)
!182 = !DISubprogram(name: "ldexp", linkageName: "_ZL5ldexpfi", scope: !82, file: !82, line: 123, type: !183, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!183 = !DISubroutineType(types: !184)
!184 = !{!90, !90, !8}
!185 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !186, line: 242)
!186 = !DISubprogram(name: "lgamma", linkageName: "_ZL6lgammaf", scope: !82, file: !82, line: 125, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!187 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !188, line: 243)
!188 = !DISubprogram(name: "llabs", linkageName: "_ZL5llabsx", scope: !82, file: !82, line: 126, type: !84, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!189 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !190, line: 244)
!190 = !DISubprogram(name: "llrint", linkageName: "_ZL6llrintf", scope: !82, file: !82, line: 128, type: !191, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!191 = !DISubroutineType(types: !192)
!192 = !{!29, !90}
!193 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !194, line: 245)
!194 = !DISubprogram(name: "log", linkageName: "_ZL3logf", scope: !82, file: !82, line: 138, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!195 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !196, line: 246)
!196 = !DISubprogram(name: "log10", linkageName: "_ZL5log10f", scope: !82, file: !82, line: 130, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!197 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !198, line: 247)
!198 = !DISubprogram(name: "log1p", linkageName: "_ZL5log1pf", scope: !82, file: !82, line: 132, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!199 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !200, line: 248)
!200 = !DISubprogram(name: "log2", linkageName: "_ZL4log2f", scope: !82, file: !82, line: 134, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!201 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !202, line: 249)
!202 = !DISubprogram(name: "logb", linkageName: "_ZL4logbf", scope: !82, file: !82, line: 136, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!203 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !204, line: 250)
!204 = !DISubprogram(name: "lrint", linkageName: "_ZL5lrintf", scope: !82, file: !82, line: 140, type: !205, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!205 = !DISubroutineType(types: !206)
!206 = !{!26, !90}
!207 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !208, line: 251)
!208 = !DISubprogram(name: "lround", linkageName: "_ZL6lroundf", scope: !82, file: !82, line: 142, type: !205, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!209 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !210, line: 252)
!210 = !DISubprogram(name: "llround", linkageName: "_ZL7llroundf", scope: !82, file: !82, line: 143, type: !191, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!211 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !212, line: 253)
!212 = !DISubprogram(name: "modf", linkageName: "_ZL4modffPf", scope: !82, file: !82, line: 145, type: !213, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!213 = !DISubroutineType(types: !214)
!214 = !{!90, !90, !215}
!215 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !90, size: 64)
!216 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !217, line: 254)
!217 = !DISubprogram(name: "nan", linkageName: "_ZL3nanPKc", scope: !82, file: !82, line: 146, type: !218, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!218 = !DISubroutineType(types: !219)
!219 = !{!220, !221}
!220 = !DIBasicType(name: "double", size: 64, encoding: DW_ATE_float)
!221 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !222, size: 64)
!222 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !7)
!223 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !224, line: 255)
!224 = !DISubprogram(name: "nanf", linkageName: "_ZL4nanfPKc", scope: !82, file: !82, line: 147, type: !225, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!225 = !DISubroutineType(types: !226)
!226 = !{!90, !221}
!227 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !228, line: 256)
!228 = !DISubprogram(name: "nearbyint", linkageName: "_ZL9nearbyintf", scope: !82, file: !82, line: 149, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!229 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !230, line: 257)
!230 = !DISubprogram(name: "nextafter", linkageName: "_ZL9nextafterff", scope: !82, file: !82, line: 151, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!231 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !232, line: 258)
!232 = !DISubprogram(name: "nexttoward", linkageName: "_ZL10nexttowardfd", scope: !82, file: !82, line: 153, type: !233, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!233 = !DISubroutineType(types: !234)
!234 = !{!90, !90, !220}
!235 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !236, line: 259)
!236 = !DISubprogram(name: "pow", linkageName: "_ZL3powfi", scope: !82, file: !82, line: 158, type: !183, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!237 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !238, line: 260)
!238 = !DISubprogram(name: "remainder", linkageName: "_ZL9remainderff", scope: !82, file: !82, line: 160, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!239 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !240, line: 261)
!240 = !DISubprogram(name: "remquo", linkageName: "_ZL6remquoffPi", scope: !82, file: !82, line: 162, type: !241, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!241 = !DISubroutineType(types: !242)
!242 = !{!90, !90, !90, !70}
!243 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !244, line: 262)
!244 = !DISubprogram(name: "rint", linkageName: "_ZL4rintf", scope: !82, file: !82, line: 164, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!245 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !246, line: 263)
!246 = !DISubprogram(name: "round", linkageName: "_ZL5roundf", scope: !82, file: !82, line: 166, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!247 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !248, line: 264)
!248 = !DISubprogram(name: "scalbln", linkageName: "_ZL7scalblnfl", scope: !82, file: !82, line: 168, type: !249, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!249 = !DISubroutineType(types: !250)
!250 = !{!90, !90, !26}
!251 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !252, line: 265)
!252 = !DISubprogram(name: "scalbn", linkageName: "_ZL6scalbnfi", scope: !82, file: !82, line: 170, type: !183, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!253 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !254, line: 266)
!254 = !DISubprogram(name: "signbit", linkageName: "_ZL7signbitf", scope: !82, file: !82, line: 172, type: !155, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!255 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !256, line: 267)
!256 = !DISubprogram(name: "sin", linkageName: "_ZL3sinf", scope: !82, file: !82, line: 174, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!257 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !258, line: 268)
!258 = !DISubprogram(name: "sinh", linkageName: "_ZL4sinhf", scope: !82, file: !82, line: 176, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!259 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !260, line: 269)
!260 = !DISubprogram(name: "sqrt", linkageName: "_ZL4sqrtf", scope: !82, file: !82, line: 178, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!261 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !262, line: 270)
!262 = !DISubprogram(name: "tan", linkageName: "_ZL3tanf", scope: !82, file: !82, line: 180, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!263 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !264, line: 271)
!264 = !DISubprogram(name: "tanh", linkageName: "_ZL4tanhf", scope: !82, file: !82, line: 182, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!265 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !266, line: 272)
!266 = !DISubprogram(name: "tgamma", linkageName: "_ZL6tgammaf", scope: !82, file: !82, line: 184, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!267 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !268, line: 273)
!268 = !DISubprogram(name: "trunc", linkageName: "_ZL5truncf", scope: !82, file: !82, line: 186, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!269 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !270, line: 102)
!270 = !DISubprogram(name: "acos", scope: !271, file: !271, line: 54, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!271 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/mathcalls.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!272 = !DISubroutineType(types: !273)
!273 = !{!220, !220}
!274 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !275, line: 121)
!275 = !DISubprogram(name: "asin", scope: !271, file: !271, line: 56, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!276 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !277, line: 140)
!277 = !DISubprogram(name: "atan", scope: !271, file: !271, line: 58, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!278 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !279, line: 159)
!279 = !DISubprogram(name: "atan2", scope: !271, file: !271, line: 60, type: !280, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!280 = !DISubroutineType(types: !281)
!281 = !{!220, !220, !220}
!282 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !283, line: 180)
!283 = !DISubprogram(name: "ceil", scope: !271, file: !271, line: 178, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!284 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !285, line: 199)
!285 = !DISubprogram(name: "cos", scope: !271, file: !271, line: 63, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!286 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !287, line: 218)
!287 = !DISubprogram(name: "cosh", scope: !271, file: !271, line: 72, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!288 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !289, line: 237)
!289 = !DISubprogram(name: "exp", scope: !271, file: !271, line: 100, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!290 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !291, line: 256)
!291 = !DISubprogram(name: "fabs", scope: !271, file: !271, line: 181, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!292 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !293, line: 275)
!293 = !DISubprogram(name: "floor", scope: !271, file: !271, line: 184, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!294 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !295, line: 294)
!295 = !DISubprogram(name: "fmod", scope: !271, file: !271, line: 187, type: !280, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!296 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !297, line: 315)
!297 = !DISubprogram(name: "frexp", scope: !271, file: !271, line: 103, type: !298, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!298 = !DISubroutineType(types: !299)
!299 = !{!220, !220, !70}
!300 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !301, line: 334)
!301 = !DISubprogram(name: "ldexp", scope: !271, file: !271, line: 106, type: !302, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!302 = !DISubroutineType(types: !303)
!303 = !{!220, !220, !8}
!304 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !305, line: 353)
!305 = !DISubprogram(name: "log", scope: !271, file: !271, line: 109, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!306 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !307, line: 372)
!307 = !DISubprogram(name: "log10", scope: !271, file: !271, line: 112, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!308 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !309, line: 391)
!309 = !DISubprogram(name: "modf", scope: !271, file: !271, line: 115, type: !310, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!310 = !DISubroutineType(types: !311)
!311 = !{!220, !220, !312}
!312 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !220, size: 64)
!313 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !314, line: 403)
!314 = !DISubprogram(name: "pow", scope: !271, file: !271, line: 153, type: !280, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!315 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !316, line: 440)
!316 = !DISubprogram(name: "sin", scope: !271, file: !271, line: 65, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!317 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !318, line: 459)
!318 = !DISubprogram(name: "sinh", scope: !271, file: !271, line: 74, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!319 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !320, line: 478)
!320 = !DISubprogram(name: "sqrt", scope: !271, file: !271, line: 156, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!321 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !322, line: 497)
!322 = !DISubprogram(name: "tan", scope: !271, file: !271, line: 67, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!323 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !324, line: 516)
!324 = !DISubprogram(name: "tanh", scope: !271, file: !271, line: 76, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!325 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !326, line: 118)
!326 = !DIDerivedType(tag: DW_TAG_typedef, name: "div_t", file: !327, line: 101, baseType: !328)
!327 = !DIFile(filename: "/usr/include/stdlib.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!328 = !DICompositeType(tag: DW_TAG_structure_type, file: !327, line: 97, flags: DIFlagFwdDecl, identifier: "_ZTS5div_t")
!329 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !330, line: 119)
!330 = !DIDerivedType(tag: DW_TAG_typedef, name: "ldiv_t", file: !327, line: 109, baseType: !331)
!331 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !327, line: 105, size: 128, elements: !332, identifier: "_ZTS6ldiv_t")
!332 = !{!333, !334}
!333 = !DIDerivedType(tag: DW_TAG_member, name: "quot", scope: !331, file: !327, line: 107, baseType: !26, size: 64)
!334 = !DIDerivedType(tag: DW_TAG_member, name: "rem", scope: !331, file: !327, line: 108, baseType: !26, size: 64, offset: 64)
!335 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !336, line: 121)
!336 = !DISubprogram(name: "abort", scope: !327, file: !327, line: 515, type: !337, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!337 = !DISubroutineType(types: !338)
!338 = !{null}
!339 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !340, line: 122)
!340 = !DISubprogram(name: "abs", scope: !327, file: !327, line: 775, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!341 = !DISubroutineType(types: !342)
!342 = !{!8, !8}
!343 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !344, line: 123)
!344 = !DISubprogram(name: "atexit", scope: !327, file: !327, line: 519, type: !345, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!345 = !DISubroutineType(types: !346)
!346 = !{!8, !347}
!347 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !337, size: 64)
!348 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !349, line: 129)
!349 = !DISubprogram(name: "atof", scope: !350, file: !350, line: 26, type: !218, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!350 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/stdlib-float.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!351 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !352, line: 130)
!352 = !DISubprogram(name: "atoi", scope: !327, file: !327, line: 278, type: !353, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!353 = !DISubroutineType(types: !354)
!354 = !{!8, !221}
!355 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !356, line: 131)
!356 = !DISubprogram(name: "atol", scope: !327, file: !327, line: 283, type: !357, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!357 = !DISubroutineType(types: !358)
!358 = !{!26, !221}
!359 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !360, line: 132)
!360 = !DISubprogram(name: "bsearch", scope: !361, file: !361, line: 20, type: !362, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!361 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/stdlib-bsearch.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!362 = !DISubroutineType(types: !363)
!363 = !{!364, !365, !365, !367, !367, !370}
!364 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 64)
!365 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !366, size: 64)
!366 = !DIDerivedType(tag: DW_TAG_const_type, baseType: null)
!367 = !DIDerivedType(tag: DW_TAG_typedef, name: "size_t", file: !368, line: 62, baseType: !369)
!368 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/stddef.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!369 = !DIBasicType(name: "long unsigned int", size: 64, encoding: DW_ATE_unsigned)
!370 = !DIDerivedType(tag: DW_TAG_typedef, name: "__compar_fn_t", file: !327, line: 742, baseType: !371)
!371 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !372, size: 64)
!372 = !DISubroutineType(types: !373)
!373 = !{!8, !365, !365}
!374 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !375, line: 133)
!375 = !DISubprogram(name: "calloc", scope: !327, file: !327, line: 468, type: !376, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!376 = !DISubroutineType(types: !377)
!377 = !{!364, !367, !367}
!378 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !379, line: 134)
!379 = !DISubprogram(name: "div", scope: !327, file: !327, line: 789, type: !380, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!380 = !DISubroutineType(types: !381)
!381 = !{!326, !8, !8}
!382 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !383, line: 135)
!383 = !DISubprogram(name: "exit", scope: !327, file: !327, line: 543, type: !384, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!384 = !DISubroutineType(types: !385)
!385 = !{null, !8}
!386 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !387, line: 136)
!387 = !DISubprogram(name: "free", scope: !327, file: !327, line: 483, type: !388, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!388 = !DISubroutineType(types: !389)
!389 = !{null, !364}
!390 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !391, line: 137)
!391 = !DISubprogram(name: "getenv", scope: !327, file: !327, line: 564, type: !392, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!392 = !DISubroutineType(types: !393)
!393 = !{!6, !221}
!394 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !395, line: 138)
!395 = !DISubprogram(name: "labs", scope: !327, file: !327, line: 776, type: !179, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!396 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !397, line: 139)
!397 = !DISubprogram(name: "ldiv", scope: !327, file: !327, line: 791, type: !398, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!398 = !DISubroutineType(types: !399)
!399 = !{!330, !26, !26}
!400 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !401, line: 140)
!401 = !DISubprogram(name: "malloc", scope: !327, file: !327, line: 466, type: !402, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!402 = !DISubroutineType(types: !403)
!403 = !{!364, !367}
!404 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !405, line: 142)
!405 = !DISubprogram(name: "mblen", scope: !327, file: !327, line: 863, type: !406, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!406 = !DISubroutineType(types: !407)
!407 = !{!8, !221, !367}
!408 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !409, line: 143)
!409 = !DISubprogram(name: "mbstowcs", scope: !327, file: !327, line: 874, type: !410, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!410 = !DISubroutineType(types: !411)
!411 = !{!367, !412, !415, !367}
!412 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !413)
!413 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !414, size: 64)
!414 = !DIBasicType(name: "wchar_t", size: 32, encoding: DW_ATE_signed)
!415 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !221)
!416 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !417, line: 144)
!417 = !DISubprogram(name: "mbtowc", scope: !327, file: !327, line: 866, type: !418, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!418 = !DISubroutineType(types: !419)
!419 = !{!8, !412, !415, !367}
!420 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !421, line: 146)
!421 = !DISubprogram(name: "qsort", scope: !327, file: !327, line: 765, type: !422, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!422 = !DISubroutineType(types: !423)
!423 = !{null, !364, !367, !367, !370}
!424 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !425, line: 152)
!425 = !DISubprogram(name: "rand", scope: !327, file: !327, line: 374, type: !426, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!426 = !DISubroutineType(types: !427)
!427 = !{!8}
!428 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !429, line: 153)
!429 = !DISubprogram(name: "realloc", scope: !327, file: !327, line: 480, type: !430, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!430 = !DISubroutineType(types: !431)
!431 = !{!364, !364, !367}
!432 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !433, line: 154)
!433 = !DISubprogram(name: "srand", scope: !327, file: !327, line: 376, type: !434, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!434 = !DISubroutineType(types: !435)
!435 = !{null, !436}
!436 = !DIBasicType(name: "unsigned int", size: 32, encoding: DW_ATE_unsigned)
!437 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !438, line: 155)
!438 = !DISubprogram(name: "strtod", scope: !327, file: !327, line: 164, type: !439, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!439 = !DISubroutineType(types: !440)
!440 = !{!220, !415, !441}
!441 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !442)
!442 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !6, size: 64)
!443 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !444, line: 156)
!444 = !DISubprogram(name: "strtol", scope: !327, file: !327, line: 183, type: !445, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!445 = !DISubroutineType(types: !446)
!446 = !{!26, !415, !441, !8}
!447 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !448, line: 157)
!448 = !DISubprogram(name: "strtoul", scope: !327, file: !327, line: 187, type: !449, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!449 = !DISubroutineType(types: !450)
!450 = !{!369, !415, !441, !8}
!451 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !452, line: 158)
!452 = !DISubprogram(name: "system", scope: !327, file: !327, line: 717, type: !353, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!453 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !454, line: 160)
!454 = !DISubprogram(name: "wcstombs", scope: !327, file: !327, line: 877, type: !455, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!455 = !DISubroutineType(types: !456)
!456 = !{!367, !457, !458, !367}
!457 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !6)
!458 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !459)
!459 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !460, size: 64)
!460 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !414)
!461 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !462, line: 161)
!462 = !DISubprogram(name: "wctomb", scope: !327, file: !327, line: 870, type: !463, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!463 = !DISubroutineType(types: !464)
!464 = !{!8, !6, !414}
!465 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !468, line: 201)
!466 = !DINamespace(name: "__gnu_cxx", scope: null, file: !467, line: 68)
!467 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/bits/cpp_type_traits.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!468 = !DIDerivedType(tag: DW_TAG_typedef, name: "lldiv_t", file: !327, line: 121, baseType: !469)
!469 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !327, line: 117, size: 128, elements: !470, identifier: "_ZTS7lldiv_t")
!470 = !{!471, !472}
!471 = !DIDerivedType(tag: DW_TAG_member, name: "quot", scope: !469, file: !327, line: 119, baseType: !29, size: 64)
!472 = !DIDerivedType(tag: DW_TAG_member, name: "rem", scope: !469, file: !327, line: 120, baseType: !29, size: 64, offset: 64)
!473 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !474, line: 207)
!474 = !DISubprogram(name: "_Exit", scope: !327, file: !327, line: 557, type: !384, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!475 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !476, line: 211)
!476 = !DISubprogram(name: "llabs", scope: !327, file: !327, line: 780, type: !84, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!477 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !478, line: 217)
!478 = !DISubprogram(name: "lldiv", scope: !327, file: !327, line: 797, type: !479, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!479 = !DISubroutineType(types: !480)
!480 = !{!468, !29, !29}
!481 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !482, line: 228)
!482 = !DISubprogram(name: "atoll", scope: !327, file: !327, line: 292, type: !483, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!483 = !DISubroutineType(types: !484)
!484 = !{!29, !221}
!485 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !486, line: 229)
!486 = !DISubprogram(name: "strtoll", scope: !327, file: !327, line: 209, type: !487, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!487 = !DISubroutineType(types: !488)
!488 = !{!29, !415, !441, !8}
!489 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !490, line: 230)
!490 = !DISubprogram(name: "strtoull", scope: !327, file: !327, line: 214, type: !491, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!491 = !DISubroutineType(types: !492)
!492 = !{!20, !415, !441, !8}
!493 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !494, line: 232)
!494 = !DISubprogram(name: "strtof", scope: !327, file: !327, line: 172, type: !495, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!495 = !DISubroutineType(types: !496)
!496 = !{!90, !415, !441}
!497 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !498, line: 233)
!498 = !DISubprogram(name: "strtold", scope: !327, file: !327, line: 175, type: !499, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!499 = !DISubroutineType(types: !500)
!500 = !{!501, !415, !441}
!501 = !DIBasicType(name: "long double", size: 64, encoding: DW_ATE_float)
!502 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !468, line: 241)
!503 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !474, line: 243)
!504 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !476, line: 245)
!505 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !506, line: 246)
!506 = !DISubprogram(name: "div", linkageName: "_ZN9__gnu_cxx3divExx", scope: !466, file: !507, line: 214, type: !479, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!507 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/cstdlib", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!508 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !478, line: 247)
!509 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !482, line: 249)
!510 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !494, line: 250)
!511 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !486, line: 251)
!512 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !490, line: 252)
!513 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !498, line: 253)
!514 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !515, line: 418)
!515 = !DISubprogram(name: "acosf", linkageName: "_ZL5acosff", scope: !516, file: !516, line: 1126, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!516 = !DIFile(filename: "/usr/local/cuda/include/math_functions.hpp", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!517 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !518, line: 419)
!518 = !DISubprogram(name: "acoshf", linkageName: "_ZL6acoshff", scope: !516, file: !516, line: 1154, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!519 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !520, line: 420)
!520 = !DISubprogram(name: "asinf", linkageName: "_ZL5asinff", scope: !516, file: !516, line: 1121, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!521 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !522, line: 421)
!522 = !DISubprogram(name: "asinhf", linkageName: "_ZL6asinhff", scope: !516, file: !516, line: 1159, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!523 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !524, line: 422)
!524 = !DISubprogram(name: "atan2f", linkageName: "_ZL6atan2fff", scope: !516, file: !516, line: 1111, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!525 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !526, line: 423)
!526 = !DISubprogram(name: "atanf", linkageName: "_ZL5atanff", scope: !516, file: !516, line: 1116, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!527 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !528, line: 424)
!528 = !DISubprogram(name: "atanhf", linkageName: "_ZL6atanhff", scope: !516, file: !516, line: 1164, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!529 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !530, line: 425)
!530 = !DISubprogram(name: "cbrtf", linkageName: "_ZL5cbrtff", scope: !516, file: !516, line: 1199, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!531 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !532, line: 426)
!532 = !DISubprogram(name: "ceilf", linkageName: "_ZL5ceilff", scope: !533, file: !533, line: 647, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!533 = !DIFile(filename: "/usr/local/cuda/include/device_functions.hpp", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!534 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !535, line: 427)
!535 = !DISubprogram(name: "copysignf", linkageName: "_ZL9copysignfff", scope: !516, file: !516, line: 973, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!536 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !537, line: 428)
!537 = !DISubprogram(name: "cosf", linkageName: "_ZL4cosff", scope: !516, file: !516, line: 1027, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!538 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !539, line: 429)
!539 = !DISubprogram(name: "coshf", linkageName: "_ZL5coshff", scope: !516, file: !516, line: 1096, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!540 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !541, line: 430)
!541 = !DISubprogram(name: "erfcf", linkageName: "_ZL5erfcff", scope: !516, file: !516, line: 1259, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!542 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !543, line: 431)
!543 = !DISubprogram(name: "erff", linkageName: "_ZL4erfff", scope: !516, file: !516, line: 1249, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!544 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !545, line: 432)
!545 = !DISubprogram(name: "exp2f", linkageName: "_ZL5exp2ff", scope: !533, file: !533, line: 637, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!546 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !547, line: 433)
!547 = !DISubprogram(name: "expf", linkageName: "_ZL4expff", scope: !516, file: !516, line: 1078, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!548 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !549, line: 434)
!549 = !DISubprogram(name: "expm1f", linkageName: "_ZL6expm1ff", scope: !516, file: !516, line: 1169, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!550 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !551, line: 435)
!551 = !DISubprogram(name: "fabsf", linkageName: "_ZL5fabsff", scope: !533, file: !533, line: 582, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!552 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !553, line: 436)
!553 = !DISubprogram(name: "fdimf", linkageName: "_ZL5fdimfff", scope: !516, file: !516, line: 1385, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!554 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !555, line: 437)
!555 = !DISubprogram(name: "floorf", linkageName: "_ZL6floorff", scope: !533, file: !533, line: 572, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!556 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !557, line: 438)
!557 = !DISubprogram(name: "fmaf", linkageName: "_ZL4fmaffff", scope: !516, file: !516, line: 1337, type: !133, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!558 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !559, line: 439)
!559 = !DISubprogram(name: "fmaxf", linkageName: "_ZL5fmaxfff", scope: !533, file: !533, line: 602, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!560 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !561, line: 440)
!561 = !DISubprogram(name: "fminf", linkageName: "_ZL5fminfff", scope: !533, file: !533, line: 597, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!562 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !563, line: 441)
!563 = !DISubprogram(name: "fmodf", linkageName: "_ZL5fmodfff", scope: !516, file: !516, line: 1322, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!564 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !565, line: 442)
!565 = !DISubprogram(name: "frexpf", linkageName: "_ZL6frexpffPi", scope: !516, file: !516, line: 1312, type: !147, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!566 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !567, line: 443)
!567 = !DISubprogram(name: "hypotf", linkageName: "_ZL6hypotfff", scope: !516, file: !516, line: 1174, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!568 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !569, line: 444)
!569 = !DISubprogram(name: "ilogbf", linkageName: "_ZL6ilogbff", scope: !516, file: !516, line: 1390, type: !143, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!570 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !571, line: 445)
!571 = !DISubprogram(name: "ldexpf", linkageName: "_ZL6ldexpffi", scope: !516, file: !516, line: 1289, type: !183, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!572 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !573, line: 446)
!573 = !DISubprogram(name: "lgammaf", linkageName: "_ZL7lgammaff", scope: !516, file: !516, line: 1284, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!574 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !575, line: 447)
!575 = !DISubprogram(name: "llrintf", linkageName: "_ZL7llrintff", scope: !516, file: !516, line: 933, type: !191, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!576 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !577, line: 448)
!577 = !DISubprogram(name: "llroundf", linkageName: "_ZL8llroundff", scope: !516, file: !516, line: 1371, type: !191, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!578 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !579, line: 449)
!579 = !DISubprogram(name: "log10f", linkageName: "_ZL6log10ff", scope: !516, file: !516, line: 1140, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!580 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !581, line: 450)
!581 = !DISubprogram(name: "log1pf", linkageName: "_ZL6log1pff", scope: !516, file: !516, line: 1149, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!582 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !583, line: 451)
!583 = !DISubprogram(name: "log2f", linkageName: "_ZL5log2ff", scope: !516, file: !516, line: 1069, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!584 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !585, line: 452)
!585 = !DISubprogram(name: "logbf", linkageName: "_ZL5logbff", scope: !516, file: !516, line: 1395, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!586 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !587, line: 453)
!587 = !DISubprogram(name: "logf", linkageName: "_ZL4logff", scope: !516, file: !516, line: 1131, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!588 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !589, line: 454)
!589 = !DISubprogram(name: "lrintf", linkageName: "_ZL6lrintff", scope: !516, file: !516, line: 924, type: !205, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!590 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !591, line: 455)
!591 = !DISubprogram(name: "lroundf", linkageName: "_ZL7lroundff", scope: !516, file: !516, line: 1376, type: !205, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!592 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !593, line: 456)
!593 = !DISubprogram(name: "modff", linkageName: "_ZL5modfffPf", scope: !516, file: !516, line: 1317, type: !213, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!594 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !595, line: 457)
!595 = !DISubprogram(name: "nearbyintf", linkageName: "_ZL10nearbyintff", scope: !516, file: !516, line: 938, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!596 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !597, line: 458)
!597 = !DISubprogram(name: "nextafterf", linkageName: "_ZL10nextafterfff", scope: !516, file: !516, line: 1002, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!598 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !599, line: 459)
!599 = !DISubprogram(name: "nexttowardf", scope: !271, file: !271, line: 284, type: !600, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!600 = !DISubroutineType(types: !601)
!601 = !{!90, !90, !501}
!602 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !599, line: 460)
!603 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !604, line: 461)
!604 = !DISubprogram(name: "powf", linkageName: "_ZL4powfff", scope: !516, file: !516, line: 1352, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!605 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !606, line: 462)
!606 = !DISubprogram(name: "remainderf", linkageName: "_ZL10remainderfff", scope: !516, file: !516, line: 1327, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!607 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !608, line: 463)
!608 = !DISubprogram(name: "remquof", linkageName: "_ZL7remquofffPi", scope: !516, file: !516, line: 1332, type: !241, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!609 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !610, line: 464)
!610 = !DISubprogram(name: "rintf", linkageName: "_ZL5rintff", scope: !516, file: !516, line: 919, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!611 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !612, line: 465)
!612 = !DISubprogram(name: "roundf", linkageName: "_ZL6roundff", scope: !516, file: !516, line: 1366, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!613 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !614, line: 466)
!614 = !DISubprogram(name: "scalblnf", linkageName: "_ZL8scalblnffl", scope: !516, file: !516, line: 1299, type: !249, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!615 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !616, line: 467)
!616 = !DISubprogram(name: "scalbnf", linkageName: "_ZL7scalbnffi", scope: !516, file: !516, line: 1294, type: !183, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!617 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !618, line: 468)
!618 = !DISubprogram(name: "sinf", linkageName: "_ZL4sinff", scope: !516, file: !516, line: 1018, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!619 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !620, line: 469)
!620 = !DISubprogram(name: "sinhf", linkageName: "_ZL5sinhff", scope: !516, file: !516, line: 1101, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!621 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !622, line: 470)
!622 = !DISubprogram(name: "sqrtf", linkageName: "_ZL5sqrtff", scope: !533, file: !533, line: 887, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!623 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !624, line: 471)
!624 = !DISubprogram(name: "tanf", linkageName: "_ZL4tanff", scope: !516, file: !516, line: 1060, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!625 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !626, line: 472)
!626 = !DISubprogram(name: "tanhf", linkageName: "_ZL5tanhff", scope: !516, file: !516, line: 1106, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!627 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !628, line: 473)
!628 = !DISubprogram(name: "tgammaf", linkageName: "_ZL7tgammaff", scope: !516, file: !516, line: 1361, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!629 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !630, line: 474)
!630 = !DISubprogram(name: "truncf", linkageName: "_ZL6truncff", scope: !533, file: !533, line: 642, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!631 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !632, line: 64)
!632 = !DIDerivedType(tag: DW_TAG_typedef, name: "mbstate_t", file: !633, line: 106, baseType: !634)
!633 = !DIFile(filename: "/usr/include/wchar.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!634 = !DIDerivedType(tag: DW_TAG_typedef, name: "__mbstate_t", file: !633, line: 94, baseType: !635)
!635 = !DICompositeType(tag: DW_TAG_structure_type, file: !633, line: 82, flags: DIFlagFwdDecl, identifier: "_ZTS11__mbstate_t")
!636 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !637, line: 139)
!637 = !DIDerivedType(tag: DW_TAG_typedef, name: "wint_t", file: !368, line: 132, baseType: !436)
!638 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !639, line: 141)
!639 = !DISubprogram(name: "btowc", scope: !633, file: !633, line: 388, type: !640, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!640 = !DISubroutineType(types: !641)
!641 = !{!637, !8}
!642 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !643, line: 142)
!643 = !DISubprogram(name: "fgetwc", scope: !633, file: !633, line: 745, type: !644, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!644 = !DISubroutineType(types: !645)
!645 = !{!637, !646}
!646 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !647, size: 64)
!647 = !DIDerivedType(tag: DW_TAG_typedef, name: "__FILE", file: !648, line: 64, baseType: !649)
!648 = !DIFile(filename: "/usr/include/stdio.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!649 = !DICompositeType(tag: DW_TAG_structure_type, name: "_IO_FILE", file: !648, line: 44, flags: DIFlagFwdDecl, identifier: "_ZTS8_IO_FILE")
!650 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !651, line: 143)
!651 = !DISubprogram(name: "fgetws", scope: !633, file: !633, line: 774, type: !652, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!652 = !DISubroutineType(types: !653)
!653 = !{!413, !412, !8, !654}
!654 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !646)
!655 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !656, line: 144)
!656 = !DISubprogram(name: "fputwc", scope: !633, file: !633, line: 759, type: !657, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!657 = !DISubroutineType(types: !658)
!658 = !{!637, !414, !646}
!659 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !660, line: 145)
!660 = !DISubprogram(name: "fputws", scope: !633, file: !633, line: 781, type: !661, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!661 = !DISubroutineType(types: !662)
!662 = !{!8, !458, !654}
!663 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !664, line: 146)
!664 = !DISubprogram(name: "fwide", scope: !633, file: !633, line: 587, type: !665, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!665 = !DISubroutineType(types: !666)
!666 = !{!8, !646, !8}
!667 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !668, line: 147)
!668 = !DISubprogram(name: "fwprintf", scope: !633, file: !633, line: 594, type: !669, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!669 = !DISubroutineType(types: !670)
!670 = !{!8, !654, !458, null}
!671 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !672, line: 148)
!672 = !DISubprogram(name: "fwscanf", scope: !633, file: !633, line: 635, type: !669, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!673 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !674, line: 149)
!674 = !DISubprogram(name: "getwc", scope: !633, file: !633, line: 746, type: !644, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!675 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !676, line: 150)
!676 = !DISubprogram(name: "getwchar", scope: !633, file: !633, line: 752, type: !677, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!677 = !DISubroutineType(types: !678)
!678 = !{!637}
!679 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !680, line: 151)
!680 = !DISubprogram(name: "mbrlen", scope: !633, file: !633, line: 399, type: !681, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!681 = !DISubroutineType(types: !682)
!682 = !{!367, !415, !367, !683}
!683 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !684)
!684 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !632, size: 64)
!685 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !686, line: 152)
!686 = !DISubprogram(name: "mbrtowc", scope: !633, file: !633, line: 365, type: !687, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!687 = !DISubroutineType(types: !688)
!688 = !{!367, !412, !415, !367, !683}
!689 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !690, line: 153)
!690 = !DISubprogram(name: "mbsinit", scope: !633, file: !633, line: 361, type: !691, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!691 = !DISubroutineType(types: !692)
!692 = !{!8, !693}
!693 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !694, size: 64)
!694 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !632)
!695 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !696, line: 154)
!696 = !DISubprogram(name: "mbsrtowcs", scope: !633, file: !633, line: 408, type: !697, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!697 = !DISubroutineType(types: !698)
!698 = !{!367, !412, !699, !367, !683}
!699 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !700)
!700 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !221, size: 64)
!701 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !702, line: 155)
!702 = !DISubprogram(name: "putwc", scope: !633, file: !633, line: 760, type: !657, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!703 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !704, line: 156)
!704 = !DISubprogram(name: "putwchar", scope: !633, file: !633, line: 766, type: !705, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!705 = !DISubroutineType(types: !706)
!706 = !{!637, !414}
!707 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !708, line: 158)
!708 = !DISubprogram(name: "swprintf", scope: !633, file: !633, line: 604, type: !709, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!709 = !DISubroutineType(types: !710)
!710 = !{!8, !412, !367, !458, null}
!711 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !712, line: 160)
!712 = !DISubprogram(name: "swscanf", scope: !633, file: !633, line: 645, type: !713, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!713 = !DISubroutineType(types: !714)
!714 = !{!8, !458, !458, null}
!715 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !716, line: 161)
!716 = !DISubprogram(name: "ungetwc", scope: !633, file: !633, line: 789, type: !717, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!717 = !DISubroutineType(types: !718)
!718 = !{!637, !637, !646}
!719 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !720, line: 162)
!720 = !DISubprogram(name: "vfwprintf", scope: !633, file: !633, line: 612, type: !721, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!721 = !DISubroutineType(types: !722)
!722 = !{!8, !654, !458, !723}
!723 = !DIDerivedType(tag: DW_TAG_typedef, name: "__gnuc_va_list", file: !724, line: 48, baseType: !725)
!724 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/stdarg.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!725 = !DIDerivedType(tag: DW_TAG_typedef, name: "__builtin_va_list", file: !3, baseType: !6)
!726 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !727, line: 164)
!727 = !DISubprogram(name: "vfwscanf", scope: !633, file: !633, line: 689, type: !721, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!728 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !729, line: 167)
!729 = !DISubprogram(name: "vswprintf", scope: !633, file: !633, line: 625, type: !730, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!730 = !DISubroutineType(types: !731)
!731 = !{!8, !412, !367, !458, !723}
!732 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !733, line: 170)
!733 = !DISubprogram(name: "vswscanf", scope: !633, file: !633, line: 701, type: !734, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!734 = !DISubroutineType(types: !735)
!735 = !{!8, !458, !458, !723}
!736 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !737, line: 172)
!737 = !DISubprogram(name: "vwprintf", scope: !633, file: !633, line: 620, type: !738, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!738 = !DISubroutineType(types: !739)
!739 = !{!8, !458, !723}
!740 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !741, line: 174)
!741 = !DISubprogram(name: "vwscanf", scope: !633, file: !633, line: 697, type: !738, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!742 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !743, line: 176)
!743 = !DISubprogram(name: "wcrtomb", scope: !633, file: !633, line: 370, type: !744, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!744 = !DISubroutineType(types: !745)
!745 = !{!367, !457, !414, !683}
!746 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !747, line: 177)
!747 = !DISubprogram(name: "wcscat", scope: !633, file: !633, line: 155, type: !748, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!748 = !DISubroutineType(types: !749)
!749 = !{!413, !412, !458}
!750 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !751, line: 178)
!751 = !DISubprogram(name: "wcscmp", scope: !633, file: !633, line: 163, type: !752, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!752 = !DISubroutineType(types: !753)
!753 = !{!8, !459, !459}
!754 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !755, line: 179)
!755 = !DISubprogram(name: "wcscoll", scope: !633, file: !633, line: 192, type: !752, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!756 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !757, line: 180)
!757 = !DISubprogram(name: "wcscpy", scope: !633, file: !633, line: 147, type: !748, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!758 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !759, line: 181)
!759 = !DISubprogram(name: "wcscspn", scope: !633, file: !633, line: 252, type: !760, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!760 = !DISubroutineType(types: !761)
!761 = !{!367, !459, !459}
!762 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !763, line: 182)
!763 = !DISubprogram(name: "wcsftime", scope: !633, file: !633, line: 855, type: !764, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!764 = !DISubroutineType(types: !765)
!765 = !{!367, !412, !367, !458, !766}
!766 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !767)
!767 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !768, size: 64)
!768 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !769)
!769 = !DICompositeType(tag: DW_TAG_structure_type, name: "tm", file: !633, line: 137, flags: DIFlagFwdDecl, identifier: "_ZTS2tm")
!770 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !771, line: 183)
!771 = !DISubprogram(name: "wcslen", scope: !633, file: !633, line: 287, type: !772, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!772 = !DISubroutineType(types: !773)
!773 = !{!367, !459}
!774 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !775, line: 184)
!775 = !DISubprogram(name: "wcsncat", scope: !633, file: !633, line: 158, type: !776, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!776 = !DISubroutineType(types: !777)
!777 = !{!413, !412, !458, !367}
!778 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !779, line: 185)
!779 = !DISubprogram(name: "wcsncmp", scope: !633, file: !633, line: 166, type: !780, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!780 = !DISubroutineType(types: !781)
!781 = !{!8, !459, !459, !367}
!782 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !783, line: 186)
!783 = !DISubprogram(name: "wcsncpy", scope: !633, file: !633, line: 150, type: !776, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!784 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !785, line: 187)
!785 = !DISubprogram(name: "wcsrtombs", scope: !633, file: !633, line: 414, type: !786, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!786 = !DISubroutineType(types: !787)
!787 = !{!367, !457, !788, !367, !683}
!788 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !789)
!789 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !459, size: 64)
!790 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !791, line: 188)
!791 = !DISubprogram(name: "wcsspn", scope: !633, file: !633, line: 256, type: !760, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!792 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !793, line: 189)
!793 = !DISubprogram(name: "wcstod", scope: !633, file: !633, line: 450, type: !794, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!794 = !DISubroutineType(types: !795)
!795 = !{!220, !458, !796}
!796 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !797)
!797 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !413, size: 64)
!798 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !799, line: 191)
!799 = !DISubprogram(name: "wcstof", scope: !633, file: !633, line: 457, type: !800, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!800 = !DISubroutineType(types: !801)
!801 = !{!90, !458, !796}
!802 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !803, line: 193)
!803 = !DISubprogram(name: "wcstok", scope: !633, file: !633, line: 282, type: !804, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!804 = !DISubroutineType(types: !805)
!805 = !{!413, !412, !458, !796}
!806 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !807, line: 194)
!807 = !DISubprogram(name: "wcstol", scope: !633, file: !633, line: 468, type: !808, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!808 = !DISubroutineType(types: !809)
!809 = !{!26, !458, !796, !8}
!810 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !811, line: 195)
!811 = !DISubprogram(name: "wcstoul", scope: !633, file: !633, line: 473, type: !812, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!812 = !DISubroutineType(types: !813)
!813 = !{!369, !458, !796, !8}
!814 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !815, line: 196)
!815 = !DISubprogram(name: "wcsxfrm", scope: !633, file: !633, line: 196, type: !816, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!816 = !DISubroutineType(types: !817)
!817 = !{!367, !412, !458, !367}
!818 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !819, line: 197)
!819 = !DISubprogram(name: "wctob", scope: !633, file: !633, line: 394, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!820 = !DISubroutineType(types: !821)
!821 = !{!8, !637}
!822 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !823, line: 198)
!823 = !DISubprogram(name: "wmemcmp", scope: !633, file: !633, line: 325, type: !780, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!824 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !825, line: 199)
!825 = !DISubprogram(name: "wmemcpy", scope: !633, file: !633, line: 329, type: !776, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!826 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !827, line: 200)
!827 = !DISubprogram(name: "wmemmove", scope: !633, file: !633, line: 334, type: !828, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!828 = !DISubroutineType(types: !829)
!829 = !{!413, !413, !459, !367}
!830 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !831, line: 201)
!831 = !DISubprogram(name: "wmemset", scope: !633, file: !633, line: 338, type: !832, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!832 = !DISubroutineType(types: !833)
!833 = !{!413, !413, !414, !367}
!834 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !835, line: 202)
!835 = !DISubprogram(name: "wprintf", scope: !633, file: !633, line: 601, type: !836, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!836 = !DISubroutineType(types: !837)
!837 = !{!8, !458, null}
!838 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !839, line: 203)
!839 = !DISubprogram(name: "wscanf", scope: !633, file: !633, line: 642, type: !836, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!840 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !841, line: 204)
!841 = !DISubprogram(name: "wcschr", scope: !633, file: !633, line: 227, type: !842, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!842 = !DISubroutineType(types: !843)
!843 = !{!413, !459, !414}
!844 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !845, line: 205)
!845 = !DISubprogram(name: "wcspbrk", scope: !633, file: !633, line: 266, type: !846, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!846 = !DISubroutineType(types: !847)
!847 = !{!413, !459, !459}
!848 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !849, line: 206)
!849 = !DISubprogram(name: "wcsrchr", scope: !633, file: !633, line: 237, type: !842, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!850 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !851, line: 207)
!851 = !DISubprogram(name: "wcsstr", scope: !633, file: !633, line: 277, type: !846, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!852 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !853, line: 208)
!853 = !DISubprogram(name: "wmemchr", scope: !633, file: !633, line: 320, type: !854, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!854 = !DISubroutineType(types: !855)
!855 = !{!413, !459, !414, !367}
!856 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !857, line: 248)
!857 = !DISubprogram(name: "wcstold", scope: !633, file: !633, line: 459, type: !858, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!858 = !DISubroutineType(types: !859)
!859 = !{!501, !458, !796}
!860 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !861, line: 257)
!861 = !DISubprogram(name: "wcstoll", scope: !633, file: !633, line: 483, type: !862, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!862 = !DISubroutineType(types: !863)
!863 = !{!29, !458, !796, !8}
!864 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !865, line: 258)
!865 = !DISubprogram(name: "wcstoull", scope: !633, file: !633, line: 490, type: !866, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!866 = !DISubroutineType(types: !867)
!867 = !{!20, !458, !796, !8}
!868 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !857, line: 264)
!869 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !861, line: 265)
!870 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !865, line: 266)
!871 = !DIImportedEntity(tag: DW_TAG_imported_module, scope: !872, entity: !874, line: 56)
!872 = !DINamespace(name: "__gnu_debug", scope: null, file: !873, line: 54)
!873 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/debug/debug.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!874 = !DINamespace(name: "__debug", scope: !81, file: !873, line: 48)
!875 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !876, line: 53)
!876 = !DICompositeType(tag: DW_TAG_structure_type, name: "lconv", file: !877, line: 53, flags: DIFlagFwdDecl, identifier: "_ZTS5lconv")
!877 = !DIFile(filename: "/usr/include/locale.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!878 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !879, line: 54)
!879 = !DISubprogram(name: "setlocale", scope: !877, file: !877, line: 124, type: !880, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!880 = !DISubroutineType(types: !881)
!881 = !{!6, !8, !221}
!882 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !883, line: 55)
!883 = !DISubprogram(name: "localeconv", scope: !877, file: !877, line: 127, type: !884, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!884 = !DISubroutineType(types: !885)
!885 = !{!886}
!886 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !876, size: 64)
!887 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !888, line: 64)
!888 = !DISubprogram(name: "isalnum", scope: !889, file: !889, line: 110, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!889 = !DIFile(filename: "/usr/include/ctype.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!890 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !891, line: 65)
!891 = !DISubprogram(name: "isalpha", scope: !889, file: !889, line: 111, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!892 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !893, line: 66)
!893 = !DISubprogram(name: "iscntrl", scope: !889, file: !889, line: 112, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!894 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !895, line: 67)
!895 = !DISubprogram(name: "isdigit", scope: !889, file: !889, line: 113, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!896 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !897, line: 68)
!897 = !DISubprogram(name: "isgraph", scope: !889, file: !889, line: 115, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!898 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !899, line: 69)
!899 = !DISubprogram(name: "islower", scope: !889, file: !889, line: 114, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!900 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !901, line: 70)
!901 = !DISubprogram(name: "isprint", scope: !889, file: !889, line: 116, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!902 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !903, line: 71)
!903 = !DISubprogram(name: "ispunct", scope: !889, file: !889, line: 117, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!904 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !905, line: 72)
!905 = !DISubprogram(name: "isspace", scope: !889, file: !889, line: 118, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!906 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !907, line: 73)
!907 = !DISubprogram(name: "isupper", scope: !889, file: !889, line: 119, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!908 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !909, line: 74)
!909 = !DISubprogram(name: "isxdigit", scope: !889, file: !889, line: 120, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!910 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !911, line: 75)
!911 = !DISubprogram(name: "tolower", scope: !889, file: !889, line: 124, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!912 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !913, line: 76)
!913 = !DISubprogram(name: "toupper", scope: !889, file: !889, line: 127, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!914 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !915, line: 44)
!915 = !DIDerivedType(tag: DW_TAG_typedef, name: "size_t", scope: !81, file: !916, line: 186, baseType: !369)
!916 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/x86_64-linux-gnu/c++/4.8/bits/c++config.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!917 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !918, line: 45)
!918 = !DIDerivedType(tag: DW_TAG_typedef, name: "ptrdiff_t", scope: !81, file: !916, line: 187, baseType: !26)
!919 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !920, line: 82)
!920 = !DIDerivedType(tag: DW_TAG_typedef, name: "wctrans_t", file: !921, line: 186, baseType: !922)
!921 = !DIFile(filename: "/usr/include/wctype.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!922 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !923, size: 64)
!923 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !924)
!924 = !DIDerivedType(tag: DW_TAG_typedef, name: "__int32_t", file: !925, line: 40, baseType: !8)
!925 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/types.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!926 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !927, line: 83)
!927 = !DIDerivedType(tag: DW_TAG_typedef, name: "wctype_t", file: !921, line: 52, baseType: !369)
!928 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !637, line: 84)
!929 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !930, line: 86)
!930 = !DISubprogram(name: "iswalnum", scope: !921, file: !921, line: 111, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!931 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !932, line: 87)
!932 = !DISubprogram(name: "iswalpha", scope: !921, file: !921, line: 117, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!933 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !934, line: 89)
!934 = !DISubprogram(name: "iswblank", scope: !921, file: !921, line: 162, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!935 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !936, line: 91)
!936 = !DISubprogram(name: "iswcntrl", scope: !921, file: !921, line: 120, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!937 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !938, line: 92)
!938 = !DISubprogram(name: "iswctype", scope: !921, file: !921, line: 175, type: !939, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!939 = !DISubroutineType(types: !940)
!940 = !{!8, !637, !927}
!941 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !942, line: 93)
!942 = !DISubprogram(name: "iswdigit", scope: !921, file: !921, line: 124, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!943 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !944, line: 94)
!944 = !DISubprogram(name: "iswgraph", scope: !921, file: !921, line: 128, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!945 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !946, line: 95)
!946 = !DISubprogram(name: "iswlower", scope: !921, file: !921, line: 133, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!947 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !948, line: 96)
!948 = !DISubprogram(name: "iswprint", scope: !921, file: !921, line: 136, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!949 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !950, line: 97)
!950 = !DISubprogram(name: "iswpunct", scope: !921, file: !921, line: 141, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!951 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !952, line: 98)
!952 = !DISubprogram(name: "iswspace", scope: !921, file: !921, line: 146, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!953 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !954, line: 99)
!954 = !DISubprogram(name: "iswupper", scope: !921, file: !921, line: 151, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!955 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !956, line: 100)
!956 = !DISubprogram(name: "iswxdigit", scope: !921, file: !921, line: 156, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!957 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !958, line: 101)
!958 = !DISubprogram(name: "towctrans", scope: !921, file: !921, line: 221, type: !959, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!959 = !DISubroutineType(types: !960)
!960 = !{!637, !637, !920}
!961 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !962, line: 102)
!962 = !DISubprogram(name: "towlower", scope: !921, file: !921, line: 194, type: !963, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!963 = !DISubroutineType(types: !964)
!964 = !{!637, !637}
!965 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !966, line: 103)
!966 = !DISubprogram(name: "towupper", scope: !921, file: !921, line: 197, type: !963, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!967 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !968, line: 104)
!968 = !DISubprogram(name: "wctrans", scope: !921, file: !921, line: 218, type: !969, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!969 = !DISubroutineType(types: !970)
!970 = !{!920, !221}
!971 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !972, line: 105)
!972 = !DISubprogram(name: "wctype", scope: !921, file: !921, line: 171, type: !973, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!973 = !DISubroutineType(types: !974)
!974 = !{!927, !221}
!975 = !{i32 2, !"Dwarf Version", i32 4}
!976 = !{i32 2, !"Debug Info Version", i32 3}
!977 = !{i32 4, !"nvvm-reflect-ftz", i32 0}
!978 = !{!"clang version 5.0.0 (trunk 294196)"}
!979 = !{i32 1, i32 2}
!980 = !{null, !"align", i32 8}
!981 = !{null, !"align", i32 8, !"align", i32 65544, !"align", i32 131080}
!982 = !{null, !"align", i32 16}
!983 = !{null, !"align", i32 16, !"align", i32 65552, !"align", i32 131088}
!984 = distinct !DISubprogram(name: "mystrcpy", linkageName: "_Z8mystrcpyPcS_", scope: !3, file: !3, line: 59, type: !985, isLocal: false, isDefinition: true, scopeLine: 60, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !987)
!985 = !DISubroutineType(types: !986)
!986 = !{null, !6, !6}
!987 = !{!988, !989, !990}
!988 = !DILocalVariable(name: "dst", arg: 1, scope: !984, file: !3, line: 59, type: !6)
!989 = !DILocalVariable(name: "src", arg: 2, scope: !984, file: !3, line: 59, type: !6)
!990 = !DILocalVariable(name: "cnt", scope: !984, file: !3, line: 61, type: !8)
!991 = !DIExpression()
!992 = !DILocation(line: 59, column: 32, scope: !984)
!993 = !DILocation(line: 59, column: 43, scope: !984)
!994 = !DILocation(line: 61, column: 6, scope: !984)
!995 = !DILocation(line: 62, column: 10, scope: !996)
!996 = !DILexicalBlockFile(scope: !984, file: !3, discriminator: 1)
!997 = !{!998, !998, i64 0}
!998 = !{!"omnipotent char", !999, i64 0}
!999 = !{!"Simple C++ TBAA"}
!1000 = !DILocation(line: 62, column: 19, scope: !996)
!1001 = !DILocation(line: 62, column: 2, scope: !1002)
!1002 = !DILexicalBlockFile(scope: !984, file: !3, discriminator: 3)
!1003 = !DILocation(line: 64, column: 12, scope: !1004)
!1004 = distinct !DILexicalBlock(scope: !984, file: !3, line: 63, column: 2)
!1005 = !DILocation(line: 65, column: 6, scope: !1004)
!1006 = !DILocation(line: 62, column: 34, scope: !1007)
!1007 = !DILexicalBlockFile(scope: !984, file: !3, discriminator: 2)
!1008 = !DILocation(line: 62, column: 27, scope: !996)
!1009 = distinct !{!1009, !1010, !1011}
!1010 = !DILocation(line: 62, column: 2, scope: !984)
!1011 = !DILocation(line: 66, column: 2, scope: !984)
!1012 = !DILocation(line: 67, column: 11, scope: !984)
!1013 = !DILocation(line: 69, column: 1, scope: !984)
!1014 = distinct !DISubprogram(name: "mystrcmp", linkageName: "_Z8mystrcmpPcS_", scope: !3, file: !3, line: 71, type: !1015, isLocal: false, isDefinition: true, scopeLine: 72, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1017)
!1015 = !DISubroutineType(types: !1016)
!1016 = !{!39, !6, !6}
!1017 = !{!1018, !1019, !1020}
!1018 = !DILocalVariable(name: "dst", arg: 1, scope: !1014, file: !3, line: 71, type: !6)
!1019 = !DILocalVariable(name: "src", arg: 2, scope: !1014, file: !3, line: 71, type: !6)
!1020 = !DILocalVariable(name: "cnt", scope: !1014, file: !3, line: 73, type: !8)
!1021 = !DILocation(line: 71, column: 32, scope: !1014)
!1022 = !DILocation(line: 71, column: 43, scope: !1014)
!1023 = !DILocation(line: 73, column: 13, scope: !1014)
!1024 = !DILocation(line: 74, column: 9, scope: !1025)
!1025 = !DILexicalBlockFile(scope: !1014, file: !3, discriminator: 1)
!1026 = !DILocation(line: 81, column: 20, scope: !1027)
!1027 = distinct !DILexicalBlock(scope: !1014, file: !3, line: 75, column: 9)
!1028 = !DILocation(line: 76, column: 8, scope: !1029)
!1029 = distinct !DILexicalBlock(scope: !1027, file: !3, line: 76, column: 8)
!1030 = !DILocation(line: 76, column: 28, scope: !1031)
!1031 = !DILexicalBlockFile(scope: !1029, file: !3, discriminator: 1)
!1032 = !DILocation(line: 76, column: 25, scope: !1029)
!1033 = !DILocation(line: 79, column: 30, scope: !1034)
!1034 = distinct !DILexicalBlock(scope: !1027, file: !3, line: 79, column: 21)
!1035 = !DILocation(line: 79, column: 21, scope: !1027)
!1036 = !DILocation(line: 84, column: 1, scope: !1014)
!1037 = !DILocation(line: 74, column: 21, scope: !1025)
!1038 = distinct !{!1038, !1039, !1040}
!1039 = !DILocation(line: 74, column: 9, scope: !1014)
!1040 = !DILocation(line: 82, column: 9, scope: !1014)
!1041 = distinct !DISubprogram(name: "getFuncID", linkageName: "_Z9getFuncIDPc", scope: !3, file: !3, line: 86, type: !1042, isLocal: false, isDefinition: true, scopeLine: 87, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1044)
!1042 = !DISubroutineType(types: !1043)
!1043 = !{!8, !6}
!1044 = !{!1045, !1046, !1048}
!1045 = !DILocalVariable(name: "func", arg: 1, scope: !1041, file: !3, line: 86, type: !6)
!1046 = !DILocalVariable(name: "i", scope: !1047, file: !3, line: 99, type: !8)
!1047 = distinct !DILexicalBlock(scope: !1041, file: !3, line: 99, column: 2)
!1048 = !DILocalVariable(name: "found", scope: !1049, file: !3, line: 101, type: !39)
!1049 = distinct !DILexicalBlock(scope: !1050, file: !3, line: 100, column: 2)
!1050 = distinct !DILexicalBlock(scope: !1047, file: !3, line: 99, column: 2)
!1051 = !DILocation(line: 86, column: 32, scope: !1041)
!1052 = !DILocation(line: 89, column: 6, scope: !1053)
!1053 = distinct !DILexicalBlock(scope: !1041, file: !3, line: 89, column: 6)
!1054 = !{!1055, !1055, i64 0}
!1055 = !{!"int", !998, i64 0}
!1056 = !DILocation(line: 89, column: 16, scope: !1053)
!1057 = !DILocation(line: 89, column: 6, scope: !1041)
!1058 = !DILocation(line: 99, column: 10, scope: !1047)
!1059 = !DILocation(line: 99, column: 17, scope: !1060)
!1060 = !DILexicalBlockFile(scope: !1050, file: !3, discriminator: 1)
!1061 = !DILocation(line: 99, column: 2, scope: !1062)
!1062 = !DILexicalBlockFile(scope: !1047, file: !3, discriminator: 1)
!1063 = !DILocation(line: 101, column: 26, scope: !1049)
!1064 = !DILocation(line: 59, column: 32, scope: !984, inlinedAt: !1065)
!1065 = distinct !DILocation(line: 91, column: 3, scope: !1066)
!1066 = distinct !DILexicalBlock(scope: !1053, file: !3, line: 90, column: 2)
!1067 = !DILocation(line: 59, column: 43, scope: !984, inlinedAt: !1065)
!1068 = !DILocation(line: 61, column: 6, scope: !984, inlinedAt: !1065)
!1069 = !DILocation(line: 62, column: 10, scope: !996, inlinedAt: !1065)
!1070 = !DILocation(line: 62, column: 19, scope: !996, inlinedAt: !1065)
!1071 = !DILocation(line: 62, column: 2, scope: !1002, inlinedAt: !1065)
!1072 = !DILocation(line: 64, column: 12, scope: !1004, inlinedAt: !1065)
!1073 = !DILocation(line: 65, column: 6, scope: !1004, inlinedAt: !1065)
!1074 = !DILocation(line: 62, column: 34, scope: !1007, inlinedAt: !1065)
!1075 = !DILocation(line: 62, column: 27, scope: !996, inlinedAt: !1065)
!1076 = !DILocation(line: 71, column: 43, scope: !1014, inlinedAt: !1077)
!1077 = distinct !DILocation(line: 101, column: 16, scope: !1049)
!1078 = !DILocation(line: 73, column: 13, scope: !1014, inlinedAt: !1077)
!1079 = !DILocation(line: 74, column: 9, scope: !1025, inlinedAt: !1077)
!1080 = !DILocation(line: 81, column: 20, scope: !1027, inlinedAt: !1077)
!1081 = !DILocation(line: 76, column: 8, scope: !1029, inlinedAt: !1077)
!1082 = !DILocation(line: 76, column: 28, scope: !1031, inlinedAt: !1077)
!1083 = !DILocation(line: 76, column: 25, scope: !1029, inlinedAt: !1077)
!1084 = !DILocation(line: 79, column: 30, scope: !1034, inlinedAt: !1077)
!1085 = !DILocation(line: 79, column: 21, scope: !1027, inlinedAt: !1077)
!1086 = !DILocation(line: 99, column: 31, scope: !1087)
!1087 = !DILexicalBlockFile(scope: !1050, file: !3, discriminator: 3)
!1088 = distinct !{!1088, !1089, !1090}
!1089 = !DILocation(line: 99, column: 2, scope: !1047)
!1090 = !DILocation(line: 105, column: 2, scope: !1047)
!1091 = !DILocation(line: 109, column: 11, scope: !1041)
!1092 = !DILocation(line: 59, column: 32, scope: !984, inlinedAt: !1093)
!1093 = distinct !DILocation(line: 109, column: 2, scope: !1041)
!1094 = !DILocation(line: 59, column: 43, scope: !984, inlinedAt: !1093)
!1095 = !DILocation(line: 61, column: 6, scope: !984, inlinedAt: !1093)
!1096 = !DILocation(line: 62, column: 10, scope: !996, inlinedAt: !1093)
!1097 = !DILocation(line: 62, column: 19, scope: !996, inlinedAt: !1093)
!1098 = !DILocation(line: 62, column: 2, scope: !1002, inlinedAt: !1093)
!1099 = !DILocation(line: 64, column: 12, scope: !1004, inlinedAt: !1093)
!1100 = !DILocation(line: 65, column: 6, scope: !1004, inlinedAt: !1093)
!1101 = !DILocation(line: 62, column: 34, scope: !1007, inlinedAt: !1093)
!1102 = !DILocation(line: 62, column: 27, scope: !996, inlinedAt: !1093)
!1103 = !DILocation(line: 67, column: 11, scope: !984, inlinedAt: !1065)
!1104 = !DILocation(line: 113, column: 1, scope: !1041)
!1105 = !DILocation(line: 74, column: 21, scope: !1025, inlinedAt: !1077)
!1106 = distinct !DISubprogram(name: "updateCallStack", linkageName: "_Z15updateCallStackiissi", scope: !3, file: !3, line: 115, type: !1107, isLocal: false, isDefinition: true, scopeLine: 116, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1109)
!1107 = !DISubroutineType(types: !1108)
!1108 = !{null, !8, !8, !15, !15, !8}
!1109 = !{!1110, !1111, !1112, !1113, !1114, !1115, !1116, !1118, !1119, !1120}
!1110 = !DILocalVariable(name: "caller", arg: 1, scope: !1106, file: !3, line: 115, type: !8)
!1111 = !DILocalVariable(name: "callee", arg: 2, scope: !1106, file: !3, line: 115, type: !8)
!1112 = !DILocalVariable(name: "sline", arg: 3, scope: !1106, file: !3, line: 115, type: !15)
!1113 = !DILocalVariable(name: "scolm", arg: 4, scope: !1106, file: !3, line: 115, type: !15)
!1114 = !DILocalVariable(name: "tid", arg: 5, scope: !1106, file: !3, line: 115, type: !8)
!1115 = !DILocalVariable(name: "callStack", scope: !1106, file: !3, line: 118, type: !59)
!1116 = !DILocalVariable(name: "height", scope: !1106, file: !3, line: 119, type: !1117)
!1117 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !8, size: 64)
!1118 = !DILocalVariable(name: "p_caller", scope: !1106, file: !3, line: 137, type: !8)
!1119 = !DILocalVariable(name: "p_callee", scope: !1106, file: !3, line: 138, type: !8)
!1120 = !DILocalVariable(name: "i", scope: !1121, file: !3, line: 167, type: !8)
!1121 = distinct !DILexicalBlock(scope: !1106, file: !3, line: 167, column: 9)
!1122 = !DILocation(line: 115, column: 37, scope: !1106)
!1123 = !DILocation(line: 115, column: 49, scope: !1106)
!1124 = !DILocation(line: 115, column: 63, scope: !1106)
!1125 = !DILocation(line: 115, column: 76, scope: !1106)
!1126 = !DILocation(line: 115, column: 87, scope: !1106)
!1127 = !DILocation(line: 118, column: 26, scope: !1106)
!1128 = !{!1129, !1129, i64 0}
!1129 = !{!"any pointer", !998, i64 0}
!1130 = !DILocation(line: 118, column: 14, scope: !1106)
!1131 = !DILocation(line: 119, column: 16, scope: !1106)
!1132 = !DILocation(line: 119, column: 7, scope: !1106)
!1133 = !DILocation(line: 121, column: 9, scope: !1106)
!1134 = !DILocation(line: 121, column: 9, scope: !1135)
!1135 = !DILexicalBlockFile(scope: !1106, file: !3, discriminator: 2)
!1136 = !DILocation(line: 125, column: 3, scope: !1137)
!1137 = distinct !DILexicalBlock(scope: !1138, file: !3, line: 124, column: 9)
!1138 = distinct !DILexicalBlock(scope: !1106, file: !3, line: 123, column: 13)
!1139 = !DILocation(line: 126, column: 16, scope: !1137)
!1140 = !DILocation(line: 126, column: 19, scope: !1137)
!1141 = !{!1142, !1055, i64 0}
!1142 = !{!"_ZTS10CallSite_t", !1055, i64 0, !1143, i64 4, !1143, i64 6}
!1143 = !{!"short", !998, i64 0}
!1144 = !DILocation(line: 127, column: 16, scope: !1137)
!1145 = !DILocation(line: 127, column: 22, scope: !1137)
!1146 = !{!1142, !1143, i64 4}
!1147 = !DILocation(line: 128, column: 16, scope: !1137)
!1148 = !DILocation(line: 128, column: 22, scope: !1137)
!1149 = !{!1142, !1143, i64 6}
!1150 = !DILocation(line: 130, column: 16, scope: !1137)
!1151 = !DILocation(line: 130, column: 19, scope: !1137)
!1152 = !DILocation(line: 131, column: 30, scope: !1137)
!1153 = !DILocation(line: 131, column: 36, scope: !1137)
!1154 = !DILocation(line: 132, column: 30, scope: !1137)
!1155 = !DILocation(line: 132, column: 36, scope: !1137)
!1156 = !DILocation(line: 133, column: 9, scope: !1137)
!1157 = !DILocation(line: 134, column: 17, scope: !1137)
!1158 = !DILocation(line: 137, column: 40, scope: !1106)
!1159 = !DILocation(line: 137, column: 24, scope: !1106)
!1160 = !DILocation(line: 137, column: 44, scope: !1106)
!1161 = !DILocation(line: 137, column: 13, scope: !1106)
!1162 = !DILocation(line: 138, column: 40, scope: !1106)
!1163 = !DILocation(line: 138, column: 24, scope: !1106)
!1164 = !DILocation(line: 138, column: 44, scope: !1106)
!1165 = !DILocation(line: 138, column: 13, scope: !1106)
!1166 = !DILocation(line: 140, column: 23, scope: !1167)
!1167 = distinct !DILexicalBlock(scope: !1106, file: !3, line: 140, column: 14)
!1168 = !DILocation(line: 140, column: 45, scope: !1169)
!1169 = !DILexicalBlockFile(scope: !1167, file: !3, discriminator: 1)
!1170 = !DILocation(line: 140, column: 33, scope: !1167)
!1171 = !DILocation(line: 142, column: 37, scope: !1172)
!1172 = distinct !DILexicalBlock(scope: !1167, file: !3, line: 141, column: 9)
!1173 = !DILocation(line: 142, column: 43, scope: !1172)
!1174 = !DILocation(line: 143, column: 37, scope: !1172)
!1175 = !DILocation(line: 143, column: 43, scope: !1172)
!1176 = !DILocation(line: 144, column: 17, scope: !1172)
!1177 = !DILocation(line: 146, column: 38, scope: !1178)
!1178 = distinct !DILexicalBlock(scope: !1167, file: !3, line: 146, column: 19)
!1179 = !DILocation(line: 148, column: 10, scope: !1180)
!1180 = distinct !DILexicalBlock(scope: !1178, file: !3, line: 147, column: 9)
!1181 = !DILocation(line: 149, column: 27, scope: !1180)
!1182 = !DILocation(line: 149, column: 33, scope: !1180)
!1183 = !DILocation(line: 149, column: 17, scope: !1180)
!1184 = !DILocation(line: 149, column: 37, scope: !1180)
!1185 = !DILocation(line: 149, column: 40, scope: !1180)
!1186 = !DILocation(line: 150, column: 27, scope: !1180)
!1187 = !DILocation(line: 150, column: 33, scope: !1180)
!1188 = !DILocation(line: 150, column: 17, scope: !1180)
!1189 = !DILocation(line: 150, column: 37, scope: !1180)
!1190 = !DILocation(line: 150, column: 43, scope: !1180)
!1191 = !DILocation(line: 151, column: 37, scope: !1180)
!1192 = !DILocation(line: 151, column: 43, scope: !1180)
!1193 = !DILocation(line: 152, column: 17, scope: !1180)
!1194 = !DILocation(line: 154, column: 28, scope: !1195)
!1195 = distinct !DILexicalBlock(scope: !1178, file: !3, line: 154, column: 19)
!1196 = !DILocation(line: 154, column: 19, scope: !1178)
!1197 = !DILocation(line: 156, column: 3, scope: !1198)
!1198 = distinct !DILexicalBlock(scope: !1195, file: !3, line: 155, column: 9)
!1199 = !DILocation(line: 157, column: 27, scope: !1198)
!1200 = !DILocation(line: 157, column: 33, scope: !1198)
!1201 = !DILocation(line: 157, column: 17, scope: !1198)
!1202 = !DILocation(line: 157, column: 37, scope: !1198)
!1203 = !DILocation(line: 157, column: 43, scope: !1198)
!1204 = !DILocation(line: 158, column: 37, scope: !1198)
!1205 = !DILocation(line: 158, column: 43, scope: !1198)
!1206 = !DILocation(line: 160, column: 17, scope: !1198)
!1207 = !DILocation(line: 160, column: 35, scope: !1198)
!1208 = !DILocation(line: 160, column: 38, scope: !1198)
!1209 = !DILocation(line: 161, column: 27, scope: !1198)
!1210 = !DILocation(line: 161, column: 17, scope: !1198)
!1211 = !DILocation(line: 161, column: 35, scope: !1198)
!1212 = !DILocation(line: 161, column: 41, scope: !1198)
!1213 = !DILocation(line: 162, column: 35, scope: !1198)
!1214 = !DILocation(line: 162, column: 41, scope: !1198)
!1215 = !DILocation(line: 163, column: 9, scope: !1198)
!1216 = !DILocation(line: 164, column: 17, scope: !1198)
!1217 = !DILocation(line: 167, column: 18, scope: !1121)
!1218 = !DILocation(line: 167, column: 31, scope: !1219)
!1219 = !DILexicalBlockFile(scope: !1220, file: !3, discriminator: 1)
!1220 = distinct !DILexicalBlock(scope: !1121, file: !3, line: 167, column: 9)
!1221 = !DILocation(line: 167, column: 9, scope: !1222)
!1222 = !DILexicalBlockFile(scope: !1121, file: !3, discriminator: 1)
!1223 = !DILocation(line: 169, column: 22, scope: !1224)
!1224 = distinct !DILexicalBlock(scope: !1225, file: !3, line: 169, column: 22)
!1225 = distinct !DILexicalBlock(scope: !1220, file: !3, line: 168, column: 9)
!1226 = !DILocation(line: 169, column: 35, scope: !1224)
!1227 = !DILocation(line: 169, column: 38, scope: !1224)
!1228 = !DILocation(line: 169, column: 22, scope: !1225)
!1229 = distinct !{!1229, !1230, !1231}
!1230 = !DILocation(line: 167, column: 9, scope: !1121)
!1231 = !DILocation(line: 180, column: 9, scope: !1121)
!1232 = !DILocation(line: 171, column: 11, scope: !1233)
!1233 = distinct !DILexicalBlock(scope: !1224, file: !3, line: 170, column: 17)
!1234 = !DILocation(line: 172, column: 41, scope: !1233)
!1235 = !DILocation(line: 173, column: 38, scope: !1233)
!1236 = !DILocation(line: 174, column: 38, scope: !1233)
!1237 = !DILocation(line: 176, column: 44, scope: !1233)
!1238 = !DILocation(line: 177, column: 44, scope: !1233)
!1239 = !DILocation(line: 183, column: 9, scope: !1106)
!1240 = !DILocation(line: 184, column: 1, scope: !1241)
!1241 = !DILexicalBlockFile(scope: !1106, file: !3, discriminator: 3)
!1242 = distinct !DISubprogram(name: "__assert_fail", linkageName: "_ZL13__assert_failPKcS0_jS0_", scope: !1243, file: !1243, line: 281, type: !1244, isLocal: true, isDefinition: true, scopeLine: 283, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1246)
!1243 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_runtime_wrapper.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!1244 = !DISubroutineType(types: !1245)
!1245 = !{null, !221, !221, !436, !221}
!1246 = !{!1247, !1248, !1249, !1250}
!1247 = !DILocalVariable(name: "__message", arg: 1, scope: !1242, file: !1243, line: 281, type: !221)
!1248 = !DILocalVariable(name: "__file", arg: 2, scope: !1242, file: !1243, line: 282, type: !221)
!1249 = !DILocalVariable(name: "__line", arg: 3, scope: !1242, file: !1243, line: 282, type: !436)
!1250 = !DILocalVariable(name: "__function", arg: 4, scope: !1242, file: !1243, line: 283, type: !221)
!1251 = !DILocation(line: 284, column: 3, scope: !1242)
!1252 = distinct !DISubprogram(name: "printCallStack", linkageName: "_Z14printCallStacki", scope: !3, file: !3, line: 187, type: !384, isLocal: false, isDefinition: true, scopeLine: 188, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1253)
!1253 = !{!1254, !1255, !1256, !1257}
!1254 = !DILocalVariable(name: "tid", arg: 1, scope: !1252, file: !3, line: 187, type: !8)
!1255 = !DILocalVariable(name: "callStack", scope: !1252, file: !3, line: 189, type: !59)
!1256 = !DILocalVariable(name: "height", scope: !1252, file: !3, line: 190, type: !8)
!1257 = !DILocalVariable(name: "i", scope: !1258, file: !3, line: 197, type: !8)
!1258 = distinct !DILexicalBlock(scope: !1252, file: !3, line: 197, column: 9)
!1259 = !DILocation(line: 187, column: 36, scope: !1252)
!1260 = !DILocation(line: 189, column: 26, scope: !1252)
!1261 = !DILocation(line: 189, column: 14, scope: !1252)
!1262 = !DILocation(line: 190, column: 22, scope: !1252)
!1263 = !DILocation(line: 190, column: 13, scope: !1252)
!1264 = !DILocation(line: 191, column: 2, scope: !1252)
!1265 = !DILocation(line: 194, column: 19, scope: !1266)
!1266 = distinct !DILexicalBlock(scope: !1252, file: !3, line: 194, column: 13)
!1267 = !DILocation(line: 197, column: 18, scope: !1258)
!1268 = !DILocation(line: 194, column: 13, scope: !1252)
!1269 = !DILocation(line: 197, column: 9, scope: !1270)
!1270 = !DILexicalBlockFile(scope: !1258, file: !3, discriminator: 1)
!1271 = !DILocation(line: 198, column: 74, scope: !1272)
!1272 = distinct !DILexicalBlock(scope: !1258, file: !3, line: 197, column: 9)
!1273 = !DILocation(line: 198, column: 92, scope: !1272)
!1274 = !DILocation(line: 198, column: 79, scope: !1272)
!1275 = !DILocation(line: 198, column: 113, scope: !1272)
!1276 = !DILocation(line: 198, column: 100, scope: !1272)
!1277 = !DILocation(line: 198, column: 17, scope: !1272)
!1278 = !DILocation(line: 198, column: 61, scope: !1272)
!1279 = !DILocation(line: 197, column: 34, scope: !1280)
!1280 = !DILexicalBlockFile(scope: !1272, file: !3, discriminator: 3)
!1281 = !DILocation(line: 197, column: 24, scope: !1282)
!1282 = !DILexicalBlockFile(scope: !1272, file: !3, discriminator: 1)
!1283 = distinct !{!1283, !1284, !1285}
!1284 = !DILocation(line: 197, column: 9, scope: !1258)
!1285 = !DILocation(line: 198, column: 119, scope: !1258)
!1286 = !DILocation(line: 199, column: 1, scope: !1287)
!1287 = !DILexicalBlockFile(scope: !1252, file: !3, discriminator: 2)
!1288 = distinct !DISubprogram(name: "holdon", linkageName: "_Z6holdonl", scope: !3, file: !3, line: 201, type: !1289, isLocal: false, isDefinition: true, scopeLine: 202, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1294)
!1289 = !DISubroutineType(types: !1290)
!1290 = !{!8, !1291}
!1291 = !DIDerivedType(tag: DW_TAG_typedef, name: "clock_t", file: !1292, line: 59, baseType: !1293)
!1292 = !DIFile(filename: "/usr/include/time.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!1293 = !DIDerivedType(tag: DW_TAG_typedef, name: "__clock_t", file: !925, line: 135, baseType: !26)
!1294 = !{!1295, !1296, !1297}
!1295 = !DILocalVariable(name: "c", arg: 1, scope: !1288, file: !3, line: 201, type: !1291)
!1296 = !DILocalVariable(name: "start_clock", scope: !1288, file: !3, line: 203, type: !1291)
!1297 = !DILocalVariable(name: "clock_offset", scope: !1288, file: !3, line: 204, type: !1291)
!1298 = !DILocation(line: 201, column: 31, scope: !1288)
!1299 = !DILocation(line: 344, column: 3, scope: !1300, inlinedAt: !1303)
!1300 = distinct !DISubprogram(name: "clock", linkageName: "_ZL5clockv", scope: !533, file: !533, line: 340, type: !426, isLocal: true, isDefinition: true, scopeLine: 342, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1301)
!1301 = !{!1302}
!1302 = !DILocalVariable(name: "r", scope: !1300, file: !533, line: 343, type: !8)
!1303 = distinct !DILocation(line: 203, column: 24, scope: !1288)
!1304 = !{i32 2280604}
!1305 = !DILocation(line: 343, column: 7, scope: !1300, inlinedAt: !1303)
!1306 = !DILocation(line: 203, column: 24, scope: !1288)
!1307 = !DILocation(line: 203, column: 10, scope: !1288)
!1308 = !DILocation(line: 204, column: 14, scope: !1288)
!1309 = !DILocation(line: 205, column: 23, scope: !1310)
!1310 = !DILexicalBlockFile(scope: !1288, file: !3, discriminator: 1)
!1311 = !DILocation(line: 205, column: 3, scope: !1310)
!1312 = !DILocation(line: 344, column: 3, scope: !1300, inlinedAt: !1313)
!1313 = distinct !DILocation(line: 206, column: 25, scope: !1288)
!1314 = !DILocation(line: 343, column: 7, scope: !1300, inlinedAt: !1313)
!1315 = !DILocation(line: 206, column: 25, scope: !1288)
!1316 = !DILocation(line: 206, column: 33, scope: !1288)
!1317 = distinct !{!1317, !1318, !1319}
!1318 = !DILocation(line: 205, column: 3, scope: !1288)
!1319 = !DILocation(line: 206, column: 35, scope: !1288)
!1320 = !DILocation(line: 208, column: 9, scope: !1288)
!1321 = !DILocation(line: 208, column: 2, scope: !1288)
!1322 = distinct !DISubprogram(name: "InitKernel", scope: !3, file: !3, line: 212, type: !337, isLocal: false, isDefinition: true, scopeLine: 213, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !4)
!1323 = !DILocation(line: 214, column: 2, scope: !1322)
!1324 = distinct !DISubprogram(name: "callFunc", scope: !3, file: !3, line: 291, type: !1325, isLocal: false, isDefinition: true, scopeLine: 292, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1327)
!1325 = !DISubroutineType(types: !1326)
!1326 = !{null, !364, !364, !8, !8}
!1327 = !{!1328, !1329, !1330, !1331}
!1328 = !DILocalVariable(name: "er", arg: 1, scope: !1324, file: !3, line: 291, type: !364)
!1329 = !DILocalVariable(name: "ee", arg: 2, scope: !1324, file: !3, line: 291, type: !364)
!1330 = !DILocalVariable(name: "sline", arg: 3, scope: !1324, file: !3, line: 291, type: !8)
!1331 = !DILocalVariable(name: "scolm", arg: 4, scope: !1324, file: !3, line: 291, type: !8)
!1332 = !DILocation(line: 291, column: 32, scope: !1324)
!1333 = !DILocation(line: 291, column: 42, scope: !1324)
!1334 = !DILocation(line: 291, column: 50, scope: !1324)
!1335 = !DILocation(line: 291, column: 61, scope: !1324)
!1336 = !DILocation(line: 293, column: 2, scope: !1324)
!1337 = distinct !DISubprogram(name: "takeString", scope: !3, file: !3, line: 318, type: !1338, isLocal: false, isDefinition: true, scopeLine: 319, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1340)
!1338 = !DISubroutineType(types: !1339)
!1339 = !{null, !364, !8}
!1340 = !{!1341, !1342}
!1341 = !DILocalVariable(name: "p", arg: 1, scope: !1337, file: !3, line: 318, type: !364)
!1342 = !DILocalVariable(name: "action", arg: 2, scope: !1337, file: !3, line: 318, type: !8)
!1343 = !DILocation(line: 318, column: 34, scope: !1337)
!1344 = !DILocation(line: 318, column: 41, scope: !1337)
!1345 = !DILocation(line: 67, column: 3, scope: !1346, inlinedAt: !1381)
!1346 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !1348, file: !1347, line: 67, type: !1351, isLocal: false, isDefinition: true, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1350, variables: !4)
!1347 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_builtin_vars.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!1348 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_threadIdx_t", file: !1347, line: 66, size: 8, elements: !1349, identifier: "_ZTS26__cuda_builtin_threadIdx_t")
!1349 = !{!1350, !1353, !1354, !1355, !1366, !1370, !1374, !1377}
!1350 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !1348, file: !1347, line: 67, type: !1351, isLocal: false, isDefinition: false, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true)
!1351 = !DISubroutineType(types: !1352)
!1352 = !{!436}
!1353 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_yEv", scope: !1348, file: !1347, line: 68, type: !1351, isLocal: false, isDefinition: false, scopeLine: 68, flags: DIFlagPrototyped, isOptimized: true)
!1354 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_zEv", scope: !1348, file: !1347, line: 69, type: !1351, isLocal: false, isDefinition: false, scopeLine: 69, flags: DIFlagPrototyped, isOptimized: true)
!1355 = !DISubprogram(name: "operator uint3", linkageName: "_ZNK26__cuda_builtin_threadIdx_tcv5uint3Ev", scope: !1348, file: !1347, line: 72, type: !1356, isLocal: false, isDefinition: false, scopeLine: 72, flags: DIFlagPrototyped, isOptimized: true)
!1356 = !DISubroutineType(types: !1357)
!1357 = !{!1358, !1364}
!1358 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "uint3", file: !1359, line: 190, size: 96, elements: !1360, identifier: "_ZTS5uint3")
!1359 = !DIFile(filename: "/usr/local/cuda/include/vector_types.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!1360 = !{!1361, !1362, !1363}
!1361 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !1358, file: !1359, line: 192, baseType: !436, size: 32)
!1362 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !1358, file: !1359, line: 192, baseType: !436, size: 32, offset: 32)
!1363 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !1358, file: !1359, line: 192, baseType: !436, size: 32, offset: 64)
!1364 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1365, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1365 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1348)
!1366 = !DISubprogram(name: "__cuda_builtin_threadIdx_t", scope: !1348, file: !1347, line: 74, type: !1367, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1367 = !DISubroutineType(types: !1368)
!1368 = !{null, !1369}
!1369 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1348, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1370 = !DISubprogram(name: "__cuda_builtin_threadIdx_t", scope: !1348, file: !1347, line: 74, type: !1371, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1371 = !DISubroutineType(types: !1372)
!1372 = !{null, !1369, !1373}
!1373 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1365, size: 64)
!1374 = !DISubprogram(name: "operator=", linkageName: "_ZNK26__cuda_builtin_threadIdx_taSERKS_", scope: !1348, file: !1347, line: 74, type: !1375, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1375 = !DISubroutineType(types: !1376)
!1376 = !{null, !1364, !1373}
!1377 = !DISubprogram(name: "operator&", linkageName: "_ZNK26__cuda_builtin_threadIdx_tadEv", scope: !1348, file: !1347, line: 74, type: !1378, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1378 = !DISubroutineType(types: !1379)
!1379 = !{!1380, !1364}
!1380 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1348, size: 64)
!1381 = distinct !DILocation(line: 320, column: 6, scope: !1382)
!1382 = distinct !DILexicalBlock(scope: !1337, file: !3, line: 320, column: 6)
!1383 = !{i32 0, i32 1024}
!1384 = !DILocation(line: 320, column: 18, scope: !1382)
!1385 = !DILocation(line: 320, column: 23, scope: !1382)
!1386 = !DILocation(line: 78, column: 3, scope: !1387, inlinedAt: !1413)
!1387 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !1388, file: !1347, line: 78, type: !1351, isLocal: false, isDefinition: true, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1390, variables: !4)
!1388 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_blockIdx_t", file: !1347, line: 77, size: 8, elements: !1389, identifier: "_ZTS25__cuda_builtin_blockIdx_t")
!1389 = !{!1390, !1391, !1392, !1393, !1398, !1402, !1406, !1409}
!1390 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !1388, file: !1347, line: 78, type: !1351, isLocal: false, isDefinition: false, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true)
!1391 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_yEv", scope: !1388, file: !1347, line: 79, type: !1351, isLocal: false, isDefinition: false, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true)
!1392 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_zEv", scope: !1388, file: !1347, line: 80, type: !1351, isLocal: false, isDefinition: false, scopeLine: 80, flags: DIFlagPrototyped, isOptimized: true)
!1393 = !DISubprogram(name: "operator uint3", linkageName: "_ZNK25__cuda_builtin_blockIdx_tcv5uint3Ev", scope: !1388, file: !1347, line: 83, type: !1394, isLocal: false, isDefinition: false, scopeLine: 83, flags: DIFlagPrototyped, isOptimized: true)
!1394 = !DISubroutineType(types: !1395)
!1395 = !{!1358, !1396}
!1396 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1397, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1397 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1388)
!1398 = !DISubprogram(name: "__cuda_builtin_blockIdx_t", scope: !1388, file: !1347, line: 85, type: !1399, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1399 = !DISubroutineType(types: !1400)
!1400 = !{null, !1401}
!1401 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1388, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1402 = !DISubprogram(name: "__cuda_builtin_blockIdx_t", scope: !1388, file: !1347, line: 85, type: !1403, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1403 = !DISubroutineType(types: !1404)
!1404 = !{null, !1401, !1405}
!1405 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1397, size: 64)
!1406 = !DISubprogram(name: "operator=", linkageName: "_ZNK25__cuda_builtin_blockIdx_taSERKS_", scope: !1388, file: !1347, line: 85, type: !1407, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1407 = !DISubroutineType(types: !1408)
!1408 = !{null, !1396, !1405}
!1409 = !DISubprogram(name: "operator&", linkageName: "_ZNK25__cuda_builtin_blockIdx_tadEv", scope: !1388, file: !1347, line: 85, type: !1410, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1410 = !DISubroutineType(types: !1411)
!1411 = !{!1412, !1396}
!1412 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1388, size: 64)
!1413 = distinct !DILocation(line: 320, column: 26, scope: !1414)
!1414 = !DILexicalBlockFile(scope: !1382, file: !3, discriminator: 1)
!1415 = !{i32 0, i32 65535}
!1416 = !DILocation(line: 320, column: 37, scope: !1414)
!1417 = !DILocation(line: 320, column: 42, scope: !1414)
!1418 = !DILocation(line: 68, column: 3, scope: !1419, inlinedAt: !1420)
!1419 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_yEv", scope: !1348, file: !1347, line: 68, type: !1351, isLocal: false, isDefinition: true, scopeLine: 68, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1353, variables: !4)
!1420 = distinct !DILocation(line: 320, column: 45, scope: !1421)
!1421 = !DILexicalBlockFile(scope: !1382, file: !3, discriminator: 2)
!1422 = !DILocation(line: 320, column: 57, scope: !1421)
!1423 = !DILocation(line: 320, column: 62, scope: !1421)
!1424 = !DILocation(line: 79, column: 3, scope: !1425, inlinedAt: !1426)
!1425 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_yEv", scope: !1388, file: !1347, line: 79, type: !1351, isLocal: false, isDefinition: true, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1391, variables: !4)
!1426 = distinct !DILocation(line: 320, column: 65, scope: !1427)
!1427 = !DILexicalBlockFile(scope: !1382, file: !3, discriminator: 3)
!1428 = !DILocation(line: 320, column: 76, scope: !1427)
!1429 = !DILocation(line: 320, column: 6, scope: !1430)
!1430 = !DILexicalBlockFile(scope: !1337, file: !3, discriminator: 3)
!1431 = !DILocation(line: 322, column: 6, scope: !1432)
!1432 = distinct !DILexicalBlock(scope: !1337, file: !3, line: 322, column: 6)
!1433 = !{!1434, !1434, i64 0}
!1434 = !{!"bool", !998, i64 0}
!1435 = !{i8 0, i8 2}
!1436 = !DILocation(line: 322, column: 6, scope: !1337)
!1437 = !DILocation(line: 324, column: 6, scope: !1438)
!1438 = distinct !DILexicalBlock(scope: !1432, file: !3, line: 323, column: 2)
!1439 = !DILocation(line: 325, column: 3, scope: !1440)
!1440 = distinct !DILexicalBlock(scope: !1438, file: !3, line: 324, column: 6)
!1441 = !DILocation(line: 327, column: 3, scope: !1442)
!1442 = distinct !DILexicalBlock(scope: !1440, file: !3, line: 326, column: 11)
!1443 = !DILocation(line: 329, column: 3, scope: !1444)
!1444 = distinct !DILexicalBlock(scope: !1442, file: !3, line: 328, column: 11)
!1445 = !DILocation(line: 331, column: 3, scope: !1444)
!1446 = !DILocation(line: 334, column: 1, scope: !1337)
!1447 = distinct !DISubprogram(name: "cxtprint", linkageName: "_Z8cxtprinti", scope: !3, file: !3, line: 336, type: !384, isLocal: false, isDefinition: true, scopeLine: 337, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1448)
!1448 = !{!1449, !1450}
!1449 = !DILocalVariable(name: "id", arg: 1, scope: !1447, file: !3, line: 336, type: !8)
!1450 = !DILocalVariable(name: "i", scope: !1451, file: !3, line: 344, type: !8)
!1451 = distinct !DILexicalBlock(scope: !1447, file: !3, line: 344, column: 2)
!1452 = !DILocation(line: 336, column: 30, scope: !1447)
!1453 = !DILocation(line: 338, column: 2, scope: !1447)
!1454 = !DILocation(line: 338, column: 2, scope: !1455)
!1455 = !DILexicalBlockFile(scope: !1447, file: !3, discriminator: 2)
!1456 = !DILocation(line: 339, column: 8, scope: !1457)
!1457 = distinct !DILexicalBlock(scope: !1447, file: !3, line: 339, column: 6)
!1458 = !DILocation(line: 339, column: 6, scope: !1447)
!1459 = !DILocation(line: 342, column: 2, scope: !1447)
!1460 = !DILocation(line: 344, column: 11, scope: !1451)
!1461 = !DILocation(line: 344, column: 46, scope: !1462)
!1462 = !DILexicalBlockFile(scope: !1463, file: !3, discriminator: 2)
!1463 = distinct !DILexicalBlock(scope: !1451, file: !3, line: 344, column: 2)
!1464 = !DILocation(line: 344, column: 43, scope: !1465)
!1465 = !DILexicalBlockFile(scope: !1463, file: !3, discriminator: 1)
!1466 = !DILocation(line: 344, column: 64, scope: !1462)
!1467 = !DILocation(line: 344, column: 67, scope: !1462)
!1468 = !DILocation(line: 344, column: 2, scope: !1469)
!1469 = !DILexicalBlockFile(scope: !1451, file: !3, discriminator: 3)
!1470 = !DILocation(line: 346, column: 108, scope: !1471)
!1471 = distinct !DILexicalBlock(scope: !1463, file: !3, line: 345, column: 2)
!1472 = !DILocation(line: 346, column: 90, scope: !1471)
!1473 = !DILocation(line: 346, column: 133, scope: !1471)
!1474 = !DILocation(line: 346, column: 115, scope: !1471)
!1475 = !DILocation(line: 346, column: 3, scope: !1471)
!1476 = !DILocation(line: 344, column: 77, scope: !1477)
!1477 = !DILexicalBlockFile(scope: !1463, file: !3, discriminator: 5)
!1478 = !DILocation(line: 344, column: 19, scope: !1465)
!1479 = distinct !{!1479, !1480, !1481}
!1480 = !DILocation(line: 344, column: 2, scope: !1451)
!1481 = !DILocation(line: 347, column: 2, scope: !1451)
!1482 = !DILocation(line: 350, column: 1, scope: !1447)
!1483 = distinct !DISubprogram(name: "cxtcpy", linkageName: "_Z6cxtcpyP10CallSite_tS0_i", scope: !3, file: !3, line: 353, type: !1484, isLocal: false, isDefinition: true, scopeLine: 354, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1486)
!1484 = !DISubroutineType(types: !1485)
!1485 = !{null, !59, !59, !8}
!1486 = !{!1487, !1488, !1489, !1490}
!1487 = !DILocalVariable(name: "dst", arg: 1, scope: !1483, file: !3, line: 353, type: !59)
!1488 = !DILocalVariable(name: "src", arg: 2, scope: !1483, file: !3, line: 353, type: !59)
!1489 = !DILocalVariable(name: "height", arg: 3, scope: !1483, file: !3, line: 353, type: !8)
!1490 = !DILocalVariable(name: "i", scope: !1483, file: !3, line: 355, type: !8)
!1491 = !DILocation(line: 353, column: 37, scope: !1483)
!1492 = !DILocation(line: 353, column: 54, scope: !1483)
!1493 = !DILocation(line: 353, column: 64, scope: !1483)
!1494 = !DILocation(line: 355, column: 6, scope: !1483)
!1495 = !DILocation(line: 356, column: 13, scope: !1496)
!1496 = !DILexicalBlockFile(scope: !1497, file: !3, discriminator: 1)
!1497 = distinct !DILexicalBlock(scope: !1498, file: !3, line: 356, column: 2)
!1498 = distinct !DILexicalBlock(scope: !1483, file: !3, line: 356, column: 2)
!1499 = !DILocation(line: 356, column: 2, scope: !1500)
!1500 = !DILexicalBlockFile(scope: !1498, file: !3, discriminator: 1)
!1501 = !DILocation(line: 357, column: 12, scope: !1497)
!1502 = !DILocation(line: 357, column: 3, scope: !1497)
!1503 = !DILocation(line: 357, column: 10, scope: !1497)
!1504 = !DILocation(line: 356, column: 24, scope: !1505)
!1505 = !DILexicalBlockFile(scope: !1497, file: !3, discriminator: 2)
!1506 = distinct !{!1506, !1507}
!1507 = !{!"llvm.loop.unroll.disable"}
!1508 = distinct !{!1508, !1509, !1510}
!1509 = !DILocation(line: 356, column: 2, scope: !1498)
!1510 = !DILocation(line: 357, column: 17, scope: !1498)
!1511 = !DILocation(line: 359, column: 2, scope: !1483)
!1512 = !DILocation(line: 359, column: 2, scope: !1513)
!1513 = !DILexicalBlockFile(scope: !1483, file: !3, discriminator: 2)
!1514 = !DILocation(line: 361, column: 2, scope: !1483)
!1515 = !DILocation(line: 361, column: 9, scope: !1483)
!1516 = !DILocation(line: 361, column: 12, scope: !1483)
!1517 = !DILocation(line: 365, column: 1, scope: !1483)
!1518 = distinct !DISubprogram(name: "cxtcmp", linkageName: "_Z6cxtcmpP10CallSite_tS0_i", scope: !3, file: !3, line: 368, type: !1519, isLocal: false, isDefinition: true, scopeLine: 369, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1521)
!1519 = !DISubroutineType(types: !1520)
!1520 = !{!39, !59, !59, !8}
!1521 = !{!1522, !1523, !1524, !1525}
!1522 = !DILocalVariable(name: "dst", arg: 1, scope: !1518, file: !3, line: 368, type: !59)
!1523 = !DILocalVariable(name: "src", arg: 2, scope: !1518, file: !3, line: 368, type: !59)
!1524 = !DILocalVariable(name: "height", arg: 3, scope: !1518, file: !3, line: 368, type: !8)
!1525 = !DILocalVariable(name: "i", scope: !1526, file: !3, line: 370, type: !8)
!1526 = distinct !DILexicalBlock(scope: !1518, file: !3, line: 370, column: 2)
!1527 = !DILocation(line: 368, column: 37, scope: !1518)
!1528 = !DILocation(line: 368, column: 54, scope: !1518)
!1529 = !DILocation(line: 368, column: 63, scope: !1518)
!1530 = !DILocation(line: 370, column: 11, scope: !1526)
!1531 = !DILocation(line: 370, column: 17, scope: !1532)
!1532 = !DILexicalBlockFile(scope: !1533, file: !3, discriminator: 1)
!1533 = distinct !DILexicalBlock(scope: !1526, file: !3, line: 370, column: 2)
!1534 = !DILocation(line: 370, column: 2, scope: !1535)
!1535 = !DILexicalBlockFile(scope: !1526, file: !3, discriminator: 1)
!1536 = !DILocation(line: 371, column: 8, scope: !1537)
!1537 = distinct !DILexicalBlock(scope: !1533, file: !3, line: 371, column: 8)
!1538 = distinct !{!1538, !1539, !1540}
!1539 = !DILocation(line: 370, column: 2, scope: !1526)
!1540 = !DILocation(line: 374, column: 11, scope: !1526)
!1541 = !DILocation(line: 371, column: 15, scope: !1537)
!1542 = !DILocation(line: 371, column: 28, scope: !1537)
!1543 = !DILocation(line: 371, column: 18, scope: !1537)
!1544 = !DILocation(line: 370, column: 28, scope: !1545)
!1545 = !DILexicalBlockFile(scope: !1533, file: !3, discriminator: 3)
!1546 = !DILocation(line: 371, column: 8, scope: !1533)
!1547 = !DILocation(line: 377, column: 1, scope: !1518)
!1548 = distinct !DISubprogram(name: "getContextID", scope: !3, file: !3, line: 380, type: !426, isLocal: false, isDefinition: true, scopeLine: 381, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !4)
!1549 = !DILocation(line: 384, column: 2, scope: !1548)
!1550 = distinct !DISubprogram(name: "passBasicBlock", scope: !3, file: !3, line: 434, type: !1551, isLocal: false, isDefinition: true, scopeLine: 435, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1553)
!1551 = !DISubroutineType(types: !1552)
!1552 = !{null, !364, !8, !8, !8}
!1553 = !{!1554, !1555, !1556, !1557, !1558, !1559, !1560, !1561, !1562, !1563, !1565, !1568}
!1554 = !DILocalVariable(name: "p", arg: 1, scope: !1550, file: !3, line: 434, type: !364)
!1555 = !DILocalVariable(name: "action", arg: 2, scope: !1550, file: !3, line: 434, type: !8)
!1556 = !DILocalVariable(name: "sline", arg: 3, scope: !1550, file: !3, line: 434, type: !8)
!1557 = !DILocalVariable(name: "scolm", arg: 4, scope: !1550, file: !3, line: 434, type: !8)
!1558 = !DILocalVariable(name: "str", scope: !1550, file: !3, line: 446, type: !6)
!1559 = !DILocalVariable(name: "bid", scope: !1550, file: !3, line: 448, type: !8)
!1560 = !DILocalVariable(name: "key", scope: !1550, file: !3, line: 459, type: !20)
!1561 = !DILocalVariable(name: "cnt", scope: !1550, file: !3, line: 460, type: !8)
!1562 = !DILocalVariable(name: "factor", scope: !1550, file: !3, line: 461, type: !29)
!1563 = !DILocalVariable(name: "i", scope: !1564, file: !3, line: 462, type: !8)
!1564 = distinct !DILexicalBlock(scope: !1550, file: !3, line: 462, column: 9)
!1565 = !DILocalVariable(name: "ascii", scope: !1566, file: !3, line: 464, type: !8)
!1566 = distinct !DILexicalBlock(scope: !1567, file: !3, line: 463, column: 2)
!1567 = distinct !DILexicalBlock(scope: !1564, file: !3, line: 462, column: 9)
!1568 = !DILocalVariable(name: "bblog", scope: !1550, file: !3, line: 475, type: !9)
!1569 = !DILocation(line: 434, column: 38, scope: !1550)
!1570 = !DILocation(line: 434, column: 45, scope: !1550)
!1571 = !DILocation(line: 434, column: 57, scope: !1550)
!1572 = !DILocation(line: 434, column: 68, scope: !1550)
!1573 = !DILocation(line: 437, column: 2, scope: !1550)
!1574 = !{!1575, !1575, i64 0}
!1575 = !{!"long long", !998, i64 0}
!1576 = !DILocation(line: 437, column: 2, scope: !1577)
!1577 = !DILexicalBlockFile(scope: !1550, file: !3, discriminator: 2)
!1578 = !DILocation(line: 78, column: 3, scope: !1387, inlinedAt: !1579)
!1579 = distinct !DILocation(line: 440, column: 8, scope: !1580)
!1580 = distinct !DILexicalBlock(scope: !1550, file: !3, line: 440, column: 7)
!1581 = !DILocation(line: 79, column: 3, scope: !1425, inlinedAt: !1582)
!1582 = distinct !DILocation(line: 440, column: 21, scope: !1583)
!1583 = !DILexicalBlockFile(scope: !1580, file: !3, discriminator: 2)
!1584 = !DILocation(line: 100, column: 3, scope: !1585, inlinedAt: !1627)
!1585 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_xEv", scope: !1586, file: !1347, line: 100, type: !1351, isLocal: false, isDefinition: true, scopeLine: 100, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1588, variables: !4)
!1586 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_gridDim_t", file: !1347, line: 99, size: 8, elements: !1587, identifier: "_ZTS24__cuda_builtin_gridDim_t")
!1587 = !{!1588, !1589, !1590, !1591, !1612, !1616, !1620, !1623}
!1588 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_xEv", scope: !1586, file: !1347, line: 100, type: !1351, isLocal: false, isDefinition: false, scopeLine: 100, flags: DIFlagPrototyped, isOptimized: true)
!1589 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_yEv", scope: !1586, file: !1347, line: 101, type: !1351, isLocal: false, isDefinition: false, scopeLine: 101, flags: DIFlagPrototyped, isOptimized: true)
!1590 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_zEv", scope: !1586, file: !1347, line: 102, type: !1351, isLocal: false, isDefinition: false, scopeLine: 102, flags: DIFlagPrototyped, isOptimized: true)
!1591 = !DISubprogram(name: "operator dim3", linkageName: "_ZNK24__cuda_builtin_gridDim_tcv4dim3Ev", scope: !1586, file: !1347, line: 105, type: !1592, isLocal: false, isDefinition: false, scopeLine: 105, flags: DIFlagPrototyped, isOptimized: true)
!1592 = !DISubroutineType(types: !1593)
!1593 = !{!1594, !1610}
!1594 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "dim3", file: !1359, line: 417, size: 96, elements: !1595, identifier: "_ZTS4dim3")
!1595 = !{!1596, !1597, !1598, !1599, !1603, !1607}
!1596 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !1594, file: !1359, line: 419, baseType: !436, size: 32)
!1597 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !1594, file: !1359, line: 419, baseType: !436, size: 32, offset: 32)
!1598 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !1594, file: !1359, line: 419, baseType: !436, size: 32, offset: 64)
!1599 = !DISubprogram(name: "dim3", scope: !1594, file: !1359, line: 421, type: !1600, isLocal: false, isDefinition: false, scopeLine: 421, flags: DIFlagPrototyped, isOptimized: true)
!1600 = !DISubroutineType(types: !1601)
!1601 = !{null, !1602, !436, !436, !436}
!1602 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1594, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1603 = !DISubprogram(name: "dim3", scope: !1594, file: !1359, line: 422, type: !1604, isLocal: false, isDefinition: false, scopeLine: 422, flags: DIFlagPrototyped, isOptimized: true)
!1604 = !DISubroutineType(types: !1605)
!1605 = !{null, !1602, !1606}
!1606 = !DIDerivedType(tag: DW_TAG_typedef, name: "uint3", file: !1359, line: 383, baseType: !1358)
!1607 = !DISubprogram(name: "operator uint3", linkageName: "_ZN4dim3cv5uint3Ev", scope: !1594, file: !1359, line: 423, type: !1608, isLocal: false, isDefinition: false, scopeLine: 423, flags: DIFlagPrototyped, isOptimized: true)
!1608 = !DISubroutineType(types: !1609)
!1609 = !{!1606, !1602}
!1610 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1611, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1611 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1586)
!1612 = !DISubprogram(name: "__cuda_builtin_gridDim_t", scope: !1586, file: !1347, line: 107, type: !1613, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1613 = !DISubroutineType(types: !1614)
!1614 = !{null, !1615}
!1615 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1586, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1616 = !DISubprogram(name: "__cuda_builtin_gridDim_t", scope: !1586, file: !1347, line: 107, type: !1617, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1617 = !DISubroutineType(types: !1618)
!1618 = !{null, !1615, !1619}
!1619 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1611, size: 64)
!1620 = !DISubprogram(name: "operator=", linkageName: "_ZNK24__cuda_builtin_gridDim_taSERKS_", scope: !1586, file: !1347, line: 107, type: !1621, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1621 = !DISubroutineType(types: !1622)
!1622 = !{null, !1610, !1619}
!1623 = !DISubprogram(name: "operator&", linkageName: "_ZNK24__cuda_builtin_gridDim_tadEv", scope: !1586, file: !1347, line: 107, type: !1624, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1624 = !DISubroutineType(types: !1625)
!1625 = !{!1626, !1610}
!1626 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1586, size: 64)
!1627 = distinct !DILocation(line: 440, column: 32, scope: !1628)
!1628 = !DILexicalBlockFile(scope: !1580, file: !3, discriminator: 3)
!1629 = !{i32 1, i32 65536}
!1630 = !DILocation(line: 440, column: 31, scope: !1580)
!1631 = !DILocation(line: 440, column: 19, scope: !1580)
!1632 = !DILocation(line: 440, column: 45, scope: !1580)
!1633 = !DILocation(line: 440, column: 43, scope: !1580)
!1634 = !DILocation(line: 440, column: 51, scope: !1580)
!1635 = !DILocation(line: 440, column: 92, scope: !1636)
!1636 = !DILexicalBlockFile(scope: !1580, file: !3, discriminator: 1)
!1637 = !DILocation(line: 440, column: 90, scope: !1636)
!1638 = !DILocation(line: 440, column: 7, scope: !1639)
!1639 = !DILexicalBlockFile(scope: !1550, file: !3, discriminator: 1)
!1640 = !DILocation(line: 446, column: 8, scope: !1550)
!1641 = !DILocalVariable(name: "val", arg: 2, scope: !1642, file: !1643, line: 201, type: !20)
!1642 = distinct !DISubprogram(name: "atomicAdd", linkageName: "_ZL9atomicAddPyy", scope: !1643, file: !1643, line: 201, type: !1644, isLocal: true, isDefinition: true, scopeLine: 202, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1647)
!1643 = !DIFile(filename: "/usr/local/cuda/include/device_atomic_functions.hpp", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!1644 = !DISubroutineType(types: !1645)
!1645 = !{!20, !1646, !20}
!1646 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !20, size: 64)
!1647 = !{!1648, !1641}
!1648 = !DILocalVariable(name: "address", arg: 1, scope: !1642, file: !1643, line: 201, type: !1646)
!1649 = !DILocation(line: 201, column: 123, scope: !1642, inlinedAt: !1650)
!1650 = distinct !DILocation(line: 448, column: 12, scope: !1550)
!1651 = !DILocalVariable(name: "val", arg: 2, scope: !1652, file: !533, line: 1524, type: !20)
!1652 = distinct !DISubprogram(name: "__ullAtomicAdd", linkageName: "_ZL14__ullAtomicAddPyy", scope: !533, file: !533, line: 1524, type: !1644, isLocal: true, isDefinition: true, scopeLine: 1525, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1653)
!1653 = !{!1654, !1651}
!1654 = !DILocalVariable(name: "p", arg: 1, scope: !1652, file: !533, line: 1524, type: !1646)
!1655 = !DILocation(line: 1524, column: 77, scope: !1652, inlinedAt: !1656)
!1656 = distinct !DILocation(line: 203, column: 10, scope: !1642, inlinedAt: !1650)
!1657 = !DILocation(line: 1526, column: 10, scope: !1652, inlinedAt: !1656)
!1658 = !DILocation(line: 459, column: 21, scope: !1550)
!1659 = !DILocation(line: 460, column: 6, scope: !1550)
!1660 = !DILocation(line: 461, column: 12, scope: !1550)
!1661 = !DILocation(line: 462, column: 17, scope: !1564)
!1662 = !DILocation(line: 462, column: 23, scope: !1663)
!1663 = !DILexicalBlockFile(scope: !1567, file: !3, discriminator: 1)
!1664 = !DILocation(line: 462, column: 32, scope: !1663)
!1665 = !DILocation(line: 462, column: 9, scope: !1666)
!1666 = !DILexicalBlockFile(scope: !1564, file: !3, discriminator: 1)
!1667 = !DILocation(line: 465, column: 16, scope: !1668)
!1668 = distinct !DILexicalBlock(scope: !1566, file: !3, line: 465, column: 7)
!1669 = !DILocation(line: 475, column: 11, scope: !1550)
!1670 = !DILocation(line: 476, column: 2, scope: !1550)
!1671 = !DILocation(line: 476, column: 13, scope: !1550)
!1672 = !DILocation(line: 476, column: 17, scope: !1550)
!1673 = !{!1674, !1575, i64 8}
!1674 = !{!"_ZTS7BBlog_t", !1143, i64 0, !1143, i64 2, !1143, i64 4, !1143, i64 6, !1575, i64 8, !1055, i64 16, !1055, i64 20, !1055, i64 24}
!1675 = !DILocation(line: 67, column: 3, scope: !1346, inlinedAt: !1676)
!1676 = distinct !DILocation(line: 478, column: 27, scope: !1550)
!1677 = !DILocation(line: 478, column: 27, scope: !1550)
!1678 = !DILocation(line: 478, column: 20, scope: !1550)
!1679 = !DILocation(line: 478, column: 25, scope: !1550)
!1680 = !{!1674, !1143, i64 4}
!1681 = !DILocation(line: 68, column: 3, scope: !1419, inlinedAt: !1682)
!1682 = distinct !DILocation(line: 479, column: 27, scope: !1550)
!1683 = !DILocation(line: 479, column: 27, scope: !1550)
!1684 = !DILocation(line: 479, column: 20, scope: !1550)
!1685 = !DILocation(line: 479, column: 25, scope: !1550)
!1686 = !{!1674, !1143, i64 6}
!1687 = !DILocation(line: 480, column: 27, scope: !1550)
!1688 = !DILocation(line: 480, column: 20, scope: !1550)
!1689 = !DILocation(line: 480, column: 25, scope: !1550)
!1690 = !{!1674, !1143, i64 0}
!1691 = !DILocation(line: 481, column: 27, scope: !1550)
!1692 = !DILocation(line: 481, column: 20, scope: !1550)
!1693 = !DILocation(line: 481, column: 25, scope: !1550)
!1694 = !{!1674, !1143, i64 2}
!1695 = !DILocation(line: 482, column: 20, scope: !1550)
!1696 = !DILocation(line: 482, column: 26, scope: !1550)
!1697 = !{!1674, !1055, i64 16}
!1698 = !DILocation(line: 483, column: 20, scope: !1550)
!1699 = !DILocation(line: 483, column: 26, scope: !1550)
!1700 = !{!1674, !1055, i64 20}
!1701 = !DILocation(line: 486, column: 13, scope: !1550)
!1702 = !DILocation(line: 486, column: 17, scope: !1550)
!1703 = !{!1674, !1055, i64 24}
!1704 = !DILocation(line: 467, column: 10, scope: !1566)
!1705 = !DILocation(line: 467, column: 15, scope: !1566)
!1706 = !DILocation(line: 467, column: 7, scope: !1566)
!1707 = !DILocation(line: 468, column: 13, scope: !1566)
!1708 = !DILocation(line: 468, column: 10, scope: !1566)
!1709 = !DILocation(line: 471, column: 9, scope: !1567)
!1710 = !DILocation(line: 462, column: 39, scope: !1711)
!1711 = !DILexicalBlockFile(scope: !1567, file: !3, discriminator: 3)
!1712 = !DILocation(line: 462, column: 28, scope: !1663)
!1713 = distinct !{!1713, !1714, !1715}
!1714 = !DILocation(line: 462, column: 9, scope: !1564)
!1715 = !DILocation(line: 471, column: 9, scope: !1564)
!1716 = !DILocation(line: 493, column: 1, scope: !1639)
!1717 = distinct !DISubprogram(name: "storeLines", linkageName: "_Z10storeLinesPvssss", scope: !3, file: !3, line: 496, type: !1718, isLocal: false, isDefinition: true, scopeLine: 497, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1720)
!1718 = !DISubroutineType(types: !1719)
!1719 = !{null, !364, !15, !15, !15, !15}
!1720 = !{!1721, !1722, !1723, !1724, !1725, !1726, !1727, !1728}
!1721 = !DILocalVariable(name: "p", arg: 1, scope: !1717, file: !3, line: 496, type: !364)
!1722 = !DILocalVariable(name: "size", arg: 2, scope: !1717, file: !3, line: 496, type: !15)
!1723 = !DILocalVariable(name: "line", arg: 3, scope: !1717, file: !3, line: 496, type: !15)
!1724 = !DILocalVariable(name: "colmn", arg: 4, scope: !1717, file: !3, line: 496, type: !15)
!1725 = !DILocalVariable(name: "op", arg: 5, scope: !1717, file: !3, line: 496, type: !15)
!1726 = !DILocalVariable(name: "bid", scope: !1717, file: !3, line: 500, type: !8)
!1727 = !DILocalVariable(name: "buffer_oN_DeViCe_short", scope: !1717, file: !3, line: 508, type: !24)
!1728 = !DILocalVariable(name: "buffer_oN_DeViCe_long", scope: !1717, file: !3, line: 509, type: !25)
!1729 = !DILocation(line: 496, column: 34, scope: !1717)
!1730 = !DILocation(line: 496, column: 43, scope: !1717)
!1731 = !DILocation(line: 496, column: 64, scope: !1717)
!1732 = !DILocation(line: 496, column: 76, scope: !1717)
!1733 = !DILocation(line: 496, column: 89, scope: !1717)
!1734 = !DILocation(line: 498, column: 2, scope: !1717)
!1735 = !DILocation(line: 498, column: 2, scope: !1736)
!1736 = !DILexicalBlockFile(scope: !1717, file: !3, discriminator: 2)
!1737 = !DILocation(line: 201, column: 123, scope: !1642, inlinedAt: !1738)
!1738 = distinct !DILocation(line: 500, column: 12, scope: !1717)
!1739 = !DILocation(line: 1524, column: 77, scope: !1652, inlinedAt: !1740)
!1740 = distinct !DILocation(line: 203, column: 10, scope: !1642, inlinedAt: !1738)
!1741 = !DILocation(line: 1526, column: 10, scope: !1652, inlinedAt: !1740)
!1742 = !DILocation(line: 500, column: 12, scope: !1717)
!1743 = !DILocation(line: 500, column: 6, scope: !1717)
!1744 = !DILocation(line: 508, column: 9, scope: !1717)
!1745 = !DILocation(line: 509, column: 8, scope: !1717)
!1746 = !DILocation(line: 78, column: 3, scope: !1387, inlinedAt: !1747)
!1747 = distinct !DILocation(line: 511, column: 37, scope: !1717)
!1748 = !DILocation(line: 511, column: 37, scope: !1717)
!1749 = !DILocation(line: 511, column: 28, scope: !1717)
!1750 = !DILocation(line: 511, column: 2, scope: !1717)
!1751 = !DILocation(line: 511, column: 35, scope: !1717)
!1752 = !{!1143, !1143, i64 0}
!1753 = !DILocation(line: 79, column: 3, scope: !1425, inlinedAt: !1754)
!1754 = distinct !DILocation(line: 512, column: 37, scope: !1717)
!1755 = !DILocation(line: 512, column: 37, scope: !1717)
!1756 = !DILocation(line: 512, column: 31, scope: !1717)
!1757 = !DILocation(line: 512, column: 2, scope: !1717)
!1758 = !DILocation(line: 512, column: 35, scope: !1717)
!1759 = !DILocation(line: 67, column: 3, scope: !1346, inlinedAt: !1760)
!1760 = distinct !DILocation(line: 513, column: 37, scope: !1717)
!1761 = !DILocation(line: 513, column: 37, scope: !1717)
!1762 = !DILocation(line: 513, column: 31, scope: !1717)
!1763 = !DILocation(line: 513, column: 2, scope: !1717)
!1764 = !DILocation(line: 513, column: 35, scope: !1717)
!1765 = !DILocation(line: 68, column: 3, scope: !1419, inlinedAt: !1766)
!1766 = distinct !DILocation(line: 514, column: 37, scope: !1717)
!1767 = !DILocation(line: 514, column: 37, scope: !1717)
!1768 = !DILocation(line: 514, column: 31, scope: !1717)
!1769 = !DILocation(line: 514, column: 2, scope: !1717)
!1770 = !DILocation(line: 514, column: 35, scope: !1717)
!1771 = !DILocation(line: 515, column: 35, scope: !1717)
!1772 = !DILocation(line: 515, column: 2, scope: !1717)
!1773 = !DILocation(line: 515, column: 33, scope: !1717)
!1774 = !{!1775, !1775, i64 0}
!1775 = !{!"long", !998, i64 0}
!1776 = !DILocation(line: 516, column: 31, scope: !1717)
!1777 = !DILocation(line: 516, column: 2, scope: !1717)
!1778 = !DILocation(line: 516, column: 35, scope: !1717)
!1779 = !DILocation(line: 517, column: 31, scope: !1717)
!1780 = !DILocation(line: 517, column: 2, scope: !1717)
!1781 = !DILocation(line: 517, column: 35, scope: !1717)
!1782 = !DILocation(line: 518, column: 31, scope: !1717)
!1783 = !DILocation(line: 518, column: 2, scope: !1717)
!1784 = !DILocation(line: 518, column: 36, scope: !1717)
!1785 = !DILocation(line: 519, column: 31, scope: !1717)
!1786 = !DILocation(line: 519, column: 2, scope: !1717)
!1787 = !DILocation(line: 519, column: 36, scope: !1717)
!1788 = !DILocation(line: 526, column: 10, scope: !1789)
!1789 = distinct !DILexicalBlock(scope: !1717, file: !3, line: 526, column: 6)
!1790 = !DILocation(line: 526, column: 6, scope: !1717)
!1791 = !DILocation(line: 527, column: 3, scope: !1789)
!1792 = !DILocation(line: 531, column: 1, scope: !1717)
!1793 = distinct !DISubprogram(name: "dumpLines", linkageName: "_Z9dumpLinesv", scope: !3, file: !3, line: 533, type: !337, isLocal: false, isDefinition: true, scopeLine: 534, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1794)
!1794 = !{!1795}
!1795 = !DILocalVariable(name: "ii", scope: !1793, file: !3, line: 537, type: !8)
!1796 = !DILocation(line: 67, column: 3, scope: !1346, inlinedAt: !1797)
!1797 = distinct !DILocation(line: 535, column: 6, scope: !1798)
!1798 = distinct !DILexicalBlock(scope: !1793, file: !3, line: 535, column: 6)
!1799 = !DILocation(line: 535, column: 18, scope: !1798)
!1800 = !DILocation(line: 535, column: 23, scope: !1798)
!1801 = !DILocation(line: 78, column: 3, scope: !1387, inlinedAt: !1802)
!1802 = distinct !DILocation(line: 535, column: 26, scope: !1803)
!1803 = !DILexicalBlockFile(scope: !1798, file: !3, discriminator: 1)
!1804 = !DILocation(line: 535, column: 37, scope: !1803)
!1805 = !DILocation(line: 535, column: 42, scope: !1803)
!1806 = !DILocation(line: 68, column: 3, scope: !1419, inlinedAt: !1807)
!1807 = distinct !DILocation(line: 535, column: 45, scope: !1808)
!1808 = !DILexicalBlockFile(scope: !1798, file: !3, discriminator: 2)
!1809 = !DILocation(line: 535, column: 57, scope: !1808)
!1810 = !DILocation(line: 535, column: 62, scope: !1808)
!1811 = !DILocation(line: 79, column: 3, scope: !1425, inlinedAt: !1812)
!1812 = distinct !DILocation(line: 535, column: 65, scope: !1813)
!1813 = !DILexicalBlockFile(scope: !1798, file: !3, discriminator: 3)
!1814 = !DILocation(line: 535, column: 76, scope: !1813)
!1815 = !DILocation(line: 535, column: 6, scope: !1816)
!1816 = !DILexicalBlockFile(scope: !1793, file: !3, discriminator: 3)
!1817 = !DILocation(line: 542, column: 2, scope: !1793)
!1818 = !DILocation(line: 553, column: 1, scope: !1793)
!1819 = !DILocation(line: 553, column: 1, scope: !1820)
!1820 = !DILexicalBlockFile(scope: !1793, file: !3, discriminator: 1)
!1821 = distinct !DISubprogram(name: "print1", linkageName: "_Z6print1i", scope: !3, file: !3, line: 555, type: !384, isLocal: false, isDefinition: true, scopeLine: 556, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1822)
!1822 = !{!1823}
!1823 = !DILocalVariable(name: "a", arg: 1, scope: !1821, file: !3, line: 555, type: !8)
!1824 = !DILocation(line: 555, column: 28, scope: !1821)
!1825 = !DILocation(line: 67, column: 3, scope: !1346, inlinedAt: !1826)
!1826 = distinct !DILocation(line: 557, column: 6, scope: !1827)
!1827 = distinct !DILexicalBlock(scope: !1821, file: !3, line: 557, column: 6)
!1828 = !DILocation(line: 68, column: 3, scope: !1419, inlinedAt: !1829)
!1829 = distinct !DILocation(line: 557, column: 20, scope: !1830)
!1830 = !DILexicalBlockFile(scope: !1827, file: !3, discriminator: 2)
!1831 = !DILocation(line: 557, column: 18, scope: !1827)
!1832 = !DILocation(line: 78, column: 3, scope: !1387, inlinedAt: !1833)
!1833 = distinct !DILocation(line: 557, column: 34, scope: !1834)
!1834 = !DILexicalBlockFile(scope: !1827, file: !3, discriminator: 3)
!1835 = !DILocation(line: 557, column: 32, scope: !1827)
!1836 = !DILocation(line: 79, column: 3, scope: !1425, inlinedAt: !1837)
!1837 = distinct !DILocation(line: 557, column: 47, scope: !1838)
!1838 = !DILexicalBlockFile(scope: !1827, file: !3, discriminator: 4)
!1839 = !DILocation(line: 557, column: 58, scope: !1827)
!1840 = !DILocation(line: 557, column: 63, scope: !1827)
!1841 = !DILocation(line: 557, column: 66, scope: !1842)
!1842 = !DILexicalBlockFile(scope: !1827, file: !3, discriminator: 1)
!1843 = !DILocation(line: 557, column: 6, scope: !1844)
!1844 = !DILexicalBlockFile(scope: !1821, file: !3, discriminator: 1)
!1845 = !DILocation(line: 559, column: 7, scope: !1846)
!1846 = distinct !DILexicalBlock(scope: !1827, file: !3, line: 558, column: 2)
!1847 = !DILocation(line: 560, column: 4, scope: !1848)
!1848 = distinct !DILexicalBlock(scope: !1846, file: !3, line: 559, column: 7)
!1849 = !DILocation(line: 560, column: 4, scope: !1850)
!1850 = !DILexicalBlockFile(scope: !1848, file: !3, discriminator: 2)
!1851 = !DILocation(line: 562, column: 4, scope: !1852)
!1852 = distinct !DILexicalBlock(scope: !1848, file: !3, line: 561, column: 12)
!1853 = !DILocation(line: 562, column: 4, scope: !1854)
!1854 = !DILexicalBlockFile(scope: !1852, file: !3, discriminator: 2)
!1855 = !DILocation(line: 564, column: 4, scope: !1852)
!1856 = !DILocation(line: 566, column: 1, scope: !1821)
!1857 = distinct !DISubprogram(name: "print2", linkageName: "_Z6print2v", scope: !3, file: !3, line: 568, type: !337, isLocal: false, isDefinition: true, scopeLine: 569, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !4)
!1858 = !DILocation(line: 67, column: 3, scope: !1346, inlinedAt: !1859)
!1859 = distinct !DILocation(line: 570, column: 6, scope: !1860)
!1860 = distinct !DILexicalBlock(scope: !1857, file: !3, line: 570, column: 6)
!1861 = !DILocation(line: 68, column: 3, scope: !1419, inlinedAt: !1862)
!1862 = distinct !DILocation(line: 570, column: 20, scope: !1863)
!1863 = !DILexicalBlockFile(scope: !1860, file: !3, discriminator: 2)
!1864 = !DILocation(line: 570, column: 18, scope: !1860)
!1865 = !DILocation(line: 78, column: 3, scope: !1387, inlinedAt: !1866)
!1866 = distinct !DILocation(line: 570, column: 34, scope: !1867)
!1867 = !DILexicalBlockFile(scope: !1860, file: !3, discriminator: 3)
!1868 = !DILocation(line: 570, column: 32, scope: !1860)
!1869 = !DILocation(line: 79, column: 3, scope: !1425, inlinedAt: !1870)
!1870 = distinct !DILocation(line: 570, column: 47, scope: !1871)
!1871 = !DILexicalBlockFile(scope: !1860, file: !3, discriminator: 4)
!1872 = !DILocation(line: 570, column: 58, scope: !1860)
!1873 = !DILocation(line: 570, column: 63, scope: !1860)
!1874 = !DILocation(line: 570, column: 66, scope: !1875)
!1875 = !DILexicalBlockFile(scope: !1860, file: !3, discriminator: 1)
!1876 = !DILocation(line: 570, column: 6, scope: !1877)
!1877 = !DILexicalBlockFile(scope: !1857, file: !3, discriminator: 1)
!1878 = !DILocation(line: 571, column: 10, scope: !1860)
!1879 = !DILocation(line: 571, column: 10, scope: !1863)
!1880 = !DILocation(line: 572, column: 1, scope: !1857)
!1881 = distinct !DISubprogram(name: "print3", linkageName: "_Z6print3ii", scope: !3, file: !3, line: 574, type: !1882, isLocal: false, isDefinition: true, scopeLine: 575, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1884)
!1882 = !DISubroutineType(types: !1883)
!1883 = !{null, !8, !8}
!1884 = !{!1885, !1886}
!1885 = !DILocalVariable(name: "line", arg: 1, scope: !1881, file: !3, line: 574, type: !8)
!1886 = !DILocalVariable(name: "col", arg: 2, scope: !1881, file: !3, line: 574, type: !8)
!1887 = !DILocation(line: 574, column: 28, scope: !1881)
!1888 = !DILocation(line: 574, column: 38, scope: !1881)
!1889 = !DILocation(line: 67, column: 3, scope: !1346, inlinedAt: !1890)
!1890 = distinct !DILocation(line: 576, column: 6, scope: !1891)
!1891 = distinct !DILexicalBlock(scope: !1881, file: !3, line: 576, column: 6)
!1892 = !DILocation(line: 68, column: 3, scope: !1419, inlinedAt: !1893)
!1893 = distinct !DILocation(line: 576, column: 20, scope: !1894)
!1894 = !DILexicalBlockFile(scope: !1891, file: !3, discriminator: 2)
!1895 = !DILocation(line: 576, column: 18, scope: !1891)
!1896 = !DILocation(line: 78, column: 3, scope: !1387, inlinedAt: !1897)
!1897 = distinct !DILocation(line: 576, column: 34, scope: !1898)
!1898 = !DILexicalBlockFile(scope: !1891, file: !3, discriminator: 3)
!1899 = !DILocation(line: 576, column: 32, scope: !1891)
!1900 = !DILocation(line: 79, column: 3, scope: !1425, inlinedAt: !1901)
!1901 = distinct !DILocation(line: 576, column: 47, scope: !1902)
!1902 = !DILexicalBlockFile(scope: !1891, file: !3, discriminator: 4)
!1903 = !DILocation(line: 576, column: 58, scope: !1891)
!1904 = !DILocation(line: 576, column: 63, scope: !1891)
!1905 = !DILocation(line: 576, column: 66, scope: !1906)
!1906 = !DILexicalBlockFile(scope: !1891, file: !3, discriminator: 1)
!1907 = !DILocation(line: 576, column: 6, scope: !1908)
!1908 = !DILexicalBlockFile(scope: !1881, file: !3, discriminator: 1)
!1909 = !DILocation(line: 577, column: 10, scope: !1891)
!1910 = !DILocation(line: 577, column: 10, scope: !1894)
!1911 = !DILocation(line: 578, column: 1, scope: !1881)
!1912 = distinct !DISubprogram(name: "print5", scope: !3, file: !3, line: 580, type: !1913, isLocal: false, isDefinition: true, scopeLine: 581, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1915)
!1913 = !DISubroutineType(types: !1914)
!1914 = !{null, !364, !8, !8, !8, !8}
!1915 = !{!1916, !1917, !1918, !1919, !1920}
!1916 = !DILocalVariable(name: "p", arg: 1, scope: !1912, file: !3, line: 580, type: !364)
!1917 = !DILocalVariable(name: "bits", arg: 2, scope: !1912, file: !3, line: 580, type: !8)
!1918 = !DILocalVariable(name: "sline", arg: 3, scope: !1912, file: !3, line: 580, type: !8)
!1919 = !DILocalVariable(name: "scolm", arg: 4, scope: !1912, file: !3, line: 580, type: !8)
!1920 = !DILocalVariable(name: "op", arg: 5, scope: !1912, file: !3, line: 580, type: !8)
!1921 = !DILocation(line: 580, column: 30, scope: !1912)
!1922 = !DILocation(line: 580, column: 37, scope: !1912)
!1923 = !DILocation(line: 580, column: 47, scope: !1912)
!1924 = !DILocation(line: 580, column: 58, scope: !1912)
!1925 = !DILocation(line: 580, column: 69, scope: !1912)
!1926 = !DILocation(line: 78, column: 3, scope: !1387, inlinedAt: !1927)
!1927 = distinct !DILocation(line: 587, column: 8, scope: !1928)
!1928 = distinct !DILexicalBlock(scope: !1912, file: !3, line: 587, column: 7)
!1929 = !DILocation(line: 79, column: 3, scope: !1425, inlinedAt: !1930)
!1930 = distinct !DILocation(line: 587, column: 21, scope: !1931)
!1931 = !DILexicalBlockFile(scope: !1928, file: !3, discriminator: 2)
!1932 = !DILocation(line: 100, column: 3, scope: !1585, inlinedAt: !1933)
!1933 = distinct !DILocation(line: 587, column: 32, scope: !1934)
!1934 = !DILexicalBlockFile(scope: !1928, file: !3, discriminator: 3)
!1935 = !DILocation(line: 587, column: 31, scope: !1928)
!1936 = !DILocation(line: 587, column: 19, scope: !1928)
!1937 = !DILocation(line: 587, column: 45, scope: !1928)
!1938 = !DILocation(line: 587, column: 43, scope: !1928)
!1939 = !DILocation(line: 587, column: 51, scope: !1928)
!1940 = !DILocation(line: 587, column: 92, scope: !1941)
!1941 = !DILexicalBlockFile(scope: !1928, file: !3, discriminator: 1)
!1942 = !DILocation(line: 587, column: 90, scope: !1941)
!1943 = !DILocation(line: 587, column: 7, scope: !1944)
!1944 = !DILexicalBlockFile(scope: !1912, file: !3, discriminator: 1)
!1945 = !DILocation(line: 496, column: 34, scope: !1717, inlinedAt: !1946)
!1946 = distinct !DILocation(line: 590, column: 9, scope: !1912)
!1947 = !DILocation(line: 496, column: 43, scope: !1717, inlinedAt: !1946)
!1948 = !DILocation(line: 496, column: 64, scope: !1717, inlinedAt: !1946)
!1949 = !DILocation(line: 496, column: 76, scope: !1717, inlinedAt: !1946)
!1950 = !DILocation(line: 496, column: 89, scope: !1717, inlinedAt: !1946)
!1951 = !DILocation(line: 498, column: 2, scope: !1717, inlinedAt: !1946)
!1952 = !DILocation(line: 498, column: 2, scope: !1736, inlinedAt: !1946)
!1953 = !DILocation(line: 590, column: 76, scope: !1912)
!1954 = !DILocation(line: 590, column: 62, scope: !1912)
!1955 = !DILocation(line: 590, column: 47, scope: !1912)
!1956 = !DILocation(line: 590, column: 35, scope: !1912)
!1957 = !DILocation(line: 590, column: 30, scope: !1912)
!1958 = !DILocation(line: 201, column: 123, scope: !1642, inlinedAt: !1959)
!1959 = distinct !DILocation(line: 500, column: 12, scope: !1717, inlinedAt: !1946)
!1960 = !DILocation(line: 1524, column: 77, scope: !1652, inlinedAt: !1961)
!1961 = distinct !DILocation(line: 203, column: 10, scope: !1642, inlinedAt: !1959)
!1962 = !DILocation(line: 1526, column: 10, scope: !1652, inlinedAt: !1961)
!1963 = !DILocation(line: 500, column: 12, scope: !1717, inlinedAt: !1946)
!1964 = !DILocation(line: 500, column: 6, scope: !1717, inlinedAt: !1946)
!1965 = !DILocation(line: 508, column: 9, scope: !1717, inlinedAt: !1946)
!1966 = !DILocation(line: 509, column: 8, scope: !1717, inlinedAt: !1946)
!1967 = !DILocation(line: 511, column: 37, scope: !1717, inlinedAt: !1946)
!1968 = !DILocation(line: 511, column: 28, scope: !1717, inlinedAt: !1946)
!1969 = !DILocation(line: 511, column: 2, scope: !1717, inlinedAt: !1946)
!1970 = !DILocation(line: 511, column: 35, scope: !1717, inlinedAt: !1946)
!1971 = !DILocation(line: 512, column: 37, scope: !1717, inlinedAt: !1946)
!1972 = !DILocation(line: 512, column: 31, scope: !1717, inlinedAt: !1946)
!1973 = !DILocation(line: 512, column: 2, scope: !1717, inlinedAt: !1946)
!1974 = !DILocation(line: 512, column: 35, scope: !1717, inlinedAt: !1946)
!1975 = !DILocation(line: 67, column: 3, scope: !1346, inlinedAt: !1976)
!1976 = distinct !DILocation(line: 513, column: 37, scope: !1717, inlinedAt: !1946)
!1977 = !DILocation(line: 513, column: 37, scope: !1717, inlinedAt: !1946)
!1978 = !DILocation(line: 513, column: 31, scope: !1717, inlinedAt: !1946)
!1979 = !DILocation(line: 513, column: 2, scope: !1717, inlinedAt: !1946)
!1980 = !DILocation(line: 513, column: 35, scope: !1717, inlinedAt: !1946)
!1981 = !DILocation(line: 68, column: 3, scope: !1419, inlinedAt: !1982)
!1982 = distinct !DILocation(line: 514, column: 37, scope: !1717, inlinedAt: !1946)
!1983 = !DILocation(line: 514, column: 37, scope: !1717, inlinedAt: !1946)
!1984 = !DILocation(line: 514, column: 31, scope: !1717, inlinedAt: !1946)
!1985 = !DILocation(line: 514, column: 2, scope: !1717, inlinedAt: !1946)
!1986 = !DILocation(line: 514, column: 35, scope: !1717, inlinedAt: !1946)
!1987 = !DILocation(line: 515, column: 35, scope: !1717, inlinedAt: !1946)
!1988 = !DILocation(line: 515, column: 2, scope: !1717, inlinedAt: !1946)
!1989 = !DILocation(line: 515, column: 33, scope: !1717, inlinedAt: !1946)
!1990 = !DILocation(line: 516, column: 31, scope: !1717, inlinedAt: !1946)
!1991 = !DILocation(line: 516, column: 2, scope: !1717, inlinedAt: !1946)
!1992 = !DILocation(line: 516, column: 35, scope: !1717, inlinedAt: !1946)
!1993 = !DILocation(line: 517, column: 31, scope: !1717, inlinedAt: !1946)
!1994 = !DILocation(line: 517, column: 2, scope: !1717, inlinedAt: !1946)
!1995 = !DILocation(line: 517, column: 35, scope: !1717, inlinedAt: !1946)
!1996 = !DILocation(line: 518, column: 31, scope: !1717, inlinedAt: !1946)
!1997 = !DILocation(line: 518, column: 2, scope: !1717, inlinedAt: !1946)
!1998 = !DILocation(line: 518, column: 36, scope: !1717, inlinedAt: !1946)
!1999 = !DILocation(line: 519, column: 31, scope: !1717, inlinedAt: !1946)
!2000 = !DILocation(line: 519, column: 2, scope: !1717, inlinedAt: !1946)
!2001 = !DILocation(line: 519, column: 36, scope: !1717, inlinedAt: !1946)
!2002 = !DILocation(line: 526, column: 10, scope: !1789, inlinedAt: !1946)
!2003 = !DILocation(line: 526, column: 6, scope: !1717, inlinedAt: !1946)
!2004 = !DILocation(line: 527, column: 3, scope: !1789, inlinedAt: !1946)
!2005 = !DILocation(line: 531, column: 1, scope: !1717, inlinedAt: !1946)
!2006 = !DILocation(line: 594, column: 1, scope: !1912)
!2007 = !DILocation(line: 594, column: 1, scope: !1944)
!2008 = distinct !DISubprogram(name: "RetKernel", scope: !3, file: !3, line: 597, type: !337, isLocal: false, isDefinition: true, scopeLine: 598, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !2009)
!2009 = !{!2010, !2015, !2016, !2017}
!2010 = !DILocalVariable(name: "bbbuffer_oN_DeViCe_short", scope: !2011, file: !3, line: 617, type: !9)
!2011 = distinct !DILexicalBlock(scope: !2012, file: !3, line: 615, column: 3)
!2012 = distinct !DILexicalBlock(scope: !2013, file: !3, line: 601, column: 7)
!2013 = distinct !DILexicalBlock(scope: !2014, file: !3, line: 600, column: 2)
!2014 = distinct !DILexicalBlock(scope: !2008, file: !3, line: 599, column: 6)
!2015 = !DILocalVariable(name: "offset1", scope: !2013, file: !3, line: 643, type: !369)
!2016 = !DILocalVariable(name: "offset2", scope: !2013, file: !3, line: 644, type: !369)
!2017 = !DILocalVariable(name: "ptr", scope: !2013, file: !3, line: 654, type: !364)
!2018 = !DILocation(line: 67, column: 3, scope: !1346, inlinedAt: !2019)
!2019 = distinct !DILocation(line: 599, column: 6, scope: !2014)
!2020 = !DILocation(line: 68, column: 3, scope: !1419, inlinedAt: !2021)
!2021 = distinct !DILocation(line: 599, column: 20, scope: !2022)
!2022 = !DILexicalBlockFile(scope: !2014, file: !3, discriminator: 1)
!2023 = !DILocation(line: 599, column: 18, scope: !2014)
!2024 = !DILocation(line: 78, column: 3, scope: !1387, inlinedAt: !2025)
!2025 = distinct !DILocation(line: 599, column: 34, scope: !2026)
!2026 = !DILexicalBlockFile(scope: !2014, file: !3, discriminator: 2)
!2027 = !DILocation(line: 599, column: 32, scope: !2014)
!2028 = !DILocation(line: 79, column: 3, scope: !1425, inlinedAt: !2029)
!2029 = distinct !DILocation(line: 599, column: 47, scope: !2030)
!2030 = !DILexicalBlockFile(scope: !2014, file: !3, discriminator: 3)
!2031 = !DILocation(line: 599, column: 58, scope: !2014)
!2032 = !DILocation(line: 599, column: 6, scope: !2008)
!2033 = !DILocation(line: 617, column: 24, scope: !2011)
!2034 = !DILocation(line: 89, column: 3, scope: !2035, inlinedAt: !2061)
!2035 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_xEv", scope: !2036, file: !1347, line: 89, type: !1351, isLocal: false, isDefinition: true, scopeLine: 89, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !2038, variables: !4)
!2036 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_blockDim_t", file: !1347, line: 88, size: 8, elements: !2037, identifier: "_ZTS25__cuda_builtin_blockDim_t")
!2037 = !{!2038, !2039, !2040, !2041, !2046, !2050, !2054, !2057}
!2038 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_xEv", scope: !2036, file: !1347, line: 89, type: !1351, isLocal: false, isDefinition: false, scopeLine: 89, flags: DIFlagPrototyped, isOptimized: true)
!2039 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_yEv", scope: !2036, file: !1347, line: 90, type: !1351, isLocal: false, isDefinition: false, scopeLine: 90, flags: DIFlagPrototyped, isOptimized: true)
!2040 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_zEv", scope: !2036, file: !1347, line: 91, type: !1351, isLocal: false, isDefinition: false, scopeLine: 91, flags: DIFlagPrototyped, isOptimized: true)
!2041 = !DISubprogram(name: "operator dim3", linkageName: "_ZNK25__cuda_builtin_blockDim_tcv4dim3Ev", scope: !2036, file: !1347, line: 94, type: !2042, isLocal: false, isDefinition: false, scopeLine: 94, flags: DIFlagPrototyped, isOptimized: true)
!2042 = !DISubroutineType(types: !2043)
!2043 = !{!1594, !2044}
!2044 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !2045, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!2045 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !2036)
!2046 = !DISubprogram(name: "__cuda_builtin_blockDim_t", scope: !2036, file: !1347, line: 96, type: !2047, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!2047 = !DISubroutineType(types: !2048)
!2048 = !{null, !2049}
!2049 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !2036, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!2050 = !DISubprogram(name: "__cuda_builtin_blockDim_t", scope: !2036, file: !1347, line: 96, type: !2051, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!2051 = !DISubroutineType(types: !2052)
!2052 = !{null, !2049, !2053}
!2053 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !2045, size: 64)
!2054 = !DISubprogram(name: "operator=", linkageName: "_ZNK25__cuda_builtin_blockDim_taSERKS_", scope: !2036, file: !1347, line: 96, type: !2055, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!2055 = !DISubroutineType(types: !2056)
!2056 = !{null, !2044, !2053}
!2057 = !DISubprogram(name: "operator&", linkageName: "_ZNK25__cuda_builtin_blockDim_tadEv", scope: !2036, file: !1347, line: 96, type: !2058, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!2058 = !DISubroutineType(types: !2059)
!2059 = !{!2060, !2044}
!2060 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !2036, size: 64)
!2061 = distinct !DILocation(line: 618, column: 50, scope: !2011)
!2062 = !{i32 1, i32 1025}
!2063 = !DILocation(line: 618, column: 50, scope: !2011)
!2064 = !DILocation(line: 618, column: 48, scope: !2011)
!2065 = !DILocation(line: 90, column: 3, scope: !2066, inlinedAt: !2067)
!2066 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_yEv", scope: !2036, file: !1347, line: 90, type: !1351, isLocal: false, isDefinition: true, scopeLine: 90, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !2039, variables: !4)
!2067 = distinct !DILocation(line: 619, column: 52, scope: !2011)
!2068 = !DILocation(line: 619, column: 52, scope: !2011)
!2069 = !DILocation(line: 619, column: 50, scope: !2011)
!2070 = !DILocation(line: 100, column: 3, scope: !1585, inlinedAt: !2071)
!2071 = distinct !DILocation(line: 620, column: 52, scope: !2011)
!2072 = !DILocation(line: 620, column: 52, scope: !2011)
!2073 = !DILocation(line: 620, column: 50, scope: !2011)
!2074 = !DILocation(line: 101, column: 3, scope: !2075, inlinedAt: !2076)
!2075 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_yEv", scope: !1586, file: !1347, line: 101, type: !1351, isLocal: false, isDefinition: true, scopeLine: 101, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1589, variables: !4)
!2076 = distinct !DILocation(line: 621, column: 52, scope: !2011)
!2077 = !DILocation(line: 621, column: 52, scope: !2011)
!2078 = !DILocation(line: 621, column: 50, scope: !2011)
!2079 = !DILocation(line: 622, column: 51, scope: !2011)
!2080 = !DILocation(line: 622, column: 49, scope: !2011)
!2081 = !DILocation(line: 623, column: 51, scope: !2011)
!2082 = !DILocation(line: 624, column: 51, scope: !2011)
!2083 = !DILocation(line: 625, column: 3, scope: !2011)
!2084 = !DILocation(line: 626, column: 64, scope: !2011)
!2085 = !DILocation(line: 626, column: 3, scope: !2011)
!2086 = !DILocation(line: 643, column: 17, scope: !2013)
!2087 = !DILocation(line: 644, column: 17, scope: !2013)
!2088 = !DILocation(line: 646, column: 3, scope: !2013)
!2089 = !DILocation(line: 648, column: 17, scope: !2013)
!2090 = !DILocation(line: 654, column: 9, scope: !2013)
!2091 = !DILocation(line: 392, column: 3, scope: !2092, inlinedAt: !2099)
!2092 = distinct !DISubprogram(name: "memcpy", linkageName: "_ZL6memcpyPvPKvm", scope: !533, file: !533, line: 390, type: !2093, isLocal: true, isDefinition: true, scopeLine: 391, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !2095)
!2093 = !DISubroutineType(types: !2094)
!2094 = !{!364, !364, !365, !367}
!2095 = !{!2096, !2097, !2098}
!2096 = !DILocalVariable(name: "dest", arg: 1, scope: !2092, file: !533, line: 390, type: !364)
!2097 = !DILocalVariable(name: "src", arg: 2, scope: !2092, file: !533, line: 390, type: !365)
!2098 = !DILocalVariable(name: "n", arg: 3, scope: !2092, file: !533, line: 390, type: !367)
!2099 = distinct !DILocation(line: 657, column: 3, scope: !2013)
!2100 = !DILocation(line: 392, column: 3, scope: !2092, inlinedAt: !2101)
!2101 = distinct !DILocation(line: 660, column: 3, scope: !2013)
!2102 = !DILocation(line: 685, column: 10, scope: !2013)
!2103 = !DILocation(line: 686, column: 12, scope: !2013)
!2104 = !DILocation(line: 688, column: 2, scope: !2013)
!2105 = !DILocation(line: 689, column: 1, scope: !2008)
