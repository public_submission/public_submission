; ModuleID = '<stdin>'
source_filename = "llvm-link"
target datalayout = "e-i64:64-v16:16-v32:32-n16:32:64"
target triple = "nvptx64-nvidia-cuda"

%struct.CallSite_t = type { i32, i16, i16 }
%printf_args = type { i32 }
%printf_args.1 = type { i32, i32, i32, i32 }
%printf_args.2 = type { i8* }
%printf_args.6 = type { i32, i32 }
%printf_args.7 = type { i32, i32, i32, i32, i32 }
%struct.BBlog_t = type { i16, i16, i16, i16, i64, i32, i32, i32 }
%printf_args.8 = type { i32* }
%printf_args.13 = type { i64 }
%printf_args.15 = type { i32, i32, i64, i64, i64 }

@CTALB = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !0
@CTAUB = local_unnamed_addr addrspace(1) externally_initialized global i32 999, align 4, !dbg !33
@CONSTANCE = local_unnamed_addr addrspace(1) externally_initialized global i32 128, align 4, !dbg !35
@VERBOSE = local_unnamed_addr addrspace(1) externally_initialized global i8 0, align 1, !dbg !37
@ccnntt = addrspace(1) externally_initialized global i64 1, align 8, !dbg !40
@bbccnntt = addrspace(1) externally_initialized global i64 1, align 8, !dbg !42
@done1 = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !44
@done2 = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !46
@funcDic = addrspace(1) externally_initialized global [15 x [31 x i8]] zeroinitializer, align 1, !dbg !48
@dicHeight = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !54
@globalCallStack = local_unnamed_addr addrspace(1) externally_initialized global [10 x %struct.CallSite_t*] zeroinitializer, align 8, !dbg !56
@stackHeight = local_unnamed_addr addrspace(1) externally_initialized global i32* null, align 8, !dbg !68
@contextDic = addrspace(1) externally_initialized global [20 x [5 x %struct.CallSite_t]] zeroinitializer, align 4, !dbg !71
@cHeight = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !77
@.str = private unnamed_addr constant [35 x i8] c"height != 1 && \22stack height == 1\22\00", align 1
@__PRETTY_FUNCTION__._Z15updateCallStackiissi = private unnamed_addr constant [50 x i8] c"void updateCallStack(int, int, short, short, int)\00", align 1
@.str.2 = private unnamed_addr constant [20 x i8] c"first ever. tid=%d\0A\00", align 1
@.str.3 = private unnamed_addr constant [30 x i8] c"same caller different callee\0A\00", align 1
@.str.4 = private unnamed_addr constant [14 x i8] c"call squence\0A\00", align 1
@.str.5 = private unnamed_addr constant [50 x i8] c"(0==-1) && \22!! undefined things happeened here\5Cn\22\00", align 1
@.str.1 = private unnamed_addr constant [59 x i8] c"/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/ansf.cu\00", align 1
@.str.6 = private unnamed_addr constant [37 x i8] c" d::: current call stack height: %d\0A\00", align 1
@.str.7 = private unnamed_addr constant [30 x i8] c" %d: call site: %d, (%d, %d)\0A\00", align 1
@.str.8 = private unnamed_addr constant [15 x i8] c"d: caller: %s\0A\00", align 1
@.str.9 = private unnamed_addr constant [15 x i8] c"d: callee: %s\0A\00", align 1
@.str.10 = private unnamed_addr constant [15 x i8] c"d: return: %s\0A\00", align 1
@.str.11 = private unnamed_addr constant [18 x i8] c"d: undefined: %s\0A\00", align 1
@.str.12 = private unnamed_addr constant [11 x i8] c"id<cHeight\00", align 1
@__PRETTY_FUNCTION__._Z8cxtprinti = private unnamed_addr constant [19 x i8] c"void cxtprint(int)\00", align 1
@.str.13 = private unnamed_addr constant [41 x i8] c"d::: requested context id: %d out of %d\0A\00", align 1
@.str.14 = private unnamed_addr constant [47 x i8] c"d::::::: current context [%d][%d]: %d, %d, %d\0A\00", align 1
@.str.15 = private unnamed_addr constant [5 x i8] c"i<15\00", align 1
@__PRETTY_FUNCTION__._Z6cxtcpyP10CallSite_tS0_i = private unnamed_addr constant [45 x i8] c"void cxtcpy(CallSite_t *, CallSite_t *, int)\00", align 1
@.str.16 = private unnamed_addr constant [36 x i8] c"bbccnntt < 24*64*1024*1024/24 - 128\00", align 1
@__PRETTY_FUNCTION__.passBasicBlock = private unnamed_addr constant [43 x i8] c"void passBasicBlock(void *, int, int, int)\00", align 1
@buffer_oN_DeViCe = external addrspace(1) global [402653184 x i32], align 4
@.str.17 = private unnamed_addr constant [34 x i8] c"ccnntt < 24*64*1024*1024/24 - 128\00", align 1
@__PRETTY_FUNCTION__._Z10storeLinesPvssss = private unnamed_addr constant [52 x i8] c"void storeLines(void *, short, short, short, short)\00", align 1
@.str.18 = private unnamed_addr constant [22 x i8] c"d: buffer handle: %p\0A\00", align 1
@.str.19 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.20 = private unnamed_addr constant [24 x i8] c"d: load by CTA (%d,%d)\0A\00", align 1
@.str.21 = private unnamed_addr constant [25 x i8] c"d: store by CTA (%d,%d)\0A\00", align 1
@.str.22 = private unnamed_addr constant [23 x i8] c"d: !!! undefined !!! \0A\00", align 1
@.str.23 = private unnamed_addr constant [47 x i8] c"d: source line: %d\09 column: %d by CTA (%d,%d)\0A\00", align 1
@.str.24 = private unnamed_addr constant [49 x i8] c"d: Kernel Returns: collected [ %llu ] BB logs. \0A\00", align 1
@.str.25 = private unnamed_addr constant [57 x i8] c"size of function dic: %d %d %lu -> %lu , roundec to %lu\0A\00", align 1
@.str.26 = private unnamed_addr constant [56 x i8] c"size of context dic: %d %d %lu -> %lu , rounded to %lu\0A\00", align 1

; Function Attrs: nounwind
define void @_Z12syr2k_kernelPfS_S_(float* nocapture readonly %a, float* nocapture readonly %b, float* nocapture %c) local_unnamed_addr #0 !dbg !988 {
entry:
  tail call void @llvm.dbg.value(metadata float* %a, i64 0, metadata !994, metadata !1002), !dbg !1003
  tail call void @llvm.dbg.value(metadata float* %b, i64 0, metadata !995, metadata !1002), !dbg !1004
  tail call void @llvm.dbg.value(metadata float* %c, i64 0, metadata !996, metadata !1002), !dbg !1005
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #9, !dbg !1006, !range !1043
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #9, !dbg !1044, !range !1089
  %mul = mul nuw nsw i32 %1, %0, !dbg !1090
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #9, !dbg !1091, !range !1120
  %add = add nuw nsw i32 %mul, %2, !dbg !1121
  tail call void @llvm.dbg.value(metadata i32 %add, i64 0, metadata !997, metadata !1002), !dbg !1122
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #9, !dbg !1123, !range !1043
  %4 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.y() #9, !dbg !1126, !range !1089
  %mul5 = mul nuw nsw i32 %4, %3, !dbg !1129
  %5 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #9, !dbg !1130, !range !1120
  %add7 = add nuw nsw i32 %mul5, %5, !dbg !1133
  tail call void @llvm.dbg.value(metadata i32 %add7, i64 0, metadata !998, metadata !1002), !dbg !1134
  %6 = or i32 %add7, %add, !dbg !1135
  %7 = and i32 %6, 268434432, !dbg !1135
  %8 = icmp eq i32 %7, 0, !dbg !1135
  br i1 %8, label %if.then, label %if.end, !dbg !1135

if.then:                                          ; preds = %entry
  %mul9 = shl nsw i32 %add7, 10, !dbg !1136
  %add10 = add nuw nsw i32 %mul9, %add, !dbg !1137
  %idxprom58 = zext i32 %add10 to i64, !dbg !1138
  %arrayidx = getelementptr inbounds float, float* %c, i64 %idxprom58, !dbg !1138
  %9 = load float, float* %arrayidx, align 4, !dbg !1139, !tbaa !1140
  %mul11 = fmul float %9, 4.546000e+03, !dbg !1139
  store float %mul11, float* %arrayidx, align 4, !dbg !1139, !tbaa !1140
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !999, metadata !1002), !dbg !1144
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !999, metadata !1002), !dbg !1144
  %mul18 = shl i32 %add, 10
  br label %for.body, !dbg !1145

for.body:                                         ; preds = %for.body, %if.then
  %10 = phi float [ %mul11, %if.then ], [ %add38, %for.body ], !dbg !1148
  %k.059 = phi i32 [ 0, %if.then ], [ %inc, %for.body ]
  %add14 = add nuw nsw i32 %k.059, %mul9, !dbg !1151
  %idxprom15 = sext i32 %add14 to i64, !dbg !1152
  %arrayidx16 = getelementptr inbounds float, float* %a, i64 %idxprom15, !dbg !1152
  %11 = load float, float* %arrayidx16, align 4, !dbg !1152, !tbaa !1140
  %mul17 = fmul float %11, 1.243500e+04, !dbg !1153
  %add19 = add nuw nsw i32 %k.059, %mul18, !dbg !1154
  %idxprom20 = sext i32 %add19 to i64, !dbg !1155
  %arrayidx21 = getelementptr inbounds float, float* %b, i64 %idxprom20, !dbg !1155
  %12 = load float, float* %arrayidx21, align 4, !dbg !1155, !tbaa !1140
  %mul22 = fmul float %mul17, %12, !dbg !1156
  %arrayidx26 = getelementptr inbounds float, float* %b, i64 %idxprom15, !dbg !1157
  %13 = load float, float* %arrayidx26, align 4, !dbg !1157, !tbaa !1140
  %mul27 = fmul float %13, 1.243500e+04, !dbg !1158
  %arrayidx31 = getelementptr inbounds float, float* %a, i64 %idxprom20, !dbg !1159
  %14 = load float, float* %arrayidx31, align 4, !dbg !1159, !tbaa !1140
  %mul32 = fmul float %mul27, %14, !dbg !1160
  %add33 = fadd float %mul22, %mul32, !dbg !1161
  %add38 = fadd float %10, %add33, !dbg !1148
  store float %add38, float* %arrayidx, align 4, !dbg !1148, !tbaa !1140
  %inc = add nuw nsw i32 %k.059, 1, !dbg !1162
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !999, metadata !1002), !dbg !1144
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !999, metadata !1002), !dbg !1144
  %exitcond = icmp eq i32 %inc, 1024, !dbg !1164
  br i1 %exitcond, label %if.end.loopexit, label %for.body, !dbg !1145, !llvm.loop !1166

if.end.loopexit:                                  ; preds = %for.body
  br label %if.end, !dbg !1169

if.end:                                           ; preds = %if.end.loopexit, %entry
  ret void, !dbg !1169
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.value(metadata, i64, metadata, metadata) #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.y() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.y() #1

; Function Attrs: nounwind
define void @_Z8mystrcpyPcS_(i8* nocapture %dst, i8* nocapture readonly %src) local_unnamed_addr #0 !dbg !1170 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %dst, i64 0, metadata !1174, metadata !1002), !dbg !1177
  tail call void @llvm.dbg.value(metadata i8* %src, i64 0, metadata !1175, metadata !1002), !dbg !1178
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1176, metadata !1002), !dbg !1179
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1176, metadata !1002), !dbg !1179
  %0 = load i8, i8* %src, align 1, !dbg !1180, !tbaa !1182
  %cmp16 = icmp eq i8 %0, 0, !dbg !1183
  br i1 %cmp16, label %while.end, label %while.body.preheader, !dbg !1184

while.body.preheader:                             ; preds = %entry
  br label %while.body, !dbg !1186

while.body:                                       ; preds = %while.body.preheader, %while.body
  %arrayidx518 = phi i8* [ %arrayidx5, %while.body ], [ %dst, %while.body.preheader ]
  %1 = phi i8 [ %2, %while.body ], [ %0, %while.body.preheader ]
  %cnt.017 = phi i32 [ %inc, %while.body ], [ 0, %while.body.preheader ]
  store i8 %1, i8* %arrayidx518, align 1, !dbg !1186, !tbaa !1182
  %inc = add nuw nsw i32 %cnt.017, 1, !dbg !1188
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1176, metadata !1002), !dbg !1179
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1176, metadata !1002), !dbg !1179
  %idxprom = sext i32 %inc to i64, !dbg !1180
  %arrayidx = getelementptr inbounds i8, i8* %src, i64 %idxprom, !dbg !1180
  %2 = load i8, i8* %arrayidx, align 1, !dbg !1180, !tbaa !1182
  %cmp = icmp ne i8 %2, 0, !dbg !1183
  %cmp1 = icmp slt i32 %inc, 30, !dbg !1189
  %3 = and i1 %cmp1, %cmp, !dbg !1191
  %arrayidx5 = getelementptr inbounds i8, i8* %dst, i64 %idxprom
  br i1 %3, label %while.body, label %while.end.loopexit, !dbg !1184, !llvm.loop !1192

while.end.loopexit:                               ; preds = %while.body
  br label %while.end, !dbg !1195

while.end:                                        ; preds = %while.end.loopexit, %entry
  %arrayidx5.lcssa = phi i8* [ %dst, %entry ], [ %arrayidx5, %while.end.loopexit ]
  store i8 0, i8* %arrayidx5.lcssa, align 1, !dbg !1195, !tbaa !1182
  ret void, !dbg !1196
}

; Function Attrs: nounwind readonly
define zeroext i1 @_Z8mystrcmpPcS_(i8* nocapture readonly %dst, i8* nocapture readonly %src) local_unnamed_addr #2 !dbg !1197 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %dst, i64 0, metadata !1201, metadata !1002), !dbg !1204
  tail call void @llvm.dbg.value(metadata i8* %src, i64 0, metadata !1202, metadata !1002), !dbg !1205
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1203, metadata !1002), !dbg !1206
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1203, metadata !1002), !dbg !1206
  br label %while.body, !dbg !1207

while.cond:                                       ; preds = %if.end
  %inc = add nuw nsw i32 %cnt.023, 1, !dbg !1209
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1203, metadata !1002), !dbg !1206
  %idxprom.1 = sext i32 %inc to i64, !dbg !1211
  %arrayidx.1 = getelementptr inbounds i8, i8* %dst, i64 %idxprom.1, !dbg !1211
  %0 = load i8, i8* %arrayidx.1, align 1, !dbg !1211, !tbaa !1182
  %1 = getelementptr inbounds i8, i8* %src, i64 %idxprom.1, !dbg !1213
  %2 = load i8, i8* %1, align 1, !dbg !1213, !tbaa !1182
  %3 = or i8 %2, %0, !dbg !1215
  %4 = icmp eq i8 %3, 0, !dbg !1215
  br i1 %4, label %cleanup, label %if.end.1, !dbg !1215

while.body:                                       ; preds = %while.cond.2, %entry
  %cnt.023 = phi i32 [ 0, %entry ], [ %inc.2, %while.cond.2 ]
  %idxprom = sext i32 %cnt.023 to i64, !dbg !1211
  %arrayidx = getelementptr inbounds i8, i8* %dst, i64 %idxprom, !dbg !1211
  %5 = load i8, i8* %arrayidx, align 1, !dbg !1211, !tbaa !1182
  %6 = getelementptr inbounds i8, i8* %src, i64 %idxprom, !dbg !1213
  %7 = load i8, i8* %6, align 1, !dbg !1213, !tbaa !1182
  %8 = or i8 %7, %5, !dbg !1215
  %9 = icmp eq i8 %8, 0, !dbg !1215
  br i1 %9, label %cleanup, label %if.end, !dbg !1215

if.end:                                           ; preds = %while.body
  %cmp12 = icmp eq i8 %5, %7, !dbg !1216
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1203, metadata !1002), !dbg !1206
  br i1 %cmp12, label %while.cond, label %cleanup, !dbg !1218

cleanup:                                          ; preds = %while.cond.2, %if.end.2, %while.cond.1, %if.end.1, %while.cond, %if.end, %while.body
  %retval.0 = phi i1 [ true, %while.body ], [ false, %if.end ], [ true, %while.cond ], [ false, %if.end.1 ], [ true, %while.cond.1 ], [ false, %if.end.2 ], [ true, %while.cond.2 ]
  ret i1 %retval.0, !dbg !1219

if.end.1:                                         ; preds = %while.cond
  %cmp12.1 = icmp eq i8 %0, %2, !dbg !1216
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1203, metadata !1002), !dbg !1206
  br i1 %cmp12.1, label %while.cond.1, label %cleanup, !dbg !1218

while.cond.1:                                     ; preds = %if.end.1
  %inc.1 = add nsw i32 %cnt.023, 2, !dbg !1209
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1203, metadata !1002), !dbg !1206
  %idxprom.2 = sext i32 %inc.1 to i64, !dbg !1211
  %arrayidx.2 = getelementptr inbounds i8, i8* %dst, i64 %idxprom.2, !dbg !1211
  %10 = load i8, i8* %arrayidx.2, align 1, !dbg !1211, !tbaa !1182
  %11 = getelementptr inbounds i8, i8* %src, i64 %idxprom.2, !dbg !1213
  %12 = load i8, i8* %11, align 1, !dbg !1213, !tbaa !1182
  %13 = or i8 %12, %10, !dbg !1215
  %14 = icmp eq i8 %13, 0, !dbg !1215
  br i1 %14, label %cleanup, label %if.end.2, !dbg !1215

if.end.2:                                         ; preds = %while.cond.1
  %cmp12.2 = icmp eq i8 %10, %12, !dbg !1216
  %inc.2 = add nsw i32 %cnt.023, 3, !dbg !1209
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1203, metadata !1002), !dbg !1206
  br i1 %cmp12.2, label %while.cond.2, label %cleanup, !dbg !1218

while.cond.2:                                     ; preds = %if.end.2
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1203, metadata !1002), !dbg !1206
  %cmp.2 = icmp slt i32 %inc.2, 30, !dbg !1220
  br i1 %cmp.2, label %while.body, label %cleanup, !dbg !1207, !llvm.loop !1221
}

; Function Attrs: nounwind
define i32 @_Z9getFuncIDPc(i8* nocapture readonly %func) local_unnamed_addr #0 !dbg !1224 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %func, i64 0, metadata !1228, metadata !1002), !dbg !1234
  %0 = load i32, i32* addrspacecast (i32 addrspace(1)* @dicHeight to i32*), align 4, !dbg !1235, !tbaa !1237
  %cmp = icmp eq i32 %0, 0, !dbg !1239
  br i1 %cmp, label %if.then, label %for.cond.preheader, !dbg !1240

for.cond.preheader:                               ; preds = %entry
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1229, metadata !1002), !dbg !1241
  %cmp152 = icmp sgt i32 %0, 0, !dbg !1242
  br i1 %cmp152, label %for.body.preheader, label %for.end, !dbg !1244

for.body.preheader:                               ; preds = %for.cond.preheader
  br label %for.body, !dbg !1246

if.then:                                          ; preds = %entry
  tail call void @llvm.dbg.value(metadata i8* getelementptr ([15 x [31 x i8]], [15 x [31 x i8]]* addrspacecast ([15 x [31 x i8]] addrspace(1)* @funcDic to [15 x [31 x i8]]*), i64 0, i64 0, i64 0), i64 0, metadata !1174, metadata !1002), !dbg !1247
  tail call void @llvm.dbg.value(metadata i8* %func, i64 0, metadata !1175, metadata !1002), !dbg !1250
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1176, metadata !1002), !dbg !1251
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1176, metadata !1002), !dbg !1251
  %1 = load i8, i8* %func, align 1, !dbg !1252, !tbaa !1182
  %cmp16.i = icmp eq i8 %1, 0, !dbg !1253
  br i1 %cmp16.i, label %return.sink.split, label %while.body.i.preheader, !dbg !1254

while.body.i.preheader:                           ; preds = %if.then
  br label %while.body.i, !dbg !1255

while.body.i:                                     ; preds = %while.body.i.preheader, %while.body.i
  %arrayidx518.i = phi i8* [ %arrayidx5.i, %while.body.i ], [ getelementptr ([15 x [31 x i8]], [15 x [31 x i8]]* addrspacecast ([15 x [31 x i8]] addrspace(1)* @funcDic to [15 x [31 x i8]]*), i64 0, i64 0, i64 0), %while.body.i.preheader ]
  %2 = phi i8 [ %3, %while.body.i ], [ %1, %while.body.i.preheader ]
  %cnt.017.i = phi i32 [ %inc.i, %while.body.i ], [ 0, %while.body.i.preheader ]
  store i8 %2, i8* %arrayidx518.i, align 1, !dbg !1255, !tbaa !1182
  %inc.i = add nuw nsw i32 %cnt.017.i, 1, !dbg !1256
  tail call void @llvm.dbg.value(metadata i32 %inc.i, i64 0, metadata !1176, metadata !1002), !dbg !1251
  tail call void @llvm.dbg.value(metadata i32 %inc.i, i64 0, metadata !1176, metadata !1002), !dbg !1251
  %idxprom.i = sext i32 %inc.i to i64, !dbg !1252
  %arrayidx.i = getelementptr inbounds i8, i8* %func, i64 %idxprom.i, !dbg !1252
  %3 = load i8, i8* %arrayidx.i, align 1, !dbg !1252, !tbaa !1182
  %cmp.i = icmp ne i8 %3, 0, !dbg !1253
  %cmp1.i = icmp slt i32 %inc.i, 30, !dbg !1257
  %4 = and i1 %cmp1.i, %cmp.i, !dbg !1258
  %arrayidx5.i50 = getelementptr [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 0, i64 %idxprom.i
  %arrayidx5.i = addrspacecast i8 addrspace(1)* %arrayidx5.i50 to i8*
  br i1 %4, label %while.body.i, label %return.sink.split.loopexit, !dbg !1254, !llvm.loop !1192

for.body:                                         ; preds = %for.body.preheader, %for.inc
  %i.053 = phi i32 [ %inc4, %for.inc ], [ 0, %for.body.preheader ]
  %idxprom = sext i32 %i.053 to i64, !dbg !1246
  tail call void @llvm.dbg.value(metadata i8* %func, i64 0, metadata !1202, metadata !1002), !dbg !1259
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1203, metadata !1002), !dbg !1261
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1203, metadata !1002), !dbg !1261
  br label %while.body.i37, !dbg !1262

while.cond.i:                                     ; preds = %if.end.i
  %inc.i38 = or i32 %cnt.023.i, 1, !dbg !1263
  tail call void @llvm.dbg.value(metadata i32 %inc.i38, i64 0, metadata !1203, metadata !1002), !dbg !1261
  %idxprom.i35.1 = sext i32 %inc.i38 to i64, !dbg !1264
  %arrayidx.i3649.1 = getelementptr inbounds [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 %idxprom, i64 %idxprom.i35.1, !dbg !1264
  %arrayidx.i36.1 = addrspacecast i8 addrspace(1)* %arrayidx.i3649.1 to i8*, !dbg !1264
  %5 = load i8, i8* %arrayidx.i36.1, align 1, !dbg !1264, !tbaa !1182
  %6 = getelementptr inbounds i8, i8* %func, i64 %idxprom.i35.1, !dbg !1265
  %7 = load i8, i8* %6, align 1, !dbg !1265, !tbaa !1182
  %8 = or i8 %7, %5, !dbg !1266
  %9 = icmp eq i8 %8, 0, !dbg !1266
  br i1 %9, label %return.loopexit, label %if.end.i.1, !dbg !1266

while.body.i37:                                   ; preds = %while.cond.i.1, %for.body
  %cnt.023.i = phi i32 [ 0, %for.body ], [ %inc.i38.1, %while.cond.i.1 ]
  %idxprom.i35 = sext i32 %cnt.023.i to i64, !dbg !1264
  %arrayidx.i3649 = getelementptr inbounds [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 %idxprom, i64 %idxprom.i35, !dbg !1264
  %arrayidx.i36 = addrspacecast i8 addrspace(1)* %arrayidx.i3649 to i8*, !dbg !1264
  %10 = load i8, i8* %arrayidx.i36, align 1, !dbg !1264, !tbaa !1182
  %11 = getelementptr inbounds i8, i8* %func, i64 %idxprom.i35, !dbg !1265
  %12 = load i8, i8* %11, align 1, !dbg !1265, !tbaa !1182
  %13 = or i8 %12, %10, !dbg !1266
  %14 = icmp eq i8 %13, 0, !dbg !1266
  br i1 %14, label %return.loopexit, label %if.end.i, !dbg !1266

if.end.i:                                         ; preds = %while.body.i37
  %cmp12.i = icmp eq i8 %10, %12, !dbg !1267
  tail call void @llvm.dbg.value(metadata i32 %inc.i38, i64 0, metadata !1203, metadata !1002), !dbg !1261
  br i1 %cmp12.i, label %while.cond.i, label %for.inc, !dbg !1268

for.inc:                                          ; preds = %if.end.i.1, %if.end.i
  %inc4 = add nuw nsw i32 %i.053, 1, !dbg !1269
  tail call void @llvm.dbg.value(metadata i32 %inc4, i64 0, metadata !1229, metadata !1002), !dbg !1241
  tail call void @llvm.dbg.value(metadata i32 %inc4, i64 0, metadata !1229, metadata !1002), !dbg !1241
  %cmp1 = icmp slt i32 %inc4, %0, !dbg !1242
  br i1 %cmp1, label %for.body, label %for.end.loopexit, !dbg !1244, !llvm.loop !1271

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end, !dbg !1274

for.end:                                          ; preds = %for.end.loopexit, %for.cond.preheader
  %idxprom7 = sext i32 %0 to i64, !dbg !1274
  %arraydecay919 = getelementptr inbounds [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 %idxprom7, i64 0, !dbg !1274
  %arraydecay9 = addrspacecast i8 addrspace(1)* %arraydecay919 to i8*, !dbg !1274
  tail call void @llvm.dbg.value(metadata i8* %arraydecay9, i64 0, metadata !1174, metadata !1002), !dbg !1275
  tail call void @llvm.dbg.value(metadata i8* %func, i64 0, metadata !1175, metadata !1002), !dbg !1277
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1176, metadata !1002), !dbg !1278
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1176, metadata !1002), !dbg !1278
  %15 = load i8, i8* %func, align 1, !dbg !1279, !tbaa !1182
  %cmp16.i22 = icmp eq i8 %15, 0, !dbg !1280
  br i1 %cmp16.i22, label %return.sink.split, label %while.body.i31.preheader, !dbg !1281

while.body.i31.preheader:                         ; preds = %for.end
  br label %while.body.i31, !dbg !1282

while.body.i31:                                   ; preds = %while.body.i31.preheader, %while.body.i31
  %arrayidx518.i23 = phi i8* [ %arrayidx5.i30, %while.body.i31 ], [ %arraydecay9, %while.body.i31.preheader ]
  %16 = phi i8 [ %17, %while.body.i31 ], [ %15, %while.body.i31.preheader ]
  %cnt.017.i24 = phi i32 [ %inc.i25, %while.body.i31 ], [ 0, %while.body.i31.preheader ]
  store i8 %16, i8* %arrayidx518.i23, align 1, !dbg !1282, !tbaa !1182
  %inc.i25 = add nuw nsw i32 %cnt.017.i24, 1, !dbg !1283
  tail call void @llvm.dbg.value(metadata i32 %inc.i25, i64 0, metadata !1176, metadata !1002), !dbg !1278
  tail call void @llvm.dbg.value(metadata i32 %inc.i25, i64 0, metadata !1176, metadata !1002), !dbg !1278
  %idxprom.i26 = sext i32 %inc.i25 to i64, !dbg !1279
  %arrayidx.i27 = getelementptr inbounds i8, i8* %func, i64 %idxprom.i26, !dbg !1279
  %17 = load i8, i8* %arrayidx.i27, align 1, !dbg !1279, !tbaa !1182
  %cmp.i28 = icmp ne i8 %17, 0, !dbg !1280
  %cmp1.i29 = icmp slt i32 %inc.i25, 30, !dbg !1284
  %18 = and i1 %cmp1.i29, %cmp.i28, !dbg !1285
  %arrayidx5.i3048 = getelementptr inbounds [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 %idxprom7, i64 %idxprom.i26
  %arrayidx5.i30 = addrspacecast i8 addrspace(1)* %arrayidx5.i3048 to i8*
  br i1 %18, label %while.body.i31, label %return.sink.split.loopexit58, !dbg !1281, !llvm.loop !1192

return.sink.split.loopexit:                       ; preds = %while.body.i
  br label %return.sink.split, !dbg !1286

return.sink.split.loopexit58:                     ; preds = %while.body.i31
  br label %return.sink.split, !dbg !1286

return.sink.split:                                ; preds = %return.sink.split.loopexit58, %return.sink.split.loopexit, %if.then, %for.end
  %arrayidx5.lcssa.i.sink = phi i8* [ %arraydecay9, %for.end ], [ getelementptr ([15 x [31 x i8]], [15 x [31 x i8]]* addrspacecast ([15 x [31 x i8]] addrspace(1)* @funcDic to [15 x [31 x i8]]*), i64 0, i64 0, i64 0), %if.then ], [ %arrayidx5.i, %return.sink.split.loopexit ], [ %arrayidx5.i30, %return.sink.split.loopexit58 ]
  %retval.3.ph = phi i32 [ %0, %for.end ], [ 0, %if.then ], [ 0, %return.sink.split.loopexit ], [ %0, %return.sink.split.loopexit58 ]
  store i8 0, i8* %arrayidx5.lcssa.i.sink, align 1, !dbg !1286, !tbaa !1182
  %inc10 = add nsw i32 %0, 1
  store i32 %inc10, i32* addrspacecast (i32 addrspace(1)* @dicHeight to i32*), align 4, !tbaa !1237
  br label %return, !dbg !1287

return.loopexit:                                  ; preds = %while.cond.i.1, %while.cond.i, %while.body.i37
  br label %return, !dbg !1287

return:                                           ; preds = %return.loopexit, %return.sink.split
  %retval.3 = phi i32 [ %retval.3.ph, %return.sink.split ], [ %i.053, %return.loopexit ]
  ret i32 %retval.3, !dbg !1287

if.end.i.1:                                       ; preds = %while.cond.i
  %cmp12.i.1 = icmp eq i8 %5, %7, !dbg !1267
  %inc.i38.1 = add nsw i32 %cnt.023.i, 2, !dbg !1263
  tail call void @llvm.dbg.value(metadata i32 %inc.i38, i64 0, metadata !1203, metadata !1002), !dbg !1261
  br i1 %cmp12.i.1, label %while.cond.i.1, label %for.inc, !dbg !1268

while.cond.i.1:                                   ; preds = %if.end.i.1
  tail call void @llvm.dbg.value(metadata i32 %inc.i38, i64 0, metadata !1203, metadata !1002), !dbg !1261
  %cmp.i34.1 = icmp slt i32 %inc.i38.1, 30, !dbg !1288
  br i1 %cmp.i34.1, label %while.body.i37, label %return.loopexit, !dbg !1262, !llvm.loop !1221
}

; Function Attrs: convergent nounwind
define void @_Z15updateCallStackiissi(i32 %caller, i32 %callee, i16 signext %sline, i16 signext %scolm, i32 %tid) local_unnamed_addr #3 !dbg !1289 {
entry:
  %tmp = alloca %printf_args, align 8
  tail call void @llvm.dbg.value(metadata i32 %caller, i64 0, metadata !1293, metadata !1002), !dbg !1305
  tail call void @llvm.dbg.value(metadata i32 %callee, i64 0, metadata !1294, metadata !1002), !dbg !1306
  tail call void @llvm.dbg.value(metadata i16 %sline, i64 0, metadata !1295, metadata !1002), !dbg !1307
  tail call void @llvm.dbg.value(metadata i16 %scolm, i64 0, metadata !1296, metadata !1002), !dbg !1308
  tail call void @llvm.dbg.value(metadata i32 %tid, i64 0, metadata !1297, metadata !1002), !dbg !1309
  %idxprom = sext i32 %tid to i64, !dbg !1310
  %arrayidx177 = getelementptr inbounds [10 x %struct.CallSite_t*], [10 x %struct.CallSite_t*] addrspace(1)* @globalCallStack, i64 0, i64 %idxprom, !dbg !1310
  %arrayidx = addrspacecast %struct.CallSite_t* addrspace(1)* %arrayidx177 to %struct.CallSite_t**, !dbg !1310
  %0 = load %struct.CallSite_t*, %struct.CallSite_t** %arrayidx, align 8, !dbg !1310, !tbaa !1311
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %0, i64 0, metadata !1298, metadata !1002), !dbg !1313
  %1 = load i32*, i32** addrspacecast (i32* addrspace(1)* @stackHeight to i32**), align 8, !dbg !1314, !tbaa !1311
  %arrayidx2 = getelementptr inbounds i32, i32* %1, i64 %idxprom, !dbg !1314
  tail call void @llvm.dbg.value(metadata i32* %arrayidx2, i64 0, metadata !1299, metadata !1002), !dbg !1315
  %2 = load i32, i32* %arrayidx2, align 4, !dbg !1316, !tbaa !1237
  switch i32 %2, label %if.end [
    i32 1, label %cond.false
    i32 0, label %if.then
  ], !dbg !1316

cond.false:                                       ; preds = %entry
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str, i64 0, i64 0), i32 121, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @__PRETTY_FUNCTION__._Z15updateCallStackiissi, i64 0, i64 0)) #10, !dbg !1317
  unreachable

if.then:                                          ; preds = %entry
  %3 = getelementptr inbounds %printf_args, %printf_args* %tmp, i64 0, i32 0, !dbg !1319
  store i32 %tid, i32* %3, align 8, !dbg !1319
  %4 = bitcast %printf_args* %tmp to i8*, !dbg !1319
  %5 = call i32 @vprintf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i64 0, i64 0), i8* nonnull %4) #9, !dbg !1319
  %id = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 0, !dbg !1322
  store i32 %caller, i32* %id, align 4, !dbg !1323, !tbaa !1324
  %sline6 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 1, !dbg !1327
  store i16 %sline, i16* %sline6, align 4, !dbg !1328, !tbaa !1329
  %scolm8 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 2, !dbg !1330
  store i16 %scolm, i16* %scolm8, align 2, !dbg !1331, !tbaa !1332
  %id10 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 1, i32 0, !dbg !1333
  store i32 %callee, i32* %id10, align 4, !dbg !1334, !tbaa !1324
  %sline12 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 1, i32 1, !dbg !1335
  store i16 -1, i16* %sline12, align 4, !dbg !1336, !tbaa !1329
  %scolm14 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 1, i32 2, !dbg !1337
  store i16 -1, i16* %scolm14, align 2, !dbg !1338, !tbaa !1332
  store i32 2, i32* %arrayidx2, align 4, !dbg !1339, !tbaa !1237
  br label %cleanup100, !dbg !1340

if.end:                                           ; preds = %entry
  %sub = add nsw i32 %2, -2, !dbg !1341
  %idxprom16 = sext i32 %sub to i64, !dbg !1342
  %id18 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom16, i32 0, !dbg !1343
  %6 = load i32, i32* %id18, align 4, !dbg !1343, !tbaa !1324
  tail call void @llvm.dbg.value(metadata i32 %6, i64 0, metadata !1301, metadata !1002), !dbg !1344
  %sub20 = add nsw i32 %2, -1, !dbg !1345
  %idxprom21 = sext i32 %sub20 to i64, !dbg !1346
  %id23 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom21, i32 0, !dbg !1347
  %7 = load i32, i32* %id23, align 4, !dbg !1347, !tbaa !1324
  tail call void @llvm.dbg.value(metadata i32 %7, i64 0, metadata !1302, metadata !1002), !dbg !1348
  %cmp24 = icmp eq i32 %6, %caller, !dbg !1349
  %cmp25 = icmp eq i32 %7, %callee, !dbg !1351
  %or.cond = and i1 %cmp24, %cmp25, !dbg !1353
  br i1 %or.cond, label %if.then26, label %if.else, !dbg !1353

if.then26:                                        ; preds = %if.end
  %sline30 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom16, i32 1, !dbg !1354
  store i16 %sline, i16* %sline30, align 4, !dbg !1356, !tbaa !1329
  %scolm34 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom16, i32 2, !dbg !1357
  store i16 %scolm, i16* %scolm34, align 2, !dbg !1358, !tbaa !1332
  br label %cleanup100, !dbg !1359

if.else:                                          ; preds = %if.end
  %cmp24.not = xor i1 %cmp24, true, !dbg !1360
  %or.cond178 = or i1 %cmp25, %cmp24.not, !dbg !1360
  br i1 %or.cond178, label %if.else51, label %if.then38, !dbg !1360

if.then38:                                        ; preds = %if.else
  %8 = tail call i32 @vprintf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.3, i64 0, i64 0), i8* null) #9, !dbg !1362
  %9 = load i32, i32* %arrayidx2, align 4, !dbg !1364, !tbaa !1237
  %sub39 = add nsw i32 %9, -1, !dbg !1365
  %idxprom40 = sext i32 %sub39 to i64, !dbg !1366
  %id42 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom40, i32 0, !dbg !1367
  store i32 %callee, i32* %id42, align 4, !dbg !1368, !tbaa !1324
  %10 = load i32, i32* %arrayidx2, align 4, !dbg !1369, !tbaa !1237
  %sub43 = add nsw i32 %10, -2, !dbg !1370
  %idxprom44 = sext i32 %sub43 to i64, !dbg !1371
  %sline46 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom44, i32 1, !dbg !1372
  store i16 %sline, i16* %sline46, align 4, !dbg !1373, !tbaa !1329
  %scolm50 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom44, i32 2, !dbg !1374
  store i16 %scolm, i16* %scolm50, align 2, !dbg !1375, !tbaa !1332
  br label %cleanup100, !dbg !1376

if.else51:                                        ; preds = %if.else
  %cmp52 = icmp eq i32 %7, %caller, !dbg !1377
  br i1 %cmp52, label %if.then53, label %for.cond.preheader, !dbg !1379

for.cond.preheader:                               ; preds = %if.else51
  br label %for.cond

if.then53:                                        ; preds = %if.else51
  %11 = tail call i32 @vprintf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.4, i64 0, i64 0), i8* null) #9, !dbg !1380
  %12 = load i32, i32* %arrayidx2, align 4, !dbg !1382, !tbaa !1237
  %sub54 = add nsw i32 %12, -1, !dbg !1383
  %idxprom55 = sext i32 %sub54 to i64, !dbg !1384
  %sline57 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom55, i32 1, !dbg !1385
  store i16 %sline, i16* %sline57, align 4, !dbg !1386, !tbaa !1329
  %scolm61 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom55, i32 2, !dbg !1387
  store i16 %scolm, i16* %scolm61, align 2, !dbg !1388, !tbaa !1332
  %idxprom62 = sext i32 %12 to i64, !dbg !1389
  %id64 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom62, i32 0, !dbg !1390
  store i32 %callee, i32* %id64, align 4, !dbg !1391, !tbaa !1324
  %13 = load i32, i32* %arrayidx2, align 4, !dbg !1392, !tbaa !1237
  %idxprom65 = sext i32 %13 to i64, !dbg !1393
  %sline67 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom65, i32 1, !dbg !1394
  store i16 -1, i16* %sline67, align 4, !dbg !1395, !tbaa !1329
  %scolm70 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom65, i32 2, !dbg !1396
  store i16 -1, i16* %scolm70, align 2, !dbg !1397, !tbaa !1332
  %inc = add nsw i32 %13, 1, !dbg !1398
  store i32 %inc, i32* %arrayidx2, align 4, !dbg !1398, !tbaa !1237
  br label %cleanup100, !dbg !1399

for.cond:                                         ; preds = %for.cond.preheader, %for.body
  %i.0.in = phi i32 [ %i.0, %for.body ], [ %2, %for.cond.preheader ]
  %i.0 = add nsw i32 %i.0.in, -1
  tail call void @llvm.dbg.value(metadata i32 %i.0, i64 0, metadata !1303, metadata !1002), !dbg !1400
  %cmp76 = icmp sgt i32 %i.0.in, 0, !dbg !1401
  br i1 %cmp76, label %for.body, label %for.end, !dbg !1404

for.body:                                         ; preds = %for.cond
  %idxprom77 = sext i32 %i.0 to i64, !dbg !1406
  %id79 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom77, i32 0, !dbg !1409
  %14 = load i32, i32* %id79, align 4, !dbg !1409, !tbaa !1324
  %cmp80 = icmp eq i32 %14, %caller, !dbg !1410
  br i1 %cmp80, label %if.then81, label %for.cond, !dbg !1411, !llvm.loop !1412

if.then81:                                        ; preds = %for.body
  store i32 %i.0.in, i32* %arrayidx2, align 4, !dbg !1415, !tbaa !1237
  store i32 %callee, i32* %id79, align 4, !dbg !1417, !tbaa !1324
  %sline87 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom77, i32 1, !dbg !1418
  %scolm90 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom77, i32 2, !dbg !1419
  store i16 %sline, i16* %sline87, align 4, !dbg !1420, !tbaa !1329
  store i16 %scolm, i16* %scolm90, align 2, !dbg !1421, !tbaa !1332
  br label %cleanup100

for.end:                                          ; preds = %for.cond
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.5, i64 0, i64 0), i32 183, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @__PRETTY_FUNCTION__._Z15updateCallStackiissi, i64 0, i64 0)) #10, !dbg !1422
  unreachable

cleanup100:                                       ; preds = %if.then26, %if.then38, %if.then53, %if.then81, %if.then
  ret void, !dbg !1423
}

; Function Attrs: convergent inlinehint noreturn nounwind
define internal fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* %__message, i32 %__line, i8* %__function) unnamed_addr #4 !dbg !1425 {
entry:
  tail call void @__assertfail(i8* %__message, i8* getelementptr inbounds ([59 x i8], [59 x i8]* @.str.1, i64 0, i64 0), i32 %__line, i8* %__function, i64 1) #11, !dbg !1434
  unreachable, !dbg !1434
}

declare i32 @vprintf(i8*, i8*) local_unnamed_addr

; Function Attrs: convergent noreturn nounwind
declare void @__assertfail(i8*, i8*, i32, i8*, i64) local_unnamed_addr #5

; Function Attrs: nounwind
define void @_Z14printCallStacki(i32 %tid) local_unnamed_addr #0 !dbg !1435 {
entry:
  %tmp = alloca %printf_args, align 8
  %tmp12 = alloca %printf_args.1, align 8
  tail call void @llvm.dbg.value(metadata i32 %tid, i64 0, metadata !1437, metadata !1002), !dbg !1442
  %idxprom = sext i32 %tid to i64, !dbg !1443
  %arrayidx27 = getelementptr inbounds [10 x %struct.CallSite_t*], [10 x %struct.CallSite_t*] addrspace(1)* @globalCallStack, i64 0, i64 %idxprom, !dbg !1443
  %arrayidx = addrspacecast %struct.CallSite_t* addrspace(1)* %arrayidx27 to %struct.CallSite_t**, !dbg !1443
  %0 = load %struct.CallSite_t*, %struct.CallSite_t** %arrayidx, align 8, !dbg !1443, !tbaa !1311
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %0, i64 0, metadata !1438, metadata !1002), !dbg !1444
  %1 = load i32*, i32** addrspacecast (i32* addrspace(1)* @stackHeight to i32**), align 8, !dbg !1445, !tbaa !1311
  %arrayidx2 = getelementptr inbounds i32, i32* %1, i64 %idxprom, !dbg !1445
  %2 = load i32, i32* %arrayidx2, align 4, !dbg !1445, !tbaa !1237
  tail call void @llvm.dbg.value(metadata i32 %2, i64 0, metadata !1439, metadata !1002), !dbg !1446
  %3 = getelementptr inbounds %printf_args, %printf_args* %tmp, i64 0, i32 0, !dbg !1447
  store i32 %2, i32* %3, align 8, !dbg !1447
  %4 = bitcast %printf_args* %tmp to i8*, !dbg !1447
  %5 = call i32 @vprintf(i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.6, i64 0, i64 0), i8* nonnull %4) #9, !dbg !1447
  %cmp = icmp sgt i32 %2, 0, !dbg !1448
  call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1440, metadata !1002), !dbg !1450
  br i1 %cmp, label %for.body.lr.ph, label %cleanup, !dbg !1451

for.body.lr.ph:                                   ; preds = %entry
  %6 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp12, i64 0, i32 0
  %7 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp12, i64 0, i32 1
  %8 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp12, i64 0, i32 2
  %9 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp12, i64 0, i32 3
  %10 = bitcast %printf_args.1* %tmp12 to i8*
  %xtraiter = and i32 %2, 1, !dbg !1452
  %lcmp.mod = icmp eq i32 %xtraiter, 0, !dbg !1452
  br i1 %lcmp.mod, label %for.body.prol.loopexit, label %for.body.prol.preheader, !dbg !1452

for.body.prol.preheader:                          ; preds = %for.body.lr.ph
  br label %for.body.prol, !dbg !1452

for.body.prol:                                    ; preds = %for.body.prol.preheader
  %id.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 0, !dbg !1454
  %11 = load i32, i32* %id.prol, align 4, !dbg !1454, !tbaa !1324
  %sline.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 1, !dbg !1456
  %12 = load i16, i16* %sline.prol, align 4, !dbg !1456, !tbaa !1329
  %conv.prol = sext i16 %12 to i32, !dbg !1457
  %scolm.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 2, !dbg !1458
  %13 = load i16, i16* %scolm.prol, align 2, !dbg !1458, !tbaa !1332
  %conv11.prol = sext i16 %13 to i32, !dbg !1459
  store i32 0, i32* %6, align 8, !dbg !1460
  store i32 %11, i32* %7, align 4, !dbg !1460
  store i32 %conv.prol, i32* %8, align 8, !dbg !1460
  store i32 %conv11.prol, i32* %9, align 4, !dbg !1460
  %14 = call i32 @vprintf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %10) #9, !dbg !1460
  call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !1440, metadata !1002), !dbg !1450
  call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !1440, metadata !1002), !dbg !1450
  br label %for.body.prol.loopexit, !dbg !1452

for.body.prol.loopexit:                           ; preds = %for.body.lr.ph, %for.body.prol
  %i.029.unr = phi i32 [ 0, %for.body.lr.ph ], [ 1, %for.body.prol ]
  %15 = icmp eq i32 %2, 1, !dbg !1452
  br i1 %15, label %cleanup.loopexit, label %for.body.lr.ph.new, !dbg !1452

for.body.lr.ph.new:                               ; preds = %for.body.prol.loopexit
  br label %for.body, !dbg !1452

for.body:                                         ; preds = %for.body, %for.body.lr.ph.new
  %i.029 = phi i32 [ %i.029.unr, %for.body.lr.ph.new ], [ %inc.1, %for.body ]
  %idxprom5 = sext i32 %i.029 to i64, !dbg !1461
  %id = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5, i32 0, !dbg !1454
  %16 = load i32, i32* %id, align 4, !dbg !1454, !tbaa !1324
  %sline = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5, i32 1, !dbg !1456
  %17 = load i16, i16* %sline, align 4, !dbg !1456, !tbaa !1329
  %conv = sext i16 %17 to i32, !dbg !1457
  %scolm = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5, i32 2, !dbg !1458
  %18 = load i16, i16* %scolm, align 2, !dbg !1458, !tbaa !1332
  %conv11 = sext i16 %18 to i32, !dbg !1459
  store i32 %i.029, i32* %6, align 8, !dbg !1460
  store i32 %16, i32* %7, align 4, !dbg !1460
  store i32 %conv, i32* %8, align 8, !dbg !1460
  store i32 %conv11, i32* %9, align 4, !dbg !1460
  %19 = call i32 @vprintf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %10) #9, !dbg !1460
  %inc = add nuw nsw i32 %i.029, 1, !dbg !1462
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1440, metadata !1002), !dbg !1450
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1440, metadata !1002), !dbg !1450
  %idxprom5.1 = sext i32 %inc to i64, !dbg !1461
  %id.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5.1, i32 0, !dbg !1454
  %20 = load i32, i32* %id.1, align 4, !dbg !1454, !tbaa !1324
  %sline.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5.1, i32 1, !dbg !1456
  %21 = load i16, i16* %sline.1, align 4, !dbg !1456, !tbaa !1329
  %conv.1 = sext i16 %21 to i32, !dbg !1457
  %scolm.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5.1, i32 2, !dbg !1458
  %22 = load i16, i16* %scolm.1, align 2, !dbg !1458, !tbaa !1332
  %conv11.1 = sext i16 %22 to i32, !dbg !1459
  store i32 %inc, i32* %6, align 8, !dbg !1460
  store i32 %20, i32* %7, align 4, !dbg !1460
  store i32 %conv.1, i32* %8, align 8, !dbg !1460
  store i32 %conv11.1, i32* %9, align 4, !dbg !1460
  %23 = call i32 @vprintf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %10) #9, !dbg !1460
  %inc.1 = add nsw i32 %i.029, 2, !dbg !1462
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1440, metadata !1002), !dbg !1450
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1440, metadata !1002), !dbg !1450
  %exitcond.1 = icmp eq i32 %inc.1, %2, !dbg !1464
  br i1 %exitcond.1, label %cleanup.loopexit.unr-lcssa, label %for.body, !dbg !1452, !llvm.loop !1466

cleanup.loopexit.unr-lcssa:                       ; preds = %for.body
  br label %cleanup.loopexit, !dbg !1469

cleanup.loopexit:                                 ; preds = %for.body.prol.loopexit, %cleanup.loopexit.unr-lcssa
  br label %cleanup, !dbg !1469

cleanup:                                          ; preds = %cleanup.loopexit, %entry
  ret void, !dbg !1469
}

; Function Attrs: convergent nounwind
define i32 @_Z6holdonl(i64 %c) local_unnamed_addr #3 !dbg !1471 {
entry:
  tail call void @llvm.dbg.value(metadata i64 %c, i64 0, metadata !1478, metadata !1002), !dbg !1481
  %0 = tail call i32 asm sideeffect "mov.u32 \09$0, %clock;", "=r"() #10, !dbg !1482, !srcloc !1487
  tail call void @llvm.dbg.value(metadata i32 %0, i64 0, metadata !1485, metadata !1002) #9, !dbg !1488
  %conv = sext i32 %0 to i64, !dbg !1489
  tail call void @llvm.dbg.value(metadata i64 %conv, i64 0, metadata !1479, metadata !1002), !dbg !1490
  tail call void @llvm.dbg.value(metadata i64 0, i64 0, metadata !1480, metadata !1002), !dbg !1491
  tail call void @llvm.dbg.value(metadata i64 0, i64 0, metadata !1480, metadata !1002), !dbg !1491
  %cmp7 = icmp sgt i64 %c, 0, !dbg !1492
  br i1 %cmp7, label %while.body.preheader, label %while.end, !dbg !1494

while.body.preheader:                             ; preds = %entry
  br label %while.body, !dbg !1495

while.body:                                       ; preds = %while.body.preheader, %while.body
  %1 = tail call i32 asm sideeffect "mov.u32 \09$0, %clock;", "=r"() #10, !dbg !1495, !srcloc !1487
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !1485, metadata !1002) #9, !dbg !1497
  %conv2 = sext i32 %1 to i64, !dbg !1498
  %sub = sub nsw i64 %conv2, %conv, !dbg !1499
  tail call void @llvm.dbg.value(metadata i64 %sub, i64 0, metadata !1480, metadata !1002), !dbg !1491
  tail call void @llvm.dbg.value(metadata i64 %sub, i64 0, metadata !1480, metadata !1002), !dbg !1491
  %cmp = icmp slt i64 %sub, %c, !dbg !1492
  br i1 %cmp, label %while.body, label %while.end.loopexit, !dbg !1494, !llvm.loop !1500

while.end.loopexit:                               ; preds = %while.body
  %phitmp = trunc i64 %sub to i32, !dbg !1503
  br label %while.end, !dbg !1503

while.end:                                        ; preds = %while.end.loopexit, %entry
  %clock_offset.0.lcssa = phi i32 [ 0, %entry ], [ %phitmp, %while.end.loopexit ]
  ret i32 %clock_offset.0.lcssa, !dbg !1504
}

; Function Attrs: norecurse nounwind readnone
define void @InitKernel() local_unnamed_addr #6 !dbg !1505 {
entry:
  ret void, !dbg !1506
}

; Function Attrs: nounwind readnone
define void @callFunc(i8* nocapture %er, i8* nocapture %ee, i32 %sline, i32 %scolm) local_unnamed_addr #7 !dbg !1507 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %er, i64 0, metadata !1511, metadata !1002), !dbg !1515
  tail call void @llvm.dbg.value(metadata i8* %ee, i64 0, metadata !1512, metadata !1002), !dbg !1516
  tail call void @llvm.dbg.value(metadata i32 %sline, i64 0, metadata !1513, metadata !1002), !dbg !1517
  tail call void @llvm.dbg.value(metadata i32 %scolm, i64 0, metadata !1514, metadata !1002), !dbg !1518
  ret void, !dbg !1519
}

; Function Attrs: nounwind
define void @takeString(i8* %p, i32 %action) local_unnamed_addr #0 !dbg !1520 {
entry:
  %tmp = alloca %printf_args.2, align 8
  %tmp14 = alloca %printf_args.2, align 8
  %tmp18 = alloca %printf_args.2, align 8
  %tmp20 = alloca %printf_args.2, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1524, metadata !1002), !dbg !1526
  tail call void @llvm.dbg.value(metadata i32 %action, i64 0, metadata !1525, metadata !1002), !dbg !1527
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #9, !dbg !1528, !range !1120
  %cmp = icmp eq i32 %0, 0, !dbg !1532
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !1533

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #9, !dbg !1534, !range !1043
  %cmp2 = icmp eq i32 %1, 0, !dbg !1538
  br i1 %cmp2, label %lor.lhs.false3, label %return, !dbg !1539

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #9, !dbg !1540, !range !1120
  %cmp5 = icmp eq i32 %2, 0, !dbg !1544
  br i1 %cmp5, label %lor.lhs.false6, label %return, !dbg !1545

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #9, !dbg !1546, !range !1043
  %cmp8 = icmp eq i32 %3, 0, !dbg !1550
  br i1 %cmp8, label %if.end, label %return, !dbg !1551

if.end:                                           ; preds = %lor.lhs.false6
  %4 = load i8, i8* addrspacecast (i8 addrspace(1)* @VERBOSE to i8*), align 1, !dbg !1553, !tbaa !1555, !range !1557
  %tobool = icmp eq i8 %4, 0, !dbg !1553
  br i1 %tobool, label %return, label %if.then9, !dbg !1558

if.then9:                                         ; preds = %if.end
  switch i32 %action, label %if.else19 [
    i32 1, label %if.then11
    i32 2, label %if.then13
    i32 3, label %if.then17
  ], !dbg !1559

if.then11:                                        ; preds = %if.then9
  %5 = getelementptr inbounds %printf_args.2, %printf_args.2* %tmp, i64 0, i32 0, !dbg !1561
  store i8* %p, i8** %5, align 8, !dbg !1561
  %6 = bitcast %printf_args.2* %tmp to i8*, !dbg !1561
  %7 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.8, i64 0, i64 0), i8* nonnull %6) #9, !dbg !1561
  br label %return, !dbg !1561

if.then13:                                        ; preds = %if.then9
  %8 = getelementptr inbounds %printf_args.2, %printf_args.2* %tmp14, i64 0, i32 0, !dbg !1563
  store i8* %p, i8** %8, align 8, !dbg !1563
  %9 = bitcast %printf_args.2* %tmp14 to i8*, !dbg !1563
  %10 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.9, i64 0, i64 0), i8* nonnull %9) #9, !dbg !1563
  br label %return, !dbg !1563

if.then17:                                        ; preds = %if.then9
  %11 = getelementptr inbounds %printf_args.2, %printf_args.2* %tmp18, i64 0, i32 0, !dbg !1565
  store i8* %p, i8** %11, align 8, !dbg !1565
  %12 = bitcast %printf_args.2* %tmp18 to i8*, !dbg !1565
  %13 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.10, i64 0, i64 0), i8* nonnull %12) #9, !dbg !1565
  br label %return, !dbg !1565

if.else19:                                        ; preds = %if.then9
  %14 = getelementptr inbounds %printf_args.2, %printf_args.2* %tmp20, i64 0, i32 0, !dbg !1567
  store i8* %p, i8** %14, align 8, !dbg !1567
  %15 = bitcast %printf_args.2* %tmp20 to i8*, !dbg !1567
  %16 = call i32 @vprintf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.11, i64 0, i64 0), i8* nonnull %15) #9, !dbg !1567
  br label %return

return:                                           ; preds = %if.end, %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry, %if.then13, %if.else19, %if.then17, %if.then11
  ret void, !dbg !1568
}

; Function Attrs: convergent nounwind
define void @_Z8cxtprinti(i32 %id) local_unnamed_addr #3 !dbg !1569 {
entry:
  %tmp = alloca %printf_args.6, align 8
  %tmp22 = alloca %printf_args.7, align 8
  tail call void @llvm.dbg.value(metadata i32 %id, i64 0, metadata !1571, metadata !1002), !dbg !1574
  %0 = load i32, i32* addrspacecast (i32 addrspace(1)* @cHeight to i32*), align 4, !dbg !1575, !tbaa !1237
  %cmp = icmp sgt i32 %0, %id, !dbg !1575
  br i1 %cmp, label %cond.end, label %cond.false, !dbg !1575

cond.false:                                       ; preds = %entry
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.12, i64 0, i64 0), i32 338, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @__PRETTY_FUNCTION__._Z8cxtprinti, i64 0, i64 0)) #10, !dbg !1576
  unreachable

cond.end:                                         ; preds = %entry
  %cmp1 = icmp slt i32 %id, 0, !dbg !1578
  br i1 %cmp1, label %return, label %if.end, !dbg !1580

if.end:                                           ; preds = %cond.end
  %1 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 0, !dbg !1581
  store i32 %id, i32* %1, align 8, !dbg !1581
  %2 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 1, !dbg !1581
  store i32 %0, i32* %2, align 4, !dbg !1581
  %3 = bitcast %printf_args.6* %tmp to i8*, !dbg !1581
  %4 = call i32 @vprintf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.13, i64 0, i64 0), i8* nonnull %3) #9, !dbg !1581
  call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1572, metadata !1002), !dbg !1582
  %idxprom = sext i32 %id to i64, !dbg !1583
  call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1572, metadata !1002), !dbg !1582
  %5 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 0
  %6 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 1
  %7 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 2
  %8 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 3
  %9 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 4
  %10 = bitcast %printf_args.7* %tmp22 to i8*
  br label %land.rhs, !dbg !1586

land.rhs:                                         ; preds = %if.end, %for.body
  %i.039 = phi i32 [ 0, %if.end ], [ %inc, %for.body ]
  %idxprom4 = sext i32 %i.039 to i64, !dbg !1583
  %arrayidx538 = getelementptr inbounds [20 x [5 x %struct.CallSite_t]], [20 x [5 x %struct.CallSite_t]] addrspace(1)* @contextDic, i64 0, i64 %idxprom, i64 %idxprom4, !dbg !1583
  %arrayidx5 = addrspacecast %struct.CallSite_t addrspace(1)* %arrayidx538 to %struct.CallSite_t*, !dbg !1583
  %id6 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %arrayidx5, i64 0, i32 0, !dbg !1588
  %11 = load i32, i32* %id6, align 4, !dbg !1588, !tbaa !1324
  %cmp7 = icmp eq i32 %11, -1, !dbg !1589
  br i1 %cmp7, label %return.loopexit, label %for.body, !dbg !1590

for.body:                                         ; preds = %land.rhs
  %sline = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %arrayidx5, i64 0, i32 1, !dbg !1592
  %12 = load i16, i16* %sline, align 4, !dbg !1592, !tbaa !1329
  %conv = sext i16 %12 to i32, !dbg !1594
  %scolm = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %arrayidx5, i64 0, i32 2, !dbg !1595
  %13 = load i16, i16* %scolm, align 2, !dbg !1595, !tbaa !1332
  %conv21 = sext i16 %13 to i32, !dbg !1596
  store i32 %id, i32* %5, align 8, !dbg !1597
  store i32 %i.039, i32* %6, align 4, !dbg !1597
  store i32 %11, i32* %7, align 8, !dbg !1597
  store i32 %conv, i32* %8, align 4, !dbg !1597
  store i32 %conv21, i32* %9, align 8, !dbg !1597
  %14 = call i32 @vprintf(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.14, i64 0, i64 0), i8* nonnull %10) #9, !dbg !1597
  %inc = add nuw nsw i32 %i.039, 1, !dbg !1598
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1572, metadata !1002), !dbg !1582
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1572, metadata !1002), !dbg !1582
  %cmp3 = icmp slt i32 %inc, 15, !dbg !1600
  br i1 %cmp3, label %land.rhs, label %return.loopexit, !dbg !1586, !llvm.loop !1601

return.loopexit:                                  ; preds = %land.rhs, %for.body
  br label %return, !dbg !1604

return:                                           ; preds = %return.loopexit, %cond.end
  ret void, !dbg !1604
}

; Function Attrs: convergent nounwind
define void @_Z6cxtcpyP10CallSite_tS0_i(%struct.CallSite_t* nocapture %dst, %struct.CallSite_t* nocapture readonly %src, i32 %height) local_unnamed_addr #3 !dbg !1605 {
entry:
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %dst, i64 0, metadata !1609, metadata !1002), !dbg !1613
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %src, i64 0, metadata !1610, metadata !1002), !dbg !1614
  tail call void @llvm.dbg.value(metadata i32 %height, i64 0, metadata !1611, metadata !1002), !dbg !1615
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1612, metadata !1002), !dbg !1616
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1612, metadata !1002), !dbg !1616
  %cmp13 = icmp sgt i32 %height, 0, !dbg !1617
  br i1 %cmp13, label %for.body.preheader, label %cond.end, !dbg !1621

for.body.preheader:                               ; preds = %entry
  %0 = add i32 %height, -1, !dbg !1623
  %xtraiter = and i32 %height, 3, !dbg !1623
  %lcmp.mod = icmp eq i32 %xtraiter, 0, !dbg !1623
  br i1 %lcmp.mod, label %for.body.prol.loopexit, label %for.body.prol.preheader, !dbg !1623

for.body.prol.preheader:                          ; preds = %for.body.preheader
  br label %for.body.prol, !dbg !1623

for.body.prol:                                    ; preds = %for.body.prol, %for.body.prol.preheader
  %i.014.prol = phi i32 [ %inc.prol, %for.body.prol ], [ 0, %for.body.prol.preheader ]
  %prol.iter = phi i32 [ %prol.iter.sub, %for.body.prol ], [ %xtraiter, %for.body.prol.preheader ]
  %idxprom.prol = sext i32 %i.014.prol to i64, !dbg !1623
  %arrayidx.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom.prol, !dbg !1623
  %arrayidx2.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom.prol, !dbg !1624
  %1 = bitcast %struct.CallSite_t* %arrayidx.prol to i64*, !dbg !1625
  %2 = bitcast %struct.CallSite_t* %arrayidx2.prol to i64*, !dbg !1625
  %3 = load i64, i64* %1, align 4, !dbg !1625
  store i64 %3, i64* %2, align 4, !dbg !1625
  %inc.prol = add nuw nsw i32 %i.014.prol, 1, !dbg !1626
  tail call void @llvm.dbg.value(metadata i32 %inc.prol, i64 0, metadata !1612, metadata !1002), !dbg !1616
  tail call void @llvm.dbg.value(metadata i32 %inc.prol, i64 0, metadata !1612, metadata !1002), !dbg !1616
  %prol.iter.sub = add i32 %prol.iter, -1, !dbg !1621
  %prol.iter.cmp = icmp eq i32 %prol.iter.sub, 0, !dbg !1621
  br i1 %prol.iter.cmp, label %for.body.prol.loopexit.unr-lcssa, label %for.body.prol, !dbg !1621, !llvm.loop !1628

for.body.prol.loopexit.unr-lcssa:                 ; preds = %for.body.prol
  br label %for.body.prol.loopexit, !dbg !1623

for.body.prol.loopexit:                           ; preds = %for.body.preheader, %for.body.prol.loopexit.unr-lcssa
  %i.014.unr = phi i32 [ 0, %for.body.preheader ], [ %inc.prol, %for.body.prol.loopexit.unr-lcssa ]
  %4 = icmp ult i32 %0, 3, !dbg !1623
  br i1 %4, label %for.end, label %for.body.preheader.new, !dbg !1623

for.body.preheader.new:                           ; preds = %for.body.prol.loopexit
  br label %for.body, !dbg !1623

for.body:                                         ; preds = %for.body, %for.body.preheader.new
  %i.014 = phi i32 [ %i.014.unr, %for.body.preheader.new ], [ %inc.3, %for.body ]
  %idxprom = sext i32 %i.014 to i64, !dbg !1623
  %arrayidx = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom, !dbg !1623
  %arrayidx2 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom, !dbg !1624
  %5 = bitcast %struct.CallSite_t* %arrayidx to i64*, !dbg !1625
  %6 = bitcast %struct.CallSite_t* %arrayidx2 to i64*, !dbg !1625
  %7 = load i64, i64* %5, align 4, !dbg !1625
  store i64 %7, i64* %6, align 4, !dbg !1625
  %inc = add nuw nsw i32 %i.014, 1, !dbg !1626
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1612, metadata !1002), !dbg !1616
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1612, metadata !1002), !dbg !1616
  %idxprom.1 = sext i32 %inc to i64, !dbg !1623
  %arrayidx.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom.1, !dbg !1623
  %arrayidx2.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom.1, !dbg !1624
  %8 = bitcast %struct.CallSite_t* %arrayidx.1 to i64*, !dbg !1625
  %9 = bitcast %struct.CallSite_t* %arrayidx2.1 to i64*, !dbg !1625
  %10 = load i64, i64* %8, align 4, !dbg !1625
  store i64 %10, i64* %9, align 4, !dbg !1625
  %inc.1 = add nsw i32 %i.014, 2, !dbg !1626
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1612, metadata !1002), !dbg !1616
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1612, metadata !1002), !dbg !1616
  %idxprom.2 = sext i32 %inc.1 to i64, !dbg !1623
  %arrayidx.2 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom.2, !dbg !1623
  %arrayidx2.2 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom.2, !dbg !1624
  %11 = bitcast %struct.CallSite_t* %arrayidx.2 to i64*, !dbg !1625
  %12 = bitcast %struct.CallSite_t* %arrayidx2.2 to i64*, !dbg !1625
  %13 = load i64, i64* %11, align 4, !dbg !1625
  store i64 %13, i64* %12, align 4, !dbg !1625
  %inc.2 = add nsw i32 %i.014, 3, !dbg !1626
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1612, metadata !1002), !dbg !1616
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1612, metadata !1002), !dbg !1616
  %idxprom.3 = sext i32 %inc.2 to i64, !dbg !1623
  %arrayidx.3 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom.3, !dbg !1623
  %arrayidx2.3 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom.3, !dbg !1624
  %14 = bitcast %struct.CallSite_t* %arrayidx.3 to i64*, !dbg !1625
  %15 = bitcast %struct.CallSite_t* %arrayidx2.3 to i64*, !dbg !1625
  %16 = load i64, i64* %14, align 4, !dbg !1625
  store i64 %16, i64* %15, align 4, !dbg !1625
  %inc.3 = add nsw i32 %i.014, 4, !dbg !1626
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1612, metadata !1002), !dbg !1616
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1612, metadata !1002), !dbg !1616
  %exitcond.3 = icmp eq i32 %inc.3, %height, !dbg !1617
  br i1 %exitcond.3, label %for.end.unr-lcssa, label %for.body, !dbg !1621, !llvm.loop !1630

for.end.unr-lcssa:                                ; preds = %for.body
  br label %for.end, !dbg !1633

for.end:                                          ; preds = %for.body.prol.loopexit, %for.end.unr-lcssa
  %cmp3 = icmp slt i32 %height, 15, !dbg !1633
  br i1 %cmp3, label %cond.end, label %cond.false, !dbg !1633

cond.false:                                       ; preds = %for.end
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.15, i64 0, i64 0), i32 359, i8* getelementptr inbounds ([45 x i8], [45 x i8]* @__PRETTY_FUNCTION__._Z6cxtcpyP10CallSite_tS0_i, i64 0, i64 0)) #10, !dbg !1634
  unreachable

cond.end:                                         ; preds = %entry, %for.end
  %i.0.lcssa16 = phi i32 [ %height, %for.end ], [ 0, %entry ]
  %idxprom4 = sext i32 %i.0.lcssa16 to i64, !dbg !1636
  %id = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom4, i32 0, !dbg !1637
  store i32 -1, i32* %id, align 4, !dbg !1638, !tbaa !1324
  ret void, !dbg !1639
}

; Function Attrs: nounwind readonly
define zeroext i1 @_Z6cxtcmpP10CallSite_tS0_i(%struct.CallSite_t* nocapture readonly %dst, %struct.CallSite_t* nocapture readonly %src, i32 %height) local_unnamed_addr #2 !dbg !1640 {
entry:
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %dst, i64 0, metadata !1644, metadata !1002), !dbg !1649
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %src, i64 0, metadata !1645, metadata !1002), !dbg !1650
  tail call void @llvm.dbg.value(metadata i32 %height, i64 0, metadata !1646, metadata !1002), !dbg !1651
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1647, metadata !1002), !dbg !1652
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1647, metadata !1002), !dbg !1652
  %cmp9 = icmp sgt i32 %height, 0, !dbg !1653
  br i1 %cmp9, label %for.body.preheader, label %cleanup, !dbg !1656

for.body.preheader:                               ; preds = %entry
  br label %for.body, !dbg !1658

for.cond:                                         ; preds = %for.body
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1647, metadata !1002), !dbg !1652
  %cmp = icmp slt i32 %inc, %height, !dbg !1653
  br i1 %cmp, label %for.body, label %cleanup.loopexit, !dbg !1656, !llvm.loop !1660

for.body:                                         ; preds = %for.body.preheader, %for.cond
  %i.010 = phi i32 [ %inc, %for.cond ], [ 0, %for.body.preheader ]
  %idxprom = sext i32 %i.010 to i64, !dbg !1658
  %id = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom, i32 0, !dbg !1663
  %0 = load i32, i32* %id, align 4, !dbg !1663, !tbaa !1324
  %id3 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom, i32 0, !dbg !1664
  %1 = load i32, i32* %id3, align 4, !dbg !1664, !tbaa !1324
  %cmp4 = icmp eq i32 %0, %1, !dbg !1665
  %inc = add nuw nsw i32 %i.010, 1, !dbg !1666
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1647, metadata !1002), !dbg !1652
  br i1 %cmp4, label %for.cond, label %cleanup.loopexit, !dbg !1668

cleanup.loopexit:                                 ; preds = %for.cond, %for.body
  %.ph = phi i1 [ true, %for.cond ], [ false, %for.body ]
  br label %cleanup, !dbg !1669

cleanup:                                          ; preds = %cleanup.loopexit, %entry
  %2 = phi i1 [ true, %entry ], [ %.ph, %cleanup.loopexit ]
  ret i1 %2, !dbg !1669
}

; Function Attrs: norecurse nounwind readnone
define i32 @getContextID() local_unnamed_addr #6 !dbg !1670 {
entry:
  ret i32 -1, !dbg !1671
}

; Function Attrs: convergent nounwind
define void @passBasicBlock(i8* nocapture readonly %p, i32 %action, i32 %sline, i32 %scolm) local_unnamed_addr #3 !dbg !1672 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1676, metadata !1002), !dbg !1691
  tail call void @llvm.dbg.value(metadata i32 %action, i64 0, metadata !1677, metadata !1002), !dbg !1692
  tail call void @llvm.dbg.value(metadata i32 %sline, i64 0, metadata !1678, metadata !1002), !dbg !1693
  tail call void @llvm.dbg.value(metadata i32 %scolm, i64 0, metadata !1679, metadata !1002), !dbg !1694
  %0 = load i64, i64* addrspacecast (i64 addrspace(1)* @bbccnntt to i64*), align 8, !dbg !1695, !tbaa !1696
  %cmp = icmp ult i64 %0, 67108736, !dbg !1695
  br i1 %cmp, label %cond.end, label %cond.false, !dbg !1695

cond.false:                                       ; preds = %entry
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.16, i64 0, i64 0), i32 437, i8* getelementptr inbounds ([43 x i8], [43 x i8]* @__PRETTY_FUNCTION__.passBasicBlock, i64 0, i64 0)) #10, !dbg !1698
  unreachable

cond.end:                                         ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #9, !dbg !1700, !range !1043
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #9, !dbg !1703, !range !1043
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #9, !dbg !1706, !range !1735
  %mul = mul nuw i32 %3, %2, !dbg !1736
  %add = add i32 %mul, %1, !dbg !1737
  %4 = load i32, i32* addrspacecast (i32 addrspace(1)* @CTALB to i32*), align 4, !dbg !1738, !tbaa !1237
  %cmp3 = icmp ult i32 %add, %4, !dbg !1739
  br i1 %cmp3, label %return, label %lor.lhs.false, !dbg !1740

lor.lhs.false:                                    ; preds = %cond.end
  %5 = load i32, i32* addrspacecast (i32 addrspace(1)* @CTAUB to i32*), align 4, !dbg !1741, !tbaa !1237
  %cmp9 = icmp ugt i32 %add, %5, !dbg !1743
  br i1 %cmp9, label %return, label %if.end, !dbg !1744

if.end:                                           ; preds = %lor.lhs.false
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1680, metadata !1002), !dbg !1746
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1747, metadata !1002), !dbg !1755
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1757, metadata !1002), !dbg !1761
  %6 = atomicrmw add i64* addrspacecast (i64 addrspace(1)* @bbccnntt to i64*), i64 1 seq_cst, !dbg !1763
  tail call void @llvm.dbg.value(metadata i64 0, i64 0, metadata !1682, metadata !1002), !dbg !1764
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1683, metadata !1002), !dbg !1765
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1684, metadata !1002), !dbg !1766
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1685, metadata !1002), !dbg !1767
  tail call void @llvm.dbg.value(metadata i64 0, i64 0, metadata !1682, metadata !1002), !dbg !1764
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1684, metadata !1002), !dbg !1766
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1685, metadata !1002), !dbg !1767
  %7 = load i8, i8* %p, align 1, !dbg !1768, !tbaa !1182
  %cmp1288 = icmp eq i8 %7, 0, !dbg !1770
  br i1 %cmp1288, label %for.cond.cleanup, label %for.body.preheader, !dbg !1771

for.body.preheader:                               ; preds = %if.end
  br label %for.body, !dbg !1773

for.cond.cleanup.loopexit:                        ; preds = %cleanup
  br label %for.cond.cleanup, !dbg !1775

for.cond.cleanup:                                 ; preds = %for.cond.cleanup.loopexit, %if.end
  %key.0.lcssa = phi i64 [ 0, %if.end ], [ %key.1, %for.cond.cleanup.loopexit ]
  tail call void @llvm.dbg.value(metadata %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 0, metadata !1690, metadata !1002), !dbg !1775
  %sext = shl i64 %6, 32, !dbg !1776
  %idxprom = ashr exact i64 %sext, 32, !dbg !1776
  %key27 = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 4, !dbg !1777
  store i64 %key.0.lcssa, i64* %key27, align 8, !dbg !1778, !tbaa !1779
  %8 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #9, !dbg !1781, !range !1120
  %conv29 = trunc i32 %8 to i16, !dbg !1783
  %tidx = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 2, !dbg !1784
  store i16 %conv29, i16* %tidx, align 4, !dbg !1785, !tbaa !1786
  %9 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #9, !dbg !1787, !range !1120
  %conv33 = trunc i32 %9 to i16, !dbg !1789
  %tidy = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 3, !dbg !1790
  store i16 %conv33, i16* %tidy, align 2, !dbg !1791, !tbaa !1792
  %conv37 = trunc i32 %1 to i16, !dbg !1793
  %bidx = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 0, !dbg !1794
  store i16 %conv37, i16* %bidx, align 8, !dbg !1795, !tbaa !1796
  %conv41 = trunc i32 %2 to i16, !dbg !1797
  %bidy = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 1, !dbg !1798
  store i16 %conv41, i16* %bidy, align 2, !dbg !1799, !tbaa !1800
  %sline46 = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 5, !dbg !1801
  store i32 %sline, i32* %sline46, align 8, !dbg !1802, !tbaa !1803
  %scolm49 = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 6, !dbg !1804
  store i32 %scolm, i32* %scolm49, align 4, !dbg !1805, !tbaa !1806
  %cid = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 7, !dbg !1807
  store i32 -1, i32* %cid, align 8, !dbg !1808, !tbaa !1809
  br label %return

for.body:                                         ; preds = %for.body.preheader, %cleanup
  %10 = phi i8 [ %13, %cleanup ], [ %7, %for.body.preheader ]
  %key.091 = phi i64 [ %key.1, %cleanup ], [ 0, %for.body.preheader ]
  %factor.090 = phi i64 [ %factor.1, %cleanup ], [ 1, %for.body.preheader ]
  %i.089 = phi i32 [ %inc, %cleanup ], [ 0, %for.body.preheader ]
  %.off = add i8 %10, -48, !dbg !1773
  %11 = icmp ugt i8 %.off, 75, !dbg !1773
  tail call void @llvm.dbg.value(metadata i64 %add23, i64 0, metadata !1682, metadata !1002), !dbg !1764
  tail call void @llvm.dbg.value(metadata i64 %mul25, i64 0, metadata !1684, metadata !1002), !dbg !1766
  br i1 %11, label %cleanup, label %if.end20, !dbg !1773

if.end20:                                         ; preds = %for.body
  %conv21 = sext i8 %10 to i64, !dbg !1810
  %mul22 = mul nsw i64 %factor.090, %conv21, !dbg !1811
  %add23 = add i64 %mul22, %key.091, !dbg !1812
  %12 = load i32, i32* addrspacecast (i32 addrspace(1)* @CONSTANCE to i32*), align 4, !dbg !1813, !tbaa !1237
  %conv24 = sext i32 %12 to i64, !dbg !1813
  %mul25 = mul nsw i64 %factor.090, %conv24, !dbg !1814
  br label %cleanup, !dbg !1815

cleanup:                                          ; preds = %for.body, %if.end20
  %factor.1 = phi i64 [ %mul25, %if.end20 ], [ %factor.090, %for.body ]
  %key.1 = phi i64 [ %add23, %if.end20 ], [ %key.091, %for.body ]
  tail call void @llvm.dbg.value(metadata i64 %key.1, i64 0, metadata !1682, metadata !1002), !dbg !1764
  tail call void @llvm.dbg.value(metadata i64 %factor.1, i64 0, metadata !1684, metadata !1002), !dbg !1766
  %inc = add nuw nsw i32 %i.089, 1, !dbg !1816
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1685, metadata !1002), !dbg !1767
  tail call void @llvm.dbg.value(metadata i64 %key.1, i64 0, metadata !1682, metadata !1002), !dbg !1764
  tail call void @llvm.dbg.value(metadata i64 %factor.1, i64 0, metadata !1684, metadata !1002), !dbg !1766
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1685, metadata !1002), !dbg !1767
  %idx.ext = sext i32 %inc to i64, !dbg !1818
  %add.ptr = getelementptr inbounds i8, i8* %p, i64 %idx.ext, !dbg !1818
  %13 = load i8, i8* %add.ptr, align 1, !dbg !1768, !tbaa !1182
  %cmp12 = icmp eq i8 %13, 0, !dbg !1770
  br i1 %cmp12, label %for.cond.cleanup.loopexit, label %for.body, !dbg !1771, !llvm.loop !1819

return:                                           ; preds = %cond.end, %lor.lhs.false, %for.cond.cleanup
  ret void, !dbg !1822
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #1

; Function Attrs: convergent nounwind
define void @_Z10storeLinesPvssss(i8* %p, i16 signext %size, i16 signext %line, i16 signext %colmn, i16 signext %op) local_unnamed_addr #3 !dbg !1823 {
entry:
  %tmp = alloca %printf_args.8, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1827, metadata !1002), !dbg !1835
  tail call void @llvm.dbg.value(metadata i16 %size, i64 0, metadata !1828, metadata !1002), !dbg !1836
  tail call void @llvm.dbg.value(metadata i16 %line, i64 0, metadata !1829, metadata !1002), !dbg !1837
  tail call void @llvm.dbg.value(metadata i16 %colmn, i64 0, metadata !1830, metadata !1002), !dbg !1838
  tail call void @llvm.dbg.value(metadata i16 %op, i64 0, metadata !1831, metadata !1002), !dbg !1839
  %0 = load i64, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !1840, !tbaa !1696
  %cmp = icmp ult i64 %0, 67108736, !dbg !1840
  br i1 %cmp, label %cond.end, label %cond.false, !dbg !1840

cond.false:                                       ; preds = %entry
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.17, i64 0, i64 0), i32 498, i8* getelementptr inbounds ([52 x i8], [52 x i8]* @__PRETTY_FUNCTION__._Z10storeLinesPvssss, i64 0, i64 0)) #10, !dbg !1841
  unreachable

cond.end:                                         ; preds = %entry
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1747, metadata !1002), !dbg !1843
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1757, metadata !1002), !dbg !1845
  %1 = atomicrmw add i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), i64 1 seq_cst, !dbg !1847
  %conv = trunc i64 %1 to i32, !dbg !1848
  tail call void @llvm.dbg.value(metadata i32 %conv, i64 0, metadata !1832, metadata !1002), !dbg !1849
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !1833, metadata !1002), !dbg !1850
  tail call void @llvm.dbg.value(metadata i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 0, metadata !1834, metadata !1002), !dbg !1851
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #9, !dbg !1852, !range !1043
  %conv2 = trunc i32 %2 to i16, !dbg !1854
  %mul = mul nsw i32 %conv, 12, !dbg !1855
  %idxprom = sext i32 %mul to i64, !dbg !1856
  %arrayidx = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom, !dbg !1856
  store i16 %conv2, i16* %arrayidx, align 2, !dbg !1857, !tbaa !1858
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #9, !dbg !1859, !range !1043
  %conv4 = trunc i32 %3 to i16, !dbg !1861
  %add6 = or i32 %mul, 1, !dbg !1862
  %idxprom7 = sext i32 %add6 to i64, !dbg !1863
  %arrayidx8 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom7, !dbg !1863
  store i16 %conv4, i16* %arrayidx8, align 2, !dbg !1864, !tbaa !1858
  %4 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #9, !dbg !1865, !range !1120
  %conv10 = trunc i32 %4 to i16, !dbg !1867
  %add12 = or i32 %mul, 2, !dbg !1868
  %idxprom13 = sext i32 %add12 to i64, !dbg !1869
  %arrayidx14 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom13, !dbg !1869
  store i16 %conv10, i16* %arrayidx14, align 2, !dbg !1870, !tbaa !1858
  %5 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #9, !dbg !1871, !range !1120
  %conv16 = trunc i32 %5 to i16, !dbg !1873
  %add18 = or i32 %mul, 3, !dbg !1874
  %idxprom19 = sext i32 %add18 to i64, !dbg !1875
  %arrayidx20 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom19, !dbg !1875
  store i16 %conv16, i16* %arrayidx20, align 2, !dbg !1876, !tbaa !1858
  %6 = ptrtoint i8* %p to i64, !dbg !1877
  %add22 = mul i64 %1, 12884901888, !dbg !1878
  %sext = add i64 %add22, 4294967296, !dbg !1878
  %idxprom23 = ashr exact i64 %sext, 32, !dbg !1878
  %arrayidx24 = getelementptr inbounds i64, i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 %idxprom23, !dbg !1878
  store i64 %6, i64* %arrayidx24, align 8, !dbg !1879, !tbaa !1880
  %add26 = add nsw i32 %mul, 8, !dbg !1882
  %idxprom27 = sext i32 %add26 to i64, !dbg !1883
  %arrayidx28 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom27, !dbg !1883
  store i16 %size, i16* %arrayidx28, align 2, !dbg !1884, !tbaa !1858
  %add30 = add nsw i32 %mul, 9, !dbg !1885
  %idxprom31 = sext i32 %add30 to i64, !dbg !1886
  %arrayidx32 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom31, !dbg !1886
  store i16 %line, i16* %arrayidx32, align 2, !dbg !1887, !tbaa !1858
  %add34 = add nsw i32 %mul, 10, !dbg !1888
  %idxprom35 = sext i32 %add34 to i64, !dbg !1889
  %arrayidx36 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom35, !dbg !1889
  store i16 %colmn, i16* %arrayidx36, align 2, !dbg !1890, !tbaa !1858
  %add38 = add nsw i32 %mul, 11, !dbg !1891
  %idxprom39 = sext i32 %add38 to i64, !dbg !1892
  %arrayidx40 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom39, !dbg !1892
  store i16 %op, i16* %arrayidx40, align 2, !dbg !1893, !tbaa !1858
  %cmp42 = icmp slt i32 %conv, 5, !dbg !1894
  br i1 %cmp42, label %if.then, label %if.end, !dbg !1896

if.then:                                          ; preds = %cond.end
  %7 = getelementptr inbounds %printf_args.8, %printf_args.8* %tmp, i64 0, i32 0, !dbg !1897
  store i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 0), i32** %7, align 8, !dbg !1897
  %8 = bitcast %printf_args.8* %tmp to i8*, !dbg !1897
  %9 = call i32 @vprintf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.18, i64 0, i64 0), i8* nonnull %8) #9, !dbg !1897
  br label %if.end, !dbg !1897

if.end:                                           ; preds = %if.then, %cond.end
  ret void, !dbg !1898
}

; Function Attrs: nounwind
define void @_Z9dumpLinesv() local_unnamed_addr #0 !dbg !1899 {
entry:
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #9, !dbg !1902, !range !1120
  %cmp = icmp eq i32 %0, 0, !dbg !1905
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !1906

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #9, !dbg !1907, !range !1043
  %cmp2 = icmp eq i32 %1, 0, !dbg !1910
  br i1 %cmp2, label %lor.lhs.false3, label %return, !dbg !1911

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #9, !dbg !1912, !range !1120
  %cmp5 = icmp eq i32 %2, 0, !dbg !1915
  br i1 %cmp5, label %lor.lhs.false6, label %return, !dbg !1916

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #9, !dbg !1917, !range !1043
  %cmp8 = icmp eq i32 %3, 0, !dbg !1920
  br i1 %cmp8, label %for.cond.preheader, label %return, !dbg !1921

for.cond.preheader:                               ; preds = %lor.lhs.false6
  %4 = tail call i32 @vprintf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.19, i64 0, i64 0), i8* null) #9, !dbg !1923
  br label %return, !dbg !1924

return:                                           ; preds = %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry, %for.cond.preheader
  ret void, !dbg !1925
}

; Function Attrs: nounwind
define void @_Z6print1i(i32 %a) local_unnamed_addr #0 !dbg !1927 {
entry:
  %tmp = alloca %printf_args.6, align 8
  %tmp14 = alloca %printf_args.6, align 8
  tail call void @llvm.dbg.value(metadata i32 %a, i64 0, metadata !1929, metadata !1002), !dbg !1930
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #9, !dbg !1931, !range !1120
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #9, !dbg !1934, !range !1120
  %add = add nuw nsw i32 %1, %0, !dbg !1937
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #9, !dbg !1938, !range !1043
  %add3 = add nuw nsw i32 %add, %2, !dbg !1941
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #9, !dbg !1942, !range !1043
  %add5 = sub nsw i32 0, %3, !dbg !1945
  %cmp = icmp eq i32 %add3, %add5, !dbg !1945
  br i1 %cmp, label %land.lhs.true, label %if.end17, !dbg !1946

land.lhs.true:                                    ; preds = %entry
  %4 = load i8, i8* addrspacecast (i8 addrspace(1)* @VERBOSE to i8*), align 1, !dbg !1947, !tbaa !1555, !range !1557
  %tobool = icmp eq i8 %4, 0, !dbg !1947
  br i1 %tobool, label %if.end17, label %if.then, !dbg !1949

if.then:                                          ; preds = %land.lhs.true
  switch i32 %a, label %if.else15 [
    i32 1, label %if.then7
    i32 2, label %if.then11
  ], !dbg !1951

if.then7:                                         ; preds = %if.then
  %5 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 0, !dbg !1953
  store i32 %2, i32* %5, align 8, !dbg !1953
  %6 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 1, !dbg !1953
  store i32 %3, i32* %6, align 4, !dbg !1953
  %7 = bitcast %printf_args.6* %tmp to i8*, !dbg !1953
  %8 = call i32 @vprintf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.20, i64 0, i64 0), i8* nonnull %7) #9, !dbg !1955
  br label %if.end17, !dbg !1953

if.then11:                                        ; preds = %if.then
  %9 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp14, i64 0, i32 0, !dbg !1957
  store i32 %2, i32* %9, align 8, !dbg !1957
  %10 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp14, i64 0, i32 1, !dbg !1957
  store i32 %3, i32* %10, align 4, !dbg !1957
  %11 = bitcast %printf_args.6* %tmp14 to i8*, !dbg !1957
  %12 = call i32 @vprintf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.21, i64 0, i64 0), i8* nonnull %11) #9, !dbg !1959
  br label %if.end17, !dbg !1957

if.else15:                                        ; preds = %if.then
  %13 = tail call i32 @vprintf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.22, i64 0, i64 0), i8* null) #9, !dbg !1961
  br label %if.end17

if.end17:                                         ; preds = %land.lhs.true, %if.then7, %if.else15, %if.then11, %entry
  ret void, !dbg !1962
}

; Function Attrs: nounwind
define void @_Z6print2v() local_unnamed_addr #0 !dbg !1963 {
entry:
  %tmp = alloca %printf_args.6, align 8
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #9, !dbg !1964, !range !1120
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #9, !dbg !1967, !range !1120
  %add = add nuw nsw i32 %1, %0, !dbg !1970
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #9, !dbg !1971, !range !1043
  %add3 = add nuw nsw i32 %add, %2, !dbg !1974
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #9, !dbg !1975, !range !1043
  %add5 = sub nsw i32 0, %3, !dbg !1978
  %cmp = icmp eq i32 %add3, %add5, !dbg !1978
  br i1 %cmp, label %land.lhs.true, label %if.end, !dbg !1979

land.lhs.true:                                    ; preds = %entry
  %4 = load i8, i8* addrspacecast (i8 addrspace(1)* @VERBOSE to i8*), align 1, !dbg !1980, !tbaa !1555, !range !1557
  %tobool = icmp eq i8 %4, 0, !dbg !1980
  br i1 %tobool, label %if.end, label %if.then, !dbg !1982

if.then:                                          ; preds = %land.lhs.true
  %5 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 0, !dbg !1984
  store i32 %2, i32* %5, align 8, !dbg !1984
  %6 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 1, !dbg !1984
  store i32 %3, i32* %6, align 4, !dbg !1984
  %7 = bitcast %printf_args.6* %tmp to i8*, !dbg !1984
  %8 = call i32 @vprintf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.21, i64 0, i64 0), i8* nonnull %7) #9, !dbg !1985
  br label %if.end, !dbg !1984

if.end:                                           ; preds = %land.lhs.true, %if.then, %entry
  ret void, !dbg !1986
}

; Function Attrs: nounwind
define void @_Z6print3ii(i32 %line, i32 %col) local_unnamed_addr #0 !dbg !1987 {
entry:
  %tmp = alloca %printf_args.1, align 8
  tail call void @llvm.dbg.value(metadata i32 %line, i64 0, metadata !1991, metadata !1002), !dbg !1993
  tail call void @llvm.dbg.value(metadata i32 %col, i64 0, metadata !1992, metadata !1002), !dbg !1994
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #9, !dbg !1995, !range !1120
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #9, !dbg !1998, !range !1120
  %add = add nuw nsw i32 %1, %0, !dbg !2001
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #9, !dbg !2002, !range !1043
  %add3 = add nuw nsw i32 %add, %2, !dbg !2005
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #9, !dbg !2006, !range !1043
  %add5 = sub nsw i32 0, %3, !dbg !2009
  %cmp = icmp eq i32 %add3, %add5, !dbg !2009
  br i1 %cmp, label %land.lhs.true, label %if.end, !dbg !2010

land.lhs.true:                                    ; preds = %entry
  %4 = load i8, i8* addrspacecast (i8 addrspace(1)* @VERBOSE to i8*), align 1, !dbg !2011, !tbaa !1555, !range !1557
  %tobool = icmp eq i8 %4, 0, !dbg !2011
  br i1 %tobool, label %if.end, label %if.then, !dbg !2013

if.then:                                          ; preds = %land.lhs.true
  %5 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp, i64 0, i32 0, !dbg !2015
  store i32 %line, i32* %5, align 8, !dbg !2015
  %6 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp, i64 0, i32 1, !dbg !2015
  store i32 %col, i32* %6, align 4, !dbg !2015
  %7 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp, i64 0, i32 2, !dbg !2015
  store i32 %2, i32* %7, align 8, !dbg !2015
  %8 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp, i64 0, i32 3, !dbg !2015
  store i32 %3, i32* %8, align 4, !dbg !2015
  %9 = bitcast %printf_args.1* %tmp to i8*, !dbg !2015
  %10 = call i32 @vprintf(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.23, i64 0, i64 0), i8* nonnull %9) #9, !dbg !2016
  br label %if.end, !dbg !2015

if.end:                                           ; preds = %land.lhs.true, %if.then, %entry
  ret void, !dbg !2017
}

; Function Attrs: convergent nounwind
define void @print5(i8* %p, i32 %bits, i32 %sline, i32 %scolm, i32 %op) local_unnamed_addr #3 !dbg !2018 {
entry:
  %tmp.i = alloca %printf_args.8, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !2022, metadata !1002), !dbg !2027
  tail call void @llvm.dbg.value(metadata i32 %bits, i64 0, metadata !2023, metadata !1002), !dbg !2028
  tail call void @llvm.dbg.value(metadata i32 %sline, i64 0, metadata !2024, metadata !1002), !dbg !2029
  tail call void @llvm.dbg.value(metadata i32 %scolm, i64 0, metadata !2025, metadata !1002), !dbg !2030
  tail call void @llvm.dbg.value(metadata i32 %op, i64 0, metadata !2026, metadata !1002), !dbg !2031
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #9, !dbg !2032, !range !1043
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #9, !dbg !2035, !range !1043
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #9, !dbg !2038, !range !1735
  %mul = mul nuw i32 %2, %1, !dbg !2041
  %add = add i32 %mul, %0, !dbg !2042
  %3 = load i32, i32* addrspacecast (i32 addrspace(1)* @CTALB to i32*), align 4, !dbg !2043, !tbaa !1237
  %cmp = icmp ult i32 %add, %3, !dbg !2044
  br i1 %cmp, label %return, label %lor.lhs.false, !dbg !2045

lor.lhs.false:                                    ; preds = %entry
  %4 = load i32, i32* addrspacecast (i32 addrspace(1)* @CTAUB to i32*), align 4, !dbg !2046, !tbaa !1237
  %cmp8 = icmp ugt i32 %add, %4, !dbg !2048
  br i1 %cmp8, label %return, label %if.end, !dbg !2049

if.end:                                           ; preds = %lor.lhs.false
  %5 = bitcast %printf_args.8* %tmp.i to i8*, !dbg !2051
  call void @llvm.lifetime.start(i64 8, i8* nonnull %5), !dbg !2051
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1827, metadata !1002) #9, !dbg !2051
  tail call void @llvm.dbg.value(metadata i16 %conv, i64 0, metadata !1828, metadata !1002) #9, !dbg !2053
  tail call void @llvm.dbg.value(metadata i16 %conv9, i64 0, metadata !1829, metadata !1002) #9, !dbg !2054
  tail call void @llvm.dbg.value(metadata i16 %conv10, i64 0, metadata !1830, metadata !1002) #9, !dbg !2055
  tail call void @llvm.dbg.value(metadata i16 %conv11, i64 0, metadata !1831, metadata !1002) #9, !dbg !2056
  %6 = load i64, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !2057, !tbaa !1696
  %cmp.i = icmp ult i64 %6, 67108736, !dbg !2057
  br i1 %cmp.i, label %cond.end.i, label %cond.false.i, !dbg !2057

cond.false.i:                                     ; preds = %if.end
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.17, i64 0, i64 0), i32 498, i8* getelementptr inbounds ([52 x i8], [52 x i8]* @__PRETTY_FUNCTION__._Z10storeLinesPvssss, i64 0, i64 0)) #10, !dbg !2058
  unreachable

cond.end.i:                                       ; preds = %if.end
  %conv11 = trunc i32 %op to i16, !dbg !2059
  %conv10 = trunc i32 %scolm to i16, !dbg !2060
  %conv9 = trunc i32 %sline to i16, !dbg !2061
  %div = sdiv i32 %bits, 8, !dbg !2062
  %conv = trunc i32 %div to i16, !dbg !2063
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1747, metadata !1002) #9, !dbg !2064
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1757, metadata !1002) #9, !dbg !2066
  %7 = atomicrmw add i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), i64 1 seq_cst, !dbg !2068
  %conv.i = trunc i64 %7 to i32, !dbg !2069
  tail call void @llvm.dbg.value(metadata i32 %conv.i, i64 0, metadata !1832, metadata !1002) #9, !dbg !2070
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !1833, metadata !1002) #9, !dbg !2071
  tail call void @llvm.dbg.value(metadata i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 0, metadata !1834, metadata !1002) #9, !dbg !2072
  %conv2.i = trunc i32 %0 to i16, !dbg !2073
  %mul.i = mul nsw i32 %conv.i, 12, !dbg !2074
  %idxprom.i = sext i32 %mul.i to i64, !dbg !2075
  %arrayidx.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom.i, !dbg !2075
  store i16 %conv2.i, i16* %arrayidx.i, align 2, !dbg !2076, !tbaa !1858
  %conv4.i = trunc i32 %1 to i16, !dbg !2077
  %add6.i = or i32 %mul.i, 1, !dbg !2078
  %idxprom7.i = sext i32 %add6.i to i64, !dbg !2079
  %arrayidx8.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom7.i, !dbg !2079
  store i16 %conv4.i, i16* %arrayidx8.i, align 2, !dbg !2080, !tbaa !1858
  %8 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #9, !dbg !2081, !range !1120
  %conv10.i = trunc i32 %8 to i16, !dbg !2083
  %add12.i = or i32 %mul.i, 2, !dbg !2084
  %idxprom13.i = sext i32 %add12.i to i64, !dbg !2085
  %arrayidx14.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom13.i, !dbg !2085
  store i16 %conv10.i, i16* %arrayidx14.i, align 2, !dbg !2086, !tbaa !1858
  %9 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #9, !dbg !2087, !range !1120
  %conv16.i = trunc i32 %9 to i16, !dbg !2089
  %add18.i = or i32 %mul.i, 3, !dbg !2090
  %idxprom19.i = sext i32 %add18.i to i64, !dbg !2091
  %arrayidx20.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom19.i, !dbg !2091
  store i16 %conv16.i, i16* %arrayidx20.i, align 2, !dbg !2092, !tbaa !1858
  %10 = ptrtoint i8* %p to i64, !dbg !2093
  %add22.i = mul i64 %7, 12884901888, !dbg !2094
  %sext.i = add i64 %add22.i, 4294967296, !dbg !2094
  %idxprom23.i = ashr exact i64 %sext.i, 32, !dbg !2094
  %arrayidx24.i = getelementptr inbounds i64, i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 %idxprom23.i, !dbg !2094
  store i64 %10, i64* %arrayidx24.i, align 8, !dbg !2095, !tbaa !1880
  %add26.i = add nsw i32 %mul.i, 8, !dbg !2096
  %idxprom27.i = sext i32 %add26.i to i64, !dbg !2097
  %arrayidx28.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom27.i, !dbg !2097
  store i16 %conv, i16* %arrayidx28.i, align 2, !dbg !2098, !tbaa !1858
  %add30.i = add nsw i32 %mul.i, 9, !dbg !2099
  %idxprom31.i = sext i32 %add30.i to i64, !dbg !2100
  %arrayidx32.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom31.i, !dbg !2100
  store i16 %conv9, i16* %arrayidx32.i, align 2, !dbg !2101, !tbaa !1858
  %add34.i = add nsw i32 %mul.i, 10, !dbg !2102
  %idxprom35.i = sext i32 %add34.i to i64, !dbg !2103
  %arrayidx36.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom35.i, !dbg !2103
  store i16 %conv10, i16* %arrayidx36.i, align 2, !dbg !2104, !tbaa !1858
  %add38.i = add nsw i32 %mul.i, 11, !dbg !2105
  %idxprom39.i = sext i32 %add38.i to i64, !dbg !2106
  %arrayidx40.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom39.i, !dbg !2106
  store i16 %conv11, i16* %arrayidx40.i, align 2, !dbg !2107, !tbaa !1858
  %cmp42.i = icmp slt i32 %conv.i, 5, !dbg !2108
  br i1 %cmp42.i, label %if.then.i, label %_Z10storeLinesPvssss.exit, !dbg !2109

if.then.i:                                        ; preds = %cond.end.i
  %11 = getelementptr inbounds %printf_args.8, %printf_args.8* %tmp.i, i64 0, i32 0, !dbg !2110
  store i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 0), i32** %11, align 8, !dbg !2110
  %12 = call i32 @vprintf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.18, i64 0, i64 0), i8* nonnull %5) #9, !dbg !2110
  br label %_Z10storeLinesPvssss.exit, !dbg !2110

_Z10storeLinesPvssss.exit:                        ; preds = %cond.end.i, %if.then.i
  call void @llvm.lifetime.end(i64 8, i8* nonnull %5), !dbg !2111
  br label %return, !dbg !2112

return:                                           ; preds = %entry, %lor.lhs.false, %_Z10storeLinesPvssss.exit
  ret void, !dbg !2113
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #8

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #8

; Function Attrs: nounwind
define void @RetKernel() local_unnamed_addr #0 !dbg !2114 {
entry:
  %tmp = alloca %printf_args.13, align 8
  %tmp19 = alloca %printf_args.13, align 8
  %tmp23 = alloca %printf_args.15, align 8
  %tmp24 = alloca %printf_args.15, align 8
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #9, !dbg !2124, !range !1120
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #9, !dbg !2126, !range !1120
  %add = add nuw nsw i32 %1, %0, !dbg !2129
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #9, !dbg !2130, !range !1043
  %add3 = add nuw nsw i32 %add, %2, !dbg !2133
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #9, !dbg !2134, !range !1043
  %add5 = sub nsw i32 0, %3, !dbg !2137
  %cmp = icmp eq i32 %add3, %add5, !dbg !2137
  br i1 %cmp, label %if.then, label %if.end, !dbg !2138

if.then:                                          ; preds = %entry
  tail call void @llvm.dbg.value(metadata %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 0, metadata !2116, metadata !1002), !dbg !2139
  %4 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #9, !dbg !2140, !range !1089
  %conv = trunc i32 %4 to i16, !dbg !2143
  store i16 %conv, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), align 8, !dbg !2144, !tbaa !1796
  %5 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.y() #9, !dbg !2145, !range !1089
  %conv8 = trunc i32 %5 to i16, !dbg !2148
  store i16 %conv8, i16* getelementptr (%struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 0, i32 1), align 2, !dbg !2149, !tbaa !1800
  %6 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #9, !dbg !2150, !range !1735
  %conv11 = trunc i32 %6 to i16, !dbg !2152
  store i16 %conv11, i16* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 1) to i16*), align 4, !dbg !2153, !tbaa !1786
  %7 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.y() #9, !dbg !2154, !range !1735
  %conv14 = trunc i32 %7 to i16, !dbg !2157
  store i16 %conv14, i16* getelementptr (%struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 0, i32 3), align 2, !dbg !2158, !tbaa !1792
  %8 = load i64, i64* addrspacecast (i64 addrspace(1)* @bbccnntt to i64*), align 8, !dbg !2159, !tbaa !1696
  store i64 %8, i64* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 2) to i64*), align 8, !dbg !2160, !tbaa !1779
  store i32 0, i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 4), align 8, !dbg !2161, !tbaa !1803
  store i32 0, i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 5), align 4, !dbg !2162, !tbaa !1806
  %9 = getelementptr inbounds %printf_args.13, %printf_args.13* %tmp, i64 0, i32 0, !dbg !2163
  store i64 %8, i64* %9, align 8, !dbg !2163
  %10 = bitcast %printf_args.13* %tmp to i8*, !dbg !2163
  %11 = call i32 @vprintf(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.24, i64 0, i64 0), i8* nonnull %10) #9, !dbg !2163
  %12 = load i64, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !2164, !tbaa !1696
  %13 = getelementptr inbounds %printf_args.13, %printf_args.13* %tmp19, i64 0, i32 0, !dbg !2165
  store i64 %12, i64* %13, align 8, !dbg !2165
  %14 = bitcast %printf_args.13* %tmp19 to i8*, !dbg !2165
  %15 = call i32 @vprintf(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.24, i64 0, i64 0), i8* nonnull %14) #9, !dbg !2165
  call void @llvm.dbg.value(metadata i64 1024, i64 0, metadata !2121, metadata !1002), !dbg !2166
  call void @llvm.dbg.value(metadata i64 3072, i64 0, metadata !2122, metadata !1002), !dbg !2167
  %16 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 0, !dbg !2168
  store i32 15, i32* %16, align 8, !dbg !2168
  %17 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 1, !dbg !2168
  store i32 31, i32* %17, align 4, !dbg !2168
  %18 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 2, !dbg !2168
  store i64 1, i64* %18, align 8, !dbg !2168
  %19 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 3, !dbg !2168
  store i64 465, i64* %19, align 8, !dbg !2168
  %20 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 4, !dbg !2168
  store i64 1024, i64* %20, align 8, !dbg !2168
  %21 = bitcast %printf_args.15* %tmp23 to i8*, !dbg !2168
  %22 = call i32 @vprintf(i8* getelementptr inbounds ([57 x i8], [57 x i8]* @.str.25, i64 0, i64 0), i8* nonnull %21) #9, !dbg !2168
  %23 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp24, i64 0, i32 0, !dbg !2169
  store i32 15, i32* %23, align 8, !dbg !2169
  %24 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp24, i64 0, i32 1, !dbg !2169
  store i32 15, i32* %24, align 4, !dbg !2169
  %25 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp24, i64 0, i32 2, !dbg !2169
  store i64 8, i64* %25, align 8, !dbg !2169
  %26 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp24, i64 0, i32 3, !dbg !2169
  store i64 1800, i64* %26, align 8, !dbg !2169
  %27 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp24, i64 0, i32 4, !dbg !2169
  store i64 3072, i64* %27, align 8, !dbg !2169
  %28 = bitcast %printf_args.15* %tmp24 to i8*, !dbg !2169
  %29 = call i32 @vprintf(i8* getelementptr inbounds ([56 x i8], [56 x i8]* @.str.26, i64 0, i64 0), i8* nonnull %28) #9, !dbg !2169
  call void @llvm.dbg.value(metadata i8* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i32 0, i64 402652928) to i8*), i64 0, metadata !2123, metadata !1002), !dbg !2170
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 402652928) to i8*), i8* addrspacecast (i8 addrspace(1)* getelementptr inbounds ([15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 0, i64 0) to i8*), i64 465, i32 1, i1 false) #9, !dbg !2171
  call void @llvm.dbg.value(metadata i8* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i32 0, i64 402652416) to i8*), i64 0, metadata !2123, metadata !1002), !dbg !2170
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 402652416) to i8*), i8* addrspacecast (i8 addrspace(1)* bitcast ([20 x [5 x %struct.CallSite_t]] addrspace(1)* @contextDic to i8 addrspace(1)*) to i8*), i64 1800, i32 1, i1 false) #9, !dbg !2180
  store i64 1, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !2182, !tbaa !1696
  store i64 1, i64* addrspacecast (i64 addrspace(1)* @bbccnntt to i64*), align 8, !dbg !2183, !tbaa !1696
  br label %if.end, !dbg !2184

if.end:                                           ; preds = %if.then, %entry
  ret void, !dbg !2185
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.y() #1

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture writeonly, i8* nocapture readonly, i64, i32, i1) #8

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { nounwind readonly "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { convergent nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { convergent inlinehint noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { convergent noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { norecurse nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { argmemonly nounwind }
attributes #9 = { nounwind }
attributes #10 = { convergent nounwind }
attributes #11 = { convergent noreturn nounwind }

!llvm.dbg.cu = !{!975, !2}
!nvvm.annotations = !{!978, !979, !980, !979, !981, !981, !981, !981, !982, !982, !981, !979, !980, !979, !981, !981, !981, !981, !982, !982, !981}
!llvm.ident = !{!983, !983}
!nvvm.internalize.after.link = !{}
!nvvmir.version = !{!984, !984}
!llvm.module.flags = !{!985, !986, !987}

!0 = !DIGlobalVariableExpression(var: !1)
!1 = distinct !DIGlobalVariable(name: "CTALB", scope: !2, file: !3, line: 15, type: !8, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C_plus_plus, file: !3, producer: "clang version 5.0.0 (trunk 294196)", isOptimized: true, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, retainedTypes: !5, globals: !32, imports: !79)
!3 = !DIFile(filename: "/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/ansf.cu", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!4 = !{}
!5 = !{!6, !8, !9, !24, !25, !26, !15, !27, !29, !30}
!6 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !7, size: 64)
!7 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!8 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!9 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !10, size: 64)
!10 = !DIDerivedType(tag: DW_TAG_typedef, name: "BBlog_t", file: !11, line: 37, baseType: !12)
!11 = !DIFile(filename: "/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/types.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!12 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "BBlog_t", file: !11, line: 27, size: 256, elements: !13, identifier: "_ZTS7BBlog_t")
!13 = !{!14, !16, !17, !18, !19, !21, !22, !23}
!14 = !DIDerivedType(tag: DW_TAG_member, name: "bidx", scope: !12, file: !11, line: 28, baseType: !15, size: 16)
!15 = !DIBasicType(name: "short", size: 16, encoding: DW_ATE_signed)
!16 = !DIDerivedType(tag: DW_TAG_member, name: "bidy", scope: !12, file: !11, line: 29, baseType: !15, size: 16, offset: 16)
!17 = !DIDerivedType(tag: DW_TAG_member, name: "tidx", scope: !12, file: !11, line: 30, baseType: !15, size: 16, offset: 32)
!18 = !DIDerivedType(tag: DW_TAG_member, name: "tidy", scope: !12, file: !11, line: 31, baseType: !15, size: 16, offset: 48)
!19 = !DIDerivedType(tag: DW_TAG_member, name: "key", scope: !12, file: !11, line: 32, baseType: !20, size: 64, offset: 64)
!20 = !DIBasicType(name: "long long unsigned int", size: 64, encoding: DW_ATE_unsigned)
!21 = !DIDerivedType(tag: DW_TAG_member, name: "sline", scope: !12, file: !11, line: 33, baseType: !8, size: 32, offset: 128)
!22 = !DIDerivedType(tag: DW_TAG_member, name: "scolm", scope: !12, file: !11, line: 34, baseType: !8, size: 32, offset: 160)
!23 = !DIDerivedType(tag: DW_TAG_member, name: "cid", scope: !12, file: !11, line: 35, baseType: !8, size: 32, offset: 192)
!24 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !15, size: 64)
!25 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !26, size: 64)
!26 = !DIBasicType(name: "long int", size: 64, encoding: DW_ATE_signed)
!27 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !28, size: 64)
!28 = !DIDerivedType(tag: DW_TAG_volatile_type, baseType: !29)
!29 = !DIBasicType(name: "long long int", size: 64, encoding: DW_ATE_signed)
!30 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !31, size: 64)
!31 = !DIBasicType(name: "unsigned char", size: 8, encoding: DW_ATE_unsigned_char)
!32 = !{!0, !33, !35, !37, !40, !42, !44, !46, !48, !54, !56, !68, !71, !77}
!33 = !DIGlobalVariableExpression(var: !34)
!34 = distinct !DIGlobalVariable(name: "CTAUB", scope: !2, file: !3, line: 16, type: !8, isLocal: false, isDefinition: true)
!35 = !DIGlobalVariableExpression(var: !36)
!36 = distinct !DIGlobalVariable(name: "CONSTANCE", scope: !2, file: !3, line: 17, type: !8, isLocal: false, isDefinition: true)
!37 = !DIGlobalVariableExpression(var: !38)
!38 = distinct !DIGlobalVariable(name: "VERBOSE", scope: !2, file: !3, line: 22, type: !39, isLocal: false, isDefinition: true)
!39 = !DIBasicType(name: "bool", size: 8, encoding: DW_ATE_boolean)
!40 = !DIGlobalVariableExpression(var: !41)
!41 = distinct !DIGlobalVariable(name: "ccnntt", scope: !2, file: !3, line: 35, type: !20, isLocal: false, isDefinition: true)
!42 = !DIGlobalVariableExpression(var: !43)
!43 = distinct !DIGlobalVariable(name: "bbccnntt", scope: !2, file: !3, line: 36, type: !20, isLocal: false, isDefinition: true)
!44 = !DIGlobalVariableExpression(var: !45)
!45 = distinct !DIGlobalVariable(name: "done1", scope: !2, file: !3, line: 42, type: !8, isLocal: false, isDefinition: true)
!46 = !DIGlobalVariableExpression(var: !47)
!47 = distinct !DIGlobalVariable(name: "done2", scope: !2, file: !3, line: 43, type: !8, isLocal: false, isDefinition: true)
!48 = !DIGlobalVariableExpression(var: !49)
!49 = distinct !DIGlobalVariable(name: "funcDic", scope: !2, file: !3, line: 46, type: !50, isLocal: false, isDefinition: true)
!50 = !DICompositeType(tag: DW_TAG_array_type, baseType: !7, size: 3720, elements: !51)
!51 = !{!52, !53}
!52 = !DISubrange(count: 15)
!53 = !DISubrange(count: 31)
!54 = !DIGlobalVariableExpression(var: !55)
!55 = distinct !DIGlobalVariable(name: "dicHeight", scope: !2, file: !3, line: 47, type: !8, isLocal: false, isDefinition: true)
!56 = !DIGlobalVariableExpression(var: !57)
!57 = distinct !DIGlobalVariable(name: "globalCallStack", scope: !2, file: !3, line: 49, type: !58, isLocal: false, isDefinition: true)
!58 = !DICompositeType(tag: DW_TAG_array_type, baseType: !59, size: 640, elements: !66)
!59 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !60, size: 64)
!60 = !DIDerivedType(tag: DW_TAG_typedef, name: "CallSite_t", file: !11, line: 12, baseType: !61)
!61 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "CallSite_t", file: !11, line: 8, size: 64, elements: !62, identifier: "_ZTS10CallSite_t")
!62 = !{!63, !64, !65}
!63 = !DIDerivedType(tag: DW_TAG_member, name: "id", scope: !61, file: !11, line: 9, baseType: !8, size: 32)
!64 = !DIDerivedType(tag: DW_TAG_member, name: "sline", scope: !61, file: !11, line: 10, baseType: !15, size: 16, offset: 32)
!65 = !DIDerivedType(tag: DW_TAG_member, name: "scolm", scope: !61, file: !11, line: 11, baseType: !15, size: 16, offset: 48)
!66 = !{!67}
!67 = !DISubrange(count: 10)
!68 = !DIGlobalVariableExpression(var: !69)
!69 = distinct !DIGlobalVariable(name: "stackHeight", scope: !2, file: !3, line: 50, type: !70, isLocal: false, isDefinition: true)
!70 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !8, size: 64)
!71 = !DIGlobalVariableExpression(var: !72)
!72 = distinct !DIGlobalVariable(name: "contextDic", scope: !2, file: !3, line: 56, type: !73, isLocal: false, isDefinition: true)
!73 = !DICompositeType(tag: DW_TAG_array_type, baseType: !60, size: 6400, elements: !74)
!74 = !{!75, !76}
!75 = !DISubrange(count: 20)
!76 = !DISubrange(count: 5)
!77 = !DIGlobalVariableExpression(var: !78)
!78 = distinct !DIGlobalVariable(name: "cHeight", scope: !2, file: !3, line: 57, type: !8, isLocal: false, isDefinition: true)
!79 = !{!80, !86, !91, !93, !95, !97, !99, !103, !105, !107, !109, !111, !113, !115, !117, !119, !121, !123, !125, !127, !129, !131, !135, !137, !139, !141, !145, !149, !151, !153, !157, !161, !163, !165, !167, !169, !171, !173, !175, !177, !181, !185, !187, !189, !193, !195, !197, !199, !201, !203, !207, !209, !211, !216, !223, !227, !229, !231, !235, !237, !239, !243, !245, !247, !251, !253, !255, !257, !259, !261, !263, !265, !267, !269, !274, !276, !278, !282, !284, !286, !288, !290, !292, !294, !296, !300, !304, !306, !308, !313, !315, !317, !319, !321, !323, !325, !329, !335, !339, !343, !348, !351, !355, !359, !374, !378, !382, !386, !390, !394, !396, !400, !404, !408, !416, !420, !424, !428, !432, !437, !443, !447, !451, !453, !461, !465, !473, !475, !477, !481, !485, !489, !493, !497, !502, !503, !504, !505, !508, !509, !510, !511, !512, !513, !514, !517, !519, !521, !523, !525, !527, !529, !531, !534, !536, !538, !540, !542, !544, !546, !548, !550, !552, !554, !556, !558, !560, !562, !564, !566, !568, !570, !572, !574, !576, !578, !580, !582, !584, !586, !588, !590, !592, !594, !596, !598, !602, !603, !605, !607, !609, !611, !613, !615, !617, !619, !621, !623, !625, !627, !629, !631, !636, !638, !642, !650, !655, !659, !663, !667, !671, !673, !675, !679, !685, !689, !695, !701, !703, !707, !711, !715, !719, !726, !728, !732, !736, !740, !742, !746, !750, !754, !756, !758, !762, !770, !774, !778, !782, !784, !790, !792, !798, !802, !806, !810, !814, !818, !822, !824, !826, !830, !834, !838, !840, !844, !848, !850, !852, !856, !860, !864, !868, !869, !870, !871, !875, !878, !882, !887, !890, !892, !894, !896, !898, !900, !902, !904, !906, !908, !910, !912, !914, !917, !919, !926, !928, !929, !931, !933, !935, !937, !941, !943, !945, !947, !949, !951, !953, !955, !957, !961, !965, !967, !971}
!80 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !83, line: 201)
!81 = !DINamespace(name: "std", scope: null, file: !82, line: 195)
!82 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_math_forward_declares.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!83 = !DISubprogram(name: "abs", linkageName: "_ZL3absx", scope: !82, file: !82, line: 44, type: !84, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!84 = !DISubroutineType(types: !85)
!85 = !{!29, !29}
!86 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !87, line: 202)
!87 = !DISubprogram(name: "acos", linkageName: "_ZL4acosf", scope: !82, file: !82, line: 46, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!88 = !DISubroutineType(types: !89)
!89 = !{!90, !90}
!90 = !DIBasicType(name: "float", size: 32, encoding: DW_ATE_float)
!91 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !92, line: 203)
!92 = !DISubprogram(name: "acosh", linkageName: "_ZL5acoshf", scope: !82, file: !82, line: 48, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!93 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !94, line: 204)
!94 = !DISubprogram(name: "asin", linkageName: "_ZL4asinf", scope: !82, file: !82, line: 50, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!95 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !96, line: 205)
!96 = !DISubprogram(name: "asinh", linkageName: "_ZL5asinhf", scope: !82, file: !82, line: 52, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!97 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !98, line: 206)
!98 = !DISubprogram(name: "atan", linkageName: "_ZL4atanf", scope: !82, file: !82, line: 56, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!99 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !100, line: 207)
!100 = !DISubprogram(name: "atan2", linkageName: "_ZL5atan2ff", scope: !82, file: !82, line: 54, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!101 = !DISubroutineType(types: !102)
!102 = !{!90, !90, !90}
!103 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !104, line: 208)
!104 = !DISubprogram(name: "atanh", linkageName: "_ZL5atanhf", scope: !82, file: !82, line: 58, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!105 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !106, line: 209)
!106 = !DISubprogram(name: "cbrt", linkageName: "_ZL4cbrtf", scope: !82, file: !82, line: 60, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!107 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !108, line: 210)
!108 = !DISubprogram(name: "ceil", linkageName: "_ZL4ceilf", scope: !82, file: !82, line: 62, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!109 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !110, line: 211)
!110 = !DISubprogram(name: "copysign", linkageName: "_ZL8copysignff", scope: !82, file: !82, line: 64, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!111 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !112, line: 212)
!112 = !DISubprogram(name: "cos", linkageName: "_ZL3cosf", scope: !82, file: !82, line: 66, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!113 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !114, line: 213)
!114 = !DISubprogram(name: "cosh", linkageName: "_ZL4coshf", scope: !82, file: !82, line: 68, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!115 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !116, line: 214)
!116 = !DISubprogram(name: "erf", linkageName: "_ZL3erff", scope: !82, file: !82, line: 72, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!117 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !118, line: 215)
!118 = !DISubprogram(name: "erfc", linkageName: "_ZL4erfcf", scope: !82, file: !82, line: 70, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!119 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !120, line: 216)
!120 = !DISubprogram(name: "exp", linkageName: "_ZL3expf", scope: !82, file: !82, line: 76, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!121 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !122, line: 217)
!122 = !DISubprogram(name: "exp2", linkageName: "_ZL4exp2f", scope: !82, file: !82, line: 74, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!123 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !124, line: 218)
!124 = !DISubprogram(name: "expm1", linkageName: "_ZL5expm1f", scope: !82, file: !82, line: 78, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!125 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !126, line: 219)
!126 = !DISubprogram(name: "fabs", linkageName: "_ZL4fabsf", scope: !82, file: !82, line: 80, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!127 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !128, line: 220)
!128 = !DISubprogram(name: "fdim", linkageName: "_ZL4fdimff", scope: !82, file: !82, line: 82, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!129 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !130, line: 221)
!130 = !DISubprogram(name: "floor", linkageName: "_ZL5floorf", scope: !82, file: !82, line: 84, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!131 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !132, line: 222)
!132 = !DISubprogram(name: "fma", linkageName: "_ZL3fmafff", scope: !82, file: !82, line: 86, type: !133, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!133 = !DISubroutineType(types: !134)
!134 = !{!90, !90, !90, !90}
!135 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !136, line: 223)
!136 = !DISubprogram(name: "fmax", linkageName: "_ZL4fmaxff", scope: !82, file: !82, line: 88, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!137 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !138, line: 224)
!138 = !DISubprogram(name: "fmin", linkageName: "_ZL4fminff", scope: !82, file: !82, line: 90, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!139 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !140, line: 225)
!140 = !DISubprogram(name: "fmod", linkageName: "_ZL4fmodff", scope: !82, file: !82, line: 92, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!141 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !142, line: 226)
!142 = !DISubprogram(name: "fpclassify", linkageName: "_ZL10fpclassifyf", scope: !82, file: !82, line: 94, type: !143, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!143 = !DISubroutineType(types: !144)
!144 = !{!8, !90}
!145 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !146, line: 227)
!146 = !DISubprogram(name: "frexp", linkageName: "_ZL5frexpfPi", scope: !82, file: !82, line: 96, type: !147, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!147 = !DISubroutineType(types: !148)
!148 = !{!90, !90, !70}
!149 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !150, line: 228)
!150 = !DISubprogram(name: "hypot", linkageName: "_ZL5hypotff", scope: !82, file: !82, line: 98, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!151 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !152, line: 229)
!152 = !DISubprogram(name: "ilogb", linkageName: "_ZL5ilogbf", scope: !82, file: !82, line: 100, type: !143, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!153 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !154, line: 230)
!154 = !DISubprogram(name: "isfinite", linkageName: "_ZL8isfinitef", scope: !82, file: !82, line: 102, type: !155, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!155 = !DISubroutineType(types: !156)
!156 = !{!39, !90}
!157 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !158, line: 231)
!158 = !DISubprogram(name: "isgreater", linkageName: "_ZL9isgreaterff", scope: !82, file: !82, line: 106, type: !159, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!159 = !DISubroutineType(types: !160)
!160 = !{!39, !90, !90}
!161 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !162, line: 232)
!162 = !DISubprogram(name: "isgreaterequal", linkageName: "_ZL14isgreaterequalff", scope: !82, file: !82, line: 105, type: !159, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!163 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !164, line: 233)
!164 = !DISubprogram(name: "isinf", linkageName: "_ZL5isinff", scope: !82, file: !82, line: 108, type: !155, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!165 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !166, line: 234)
!166 = !DISubprogram(name: "isless", linkageName: "_ZL6islessff", scope: !82, file: !82, line: 112, type: !159, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!167 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !168, line: 235)
!168 = !DISubprogram(name: "islessequal", linkageName: "_ZL11islessequalff", scope: !82, file: !82, line: 111, type: !159, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!169 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !170, line: 236)
!170 = !DISubprogram(name: "islessgreater", linkageName: "_ZL13islessgreaterff", scope: !82, file: !82, line: 114, type: !159, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!171 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !172, line: 237)
!172 = !DISubprogram(name: "isnan", linkageName: "_ZL5isnanf", scope: !82, file: !82, line: 116, type: !155, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!173 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !174, line: 238)
!174 = !DISubprogram(name: "isnormal", linkageName: "_ZL8isnormalf", scope: !82, file: !82, line: 118, type: !155, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!175 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !176, line: 239)
!176 = !DISubprogram(name: "isunordered", linkageName: "_ZL11isunorderedff", scope: !82, file: !82, line: 120, type: !159, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!177 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !178, line: 240)
!178 = !DISubprogram(name: "labs", linkageName: "_ZL4labsl", scope: !82, file: !82, line: 121, type: !179, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!179 = !DISubroutineType(types: !180)
!180 = !{!26, !26}
!181 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !182, line: 241)
!182 = !DISubprogram(name: "ldexp", linkageName: "_ZL5ldexpfi", scope: !82, file: !82, line: 123, type: !183, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!183 = !DISubroutineType(types: !184)
!184 = !{!90, !90, !8}
!185 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !186, line: 242)
!186 = !DISubprogram(name: "lgamma", linkageName: "_ZL6lgammaf", scope: !82, file: !82, line: 125, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!187 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !188, line: 243)
!188 = !DISubprogram(name: "llabs", linkageName: "_ZL5llabsx", scope: !82, file: !82, line: 126, type: !84, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!189 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !190, line: 244)
!190 = !DISubprogram(name: "llrint", linkageName: "_ZL6llrintf", scope: !82, file: !82, line: 128, type: !191, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!191 = !DISubroutineType(types: !192)
!192 = !{!29, !90}
!193 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !194, line: 245)
!194 = !DISubprogram(name: "log", linkageName: "_ZL3logf", scope: !82, file: !82, line: 138, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!195 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !196, line: 246)
!196 = !DISubprogram(name: "log10", linkageName: "_ZL5log10f", scope: !82, file: !82, line: 130, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!197 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !198, line: 247)
!198 = !DISubprogram(name: "log1p", linkageName: "_ZL5log1pf", scope: !82, file: !82, line: 132, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!199 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !200, line: 248)
!200 = !DISubprogram(name: "log2", linkageName: "_ZL4log2f", scope: !82, file: !82, line: 134, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!201 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !202, line: 249)
!202 = !DISubprogram(name: "logb", linkageName: "_ZL4logbf", scope: !82, file: !82, line: 136, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!203 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !204, line: 250)
!204 = !DISubprogram(name: "lrint", linkageName: "_ZL5lrintf", scope: !82, file: !82, line: 140, type: !205, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!205 = !DISubroutineType(types: !206)
!206 = !{!26, !90}
!207 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !208, line: 251)
!208 = !DISubprogram(name: "lround", linkageName: "_ZL6lroundf", scope: !82, file: !82, line: 142, type: !205, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!209 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !210, line: 252)
!210 = !DISubprogram(name: "llround", linkageName: "_ZL7llroundf", scope: !82, file: !82, line: 143, type: !191, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!211 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !212, line: 253)
!212 = !DISubprogram(name: "modf", linkageName: "_ZL4modffPf", scope: !82, file: !82, line: 145, type: !213, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!213 = !DISubroutineType(types: !214)
!214 = !{!90, !90, !215}
!215 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !90, size: 64)
!216 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !217, line: 254)
!217 = !DISubprogram(name: "nan", linkageName: "_ZL3nanPKc", scope: !82, file: !82, line: 146, type: !218, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!218 = !DISubroutineType(types: !219)
!219 = !{!220, !221}
!220 = !DIBasicType(name: "double", size: 64, encoding: DW_ATE_float)
!221 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !222, size: 64)
!222 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !7)
!223 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !224, line: 255)
!224 = !DISubprogram(name: "nanf", linkageName: "_ZL4nanfPKc", scope: !82, file: !82, line: 147, type: !225, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!225 = !DISubroutineType(types: !226)
!226 = !{!90, !221}
!227 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !228, line: 256)
!228 = !DISubprogram(name: "nearbyint", linkageName: "_ZL9nearbyintf", scope: !82, file: !82, line: 149, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!229 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !230, line: 257)
!230 = !DISubprogram(name: "nextafter", linkageName: "_ZL9nextafterff", scope: !82, file: !82, line: 151, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!231 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !232, line: 258)
!232 = !DISubprogram(name: "nexttoward", linkageName: "_ZL10nexttowardfd", scope: !82, file: !82, line: 153, type: !233, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!233 = !DISubroutineType(types: !234)
!234 = !{!90, !90, !220}
!235 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !236, line: 259)
!236 = !DISubprogram(name: "pow", linkageName: "_ZL3powfi", scope: !82, file: !82, line: 158, type: !183, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!237 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !238, line: 260)
!238 = !DISubprogram(name: "remainder", linkageName: "_ZL9remainderff", scope: !82, file: !82, line: 160, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!239 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !240, line: 261)
!240 = !DISubprogram(name: "remquo", linkageName: "_ZL6remquoffPi", scope: !82, file: !82, line: 162, type: !241, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!241 = !DISubroutineType(types: !242)
!242 = !{!90, !90, !90, !70}
!243 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !244, line: 262)
!244 = !DISubprogram(name: "rint", linkageName: "_ZL4rintf", scope: !82, file: !82, line: 164, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!245 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !246, line: 263)
!246 = !DISubprogram(name: "round", linkageName: "_ZL5roundf", scope: !82, file: !82, line: 166, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!247 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !248, line: 264)
!248 = !DISubprogram(name: "scalbln", linkageName: "_ZL7scalblnfl", scope: !82, file: !82, line: 168, type: !249, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!249 = !DISubroutineType(types: !250)
!250 = !{!90, !90, !26}
!251 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !252, line: 265)
!252 = !DISubprogram(name: "scalbn", linkageName: "_ZL6scalbnfi", scope: !82, file: !82, line: 170, type: !183, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!253 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !254, line: 266)
!254 = !DISubprogram(name: "signbit", linkageName: "_ZL7signbitf", scope: !82, file: !82, line: 172, type: !155, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!255 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !256, line: 267)
!256 = !DISubprogram(name: "sin", linkageName: "_ZL3sinf", scope: !82, file: !82, line: 174, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!257 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !258, line: 268)
!258 = !DISubprogram(name: "sinh", linkageName: "_ZL4sinhf", scope: !82, file: !82, line: 176, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!259 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !260, line: 269)
!260 = !DISubprogram(name: "sqrt", linkageName: "_ZL4sqrtf", scope: !82, file: !82, line: 178, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!261 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !262, line: 270)
!262 = !DISubprogram(name: "tan", linkageName: "_ZL3tanf", scope: !82, file: !82, line: 180, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!263 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !264, line: 271)
!264 = !DISubprogram(name: "tanh", linkageName: "_ZL4tanhf", scope: !82, file: !82, line: 182, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!265 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !266, line: 272)
!266 = !DISubprogram(name: "tgamma", linkageName: "_ZL6tgammaf", scope: !82, file: !82, line: 184, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!267 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !268, line: 273)
!268 = !DISubprogram(name: "trunc", linkageName: "_ZL5truncf", scope: !82, file: !82, line: 186, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!269 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !270, line: 102)
!270 = !DISubprogram(name: "acos", scope: !271, file: !271, line: 54, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!271 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/mathcalls.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!272 = !DISubroutineType(types: !273)
!273 = !{!220, !220}
!274 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !275, line: 121)
!275 = !DISubprogram(name: "asin", scope: !271, file: !271, line: 56, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!276 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !277, line: 140)
!277 = !DISubprogram(name: "atan", scope: !271, file: !271, line: 58, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!278 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !279, line: 159)
!279 = !DISubprogram(name: "atan2", scope: !271, file: !271, line: 60, type: !280, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!280 = !DISubroutineType(types: !281)
!281 = !{!220, !220, !220}
!282 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !283, line: 180)
!283 = !DISubprogram(name: "ceil", scope: !271, file: !271, line: 178, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!284 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !285, line: 199)
!285 = !DISubprogram(name: "cos", scope: !271, file: !271, line: 63, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!286 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !287, line: 218)
!287 = !DISubprogram(name: "cosh", scope: !271, file: !271, line: 72, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!288 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !289, line: 237)
!289 = !DISubprogram(name: "exp", scope: !271, file: !271, line: 100, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!290 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !291, line: 256)
!291 = !DISubprogram(name: "fabs", scope: !271, file: !271, line: 181, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!292 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !293, line: 275)
!293 = !DISubprogram(name: "floor", scope: !271, file: !271, line: 184, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!294 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !295, line: 294)
!295 = !DISubprogram(name: "fmod", scope: !271, file: !271, line: 187, type: !280, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!296 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !297, line: 315)
!297 = !DISubprogram(name: "frexp", scope: !271, file: !271, line: 103, type: !298, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!298 = !DISubroutineType(types: !299)
!299 = !{!220, !220, !70}
!300 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !301, line: 334)
!301 = !DISubprogram(name: "ldexp", scope: !271, file: !271, line: 106, type: !302, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!302 = !DISubroutineType(types: !303)
!303 = !{!220, !220, !8}
!304 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !305, line: 353)
!305 = !DISubprogram(name: "log", scope: !271, file: !271, line: 109, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!306 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !307, line: 372)
!307 = !DISubprogram(name: "log10", scope: !271, file: !271, line: 112, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!308 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !309, line: 391)
!309 = !DISubprogram(name: "modf", scope: !271, file: !271, line: 115, type: !310, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!310 = !DISubroutineType(types: !311)
!311 = !{!220, !220, !312}
!312 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !220, size: 64)
!313 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !314, line: 403)
!314 = !DISubprogram(name: "pow", scope: !271, file: !271, line: 153, type: !280, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!315 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !316, line: 440)
!316 = !DISubprogram(name: "sin", scope: !271, file: !271, line: 65, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!317 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !318, line: 459)
!318 = !DISubprogram(name: "sinh", scope: !271, file: !271, line: 74, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!319 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !320, line: 478)
!320 = !DISubprogram(name: "sqrt", scope: !271, file: !271, line: 156, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!321 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !322, line: 497)
!322 = !DISubprogram(name: "tan", scope: !271, file: !271, line: 67, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!323 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !324, line: 516)
!324 = !DISubprogram(name: "tanh", scope: !271, file: !271, line: 76, type: !272, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!325 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !326, line: 118)
!326 = !DIDerivedType(tag: DW_TAG_typedef, name: "div_t", file: !327, line: 101, baseType: !328)
!327 = !DIFile(filename: "/usr/include/stdlib.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!328 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !327, line: 97, flags: DIFlagFwdDecl, identifier: "_ZTS5div_t")
!329 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !330, line: 119)
!330 = !DIDerivedType(tag: DW_TAG_typedef, name: "ldiv_t", file: !327, line: 109, baseType: !331)
!331 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !327, line: 105, size: 128, elements: !332, identifier: "_ZTS6ldiv_t")
!332 = !{!333, !334}
!333 = !DIDerivedType(tag: DW_TAG_member, name: "quot", scope: !331, file: !327, line: 107, baseType: !26, size: 64)
!334 = !DIDerivedType(tag: DW_TAG_member, name: "rem", scope: !331, file: !327, line: 108, baseType: !26, size: 64, offset: 64)
!335 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !336, line: 121)
!336 = !DISubprogram(name: "abort", scope: !327, file: !327, line: 515, type: !337, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!337 = !DISubroutineType(types: !338)
!338 = !{null}
!339 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !340, line: 122)
!340 = !DISubprogram(name: "abs", scope: !327, file: !327, line: 775, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!341 = !DISubroutineType(types: !342)
!342 = !{!8, !8}
!343 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !344, line: 123)
!344 = !DISubprogram(name: "atexit", scope: !327, file: !327, line: 519, type: !345, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!345 = !DISubroutineType(types: !346)
!346 = !{!8, !347}
!347 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !337, size: 64)
!348 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !349, line: 129)
!349 = !DISubprogram(name: "atof", scope: !350, file: !350, line: 26, type: !218, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!350 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/stdlib-float.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!351 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !352, line: 130)
!352 = !DISubprogram(name: "atoi", scope: !327, file: !327, line: 278, type: !353, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!353 = !DISubroutineType(types: !354)
!354 = !{!8, !221}
!355 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !356, line: 131)
!356 = !DISubprogram(name: "atol", scope: !327, file: !327, line: 283, type: !357, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!357 = !DISubroutineType(types: !358)
!358 = !{!26, !221}
!359 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !360, line: 132)
!360 = !DISubprogram(name: "bsearch", scope: !361, file: !361, line: 20, type: !362, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!361 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/stdlib-bsearch.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!362 = !DISubroutineType(types: !363)
!363 = !{!364, !365, !365, !367, !367, !370}
!364 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 64)
!365 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !366, size: 64)
!366 = !DIDerivedType(tag: DW_TAG_const_type, baseType: null)
!367 = !DIDerivedType(tag: DW_TAG_typedef, name: "size_t", file: !368, line: 62, baseType: !369)
!368 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/stddef.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!369 = !DIBasicType(name: "long unsigned int", size: 64, encoding: DW_ATE_unsigned)
!370 = !DIDerivedType(tag: DW_TAG_typedef, name: "__compar_fn_t", file: !327, line: 742, baseType: !371)
!371 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !372, size: 64)
!372 = !DISubroutineType(types: !373)
!373 = !{!8, !365, !365}
!374 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !375, line: 133)
!375 = !DISubprogram(name: "calloc", scope: !327, file: !327, line: 468, type: !376, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!376 = !DISubroutineType(types: !377)
!377 = !{!364, !367, !367}
!378 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !379, line: 134)
!379 = !DISubprogram(name: "div", scope: !327, file: !327, line: 789, type: !380, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!380 = !DISubroutineType(types: !381)
!381 = !{!326, !8, !8}
!382 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !383, line: 135)
!383 = !DISubprogram(name: "exit", scope: !327, file: !327, line: 543, type: !384, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!384 = !DISubroutineType(types: !385)
!385 = !{null, !8}
!386 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !387, line: 136)
!387 = !DISubprogram(name: "free", scope: !327, file: !327, line: 483, type: !388, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!388 = !DISubroutineType(types: !389)
!389 = !{null, !364}
!390 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !391, line: 137)
!391 = !DISubprogram(name: "getenv", scope: !327, file: !327, line: 564, type: !392, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!392 = !DISubroutineType(types: !393)
!393 = !{!6, !221}
!394 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !395, line: 138)
!395 = !DISubprogram(name: "labs", scope: !327, file: !327, line: 776, type: !179, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!396 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !397, line: 139)
!397 = !DISubprogram(name: "ldiv", scope: !327, file: !327, line: 791, type: !398, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!398 = !DISubroutineType(types: !399)
!399 = !{!330, !26, !26}
!400 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !401, line: 140)
!401 = !DISubprogram(name: "malloc", scope: !327, file: !327, line: 466, type: !402, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!402 = !DISubroutineType(types: !403)
!403 = !{!364, !367}
!404 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !405, line: 142)
!405 = !DISubprogram(name: "mblen", scope: !327, file: !327, line: 863, type: !406, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!406 = !DISubroutineType(types: !407)
!407 = !{!8, !221, !367}
!408 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !409, line: 143)
!409 = !DISubprogram(name: "mbstowcs", scope: !327, file: !327, line: 874, type: !410, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!410 = !DISubroutineType(types: !411)
!411 = !{!367, !412, !415, !367}
!412 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !413)
!413 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !414, size: 64)
!414 = !DIBasicType(name: "wchar_t", size: 32, encoding: DW_ATE_signed)
!415 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !221)
!416 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !417, line: 144)
!417 = !DISubprogram(name: "mbtowc", scope: !327, file: !327, line: 866, type: !418, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!418 = !DISubroutineType(types: !419)
!419 = !{!8, !412, !415, !367}
!420 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !421, line: 146)
!421 = !DISubprogram(name: "qsort", scope: !327, file: !327, line: 765, type: !422, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!422 = !DISubroutineType(types: !423)
!423 = !{null, !364, !367, !367, !370}
!424 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !425, line: 152)
!425 = !DISubprogram(name: "rand", scope: !327, file: !327, line: 374, type: !426, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!426 = !DISubroutineType(types: !427)
!427 = !{!8}
!428 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !429, line: 153)
!429 = !DISubprogram(name: "realloc", scope: !327, file: !327, line: 480, type: !430, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!430 = !DISubroutineType(types: !431)
!431 = !{!364, !364, !367}
!432 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !433, line: 154)
!433 = !DISubprogram(name: "srand", scope: !327, file: !327, line: 376, type: !434, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!434 = !DISubroutineType(types: !435)
!435 = !{null, !436}
!436 = !DIBasicType(name: "unsigned int", size: 32, encoding: DW_ATE_unsigned)
!437 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !438, line: 155)
!438 = !DISubprogram(name: "strtod", scope: !327, file: !327, line: 164, type: !439, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!439 = !DISubroutineType(types: !440)
!440 = !{!220, !415, !441}
!441 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !442)
!442 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !6, size: 64)
!443 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !444, line: 156)
!444 = !DISubprogram(name: "strtol", scope: !327, file: !327, line: 183, type: !445, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!445 = !DISubroutineType(types: !446)
!446 = !{!26, !415, !441, !8}
!447 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !448, line: 157)
!448 = !DISubprogram(name: "strtoul", scope: !327, file: !327, line: 187, type: !449, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!449 = !DISubroutineType(types: !450)
!450 = !{!369, !415, !441, !8}
!451 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !452, line: 158)
!452 = !DISubprogram(name: "system", scope: !327, file: !327, line: 717, type: !353, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!453 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !454, line: 160)
!454 = !DISubprogram(name: "wcstombs", scope: !327, file: !327, line: 877, type: !455, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!455 = !DISubroutineType(types: !456)
!456 = !{!367, !457, !458, !367}
!457 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !6)
!458 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !459)
!459 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !460, size: 64)
!460 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !414)
!461 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !462, line: 161)
!462 = !DISubprogram(name: "wctomb", scope: !327, file: !327, line: 870, type: !463, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!463 = !DISubroutineType(types: !464)
!464 = !{!8, !6, !414}
!465 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !468, line: 201)
!466 = !DINamespace(name: "__gnu_cxx", scope: null, file: !467, line: 68)
!467 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/bits/cpp_type_traits.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!468 = !DIDerivedType(tag: DW_TAG_typedef, name: "lldiv_t", file: !327, line: 121, baseType: !469)
!469 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !327, line: 117, size: 128, elements: !470, identifier: "_ZTS7lldiv_t")
!470 = !{!471, !472}
!471 = !DIDerivedType(tag: DW_TAG_member, name: "quot", scope: !469, file: !327, line: 119, baseType: !29, size: 64)
!472 = !DIDerivedType(tag: DW_TAG_member, name: "rem", scope: !469, file: !327, line: 120, baseType: !29, size: 64, offset: 64)
!473 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !474, line: 207)
!474 = !DISubprogram(name: "_Exit", scope: !327, file: !327, line: 557, type: !384, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!475 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !476, line: 211)
!476 = !DISubprogram(name: "llabs", scope: !327, file: !327, line: 780, type: !84, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!477 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !478, line: 217)
!478 = !DISubprogram(name: "lldiv", scope: !327, file: !327, line: 797, type: !479, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!479 = !DISubroutineType(types: !480)
!480 = !{!468, !29, !29}
!481 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !482, line: 228)
!482 = !DISubprogram(name: "atoll", scope: !327, file: !327, line: 292, type: !483, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!483 = !DISubroutineType(types: !484)
!484 = !{!29, !221}
!485 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !486, line: 229)
!486 = !DISubprogram(name: "strtoll", scope: !327, file: !327, line: 209, type: !487, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!487 = !DISubroutineType(types: !488)
!488 = !{!29, !415, !441, !8}
!489 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !490, line: 230)
!490 = !DISubprogram(name: "strtoull", scope: !327, file: !327, line: 214, type: !491, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!491 = !DISubroutineType(types: !492)
!492 = !{!20, !415, !441, !8}
!493 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !494, line: 232)
!494 = !DISubprogram(name: "strtof", scope: !327, file: !327, line: 172, type: !495, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!495 = !DISubroutineType(types: !496)
!496 = !{!90, !415, !441}
!497 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !498, line: 233)
!498 = !DISubprogram(name: "strtold", scope: !327, file: !327, line: 175, type: !499, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!499 = !DISubroutineType(types: !500)
!500 = !{!501, !415, !441}
!501 = !DIBasicType(name: "long double", size: 64, encoding: DW_ATE_float)
!502 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !468, line: 241)
!503 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !474, line: 243)
!504 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !476, line: 245)
!505 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !506, line: 246)
!506 = !DISubprogram(name: "div", linkageName: "_ZN9__gnu_cxx3divExx", scope: !466, file: !507, line: 214, type: !479, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!507 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/cstdlib", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!508 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !478, line: 247)
!509 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !482, line: 249)
!510 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !494, line: 250)
!511 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !486, line: 251)
!512 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !490, line: 252)
!513 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !498, line: 253)
!514 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !515, line: 418)
!515 = !DISubprogram(name: "acosf", linkageName: "_ZL5acosff", scope: !516, file: !516, line: 1126, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!516 = !DIFile(filename: "/usr/local/cuda/include/math_functions.hpp", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!517 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !518, line: 419)
!518 = !DISubprogram(name: "acoshf", linkageName: "_ZL6acoshff", scope: !516, file: !516, line: 1154, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!519 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !520, line: 420)
!520 = !DISubprogram(name: "asinf", linkageName: "_ZL5asinff", scope: !516, file: !516, line: 1121, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!521 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !522, line: 421)
!522 = !DISubprogram(name: "asinhf", linkageName: "_ZL6asinhff", scope: !516, file: !516, line: 1159, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!523 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !524, line: 422)
!524 = !DISubprogram(name: "atan2f", linkageName: "_ZL6atan2fff", scope: !516, file: !516, line: 1111, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!525 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !526, line: 423)
!526 = !DISubprogram(name: "atanf", linkageName: "_ZL5atanff", scope: !516, file: !516, line: 1116, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!527 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !528, line: 424)
!528 = !DISubprogram(name: "atanhf", linkageName: "_ZL6atanhff", scope: !516, file: !516, line: 1164, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!529 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !530, line: 425)
!530 = !DISubprogram(name: "cbrtf", linkageName: "_ZL5cbrtff", scope: !516, file: !516, line: 1199, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!531 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !532, line: 426)
!532 = !DISubprogram(name: "ceilf", linkageName: "_ZL5ceilff", scope: !533, file: !533, line: 647, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!533 = !DIFile(filename: "/usr/local/cuda/include/device_functions.hpp", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!534 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !535, line: 427)
!535 = !DISubprogram(name: "copysignf", linkageName: "_ZL9copysignfff", scope: !516, file: !516, line: 973, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!536 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !537, line: 428)
!537 = !DISubprogram(name: "cosf", linkageName: "_ZL4cosff", scope: !516, file: !516, line: 1027, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!538 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !539, line: 429)
!539 = !DISubprogram(name: "coshf", linkageName: "_ZL5coshff", scope: !516, file: !516, line: 1096, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!540 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !541, line: 430)
!541 = !DISubprogram(name: "erfcf", linkageName: "_ZL5erfcff", scope: !516, file: !516, line: 1259, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!542 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !543, line: 431)
!543 = !DISubprogram(name: "erff", linkageName: "_ZL4erfff", scope: !516, file: !516, line: 1249, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!544 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !545, line: 432)
!545 = !DISubprogram(name: "exp2f", linkageName: "_ZL5exp2ff", scope: !533, file: !533, line: 637, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!546 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !547, line: 433)
!547 = !DISubprogram(name: "expf", linkageName: "_ZL4expff", scope: !516, file: !516, line: 1078, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!548 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !549, line: 434)
!549 = !DISubprogram(name: "expm1f", linkageName: "_ZL6expm1ff", scope: !516, file: !516, line: 1169, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!550 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !551, line: 435)
!551 = !DISubprogram(name: "fabsf", linkageName: "_ZL5fabsff", scope: !533, file: !533, line: 582, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!552 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !553, line: 436)
!553 = !DISubprogram(name: "fdimf", linkageName: "_ZL5fdimfff", scope: !516, file: !516, line: 1385, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!554 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !555, line: 437)
!555 = !DISubprogram(name: "floorf", linkageName: "_ZL6floorff", scope: !533, file: !533, line: 572, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!556 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !557, line: 438)
!557 = !DISubprogram(name: "fmaf", linkageName: "_ZL4fmaffff", scope: !516, file: !516, line: 1337, type: !133, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!558 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !559, line: 439)
!559 = !DISubprogram(name: "fmaxf", linkageName: "_ZL5fmaxfff", scope: !533, file: !533, line: 602, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!560 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !561, line: 440)
!561 = !DISubprogram(name: "fminf", linkageName: "_ZL5fminfff", scope: !533, file: !533, line: 597, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!562 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !563, line: 441)
!563 = !DISubprogram(name: "fmodf", linkageName: "_ZL5fmodfff", scope: !516, file: !516, line: 1322, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!564 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !565, line: 442)
!565 = !DISubprogram(name: "frexpf", linkageName: "_ZL6frexpffPi", scope: !516, file: !516, line: 1312, type: !147, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!566 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !567, line: 443)
!567 = !DISubprogram(name: "hypotf", linkageName: "_ZL6hypotfff", scope: !516, file: !516, line: 1174, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!568 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !569, line: 444)
!569 = !DISubprogram(name: "ilogbf", linkageName: "_ZL6ilogbff", scope: !516, file: !516, line: 1390, type: !143, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!570 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !571, line: 445)
!571 = !DISubprogram(name: "ldexpf", linkageName: "_ZL6ldexpffi", scope: !516, file: !516, line: 1289, type: !183, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!572 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !573, line: 446)
!573 = !DISubprogram(name: "lgammaf", linkageName: "_ZL7lgammaff", scope: !516, file: !516, line: 1284, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!574 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !575, line: 447)
!575 = !DISubprogram(name: "llrintf", linkageName: "_ZL7llrintff", scope: !516, file: !516, line: 933, type: !191, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!576 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !577, line: 448)
!577 = !DISubprogram(name: "llroundf", linkageName: "_ZL8llroundff", scope: !516, file: !516, line: 1371, type: !191, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!578 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !579, line: 449)
!579 = !DISubprogram(name: "log10f", linkageName: "_ZL6log10ff", scope: !516, file: !516, line: 1140, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!580 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !581, line: 450)
!581 = !DISubprogram(name: "log1pf", linkageName: "_ZL6log1pff", scope: !516, file: !516, line: 1149, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!582 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !583, line: 451)
!583 = !DISubprogram(name: "log2f", linkageName: "_ZL5log2ff", scope: !516, file: !516, line: 1069, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!584 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !585, line: 452)
!585 = !DISubprogram(name: "logbf", linkageName: "_ZL5logbff", scope: !516, file: !516, line: 1395, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!586 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !587, line: 453)
!587 = !DISubprogram(name: "logf", linkageName: "_ZL4logff", scope: !516, file: !516, line: 1131, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!588 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !589, line: 454)
!589 = !DISubprogram(name: "lrintf", linkageName: "_ZL6lrintff", scope: !516, file: !516, line: 924, type: !205, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!590 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !591, line: 455)
!591 = !DISubprogram(name: "lroundf", linkageName: "_ZL7lroundff", scope: !516, file: !516, line: 1376, type: !205, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!592 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !593, line: 456)
!593 = !DISubprogram(name: "modff", linkageName: "_ZL5modfffPf", scope: !516, file: !516, line: 1317, type: !213, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!594 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !595, line: 457)
!595 = !DISubprogram(name: "nearbyintf", linkageName: "_ZL10nearbyintff", scope: !516, file: !516, line: 938, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!596 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !597, line: 458)
!597 = !DISubprogram(name: "nextafterf", linkageName: "_ZL10nextafterfff", scope: !516, file: !516, line: 1002, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!598 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !599, line: 459)
!599 = !DISubprogram(name: "nexttowardf", scope: !271, file: !271, line: 284, type: !600, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!600 = !DISubroutineType(types: !601)
!601 = !{!90, !90, !501}
!602 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !599, line: 460)
!603 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !604, line: 461)
!604 = !DISubprogram(name: "powf", linkageName: "_ZL4powfff", scope: !516, file: !516, line: 1352, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!605 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !606, line: 462)
!606 = !DISubprogram(name: "remainderf", linkageName: "_ZL10remainderfff", scope: !516, file: !516, line: 1327, type: !101, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!607 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !608, line: 463)
!608 = !DISubprogram(name: "remquof", linkageName: "_ZL7remquofffPi", scope: !516, file: !516, line: 1332, type: !241, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!609 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !610, line: 464)
!610 = !DISubprogram(name: "rintf", linkageName: "_ZL5rintff", scope: !516, file: !516, line: 919, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!611 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !612, line: 465)
!612 = !DISubprogram(name: "roundf", linkageName: "_ZL6roundff", scope: !516, file: !516, line: 1366, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!613 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !614, line: 466)
!614 = !DISubprogram(name: "scalblnf", linkageName: "_ZL8scalblnffl", scope: !516, file: !516, line: 1299, type: !249, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!615 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !616, line: 467)
!616 = !DISubprogram(name: "scalbnf", linkageName: "_ZL7scalbnffi", scope: !516, file: !516, line: 1294, type: !183, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!617 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !618, line: 468)
!618 = !DISubprogram(name: "sinf", linkageName: "_ZL4sinff", scope: !516, file: !516, line: 1018, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!619 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !620, line: 469)
!620 = !DISubprogram(name: "sinhf", linkageName: "_ZL5sinhff", scope: !516, file: !516, line: 1101, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!621 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !622, line: 470)
!622 = !DISubprogram(name: "sqrtf", linkageName: "_ZL5sqrtff", scope: !533, file: !533, line: 887, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!623 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !624, line: 471)
!624 = !DISubprogram(name: "tanf", linkageName: "_ZL4tanff", scope: !516, file: !516, line: 1060, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!625 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !626, line: 472)
!626 = !DISubprogram(name: "tanhf", linkageName: "_ZL5tanhff", scope: !516, file: !516, line: 1106, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!627 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !628, line: 473)
!628 = !DISubprogram(name: "tgammaf", linkageName: "_ZL7tgammaff", scope: !516, file: !516, line: 1361, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!629 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !630, line: 474)
!630 = !DISubprogram(name: "truncf", linkageName: "_ZL6truncff", scope: !533, file: !533, line: 642, type: !88, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!631 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !632, line: 64)
!632 = !DIDerivedType(tag: DW_TAG_typedef, name: "mbstate_t", file: !633, line: 106, baseType: !634)
!633 = !DIFile(filename: "/usr/include/wchar.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!634 = !DIDerivedType(tag: DW_TAG_typedef, name: "__mbstate_t", file: !633, line: 94, baseType: !635)
!635 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !633, line: 82, flags: DIFlagFwdDecl, identifier: "_ZTS11__mbstate_t")
!636 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !637, line: 139)
!637 = !DIDerivedType(tag: DW_TAG_typedef, name: "wint_t", file: !368, line: 132, baseType: !436)
!638 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !639, line: 141)
!639 = !DISubprogram(name: "btowc", scope: !633, file: !633, line: 388, type: !640, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!640 = !DISubroutineType(types: !641)
!641 = !{!637, !8}
!642 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !643, line: 142)
!643 = !DISubprogram(name: "fgetwc", scope: !633, file: !633, line: 745, type: !644, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!644 = !DISubroutineType(types: !645)
!645 = !{!637, !646}
!646 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !647, size: 64)
!647 = !DIDerivedType(tag: DW_TAG_typedef, name: "__FILE", file: !648, line: 64, baseType: !649)
!648 = !DIFile(filename: "/usr/include/stdio.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!649 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "_IO_FILE", file: !648, line: 44, flags: DIFlagFwdDecl, identifier: "_ZTS8_IO_FILE")
!650 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !651, line: 143)
!651 = !DISubprogram(name: "fgetws", scope: !633, file: !633, line: 774, type: !652, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!652 = !DISubroutineType(types: !653)
!653 = !{!413, !412, !8, !654}
!654 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !646)
!655 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !656, line: 144)
!656 = !DISubprogram(name: "fputwc", scope: !633, file: !633, line: 759, type: !657, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!657 = !DISubroutineType(types: !658)
!658 = !{!637, !414, !646}
!659 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !660, line: 145)
!660 = !DISubprogram(name: "fputws", scope: !633, file: !633, line: 781, type: !661, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!661 = !DISubroutineType(types: !662)
!662 = !{!8, !458, !654}
!663 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !664, line: 146)
!664 = !DISubprogram(name: "fwide", scope: !633, file: !633, line: 587, type: !665, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!665 = !DISubroutineType(types: !666)
!666 = !{!8, !646, !8}
!667 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !668, line: 147)
!668 = !DISubprogram(name: "fwprintf", scope: !633, file: !633, line: 594, type: !669, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!669 = !DISubroutineType(types: !670)
!670 = !{!8, !654, !458, null}
!671 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !672, line: 148)
!672 = !DISubprogram(name: "fwscanf", scope: !633, file: !633, line: 635, type: !669, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!673 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !674, line: 149)
!674 = !DISubprogram(name: "getwc", scope: !633, file: !633, line: 746, type: !644, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!675 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !676, line: 150)
!676 = !DISubprogram(name: "getwchar", scope: !633, file: !633, line: 752, type: !677, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!677 = !DISubroutineType(types: !678)
!678 = !{!637}
!679 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !680, line: 151)
!680 = !DISubprogram(name: "mbrlen", scope: !633, file: !633, line: 399, type: !681, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!681 = !DISubroutineType(types: !682)
!682 = !{!367, !415, !367, !683}
!683 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !684)
!684 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !632, size: 64)
!685 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !686, line: 152)
!686 = !DISubprogram(name: "mbrtowc", scope: !633, file: !633, line: 365, type: !687, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!687 = !DISubroutineType(types: !688)
!688 = !{!367, !412, !415, !367, !683}
!689 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !690, line: 153)
!690 = !DISubprogram(name: "mbsinit", scope: !633, file: !633, line: 361, type: !691, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!691 = !DISubroutineType(types: !692)
!692 = !{!8, !693}
!693 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !694, size: 64)
!694 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !632)
!695 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !696, line: 154)
!696 = !DISubprogram(name: "mbsrtowcs", scope: !633, file: !633, line: 408, type: !697, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!697 = !DISubroutineType(types: !698)
!698 = !{!367, !412, !699, !367, !683}
!699 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !700)
!700 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !221, size: 64)
!701 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !702, line: 155)
!702 = !DISubprogram(name: "putwc", scope: !633, file: !633, line: 760, type: !657, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!703 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !704, line: 156)
!704 = !DISubprogram(name: "putwchar", scope: !633, file: !633, line: 766, type: !705, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!705 = !DISubroutineType(types: !706)
!706 = !{!637, !414}
!707 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !708, line: 158)
!708 = !DISubprogram(name: "swprintf", scope: !633, file: !633, line: 604, type: !709, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!709 = !DISubroutineType(types: !710)
!710 = !{!8, !412, !367, !458, null}
!711 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !712, line: 160)
!712 = !DISubprogram(name: "swscanf", scope: !633, file: !633, line: 645, type: !713, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!713 = !DISubroutineType(types: !714)
!714 = !{!8, !458, !458, null}
!715 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !716, line: 161)
!716 = !DISubprogram(name: "ungetwc", scope: !633, file: !633, line: 789, type: !717, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!717 = !DISubroutineType(types: !718)
!718 = !{!637, !637, !646}
!719 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !720, line: 162)
!720 = !DISubprogram(name: "vfwprintf", scope: !633, file: !633, line: 612, type: !721, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!721 = !DISubroutineType(types: !722)
!722 = !{!8, !654, !458, !723}
!723 = !DIDerivedType(tag: DW_TAG_typedef, name: "__gnuc_va_list", file: !724, line: 48, baseType: !725)
!724 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/stdarg.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!725 = !DIDerivedType(tag: DW_TAG_typedef, name: "__builtin_va_list", file: !3, baseType: !6)
!726 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !727, line: 164)
!727 = !DISubprogram(name: "vfwscanf", scope: !633, file: !633, line: 689, type: !721, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!728 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !729, line: 167)
!729 = !DISubprogram(name: "vswprintf", scope: !633, file: !633, line: 625, type: !730, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!730 = !DISubroutineType(types: !731)
!731 = !{!8, !412, !367, !458, !723}
!732 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !733, line: 170)
!733 = !DISubprogram(name: "vswscanf", scope: !633, file: !633, line: 701, type: !734, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!734 = !DISubroutineType(types: !735)
!735 = !{!8, !458, !458, !723}
!736 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !737, line: 172)
!737 = !DISubprogram(name: "vwprintf", scope: !633, file: !633, line: 620, type: !738, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!738 = !DISubroutineType(types: !739)
!739 = !{!8, !458, !723}
!740 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !741, line: 174)
!741 = !DISubprogram(name: "vwscanf", scope: !633, file: !633, line: 697, type: !738, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!742 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !743, line: 176)
!743 = !DISubprogram(name: "wcrtomb", scope: !633, file: !633, line: 370, type: !744, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!744 = !DISubroutineType(types: !745)
!745 = !{!367, !457, !414, !683}
!746 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !747, line: 177)
!747 = !DISubprogram(name: "wcscat", scope: !633, file: !633, line: 155, type: !748, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!748 = !DISubroutineType(types: !749)
!749 = !{!413, !412, !458}
!750 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !751, line: 178)
!751 = !DISubprogram(name: "wcscmp", scope: !633, file: !633, line: 163, type: !752, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!752 = !DISubroutineType(types: !753)
!753 = !{!8, !459, !459}
!754 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !755, line: 179)
!755 = !DISubprogram(name: "wcscoll", scope: !633, file: !633, line: 192, type: !752, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!756 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !757, line: 180)
!757 = !DISubprogram(name: "wcscpy", scope: !633, file: !633, line: 147, type: !748, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!758 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !759, line: 181)
!759 = !DISubprogram(name: "wcscspn", scope: !633, file: !633, line: 252, type: !760, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!760 = !DISubroutineType(types: !761)
!761 = !{!367, !459, !459}
!762 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !763, line: 182)
!763 = !DISubprogram(name: "wcsftime", scope: !633, file: !633, line: 855, type: !764, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!764 = !DISubroutineType(types: !765)
!765 = !{!367, !412, !367, !458, !766}
!766 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !767)
!767 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !768, size: 64)
!768 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !769)
!769 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "tm", file: !633, line: 137, flags: DIFlagFwdDecl, identifier: "_ZTS2tm")
!770 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !771, line: 183)
!771 = !DISubprogram(name: "wcslen", scope: !633, file: !633, line: 287, type: !772, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!772 = !DISubroutineType(types: !773)
!773 = !{!367, !459}
!774 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !775, line: 184)
!775 = !DISubprogram(name: "wcsncat", scope: !633, file: !633, line: 158, type: !776, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!776 = !DISubroutineType(types: !777)
!777 = !{!413, !412, !458, !367}
!778 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !779, line: 185)
!779 = !DISubprogram(name: "wcsncmp", scope: !633, file: !633, line: 166, type: !780, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!780 = !DISubroutineType(types: !781)
!781 = !{!8, !459, !459, !367}
!782 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !783, line: 186)
!783 = !DISubprogram(name: "wcsncpy", scope: !633, file: !633, line: 150, type: !776, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!784 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !785, line: 187)
!785 = !DISubprogram(name: "wcsrtombs", scope: !633, file: !633, line: 414, type: !786, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!786 = !DISubroutineType(types: !787)
!787 = !{!367, !457, !788, !367, !683}
!788 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !789)
!789 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !459, size: 64)
!790 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !791, line: 188)
!791 = !DISubprogram(name: "wcsspn", scope: !633, file: !633, line: 256, type: !760, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!792 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !793, line: 189)
!793 = !DISubprogram(name: "wcstod", scope: !633, file: !633, line: 450, type: !794, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!794 = !DISubroutineType(types: !795)
!795 = !{!220, !458, !796}
!796 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !797)
!797 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !413, size: 64)
!798 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !799, line: 191)
!799 = !DISubprogram(name: "wcstof", scope: !633, file: !633, line: 457, type: !800, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!800 = !DISubroutineType(types: !801)
!801 = !{!90, !458, !796}
!802 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !803, line: 193)
!803 = !DISubprogram(name: "wcstok", scope: !633, file: !633, line: 282, type: !804, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!804 = !DISubroutineType(types: !805)
!805 = !{!413, !412, !458, !796}
!806 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !807, line: 194)
!807 = !DISubprogram(name: "wcstol", scope: !633, file: !633, line: 468, type: !808, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!808 = !DISubroutineType(types: !809)
!809 = !{!26, !458, !796, !8}
!810 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !811, line: 195)
!811 = !DISubprogram(name: "wcstoul", scope: !633, file: !633, line: 473, type: !812, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!812 = !DISubroutineType(types: !813)
!813 = !{!369, !458, !796, !8}
!814 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !815, line: 196)
!815 = !DISubprogram(name: "wcsxfrm", scope: !633, file: !633, line: 196, type: !816, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!816 = !DISubroutineType(types: !817)
!817 = !{!367, !412, !458, !367}
!818 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !819, line: 197)
!819 = !DISubprogram(name: "wctob", scope: !633, file: !633, line: 394, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!820 = !DISubroutineType(types: !821)
!821 = !{!8, !637}
!822 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !823, line: 198)
!823 = !DISubprogram(name: "wmemcmp", scope: !633, file: !633, line: 325, type: !780, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!824 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !825, line: 199)
!825 = !DISubprogram(name: "wmemcpy", scope: !633, file: !633, line: 329, type: !776, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!826 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !827, line: 200)
!827 = !DISubprogram(name: "wmemmove", scope: !633, file: !633, line: 334, type: !828, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!828 = !DISubroutineType(types: !829)
!829 = !{!413, !413, !459, !367}
!830 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !831, line: 201)
!831 = !DISubprogram(name: "wmemset", scope: !633, file: !633, line: 338, type: !832, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!832 = !DISubroutineType(types: !833)
!833 = !{!413, !413, !414, !367}
!834 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !835, line: 202)
!835 = !DISubprogram(name: "wprintf", scope: !633, file: !633, line: 601, type: !836, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!836 = !DISubroutineType(types: !837)
!837 = !{!8, !458, null}
!838 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !839, line: 203)
!839 = !DISubprogram(name: "wscanf", scope: !633, file: !633, line: 642, type: !836, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!840 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !841, line: 204)
!841 = !DISubprogram(name: "wcschr", scope: !633, file: !633, line: 227, type: !842, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!842 = !DISubroutineType(types: !843)
!843 = !{!413, !459, !414}
!844 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !845, line: 205)
!845 = !DISubprogram(name: "wcspbrk", scope: !633, file: !633, line: 266, type: !846, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!846 = !DISubroutineType(types: !847)
!847 = !{!413, !459, !459}
!848 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !849, line: 206)
!849 = !DISubprogram(name: "wcsrchr", scope: !633, file: !633, line: 237, type: !842, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!850 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !851, line: 207)
!851 = !DISubprogram(name: "wcsstr", scope: !633, file: !633, line: 277, type: !846, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!852 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !853, line: 208)
!853 = !DISubprogram(name: "wmemchr", scope: !633, file: !633, line: 320, type: !854, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!854 = !DISubroutineType(types: !855)
!855 = !{!413, !459, !414, !367}
!856 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !857, line: 248)
!857 = !DISubprogram(name: "wcstold", scope: !633, file: !633, line: 459, type: !858, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!858 = !DISubroutineType(types: !859)
!859 = !{!501, !458, !796}
!860 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !861, line: 257)
!861 = !DISubprogram(name: "wcstoll", scope: !633, file: !633, line: 483, type: !862, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!862 = !DISubroutineType(types: !863)
!863 = !{!29, !458, !796, !8}
!864 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !865, line: 258)
!865 = !DISubprogram(name: "wcstoull", scope: !633, file: !633, line: 490, type: !866, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!866 = !DISubroutineType(types: !867)
!867 = !{!20, !458, !796, !8}
!868 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !857, line: 264)
!869 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !861, line: 265)
!870 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !865, line: 266)
!871 = !DIImportedEntity(tag: DW_TAG_imported_module, scope: !872, entity: !874, line: 56)
!872 = !DINamespace(name: "__gnu_debug", scope: null, file: !873, line: 54)
!873 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/debug/debug.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!874 = !DINamespace(name: "__debug", scope: !81, file: !873, line: 48)
!875 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !876, line: 53)
!876 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "lconv", file: !877, line: 53, flags: DIFlagFwdDecl, identifier: "_ZTS5lconv")
!877 = !DIFile(filename: "/usr/include/locale.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!878 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !879, line: 54)
!879 = !DISubprogram(name: "setlocale", scope: !877, file: !877, line: 124, type: !880, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!880 = !DISubroutineType(types: !881)
!881 = !{!6, !8, !221}
!882 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !883, line: 55)
!883 = !DISubprogram(name: "localeconv", scope: !877, file: !877, line: 127, type: !884, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!884 = !DISubroutineType(types: !885)
!885 = !{!886}
!886 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !876, size: 64)
!887 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !888, line: 64)
!888 = !DISubprogram(name: "isalnum", scope: !889, file: !889, line: 110, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!889 = !DIFile(filename: "/usr/include/ctype.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!890 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !891, line: 65)
!891 = !DISubprogram(name: "isalpha", scope: !889, file: !889, line: 111, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!892 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !893, line: 66)
!893 = !DISubprogram(name: "iscntrl", scope: !889, file: !889, line: 112, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!894 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !895, line: 67)
!895 = !DISubprogram(name: "isdigit", scope: !889, file: !889, line: 113, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!896 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !897, line: 68)
!897 = !DISubprogram(name: "isgraph", scope: !889, file: !889, line: 115, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!898 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !899, line: 69)
!899 = !DISubprogram(name: "islower", scope: !889, file: !889, line: 114, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!900 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !901, line: 70)
!901 = !DISubprogram(name: "isprint", scope: !889, file: !889, line: 116, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!902 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !903, line: 71)
!903 = !DISubprogram(name: "ispunct", scope: !889, file: !889, line: 117, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!904 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !905, line: 72)
!905 = !DISubprogram(name: "isspace", scope: !889, file: !889, line: 118, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!906 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !907, line: 73)
!907 = !DISubprogram(name: "isupper", scope: !889, file: !889, line: 119, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!908 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !909, line: 74)
!909 = !DISubprogram(name: "isxdigit", scope: !889, file: !889, line: 120, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!910 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !911, line: 75)
!911 = !DISubprogram(name: "tolower", scope: !889, file: !889, line: 124, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!912 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !913, line: 76)
!913 = !DISubprogram(name: "toupper", scope: !889, file: !889, line: 127, type: !341, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!914 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !915, line: 44)
!915 = !DIDerivedType(tag: DW_TAG_typedef, name: "size_t", scope: !81, file: !916, line: 186, baseType: !369)
!916 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/x86_64-linux-gnu/c++/4.8/bits/c++config.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!917 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !466, entity: !918, line: 45)
!918 = !DIDerivedType(tag: DW_TAG_typedef, name: "ptrdiff_t", scope: !81, file: !916, line: 187, baseType: !26)
!919 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !920, line: 82)
!920 = !DIDerivedType(tag: DW_TAG_typedef, name: "wctrans_t", file: !921, line: 186, baseType: !922)
!921 = !DIFile(filename: "/usr/include/wctype.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!922 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !923, size: 64)
!923 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !924)
!924 = !DIDerivedType(tag: DW_TAG_typedef, name: "__int32_t", file: !925, line: 40, baseType: !8)
!925 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/types.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!926 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !927, line: 83)
!927 = !DIDerivedType(tag: DW_TAG_typedef, name: "wctype_t", file: !921, line: 52, baseType: !369)
!928 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !637, line: 84)
!929 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !930, line: 86)
!930 = !DISubprogram(name: "iswalnum", scope: !921, file: !921, line: 111, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!931 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !932, line: 87)
!932 = !DISubprogram(name: "iswalpha", scope: !921, file: !921, line: 117, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!933 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !934, line: 89)
!934 = !DISubprogram(name: "iswblank", scope: !921, file: !921, line: 162, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!935 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !936, line: 91)
!936 = !DISubprogram(name: "iswcntrl", scope: !921, file: !921, line: 120, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!937 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !938, line: 92)
!938 = !DISubprogram(name: "iswctype", scope: !921, file: !921, line: 175, type: !939, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!939 = !DISubroutineType(types: !940)
!940 = !{!8, !637, !927}
!941 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !942, line: 93)
!942 = !DISubprogram(name: "iswdigit", scope: !921, file: !921, line: 124, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!943 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !944, line: 94)
!944 = !DISubprogram(name: "iswgraph", scope: !921, file: !921, line: 128, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!945 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !946, line: 95)
!946 = !DISubprogram(name: "iswlower", scope: !921, file: !921, line: 133, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!947 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !948, line: 96)
!948 = !DISubprogram(name: "iswprint", scope: !921, file: !921, line: 136, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!949 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !950, line: 97)
!950 = !DISubprogram(name: "iswpunct", scope: !921, file: !921, line: 141, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!951 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !952, line: 98)
!952 = !DISubprogram(name: "iswspace", scope: !921, file: !921, line: 146, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!953 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !954, line: 99)
!954 = !DISubprogram(name: "iswupper", scope: !921, file: !921, line: 151, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!955 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !956, line: 100)
!956 = !DISubprogram(name: "iswxdigit", scope: !921, file: !921, line: 156, type: !820, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!957 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !958, line: 101)
!958 = !DISubprogram(name: "towctrans", scope: !921, file: !921, line: 221, type: !959, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!959 = !DISubroutineType(types: !960)
!960 = !{!637, !637, !920}
!961 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !962, line: 102)
!962 = !DISubprogram(name: "towlower", scope: !921, file: !921, line: 194, type: !963, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!963 = !DISubroutineType(types: !964)
!964 = !{!637, !637}
!965 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !966, line: 103)
!966 = !DISubprogram(name: "towupper", scope: !921, file: !921, line: 197, type: !963, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!967 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !968, line: 104)
!968 = !DISubprogram(name: "wctrans", scope: !921, file: !921, line: 218, type: !969, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!969 = !DISubroutineType(types: !970)
!970 = !{!920, !221}
!971 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !81, entity: !972, line: 105)
!972 = !DISubprogram(name: "wctype", scope: !921, file: !921, line: 171, type: !973, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!973 = !DISubroutineType(types: !974)
!974 = !{!927, !221}
!975 = distinct !DICompileUnit(language: DW_LANG_C_plus_plus, file: !976, producer: "clang version 5.0.0 (trunk 294196)", isOptimized: true, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, imports: !977)
!976 = !DIFile(filename: "syr2k.cu", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!977 = !{!80, !86, !91, !93, !95, !97, !99, !103, !105, !107, !109, !111, !113, !115, !117, !119, !121, !123, !125, !127, !129, !131, !135, !137, !139, !141, !145, !149, !151, !153, !157, !161, !163, !165, !167, !169, !171, !173, !175, !177, !181, !185, !187, !189, !193, !195, !197, !199, !201, !203, !207, !209, !211, !216, !223, !227, !229, !231, !235, !237, !239, !243, !245, !247, !251, !253, !255, !257, !259, !261, !263, !265, !267, !269, !274, !276, !278, !282, !284, !286, !288, !290, !292, !294, !296, !300, !304, !306, !308, !313, !315, !317, !319, !321, !323, !325, !329, !335, !339, !343, !348, !351, !355, !359, !374, !378, !382, !386, !390, !394, !396, !400, !404, !408, !416, !420, !424, !428, !432, !437, !443, !447, !451, !453, !461, !465, !473, !475, !477, !481, !485, !489, !493, !497, !502, !503, !504, !505, !508, !509, !510, !511, !512, !513, !514, !517, !519, !521, !523, !525, !527, !529, !531, !534, !536, !538, !540, !542, !544, !546, !548, !550, !552, !554, !556, !558, !560, !562, !564, !566, !568, !570, !572, !574, !576, !578, !580, !582, !584, !586, !588, !590, !592, !594, !596, !598, !602, !603, !605, !607, !609, !611, !613, !615, !617, !619, !621, !623, !625, !627, !629}
!978 = !{void (float*, float*, float*)* @_Z12syr2k_kernelPfS_S_, !"kernel", i32 1}
!979 = !{null, !"align", i32 8}
!980 = !{null, !"align", i32 8, !"align", i32 65544, !"align", i32 131080}
!981 = !{null, !"align", i32 16}
!982 = !{null, !"align", i32 16, !"align", i32 65552, !"align", i32 131088}
!983 = !{!"clang version 5.0.0 (trunk 294196)"}
!984 = !{i32 1, i32 2}
!985 = !{i32 2, !"Dwarf Version", i32 4}
!986 = !{i32 2, !"Debug Info Version", i32 3}
!987 = !{i32 4, !"nvvm-reflect-ftz", i32 0}
!988 = distinct !DISubprogram(name: "syr2k_kernel", linkageName: "_Z12syr2k_kernelPfS_S_", scope: !976, file: !976, line: 119, type: !989, isLocal: false, isDefinition: true, scopeLine: 120, flags: DIFlagPrototyped, isOptimized: true, unit: !975, variables: !993)
!989 = !DISubroutineType(types: !990)
!990 = !{null, !991, !991, !991}
!991 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !992, size: 64)
!992 = !DIDerivedType(tag: DW_TAG_typedef, name: "DATA_TYPE", file: !976, line: 38, baseType: !90)
!993 = !{!994, !995, !996, !997, !998, !999}
!994 = !DILocalVariable(name: "a", arg: 1, scope: !988, file: !976, line: 119, type: !991)
!995 = !DILocalVariable(name: "b", arg: 2, scope: !988, file: !976, line: 119, type: !991)
!996 = !DILocalVariable(name: "c", arg: 3, scope: !988, file: !976, line: 119, type: !991)
!997 = !DILocalVariable(name: "j", scope: !988, file: !976, line: 121, type: !8)
!998 = !DILocalVariable(name: "i", scope: !988, file: !976, line: 122, type: !8)
!999 = !DILocalVariable(name: "k", scope: !1000, file: !976, line: 128, type: !8)
!1000 = distinct !DILexicalBlock(scope: !1001, file: !976, line: 125, column: 2)
!1001 = distinct !DILexicalBlock(scope: !988, file: !976, line: 124, column: 6)
!1002 = !DIExpression()
!1003 = !DILocation(line: 119, column: 41, scope: !988)
!1004 = !DILocation(line: 119, column: 55, scope: !988)
!1005 = !DILocation(line: 119, column: 69, scope: !988)
!1006 = !DILocation(line: 78, column: 3, scope: !1007, inlinedAt: !1042)
!1007 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !1009, file: !1008, line: 78, type: !1012, isLocal: false, isDefinition: true, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true, unit: !975, declaration: !1011, variables: !4)
!1008 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_builtin_vars.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!1009 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_blockIdx_t", file: !1008, line: 77, size: 8, elements: !1010, identifier: "_ZTS25__cuda_builtin_blockIdx_t")
!1010 = !{!1011, !1014, !1015, !1016, !1027, !1031, !1035, !1038}
!1011 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !1009, file: !1008, line: 78, type: !1012, isLocal: false, isDefinition: false, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true)
!1012 = !DISubroutineType(types: !1013)
!1013 = !{!436}
!1014 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_yEv", scope: !1009, file: !1008, line: 79, type: !1012, isLocal: false, isDefinition: false, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true)
!1015 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_zEv", scope: !1009, file: !1008, line: 80, type: !1012, isLocal: false, isDefinition: false, scopeLine: 80, flags: DIFlagPrototyped, isOptimized: true)
!1016 = !DISubprogram(name: "operator uint3", linkageName: "_ZNK25__cuda_builtin_blockIdx_tcv5uint3Ev", scope: !1009, file: !1008, line: 83, type: !1017, isLocal: false, isDefinition: false, scopeLine: 83, flags: DIFlagPrototyped, isOptimized: true)
!1017 = !DISubroutineType(types: !1018)
!1018 = !{!1019, !1025}
!1019 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "uint3", file: !1020, line: 190, size: 96, elements: !1021, identifier: "_ZTS5uint3")
!1020 = !DIFile(filename: "/usr/local/cuda/include/vector_types.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!1021 = !{!1022, !1023, !1024}
!1022 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !1019, file: !1020, line: 192, baseType: !436, size: 32)
!1023 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !1019, file: !1020, line: 192, baseType: !436, size: 32, offset: 32)
!1024 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !1019, file: !1020, line: 192, baseType: !436, size: 32, offset: 64)
!1025 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1026, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1026 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1009)
!1027 = !DISubprogram(name: "__cuda_builtin_blockIdx_t", scope: !1009, file: !1008, line: 85, type: !1028, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1028 = !DISubroutineType(types: !1029)
!1029 = !{null, !1030}
!1030 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1009, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1031 = !DISubprogram(name: "__cuda_builtin_blockIdx_t", scope: !1009, file: !1008, line: 85, type: !1032, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1032 = !DISubroutineType(types: !1033)
!1033 = !{null, !1030, !1034}
!1034 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1026, size: 64)
!1035 = !DISubprogram(name: "operator=", linkageName: "_ZNK25__cuda_builtin_blockIdx_taSERKS_", scope: !1009, file: !1008, line: 85, type: !1036, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1036 = !DISubroutineType(types: !1037)
!1037 = !{null, !1025, !1034}
!1038 = !DISubprogram(name: "operator&", linkageName: "_ZNK25__cuda_builtin_blockIdx_tadEv", scope: !1009, file: !1008, line: 85, type: !1039, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1039 = !DISubroutineType(types: !1040)
!1040 = !{!1041, !1025}
!1041 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1009, size: 64)
!1042 = distinct !DILocation(line: 121, column: 10, scope: !988)
!1043 = !{i32 0, i32 65535}
!1044 = !DILocation(line: 89, column: 3, scope: !1045, inlinedAt: !1087)
!1045 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_xEv", scope: !1046, file: !1008, line: 89, type: !1012, isLocal: false, isDefinition: true, scopeLine: 89, flags: DIFlagPrototyped, isOptimized: true, unit: !975, declaration: !1048, variables: !4)
!1046 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_blockDim_t", file: !1008, line: 88, size: 8, elements: !1047, identifier: "_ZTS25__cuda_builtin_blockDim_t")
!1047 = !{!1048, !1049, !1050, !1051, !1072, !1076, !1080, !1083}
!1048 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_xEv", scope: !1046, file: !1008, line: 89, type: !1012, isLocal: false, isDefinition: false, scopeLine: 89, flags: DIFlagPrototyped, isOptimized: true)
!1049 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_yEv", scope: !1046, file: !1008, line: 90, type: !1012, isLocal: false, isDefinition: false, scopeLine: 90, flags: DIFlagPrototyped, isOptimized: true)
!1050 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_zEv", scope: !1046, file: !1008, line: 91, type: !1012, isLocal: false, isDefinition: false, scopeLine: 91, flags: DIFlagPrototyped, isOptimized: true)
!1051 = !DISubprogram(name: "operator dim3", linkageName: "_ZNK25__cuda_builtin_blockDim_tcv4dim3Ev", scope: !1046, file: !1008, line: 94, type: !1052, isLocal: false, isDefinition: false, scopeLine: 94, flags: DIFlagPrototyped, isOptimized: true)
!1052 = !DISubroutineType(types: !1053)
!1053 = !{!1054, !1070}
!1054 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "dim3", file: !1020, line: 417, size: 96, elements: !1055, identifier: "_ZTS4dim3")
!1055 = !{!1056, !1057, !1058, !1059, !1063, !1067}
!1056 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !1054, file: !1020, line: 419, baseType: !436, size: 32)
!1057 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !1054, file: !1020, line: 419, baseType: !436, size: 32, offset: 32)
!1058 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !1054, file: !1020, line: 419, baseType: !436, size: 32, offset: 64)
!1059 = !DISubprogram(name: "dim3", scope: !1054, file: !1020, line: 421, type: !1060, isLocal: false, isDefinition: false, scopeLine: 421, flags: DIFlagPrototyped, isOptimized: true)
!1060 = !DISubroutineType(types: !1061)
!1061 = !{null, !1062, !436, !436, !436}
!1062 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1054, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1063 = !DISubprogram(name: "dim3", scope: !1054, file: !1020, line: 422, type: !1064, isLocal: false, isDefinition: false, scopeLine: 422, flags: DIFlagPrototyped, isOptimized: true)
!1064 = !DISubroutineType(types: !1065)
!1065 = !{null, !1062, !1066}
!1066 = !DIDerivedType(tag: DW_TAG_typedef, name: "uint3", file: !1020, line: 383, baseType: !1019)
!1067 = !DISubprogram(name: "operator uint3", linkageName: "_ZN4dim3cv5uint3Ev", scope: !1054, file: !1020, line: 423, type: !1068, isLocal: false, isDefinition: false, scopeLine: 423, flags: DIFlagPrototyped, isOptimized: true)
!1068 = !DISubroutineType(types: !1069)
!1069 = !{!1066, !1062}
!1070 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1071, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1071 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1046)
!1072 = !DISubprogram(name: "__cuda_builtin_blockDim_t", scope: !1046, file: !1008, line: 96, type: !1073, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1073 = !DISubroutineType(types: !1074)
!1074 = !{null, !1075}
!1075 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1046, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1076 = !DISubprogram(name: "__cuda_builtin_blockDim_t", scope: !1046, file: !1008, line: 96, type: !1077, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1077 = !DISubroutineType(types: !1078)
!1078 = !{null, !1075, !1079}
!1079 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1071, size: 64)
!1080 = !DISubprogram(name: "operator=", linkageName: "_ZNK25__cuda_builtin_blockDim_taSERKS_", scope: !1046, file: !1008, line: 96, type: !1081, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1081 = !DISubroutineType(types: !1082)
!1082 = !{null, !1070, !1079}
!1083 = !DISubprogram(name: "operator&", linkageName: "_ZNK25__cuda_builtin_blockDim_tadEv", scope: !1046, file: !1008, line: 96, type: !1084, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1084 = !DISubroutineType(types: !1085)
!1085 = !{!1086, !1070}
!1086 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1046, size: 64)
!1087 = distinct !DILocation(line: 121, column: 23, scope: !1088)
!1088 = !DILexicalBlockFile(scope: !988, file: !976, discriminator: 1)
!1089 = !{i32 1, i32 1025}
!1090 = !DILocation(line: 121, column: 21, scope: !988)
!1091 = !DILocation(line: 67, column: 3, scope: !1092, inlinedAt: !1118)
!1092 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !1093, file: !1008, line: 67, type: !1012, isLocal: false, isDefinition: true, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true, unit: !975, declaration: !1095, variables: !4)
!1093 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_threadIdx_t", file: !1008, line: 66, size: 8, elements: !1094, identifier: "_ZTS26__cuda_builtin_threadIdx_t")
!1094 = !{!1095, !1096, !1097, !1098, !1103, !1107, !1111, !1114}
!1095 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !1093, file: !1008, line: 67, type: !1012, isLocal: false, isDefinition: false, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true)
!1096 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_yEv", scope: !1093, file: !1008, line: 68, type: !1012, isLocal: false, isDefinition: false, scopeLine: 68, flags: DIFlagPrototyped, isOptimized: true)
!1097 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_zEv", scope: !1093, file: !1008, line: 69, type: !1012, isLocal: false, isDefinition: false, scopeLine: 69, flags: DIFlagPrototyped, isOptimized: true)
!1098 = !DISubprogram(name: "operator uint3", linkageName: "_ZNK26__cuda_builtin_threadIdx_tcv5uint3Ev", scope: !1093, file: !1008, line: 72, type: !1099, isLocal: false, isDefinition: false, scopeLine: 72, flags: DIFlagPrototyped, isOptimized: true)
!1099 = !DISubroutineType(types: !1100)
!1100 = !{!1019, !1101}
!1101 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1102, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1102 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1093)
!1103 = !DISubprogram(name: "__cuda_builtin_threadIdx_t", scope: !1093, file: !1008, line: 74, type: !1104, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1104 = !DISubroutineType(types: !1105)
!1105 = !{null, !1106}
!1106 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1093, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1107 = !DISubprogram(name: "__cuda_builtin_threadIdx_t", scope: !1093, file: !1008, line: 74, type: !1108, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1108 = !DISubroutineType(types: !1109)
!1109 = !{null, !1106, !1110}
!1110 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1102, size: 64)
!1111 = !DISubprogram(name: "operator=", linkageName: "_ZNK26__cuda_builtin_threadIdx_taSERKS_", scope: !1093, file: !1008, line: 74, type: !1112, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1112 = !DISubroutineType(types: !1113)
!1113 = !{null, !1101, !1110}
!1114 = !DISubprogram(name: "operator&", linkageName: "_ZNK26__cuda_builtin_threadIdx_tadEv", scope: !1093, file: !1008, line: 74, type: !1115, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1115 = !DISubroutineType(types: !1116)
!1116 = !{!1117, !1101}
!1117 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1093, size: 64)
!1118 = distinct !DILocation(line: 121, column: 36, scope: !1119)
!1119 = !DILexicalBlockFile(scope: !988, file: !976, discriminator: 2)
!1120 = !{i32 0, i32 1024}
!1121 = !DILocation(line: 121, column: 34, scope: !988)
!1122 = !DILocation(line: 121, column: 6, scope: !988)
!1123 = !DILocation(line: 79, column: 3, scope: !1124, inlinedAt: !1125)
!1124 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_yEv", scope: !1009, file: !1008, line: 79, type: !1012, isLocal: false, isDefinition: true, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true, unit: !975, declaration: !1014, variables: !4)
!1125 = distinct !DILocation(line: 122, column: 10, scope: !988)
!1126 = !DILocation(line: 90, column: 3, scope: !1127, inlinedAt: !1128)
!1127 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_yEv", scope: !1046, file: !1008, line: 90, type: !1012, isLocal: false, isDefinition: true, scopeLine: 90, flags: DIFlagPrototyped, isOptimized: true, unit: !975, declaration: !1049, variables: !4)
!1128 = distinct !DILocation(line: 122, column: 23, scope: !1088)
!1129 = !DILocation(line: 122, column: 21, scope: !988)
!1130 = !DILocation(line: 68, column: 3, scope: !1131, inlinedAt: !1132)
!1131 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_yEv", scope: !1093, file: !1008, line: 68, type: !1012, isLocal: false, isDefinition: true, scopeLine: 68, flags: DIFlagPrototyped, isOptimized: true, unit: !975, declaration: !1096, variables: !4)
!1132 = distinct !DILocation(line: 122, column: 36, scope: !1119)
!1133 = !DILocation(line: 122, column: 34, scope: !988)
!1134 = !DILocation(line: 122, column: 6, scope: !988)
!1135 = !DILocation(line: 124, column: 14, scope: !1001)
!1136 = !DILocation(line: 126, column: 7, scope: !1000)
!1137 = !DILocation(line: 126, column: 11, scope: !1000)
!1138 = !DILocation(line: 126, column: 3, scope: !1000)
!1139 = !DILocation(line: 126, column: 16, scope: !1000)
!1140 = !{!1141, !1141, i64 0}
!1141 = !{!"float", !1142, i64 0}
!1142 = !{!"omnipotent char", !1143, i64 0}
!1143 = !{!"Simple C++ TBAA"}
!1144 = !DILocation(line: 128, column: 7, scope: !1000)
!1145 = !DILocation(line: 129, column: 3, scope: !1146)
!1146 = !DILexicalBlockFile(scope: !1147, file: !976, discriminator: 1)
!1147 = distinct !DILexicalBlock(scope: !1000, file: !976, line: 129, column: 3)
!1148 = !DILocation(line: 131, column: 17, scope: !1149)
!1149 = distinct !DILexicalBlock(scope: !1150, file: !976, line: 130, column: 3)
!1150 = distinct !DILexicalBlock(scope: !1147, file: !976, line: 129, column: 3)
!1151 = !DILocation(line: 131, column: 36, scope: !1149)
!1152 = !DILocation(line: 131, column: 28, scope: !1149)
!1153 = !DILocation(line: 131, column: 26, scope: !1149)
!1154 = !DILocation(line: 131, column: 51, scope: !1149)
!1155 = !DILocation(line: 131, column: 43, scope: !1149)
!1156 = !DILocation(line: 131, column: 41, scope: !1149)
!1157 = !DILocation(line: 131, column: 66, scope: !1149)
!1158 = !DILocation(line: 131, column: 64, scope: !1149)
!1159 = !DILocation(line: 131, column: 81, scope: !1149)
!1160 = !DILocation(line: 131, column: 79, scope: !1149)
!1161 = !DILocation(line: 131, column: 56, scope: !1149)
!1162 = !DILocation(line: 129, column: 22, scope: !1163)
!1163 = !DILexicalBlockFile(scope: !1150, file: !976, discriminator: 2)
!1164 = !DILocation(line: 129, column: 16, scope: !1165)
!1165 = !DILexicalBlockFile(scope: !1150, file: !976, discriminator: 1)
!1166 = distinct !{!1166, !1167, !1168}
!1167 = !DILocation(line: 129, column: 3, scope: !1147)
!1168 = !DILocation(line: 132, column: 3, scope: !1147)
!1169 = !DILocation(line: 134, column: 1, scope: !988)
!1170 = distinct !DISubprogram(name: "mystrcpy", linkageName: "_Z8mystrcpyPcS_", scope: !3, file: !3, line: 59, type: !1171, isLocal: false, isDefinition: true, scopeLine: 60, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1173)
!1171 = !DISubroutineType(types: !1172)
!1172 = !{null, !6, !6}
!1173 = !{!1174, !1175, !1176}
!1174 = !DILocalVariable(name: "dst", arg: 1, scope: !1170, file: !3, line: 59, type: !6)
!1175 = !DILocalVariable(name: "src", arg: 2, scope: !1170, file: !3, line: 59, type: !6)
!1176 = !DILocalVariable(name: "cnt", scope: !1170, file: !3, line: 61, type: !8)
!1177 = !DILocation(line: 59, column: 32, scope: !1170)
!1178 = !DILocation(line: 59, column: 43, scope: !1170)
!1179 = !DILocation(line: 61, column: 6, scope: !1170)
!1180 = !DILocation(line: 62, column: 10, scope: !1181)
!1181 = !DILexicalBlockFile(scope: !1170, file: !3, discriminator: 1)
!1182 = !{!1142, !1142, i64 0}
!1183 = !DILocation(line: 62, column: 19, scope: !1181)
!1184 = !DILocation(line: 62, column: 2, scope: !1185)
!1185 = !DILexicalBlockFile(scope: !1170, file: !3, discriminator: 3)
!1186 = !DILocation(line: 64, column: 12, scope: !1187)
!1187 = distinct !DILexicalBlock(scope: !1170, file: !3, line: 63, column: 2)
!1188 = !DILocation(line: 65, column: 6, scope: !1187)
!1189 = !DILocation(line: 62, column: 34, scope: !1190)
!1190 = !DILexicalBlockFile(scope: !1170, file: !3, discriminator: 2)
!1191 = !DILocation(line: 62, column: 27, scope: !1181)
!1192 = distinct !{!1192, !1193, !1194}
!1193 = !DILocation(line: 62, column: 2, scope: !1170)
!1194 = !DILocation(line: 66, column: 2, scope: !1170)
!1195 = !DILocation(line: 67, column: 11, scope: !1170)
!1196 = !DILocation(line: 69, column: 1, scope: !1170)
!1197 = distinct !DISubprogram(name: "mystrcmp", linkageName: "_Z8mystrcmpPcS_", scope: !3, file: !3, line: 71, type: !1198, isLocal: false, isDefinition: true, scopeLine: 72, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1200)
!1198 = !DISubroutineType(types: !1199)
!1199 = !{!39, !6, !6}
!1200 = !{!1201, !1202, !1203}
!1201 = !DILocalVariable(name: "dst", arg: 1, scope: !1197, file: !3, line: 71, type: !6)
!1202 = !DILocalVariable(name: "src", arg: 2, scope: !1197, file: !3, line: 71, type: !6)
!1203 = !DILocalVariable(name: "cnt", scope: !1197, file: !3, line: 73, type: !8)
!1204 = !DILocation(line: 71, column: 32, scope: !1197)
!1205 = !DILocation(line: 71, column: 43, scope: !1197)
!1206 = !DILocation(line: 73, column: 13, scope: !1197)
!1207 = !DILocation(line: 74, column: 9, scope: !1208)
!1208 = !DILexicalBlockFile(scope: !1197, file: !3, discriminator: 1)
!1209 = !DILocation(line: 81, column: 20, scope: !1210)
!1210 = distinct !DILexicalBlock(scope: !1197, file: !3, line: 75, column: 9)
!1211 = !DILocation(line: 76, column: 8, scope: !1212)
!1212 = distinct !DILexicalBlock(scope: !1210, file: !3, line: 76, column: 8)
!1213 = !DILocation(line: 76, column: 28, scope: !1214)
!1214 = !DILexicalBlockFile(scope: !1212, file: !3, discriminator: 1)
!1215 = !DILocation(line: 76, column: 25, scope: !1212)
!1216 = !DILocation(line: 79, column: 30, scope: !1217)
!1217 = distinct !DILexicalBlock(scope: !1210, file: !3, line: 79, column: 21)
!1218 = !DILocation(line: 79, column: 21, scope: !1210)
!1219 = !DILocation(line: 84, column: 1, scope: !1197)
!1220 = !DILocation(line: 74, column: 21, scope: !1208)
!1221 = distinct !{!1221, !1222, !1223}
!1222 = !DILocation(line: 74, column: 9, scope: !1197)
!1223 = !DILocation(line: 82, column: 9, scope: !1197)
!1224 = distinct !DISubprogram(name: "getFuncID", linkageName: "_Z9getFuncIDPc", scope: !3, file: !3, line: 86, type: !1225, isLocal: false, isDefinition: true, scopeLine: 87, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1227)
!1225 = !DISubroutineType(types: !1226)
!1226 = !{!8, !6}
!1227 = !{!1228, !1229, !1231}
!1228 = !DILocalVariable(name: "func", arg: 1, scope: !1224, file: !3, line: 86, type: !6)
!1229 = !DILocalVariable(name: "i", scope: !1230, file: !3, line: 99, type: !8)
!1230 = distinct !DILexicalBlock(scope: !1224, file: !3, line: 99, column: 2)
!1231 = !DILocalVariable(name: "found", scope: !1232, file: !3, line: 101, type: !39)
!1232 = distinct !DILexicalBlock(scope: !1233, file: !3, line: 100, column: 2)
!1233 = distinct !DILexicalBlock(scope: !1230, file: !3, line: 99, column: 2)
!1234 = !DILocation(line: 86, column: 32, scope: !1224)
!1235 = !DILocation(line: 89, column: 6, scope: !1236)
!1236 = distinct !DILexicalBlock(scope: !1224, file: !3, line: 89, column: 6)
!1237 = !{!1238, !1238, i64 0}
!1238 = !{!"int", !1142, i64 0}
!1239 = !DILocation(line: 89, column: 16, scope: !1236)
!1240 = !DILocation(line: 89, column: 6, scope: !1224)
!1241 = !DILocation(line: 99, column: 10, scope: !1230)
!1242 = !DILocation(line: 99, column: 17, scope: !1243)
!1243 = !DILexicalBlockFile(scope: !1233, file: !3, discriminator: 1)
!1244 = !DILocation(line: 99, column: 2, scope: !1245)
!1245 = !DILexicalBlockFile(scope: !1230, file: !3, discriminator: 1)
!1246 = !DILocation(line: 101, column: 26, scope: !1232)
!1247 = !DILocation(line: 59, column: 32, scope: !1170, inlinedAt: !1248)
!1248 = distinct !DILocation(line: 91, column: 3, scope: !1249)
!1249 = distinct !DILexicalBlock(scope: !1236, file: !3, line: 90, column: 2)
!1250 = !DILocation(line: 59, column: 43, scope: !1170, inlinedAt: !1248)
!1251 = !DILocation(line: 61, column: 6, scope: !1170, inlinedAt: !1248)
!1252 = !DILocation(line: 62, column: 10, scope: !1181, inlinedAt: !1248)
!1253 = !DILocation(line: 62, column: 19, scope: !1181, inlinedAt: !1248)
!1254 = !DILocation(line: 62, column: 2, scope: !1185, inlinedAt: !1248)
!1255 = !DILocation(line: 64, column: 12, scope: !1187, inlinedAt: !1248)
!1256 = !DILocation(line: 65, column: 6, scope: !1187, inlinedAt: !1248)
!1257 = !DILocation(line: 62, column: 34, scope: !1190, inlinedAt: !1248)
!1258 = !DILocation(line: 62, column: 27, scope: !1181, inlinedAt: !1248)
!1259 = !DILocation(line: 71, column: 43, scope: !1197, inlinedAt: !1260)
!1260 = distinct !DILocation(line: 101, column: 16, scope: !1232)
!1261 = !DILocation(line: 73, column: 13, scope: !1197, inlinedAt: !1260)
!1262 = !DILocation(line: 74, column: 9, scope: !1208, inlinedAt: !1260)
!1263 = !DILocation(line: 81, column: 20, scope: !1210, inlinedAt: !1260)
!1264 = !DILocation(line: 76, column: 8, scope: !1212, inlinedAt: !1260)
!1265 = !DILocation(line: 76, column: 28, scope: !1214, inlinedAt: !1260)
!1266 = !DILocation(line: 76, column: 25, scope: !1212, inlinedAt: !1260)
!1267 = !DILocation(line: 79, column: 30, scope: !1217, inlinedAt: !1260)
!1268 = !DILocation(line: 79, column: 21, scope: !1210, inlinedAt: !1260)
!1269 = !DILocation(line: 99, column: 31, scope: !1270)
!1270 = !DILexicalBlockFile(scope: !1233, file: !3, discriminator: 3)
!1271 = distinct !{!1271, !1272, !1273}
!1272 = !DILocation(line: 99, column: 2, scope: !1230)
!1273 = !DILocation(line: 105, column: 2, scope: !1230)
!1274 = !DILocation(line: 109, column: 11, scope: !1224)
!1275 = !DILocation(line: 59, column: 32, scope: !1170, inlinedAt: !1276)
!1276 = distinct !DILocation(line: 109, column: 2, scope: !1224)
!1277 = !DILocation(line: 59, column: 43, scope: !1170, inlinedAt: !1276)
!1278 = !DILocation(line: 61, column: 6, scope: !1170, inlinedAt: !1276)
!1279 = !DILocation(line: 62, column: 10, scope: !1181, inlinedAt: !1276)
!1280 = !DILocation(line: 62, column: 19, scope: !1181, inlinedAt: !1276)
!1281 = !DILocation(line: 62, column: 2, scope: !1185, inlinedAt: !1276)
!1282 = !DILocation(line: 64, column: 12, scope: !1187, inlinedAt: !1276)
!1283 = !DILocation(line: 65, column: 6, scope: !1187, inlinedAt: !1276)
!1284 = !DILocation(line: 62, column: 34, scope: !1190, inlinedAt: !1276)
!1285 = !DILocation(line: 62, column: 27, scope: !1181, inlinedAt: !1276)
!1286 = !DILocation(line: 67, column: 11, scope: !1170, inlinedAt: !1248)
!1287 = !DILocation(line: 113, column: 1, scope: !1224)
!1288 = !DILocation(line: 74, column: 21, scope: !1208, inlinedAt: !1260)
!1289 = distinct !DISubprogram(name: "updateCallStack", linkageName: "_Z15updateCallStackiissi", scope: !3, file: !3, line: 115, type: !1290, isLocal: false, isDefinition: true, scopeLine: 116, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1292)
!1290 = !DISubroutineType(types: !1291)
!1291 = !{null, !8, !8, !15, !15, !8}
!1292 = !{!1293, !1294, !1295, !1296, !1297, !1298, !1299, !1301, !1302, !1303}
!1293 = !DILocalVariable(name: "caller", arg: 1, scope: !1289, file: !3, line: 115, type: !8)
!1294 = !DILocalVariable(name: "callee", arg: 2, scope: !1289, file: !3, line: 115, type: !8)
!1295 = !DILocalVariable(name: "sline", arg: 3, scope: !1289, file: !3, line: 115, type: !15)
!1296 = !DILocalVariable(name: "scolm", arg: 4, scope: !1289, file: !3, line: 115, type: !15)
!1297 = !DILocalVariable(name: "tid", arg: 5, scope: !1289, file: !3, line: 115, type: !8)
!1298 = !DILocalVariable(name: "callStack", scope: !1289, file: !3, line: 118, type: !59)
!1299 = !DILocalVariable(name: "height", scope: !1289, file: !3, line: 119, type: !1300)
!1300 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !8, size: 64)
!1301 = !DILocalVariable(name: "p_caller", scope: !1289, file: !3, line: 137, type: !8)
!1302 = !DILocalVariable(name: "p_callee", scope: !1289, file: !3, line: 138, type: !8)
!1303 = !DILocalVariable(name: "i", scope: !1304, file: !3, line: 167, type: !8)
!1304 = distinct !DILexicalBlock(scope: !1289, file: !3, line: 167, column: 9)
!1305 = !DILocation(line: 115, column: 37, scope: !1289)
!1306 = !DILocation(line: 115, column: 49, scope: !1289)
!1307 = !DILocation(line: 115, column: 63, scope: !1289)
!1308 = !DILocation(line: 115, column: 76, scope: !1289)
!1309 = !DILocation(line: 115, column: 87, scope: !1289)
!1310 = !DILocation(line: 118, column: 26, scope: !1289)
!1311 = !{!1312, !1312, i64 0}
!1312 = !{!"any pointer", !1142, i64 0}
!1313 = !DILocation(line: 118, column: 14, scope: !1289)
!1314 = !DILocation(line: 119, column: 16, scope: !1289)
!1315 = !DILocation(line: 119, column: 7, scope: !1289)
!1316 = !DILocation(line: 121, column: 9, scope: !1289)
!1317 = !DILocation(line: 121, column: 9, scope: !1318)
!1318 = !DILexicalBlockFile(scope: !1289, file: !3, discriminator: 2)
!1319 = !DILocation(line: 125, column: 3, scope: !1320)
!1320 = distinct !DILexicalBlock(scope: !1321, file: !3, line: 124, column: 9)
!1321 = distinct !DILexicalBlock(scope: !1289, file: !3, line: 123, column: 13)
!1322 = !DILocation(line: 126, column: 16, scope: !1320)
!1323 = !DILocation(line: 126, column: 19, scope: !1320)
!1324 = !{!1325, !1238, i64 0}
!1325 = !{!"_ZTS10CallSite_t", !1238, i64 0, !1326, i64 4, !1326, i64 6}
!1326 = !{!"short", !1142, i64 0}
!1327 = !DILocation(line: 127, column: 16, scope: !1320)
!1328 = !DILocation(line: 127, column: 22, scope: !1320)
!1329 = !{!1325, !1326, i64 4}
!1330 = !DILocation(line: 128, column: 16, scope: !1320)
!1331 = !DILocation(line: 128, column: 22, scope: !1320)
!1332 = !{!1325, !1326, i64 6}
!1333 = !DILocation(line: 130, column: 16, scope: !1320)
!1334 = !DILocation(line: 130, column: 19, scope: !1320)
!1335 = !DILocation(line: 131, column: 30, scope: !1320)
!1336 = !DILocation(line: 131, column: 36, scope: !1320)
!1337 = !DILocation(line: 132, column: 30, scope: !1320)
!1338 = !DILocation(line: 132, column: 36, scope: !1320)
!1339 = !DILocation(line: 133, column: 9, scope: !1320)
!1340 = !DILocation(line: 134, column: 17, scope: !1320)
!1341 = !DILocation(line: 137, column: 40, scope: !1289)
!1342 = !DILocation(line: 137, column: 24, scope: !1289)
!1343 = !DILocation(line: 137, column: 44, scope: !1289)
!1344 = !DILocation(line: 137, column: 13, scope: !1289)
!1345 = !DILocation(line: 138, column: 40, scope: !1289)
!1346 = !DILocation(line: 138, column: 24, scope: !1289)
!1347 = !DILocation(line: 138, column: 44, scope: !1289)
!1348 = !DILocation(line: 138, column: 13, scope: !1289)
!1349 = !DILocation(line: 140, column: 23, scope: !1350)
!1350 = distinct !DILexicalBlock(scope: !1289, file: !3, line: 140, column: 14)
!1351 = !DILocation(line: 140, column: 45, scope: !1352)
!1352 = !DILexicalBlockFile(scope: !1350, file: !3, discriminator: 1)
!1353 = !DILocation(line: 140, column: 33, scope: !1350)
!1354 = !DILocation(line: 142, column: 37, scope: !1355)
!1355 = distinct !DILexicalBlock(scope: !1350, file: !3, line: 141, column: 9)
!1356 = !DILocation(line: 142, column: 43, scope: !1355)
!1357 = !DILocation(line: 143, column: 37, scope: !1355)
!1358 = !DILocation(line: 143, column: 43, scope: !1355)
!1359 = !DILocation(line: 144, column: 17, scope: !1355)
!1360 = !DILocation(line: 146, column: 38, scope: !1361)
!1361 = distinct !DILexicalBlock(scope: !1350, file: !3, line: 146, column: 19)
!1362 = !DILocation(line: 148, column: 10, scope: !1363)
!1363 = distinct !DILexicalBlock(scope: !1361, file: !3, line: 147, column: 9)
!1364 = !DILocation(line: 149, column: 27, scope: !1363)
!1365 = !DILocation(line: 149, column: 33, scope: !1363)
!1366 = !DILocation(line: 149, column: 17, scope: !1363)
!1367 = !DILocation(line: 149, column: 37, scope: !1363)
!1368 = !DILocation(line: 149, column: 40, scope: !1363)
!1369 = !DILocation(line: 150, column: 27, scope: !1363)
!1370 = !DILocation(line: 150, column: 33, scope: !1363)
!1371 = !DILocation(line: 150, column: 17, scope: !1363)
!1372 = !DILocation(line: 150, column: 37, scope: !1363)
!1373 = !DILocation(line: 150, column: 43, scope: !1363)
!1374 = !DILocation(line: 151, column: 37, scope: !1363)
!1375 = !DILocation(line: 151, column: 43, scope: !1363)
!1376 = !DILocation(line: 152, column: 17, scope: !1363)
!1377 = !DILocation(line: 154, column: 28, scope: !1378)
!1378 = distinct !DILexicalBlock(scope: !1361, file: !3, line: 154, column: 19)
!1379 = !DILocation(line: 154, column: 19, scope: !1361)
!1380 = !DILocation(line: 156, column: 3, scope: !1381)
!1381 = distinct !DILexicalBlock(scope: !1378, file: !3, line: 155, column: 9)
!1382 = !DILocation(line: 157, column: 27, scope: !1381)
!1383 = !DILocation(line: 157, column: 33, scope: !1381)
!1384 = !DILocation(line: 157, column: 17, scope: !1381)
!1385 = !DILocation(line: 157, column: 37, scope: !1381)
!1386 = !DILocation(line: 157, column: 43, scope: !1381)
!1387 = !DILocation(line: 158, column: 37, scope: !1381)
!1388 = !DILocation(line: 158, column: 43, scope: !1381)
!1389 = !DILocation(line: 160, column: 17, scope: !1381)
!1390 = !DILocation(line: 160, column: 35, scope: !1381)
!1391 = !DILocation(line: 160, column: 38, scope: !1381)
!1392 = !DILocation(line: 161, column: 27, scope: !1381)
!1393 = !DILocation(line: 161, column: 17, scope: !1381)
!1394 = !DILocation(line: 161, column: 35, scope: !1381)
!1395 = !DILocation(line: 161, column: 41, scope: !1381)
!1396 = !DILocation(line: 162, column: 35, scope: !1381)
!1397 = !DILocation(line: 162, column: 41, scope: !1381)
!1398 = !DILocation(line: 163, column: 9, scope: !1381)
!1399 = !DILocation(line: 164, column: 17, scope: !1381)
!1400 = !DILocation(line: 167, column: 18, scope: !1304)
!1401 = !DILocation(line: 167, column: 31, scope: !1402)
!1402 = !DILexicalBlockFile(scope: !1403, file: !3, discriminator: 1)
!1403 = distinct !DILexicalBlock(scope: !1304, file: !3, line: 167, column: 9)
!1404 = !DILocation(line: 167, column: 9, scope: !1405)
!1405 = !DILexicalBlockFile(scope: !1304, file: !3, discriminator: 1)
!1406 = !DILocation(line: 169, column: 22, scope: !1407)
!1407 = distinct !DILexicalBlock(scope: !1408, file: !3, line: 169, column: 22)
!1408 = distinct !DILexicalBlock(scope: !1403, file: !3, line: 168, column: 9)
!1409 = !DILocation(line: 169, column: 35, scope: !1407)
!1410 = !DILocation(line: 169, column: 38, scope: !1407)
!1411 = !DILocation(line: 169, column: 22, scope: !1408)
!1412 = distinct !{!1412, !1413, !1414}
!1413 = !DILocation(line: 167, column: 9, scope: !1304)
!1414 = !DILocation(line: 180, column: 9, scope: !1304)
!1415 = !DILocation(line: 171, column: 11, scope: !1416)
!1416 = distinct !DILexicalBlock(scope: !1407, file: !3, line: 170, column: 17)
!1417 = !DILocation(line: 172, column: 41, scope: !1416)
!1418 = !DILocation(line: 173, column: 38, scope: !1416)
!1419 = !DILocation(line: 174, column: 38, scope: !1416)
!1420 = !DILocation(line: 176, column: 44, scope: !1416)
!1421 = !DILocation(line: 177, column: 44, scope: !1416)
!1422 = !DILocation(line: 183, column: 9, scope: !1289)
!1423 = !DILocation(line: 184, column: 1, scope: !1424)
!1424 = !DILexicalBlockFile(scope: !1289, file: !3, discriminator: 3)
!1425 = distinct !DISubprogram(name: "__assert_fail", linkageName: "_ZL13__assert_failPKcS0_jS0_", scope: !1426, file: !1426, line: 281, type: !1427, isLocal: true, isDefinition: true, scopeLine: 283, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1429)
!1426 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_runtime_wrapper.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!1427 = !DISubroutineType(types: !1428)
!1428 = !{null, !221, !221, !436, !221}
!1429 = !{!1430, !1431, !1432, !1433}
!1430 = !DILocalVariable(name: "__message", arg: 1, scope: !1425, file: !1426, line: 281, type: !221)
!1431 = !DILocalVariable(name: "__file", arg: 2, scope: !1425, file: !1426, line: 282, type: !221)
!1432 = !DILocalVariable(name: "__line", arg: 3, scope: !1425, file: !1426, line: 282, type: !436)
!1433 = !DILocalVariable(name: "__function", arg: 4, scope: !1425, file: !1426, line: 283, type: !221)
!1434 = !DILocation(line: 284, column: 3, scope: !1425)
!1435 = distinct !DISubprogram(name: "printCallStack", linkageName: "_Z14printCallStacki", scope: !3, file: !3, line: 187, type: !384, isLocal: false, isDefinition: true, scopeLine: 188, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1436)
!1436 = !{!1437, !1438, !1439, !1440}
!1437 = !DILocalVariable(name: "tid", arg: 1, scope: !1435, file: !3, line: 187, type: !8)
!1438 = !DILocalVariable(name: "callStack", scope: !1435, file: !3, line: 189, type: !59)
!1439 = !DILocalVariable(name: "height", scope: !1435, file: !3, line: 190, type: !8)
!1440 = !DILocalVariable(name: "i", scope: !1441, file: !3, line: 197, type: !8)
!1441 = distinct !DILexicalBlock(scope: !1435, file: !3, line: 197, column: 9)
!1442 = !DILocation(line: 187, column: 36, scope: !1435)
!1443 = !DILocation(line: 189, column: 26, scope: !1435)
!1444 = !DILocation(line: 189, column: 14, scope: !1435)
!1445 = !DILocation(line: 190, column: 22, scope: !1435)
!1446 = !DILocation(line: 190, column: 13, scope: !1435)
!1447 = !DILocation(line: 191, column: 2, scope: !1435)
!1448 = !DILocation(line: 194, column: 19, scope: !1449)
!1449 = distinct !DILexicalBlock(scope: !1435, file: !3, line: 194, column: 13)
!1450 = !DILocation(line: 197, column: 18, scope: !1441)
!1451 = !DILocation(line: 194, column: 13, scope: !1435)
!1452 = !DILocation(line: 197, column: 9, scope: !1453)
!1453 = !DILexicalBlockFile(scope: !1441, file: !3, discriminator: 1)
!1454 = !DILocation(line: 198, column: 74, scope: !1455)
!1455 = distinct !DILexicalBlock(scope: !1441, file: !3, line: 197, column: 9)
!1456 = !DILocation(line: 198, column: 92, scope: !1455)
!1457 = !DILocation(line: 198, column: 79, scope: !1455)
!1458 = !DILocation(line: 198, column: 113, scope: !1455)
!1459 = !DILocation(line: 198, column: 100, scope: !1455)
!1460 = !DILocation(line: 198, column: 17, scope: !1455)
!1461 = !DILocation(line: 198, column: 61, scope: !1455)
!1462 = !DILocation(line: 197, column: 34, scope: !1463)
!1463 = !DILexicalBlockFile(scope: !1455, file: !3, discriminator: 3)
!1464 = !DILocation(line: 197, column: 24, scope: !1465)
!1465 = !DILexicalBlockFile(scope: !1455, file: !3, discriminator: 1)
!1466 = distinct !{!1466, !1467, !1468}
!1467 = !DILocation(line: 197, column: 9, scope: !1441)
!1468 = !DILocation(line: 198, column: 119, scope: !1441)
!1469 = !DILocation(line: 199, column: 1, scope: !1470)
!1470 = !DILexicalBlockFile(scope: !1435, file: !3, discriminator: 2)
!1471 = distinct !DISubprogram(name: "holdon", linkageName: "_Z6holdonl", scope: !3, file: !3, line: 201, type: !1472, isLocal: false, isDefinition: true, scopeLine: 202, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1477)
!1472 = !DISubroutineType(types: !1473)
!1473 = !{!8, !1474}
!1474 = !DIDerivedType(tag: DW_TAG_typedef, name: "clock_t", file: !1475, line: 59, baseType: !1476)
!1475 = !DIFile(filename: "/usr/include/time.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!1476 = !DIDerivedType(tag: DW_TAG_typedef, name: "__clock_t", file: !925, line: 135, baseType: !26)
!1477 = !{!1478, !1479, !1480}
!1478 = !DILocalVariable(name: "c", arg: 1, scope: !1471, file: !3, line: 201, type: !1474)
!1479 = !DILocalVariable(name: "start_clock", scope: !1471, file: !3, line: 203, type: !1474)
!1480 = !DILocalVariable(name: "clock_offset", scope: !1471, file: !3, line: 204, type: !1474)
!1481 = !DILocation(line: 201, column: 31, scope: !1471)
!1482 = !DILocation(line: 344, column: 3, scope: !1483, inlinedAt: !1486)
!1483 = distinct !DISubprogram(name: "clock", linkageName: "_ZL5clockv", scope: !533, file: !533, line: 340, type: !426, isLocal: true, isDefinition: true, scopeLine: 342, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1484)
!1484 = !{!1485}
!1485 = !DILocalVariable(name: "r", scope: !1483, file: !533, line: 343, type: !8)
!1486 = distinct !DILocation(line: 203, column: 24, scope: !1471)
!1487 = !{i32 2280604}
!1488 = !DILocation(line: 343, column: 7, scope: !1483, inlinedAt: !1486)
!1489 = !DILocation(line: 203, column: 24, scope: !1471)
!1490 = !DILocation(line: 203, column: 10, scope: !1471)
!1491 = !DILocation(line: 204, column: 14, scope: !1471)
!1492 = !DILocation(line: 205, column: 23, scope: !1493)
!1493 = !DILexicalBlockFile(scope: !1471, file: !3, discriminator: 1)
!1494 = !DILocation(line: 205, column: 3, scope: !1493)
!1495 = !DILocation(line: 344, column: 3, scope: !1483, inlinedAt: !1496)
!1496 = distinct !DILocation(line: 206, column: 25, scope: !1471)
!1497 = !DILocation(line: 343, column: 7, scope: !1483, inlinedAt: !1496)
!1498 = !DILocation(line: 206, column: 25, scope: !1471)
!1499 = !DILocation(line: 206, column: 33, scope: !1471)
!1500 = distinct !{!1500, !1501, !1502}
!1501 = !DILocation(line: 205, column: 3, scope: !1471)
!1502 = !DILocation(line: 206, column: 35, scope: !1471)
!1503 = !DILocation(line: 208, column: 9, scope: !1471)
!1504 = !DILocation(line: 208, column: 2, scope: !1471)
!1505 = distinct !DISubprogram(name: "InitKernel", scope: !3, file: !3, line: 212, type: !337, isLocal: false, isDefinition: true, scopeLine: 213, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !4)
!1506 = !DILocation(line: 214, column: 2, scope: !1505)
!1507 = distinct !DISubprogram(name: "callFunc", scope: !3, file: !3, line: 291, type: !1508, isLocal: false, isDefinition: true, scopeLine: 292, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1510)
!1508 = !DISubroutineType(types: !1509)
!1509 = !{null, !364, !364, !8, !8}
!1510 = !{!1511, !1512, !1513, !1514}
!1511 = !DILocalVariable(name: "er", arg: 1, scope: !1507, file: !3, line: 291, type: !364)
!1512 = !DILocalVariable(name: "ee", arg: 2, scope: !1507, file: !3, line: 291, type: !364)
!1513 = !DILocalVariable(name: "sline", arg: 3, scope: !1507, file: !3, line: 291, type: !8)
!1514 = !DILocalVariable(name: "scolm", arg: 4, scope: !1507, file: !3, line: 291, type: !8)
!1515 = !DILocation(line: 291, column: 32, scope: !1507)
!1516 = !DILocation(line: 291, column: 42, scope: !1507)
!1517 = !DILocation(line: 291, column: 50, scope: !1507)
!1518 = !DILocation(line: 291, column: 61, scope: !1507)
!1519 = !DILocation(line: 293, column: 2, scope: !1507)
!1520 = distinct !DISubprogram(name: "takeString", scope: !3, file: !3, line: 318, type: !1521, isLocal: false, isDefinition: true, scopeLine: 319, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1523)
!1521 = !DISubroutineType(types: !1522)
!1522 = !{null, !364, !8}
!1523 = !{!1524, !1525}
!1524 = !DILocalVariable(name: "p", arg: 1, scope: !1520, file: !3, line: 318, type: !364)
!1525 = !DILocalVariable(name: "action", arg: 2, scope: !1520, file: !3, line: 318, type: !8)
!1526 = !DILocation(line: 318, column: 34, scope: !1520)
!1527 = !DILocation(line: 318, column: 41, scope: !1520)
!1528 = !DILocation(line: 67, column: 3, scope: !1529, inlinedAt: !1530)
!1529 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !1093, file: !1008, line: 67, type: !1012, isLocal: false, isDefinition: true, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1095, variables: !4)
!1530 = distinct !DILocation(line: 320, column: 6, scope: !1531)
!1531 = distinct !DILexicalBlock(scope: !1520, file: !3, line: 320, column: 6)
!1532 = !DILocation(line: 320, column: 18, scope: !1531)
!1533 = !DILocation(line: 320, column: 23, scope: !1531)
!1534 = !DILocation(line: 78, column: 3, scope: !1535, inlinedAt: !1536)
!1535 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !1009, file: !1008, line: 78, type: !1012, isLocal: false, isDefinition: true, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1011, variables: !4)
!1536 = distinct !DILocation(line: 320, column: 26, scope: !1537)
!1537 = !DILexicalBlockFile(scope: !1531, file: !3, discriminator: 1)
!1538 = !DILocation(line: 320, column: 37, scope: !1537)
!1539 = !DILocation(line: 320, column: 42, scope: !1537)
!1540 = !DILocation(line: 68, column: 3, scope: !1541, inlinedAt: !1542)
!1541 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_yEv", scope: !1093, file: !1008, line: 68, type: !1012, isLocal: false, isDefinition: true, scopeLine: 68, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1096, variables: !4)
!1542 = distinct !DILocation(line: 320, column: 45, scope: !1543)
!1543 = !DILexicalBlockFile(scope: !1531, file: !3, discriminator: 2)
!1544 = !DILocation(line: 320, column: 57, scope: !1543)
!1545 = !DILocation(line: 320, column: 62, scope: !1543)
!1546 = !DILocation(line: 79, column: 3, scope: !1547, inlinedAt: !1548)
!1547 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_yEv", scope: !1009, file: !1008, line: 79, type: !1012, isLocal: false, isDefinition: true, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1014, variables: !4)
!1548 = distinct !DILocation(line: 320, column: 65, scope: !1549)
!1549 = !DILexicalBlockFile(scope: !1531, file: !3, discriminator: 3)
!1550 = !DILocation(line: 320, column: 76, scope: !1549)
!1551 = !DILocation(line: 320, column: 6, scope: !1552)
!1552 = !DILexicalBlockFile(scope: !1520, file: !3, discriminator: 3)
!1553 = !DILocation(line: 322, column: 6, scope: !1554)
!1554 = distinct !DILexicalBlock(scope: !1520, file: !3, line: 322, column: 6)
!1555 = !{!1556, !1556, i64 0}
!1556 = !{!"bool", !1142, i64 0}
!1557 = !{i8 0, i8 2}
!1558 = !DILocation(line: 322, column: 6, scope: !1520)
!1559 = !DILocation(line: 324, column: 6, scope: !1560)
!1560 = distinct !DILexicalBlock(scope: !1554, file: !3, line: 323, column: 2)
!1561 = !DILocation(line: 325, column: 3, scope: !1562)
!1562 = distinct !DILexicalBlock(scope: !1560, file: !3, line: 324, column: 6)
!1563 = !DILocation(line: 327, column: 3, scope: !1564)
!1564 = distinct !DILexicalBlock(scope: !1562, file: !3, line: 326, column: 11)
!1565 = !DILocation(line: 329, column: 3, scope: !1566)
!1566 = distinct !DILexicalBlock(scope: !1564, file: !3, line: 328, column: 11)
!1567 = !DILocation(line: 331, column: 3, scope: !1566)
!1568 = !DILocation(line: 334, column: 1, scope: !1520)
!1569 = distinct !DISubprogram(name: "cxtprint", linkageName: "_Z8cxtprinti", scope: !3, file: !3, line: 336, type: !384, isLocal: false, isDefinition: true, scopeLine: 337, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1570)
!1570 = !{!1571, !1572}
!1571 = !DILocalVariable(name: "id", arg: 1, scope: !1569, file: !3, line: 336, type: !8)
!1572 = !DILocalVariable(name: "i", scope: !1573, file: !3, line: 344, type: !8)
!1573 = distinct !DILexicalBlock(scope: !1569, file: !3, line: 344, column: 2)
!1574 = !DILocation(line: 336, column: 30, scope: !1569)
!1575 = !DILocation(line: 338, column: 2, scope: !1569)
!1576 = !DILocation(line: 338, column: 2, scope: !1577)
!1577 = !DILexicalBlockFile(scope: !1569, file: !3, discriminator: 2)
!1578 = !DILocation(line: 339, column: 8, scope: !1579)
!1579 = distinct !DILexicalBlock(scope: !1569, file: !3, line: 339, column: 6)
!1580 = !DILocation(line: 339, column: 6, scope: !1569)
!1581 = !DILocation(line: 342, column: 2, scope: !1569)
!1582 = !DILocation(line: 344, column: 11, scope: !1573)
!1583 = !DILocation(line: 344, column: 46, scope: !1584)
!1584 = !DILexicalBlockFile(scope: !1585, file: !3, discriminator: 2)
!1585 = distinct !DILexicalBlock(scope: !1573, file: !3, line: 344, column: 2)
!1586 = !DILocation(line: 344, column: 43, scope: !1587)
!1587 = !DILexicalBlockFile(scope: !1585, file: !3, discriminator: 1)
!1588 = !DILocation(line: 344, column: 64, scope: !1584)
!1589 = !DILocation(line: 344, column: 67, scope: !1584)
!1590 = !DILocation(line: 344, column: 2, scope: !1591)
!1591 = !DILexicalBlockFile(scope: !1573, file: !3, discriminator: 3)
!1592 = !DILocation(line: 346, column: 108, scope: !1593)
!1593 = distinct !DILexicalBlock(scope: !1585, file: !3, line: 345, column: 2)
!1594 = !DILocation(line: 346, column: 90, scope: !1593)
!1595 = !DILocation(line: 346, column: 133, scope: !1593)
!1596 = !DILocation(line: 346, column: 115, scope: !1593)
!1597 = !DILocation(line: 346, column: 3, scope: !1593)
!1598 = !DILocation(line: 344, column: 77, scope: !1599)
!1599 = !DILexicalBlockFile(scope: !1585, file: !3, discriminator: 5)
!1600 = !DILocation(line: 344, column: 19, scope: !1587)
!1601 = distinct !{!1601, !1602, !1603}
!1602 = !DILocation(line: 344, column: 2, scope: !1573)
!1603 = !DILocation(line: 347, column: 2, scope: !1573)
!1604 = !DILocation(line: 350, column: 1, scope: !1569)
!1605 = distinct !DISubprogram(name: "cxtcpy", linkageName: "_Z6cxtcpyP10CallSite_tS0_i", scope: !3, file: !3, line: 353, type: !1606, isLocal: false, isDefinition: true, scopeLine: 354, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1608)
!1606 = !DISubroutineType(types: !1607)
!1607 = !{null, !59, !59, !8}
!1608 = !{!1609, !1610, !1611, !1612}
!1609 = !DILocalVariable(name: "dst", arg: 1, scope: !1605, file: !3, line: 353, type: !59)
!1610 = !DILocalVariable(name: "src", arg: 2, scope: !1605, file: !3, line: 353, type: !59)
!1611 = !DILocalVariable(name: "height", arg: 3, scope: !1605, file: !3, line: 353, type: !8)
!1612 = !DILocalVariable(name: "i", scope: !1605, file: !3, line: 355, type: !8)
!1613 = !DILocation(line: 353, column: 37, scope: !1605)
!1614 = !DILocation(line: 353, column: 54, scope: !1605)
!1615 = !DILocation(line: 353, column: 64, scope: !1605)
!1616 = !DILocation(line: 355, column: 6, scope: !1605)
!1617 = !DILocation(line: 356, column: 13, scope: !1618)
!1618 = !DILexicalBlockFile(scope: !1619, file: !3, discriminator: 1)
!1619 = distinct !DILexicalBlock(scope: !1620, file: !3, line: 356, column: 2)
!1620 = distinct !DILexicalBlock(scope: !1605, file: !3, line: 356, column: 2)
!1621 = !DILocation(line: 356, column: 2, scope: !1622)
!1622 = !DILexicalBlockFile(scope: !1620, file: !3, discriminator: 1)
!1623 = !DILocation(line: 357, column: 12, scope: !1619)
!1624 = !DILocation(line: 357, column: 3, scope: !1619)
!1625 = !DILocation(line: 357, column: 10, scope: !1619)
!1626 = !DILocation(line: 356, column: 24, scope: !1627)
!1627 = !DILexicalBlockFile(scope: !1619, file: !3, discriminator: 2)
!1628 = distinct !{!1628, !1629}
!1629 = !{!"llvm.loop.unroll.disable"}
!1630 = distinct !{!1630, !1631, !1632}
!1631 = !DILocation(line: 356, column: 2, scope: !1620)
!1632 = !DILocation(line: 357, column: 17, scope: !1620)
!1633 = !DILocation(line: 359, column: 2, scope: !1605)
!1634 = !DILocation(line: 359, column: 2, scope: !1635)
!1635 = !DILexicalBlockFile(scope: !1605, file: !3, discriminator: 2)
!1636 = !DILocation(line: 361, column: 2, scope: !1605)
!1637 = !DILocation(line: 361, column: 9, scope: !1605)
!1638 = !DILocation(line: 361, column: 12, scope: !1605)
!1639 = !DILocation(line: 365, column: 1, scope: !1605)
!1640 = distinct !DISubprogram(name: "cxtcmp", linkageName: "_Z6cxtcmpP10CallSite_tS0_i", scope: !3, file: !3, line: 368, type: !1641, isLocal: false, isDefinition: true, scopeLine: 369, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1643)
!1641 = !DISubroutineType(types: !1642)
!1642 = !{!39, !59, !59, !8}
!1643 = !{!1644, !1645, !1646, !1647}
!1644 = !DILocalVariable(name: "dst", arg: 1, scope: !1640, file: !3, line: 368, type: !59)
!1645 = !DILocalVariable(name: "src", arg: 2, scope: !1640, file: !3, line: 368, type: !59)
!1646 = !DILocalVariable(name: "height", arg: 3, scope: !1640, file: !3, line: 368, type: !8)
!1647 = !DILocalVariable(name: "i", scope: !1648, file: !3, line: 370, type: !8)
!1648 = distinct !DILexicalBlock(scope: !1640, file: !3, line: 370, column: 2)
!1649 = !DILocation(line: 368, column: 37, scope: !1640)
!1650 = !DILocation(line: 368, column: 54, scope: !1640)
!1651 = !DILocation(line: 368, column: 63, scope: !1640)
!1652 = !DILocation(line: 370, column: 11, scope: !1648)
!1653 = !DILocation(line: 370, column: 17, scope: !1654)
!1654 = !DILexicalBlockFile(scope: !1655, file: !3, discriminator: 1)
!1655 = distinct !DILexicalBlock(scope: !1648, file: !3, line: 370, column: 2)
!1656 = !DILocation(line: 370, column: 2, scope: !1657)
!1657 = !DILexicalBlockFile(scope: !1648, file: !3, discriminator: 1)
!1658 = !DILocation(line: 371, column: 8, scope: !1659)
!1659 = distinct !DILexicalBlock(scope: !1655, file: !3, line: 371, column: 8)
!1660 = distinct !{!1660, !1661, !1662}
!1661 = !DILocation(line: 370, column: 2, scope: !1648)
!1662 = !DILocation(line: 374, column: 11, scope: !1648)
!1663 = !DILocation(line: 371, column: 15, scope: !1659)
!1664 = !DILocation(line: 371, column: 28, scope: !1659)
!1665 = !DILocation(line: 371, column: 18, scope: !1659)
!1666 = !DILocation(line: 370, column: 28, scope: !1667)
!1667 = !DILexicalBlockFile(scope: !1655, file: !3, discriminator: 3)
!1668 = !DILocation(line: 371, column: 8, scope: !1655)
!1669 = !DILocation(line: 377, column: 1, scope: !1640)
!1670 = distinct !DISubprogram(name: "getContextID", scope: !3, file: !3, line: 380, type: !426, isLocal: false, isDefinition: true, scopeLine: 381, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !4)
!1671 = !DILocation(line: 384, column: 2, scope: !1670)
!1672 = distinct !DISubprogram(name: "passBasicBlock", scope: !3, file: !3, line: 434, type: !1673, isLocal: false, isDefinition: true, scopeLine: 435, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1675)
!1673 = !DISubroutineType(types: !1674)
!1674 = !{null, !364, !8, !8, !8}
!1675 = !{!1676, !1677, !1678, !1679, !1680, !1681, !1682, !1683, !1684, !1685, !1687, !1690}
!1676 = !DILocalVariable(name: "p", arg: 1, scope: !1672, file: !3, line: 434, type: !364)
!1677 = !DILocalVariable(name: "action", arg: 2, scope: !1672, file: !3, line: 434, type: !8)
!1678 = !DILocalVariable(name: "sline", arg: 3, scope: !1672, file: !3, line: 434, type: !8)
!1679 = !DILocalVariable(name: "scolm", arg: 4, scope: !1672, file: !3, line: 434, type: !8)
!1680 = !DILocalVariable(name: "str", scope: !1672, file: !3, line: 446, type: !6)
!1681 = !DILocalVariable(name: "bid", scope: !1672, file: !3, line: 448, type: !8)
!1682 = !DILocalVariable(name: "key", scope: !1672, file: !3, line: 459, type: !20)
!1683 = !DILocalVariable(name: "cnt", scope: !1672, file: !3, line: 460, type: !8)
!1684 = !DILocalVariable(name: "factor", scope: !1672, file: !3, line: 461, type: !29)
!1685 = !DILocalVariable(name: "i", scope: !1686, file: !3, line: 462, type: !8)
!1686 = distinct !DILexicalBlock(scope: !1672, file: !3, line: 462, column: 9)
!1687 = !DILocalVariable(name: "ascii", scope: !1688, file: !3, line: 464, type: !8)
!1688 = distinct !DILexicalBlock(scope: !1689, file: !3, line: 463, column: 2)
!1689 = distinct !DILexicalBlock(scope: !1686, file: !3, line: 462, column: 9)
!1690 = !DILocalVariable(name: "bblog", scope: !1672, file: !3, line: 475, type: !9)
!1691 = !DILocation(line: 434, column: 38, scope: !1672)
!1692 = !DILocation(line: 434, column: 45, scope: !1672)
!1693 = !DILocation(line: 434, column: 57, scope: !1672)
!1694 = !DILocation(line: 434, column: 68, scope: !1672)
!1695 = !DILocation(line: 437, column: 2, scope: !1672)
!1696 = !{!1697, !1697, i64 0}
!1697 = !{!"long long", !1142, i64 0}
!1698 = !DILocation(line: 437, column: 2, scope: !1699)
!1699 = !DILexicalBlockFile(scope: !1672, file: !3, discriminator: 2)
!1700 = !DILocation(line: 78, column: 3, scope: !1535, inlinedAt: !1701)
!1701 = distinct !DILocation(line: 440, column: 8, scope: !1702)
!1702 = distinct !DILexicalBlock(scope: !1672, file: !3, line: 440, column: 7)
!1703 = !DILocation(line: 79, column: 3, scope: !1547, inlinedAt: !1704)
!1704 = distinct !DILocation(line: 440, column: 21, scope: !1705)
!1705 = !DILexicalBlockFile(scope: !1702, file: !3, discriminator: 2)
!1706 = !DILocation(line: 100, column: 3, scope: !1707, inlinedAt: !1733)
!1707 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_xEv", scope: !1708, file: !1008, line: 100, type: !1012, isLocal: false, isDefinition: true, scopeLine: 100, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1710, variables: !4)
!1708 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_gridDim_t", file: !1008, line: 99, size: 8, elements: !1709, identifier: "_ZTS24__cuda_builtin_gridDim_t")
!1709 = !{!1710, !1711, !1712, !1713, !1718, !1722, !1726, !1729}
!1710 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_xEv", scope: !1708, file: !1008, line: 100, type: !1012, isLocal: false, isDefinition: false, scopeLine: 100, flags: DIFlagPrototyped, isOptimized: true)
!1711 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_yEv", scope: !1708, file: !1008, line: 101, type: !1012, isLocal: false, isDefinition: false, scopeLine: 101, flags: DIFlagPrototyped, isOptimized: true)
!1712 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_zEv", scope: !1708, file: !1008, line: 102, type: !1012, isLocal: false, isDefinition: false, scopeLine: 102, flags: DIFlagPrototyped, isOptimized: true)
!1713 = !DISubprogram(name: "operator dim3", linkageName: "_ZNK24__cuda_builtin_gridDim_tcv4dim3Ev", scope: !1708, file: !1008, line: 105, type: !1714, isLocal: false, isDefinition: false, scopeLine: 105, flags: DIFlagPrototyped, isOptimized: true)
!1714 = !DISubroutineType(types: !1715)
!1715 = !{!1054, !1716}
!1716 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1717, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1717 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1708)
!1718 = !DISubprogram(name: "__cuda_builtin_gridDim_t", scope: !1708, file: !1008, line: 107, type: !1719, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1719 = !DISubroutineType(types: !1720)
!1720 = !{null, !1721}
!1721 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1708, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1722 = !DISubprogram(name: "__cuda_builtin_gridDim_t", scope: !1708, file: !1008, line: 107, type: !1723, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1723 = !DISubroutineType(types: !1724)
!1724 = !{null, !1721, !1725}
!1725 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1717, size: 64)
!1726 = !DISubprogram(name: "operator=", linkageName: "_ZNK24__cuda_builtin_gridDim_taSERKS_", scope: !1708, file: !1008, line: 107, type: !1727, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1727 = !DISubroutineType(types: !1728)
!1728 = !{null, !1716, !1725}
!1729 = !DISubprogram(name: "operator&", linkageName: "_ZNK24__cuda_builtin_gridDim_tadEv", scope: !1708, file: !1008, line: 107, type: !1730, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1730 = !DISubroutineType(types: !1731)
!1731 = !{!1732, !1716}
!1732 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1708, size: 64)
!1733 = distinct !DILocation(line: 440, column: 32, scope: !1734)
!1734 = !DILexicalBlockFile(scope: !1702, file: !3, discriminator: 3)
!1735 = !{i32 1, i32 65536}
!1736 = !DILocation(line: 440, column: 31, scope: !1702)
!1737 = !DILocation(line: 440, column: 19, scope: !1702)
!1738 = !DILocation(line: 440, column: 45, scope: !1702)
!1739 = !DILocation(line: 440, column: 43, scope: !1702)
!1740 = !DILocation(line: 440, column: 51, scope: !1702)
!1741 = !DILocation(line: 440, column: 92, scope: !1742)
!1742 = !DILexicalBlockFile(scope: !1702, file: !3, discriminator: 1)
!1743 = !DILocation(line: 440, column: 90, scope: !1742)
!1744 = !DILocation(line: 440, column: 7, scope: !1745)
!1745 = !DILexicalBlockFile(scope: !1672, file: !3, discriminator: 1)
!1746 = !DILocation(line: 446, column: 8, scope: !1672)
!1747 = !DILocalVariable(name: "val", arg: 2, scope: !1748, file: !1749, line: 201, type: !20)
!1748 = distinct !DISubprogram(name: "atomicAdd", linkageName: "_ZL9atomicAddPyy", scope: !1749, file: !1749, line: 201, type: !1750, isLocal: true, isDefinition: true, scopeLine: 202, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1753)
!1749 = !DIFile(filename: "/usr/local/cuda/include/device_atomic_functions.hpp", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/syr2kllvm")
!1750 = !DISubroutineType(types: !1751)
!1751 = !{!20, !1752, !20}
!1752 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !20, size: 64)
!1753 = !{!1754, !1747}
!1754 = !DILocalVariable(name: "address", arg: 1, scope: !1748, file: !1749, line: 201, type: !1752)
!1755 = !DILocation(line: 201, column: 123, scope: !1748, inlinedAt: !1756)
!1756 = distinct !DILocation(line: 448, column: 12, scope: !1672)
!1757 = !DILocalVariable(name: "val", arg: 2, scope: !1758, file: !533, line: 1524, type: !20)
!1758 = distinct !DISubprogram(name: "__ullAtomicAdd", linkageName: "_ZL14__ullAtomicAddPyy", scope: !533, file: !533, line: 1524, type: !1750, isLocal: true, isDefinition: true, scopeLine: 1525, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1759)
!1759 = !{!1760, !1757}
!1760 = !DILocalVariable(name: "p", arg: 1, scope: !1758, file: !533, line: 1524, type: !1752)
!1761 = !DILocation(line: 1524, column: 77, scope: !1758, inlinedAt: !1762)
!1762 = distinct !DILocation(line: 203, column: 10, scope: !1748, inlinedAt: !1756)
!1763 = !DILocation(line: 1526, column: 10, scope: !1758, inlinedAt: !1762)
!1764 = !DILocation(line: 459, column: 21, scope: !1672)
!1765 = !DILocation(line: 460, column: 6, scope: !1672)
!1766 = !DILocation(line: 461, column: 12, scope: !1672)
!1767 = !DILocation(line: 462, column: 17, scope: !1686)
!1768 = !DILocation(line: 462, column: 23, scope: !1769)
!1769 = !DILexicalBlockFile(scope: !1689, file: !3, discriminator: 1)
!1770 = !DILocation(line: 462, column: 32, scope: !1769)
!1771 = !DILocation(line: 462, column: 9, scope: !1772)
!1772 = !DILexicalBlockFile(scope: !1686, file: !3, discriminator: 1)
!1773 = !DILocation(line: 465, column: 16, scope: !1774)
!1774 = distinct !DILexicalBlock(scope: !1688, file: !3, line: 465, column: 7)
!1775 = !DILocation(line: 475, column: 11, scope: !1672)
!1776 = !DILocation(line: 476, column: 2, scope: !1672)
!1777 = !DILocation(line: 476, column: 13, scope: !1672)
!1778 = !DILocation(line: 476, column: 17, scope: !1672)
!1779 = !{!1780, !1697, i64 8}
!1780 = !{!"_ZTS7BBlog_t", !1326, i64 0, !1326, i64 2, !1326, i64 4, !1326, i64 6, !1697, i64 8, !1238, i64 16, !1238, i64 20, !1238, i64 24}
!1781 = !DILocation(line: 67, column: 3, scope: !1529, inlinedAt: !1782)
!1782 = distinct !DILocation(line: 478, column: 27, scope: !1672)
!1783 = !DILocation(line: 478, column: 27, scope: !1672)
!1784 = !DILocation(line: 478, column: 20, scope: !1672)
!1785 = !DILocation(line: 478, column: 25, scope: !1672)
!1786 = !{!1780, !1326, i64 4}
!1787 = !DILocation(line: 68, column: 3, scope: !1541, inlinedAt: !1788)
!1788 = distinct !DILocation(line: 479, column: 27, scope: !1672)
!1789 = !DILocation(line: 479, column: 27, scope: !1672)
!1790 = !DILocation(line: 479, column: 20, scope: !1672)
!1791 = !DILocation(line: 479, column: 25, scope: !1672)
!1792 = !{!1780, !1326, i64 6}
!1793 = !DILocation(line: 480, column: 27, scope: !1672)
!1794 = !DILocation(line: 480, column: 20, scope: !1672)
!1795 = !DILocation(line: 480, column: 25, scope: !1672)
!1796 = !{!1780, !1326, i64 0}
!1797 = !DILocation(line: 481, column: 27, scope: !1672)
!1798 = !DILocation(line: 481, column: 20, scope: !1672)
!1799 = !DILocation(line: 481, column: 25, scope: !1672)
!1800 = !{!1780, !1326, i64 2}
!1801 = !DILocation(line: 482, column: 20, scope: !1672)
!1802 = !DILocation(line: 482, column: 26, scope: !1672)
!1803 = !{!1780, !1238, i64 16}
!1804 = !DILocation(line: 483, column: 20, scope: !1672)
!1805 = !DILocation(line: 483, column: 26, scope: !1672)
!1806 = !{!1780, !1238, i64 20}
!1807 = !DILocation(line: 486, column: 13, scope: !1672)
!1808 = !DILocation(line: 486, column: 17, scope: !1672)
!1809 = !{!1780, !1238, i64 24}
!1810 = !DILocation(line: 467, column: 10, scope: !1688)
!1811 = !DILocation(line: 467, column: 15, scope: !1688)
!1812 = !DILocation(line: 467, column: 7, scope: !1688)
!1813 = !DILocation(line: 468, column: 13, scope: !1688)
!1814 = !DILocation(line: 468, column: 10, scope: !1688)
!1815 = !DILocation(line: 471, column: 9, scope: !1689)
!1816 = !DILocation(line: 462, column: 39, scope: !1817)
!1817 = !DILexicalBlockFile(scope: !1689, file: !3, discriminator: 3)
!1818 = !DILocation(line: 462, column: 28, scope: !1769)
!1819 = distinct !{!1819, !1820, !1821}
!1820 = !DILocation(line: 462, column: 9, scope: !1686)
!1821 = !DILocation(line: 471, column: 9, scope: !1686)
!1822 = !DILocation(line: 493, column: 1, scope: !1745)
!1823 = distinct !DISubprogram(name: "storeLines", linkageName: "_Z10storeLinesPvssss", scope: !3, file: !3, line: 496, type: !1824, isLocal: false, isDefinition: true, scopeLine: 497, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1826)
!1824 = !DISubroutineType(types: !1825)
!1825 = !{null, !364, !15, !15, !15, !15}
!1826 = !{!1827, !1828, !1829, !1830, !1831, !1832, !1833, !1834}
!1827 = !DILocalVariable(name: "p", arg: 1, scope: !1823, file: !3, line: 496, type: !364)
!1828 = !DILocalVariable(name: "size", arg: 2, scope: !1823, file: !3, line: 496, type: !15)
!1829 = !DILocalVariable(name: "line", arg: 3, scope: !1823, file: !3, line: 496, type: !15)
!1830 = !DILocalVariable(name: "colmn", arg: 4, scope: !1823, file: !3, line: 496, type: !15)
!1831 = !DILocalVariable(name: "op", arg: 5, scope: !1823, file: !3, line: 496, type: !15)
!1832 = !DILocalVariable(name: "bid", scope: !1823, file: !3, line: 500, type: !8)
!1833 = !DILocalVariable(name: "buffer_oN_DeViCe_short", scope: !1823, file: !3, line: 508, type: !24)
!1834 = !DILocalVariable(name: "buffer_oN_DeViCe_long", scope: !1823, file: !3, line: 509, type: !25)
!1835 = !DILocation(line: 496, column: 34, scope: !1823)
!1836 = !DILocation(line: 496, column: 43, scope: !1823)
!1837 = !DILocation(line: 496, column: 64, scope: !1823)
!1838 = !DILocation(line: 496, column: 76, scope: !1823)
!1839 = !DILocation(line: 496, column: 89, scope: !1823)
!1840 = !DILocation(line: 498, column: 2, scope: !1823)
!1841 = !DILocation(line: 498, column: 2, scope: !1842)
!1842 = !DILexicalBlockFile(scope: !1823, file: !3, discriminator: 2)
!1843 = !DILocation(line: 201, column: 123, scope: !1748, inlinedAt: !1844)
!1844 = distinct !DILocation(line: 500, column: 12, scope: !1823)
!1845 = !DILocation(line: 1524, column: 77, scope: !1758, inlinedAt: !1846)
!1846 = distinct !DILocation(line: 203, column: 10, scope: !1748, inlinedAt: !1844)
!1847 = !DILocation(line: 1526, column: 10, scope: !1758, inlinedAt: !1846)
!1848 = !DILocation(line: 500, column: 12, scope: !1823)
!1849 = !DILocation(line: 500, column: 6, scope: !1823)
!1850 = !DILocation(line: 508, column: 9, scope: !1823)
!1851 = !DILocation(line: 509, column: 8, scope: !1823)
!1852 = !DILocation(line: 78, column: 3, scope: !1535, inlinedAt: !1853)
!1853 = distinct !DILocation(line: 511, column: 37, scope: !1823)
!1854 = !DILocation(line: 511, column: 37, scope: !1823)
!1855 = !DILocation(line: 511, column: 28, scope: !1823)
!1856 = !DILocation(line: 511, column: 2, scope: !1823)
!1857 = !DILocation(line: 511, column: 35, scope: !1823)
!1858 = !{!1326, !1326, i64 0}
!1859 = !DILocation(line: 79, column: 3, scope: !1547, inlinedAt: !1860)
!1860 = distinct !DILocation(line: 512, column: 37, scope: !1823)
!1861 = !DILocation(line: 512, column: 37, scope: !1823)
!1862 = !DILocation(line: 512, column: 31, scope: !1823)
!1863 = !DILocation(line: 512, column: 2, scope: !1823)
!1864 = !DILocation(line: 512, column: 35, scope: !1823)
!1865 = !DILocation(line: 67, column: 3, scope: !1529, inlinedAt: !1866)
!1866 = distinct !DILocation(line: 513, column: 37, scope: !1823)
!1867 = !DILocation(line: 513, column: 37, scope: !1823)
!1868 = !DILocation(line: 513, column: 31, scope: !1823)
!1869 = !DILocation(line: 513, column: 2, scope: !1823)
!1870 = !DILocation(line: 513, column: 35, scope: !1823)
!1871 = !DILocation(line: 68, column: 3, scope: !1541, inlinedAt: !1872)
!1872 = distinct !DILocation(line: 514, column: 37, scope: !1823)
!1873 = !DILocation(line: 514, column: 37, scope: !1823)
!1874 = !DILocation(line: 514, column: 31, scope: !1823)
!1875 = !DILocation(line: 514, column: 2, scope: !1823)
!1876 = !DILocation(line: 514, column: 35, scope: !1823)
!1877 = !DILocation(line: 515, column: 35, scope: !1823)
!1878 = !DILocation(line: 515, column: 2, scope: !1823)
!1879 = !DILocation(line: 515, column: 33, scope: !1823)
!1880 = !{!1881, !1881, i64 0}
!1881 = !{!"long", !1142, i64 0}
!1882 = !DILocation(line: 516, column: 31, scope: !1823)
!1883 = !DILocation(line: 516, column: 2, scope: !1823)
!1884 = !DILocation(line: 516, column: 35, scope: !1823)
!1885 = !DILocation(line: 517, column: 31, scope: !1823)
!1886 = !DILocation(line: 517, column: 2, scope: !1823)
!1887 = !DILocation(line: 517, column: 35, scope: !1823)
!1888 = !DILocation(line: 518, column: 31, scope: !1823)
!1889 = !DILocation(line: 518, column: 2, scope: !1823)
!1890 = !DILocation(line: 518, column: 36, scope: !1823)
!1891 = !DILocation(line: 519, column: 31, scope: !1823)
!1892 = !DILocation(line: 519, column: 2, scope: !1823)
!1893 = !DILocation(line: 519, column: 36, scope: !1823)
!1894 = !DILocation(line: 526, column: 10, scope: !1895)
!1895 = distinct !DILexicalBlock(scope: !1823, file: !3, line: 526, column: 6)
!1896 = !DILocation(line: 526, column: 6, scope: !1823)
!1897 = !DILocation(line: 527, column: 3, scope: !1895)
!1898 = !DILocation(line: 531, column: 1, scope: !1823)
!1899 = distinct !DISubprogram(name: "dumpLines", linkageName: "_Z9dumpLinesv", scope: !3, file: !3, line: 533, type: !337, isLocal: false, isDefinition: true, scopeLine: 534, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1900)
!1900 = !{!1901}
!1901 = !DILocalVariable(name: "ii", scope: !1899, file: !3, line: 537, type: !8)
!1902 = !DILocation(line: 67, column: 3, scope: !1529, inlinedAt: !1903)
!1903 = distinct !DILocation(line: 535, column: 6, scope: !1904)
!1904 = distinct !DILexicalBlock(scope: !1899, file: !3, line: 535, column: 6)
!1905 = !DILocation(line: 535, column: 18, scope: !1904)
!1906 = !DILocation(line: 535, column: 23, scope: !1904)
!1907 = !DILocation(line: 78, column: 3, scope: !1535, inlinedAt: !1908)
!1908 = distinct !DILocation(line: 535, column: 26, scope: !1909)
!1909 = !DILexicalBlockFile(scope: !1904, file: !3, discriminator: 1)
!1910 = !DILocation(line: 535, column: 37, scope: !1909)
!1911 = !DILocation(line: 535, column: 42, scope: !1909)
!1912 = !DILocation(line: 68, column: 3, scope: !1541, inlinedAt: !1913)
!1913 = distinct !DILocation(line: 535, column: 45, scope: !1914)
!1914 = !DILexicalBlockFile(scope: !1904, file: !3, discriminator: 2)
!1915 = !DILocation(line: 535, column: 57, scope: !1914)
!1916 = !DILocation(line: 535, column: 62, scope: !1914)
!1917 = !DILocation(line: 79, column: 3, scope: !1547, inlinedAt: !1918)
!1918 = distinct !DILocation(line: 535, column: 65, scope: !1919)
!1919 = !DILexicalBlockFile(scope: !1904, file: !3, discriminator: 3)
!1920 = !DILocation(line: 535, column: 76, scope: !1919)
!1921 = !DILocation(line: 535, column: 6, scope: !1922)
!1922 = !DILexicalBlockFile(scope: !1899, file: !3, discriminator: 3)
!1923 = !DILocation(line: 542, column: 2, scope: !1899)
!1924 = !DILocation(line: 553, column: 1, scope: !1899)
!1925 = !DILocation(line: 553, column: 1, scope: !1926)
!1926 = !DILexicalBlockFile(scope: !1899, file: !3, discriminator: 1)
!1927 = distinct !DISubprogram(name: "print1", linkageName: "_Z6print1i", scope: !3, file: !3, line: 555, type: !384, isLocal: false, isDefinition: true, scopeLine: 556, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1928)
!1928 = !{!1929}
!1929 = !DILocalVariable(name: "a", arg: 1, scope: !1927, file: !3, line: 555, type: !8)
!1930 = !DILocation(line: 555, column: 28, scope: !1927)
!1931 = !DILocation(line: 67, column: 3, scope: !1529, inlinedAt: !1932)
!1932 = distinct !DILocation(line: 557, column: 6, scope: !1933)
!1933 = distinct !DILexicalBlock(scope: !1927, file: !3, line: 557, column: 6)
!1934 = !DILocation(line: 68, column: 3, scope: !1541, inlinedAt: !1935)
!1935 = distinct !DILocation(line: 557, column: 20, scope: !1936)
!1936 = !DILexicalBlockFile(scope: !1933, file: !3, discriminator: 2)
!1937 = !DILocation(line: 557, column: 18, scope: !1933)
!1938 = !DILocation(line: 78, column: 3, scope: !1535, inlinedAt: !1939)
!1939 = distinct !DILocation(line: 557, column: 34, scope: !1940)
!1940 = !DILexicalBlockFile(scope: !1933, file: !3, discriminator: 3)
!1941 = !DILocation(line: 557, column: 32, scope: !1933)
!1942 = !DILocation(line: 79, column: 3, scope: !1547, inlinedAt: !1943)
!1943 = distinct !DILocation(line: 557, column: 47, scope: !1944)
!1944 = !DILexicalBlockFile(scope: !1933, file: !3, discriminator: 4)
!1945 = !DILocation(line: 557, column: 58, scope: !1933)
!1946 = !DILocation(line: 557, column: 63, scope: !1933)
!1947 = !DILocation(line: 557, column: 66, scope: !1948)
!1948 = !DILexicalBlockFile(scope: !1933, file: !3, discriminator: 1)
!1949 = !DILocation(line: 557, column: 6, scope: !1950)
!1950 = !DILexicalBlockFile(scope: !1927, file: !3, discriminator: 1)
!1951 = !DILocation(line: 559, column: 7, scope: !1952)
!1952 = distinct !DILexicalBlock(scope: !1933, file: !3, line: 558, column: 2)
!1953 = !DILocation(line: 560, column: 4, scope: !1954)
!1954 = distinct !DILexicalBlock(scope: !1952, file: !3, line: 559, column: 7)
!1955 = !DILocation(line: 560, column: 4, scope: !1956)
!1956 = !DILexicalBlockFile(scope: !1954, file: !3, discriminator: 2)
!1957 = !DILocation(line: 562, column: 4, scope: !1958)
!1958 = distinct !DILexicalBlock(scope: !1954, file: !3, line: 561, column: 12)
!1959 = !DILocation(line: 562, column: 4, scope: !1960)
!1960 = !DILexicalBlockFile(scope: !1958, file: !3, discriminator: 2)
!1961 = !DILocation(line: 564, column: 4, scope: !1958)
!1962 = !DILocation(line: 566, column: 1, scope: !1927)
!1963 = distinct !DISubprogram(name: "print2", linkageName: "_Z6print2v", scope: !3, file: !3, line: 568, type: !337, isLocal: false, isDefinition: true, scopeLine: 569, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !4)
!1964 = !DILocation(line: 67, column: 3, scope: !1529, inlinedAt: !1965)
!1965 = distinct !DILocation(line: 570, column: 6, scope: !1966)
!1966 = distinct !DILexicalBlock(scope: !1963, file: !3, line: 570, column: 6)
!1967 = !DILocation(line: 68, column: 3, scope: !1541, inlinedAt: !1968)
!1968 = distinct !DILocation(line: 570, column: 20, scope: !1969)
!1969 = !DILexicalBlockFile(scope: !1966, file: !3, discriminator: 2)
!1970 = !DILocation(line: 570, column: 18, scope: !1966)
!1971 = !DILocation(line: 78, column: 3, scope: !1535, inlinedAt: !1972)
!1972 = distinct !DILocation(line: 570, column: 34, scope: !1973)
!1973 = !DILexicalBlockFile(scope: !1966, file: !3, discriminator: 3)
!1974 = !DILocation(line: 570, column: 32, scope: !1966)
!1975 = !DILocation(line: 79, column: 3, scope: !1547, inlinedAt: !1976)
!1976 = distinct !DILocation(line: 570, column: 47, scope: !1977)
!1977 = !DILexicalBlockFile(scope: !1966, file: !3, discriminator: 4)
!1978 = !DILocation(line: 570, column: 58, scope: !1966)
!1979 = !DILocation(line: 570, column: 63, scope: !1966)
!1980 = !DILocation(line: 570, column: 66, scope: !1981)
!1981 = !DILexicalBlockFile(scope: !1966, file: !3, discriminator: 1)
!1982 = !DILocation(line: 570, column: 6, scope: !1983)
!1983 = !DILexicalBlockFile(scope: !1963, file: !3, discriminator: 1)
!1984 = !DILocation(line: 571, column: 10, scope: !1966)
!1985 = !DILocation(line: 571, column: 10, scope: !1969)
!1986 = !DILocation(line: 572, column: 1, scope: !1963)
!1987 = distinct !DISubprogram(name: "print3", linkageName: "_Z6print3ii", scope: !3, file: !3, line: 574, type: !1988, isLocal: false, isDefinition: true, scopeLine: 575, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1990)
!1988 = !DISubroutineType(types: !1989)
!1989 = !{null, !8, !8}
!1990 = !{!1991, !1992}
!1991 = !DILocalVariable(name: "line", arg: 1, scope: !1987, file: !3, line: 574, type: !8)
!1992 = !DILocalVariable(name: "col", arg: 2, scope: !1987, file: !3, line: 574, type: !8)
!1993 = !DILocation(line: 574, column: 28, scope: !1987)
!1994 = !DILocation(line: 574, column: 38, scope: !1987)
!1995 = !DILocation(line: 67, column: 3, scope: !1529, inlinedAt: !1996)
!1996 = distinct !DILocation(line: 576, column: 6, scope: !1997)
!1997 = distinct !DILexicalBlock(scope: !1987, file: !3, line: 576, column: 6)
!1998 = !DILocation(line: 68, column: 3, scope: !1541, inlinedAt: !1999)
!1999 = distinct !DILocation(line: 576, column: 20, scope: !2000)
!2000 = !DILexicalBlockFile(scope: !1997, file: !3, discriminator: 2)
!2001 = !DILocation(line: 576, column: 18, scope: !1997)
!2002 = !DILocation(line: 78, column: 3, scope: !1535, inlinedAt: !2003)
!2003 = distinct !DILocation(line: 576, column: 34, scope: !2004)
!2004 = !DILexicalBlockFile(scope: !1997, file: !3, discriminator: 3)
!2005 = !DILocation(line: 576, column: 32, scope: !1997)
!2006 = !DILocation(line: 79, column: 3, scope: !1547, inlinedAt: !2007)
!2007 = distinct !DILocation(line: 576, column: 47, scope: !2008)
!2008 = !DILexicalBlockFile(scope: !1997, file: !3, discriminator: 4)
!2009 = !DILocation(line: 576, column: 58, scope: !1997)
!2010 = !DILocation(line: 576, column: 63, scope: !1997)
!2011 = !DILocation(line: 576, column: 66, scope: !2012)
!2012 = !DILexicalBlockFile(scope: !1997, file: !3, discriminator: 1)
!2013 = !DILocation(line: 576, column: 6, scope: !2014)
!2014 = !DILexicalBlockFile(scope: !1987, file: !3, discriminator: 1)
!2015 = !DILocation(line: 577, column: 10, scope: !1997)
!2016 = !DILocation(line: 577, column: 10, scope: !2000)
!2017 = !DILocation(line: 578, column: 1, scope: !1987)
!2018 = distinct !DISubprogram(name: "print5", scope: !3, file: !3, line: 580, type: !2019, isLocal: false, isDefinition: true, scopeLine: 581, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !2021)
!2019 = !DISubroutineType(types: !2020)
!2020 = !{null, !364, !8, !8, !8, !8}
!2021 = !{!2022, !2023, !2024, !2025, !2026}
!2022 = !DILocalVariable(name: "p", arg: 1, scope: !2018, file: !3, line: 580, type: !364)
!2023 = !DILocalVariable(name: "bits", arg: 2, scope: !2018, file: !3, line: 580, type: !8)
!2024 = !DILocalVariable(name: "sline", arg: 3, scope: !2018, file: !3, line: 580, type: !8)
!2025 = !DILocalVariable(name: "scolm", arg: 4, scope: !2018, file: !3, line: 580, type: !8)
!2026 = !DILocalVariable(name: "op", arg: 5, scope: !2018, file: !3, line: 580, type: !8)
!2027 = !DILocation(line: 580, column: 30, scope: !2018)
!2028 = !DILocation(line: 580, column: 37, scope: !2018)
!2029 = !DILocation(line: 580, column: 47, scope: !2018)
!2030 = !DILocation(line: 580, column: 58, scope: !2018)
!2031 = !DILocation(line: 580, column: 69, scope: !2018)
!2032 = !DILocation(line: 78, column: 3, scope: !1535, inlinedAt: !2033)
!2033 = distinct !DILocation(line: 587, column: 8, scope: !2034)
!2034 = distinct !DILexicalBlock(scope: !2018, file: !3, line: 587, column: 7)
!2035 = !DILocation(line: 79, column: 3, scope: !1547, inlinedAt: !2036)
!2036 = distinct !DILocation(line: 587, column: 21, scope: !2037)
!2037 = !DILexicalBlockFile(scope: !2034, file: !3, discriminator: 2)
!2038 = !DILocation(line: 100, column: 3, scope: !1707, inlinedAt: !2039)
!2039 = distinct !DILocation(line: 587, column: 32, scope: !2040)
!2040 = !DILexicalBlockFile(scope: !2034, file: !3, discriminator: 3)
!2041 = !DILocation(line: 587, column: 31, scope: !2034)
!2042 = !DILocation(line: 587, column: 19, scope: !2034)
!2043 = !DILocation(line: 587, column: 45, scope: !2034)
!2044 = !DILocation(line: 587, column: 43, scope: !2034)
!2045 = !DILocation(line: 587, column: 51, scope: !2034)
!2046 = !DILocation(line: 587, column: 92, scope: !2047)
!2047 = !DILexicalBlockFile(scope: !2034, file: !3, discriminator: 1)
!2048 = !DILocation(line: 587, column: 90, scope: !2047)
!2049 = !DILocation(line: 587, column: 7, scope: !2050)
!2050 = !DILexicalBlockFile(scope: !2018, file: !3, discriminator: 1)
!2051 = !DILocation(line: 496, column: 34, scope: !1823, inlinedAt: !2052)
!2052 = distinct !DILocation(line: 590, column: 9, scope: !2018)
!2053 = !DILocation(line: 496, column: 43, scope: !1823, inlinedAt: !2052)
!2054 = !DILocation(line: 496, column: 64, scope: !1823, inlinedAt: !2052)
!2055 = !DILocation(line: 496, column: 76, scope: !1823, inlinedAt: !2052)
!2056 = !DILocation(line: 496, column: 89, scope: !1823, inlinedAt: !2052)
!2057 = !DILocation(line: 498, column: 2, scope: !1823, inlinedAt: !2052)
!2058 = !DILocation(line: 498, column: 2, scope: !1842, inlinedAt: !2052)
!2059 = !DILocation(line: 590, column: 76, scope: !2018)
!2060 = !DILocation(line: 590, column: 62, scope: !2018)
!2061 = !DILocation(line: 590, column: 47, scope: !2018)
!2062 = !DILocation(line: 590, column: 35, scope: !2018)
!2063 = !DILocation(line: 590, column: 30, scope: !2018)
!2064 = !DILocation(line: 201, column: 123, scope: !1748, inlinedAt: !2065)
!2065 = distinct !DILocation(line: 500, column: 12, scope: !1823, inlinedAt: !2052)
!2066 = !DILocation(line: 1524, column: 77, scope: !1758, inlinedAt: !2067)
!2067 = distinct !DILocation(line: 203, column: 10, scope: !1748, inlinedAt: !2065)
!2068 = !DILocation(line: 1526, column: 10, scope: !1758, inlinedAt: !2067)
!2069 = !DILocation(line: 500, column: 12, scope: !1823, inlinedAt: !2052)
!2070 = !DILocation(line: 500, column: 6, scope: !1823, inlinedAt: !2052)
!2071 = !DILocation(line: 508, column: 9, scope: !1823, inlinedAt: !2052)
!2072 = !DILocation(line: 509, column: 8, scope: !1823, inlinedAt: !2052)
!2073 = !DILocation(line: 511, column: 37, scope: !1823, inlinedAt: !2052)
!2074 = !DILocation(line: 511, column: 28, scope: !1823, inlinedAt: !2052)
!2075 = !DILocation(line: 511, column: 2, scope: !1823, inlinedAt: !2052)
!2076 = !DILocation(line: 511, column: 35, scope: !1823, inlinedAt: !2052)
!2077 = !DILocation(line: 512, column: 37, scope: !1823, inlinedAt: !2052)
!2078 = !DILocation(line: 512, column: 31, scope: !1823, inlinedAt: !2052)
!2079 = !DILocation(line: 512, column: 2, scope: !1823, inlinedAt: !2052)
!2080 = !DILocation(line: 512, column: 35, scope: !1823, inlinedAt: !2052)
!2081 = !DILocation(line: 67, column: 3, scope: !1529, inlinedAt: !2082)
!2082 = distinct !DILocation(line: 513, column: 37, scope: !1823, inlinedAt: !2052)
!2083 = !DILocation(line: 513, column: 37, scope: !1823, inlinedAt: !2052)
!2084 = !DILocation(line: 513, column: 31, scope: !1823, inlinedAt: !2052)
!2085 = !DILocation(line: 513, column: 2, scope: !1823, inlinedAt: !2052)
!2086 = !DILocation(line: 513, column: 35, scope: !1823, inlinedAt: !2052)
!2087 = !DILocation(line: 68, column: 3, scope: !1541, inlinedAt: !2088)
!2088 = distinct !DILocation(line: 514, column: 37, scope: !1823, inlinedAt: !2052)
!2089 = !DILocation(line: 514, column: 37, scope: !1823, inlinedAt: !2052)
!2090 = !DILocation(line: 514, column: 31, scope: !1823, inlinedAt: !2052)
!2091 = !DILocation(line: 514, column: 2, scope: !1823, inlinedAt: !2052)
!2092 = !DILocation(line: 514, column: 35, scope: !1823, inlinedAt: !2052)
!2093 = !DILocation(line: 515, column: 35, scope: !1823, inlinedAt: !2052)
!2094 = !DILocation(line: 515, column: 2, scope: !1823, inlinedAt: !2052)
!2095 = !DILocation(line: 515, column: 33, scope: !1823, inlinedAt: !2052)
!2096 = !DILocation(line: 516, column: 31, scope: !1823, inlinedAt: !2052)
!2097 = !DILocation(line: 516, column: 2, scope: !1823, inlinedAt: !2052)
!2098 = !DILocation(line: 516, column: 35, scope: !1823, inlinedAt: !2052)
!2099 = !DILocation(line: 517, column: 31, scope: !1823, inlinedAt: !2052)
!2100 = !DILocation(line: 517, column: 2, scope: !1823, inlinedAt: !2052)
!2101 = !DILocation(line: 517, column: 35, scope: !1823, inlinedAt: !2052)
!2102 = !DILocation(line: 518, column: 31, scope: !1823, inlinedAt: !2052)
!2103 = !DILocation(line: 518, column: 2, scope: !1823, inlinedAt: !2052)
!2104 = !DILocation(line: 518, column: 36, scope: !1823, inlinedAt: !2052)
!2105 = !DILocation(line: 519, column: 31, scope: !1823, inlinedAt: !2052)
!2106 = !DILocation(line: 519, column: 2, scope: !1823, inlinedAt: !2052)
!2107 = !DILocation(line: 519, column: 36, scope: !1823, inlinedAt: !2052)
!2108 = !DILocation(line: 526, column: 10, scope: !1895, inlinedAt: !2052)
!2109 = !DILocation(line: 526, column: 6, scope: !1823, inlinedAt: !2052)
!2110 = !DILocation(line: 527, column: 3, scope: !1895, inlinedAt: !2052)
!2111 = !DILocation(line: 531, column: 1, scope: !1823, inlinedAt: !2052)
!2112 = !DILocation(line: 594, column: 1, scope: !2018)
!2113 = !DILocation(line: 594, column: 1, scope: !2050)
!2114 = distinct !DISubprogram(name: "RetKernel", scope: !3, file: !3, line: 597, type: !337, isLocal: false, isDefinition: true, scopeLine: 598, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !2115)
!2115 = !{!2116, !2121, !2122, !2123}
!2116 = !DILocalVariable(name: "bbbuffer_oN_DeViCe_short", scope: !2117, file: !3, line: 617, type: !9)
!2117 = distinct !DILexicalBlock(scope: !2118, file: !3, line: 615, column: 3)
!2118 = distinct !DILexicalBlock(scope: !2119, file: !3, line: 601, column: 7)
!2119 = distinct !DILexicalBlock(scope: !2120, file: !3, line: 600, column: 2)
!2120 = distinct !DILexicalBlock(scope: !2114, file: !3, line: 599, column: 6)
!2121 = !DILocalVariable(name: "offset1", scope: !2119, file: !3, line: 643, type: !369)
!2122 = !DILocalVariable(name: "offset2", scope: !2119, file: !3, line: 644, type: !369)
!2123 = !DILocalVariable(name: "ptr", scope: !2119, file: !3, line: 654, type: !364)
!2124 = !DILocation(line: 67, column: 3, scope: !1529, inlinedAt: !2125)
!2125 = distinct !DILocation(line: 599, column: 6, scope: !2120)
!2126 = !DILocation(line: 68, column: 3, scope: !1541, inlinedAt: !2127)
!2127 = distinct !DILocation(line: 599, column: 20, scope: !2128)
!2128 = !DILexicalBlockFile(scope: !2120, file: !3, discriminator: 1)
!2129 = !DILocation(line: 599, column: 18, scope: !2120)
!2130 = !DILocation(line: 78, column: 3, scope: !1535, inlinedAt: !2131)
!2131 = distinct !DILocation(line: 599, column: 34, scope: !2132)
!2132 = !DILexicalBlockFile(scope: !2120, file: !3, discriminator: 2)
!2133 = !DILocation(line: 599, column: 32, scope: !2120)
!2134 = !DILocation(line: 79, column: 3, scope: !1547, inlinedAt: !2135)
!2135 = distinct !DILocation(line: 599, column: 47, scope: !2136)
!2136 = !DILexicalBlockFile(scope: !2120, file: !3, discriminator: 3)
!2137 = !DILocation(line: 599, column: 58, scope: !2120)
!2138 = !DILocation(line: 599, column: 6, scope: !2114)
!2139 = !DILocation(line: 617, column: 24, scope: !2117)
!2140 = !DILocation(line: 89, column: 3, scope: !2141, inlinedAt: !2142)
!2141 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_xEv", scope: !1046, file: !1008, line: 89, type: !1012, isLocal: false, isDefinition: true, scopeLine: 89, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1048, variables: !4)
!2142 = distinct !DILocation(line: 618, column: 50, scope: !2117)
!2143 = !DILocation(line: 618, column: 50, scope: !2117)
!2144 = !DILocation(line: 618, column: 48, scope: !2117)
!2145 = !DILocation(line: 90, column: 3, scope: !2146, inlinedAt: !2147)
!2146 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_yEv", scope: !1046, file: !1008, line: 90, type: !1012, isLocal: false, isDefinition: true, scopeLine: 90, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1049, variables: !4)
!2147 = distinct !DILocation(line: 619, column: 52, scope: !2117)
!2148 = !DILocation(line: 619, column: 52, scope: !2117)
!2149 = !DILocation(line: 619, column: 50, scope: !2117)
!2150 = !DILocation(line: 100, column: 3, scope: !1707, inlinedAt: !2151)
!2151 = distinct !DILocation(line: 620, column: 52, scope: !2117)
!2152 = !DILocation(line: 620, column: 52, scope: !2117)
!2153 = !DILocation(line: 620, column: 50, scope: !2117)
!2154 = !DILocation(line: 101, column: 3, scope: !2155, inlinedAt: !2156)
!2155 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_yEv", scope: !1708, file: !1008, line: 101, type: !1012, isLocal: false, isDefinition: true, scopeLine: 101, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1711, variables: !4)
!2156 = distinct !DILocation(line: 621, column: 52, scope: !2117)
!2157 = !DILocation(line: 621, column: 52, scope: !2117)
!2158 = !DILocation(line: 621, column: 50, scope: !2117)
!2159 = !DILocation(line: 622, column: 51, scope: !2117)
!2160 = !DILocation(line: 622, column: 49, scope: !2117)
!2161 = !DILocation(line: 623, column: 51, scope: !2117)
!2162 = !DILocation(line: 624, column: 51, scope: !2117)
!2163 = !DILocation(line: 625, column: 3, scope: !2117)
!2164 = !DILocation(line: 626, column: 64, scope: !2117)
!2165 = !DILocation(line: 626, column: 3, scope: !2117)
!2166 = !DILocation(line: 643, column: 17, scope: !2119)
!2167 = !DILocation(line: 644, column: 17, scope: !2119)
!2168 = !DILocation(line: 646, column: 3, scope: !2119)
!2169 = !DILocation(line: 648, column: 17, scope: !2119)
!2170 = !DILocation(line: 654, column: 9, scope: !2119)
!2171 = !DILocation(line: 392, column: 3, scope: !2172, inlinedAt: !2179)
!2172 = distinct !DISubprogram(name: "memcpy", linkageName: "_ZL6memcpyPvPKvm", scope: !533, file: !533, line: 390, type: !2173, isLocal: true, isDefinition: true, scopeLine: 391, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !2175)
!2173 = !DISubroutineType(types: !2174)
!2174 = !{!364, !364, !365, !367}
!2175 = !{!2176, !2177, !2178}
!2176 = !DILocalVariable(name: "dest", arg: 1, scope: !2172, file: !533, line: 390, type: !364)
!2177 = !DILocalVariable(name: "src", arg: 2, scope: !2172, file: !533, line: 390, type: !365)
!2178 = !DILocalVariable(name: "n", arg: 3, scope: !2172, file: !533, line: 390, type: !367)
!2179 = distinct !DILocation(line: 657, column: 3, scope: !2119)
!2180 = !DILocation(line: 392, column: 3, scope: !2172, inlinedAt: !2181)
!2181 = distinct !DILocation(line: 660, column: 3, scope: !2119)
!2182 = !DILocation(line: 685, column: 10, scope: !2119)
!2183 = !DILocation(line: 686, column: 12, scope: !2119)
!2184 = !DILocation(line: 688, column: 2, scope: !2119)
!2185 = !DILocation(line: 689, column: 1, scope: !2114)
