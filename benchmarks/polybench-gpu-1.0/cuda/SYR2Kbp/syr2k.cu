/**
 * syr2k.cu: This file is part of the PolyBench/GPU 1.0 test suite.
 *
 *
 * Contact: Scott Grauer-Gray <sgrauerg@gmail.com>
 * Louis-Noel Pouchet <pouchet@cse.ohio-state.edu>
 * Web address: http://www.cse.ohio-state.edu/~pouchet/software/polybench/GPU
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <unistd.h>
#include <sys/time.h>
#include <unistd.h>
#include <cuda.h>

#include "../../common/polybenchUtilFuncts.h"

//define the error threshold for the results "not matching"
#define PERCENT_DIFF_ERROR_THRESHOLD 0.005

#define GPU_DEVICE 0

/* Problem size */
#define N 2048
#define M 2048

/* Thread block dimensions */
#define DIM_THREAD_BLOCK_X 32
#define DIM_THREAD_BLOCK_Y 8

/* Declared constant values for ALPHA and BETA (same as values in PolyBench 2.0) */
#define ALPHA 12435
#define BETA 4546

/* Can switch DATA_TYPE between float and double */
typedef float DATA_TYPE;



void init_arrays(DATA_TYPE *A, DATA_TYPE *B, DATA_TYPE *C)
{
	int i, j;
  
	for (i = 0; i < N; i++)
    	{
    		for (j = 0; j < N; j++)
		{
			C[i*N + j] = ((DATA_TYPE) i*j + 2) / N;
		}
      	
		for (j = 0; j < M; j++)
		{
	  		A[i*N + j] = ((DATA_TYPE) i*j) / N;
	  		B[i*N + j] = ((DATA_TYPE) i*j + 1) / N;
		}
    	}
}


void syr2k(DATA_TYPE *A, DATA_TYPE *B, DATA_TYPE *C)
{
	int i, j, k;
		
  	for (i = 0; i < N; i++)
	{
   		for (j = 0; j < N; j++)
		{
     			C[i*N + j] *= BETA;
		}
	}

  	for (i = 0; i < N; i++)
	{
   		for (j = 0; j < N; j++)
		{
      			for (k = 0; k < M; k++)
			{
	  			C[i*N + j] += ALPHA * A[i*M + k] * B[j*M + k];
	 		 	C[i*N + j] += ALPHA * B[i*M + k] * A[j*M + k];
			}
		}
	}
}


void compareResults(DATA_TYPE *C, DATA_TYPE *C_outputFromGpu)
{
	int i,j,fail;
	fail = 0;

	// Compare C with D
	for (i=0; i<N; i++)
	{
		for (j=0; j<N; j++)
		{
			if (percentDiff(C[i*N + j], C_outputFromGpu[i*N + j]) > PERCENT_DIFF_ERROR_THRESHOLD)
			{ 
				fail++;
			}
		}
	}
	
	// print results
	printf("Non-Matching CPU-GPU Outputs Beyond Error Threshold of %f Percent: %d\n", PERCENT_DIFF_ERROR_THRESHOLD, fail);
}


__global__ void syr2k_kernel(DATA_TYPE *a, DATA_TYPE *b, DATA_TYPE *c, int BPTH)
{
	int j = blockIdx.x * blockDim.x + threadIdx.x;
	int i = blockIdx.y * blockDim.y + threadIdx.y;

	unsigned tinb = blockDim.x*threadIdx.y + threadIdx.x;//Du
        bool bypass = (tinb >> 5) >= BPTH; //THRESHOLD
//	if( threadIdx.x==0 && threadIdx.y==0 && blockIdx.x==0 && blockIdx.y==0  )		printf("%d, %d\n", blockDim.x ,blockDim.y);//32*8

	if ((i < N) && (j < N))
	{
	
		if(bypass)
		{
			DATA_TYPE tc, tt;
			asm("ld.global.cg.f32 %0,[%1];":"=f"(tc):"l"(&(c[i * N + j] )) );
			tt = BETA*tc;
			c[i * N + j] = tt;
			
			int k;
			for(k = 0; k < M; k++)
			{
				DATA_TYPE t1, t2, t3, t4 ;
					asm("ld.global.cg.f32 %0,[%1];":"=f"(t1):"l"(&(a[i * M + k] )) );//64bit
					asm("ld.global.cg.f32 %0,[%1];":"=f"(t2):"l"(&(b[j * M + k] )) );//64bit
					asm("ld.global.cg.f32 %0,[%1];":"=f"(t3):"l"(&(b[i * M + k] )) );//64bit
					asm("ld.global.cg.f32 %0,[%1];":"=f"(t4):"l"(&(a[j * M + k] )) );//64bit
				//	asm("ld.global.cg.f32 %0,[%1];":"=f"(tt):"l"(&(c[i * N + j] )) );//64bit

				c[i * N + j] += ALPHA * t1 * t2 + ALPHA * t3 * t4 ;
			}
		}
		else 
		{
			c[i * N + j] *= BETA;

			int k;
			for(k = 0; k < M; k++)
			{
			      	c[i * N + j] += ALPHA * a[i * M + k] * b[j * M + k] + ALPHA * b[i * M + k] * a[j * M + k];
			}

		}//end of if- else
	}
}


void GPU_argv_init()
{
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, GPU_DEVICE);
	printf("setting device %d with name %s\n",GPU_DEVICE,deviceProp.name);

	cudaSetDevice( GPU_DEVICE );

}

void syr2kCuda(DATA_TYPE* A, DATA_TYPE* B, DATA_TYPE* C, DATA_TYPE* C_outputFromGpu, int BPTH) 
{
	double t_start, t_end;

	DATA_TYPE *A_gpu;
	DATA_TYPE *B_gpu;
	DATA_TYPE *C_gpu;

	cudaMalloc((void **)&A_gpu, sizeof(DATA_TYPE) * N * M);
	cudaMalloc((void **)&B_gpu, sizeof(DATA_TYPE) * N * M);
	cudaMalloc((void **)&C_gpu, sizeof(DATA_TYPE) * N * N);
	cudaMemcpy(A_gpu, A, sizeof(DATA_TYPE) * N * M, cudaMemcpyHostToDevice);
	cudaMemcpy(B_gpu, B, sizeof(DATA_TYPE) * N * M, cudaMemcpyHostToDevice);
	cudaMemcpy(C_gpu, C, sizeof(DATA_TYPE) * N * N, cudaMemcpyHostToDevice);
	
	dim3 block(DIM_THREAD_BLOCK_X, DIM_THREAD_BLOCK_Y);
	dim3 grid((size_t)ceil( ((float)N) / ((float)DIM_THREAD_BLOCK_X) ), (size_t)(ceil( ((float)N) / ((float)DIM_THREAD_BLOCK_Y) )));


	cudaFuncSetCacheConfig( syr2k_kernel, cudaFuncCachePreferL1);//Du
//	cudaFuncSetCacheConfig( syr2k_kernel, cudaFuncCachePreferShared);//Du

	cudaDeviceSynchronize();//Du
	t_start = rtclock();

	syr2k_kernel<<<grid,block>>>(A_gpu,B_gpu,C_gpu, BPTH);

	cudaDeviceSynchronize();//Du
//	cudaThreadSynchronize();
	t_end = rtclock();
	fprintf(stderr, "%f\n", t_end - t_start);
	
	cudaMemcpy(C_outputFromGpu, C_gpu, sizeof(DATA_TYPE) * N * N, cudaMemcpyDeviceToHost);

	cudaFree(A_gpu);
	cudaFree(B_gpu);
	cudaFree(C_gpu);
}


int main()
{
	double t_start, t_end;

	DATA_TYPE* A;
	DATA_TYPE* B;
	DATA_TYPE* C;
	DATA_TYPE* C_outputFromGpu;

	A = (DATA_TYPE*)malloc(N*M*sizeof(DATA_TYPE));
	B = (DATA_TYPE*)malloc(N*M*sizeof(DATA_TYPE));
	C = (DATA_TYPE*)malloc(N*M*sizeof(DATA_TYPE));
	C_outputFromGpu = (DATA_TYPE*)malloc(N*M*sizeof(DATA_TYPE));

	init_arrays(A, B, C);
    
	GPU_argv_init();

	int BPTH;
	for (BPTH=0; BPTH <= 8; BPTH++)
	{
		sleep(1);
		syr2kCuda(A, B, C, C_outputFromGpu,  BPTH);
	}
	
	t_start = rtclock();
//	syr2k(A, B, C);//Du
	t_end = rtclock();
	fprintf(stdout, "CPU Runtime: %0.6lfs\n", t_end - t_start);

//	compareResults(C, C_outputFromGpu);//Du

	free(A);
	free(B);
	free(C);
	free(C_outputFromGpu);

  	return 0;
}

