; ModuleID = '<stdin>'
source_filename = "/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/ansf.cu"
target datalayout = "e-i64:64-v16:16-v32:32-n16:32:64"
target triple = "nvptx64-nvidia-cuda"

%printf_args = type { i8* }
%printf_args.0 = type { i8* }
%printf_args.1 = type { i8* }
%printf_args.2 = type { i8* }
%printf_args.3 = type { i32* }
%printf_args.4 = type { i32, i32 }
%printf_args.5 = type { i32, i32 }
%printf_args.6 = type { i32, i32 }
%printf_args.7 = type { i32, i32, i32 }
%printf_args.8 = type { i32 }

@ccnntt = addrspace(1) externally_initialized global i32 1, align 4, !dbg !0
@.str = private unnamed_addr constant [15 x i8] c"d: caller: %s\0A\00", align 1
@.str.1 = private unnamed_addr constant [15 x i8] c"d: callee: %s\0A\00", align 1
@.str.2 = private unnamed_addr constant [15 x i8] c"d: return: %s\0A\00", align 1
@.str.3 = private unnamed_addr constant [18 x i8] c"d: undefined: %s\0A\00", align 1
@buffer_oN_DeViCe = external addrspace(1) global [25165824 x i32], align 4
@.str.4 = private unnamed_addr constant [22 x i8] c"d: buffer handle: %p\0A\00", align 1
@.str.5 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.6 = private unnamed_addr constant [24 x i8] c"d: load by CTA (%d,%d)\0A\00", align 1
@.str.7 = private unnamed_addr constant [25 x i8] c"d: store by CTA (%d,%d)\0A\00", align 1
@.str.8 = private unnamed_addr constant [23 x i8] c"d: !!! undefined !!! \0A\00", align 1
@.str.9 = private unnamed_addr constant [35 x i8] c"d: source line: %d by CTA (%d,%d)\0A\00", align 1
@.str.10 = private unnamed_addr constant [47 x i8] c"d: Kernel Returns: collected [ %d ] entries. \0A\00", align 1

; Function Attrs: nounwind
define void @takeString(i8* %p, i32 %action) local_unnamed_addr #0 !dbg !581 {
entry:
  %tmp = alloca %printf_args, align 8
  %tmp13 = alloca %printf_args.0, align 8
  %tmp17 = alloca %printf_args.1, align 8
  %tmp19 = alloca %printf_args.2, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !585, metadata !587), !dbg !588
  tail call void @llvm.dbg.value(metadata i32 %action, i64 0, metadata !586, metadata !587), !dbg !589
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !590, !range !628
  %cmp = icmp eq i32 %0, 0, !dbg !629
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !630

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !631, !range !660
  %cmp2 = icmp eq i32 %1, 0, !dbg !661
  br i1 %cmp2, label %lor.lhs.false3, label %return, !dbg !662

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !663, !range !628
  %cmp5 = icmp eq i32 %2, 0, !dbg !667
  br i1 %cmp5, label %lor.lhs.false6, label %return, !dbg !668

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !669, !range !660
  %cmp8 = icmp eq i32 %3, 0, !dbg !673
  br i1 %cmp8, label %if.end, label %return, !dbg !674

if.end:                                           ; preds = %lor.lhs.false6
  switch i32 %action, label %if.else18 [
    i32 1, label %if.then10
    i32 2, label %if.then12
    i32 3, label %if.then16
  ], !dbg !676

if.then10:                                        ; preds = %if.end
  %4 = getelementptr inbounds %printf_args, %printf_args* %tmp, i64 0, i32 0, !dbg !677
  store i8* %p, i8** %4, align 8, !dbg !677
  %5 = bitcast %printf_args* %tmp to i8*, !dbg !677
  %6 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i64 0, i64 0), i8* nonnull %5) #3, !dbg !677
  br label %return, !dbg !677

if.then12:                                        ; preds = %if.end
  %7 = getelementptr inbounds %printf_args.0, %printf_args.0* %tmp13, i64 0, i32 0, !dbg !679
  store i8* %p, i8** %7, align 8, !dbg !679
  %8 = bitcast %printf_args.0* %tmp13 to i8*, !dbg !679
  %9 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.1, i64 0, i64 0), i8* nonnull %8) #3, !dbg !679
  br label %return, !dbg !679

if.then16:                                        ; preds = %if.end
  %10 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp17, i64 0, i32 0, !dbg !681
  store i8* %p, i8** %10, align 8, !dbg !681
  %11 = bitcast %printf_args.1* %tmp17 to i8*, !dbg !681
  %12 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i8* nonnull %11) #3, !dbg !681
  br label %return, !dbg !681

if.else18:                                        ; preds = %if.end
  %13 = getelementptr inbounds %printf_args.2, %printf_args.2* %tmp19, i64 0, i32 0, !dbg !683
  store i8* %p, i8** %13, align 8, !dbg !683
  %14 = bitcast %printf_args.2* %tmp19 to i8*, !dbg !683
  %15 = call i32 @vprintf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.3, i64 0, i64 0), i8* nonnull %14) #3, !dbg !683
  br label %return

return:                                           ; preds = %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry, %if.then10, %if.then16, %if.else18, %if.then12
  ret void, !dbg !684
}

declare i32 @vprintf(i8*, i8*) local_unnamed_addr

; Function Attrs: nounwind
define void @_Z10storeLinesPvssi(i8* %p, i16 signext %size, i16 signext %line, i32 %op) local_unnamed_addr #0 !dbg !685 {
entry:
  %tmp = alloca %printf_args.3, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !689, metadata !587), !dbg !696
  tail call void @llvm.dbg.value(metadata i16 %size, i64 0, metadata !690, metadata !587), !dbg !697
  tail call void @llvm.dbg.value(metadata i16 %line, i64 0, metadata !691, metadata !587), !dbg !698
  tail call void @llvm.dbg.value(metadata i32 %op, i64 0, metadata !692, metadata !587), !dbg !699
  tail call void @llvm.dbg.value(metadata i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), i64 0, metadata !700, metadata !587), !dbg !707
  tail call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !706, metadata !587), !dbg !709
  tail call void @llvm.dbg.value(metadata i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), i64 0, metadata !710, metadata !587), !dbg !714
  tail call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !713, metadata !587), !dbg !716
  %0 = atomicrmw add i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), i32 1 seq_cst, !dbg !717
  tail call void @llvm.dbg.value(metadata i32 %0, i64 0, metadata !693, metadata !587), !dbg !718
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !694, metadata !587), !dbg !719
  tail call void @llvm.dbg.value(metadata i64* addrspacecast (i64 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 0, metadata !695, metadata !587), !dbg !720
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !721, !range !660
  %conv = trunc i32 %1 to i16, !dbg !723
  %mul = mul nsw i32 %0, 12, !dbg !724
  %idxprom = sext i32 %mul to i64, !dbg !725
  %arrayidx = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom, !dbg !725
  store i16 %conv, i16* %arrayidx, align 2, !dbg !726, !tbaa !727
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !731, !range !660
  %conv3 = trunc i32 %2 to i16, !dbg !733
  %add5 = or i32 %mul, 1, !dbg !734
  %idxprom6 = sext i32 %add5 to i64, !dbg !735
  %arrayidx7 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom6, !dbg !735
  store i16 %conv3, i16* %arrayidx7, align 2, !dbg !736, !tbaa !727
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !737, !range !628
  %conv9 = trunc i32 %3 to i16, !dbg !739
  %add11 = or i32 %mul, 2, !dbg !740
  %idxprom12 = sext i32 %add11 to i64, !dbg !741
  %arrayidx13 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom12, !dbg !741
  store i16 %conv9, i16* %arrayidx13, align 2, !dbg !742, !tbaa !727
  %4 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !743, !range !628
  %conv15 = trunc i32 %4 to i16, !dbg !745
  %add17 = or i32 %mul, 3, !dbg !746
  %idxprom18 = sext i32 %add17 to i64, !dbg !747
  %arrayidx19 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom18, !dbg !747
  store i16 %conv15, i16* %arrayidx19, align 2, !dbg !748, !tbaa !727
  %5 = ptrtoint i8* %p to i64, !dbg !749
  %mul20 = mul nsw i32 %0, 3, !dbg !750
  %add21 = add nsw i32 %mul20, 1, !dbg !751
  %idxprom22 = sext i32 %add21 to i64, !dbg !752
  %arrayidx23 = getelementptr inbounds i64, i64* addrspacecast (i64 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 %idxprom22, !dbg !752
  store i64 %5, i64* %arrayidx23, align 8, !dbg !753, !tbaa !754
  %add25 = add nsw i32 %mul, 8, !dbg !756
  %idxprom26 = sext i32 %add25 to i64, !dbg !757
  %arrayidx27 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom26, !dbg !757
  store i16 %size, i16* %arrayidx27, align 2, !dbg !758, !tbaa !727
  %add29 = add nsw i32 %mul, 9, !dbg !759
  %idxprom30 = sext i32 %add29 to i64, !dbg !760
  %arrayidx31 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom30, !dbg !760
  store i16 %line, i16* %arrayidx31, align 2, !dbg !761, !tbaa !727
  %mul32 = mul nsw i32 %0, 6, !dbg !762
  %add33 = add nsw i32 %mul32, 5, !dbg !763
  %idxprom34 = sext i32 %add33 to i64, !dbg !764
  %arrayidx3552 = getelementptr inbounds [25165824 x i32], [25165824 x i32] addrspace(1)* @buffer_oN_DeViCe, i64 0, i64 %idxprom34, !dbg !764
  %arrayidx35 = addrspacecast i32 addrspace(1)* %arrayidx3552 to i32*, !dbg !764
  store i32 %op, i32* %arrayidx35, align 4, !dbg !765, !tbaa !766
  %cmp = icmp slt i32 %0, 5, !dbg !768
  br i1 %cmp, label %if.then, label %if.end, !dbg !770

if.then:                                          ; preds = %entry
  %6 = getelementptr inbounds %printf_args.3, %printf_args.3* %tmp, i64 0, i32 0, !dbg !771
  store i32* getelementptr ([25165824 x i32], [25165824 x i32]* addrspacecast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to [25165824 x i32]*), i64 0, i64 0), i32** %6, align 8, !dbg !771
  %7 = bitcast %printf_args.3* %tmp to i8*, !dbg !771
  %8 = call i32 @vprintf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i64 0, i64 0), i8* nonnull %7) #3, !dbg !771
  br label %if.end, !dbg !771

if.end:                                           ; preds = %if.then, %entry
  ret void, !dbg !772
}

; Function Attrs: nounwind
define void @_Z9dumpLinesv() local_unnamed_addr #0 !dbg !773 {
entry:
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !776, !range !628
  %cmp = icmp eq i32 %0, 0, !dbg !779
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !780

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !781, !range !660
  %cmp2 = icmp eq i32 %1, 0, !dbg !784
  br i1 %cmp2, label %lor.lhs.false3, label %return, !dbg !785

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !786, !range !628
  %cmp5 = icmp eq i32 %2, 0, !dbg !789
  br i1 %cmp5, label %lor.lhs.false6, label %return, !dbg !790

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !791, !range !660
  %cmp8 = icmp eq i32 %3, 0, !dbg !794
  br i1 %cmp8, label %for.cond.preheader, label %return, !dbg !795

for.cond.preheader:                               ; preds = %lor.lhs.false6
  %4 = tail call i32 @vprintf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.5, i64 0, i64 0), i8* null) #3, !dbg !797
  br label %return, !dbg !798

return:                                           ; preds = %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry, %for.cond.preheader
  ret void, !dbg !799
}

; Function Attrs: nounwind
define void @_Z6print1i(i32 %a) local_unnamed_addr #0 !dbg !801 {
entry:
  %tmp = alloca %printf_args.4, align 8
  %tmp18 = alloca %printf_args.5, align 8
  tail call void @llvm.dbg.value(metadata i32 %a, i64 0, metadata !803, metadata !587), !dbg !804
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !805, !range !628
  %cmp = icmp eq i32 %0, 0, !dbg !808
  br i1 %cmp, label %lor.lhs.false, label %if.end21, !dbg !809

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !810, !range !628
  %cmp2 = icmp eq i32 %1, 0, !dbg !813
  br i1 %cmp2, label %if.end, label %if.end21, !dbg !814

if.end:                                           ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !816, !range !660
  %cmp4 = icmp eq i32 %2, 0, !dbg !819
  br i1 %cmp4, label %lor.lhs.false5, label %if.end21, !dbg !820

lor.lhs.false5:                                   ; preds = %if.end
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !821, !range !660
  %cmp7 = icmp eq i32 %3, 0, !dbg !824
  br i1 %cmp7, label %if.end9, label %if.end21, !dbg !825

if.end9:                                          ; preds = %lor.lhs.false5
  switch i32 %a, label %if.else19 [
    i32 1, label %if.then11
    i32 2, label %if.then15
  ], !dbg !826

if.then11:                                        ; preds = %if.end9
  %4 = getelementptr inbounds %printf_args.4, %printf_args.4* %tmp, i64 0, i32 0, !dbg !827
  store i32 0, i32* %4, align 8, !dbg !827
  %5 = getelementptr inbounds %printf_args.4, %printf_args.4* %tmp, i64 0, i32 1, !dbg !827
  store i32 0, i32* %5, align 4, !dbg !827
  %6 = bitcast %printf_args.4* %tmp to i8*, !dbg !827
  %7 = call i32 @vprintf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.6, i64 0, i64 0), i8* nonnull %6) #3, !dbg !829
  br label %if.end21, !dbg !827

if.then15:                                        ; preds = %if.end9
  %8 = getelementptr inbounds %printf_args.5, %printf_args.5* %tmp18, i64 0, i32 0, !dbg !831
  store i32 0, i32* %8, align 8, !dbg !831
  %9 = getelementptr inbounds %printf_args.5, %printf_args.5* %tmp18, i64 0, i32 1, !dbg !831
  store i32 0, i32* %9, align 4, !dbg !831
  %10 = bitcast %printf_args.5* %tmp18 to i8*, !dbg !831
  %11 = call i32 @vprintf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %10) #3, !dbg !833
  br label %if.end21, !dbg !831

if.else19:                                        ; preds = %if.end9
  %12 = tail call i32 @vprintf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.8, i64 0, i64 0), i8* null) #3, !dbg !835
  br label %if.end21

if.end21:                                         ; preds = %lor.lhs.false5, %if.end, %lor.lhs.false, %entry, %if.then15, %if.else19, %if.then11
  ret void, !dbg !836
}

; Function Attrs: nounwind
define void @_Z6print2v() local_unnamed_addr #0 !dbg !837 {
entry:
  %tmp = alloca %printf_args.6, align 8
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !838, !range !628
  %cmp = icmp eq i32 %0, 0, !dbg !841
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !842

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !843, !range !628
  %cmp2 = icmp eq i32 %1, 0, !dbg !846
  br i1 %cmp2, label %if.end, label %return, !dbg !847

if.end:                                           ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !849, !range !660
  %cmp4 = icmp eq i32 %2, 0, !dbg !852
  br i1 %cmp4, label %lor.lhs.false5, label %return, !dbg !853

lor.lhs.false5:                                   ; preds = %if.end
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !854, !range !660
  %cmp7 = icmp eq i32 %3, 0, !dbg !857
  br i1 %cmp7, label %if.end9, label %return, !dbg !858

if.end9:                                          ; preds = %lor.lhs.false5
  %4 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 0, !dbg !859
  store i32 0, i32* %4, align 8, !dbg !859
  %5 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 1, !dbg !859
  store i32 0, i32* %5, align 4, !dbg !859
  %6 = bitcast %printf_args.6* %tmp to i8*, !dbg !859
  %7 = call i32 @vprintf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %6) #3, !dbg !860
  br label %return, !dbg !862

return:                                           ; preds = %lor.lhs.false5, %if.end, %lor.lhs.false, %entry, %if.end9
  ret void, !dbg !863
}

; Function Attrs: nounwind
define void @_Z6print3i(i32 %a) local_unnamed_addr #0 !dbg !864 {
entry:
  %tmp = alloca %printf_args.7, align 8
  tail call void @llvm.dbg.value(metadata i32 %a, i64 0, metadata !866, metadata !587), !dbg !867
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !868, !range !628
  %cmp = icmp eq i32 %0, 0, !dbg !871
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !872

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !873, !range !628
  %cmp2 = icmp eq i32 %1, 0, !dbg !876
  br i1 %cmp2, label %if.end, label %return, !dbg !877

if.end:                                           ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !879, !range !660
  %cmp4 = icmp eq i32 %2, 0, !dbg !882
  br i1 %cmp4, label %lor.lhs.false5, label %return, !dbg !883

lor.lhs.false5:                                   ; preds = %if.end
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !884, !range !660
  %cmp7 = icmp eq i32 %3, 0, !dbg !887
  br i1 %cmp7, label %if.end9, label %return, !dbg !888

if.end9:                                          ; preds = %lor.lhs.false5
  %4 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp, i64 0, i32 0, !dbg !889
  store i32 %a, i32* %4, align 8, !dbg !889
  %5 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp, i64 0, i32 1, !dbg !889
  store i32 0, i32* %5, align 4, !dbg !889
  %6 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp, i64 0, i32 2, !dbg !889
  store i32 0, i32* %6, align 8, !dbg !889
  %7 = bitcast %printf_args.7* %tmp to i8*, !dbg !889
  %8 = call i32 @vprintf(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.9, i64 0, i64 0), i8* nonnull %7) #3, !dbg !890
  br label %return, !dbg !892

return:                                           ; preds = %lor.lhs.false5, %if.end, %lor.lhs.false, %entry, %if.end9
  ret void, !dbg !893
}

; Function Attrs: nounwind
define void @_Z6print5Pviii(i8* %p, i32 %bits, i32 %line, i32 %op) local_unnamed_addr #0 !dbg !894 {
entry:
  %tmp.i = alloca %printf_args.3, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !898, metadata !587), !dbg !902
  tail call void @llvm.dbg.value(metadata i32 %bits, i64 0, metadata !899, metadata !587), !dbg !903
  tail call void @llvm.dbg.value(metadata i32 %line, i64 0, metadata !900, metadata !587), !dbg !904
  tail call void @llvm.dbg.value(metadata i32 %op, i64 0, metadata !901, metadata !587), !dbg !905
  %div = sdiv i32 %bits, 8, !dbg !906
  %conv = trunc i32 %div to i16, !dbg !907
  %conv1 = trunc i32 %line to i16, !dbg !908
  %0 = bitcast %printf_args.3* %tmp.i to i8*, !dbg !909
  call void @llvm.lifetime.start(i64 8, i8* nonnull %0), !dbg !909
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !689, metadata !587) #3, !dbg !909
  tail call void @llvm.dbg.value(metadata i16 %conv, i64 0, metadata !690, metadata !587) #3, !dbg !911
  tail call void @llvm.dbg.value(metadata i16 %conv1, i64 0, metadata !691, metadata !587) #3, !dbg !912
  tail call void @llvm.dbg.value(metadata i32 %op, i64 0, metadata !692, metadata !587) #3, !dbg !913
  tail call void @llvm.dbg.value(metadata i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), i64 0, metadata !700, metadata !587) #3, !dbg !914
  tail call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !706, metadata !587) #3, !dbg !916
  tail call void @llvm.dbg.value(metadata i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), i64 0, metadata !710, metadata !587) #3, !dbg !917
  tail call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !713, metadata !587) #3, !dbg !919
  %1 = atomicrmw add i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), i32 1 seq_cst, !dbg !920
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !693, metadata !587) #3, !dbg !921
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !694, metadata !587) #3, !dbg !922
  tail call void @llvm.dbg.value(metadata i64* addrspacecast (i64 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 0, metadata !695, metadata !587) #3, !dbg !923
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !924, !range !660
  %conv.i = trunc i32 %2 to i16, !dbg !926
  %mul.i = mul nsw i32 %1, 12, !dbg !927
  %idxprom.i = sext i32 %mul.i to i64, !dbg !928
  %arrayidx.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom.i, !dbg !928
  store i16 %conv.i, i16* %arrayidx.i, align 2, !dbg !929, !tbaa !727
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !930, !range !660
  %conv3.i = trunc i32 %3 to i16, !dbg !932
  %add5.i = or i32 %mul.i, 1, !dbg !933
  %idxprom6.i = sext i32 %add5.i to i64, !dbg !934
  %arrayidx7.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom6.i, !dbg !934
  store i16 %conv3.i, i16* %arrayidx7.i, align 2, !dbg !935, !tbaa !727
  %4 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !936, !range !628
  %conv9.i = trunc i32 %4 to i16, !dbg !938
  %add11.i = or i32 %mul.i, 2, !dbg !939
  %idxprom12.i = sext i32 %add11.i to i64, !dbg !940
  %arrayidx13.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom12.i, !dbg !940
  store i16 %conv9.i, i16* %arrayidx13.i, align 2, !dbg !941, !tbaa !727
  %5 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !942, !range !628
  %conv15.i = trunc i32 %5 to i16, !dbg !944
  %add17.i = or i32 %mul.i, 3, !dbg !945
  %idxprom18.i = sext i32 %add17.i to i64, !dbg !946
  %arrayidx19.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom18.i, !dbg !946
  store i16 %conv15.i, i16* %arrayidx19.i, align 2, !dbg !947, !tbaa !727
  %6 = ptrtoint i8* %p to i64, !dbg !948
  %mul20.i = mul nsw i32 %1, 3, !dbg !949
  %add21.i = add nsw i32 %mul20.i, 1, !dbg !950
  %idxprom22.i = sext i32 %add21.i to i64, !dbg !951
  %arrayidx23.i = getelementptr inbounds i64, i64* addrspacecast (i64 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 %idxprom22.i, !dbg !951
  store i64 %6, i64* %arrayidx23.i, align 8, !dbg !952, !tbaa !754
  %add25.i = add nsw i32 %mul.i, 8, !dbg !953
  %idxprom26.i = sext i32 %add25.i to i64, !dbg !954
  %arrayidx27.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom26.i, !dbg !954
  store i16 %conv, i16* %arrayidx27.i, align 2, !dbg !955, !tbaa !727
  %add29.i = add nsw i32 %mul.i, 9, !dbg !956
  %idxprom30.i = sext i32 %add29.i to i64, !dbg !957
  %arrayidx31.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom30.i, !dbg !957
  store i16 %conv1, i16* %arrayidx31.i, align 2, !dbg !958, !tbaa !727
  %mul32.i = mul nsw i32 %1, 6, !dbg !959
  %add33.i = add nsw i32 %mul32.i, 5, !dbg !960
  %idxprom34.i = sext i32 %add33.i to i64, !dbg !961
  %arrayidx3552.i = getelementptr inbounds [25165824 x i32], [25165824 x i32] addrspace(1)* @buffer_oN_DeViCe, i64 0, i64 %idxprom34.i, !dbg !961
  %arrayidx35.i = addrspacecast i32 addrspace(1)* %arrayidx3552.i to i32*, !dbg !961
  store i32 %op, i32* %arrayidx35.i, align 4, !dbg !962, !tbaa !766
  %cmp.i = icmp slt i32 %1, 5, !dbg !963
  br i1 %cmp.i, label %if.then.i, label %_Z10storeLinesPvssi.exit, !dbg !964

if.then.i:                                        ; preds = %entry
  %7 = getelementptr inbounds %printf_args.3, %printf_args.3* %tmp.i, i64 0, i32 0, !dbg !965
  store i32* getelementptr ([25165824 x i32], [25165824 x i32]* addrspacecast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to [25165824 x i32]*), i64 0, i64 0), i32** %7, align 8, !dbg !965
  %8 = call i32 @vprintf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i64 0, i64 0), i8* nonnull %0) #3, !dbg !965
  br label %_Z10storeLinesPvssi.exit, !dbg !965

_Z10storeLinesPvssi.exit:                         ; preds = %entry, %if.then.i
  call void @llvm.lifetime.end(i64 8, i8* nonnull %0), !dbg !966
  ret void, !dbg !967
}

; Function Attrs: nounwind
define void @RetKernel() local_unnamed_addr #0 !dbg !968 {
entry:
  %tmp = alloca %printf_args.8, align 8
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !971, !range !628
  %cmp = icmp eq i32 %0, 0, !dbg !974
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !975

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !976, !range !660
  %cmp2 = icmp eq i32 %1, 0, !dbg !979
  br i1 %cmp2, label %lor.lhs.false3, label %return, !dbg !980

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !981, !range !628
  %cmp5 = icmp eq i32 %2, 0, !dbg !984
  br i1 %cmp5, label %lor.lhs.false6, label %return, !dbg !985

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !986, !range !660
  %cmp8 = icmp eq i32 %3, 0, !dbg !989
  br i1 %cmp8, label %if.end, label %return, !dbg !990

if.end:                                           ; preds = %lor.lhs.false6
  %4 = load i32, i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), align 4, !dbg !992, !tbaa !766
  store i32 %4, i32* getelementptr inbounds ([25165824 x i32], [25165824 x i32]* addrspacecast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to [25165824 x i32]*), i64 0, i64 5), align 4, !dbg !993, !tbaa !766
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !970, metadata !587), !dbg !994
  %5 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #3, !dbg !995, !range !1039
  %conv = trunc i32 %5 to i16, !dbg !1040
  store i16 %conv, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), align 4, !dbg !1041, !tbaa !727
  %6 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.y() #3, !dbg !1042, !range !1039
  %conv11 = trunc i32 %6 to i16, !dbg !1045
  store i16 %conv11, i16* getelementptr (i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 1), align 2, !dbg !1046, !tbaa !727
  %7 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #3, !dbg !1047, !range !1075
  %conv14 = trunc i32 %7 to i16, !dbg !1076
  store i16 %conv14, i16* bitcast (i32* getelementptr ([25165824 x i32], [25165824 x i32]* addrspacecast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to [25165824 x i32]*), i64 0, i64 1) to i16*), align 2, !dbg !1077, !tbaa !727
  %8 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.y() #3, !dbg !1078, !range !1075
  %conv17 = trunc i32 %8 to i16, !dbg !1081
  store i16 %conv17, i16* getelementptr (i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 3), align 2, !dbg !1082, !tbaa !727
  %9 = getelementptr inbounds %printf_args.8, %printf_args.8* %tmp, i64 0, i32 0, !dbg !1083
  store i32 %4, i32* %9, align 8, !dbg !1083
  %10 = bitcast %printf_args.8* %tmp to i8*, !dbg !1083
  %11 = call i32 @vprintf(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.10, i64 0, i64 0), i8* nonnull %10) #3, !dbg !1083
  store i32 1, i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), align 4, !dbg !1084, !tbaa !766
  br label %return, !dbg !1085

return:                                           ; preds = %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry, %if.end
  ret void, !dbg !1086
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.y() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.y() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.y() #1

; Function Attrs: nounwind readnone
declare void @llvm.dbg.value(metadata, i64, metadata, metadata) #1

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #2

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { argmemonly nounwind }
attributes #3 = { nounwind }

!llvm.dbg.cu = !{!2}
!llvm.module.flags = !{!572, !573, !574}
!llvm.ident = !{!575}
!nvvm.internalize.after.link = !{}
!nvvmir.version = !{!576}
!nvvm.annotations = !{!577, !578, !577, !579, !579, !579, !579, !580, !580, !579}

!0 = !DIGlobalVariableExpression(var: !1)
!1 = distinct !DIGlobalVariable(name: "ccnntt", scope: !2, file: !3, line: 25, type: !8, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C_plus_plus, file: !3, producer: "clang version 5.0.0 (trunk 294196)", isOptimized: true, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, retainedTypes: !5, globals: !15, imports: !16)
!3 = !DIFile(filename: "/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/ansf.cu", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!4 = !{}
!5 = !{!6, !8, !9, !11, !12, !10, !13}
!6 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !7, size: 64)
!7 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!8 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!9 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !10, size: 64)
!10 = !DIBasicType(name: "short", size: 16, encoding: DW_ATE_signed)
!11 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !12, size: 64)
!12 = !DIBasicType(name: "long int", size: 64, encoding: DW_ATE_signed)
!13 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !14, size: 64)
!14 = !DIDerivedType(tag: DW_TAG_volatile_type, baseType: !8)
!15 = !{!0}
!16 = !{!17, !24, !29, !31, !33, !35, !37, !41, !43, !45, !47, !49, !51, !53, !55, !57, !59, !61, !63, !65, !67, !69, !73, !75, !77, !79, !83, !88, !90, !92, !97, !101, !103, !105, !107, !109, !111, !113, !115, !117, !121, !125, !127, !129, !133, !135, !137, !139, !141, !143, !147, !149, !151, !156, !163, !167, !169, !171, !175, !177, !179, !183, !185, !187, !191, !193, !195, !197, !199, !201, !203, !205, !207, !209, !214, !216, !218, !222, !224, !226, !228, !230, !232, !234, !236, !240, !244, !246, !248, !253, !255, !257, !259, !261, !263, !265, !269, !275, !279, !283, !288, !291, !295, !299, !314, !318, !322, !326, !330, !334, !336, !340, !344, !348, !356, !360, !364, !368, !372, !377, !383, !387, !391, !393, !401, !405, !413, !415, !417, !421, !425, !429, !434, !438, !443, !444, !445, !446, !449, !450, !451, !452, !453, !454, !455, !458, !460, !462, !464, !466, !468, !470, !472, !475, !477, !479, !481, !483, !485, !487, !489, !491, !493, !495, !497, !499, !501, !503, !505, !507, !509, !511, !513, !515, !517, !519, !521, !523, !525, !527, !529, !531, !533, !535, !537, !539, !543, !544, !546, !548, !550, !552, !554, !556, !558, !560, !562, !564, !566, !568, !570}
!17 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !20, line: 201)
!18 = !DINamespace(name: "std", scope: null, file: !19, line: 195)
!19 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_math_forward_declares.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!20 = !DISubprogram(name: "abs", linkageName: "_ZL3absx", scope: !19, file: !19, line: 44, type: !21, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!21 = !DISubroutineType(types: !22)
!22 = !{!23, !23}
!23 = !DIBasicType(name: "long long int", size: 64, encoding: DW_ATE_signed)
!24 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !25, line: 202)
!25 = !DISubprogram(name: "acos", linkageName: "_ZL4acosf", scope: !19, file: !19, line: 46, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!26 = !DISubroutineType(types: !27)
!27 = !{!28, !28}
!28 = !DIBasicType(name: "float", size: 32, encoding: DW_ATE_float)
!29 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !30, line: 203)
!30 = !DISubprogram(name: "acosh", linkageName: "_ZL5acoshf", scope: !19, file: !19, line: 48, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!31 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !32, line: 204)
!32 = !DISubprogram(name: "asin", linkageName: "_ZL4asinf", scope: !19, file: !19, line: 50, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!33 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !34, line: 205)
!34 = !DISubprogram(name: "asinh", linkageName: "_ZL5asinhf", scope: !19, file: !19, line: 52, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!35 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !36, line: 206)
!36 = !DISubprogram(name: "atan", linkageName: "_ZL4atanf", scope: !19, file: !19, line: 56, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!37 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !38, line: 207)
!38 = !DISubprogram(name: "atan2", linkageName: "_ZL5atan2ff", scope: !19, file: !19, line: 54, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!39 = !DISubroutineType(types: !40)
!40 = !{!28, !28, !28}
!41 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !42, line: 208)
!42 = !DISubprogram(name: "atanh", linkageName: "_ZL5atanhf", scope: !19, file: !19, line: 58, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!43 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !44, line: 209)
!44 = !DISubprogram(name: "cbrt", linkageName: "_ZL4cbrtf", scope: !19, file: !19, line: 60, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!45 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !46, line: 210)
!46 = !DISubprogram(name: "ceil", linkageName: "_ZL4ceilf", scope: !19, file: !19, line: 62, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!47 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !48, line: 211)
!48 = !DISubprogram(name: "copysign", linkageName: "_ZL8copysignff", scope: !19, file: !19, line: 64, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!49 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !50, line: 212)
!50 = !DISubprogram(name: "cos", linkageName: "_ZL3cosf", scope: !19, file: !19, line: 66, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!51 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !52, line: 213)
!52 = !DISubprogram(name: "cosh", linkageName: "_ZL4coshf", scope: !19, file: !19, line: 68, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!53 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !54, line: 214)
!54 = !DISubprogram(name: "erf", linkageName: "_ZL3erff", scope: !19, file: !19, line: 72, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!55 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !56, line: 215)
!56 = !DISubprogram(name: "erfc", linkageName: "_ZL4erfcf", scope: !19, file: !19, line: 70, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!57 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !58, line: 216)
!58 = !DISubprogram(name: "exp", linkageName: "_ZL3expf", scope: !19, file: !19, line: 76, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!59 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !60, line: 217)
!60 = !DISubprogram(name: "exp2", linkageName: "_ZL4exp2f", scope: !19, file: !19, line: 74, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!61 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !62, line: 218)
!62 = !DISubprogram(name: "expm1", linkageName: "_ZL5expm1f", scope: !19, file: !19, line: 78, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!63 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !64, line: 219)
!64 = !DISubprogram(name: "fabs", linkageName: "_ZL4fabsf", scope: !19, file: !19, line: 80, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!65 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !66, line: 220)
!66 = !DISubprogram(name: "fdim", linkageName: "_ZL4fdimff", scope: !19, file: !19, line: 82, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!67 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !68, line: 221)
!68 = !DISubprogram(name: "floor", linkageName: "_ZL5floorf", scope: !19, file: !19, line: 84, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!69 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !70, line: 222)
!70 = !DISubprogram(name: "fma", linkageName: "_ZL3fmafff", scope: !19, file: !19, line: 86, type: !71, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!71 = !DISubroutineType(types: !72)
!72 = !{!28, !28, !28, !28}
!73 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !74, line: 223)
!74 = !DISubprogram(name: "fmax", linkageName: "_ZL4fmaxff", scope: !19, file: !19, line: 88, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!75 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !76, line: 224)
!76 = !DISubprogram(name: "fmin", linkageName: "_ZL4fminff", scope: !19, file: !19, line: 90, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!77 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !78, line: 225)
!78 = !DISubprogram(name: "fmod", linkageName: "_ZL4fmodff", scope: !19, file: !19, line: 92, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!79 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !80, line: 226)
!80 = !DISubprogram(name: "fpclassify", linkageName: "_ZL10fpclassifyf", scope: !19, file: !19, line: 94, type: !81, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!81 = !DISubroutineType(types: !82)
!82 = !{!8, !28}
!83 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !84, line: 227)
!84 = !DISubprogram(name: "frexp", linkageName: "_ZL5frexpfPi", scope: !19, file: !19, line: 96, type: !85, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!85 = !DISubroutineType(types: !86)
!86 = !{!28, !28, !87}
!87 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !8, size: 64)
!88 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !89, line: 228)
!89 = !DISubprogram(name: "hypot", linkageName: "_ZL5hypotff", scope: !19, file: !19, line: 98, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!90 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !91, line: 229)
!91 = !DISubprogram(name: "ilogb", linkageName: "_ZL5ilogbf", scope: !19, file: !19, line: 100, type: !81, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!92 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !93, line: 230)
!93 = !DISubprogram(name: "isfinite", linkageName: "_ZL8isfinitef", scope: !19, file: !19, line: 102, type: !94, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!94 = !DISubroutineType(types: !95)
!95 = !{!96, !28}
!96 = !DIBasicType(name: "bool", size: 8, encoding: DW_ATE_boolean)
!97 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !98, line: 231)
!98 = !DISubprogram(name: "isgreater", linkageName: "_ZL9isgreaterff", scope: !19, file: !19, line: 106, type: !99, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!99 = !DISubroutineType(types: !100)
!100 = !{!96, !28, !28}
!101 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !102, line: 232)
!102 = !DISubprogram(name: "isgreaterequal", linkageName: "_ZL14isgreaterequalff", scope: !19, file: !19, line: 105, type: !99, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!103 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !104, line: 233)
!104 = !DISubprogram(name: "isinf", linkageName: "_ZL5isinff", scope: !19, file: !19, line: 108, type: !94, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!105 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !106, line: 234)
!106 = !DISubprogram(name: "isless", linkageName: "_ZL6islessff", scope: !19, file: !19, line: 112, type: !99, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!107 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !108, line: 235)
!108 = !DISubprogram(name: "islessequal", linkageName: "_ZL11islessequalff", scope: !19, file: !19, line: 111, type: !99, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!109 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !110, line: 236)
!110 = !DISubprogram(name: "islessgreater", linkageName: "_ZL13islessgreaterff", scope: !19, file: !19, line: 114, type: !99, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!111 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !112, line: 237)
!112 = !DISubprogram(name: "isnan", linkageName: "_ZL5isnanf", scope: !19, file: !19, line: 116, type: !94, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!113 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !114, line: 238)
!114 = !DISubprogram(name: "isnormal", linkageName: "_ZL8isnormalf", scope: !19, file: !19, line: 118, type: !94, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!115 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !116, line: 239)
!116 = !DISubprogram(name: "isunordered", linkageName: "_ZL11isunorderedff", scope: !19, file: !19, line: 120, type: !99, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!117 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !118, line: 240)
!118 = !DISubprogram(name: "labs", linkageName: "_ZL4labsl", scope: !19, file: !19, line: 121, type: !119, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!119 = !DISubroutineType(types: !120)
!120 = !{!12, !12}
!121 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !122, line: 241)
!122 = !DISubprogram(name: "ldexp", linkageName: "_ZL5ldexpfi", scope: !19, file: !19, line: 123, type: !123, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!123 = !DISubroutineType(types: !124)
!124 = !{!28, !28, !8}
!125 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !126, line: 242)
!126 = !DISubprogram(name: "lgamma", linkageName: "_ZL6lgammaf", scope: !19, file: !19, line: 125, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!127 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !128, line: 243)
!128 = !DISubprogram(name: "llabs", linkageName: "_ZL5llabsx", scope: !19, file: !19, line: 126, type: !21, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!129 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !130, line: 244)
!130 = !DISubprogram(name: "llrint", linkageName: "_ZL6llrintf", scope: !19, file: !19, line: 128, type: !131, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!131 = !DISubroutineType(types: !132)
!132 = !{!23, !28}
!133 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !134, line: 245)
!134 = !DISubprogram(name: "log", linkageName: "_ZL3logf", scope: !19, file: !19, line: 138, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!135 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !136, line: 246)
!136 = !DISubprogram(name: "log10", linkageName: "_ZL5log10f", scope: !19, file: !19, line: 130, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!137 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !138, line: 247)
!138 = !DISubprogram(name: "log1p", linkageName: "_ZL5log1pf", scope: !19, file: !19, line: 132, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!139 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !140, line: 248)
!140 = !DISubprogram(name: "log2", linkageName: "_ZL4log2f", scope: !19, file: !19, line: 134, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!141 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !142, line: 249)
!142 = !DISubprogram(name: "logb", linkageName: "_ZL4logbf", scope: !19, file: !19, line: 136, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!143 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !144, line: 250)
!144 = !DISubprogram(name: "lrint", linkageName: "_ZL5lrintf", scope: !19, file: !19, line: 140, type: !145, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!145 = !DISubroutineType(types: !146)
!146 = !{!12, !28}
!147 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !148, line: 251)
!148 = !DISubprogram(name: "lround", linkageName: "_ZL6lroundf", scope: !19, file: !19, line: 142, type: !145, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!149 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !150, line: 252)
!150 = !DISubprogram(name: "llround", linkageName: "_ZL7llroundf", scope: !19, file: !19, line: 143, type: !131, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!151 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !152, line: 253)
!152 = !DISubprogram(name: "modf", linkageName: "_ZL4modffPf", scope: !19, file: !19, line: 145, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!153 = !DISubroutineType(types: !154)
!154 = !{!28, !28, !155}
!155 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !28, size: 64)
!156 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !157, line: 254)
!157 = !DISubprogram(name: "nan", linkageName: "_ZL3nanPKc", scope: !19, file: !19, line: 146, type: !158, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!158 = !DISubroutineType(types: !159)
!159 = !{!160, !161}
!160 = !DIBasicType(name: "double", size: 64, encoding: DW_ATE_float)
!161 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !162, size: 64)
!162 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !7)
!163 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !164, line: 255)
!164 = !DISubprogram(name: "nanf", linkageName: "_ZL4nanfPKc", scope: !19, file: !19, line: 147, type: !165, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!165 = !DISubroutineType(types: !166)
!166 = !{!28, !161}
!167 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !168, line: 256)
!168 = !DISubprogram(name: "nearbyint", linkageName: "_ZL9nearbyintf", scope: !19, file: !19, line: 149, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!169 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !170, line: 257)
!170 = !DISubprogram(name: "nextafter", linkageName: "_ZL9nextafterff", scope: !19, file: !19, line: 151, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!171 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !172, line: 258)
!172 = !DISubprogram(name: "nexttoward", linkageName: "_ZL10nexttowardfd", scope: !19, file: !19, line: 153, type: !173, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!173 = !DISubroutineType(types: !174)
!174 = !{!28, !28, !160}
!175 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !176, line: 259)
!176 = !DISubprogram(name: "pow", linkageName: "_ZL3powfi", scope: !19, file: !19, line: 158, type: !123, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!177 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !178, line: 260)
!178 = !DISubprogram(name: "remainder", linkageName: "_ZL9remainderff", scope: !19, file: !19, line: 160, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!179 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !180, line: 261)
!180 = !DISubprogram(name: "remquo", linkageName: "_ZL6remquoffPi", scope: !19, file: !19, line: 162, type: !181, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!181 = !DISubroutineType(types: !182)
!182 = !{!28, !28, !28, !87}
!183 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !184, line: 262)
!184 = !DISubprogram(name: "rint", linkageName: "_ZL4rintf", scope: !19, file: !19, line: 164, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!185 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !186, line: 263)
!186 = !DISubprogram(name: "round", linkageName: "_ZL5roundf", scope: !19, file: !19, line: 166, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!187 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !188, line: 264)
!188 = !DISubprogram(name: "scalbln", linkageName: "_ZL7scalblnfl", scope: !19, file: !19, line: 168, type: !189, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!189 = !DISubroutineType(types: !190)
!190 = !{!28, !28, !12}
!191 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !192, line: 265)
!192 = !DISubprogram(name: "scalbn", linkageName: "_ZL6scalbnfi", scope: !19, file: !19, line: 170, type: !123, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!193 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !194, line: 266)
!194 = !DISubprogram(name: "signbit", linkageName: "_ZL7signbitf", scope: !19, file: !19, line: 172, type: !94, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!195 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !196, line: 267)
!196 = !DISubprogram(name: "sin", linkageName: "_ZL3sinf", scope: !19, file: !19, line: 174, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!197 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !198, line: 268)
!198 = !DISubprogram(name: "sinh", linkageName: "_ZL4sinhf", scope: !19, file: !19, line: 176, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!199 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !200, line: 269)
!200 = !DISubprogram(name: "sqrt", linkageName: "_ZL4sqrtf", scope: !19, file: !19, line: 178, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!201 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !202, line: 270)
!202 = !DISubprogram(name: "tan", linkageName: "_ZL3tanf", scope: !19, file: !19, line: 180, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!203 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !204, line: 271)
!204 = !DISubprogram(name: "tanh", linkageName: "_ZL4tanhf", scope: !19, file: !19, line: 182, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!205 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !206, line: 272)
!206 = !DISubprogram(name: "tgamma", linkageName: "_ZL6tgammaf", scope: !19, file: !19, line: 184, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!207 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !208, line: 273)
!208 = !DISubprogram(name: "trunc", linkageName: "_ZL5truncf", scope: !19, file: !19, line: 186, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!209 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !210, line: 102)
!210 = !DISubprogram(name: "acos", scope: !211, file: !211, line: 54, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!211 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/mathcalls.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!212 = !DISubroutineType(types: !213)
!213 = !{!160, !160}
!214 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !215, line: 121)
!215 = !DISubprogram(name: "asin", scope: !211, file: !211, line: 56, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!216 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !217, line: 140)
!217 = !DISubprogram(name: "atan", scope: !211, file: !211, line: 58, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!218 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !219, line: 159)
!219 = !DISubprogram(name: "atan2", scope: !211, file: !211, line: 60, type: !220, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!220 = !DISubroutineType(types: !221)
!221 = !{!160, !160, !160}
!222 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !223, line: 180)
!223 = !DISubprogram(name: "ceil", scope: !211, file: !211, line: 178, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!224 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !225, line: 199)
!225 = !DISubprogram(name: "cos", scope: !211, file: !211, line: 63, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!226 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !227, line: 218)
!227 = !DISubprogram(name: "cosh", scope: !211, file: !211, line: 72, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!228 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !229, line: 237)
!229 = !DISubprogram(name: "exp", scope: !211, file: !211, line: 100, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!230 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !231, line: 256)
!231 = !DISubprogram(name: "fabs", scope: !211, file: !211, line: 181, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!232 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !233, line: 275)
!233 = !DISubprogram(name: "floor", scope: !211, file: !211, line: 184, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!234 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !235, line: 294)
!235 = !DISubprogram(name: "fmod", scope: !211, file: !211, line: 187, type: !220, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!236 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !237, line: 315)
!237 = !DISubprogram(name: "frexp", scope: !211, file: !211, line: 103, type: !238, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!238 = !DISubroutineType(types: !239)
!239 = !{!160, !160, !87}
!240 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !241, line: 334)
!241 = !DISubprogram(name: "ldexp", scope: !211, file: !211, line: 106, type: !242, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!242 = !DISubroutineType(types: !243)
!243 = !{!160, !160, !8}
!244 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !245, line: 353)
!245 = !DISubprogram(name: "log", scope: !211, file: !211, line: 109, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!246 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !247, line: 372)
!247 = !DISubprogram(name: "log10", scope: !211, file: !211, line: 112, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!248 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !249, line: 391)
!249 = !DISubprogram(name: "modf", scope: !211, file: !211, line: 115, type: !250, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!250 = !DISubroutineType(types: !251)
!251 = !{!160, !160, !252}
!252 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !160, size: 64)
!253 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !254, line: 403)
!254 = !DISubprogram(name: "pow", scope: !211, file: !211, line: 153, type: !220, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!255 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !256, line: 440)
!256 = !DISubprogram(name: "sin", scope: !211, file: !211, line: 65, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!257 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !258, line: 459)
!258 = !DISubprogram(name: "sinh", scope: !211, file: !211, line: 74, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!259 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !260, line: 478)
!260 = !DISubprogram(name: "sqrt", scope: !211, file: !211, line: 156, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!261 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !262, line: 497)
!262 = !DISubprogram(name: "tan", scope: !211, file: !211, line: 67, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!263 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !264, line: 516)
!264 = !DISubprogram(name: "tanh", scope: !211, file: !211, line: 76, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!265 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !266, line: 118)
!266 = !DIDerivedType(tag: DW_TAG_typedef, name: "div_t", file: !267, line: 101, baseType: !268)
!267 = !DIFile(filename: "/usr/include/stdlib.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!268 = !DICompositeType(tag: DW_TAG_structure_type, file: !267, line: 97, flags: DIFlagFwdDecl, identifier: "_ZTS5div_t")
!269 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !270, line: 119)
!270 = !DIDerivedType(tag: DW_TAG_typedef, name: "ldiv_t", file: !267, line: 109, baseType: !271)
!271 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !267, line: 105, size: 128, elements: !272, identifier: "_ZTS6ldiv_t")
!272 = !{!273, !274}
!273 = !DIDerivedType(tag: DW_TAG_member, name: "quot", scope: !271, file: !267, line: 107, baseType: !12, size: 64)
!274 = !DIDerivedType(tag: DW_TAG_member, name: "rem", scope: !271, file: !267, line: 108, baseType: !12, size: 64, offset: 64)
!275 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !276, line: 121)
!276 = !DISubprogram(name: "abort", scope: !267, file: !267, line: 515, type: !277, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!277 = !DISubroutineType(types: !278)
!278 = !{null}
!279 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !280, line: 122)
!280 = !DISubprogram(name: "abs", scope: !267, file: !267, line: 775, type: !281, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!281 = !DISubroutineType(types: !282)
!282 = !{!8, !8}
!283 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !284, line: 123)
!284 = !DISubprogram(name: "atexit", scope: !267, file: !267, line: 519, type: !285, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!285 = !DISubroutineType(types: !286)
!286 = !{!8, !287}
!287 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !277, size: 64)
!288 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !289, line: 129)
!289 = !DISubprogram(name: "atof", scope: !290, file: !290, line: 26, type: !158, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!290 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/stdlib-float.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!291 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !292, line: 130)
!292 = !DISubprogram(name: "atoi", scope: !267, file: !267, line: 278, type: !293, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!293 = !DISubroutineType(types: !294)
!294 = !{!8, !161}
!295 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !296, line: 131)
!296 = !DISubprogram(name: "atol", scope: !267, file: !267, line: 283, type: !297, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!297 = !DISubroutineType(types: !298)
!298 = !{!12, !161}
!299 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !300, line: 132)
!300 = !DISubprogram(name: "bsearch", scope: !301, file: !301, line: 20, type: !302, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!301 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/stdlib-bsearch.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!302 = !DISubroutineType(types: !303)
!303 = !{!304, !305, !305, !307, !307, !310}
!304 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 64)
!305 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !306, size: 64)
!306 = !DIDerivedType(tag: DW_TAG_const_type, baseType: null)
!307 = !DIDerivedType(tag: DW_TAG_typedef, name: "size_t", file: !308, line: 62, baseType: !309)
!308 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/stddef.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!309 = !DIBasicType(name: "long unsigned int", size: 64, encoding: DW_ATE_unsigned)
!310 = !DIDerivedType(tag: DW_TAG_typedef, name: "__compar_fn_t", file: !267, line: 742, baseType: !311)
!311 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !312, size: 64)
!312 = !DISubroutineType(types: !313)
!313 = !{!8, !305, !305}
!314 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !315, line: 133)
!315 = !DISubprogram(name: "calloc", scope: !267, file: !267, line: 468, type: !316, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!316 = !DISubroutineType(types: !317)
!317 = !{!304, !307, !307}
!318 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !319, line: 134)
!319 = !DISubprogram(name: "div", scope: !267, file: !267, line: 789, type: !320, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!320 = !DISubroutineType(types: !321)
!321 = !{!266, !8, !8}
!322 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !323, line: 135)
!323 = !DISubprogram(name: "exit", scope: !267, file: !267, line: 543, type: !324, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!324 = !DISubroutineType(types: !325)
!325 = !{null, !8}
!326 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !327, line: 136)
!327 = !DISubprogram(name: "free", scope: !267, file: !267, line: 483, type: !328, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!328 = !DISubroutineType(types: !329)
!329 = !{null, !304}
!330 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !331, line: 137)
!331 = !DISubprogram(name: "getenv", scope: !267, file: !267, line: 564, type: !332, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!332 = !DISubroutineType(types: !333)
!333 = !{!6, !161}
!334 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !335, line: 138)
!335 = !DISubprogram(name: "labs", scope: !267, file: !267, line: 776, type: !119, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!336 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !337, line: 139)
!337 = !DISubprogram(name: "ldiv", scope: !267, file: !267, line: 791, type: !338, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!338 = !DISubroutineType(types: !339)
!339 = !{!270, !12, !12}
!340 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !341, line: 140)
!341 = !DISubprogram(name: "malloc", scope: !267, file: !267, line: 466, type: !342, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!342 = !DISubroutineType(types: !343)
!343 = !{!304, !307}
!344 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !345, line: 142)
!345 = !DISubprogram(name: "mblen", scope: !267, file: !267, line: 863, type: !346, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!346 = !DISubroutineType(types: !347)
!347 = !{!8, !161, !307}
!348 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !349, line: 143)
!349 = !DISubprogram(name: "mbstowcs", scope: !267, file: !267, line: 874, type: !350, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!350 = !DISubroutineType(types: !351)
!351 = !{!307, !352, !355, !307}
!352 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !353)
!353 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !354, size: 64)
!354 = !DIBasicType(name: "wchar_t", size: 32, encoding: DW_ATE_signed)
!355 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !161)
!356 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !357, line: 144)
!357 = !DISubprogram(name: "mbtowc", scope: !267, file: !267, line: 866, type: !358, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!358 = !DISubroutineType(types: !359)
!359 = !{!8, !352, !355, !307}
!360 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !361, line: 146)
!361 = !DISubprogram(name: "qsort", scope: !267, file: !267, line: 765, type: !362, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!362 = !DISubroutineType(types: !363)
!363 = !{null, !304, !307, !307, !310}
!364 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !365, line: 152)
!365 = !DISubprogram(name: "rand", scope: !267, file: !267, line: 374, type: !366, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!366 = !DISubroutineType(types: !367)
!367 = !{!8}
!368 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !369, line: 153)
!369 = !DISubprogram(name: "realloc", scope: !267, file: !267, line: 480, type: !370, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!370 = !DISubroutineType(types: !371)
!371 = !{!304, !304, !307}
!372 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !373, line: 154)
!373 = !DISubprogram(name: "srand", scope: !267, file: !267, line: 376, type: !374, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!374 = !DISubroutineType(types: !375)
!375 = !{null, !376}
!376 = !DIBasicType(name: "unsigned int", size: 32, encoding: DW_ATE_unsigned)
!377 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !378, line: 155)
!378 = !DISubprogram(name: "strtod", scope: !267, file: !267, line: 164, type: !379, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!379 = !DISubroutineType(types: !380)
!380 = !{!160, !355, !381}
!381 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !382)
!382 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !6, size: 64)
!383 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !384, line: 156)
!384 = !DISubprogram(name: "strtol", scope: !267, file: !267, line: 183, type: !385, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!385 = !DISubroutineType(types: !386)
!386 = !{!12, !355, !381, !8}
!387 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !388, line: 157)
!388 = !DISubprogram(name: "strtoul", scope: !267, file: !267, line: 187, type: !389, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!389 = !DISubroutineType(types: !390)
!390 = !{!309, !355, !381, !8}
!391 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !392, line: 158)
!392 = !DISubprogram(name: "system", scope: !267, file: !267, line: 717, type: !293, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!393 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !394, line: 160)
!394 = !DISubprogram(name: "wcstombs", scope: !267, file: !267, line: 877, type: !395, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!395 = !DISubroutineType(types: !396)
!396 = !{!307, !397, !398, !307}
!397 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !6)
!398 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !399)
!399 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !400, size: 64)
!400 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !354)
!401 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !402, line: 161)
!402 = !DISubprogram(name: "wctomb", scope: !267, file: !267, line: 870, type: !403, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!403 = !DISubroutineType(types: !404)
!404 = !{!8, !6, !354}
!405 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !408, line: 201)
!406 = !DINamespace(name: "__gnu_cxx", scope: null, file: !407, line: 68)
!407 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/bits/cpp_type_traits.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!408 = !DIDerivedType(tag: DW_TAG_typedef, name: "lldiv_t", file: !267, line: 121, baseType: !409)
!409 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !267, line: 117, size: 128, elements: !410, identifier: "_ZTS7lldiv_t")
!410 = !{!411, !412}
!411 = !DIDerivedType(tag: DW_TAG_member, name: "quot", scope: !409, file: !267, line: 119, baseType: !23, size: 64)
!412 = !DIDerivedType(tag: DW_TAG_member, name: "rem", scope: !409, file: !267, line: 120, baseType: !23, size: 64, offset: 64)
!413 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !414, line: 207)
!414 = !DISubprogram(name: "_Exit", scope: !267, file: !267, line: 557, type: !324, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!415 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !416, line: 211)
!416 = !DISubprogram(name: "llabs", scope: !267, file: !267, line: 780, type: !21, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!417 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !418, line: 217)
!418 = !DISubprogram(name: "lldiv", scope: !267, file: !267, line: 797, type: !419, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!419 = !DISubroutineType(types: !420)
!420 = !{!408, !23, !23}
!421 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !422, line: 228)
!422 = !DISubprogram(name: "atoll", scope: !267, file: !267, line: 292, type: !423, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!423 = !DISubroutineType(types: !424)
!424 = !{!23, !161}
!425 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !426, line: 229)
!426 = !DISubprogram(name: "strtoll", scope: !267, file: !267, line: 209, type: !427, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!427 = !DISubroutineType(types: !428)
!428 = !{!23, !355, !381, !8}
!429 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !430, line: 230)
!430 = !DISubprogram(name: "strtoull", scope: !267, file: !267, line: 214, type: !431, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!431 = !DISubroutineType(types: !432)
!432 = !{!433, !355, !381, !8}
!433 = !DIBasicType(name: "long long unsigned int", size: 64, encoding: DW_ATE_unsigned)
!434 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !435, line: 232)
!435 = !DISubprogram(name: "strtof", scope: !267, file: !267, line: 172, type: !436, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!436 = !DISubroutineType(types: !437)
!437 = !{!28, !355, !381}
!438 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !439, line: 233)
!439 = !DISubprogram(name: "strtold", scope: !267, file: !267, line: 175, type: !440, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!440 = !DISubroutineType(types: !441)
!441 = !{!442, !355, !381}
!442 = !DIBasicType(name: "long double", size: 64, encoding: DW_ATE_float)
!443 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !408, line: 241)
!444 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !414, line: 243)
!445 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !416, line: 245)
!446 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !447, line: 246)
!447 = !DISubprogram(name: "div", linkageName: "_ZN9__gnu_cxx3divExx", scope: !406, file: !448, line: 214, type: !419, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!448 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/cstdlib", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!449 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !418, line: 247)
!450 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !422, line: 249)
!451 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !435, line: 250)
!452 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !426, line: 251)
!453 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !430, line: 252)
!454 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !439, line: 253)
!455 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !456, line: 418)
!456 = !DISubprogram(name: "acosf", linkageName: "_ZL5acosff", scope: !457, file: !457, line: 1126, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!457 = !DIFile(filename: "/usr/local/cuda/include/math_functions.hpp", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!458 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !459, line: 419)
!459 = !DISubprogram(name: "acoshf", linkageName: "_ZL6acoshff", scope: !457, file: !457, line: 1154, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!460 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !461, line: 420)
!461 = !DISubprogram(name: "asinf", linkageName: "_ZL5asinff", scope: !457, file: !457, line: 1121, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!462 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !463, line: 421)
!463 = !DISubprogram(name: "asinhf", linkageName: "_ZL6asinhff", scope: !457, file: !457, line: 1159, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!464 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !465, line: 422)
!465 = !DISubprogram(name: "atan2f", linkageName: "_ZL6atan2fff", scope: !457, file: !457, line: 1111, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!466 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !467, line: 423)
!467 = !DISubprogram(name: "atanf", linkageName: "_ZL5atanff", scope: !457, file: !457, line: 1116, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!468 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !469, line: 424)
!469 = !DISubprogram(name: "atanhf", linkageName: "_ZL6atanhff", scope: !457, file: !457, line: 1164, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!470 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !471, line: 425)
!471 = !DISubprogram(name: "cbrtf", linkageName: "_ZL5cbrtff", scope: !457, file: !457, line: 1199, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!472 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !473, line: 426)
!473 = !DISubprogram(name: "ceilf", linkageName: "_ZL5ceilff", scope: !474, file: !474, line: 647, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!474 = !DIFile(filename: "/usr/local/cuda/include/device_functions.hpp", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!475 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !476, line: 427)
!476 = !DISubprogram(name: "copysignf", linkageName: "_ZL9copysignfff", scope: !457, file: !457, line: 973, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!477 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !478, line: 428)
!478 = !DISubprogram(name: "cosf", linkageName: "_ZL4cosff", scope: !457, file: !457, line: 1027, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!479 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !480, line: 429)
!480 = !DISubprogram(name: "coshf", linkageName: "_ZL5coshff", scope: !457, file: !457, line: 1096, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!481 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !482, line: 430)
!482 = !DISubprogram(name: "erfcf", linkageName: "_ZL5erfcff", scope: !457, file: !457, line: 1259, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!483 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !484, line: 431)
!484 = !DISubprogram(name: "erff", linkageName: "_ZL4erfff", scope: !457, file: !457, line: 1249, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!485 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !486, line: 432)
!486 = !DISubprogram(name: "exp2f", linkageName: "_ZL5exp2ff", scope: !474, file: !474, line: 637, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!487 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !488, line: 433)
!488 = !DISubprogram(name: "expf", linkageName: "_ZL4expff", scope: !457, file: !457, line: 1078, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!489 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !490, line: 434)
!490 = !DISubprogram(name: "expm1f", linkageName: "_ZL6expm1ff", scope: !457, file: !457, line: 1169, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!491 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !492, line: 435)
!492 = !DISubprogram(name: "fabsf", linkageName: "_ZL5fabsff", scope: !474, file: !474, line: 582, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!493 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !494, line: 436)
!494 = !DISubprogram(name: "fdimf", linkageName: "_ZL5fdimfff", scope: !457, file: !457, line: 1385, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!495 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !496, line: 437)
!496 = !DISubprogram(name: "floorf", linkageName: "_ZL6floorff", scope: !474, file: !474, line: 572, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!497 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !498, line: 438)
!498 = !DISubprogram(name: "fmaf", linkageName: "_ZL4fmaffff", scope: !457, file: !457, line: 1337, type: !71, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!499 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !500, line: 439)
!500 = !DISubprogram(name: "fmaxf", linkageName: "_ZL5fmaxfff", scope: !474, file: !474, line: 602, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!501 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !502, line: 440)
!502 = !DISubprogram(name: "fminf", linkageName: "_ZL5fminfff", scope: !474, file: !474, line: 597, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!503 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !504, line: 441)
!504 = !DISubprogram(name: "fmodf", linkageName: "_ZL5fmodfff", scope: !457, file: !457, line: 1322, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!505 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !506, line: 442)
!506 = !DISubprogram(name: "frexpf", linkageName: "_ZL6frexpffPi", scope: !457, file: !457, line: 1312, type: !85, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!507 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !508, line: 443)
!508 = !DISubprogram(name: "hypotf", linkageName: "_ZL6hypotfff", scope: !457, file: !457, line: 1174, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!509 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !510, line: 444)
!510 = !DISubprogram(name: "ilogbf", linkageName: "_ZL6ilogbff", scope: !457, file: !457, line: 1390, type: !81, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!511 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !512, line: 445)
!512 = !DISubprogram(name: "ldexpf", linkageName: "_ZL6ldexpffi", scope: !457, file: !457, line: 1289, type: !123, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!513 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !514, line: 446)
!514 = !DISubprogram(name: "lgammaf", linkageName: "_ZL7lgammaff", scope: !457, file: !457, line: 1284, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!515 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !516, line: 447)
!516 = !DISubprogram(name: "llrintf", linkageName: "_ZL7llrintff", scope: !457, file: !457, line: 933, type: !131, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!517 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !518, line: 448)
!518 = !DISubprogram(name: "llroundf", linkageName: "_ZL8llroundff", scope: !457, file: !457, line: 1371, type: !131, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!519 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !520, line: 449)
!520 = !DISubprogram(name: "log10f", linkageName: "_ZL6log10ff", scope: !457, file: !457, line: 1140, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!521 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !522, line: 450)
!522 = !DISubprogram(name: "log1pf", linkageName: "_ZL6log1pff", scope: !457, file: !457, line: 1149, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!523 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !524, line: 451)
!524 = !DISubprogram(name: "log2f", linkageName: "_ZL5log2ff", scope: !457, file: !457, line: 1069, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!525 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !526, line: 452)
!526 = !DISubprogram(name: "logbf", linkageName: "_ZL5logbff", scope: !457, file: !457, line: 1395, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!527 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !528, line: 453)
!528 = !DISubprogram(name: "logf", linkageName: "_ZL4logff", scope: !457, file: !457, line: 1131, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!529 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !530, line: 454)
!530 = !DISubprogram(name: "lrintf", linkageName: "_ZL6lrintff", scope: !457, file: !457, line: 924, type: !145, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!531 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !532, line: 455)
!532 = !DISubprogram(name: "lroundf", linkageName: "_ZL7lroundff", scope: !457, file: !457, line: 1376, type: !145, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!533 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !534, line: 456)
!534 = !DISubprogram(name: "modff", linkageName: "_ZL5modfffPf", scope: !457, file: !457, line: 1317, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!535 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !536, line: 457)
!536 = !DISubprogram(name: "nearbyintf", linkageName: "_ZL10nearbyintff", scope: !457, file: !457, line: 938, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!537 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !538, line: 458)
!538 = !DISubprogram(name: "nextafterf", linkageName: "_ZL10nextafterfff", scope: !457, file: !457, line: 1002, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!539 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !540, line: 459)
!540 = !DISubprogram(name: "nexttowardf", scope: !211, file: !211, line: 284, type: !541, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!541 = !DISubroutineType(types: !542)
!542 = !{!28, !28, !442}
!543 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !540, line: 460)
!544 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !545, line: 461)
!545 = !DISubprogram(name: "powf", linkageName: "_ZL4powfff", scope: !457, file: !457, line: 1352, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!546 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !547, line: 462)
!547 = !DISubprogram(name: "remainderf", linkageName: "_ZL10remainderfff", scope: !457, file: !457, line: 1327, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!548 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !549, line: 463)
!549 = !DISubprogram(name: "remquof", linkageName: "_ZL7remquofffPi", scope: !457, file: !457, line: 1332, type: !181, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!550 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !551, line: 464)
!551 = !DISubprogram(name: "rintf", linkageName: "_ZL5rintff", scope: !457, file: !457, line: 919, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!552 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !553, line: 465)
!553 = !DISubprogram(name: "roundf", linkageName: "_ZL6roundff", scope: !457, file: !457, line: 1366, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!554 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !555, line: 466)
!555 = !DISubprogram(name: "scalblnf", linkageName: "_ZL8scalblnffl", scope: !457, file: !457, line: 1299, type: !189, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!556 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !557, line: 467)
!557 = !DISubprogram(name: "scalbnf", linkageName: "_ZL7scalbnffi", scope: !457, file: !457, line: 1294, type: !123, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!558 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !559, line: 468)
!559 = !DISubprogram(name: "sinf", linkageName: "_ZL4sinff", scope: !457, file: !457, line: 1018, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!560 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !561, line: 469)
!561 = !DISubprogram(name: "sinhf", linkageName: "_ZL5sinhff", scope: !457, file: !457, line: 1101, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!562 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !563, line: 470)
!563 = !DISubprogram(name: "sqrtf", linkageName: "_ZL5sqrtff", scope: !474, file: !474, line: 887, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!564 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !565, line: 471)
!565 = !DISubprogram(name: "tanf", linkageName: "_ZL4tanff", scope: !457, file: !457, line: 1060, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!566 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !567, line: 472)
!567 = !DISubprogram(name: "tanhf", linkageName: "_ZL5tanhff", scope: !457, file: !457, line: 1106, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!568 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !569, line: 473)
!569 = !DISubprogram(name: "tgammaf", linkageName: "_ZL7tgammaff", scope: !457, file: !457, line: 1361, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!570 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !571, line: 474)
!571 = !DISubprogram(name: "truncf", linkageName: "_ZL6truncff", scope: !474, file: !474, line: 642, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!572 = !{i32 2, !"Dwarf Version", i32 4}
!573 = !{i32 2, !"Debug Info Version", i32 3}
!574 = !{i32 4, !"nvvm-reflect-ftz", i32 0}
!575 = !{!"clang version 5.0.0 (trunk 294196)"}
!576 = !{i32 1, i32 2}
!577 = !{null, !"align", i32 8}
!578 = !{null, !"align", i32 8, !"align", i32 65544, !"align", i32 131080}
!579 = !{null, !"align", i32 16}
!580 = !{null, !"align", i32 16, !"align", i32 65552, !"align", i32 131088}
!581 = distinct !DISubprogram(name: "takeString", scope: !3, file: !3, line: 34, type: !582, isLocal: false, isDefinition: true, scopeLine: 35, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !584)
!582 = !DISubroutineType(types: !583)
!583 = !{null, !304, !8}
!584 = !{!585, !586}
!585 = !DILocalVariable(name: "p", arg: 1, scope: !581, file: !3, line: 34, type: !304)
!586 = !DILocalVariable(name: "action", arg: 2, scope: !581, file: !3, line: 34, type: !8)
!587 = !DIExpression()
!588 = !DILocation(line: 34, column: 34, scope: !581)
!589 = !DILocation(line: 34, column: 41, scope: !581)
!590 = !DILocation(line: 67, column: 3, scope: !591, inlinedAt: !626)
!591 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !593, file: !592, line: 67, type: !596, isLocal: false, isDefinition: true, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !595, variables: !4)
!592 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_builtin_vars.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!593 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_threadIdx_t", file: !592, line: 66, size: 8, elements: !594, identifier: "_ZTS26__cuda_builtin_threadIdx_t")
!594 = !{!595, !598, !599, !600, !611, !615, !619, !622}
!595 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !593, file: !592, line: 67, type: !596, isLocal: false, isDefinition: false, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true)
!596 = !DISubroutineType(types: !597)
!597 = !{!376}
!598 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_yEv", scope: !593, file: !592, line: 68, type: !596, isLocal: false, isDefinition: false, scopeLine: 68, flags: DIFlagPrototyped, isOptimized: true)
!599 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_zEv", scope: !593, file: !592, line: 69, type: !596, isLocal: false, isDefinition: false, scopeLine: 69, flags: DIFlagPrototyped, isOptimized: true)
!600 = !DISubprogram(name: "operator uint3", linkageName: "_ZNK26__cuda_builtin_threadIdx_tcv5uint3Ev", scope: !593, file: !592, line: 72, type: !601, isLocal: false, isDefinition: false, scopeLine: 72, flags: DIFlagPrototyped, isOptimized: true)
!601 = !DISubroutineType(types: !602)
!602 = !{!603, !609}
!603 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "uint3", file: !604, line: 190, size: 96, elements: !605, identifier: "_ZTS5uint3")
!604 = !DIFile(filename: "/usr/local/cuda/include/vector_types.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!605 = !{!606, !607, !608}
!606 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !603, file: !604, line: 192, baseType: !376, size: 32)
!607 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !603, file: !604, line: 192, baseType: !376, size: 32, offset: 32)
!608 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !603, file: !604, line: 192, baseType: !376, size: 32, offset: 64)
!609 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !610, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!610 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !593)
!611 = !DISubprogram(name: "__cuda_builtin_threadIdx_t", scope: !593, file: !592, line: 74, type: !612, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!612 = !DISubroutineType(types: !613)
!613 = !{null, !614}
!614 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !593, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!615 = !DISubprogram(name: "__cuda_builtin_threadIdx_t", scope: !593, file: !592, line: 74, type: !616, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!616 = !DISubroutineType(types: !617)
!617 = !{null, !614, !618}
!618 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !610, size: 64)
!619 = !DISubprogram(name: "operator=", linkageName: "_ZNK26__cuda_builtin_threadIdx_taSERKS_", scope: !593, file: !592, line: 74, type: !620, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!620 = !DISubroutineType(types: !621)
!621 = !{null, !609, !618}
!622 = !DISubprogram(name: "operator&", linkageName: "_ZNK26__cuda_builtin_threadIdx_tadEv", scope: !593, file: !592, line: 74, type: !623, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!623 = !DISubroutineType(types: !624)
!624 = !{!625, !609}
!625 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !593, size: 64)
!626 = distinct !DILocation(line: 36, column: 6, scope: !627)
!627 = distinct !DILexicalBlock(scope: !581, file: !3, line: 36, column: 6)
!628 = !{i32 0, i32 1024}
!629 = !DILocation(line: 36, column: 18, scope: !627)
!630 = !DILocation(line: 36, column: 23, scope: !627)
!631 = !DILocation(line: 78, column: 3, scope: !632, inlinedAt: !658)
!632 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !633, file: !592, line: 78, type: !596, isLocal: false, isDefinition: true, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !635, variables: !4)
!633 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_blockIdx_t", file: !592, line: 77, size: 8, elements: !634, identifier: "_ZTS25__cuda_builtin_blockIdx_t")
!634 = !{!635, !636, !637, !638, !643, !647, !651, !654}
!635 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !633, file: !592, line: 78, type: !596, isLocal: false, isDefinition: false, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true)
!636 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_yEv", scope: !633, file: !592, line: 79, type: !596, isLocal: false, isDefinition: false, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true)
!637 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_zEv", scope: !633, file: !592, line: 80, type: !596, isLocal: false, isDefinition: false, scopeLine: 80, flags: DIFlagPrototyped, isOptimized: true)
!638 = !DISubprogram(name: "operator uint3", linkageName: "_ZNK25__cuda_builtin_blockIdx_tcv5uint3Ev", scope: !633, file: !592, line: 83, type: !639, isLocal: false, isDefinition: false, scopeLine: 83, flags: DIFlagPrototyped, isOptimized: true)
!639 = !DISubroutineType(types: !640)
!640 = !{!603, !641}
!641 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !642, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!642 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !633)
!643 = !DISubprogram(name: "__cuda_builtin_blockIdx_t", scope: !633, file: !592, line: 85, type: !644, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!644 = !DISubroutineType(types: !645)
!645 = !{null, !646}
!646 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !633, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!647 = !DISubprogram(name: "__cuda_builtin_blockIdx_t", scope: !633, file: !592, line: 85, type: !648, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!648 = !DISubroutineType(types: !649)
!649 = !{null, !646, !650}
!650 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !642, size: 64)
!651 = !DISubprogram(name: "operator=", linkageName: "_ZNK25__cuda_builtin_blockIdx_taSERKS_", scope: !633, file: !592, line: 85, type: !652, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!652 = !DISubroutineType(types: !653)
!653 = !{null, !641, !650}
!654 = !DISubprogram(name: "operator&", linkageName: "_ZNK25__cuda_builtin_blockIdx_tadEv", scope: !633, file: !592, line: 85, type: !655, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!655 = !DISubroutineType(types: !656)
!656 = !{!657, !641}
!657 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !633, size: 64)
!658 = distinct !DILocation(line: 36, column: 26, scope: !659)
!659 = !DILexicalBlockFile(scope: !627, file: !3, discriminator: 1)
!660 = !{i32 0, i32 65535}
!661 = !DILocation(line: 36, column: 37, scope: !659)
!662 = !DILocation(line: 36, column: 42, scope: !659)
!663 = !DILocation(line: 68, column: 3, scope: !664, inlinedAt: !665)
!664 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_yEv", scope: !593, file: !592, line: 68, type: !596, isLocal: false, isDefinition: true, scopeLine: 68, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !598, variables: !4)
!665 = distinct !DILocation(line: 36, column: 45, scope: !666)
!666 = !DILexicalBlockFile(scope: !627, file: !3, discriminator: 2)
!667 = !DILocation(line: 36, column: 57, scope: !666)
!668 = !DILocation(line: 36, column: 62, scope: !666)
!669 = !DILocation(line: 79, column: 3, scope: !670, inlinedAt: !671)
!670 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_yEv", scope: !633, file: !592, line: 79, type: !596, isLocal: false, isDefinition: true, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !636, variables: !4)
!671 = distinct !DILocation(line: 36, column: 65, scope: !672)
!672 = !DILexicalBlockFile(scope: !627, file: !3, discriminator: 3)
!673 = !DILocation(line: 36, column: 76, scope: !672)
!674 = !DILocation(line: 36, column: 6, scope: !675)
!675 = !DILexicalBlockFile(scope: !581, file: !3, discriminator: 3)
!676 = !DILocation(line: 38, column: 6, scope: !581)
!677 = !DILocation(line: 39, column: 3, scope: !678)
!678 = distinct !DILexicalBlock(scope: !581, file: !3, line: 38, column: 6)
!679 = !DILocation(line: 41, column: 3, scope: !680)
!680 = distinct !DILexicalBlock(scope: !678, file: !3, line: 40, column: 11)
!681 = !DILocation(line: 43, column: 3, scope: !682)
!682 = distinct !DILexicalBlock(scope: !680, file: !3, line: 42, column: 11)
!683 = !DILocation(line: 45, column: 3, scope: !682)
!684 = !DILocation(line: 47, column: 1, scope: !581)
!685 = distinct !DISubprogram(name: "storeLines", linkageName: "_Z10storeLinesPvssi", scope: !3, file: !3, line: 50, type: !686, isLocal: false, isDefinition: true, scopeLine: 51, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !688)
!686 = !DISubroutineType(types: !687)
!687 = !{null, !304, !10, !10, !8}
!688 = !{!689, !690, !691, !692, !693, !694, !695}
!689 = !DILocalVariable(name: "p", arg: 1, scope: !685, file: !3, line: 50, type: !304)
!690 = !DILocalVariable(name: "size", arg: 2, scope: !685, file: !3, line: 50, type: !10)
!691 = !DILocalVariable(name: "line", arg: 3, scope: !685, file: !3, line: 50, type: !10)
!692 = !DILocalVariable(name: "op", arg: 4, scope: !685, file: !3, line: 50, type: !8)
!693 = !DILocalVariable(name: "bid", scope: !685, file: !3, line: 52, type: !8)
!694 = !DILocalVariable(name: "buffer_oN_DeViCe_short", scope: !685, file: !3, line: 60, type: !9)
!695 = !DILocalVariable(name: "buffer_oN_DeViCe_long", scope: !685, file: !3, line: 61, type: !11)
!696 = !DILocation(line: 50, column: 34, scope: !685)
!697 = !DILocation(line: 50, column: 43, scope: !685)
!698 = !DILocation(line: 50, column: 64, scope: !685)
!699 = !DILocation(line: 50, column: 74, scope: !685)
!700 = !DILocalVariable(name: "address", arg: 1, scope: !701, file: !702, line: 78, type: !87)
!701 = distinct !DISubprogram(name: "atomicAdd", linkageName: "_ZL9atomicAddPii", scope: !702, file: !702, line: 78, type: !703, isLocal: true, isDefinition: true, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !705)
!702 = !DIFile(filename: "/usr/local/cuda/include/device_atomic_functions.hpp", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!703 = !DISubroutineType(types: !704)
!704 = !{!8, !87, !8}
!705 = !{!700, !706}
!706 = !DILocalVariable(name: "val", arg: 2, scope: !701, file: !702, line: 78, type: !8)
!707 = !DILocation(line: 78, column: 53, scope: !701, inlinedAt: !708)
!708 = distinct !DILocation(line: 52, column: 12, scope: !685)
!709 = !DILocation(line: 78, column: 66, scope: !701, inlinedAt: !708)
!710 = !DILocalVariable(name: "p", arg: 1, scope: !711, file: !474, line: 1512, type: !87)
!711 = distinct !DISubprogram(name: "__iAtomicAdd", linkageName: "_ZL12__iAtomicAddPii", scope: !474, file: !474, line: 1512, type: !703, isLocal: true, isDefinition: true, scopeLine: 1513, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !712)
!712 = !{!710, !713}
!713 = !DILocalVariable(name: "val", arg: 2, scope: !711, file: !474, line: 1512, type: !8)
!714 = !DILocation(line: 1512, column: 23, scope: !711, inlinedAt: !715)
!715 = distinct !DILocation(line: 80, column: 10, scope: !701, inlinedAt: !708)
!716 = !DILocation(line: 1512, column: 30, scope: !711, inlinedAt: !715)
!717 = !DILocation(line: 1514, column: 10, scope: !711, inlinedAt: !715)
!718 = !DILocation(line: 52, column: 6, scope: !685)
!719 = !DILocation(line: 60, column: 9, scope: !685)
!720 = !DILocation(line: 61, column: 8, scope: !685)
!721 = !DILocation(line: 78, column: 3, scope: !632, inlinedAt: !722)
!722 = distinct !DILocation(line: 63, column: 37, scope: !685)
!723 = !DILocation(line: 63, column: 37, scope: !685)
!724 = !DILocation(line: 63, column: 28, scope: !685)
!725 = !DILocation(line: 63, column: 2, scope: !685)
!726 = !DILocation(line: 63, column: 35, scope: !685)
!727 = !{!728, !728, i64 0}
!728 = !{!"short", !729, i64 0}
!729 = !{!"omnipotent char", !730, i64 0}
!730 = !{!"Simple C++ TBAA"}
!731 = !DILocation(line: 79, column: 3, scope: !670, inlinedAt: !732)
!732 = distinct !DILocation(line: 64, column: 37, scope: !685)
!733 = !DILocation(line: 64, column: 37, scope: !685)
!734 = !DILocation(line: 64, column: 31, scope: !685)
!735 = !DILocation(line: 64, column: 2, scope: !685)
!736 = !DILocation(line: 64, column: 35, scope: !685)
!737 = !DILocation(line: 67, column: 3, scope: !591, inlinedAt: !738)
!738 = distinct !DILocation(line: 65, column: 37, scope: !685)
!739 = !DILocation(line: 65, column: 37, scope: !685)
!740 = !DILocation(line: 65, column: 31, scope: !685)
!741 = !DILocation(line: 65, column: 2, scope: !685)
!742 = !DILocation(line: 65, column: 35, scope: !685)
!743 = !DILocation(line: 68, column: 3, scope: !664, inlinedAt: !744)
!744 = distinct !DILocation(line: 66, column: 37, scope: !685)
!745 = !DILocation(line: 66, column: 37, scope: !685)
!746 = !DILocation(line: 66, column: 31, scope: !685)
!747 = !DILocation(line: 66, column: 2, scope: !685)
!748 = !DILocation(line: 66, column: 35, scope: !685)
!749 = !DILocation(line: 67, column: 35, scope: !685)
!750 = !DILocation(line: 67, column: 27, scope: !685)
!751 = !DILocation(line: 67, column: 29, scope: !685)
!752 = !DILocation(line: 67, column: 2, scope: !685)
!753 = !DILocation(line: 67, column: 33, scope: !685)
!754 = !{!755, !755, i64 0}
!755 = !{!"long", !729, i64 0}
!756 = !DILocation(line: 68, column: 31, scope: !685)
!757 = !DILocation(line: 68, column: 2, scope: !685)
!758 = !DILocation(line: 68, column: 35, scope: !685)
!759 = !DILocation(line: 69, column: 31, scope: !685)
!760 = !DILocation(line: 69, column: 2, scope: !685)
!761 = !DILocation(line: 69, column: 35, scope: !685)
!762 = !DILocation(line: 70, column: 22, scope: !685)
!763 = !DILocation(line: 70, column: 24, scope: !685)
!764 = !DILocation(line: 70, column: 2, scope: !685)
!765 = !DILocation(line: 70, column: 28, scope: !685)
!766 = !{!767, !767, i64 0}
!767 = !{!"int", !729, i64 0}
!768 = !DILocation(line: 75, column: 10, scope: !769)
!769 = distinct !DILexicalBlock(scope: !685, file: !3, line: 75, column: 6)
!770 = !DILocation(line: 75, column: 6, scope: !685)
!771 = !DILocation(line: 76, column: 3, scope: !769)
!772 = !DILocation(line: 80, column: 1, scope: !685)
!773 = distinct !DISubprogram(name: "dumpLines", linkageName: "_Z9dumpLinesv", scope: !3, file: !3, line: 82, type: !277, isLocal: false, isDefinition: true, scopeLine: 83, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !774)
!774 = !{!775}
!775 = !DILocalVariable(name: "ii", scope: !773, file: !3, line: 86, type: !8)
!776 = !DILocation(line: 67, column: 3, scope: !591, inlinedAt: !777)
!777 = distinct !DILocation(line: 84, column: 6, scope: !778)
!778 = distinct !DILexicalBlock(scope: !773, file: !3, line: 84, column: 6)
!779 = !DILocation(line: 84, column: 18, scope: !778)
!780 = !DILocation(line: 84, column: 23, scope: !778)
!781 = !DILocation(line: 78, column: 3, scope: !632, inlinedAt: !782)
!782 = distinct !DILocation(line: 84, column: 26, scope: !783)
!783 = !DILexicalBlockFile(scope: !778, file: !3, discriminator: 1)
!784 = !DILocation(line: 84, column: 37, scope: !783)
!785 = !DILocation(line: 84, column: 42, scope: !783)
!786 = !DILocation(line: 68, column: 3, scope: !664, inlinedAt: !787)
!787 = distinct !DILocation(line: 84, column: 45, scope: !788)
!788 = !DILexicalBlockFile(scope: !778, file: !3, discriminator: 2)
!789 = !DILocation(line: 84, column: 57, scope: !788)
!790 = !DILocation(line: 84, column: 62, scope: !788)
!791 = !DILocation(line: 79, column: 3, scope: !670, inlinedAt: !792)
!792 = distinct !DILocation(line: 84, column: 65, scope: !793)
!793 = !DILexicalBlockFile(scope: !778, file: !3, discriminator: 3)
!794 = !DILocation(line: 84, column: 76, scope: !793)
!795 = !DILocation(line: 84, column: 6, scope: !796)
!796 = !DILexicalBlockFile(scope: !773, file: !3, discriminator: 3)
!797 = !DILocation(line: 91, column: 2, scope: !773)
!798 = !DILocation(line: 102, column: 1, scope: !773)
!799 = !DILocation(line: 102, column: 1, scope: !800)
!800 = !DILexicalBlockFile(scope: !773, file: !3, discriminator: 1)
!801 = distinct !DISubprogram(name: "print1", linkageName: "_Z6print1i", scope: !3, file: !3, line: 104, type: !324, isLocal: false, isDefinition: true, scopeLine: 105, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !802)
!802 = !{!803}
!803 = !DILocalVariable(name: "a", arg: 1, scope: !801, file: !3, line: 104, type: !8)
!804 = !DILocation(line: 104, column: 28, scope: !801)
!805 = !DILocation(line: 67, column: 3, scope: !591, inlinedAt: !806)
!806 = distinct !DILocation(line: 106, column: 6, scope: !807)
!807 = distinct !DILexicalBlock(scope: !801, file: !3, line: 106, column: 6)
!808 = !DILocation(line: 106, column: 18, scope: !807)
!809 = !DILocation(line: 106, column: 23, scope: !807)
!810 = !DILocation(line: 68, column: 3, scope: !664, inlinedAt: !811)
!811 = distinct !DILocation(line: 106, column: 26, scope: !812)
!812 = !DILexicalBlockFile(scope: !807, file: !3, discriminator: 1)
!813 = !DILocation(line: 106, column: 38, scope: !812)
!814 = !DILocation(line: 106, column: 6, scope: !815)
!815 = !DILexicalBlockFile(scope: !801, file: !3, discriminator: 1)
!816 = !DILocation(line: 78, column: 3, scope: !632, inlinedAt: !817)
!817 = distinct !DILocation(line: 107, column: 6, scope: !818)
!818 = distinct !DILexicalBlock(scope: !801, file: !3, line: 107, column: 6)
!819 = !DILocation(line: 107, column: 17, scope: !818)
!820 = !DILocation(line: 107, column: 22, scope: !818)
!821 = !DILocation(line: 79, column: 3, scope: !670, inlinedAt: !822)
!822 = distinct !DILocation(line: 107, column: 25, scope: !823)
!823 = !DILexicalBlockFile(scope: !818, file: !3, discriminator: 1)
!824 = !DILocation(line: 107, column: 36, scope: !823)
!825 = !DILocation(line: 107, column: 6, scope: !815)
!826 = !DILocation(line: 109, column: 6, scope: !801)
!827 = !DILocation(line: 110, column: 3, scope: !828)
!828 = distinct !DILexicalBlock(scope: !801, file: !3, line: 109, column: 6)
!829 = !DILocation(line: 110, column: 3, scope: !830)
!830 = !DILexicalBlockFile(scope: !828, file: !3, discriminator: 2)
!831 = !DILocation(line: 112, column: 3, scope: !832)
!832 = distinct !DILexicalBlock(scope: !828, file: !3, line: 111, column: 11)
!833 = !DILocation(line: 112, column: 3, scope: !834)
!834 = !DILexicalBlockFile(scope: !832, file: !3, discriminator: 2)
!835 = !DILocation(line: 114, column: 3, scope: !832)
!836 = !DILocation(line: 116, column: 1, scope: !801)
!837 = distinct !DISubprogram(name: "print2", linkageName: "_Z6print2v", scope: !3, file: !3, line: 118, type: !277, isLocal: false, isDefinition: true, scopeLine: 119, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !4)
!838 = !DILocation(line: 67, column: 3, scope: !591, inlinedAt: !839)
!839 = distinct !DILocation(line: 120, column: 6, scope: !840)
!840 = distinct !DILexicalBlock(scope: !837, file: !3, line: 120, column: 6)
!841 = !DILocation(line: 120, column: 18, scope: !840)
!842 = !DILocation(line: 120, column: 23, scope: !840)
!843 = !DILocation(line: 68, column: 3, scope: !664, inlinedAt: !844)
!844 = distinct !DILocation(line: 120, column: 26, scope: !845)
!845 = !DILexicalBlockFile(scope: !840, file: !3, discriminator: 1)
!846 = !DILocation(line: 120, column: 38, scope: !845)
!847 = !DILocation(line: 120, column: 6, scope: !848)
!848 = !DILexicalBlockFile(scope: !837, file: !3, discriminator: 1)
!849 = !DILocation(line: 78, column: 3, scope: !632, inlinedAt: !850)
!850 = distinct !DILocation(line: 121, column: 6, scope: !851)
!851 = distinct !DILexicalBlock(scope: !837, file: !3, line: 121, column: 6)
!852 = !DILocation(line: 121, column: 17, scope: !851)
!853 = !DILocation(line: 121, column: 22, scope: !851)
!854 = !DILocation(line: 79, column: 3, scope: !670, inlinedAt: !855)
!855 = distinct !DILocation(line: 121, column: 25, scope: !856)
!856 = !DILexicalBlockFile(scope: !851, file: !3, discriminator: 1)
!857 = !DILocation(line: 121, column: 36, scope: !856)
!858 = !DILocation(line: 121, column: 6, scope: !848)
!859 = !DILocation(line: 122, column: 9, scope: !837)
!860 = !DILocation(line: 122, column: 9, scope: !861)
!861 = !DILexicalBlockFile(scope: !837, file: !3, discriminator: 2)
!862 = !DILocation(line: 123, column: 1, scope: !837)
!863 = !DILocation(line: 123, column: 1, scope: !848)
!864 = distinct !DISubprogram(name: "print3", linkageName: "_Z6print3i", scope: !3, file: !3, line: 125, type: !324, isLocal: false, isDefinition: true, scopeLine: 126, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !865)
!865 = !{!866}
!866 = !DILocalVariable(name: "a", arg: 1, scope: !864, file: !3, line: 125, type: !8)
!867 = !DILocation(line: 125, column: 28, scope: !864)
!868 = !DILocation(line: 67, column: 3, scope: !591, inlinedAt: !869)
!869 = distinct !DILocation(line: 127, column: 6, scope: !870)
!870 = distinct !DILexicalBlock(scope: !864, file: !3, line: 127, column: 6)
!871 = !DILocation(line: 127, column: 18, scope: !870)
!872 = !DILocation(line: 127, column: 23, scope: !870)
!873 = !DILocation(line: 68, column: 3, scope: !664, inlinedAt: !874)
!874 = distinct !DILocation(line: 127, column: 26, scope: !875)
!875 = !DILexicalBlockFile(scope: !870, file: !3, discriminator: 1)
!876 = !DILocation(line: 127, column: 38, scope: !875)
!877 = !DILocation(line: 127, column: 6, scope: !878)
!878 = !DILexicalBlockFile(scope: !864, file: !3, discriminator: 1)
!879 = !DILocation(line: 78, column: 3, scope: !632, inlinedAt: !880)
!880 = distinct !DILocation(line: 128, column: 6, scope: !881)
!881 = distinct !DILexicalBlock(scope: !864, file: !3, line: 128, column: 6)
!882 = !DILocation(line: 128, column: 17, scope: !881)
!883 = !DILocation(line: 128, column: 22, scope: !881)
!884 = !DILocation(line: 79, column: 3, scope: !670, inlinedAt: !885)
!885 = distinct !DILocation(line: 128, column: 25, scope: !886)
!886 = !DILexicalBlockFile(scope: !881, file: !3, discriminator: 1)
!887 = !DILocation(line: 128, column: 36, scope: !886)
!888 = !DILocation(line: 128, column: 6, scope: !878)
!889 = !DILocation(line: 129, column: 9, scope: !864)
!890 = !DILocation(line: 129, column: 9, scope: !891)
!891 = !DILexicalBlockFile(scope: !864, file: !3, discriminator: 2)
!892 = !DILocation(line: 130, column: 1, scope: !864)
!893 = !DILocation(line: 130, column: 1, scope: !878)
!894 = distinct !DISubprogram(name: "print5", linkageName: "_Z6print5Pviii", scope: !3, file: !3, line: 132, type: !895, isLocal: false, isDefinition: true, scopeLine: 133, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !897)
!895 = !DISubroutineType(types: !896)
!896 = !{null, !304, !8, !8, !8}
!897 = !{!898, !899, !900, !901}
!898 = !DILocalVariable(name: "p", arg: 1, scope: !894, file: !3, line: 132, type: !304)
!899 = !DILocalVariable(name: "bits", arg: 2, scope: !894, file: !3, line: 132, type: !8)
!900 = !DILocalVariable(name: "line", arg: 3, scope: !894, file: !3, line: 132, type: !8)
!901 = !DILocalVariable(name: "op", arg: 4, scope: !894, file: !3, line: 132, type: !8)
!902 = !DILocation(line: 132, column: 30, scope: !894)
!903 = !DILocation(line: 132, column: 37, scope: !894)
!904 = !DILocation(line: 132, column: 47, scope: !894)
!905 = !DILocation(line: 132, column: 57, scope: !894)
!906 = !DILocation(line: 136, column: 35, scope: !894)
!907 = !DILocation(line: 136, column: 30, scope: !894)
!908 = !DILocation(line: 136, column: 47, scope: !894)
!909 = !DILocation(line: 50, column: 34, scope: !685, inlinedAt: !910)
!910 = distinct !DILocation(line: 136, column: 9, scope: !894)
!911 = !DILocation(line: 50, column: 43, scope: !685, inlinedAt: !910)
!912 = !DILocation(line: 50, column: 64, scope: !685, inlinedAt: !910)
!913 = !DILocation(line: 50, column: 74, scope: !685, inlinedAt: !910)
!914 = !DILocation(line: 78, column: 53, scope: !701, inlinedAt: !915)
!915 = distinct !DILocation(line: 52, column: 12, scope: !685, inlinedAt: !910)
!916 = !DILocation(line: 78, column: 66, scope: !701, inlinedAt: !915)
!917 = !DILocation(line: 1512, column: 23, scope: !711, inlinedAt: !918)
!918 = distinct !DILocation(line: 80, column: 10, scope: !701, inlinedAt: !915)
!919 = !DILocation(line: 1512, column: 30, scope: !711, inlinedAt: !918)
!920 = !DILocation(line: 1514, column: 10, scope: !711, inlinedAt: !918)
!921 = !DILocation(line: 52, column: 6, scope: !685, inlinedAt: !910)
!922 = !DILocation(line: 60, column: 9, scope: !685, inlinedAt: !910)
!923 = !DILocation(line: 61, column: 8, scope: !685, inlinedAt: !910)
!924 = !DILocation(line: 78, column: 3, scope: !632, inlinedAt: !925)
!925 = distinct !DILocation(line: 63, column: 37, scope: !685, inlinedAt: !910)
!926 = !DILocation(line: 63, column: 37, scope: !685, inlinedAt: !910)
!927 = !DILocation(line: 63, column: 28, scope: !685, inlinedAt: !910)
!928 = !DILocation(line: 63, column: 2, scope: !685, inlinedAt: !910)
!929 = !DILocation(line: 63, column: 35, scope: !685, inlinedAt: !910)
!930 = !DILocation(line: 79, column: 3, scope: !670, inlinedAt: !931)
!931 = distinct !DILocation(line: 64, column: 37, scope: !685, inlinedAt: !910)
!932 = !DILocation(line: 64, column: 37, scope: !685, inlinedAt: !910)
!933 = !DILocation(line: 64, column: 31, scope: !685, inlinedAt: !910)
!934 = !DILocation(line: 64, column: 2, scope: !685, inlinedAt: !910)
!935 = !DILocation(line: 64, column: 35, scope: !685, inlinedAt: !910)
!936 = !DILocation(line: 67, column: 3, scope: !591, inlinedAt: !937)
!937 = distinct !DILocation(line: 65, column: 37, scope: !685, inlinedAt: !910)
!938 = !DILocation(line: 65, column: 37, scope: !685, inlinedAt: !910)
!939 = !DILocation(line: 65, column: 31, scope: !685, inlinedAt: !910)
!940 = !DILocation(line: 65, column: 2, scope: !685, inlinedAt: !910)
!941 = !DILocation(line: 65, column: 35, scope: !685, inlinedAt: !910)
!942 = !DILocation(line: 68, column: 3, scope: !664, inlinedAt: !943)
!943 = distinct !DILocation(line: 66, column: 37, scope: !685, inlinedAt: !910)
!944 = !DILocation(line: 66, column: 37, scope: !685, inlinedAt: !910)
!945 = !DILocation(line: 66, column: 31, scope: !685, inlinedAt: !910)
!946 = !DILocation(line: 66, column: 2, scope: !685, inlinedAt: !910)
!947 = !DILocation(line: 66, column: 35, scope: !685, inlinedAt: !910)
!948 = !DILocation(line: 67, column: 35, scope: !685, inlinedAt: !910)
!949 = !DILocation(line: 67, column: 27, scope: !685, inlinedAt: !910)
!950 = !DILocation(line: 67, column: 29, scope: !685, inlinedAt: !910)
!951 = !DILocation(line: 67, column: 2, scope: !685, inlinedAt: !910)
!952 = !DILocation(line: 67, column: 33, scope: !685, inlinedAt: !910)
!953 = !DILocation(line: 68, column: 31, scope: !685, inlinedAt: !910)
!954 = !DILocation(line: 68, column: 2, scope: !685, inlinedAt: !910)
!955 = !DILocation(line: 68, column: 35, scope: !685, inlinedAt: !910)
!956 = !DILocation(line: 69, column: 31, scope: !685, inlinedAt: !910)
!957 = !DILocation(line: 69, column: 2, scope: !685, inlinedAt: !910)
!958 = !DILocation(line: 69, column: 35, scope: !685, inlinedAt: !910)
!959 = !DILocation(line: 70, column: 22, scope: !685, inlinedAt: !910)
!960 = !DILocation(line: 70, column: 24, scope: !685, inlinedAt: !910)
!961 = !DILocation(line: 70, column: 2, scope: !685, inlinedAt: !910)
!962 = !DILocation(line: 70, column: 28, scope: !685, inlinedAt: !910)
!963 = !DILocation(line: 75, column: 10, scope: !769, inlinedAt: !910)
!964 = !DILocation(line: 75, column: 6, scope: !685, inlinedAt: !910)
!965 = !DILocation(line: 76, column: 3, scope: !769, inlinedAt: !910)
!966 = !DILocation(line: 80, column: 1, scope: !685, inlinedAt: !910)
!967 = !DILocation(line: 137, column: 1, scope: !894)
!968 = distinct !DISubprogram(name: "RetKernel", scope: !3, file: !3, line: 140, type: !277, isLocal: false, isDefinition: true, scopeLine: 141, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !969)
!969 = !{!970}
!970 = !DILocalVariable(name: "buffer_oN_DeViCe_short", scope: !968, file: !3, line: 147, type: !9)
!971 = !DILocation(line: 67, column: 3, scope: !591, inlinedAt: !972)
!972 = distinct !DILocation(line: 143, column: 6, scope: !973)
!973 = distinct !DILexicalBlock(scope: !968, file: !3, line: 143, column: 6)
!974 = !DILocation(line: 143, column: 18, scope: !973)
!975 = !DILocation(line: 143, column: 23, scope: !973)
!976 = !DILocation(line: 78, column: 3, scope: !632, inlinedAt: !977)
!977 = distinct !DILocation(line: 143, column: 26, scope: !978)
!978 = !DILexicalBlockFile(scope: !973, file: !3, discriminator: 1)
!979 = !DILocation(line: 143, column: 37, scope: !978)
!980 = !DILocation(line: 143, column: 42, scope: !978)
!981 = !DILocation(line: 68, column: 3, scope: !664, inlinedAt: !982)
!982 = distinct !DILocation(line: 143, column: 45, scope: !983)
!983 = !DILexicalBlockFile(scope: !973, file: !3, discriminator: 2)
!984 = !DILocation(line: 143, column: 57, scope: !983)
!985 = !DILocation(line: 143, column: 62, scope: !983)
!986 = !DILocation(line: 79, column: 3, scope: !670, inlinedAt: !987)
!987 = distinct !DILocation(line: 143, column: 65, scope: !988)
!988 = !DILexicalBlockFile(scope: !973, file: !3, discriminator: 3)
!989 = !DILocation(line: 143, column: 76, scope: !988)
!990 = !DILocation(line: 143, column: 6, scope: !991)
!991 = !DILexicalBlockFile(scope: !968, file: !3, discriminator: 3)
!992 = !DILocation(line: 145, column: 26, scope: !968)
!993 = !DILocation(line: 145, column: 24, scope: !968)
!994 = !DILocation(line: 147, column: 9, scope: !968)
!995 = !DILocation(line: 89, column: 3, scope: !996, inlinedAt: !1038)
!996 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_xEv", scope: !997, file: !592, line: 89, type: !596, isLocal: false, isDefinition: true, scopeLine: 89, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !999, variables: !4)
!997 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_blockDim_t", file: !592, line: 88, size: 8, elements: !998, identifier: "_ZTS25__cuda_builtin_blockDim_t")
!998 = !{!999, !1000, !1001, !1002, !1023, !1027, !1031, !1034}
!999 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_xEv", scope: !997, file: !592, line: 89, type: !596, isLocal: false, isDefinition: false, scopeLine: 89, flags: DIFlagPrototyped, isOptimized: true)
!1000 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_yEv", scope: !997, file: !592, line: 90, type: !596, isLocal: false, isDefinition: false, scopeLine: 90, flags: DIFlagPrototyped, isOptimized: true)
!1001 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_zEv", scope: !997, file: !592, line: 91, type: !596, isLocal: false, isDefinition: false, scopeLine: 91, flags: DIFlagPrototyped, isOptimized: true)
!1002 = !DISubprogram(name: "operator dim3", linkageName: "_ZNK25__cuda_builtin_blockDim_tcv4dim3Ev", scope: !997, file: !592, line: 94, type: !1003, isLocal: false, isDefinition: false, scopeLine: 94, flags: DIFlagPrototyped, isOptimized: true)
!1003 = !DISubroutineType(types: !1004)
!1004 = !{!1005, !1021}
!1005 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "dim3", file: !604, line: 417, size: 96, elements: !1006, identifier: "_ZTS4dim3")
!1006 = !{!1007, !1008, !1009, !1010, !1014, !1018}
!1007 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !1005, file: !604, line: 419, baseType: !376, size: 32)
!1008 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !1005, file: !604, line: 419, baseType: !376, size: 32, offset: 32)
!1009 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !1005, file: !604, line: 419, baseType: !376, size: 32, offset: 64)
!1010 = !DISubprogram(name: "dim3", scope: !1005, file: !604, line: 421, type: !1011, isLocal: false, isDefinition: false, scopeLine: 421, flags: DIFlagPrototyped, isOptimized: true)
!1011 = !DISubroutineType(types: !1012)
!1012 = !{null, !1013, !376, !376, !376}
!1013 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1005, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1014 = !DISubprogram(name: "dim3", scope: !1005, file: !604, line: 422, type: !1015, isLocal: false, isDefinition: false, scopeLine: 422, flags: DIFlagPrototyped, isOptimized: true)
!1015 = !DISubroutineType(types: !1016)
!1016 = !{null, !1013, !1017}
!1017 = !DIDerivedType(tag: DW_TAG_typedef, name: "uint3", file: !604, line: 383, baseType: !603)
!1018 = !DISubprogram(name: "operator uint3", linkageName: "_ZN4dim3cv5uint3Ev", scope: !1005, file: !604, line: 423, type: !1019, isLocal: false, isDefinition: false, scopeLine: 423, flags: DIFlagPrototyped, isOptimized: true)
!1019 = !DISubroutineType(types: !1020)
!1020 = !{!1017, !1013}
!1021 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1022, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1022 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !997)
!1023 = !DISubprogram(name: "__cuda_builtin_blockDim_t", scope: !997, file: !592, line: 96, type: !1024, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1024 = !DISubroutineType(types: !1025)
!1025 = !{null, !1026}
!1026 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !997, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1027 = !DISubprogram(name: "__cuda_builtin_blockDim_t", scope: !997, file: !592, line: 96, type: !1028, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1028 = !DISubroutineType(types: !1029)
!1029 = !{null, !1026, !1030}
!1030 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1022, size: 64)
!1031 = !DISubprogram(name: "operator=", linkageName: "_ZNK25__cuda_builtin_blockDim_taSERKS_", scope: !997, file: !592, line: 96, type: !1032, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1032 = !DISubroutineType(types: !1033)
!1033 = !{null, !1021, !1030}
!1034 = !DISubprogram(name: "operator&", linkageName: "_ZNK25__cuda_builtin_blockDim_tadEv", scope: !997, file: !592, line: 96, type: !1035, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1035 = !DISubroutineType(types: !1036)
!1036 = !{!1037, !1021}
!1037 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !997, size: 64)
!1038 = distinct !DILocation(line: 148, column: 32, scope: !968)
!1039 = !{i32 1, i32 1025}
!1040 = !DILocation(line: 148, column: 32, scope: !968)
!1041 = !DILocation(line: 148, column: 30, scope: !968)
!1042 = !DILocation(line: 90, column: 3, scope: !1043, inlinedAt: !1044)
!1043 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_yEv", scope: !997, file: !592, line: 90, type: !596, isLocal: false, isDefinition: true, scopeLine: 90, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1000, variables: !4)
!1044 = distinct !DILocation(line: 149, column: 32, scope: !968)
!1045 = !DILocation(line: 149, column: 32, scope: !968)
!1046 = !DILocation(line: 149, column: 30, scope: !968)
!1047 = !DILocation(line: 100, column: 3, scope: !1048, inlinedAt: !1074)
!1048 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_xEv", scope: !1049, file: !592, line: 100, type: !596, isLocal: false, isDefinition: true, scopeLine: 100, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1051, variables: !4)
!1049 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_gridDim_t", file: !592, line: 99, size: 8, elements: !1050, identifier: "_ZTS24__cuda_builtin_gridDim_t")
!1050 = !{!1051, !1052, !1053, !1054, !1059, !1063, !1067, !1070}
!1051 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_xEv", scope: !1049, file: !592, line: 100, type: !596, isLocal: false, isDefinition: false, scopeLine: 100, flags: DIFlagPrototyped, isOptimized: true)
!1052 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_yEv", scope: !1049, file: !592, line: 101, type: !596, isLocal: false, isDefinition: false, scopeLine: 101, flags: DIFlagPrototyped, isOptimized: true)
!1053 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_zEv", scope: !1049, file: !592, line: 102, type: !596, isLocal: false, isDefinition: false, scopeLine: 102, flags: DIFlagPrototyped, isOptimized: true)
!1054 = !DISubprogram(name: "operator dim3", linkageName: "_ZNK24__cuda_builtin_gridDim_tcv4dim3Ev", scope: !1049, file: !592, line: 105, type: !1055, isLocal: false, isDefinition: false, scopeLine: 105, flags: DIFlagPrototyped, isOptimized: true)
!1055 = !DISubroutineType(types: !1056)
!1056 = !{!1005, !1057}
!1057 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1058, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1058 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1049)
!1059 = !DISubprogram(name: "__cuda_builtin_gridDim_t", scope: !1049, file: !592, line: 107, type: !1060, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1060 = !DISubroutineType(types: !1061)
!1061 = !{null, !1062}
!1062 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1049, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1063 = !DISubprogram(name: "__cuda_builtin_gridDim_t", scope: !1049, file: !592, line: 107, type: !1064, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1064 = !DISubroutineType(types: !1065)
!1065 = !{null, !1062, !1066}
!1066 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1058, size: 64)
!1067 = !DISubprogram(name: "operator=", linkageName: "_ZNK24__cuda_builtin_gridDim_taSERKS_", scope: !1049, file: !592, line: 107, type: !1068, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1068 = !DISubroutineType(types: !1069)
!1069 = !{null, !1057, !1066}
!1070 = !DISubprogram(name: "operator&", linkageName: "_ZNK24__cuda_builtin_gridDim_tadEv", scope: !1049, file: !592, line: 107, type: !1071, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1071 = !DISubroutineType(types: !1072)
!1072 = !{!1073, !1057}
!1073 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1049, size: 64)
!1074 = distinct !DILocation(line: 150, column: 32, scope: !968)
!1075 = !{i32 1, i32 65536}
!1076 = !DILocation(line: 150, column: 32, scope: !968)
!1077 = !DILocation(line: 150, column: 30, scope: !968)
!1078 = !DILocation(line: 101, column: 3, scope: !1079, inlinedAt: !1080)
!1079 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_yEv", scope: !1049, file: !592, line: 101, type: !596, isLocal: false, isDefinition: true, scopeLine: 101, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1052, variables: !4)
!1080 = distinct !DILocation(line: 151, column: 32, scope: !968)
!1081 = !DILocation(line: 151, column: 32, scope: !968)
!1082 = !DILocation(line: 151, column: 30, scope: !968)
!1083 = !DILocation(line: 152, column: 2, scope: !968)
!1084 = !DILocation(line: 158, column: 9, scope: !968)
!1085 = !DILocation(line: 159, column: 1, scope: !968)
!1086 = !DILocation(line: 159, column: 1, scope: !1087)
!1087 = !DILexicalBlockFile(scope: !968, file: !3, discriminator: 1)
