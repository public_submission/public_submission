; ModuleID = '<stdin>'
source_filename = "llvm-link"
target datalayout = "e-i64:64-v16:16-v32:32-n16:32:64"
target triple = "nvptx64-nvidia-cuda"

%printf_args = type { i8* }
%printf_args.3 = type { i32* }
%printf_args.4 = type { i32, i32 }
%printf_args.7 = type { i32, i32, i32 }
%printf_args.8 = type { i32 }

@ccnntt = addrspace(1) externally_initialized global i32 1, align 4, !dbg !0
@.str = private unnamed_addr constant [15 x i8] c"d: caller: %s\0A\00", align 1
@.str.1 = private unnamed_addr constant [15 x i8] c"d: callee: %s\0A\00", align 1
@.str.2 = private unnamed_addr constant [15 x i8] c"d: return: %s\0A\00", align 1
@.str.3 = private unnamed_addr constant [18 x i8] c"d: undefined: %s\0A\00", align 1
@buffer_oN_DeViCe = addrspace(1) externally_initialized global [25165824 x i32] zeroinitializer, align 4
@.str.4 = private unnamed_addr constant [22 x i8] c"d: buffer handle: %p\0A\00", align 1
@.str.5 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.6 = private unnamed_addr constant [24 x i8] c"d: load by CTA (%d,%d)\0A\00", align 1
@.str.7 = private unnamed_addr constant [25 x i8] c"d: store by CTA (%d,%d)\0A\00", align 1
@.str.8 = private unnamed_addr constant [23 x i8] c"d: !!! undefined !!! \0A\00", align 1
@.str.9 = private unnamed_addr constant [35 x i8] c"d: source line: %d by CTA (%d,%d)\0A\00", align 1
@.str.10 = private unnamed_addr constant [47 x i8] c"d: Kernel Returns: collected [ %d ] entries. \0A\00", align 1
@0 = private unnamed_addr constant [23 x i8] c"_Z12bicg_kernel1PfS_S_\00"
@1 = private unnamed_addr constant [23 x i8] c"_Z12bicg_kernel2PfS_S_\00"

; Function Attrs: nounwind
define void @_Z12bicg_kernel1PfS_S_(float* nocapture readonly %A, float* nocapture readonly %r, float* nocapture %s) local_unnamed_addr #0 !dbg !585 {
entry:
  tail call void @llvm.dbg.value(metadata float* %A, i64 0, metadata !591, metadata !598), !dbg !599
  tail call void @llvm.dbg.value(metadata float* %r, i64 0, metadata !592, metadata !598), !dbg !600
  tail call void @llvm.dbg.value(metadata float* %s, i64 0, metadata !593, metadata !598), !dbg !601
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !602, !range !639
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #3, !dbg !640, !range !685
  %mul = mul nuw nsw i32 %1, %0, !dbg !686
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !687, !range !716
  %add = add nuw nsw i32 %mul, %2, !dbg !717
  tail call void @llvm.dbg.value(metadata i32 %add, i64 0, metadata !594, metadata !598), !dbg !718
  %cmp = icmp ult i32 %add, 4096, !dbg !719
  br i1 %cmp, label %if.then, label %if.end, !dbg !720

if.then:                                          ; preds = %entry
  %idxprom23 = zext i32 %add to i64, !dbg !721
  %arrayidx = getelementptr inbounds float, float* %s, i64 %idxprom23, !dbg !721
  store float 0.000000e+00, float* %arrayidx, align 4, !dbg !722, !tbaa !723
  call void @_Z6print3i(i32 105), !dbg !722
  call void @_Z6print1i(i32 2), !dbg !722
  %3 = bitcast float* %arrayidx to i8*, !dbg !727
  call void @_Z6print5Pviii(i8* %3, i32 32, i32 105, i32 2), !dbg !722
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !595, metadata !598), !dbg !727
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !595, metadata !598), !dbg !727
  br label %for.body, !dbg !728

for.body:                                         ; preds = %for.body, %if.then
  %4 = phi float [ 0.000000e+00, %if.then ], [ %add13.1, %for.body ], !dbg !731
  %i.024 = phi i32 [ 0, %if.then ], [ %inc.1, %for.body ]
  %mul4 = shl i32 %i.024, 12, !dbg !734
  %add5 = add nuw nsw i32 %mul4, %add, !dbg !735
  %idxprom6 = sext i32 %add5 to i64, !dbg !736
  %arrayidx7 = getelementptr inbounds float, float* %A, i64 %idxprom6, !dbg !736
  %5 = load float, float* %arrayidx7, align 4, !dbg !736, !tbaa !723
  call void @_Z6print3i(i32 110), !dbg !736
  call void @_Z6print1i(i32 1), !dbg !736
  %6 = bitcast float* %arrayidx7 to i8*, !dbg !737
  call void @_Z6print5Pviii(i8* %6, i32 32, i32 110, i32 1), !dbg !736
  %idxprom8 = sext i32 %i.024 to i64, !dbg !737
  %arrayidx9 = getelementptr inbounds float, float* %r, i64 %idxprom8, !dbg !737
  %7 = load float, float* %arrayidx9, align 4, !dbg !737, !tbaa !723
  call void @_Z6print3i(i32 110), !dbg !737
  call void @_Z6print1i(i32 1), !dbg !737
  %8 = bitcast float* %arrayidx9 to i8*, !dbg !738
  call void @_Z6print5Pviii(i8* %8, i32 32, i32 110, i32 1), !dbg !737
  %mul10 = fmul float %5, %7, !dbg !738
  %add13 = fadd float %4, %mul10, !dbg !731
  store float %add13, float* %arrayidx, align 4, !dbg !731, !tbaa !723
  call void @_Z6print3i(i32 110), !dbg !731
  call void @_Z6print1i(i32 2), !dbg !731
  %9 = bitcast float* %arrayidx to i8*, !dbg !739
  call void @_Z6print5Pviii(i8* %9, i32 32, i32 110, i32 2), !dbg !731
  %inc = or i32 %i.024, 1, !dbg !739
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !595, metadata !598), !dbg !727
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !595, metadata !598), !dbg !727
  %mul4.1 = shl i32 %inc, 12, !dbg !734
  %add5.1 = add nuw nsw i32 %mul4.1, %add, !dbg !735
  %idxprom6.1 = sext i32 %add5.1 to i64, !dbg !736
  %arrayidx7.1 = getelementptr inbounds float, float* %A, i64 %idxprom6.1, !dbg !736
  %10 = load float, float* %arrayidx7.1, align 4, !dbg !736, !tbaa !723
  call void @_Z6print3i(i32 110), !dbg !736
  call void @_Z6print1i(i32 1), !dbg !736
  %11 = bitcast float* %arrayidx7.1 to i8*, !dbg !737
  call void @_Z6print5Pviii(i8* %11, i32 32, i32 110, i32 1), !dbg !736
  %idxprom8.1 = sext i32 %inc to i64, !dbg !737
  %arrayidx9.1 = getelementptr inbounds float, float* %r, i64 %idxprom8.1, !dbg !737
  %12 = load float, float* %arrayidx9.1, align 4, !dbg !737, !tbaa !723
  call void @_Z6print3i(i32 110), !dbg !737
  call void @_Z6print1i(i32 1), !dbg !737
  %13 = bitcast float* %arrayidx9.1 to i8*, !dbg !738
  call void @_Z6print5Pviii(i8* %13, i32 32, i32 110, i32 1), !dbg !737
  %mul10.1 = fmul float %10, %12, !dbg !738
  %add13.1 = fadd float %add13, %mul10.1, !dbg !731
  store float %add13.1, float* %arrayidx, align 4, !dbg !731, !tbaa !723
  call void @_Z6print3i(i32 110), !dbg !731
  call void @_Z6print1i(i32 2), !dbg !731
  %14 = bitcast float* %arrayidx to i8*, !dbg !739
  call void @_Z6print5Pviii(i8* %14, i32 32, i32 110, i32 2), !dbg !731
  %inc.1 = add nsw i32 %i.024, 2, !dbg !739
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !595, metadata !598), !dbg !727
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !595, metadata !598), !dbg !727
  %exitcond.1 = icmp eq i32 %inc.1, 4096, !dbg !741
  br i1 %exitcond.1, label %if.end.loopexit, label %for.body, !dbg !728, !llvm.loop !743

if.end.loopexit:                                  ; preds = %for.body
  br label %if.end, !dbg !746

if.end:                                           ; preds = %if.end.loopexit, %entry
  call void @takeString(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @0, i32 0, i32 0), i32 3), !dbg !746
  call void @RetKernel(), !dbg !746
  ret void, !dbg !746
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.value(metadata, i64, metadata, metadata) #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.x() #1

; Function Attrs: nounwind
define void @_Z12bicg_kernel2PfS_S_(float* nocapture readonly %A, float* nocapture readonly %p, float* nocapture %q) local_unnamed_addr #0 !dbg !747 {
entry:
  tail call void @llvm.dbg.value(metadata float* %A, i64 0, metadata !749, metadata !598), !dbg !756
  tail call void @llvm.dbg.value(metadata float* %p, i64 0, metadata !750, metadata !598), !dbg !757
  tail call void @llvm.dbg.value(metadata float* %q, i64 0, metadata !751, metadata !598), !dbg !758
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !759, !range !639
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #3, !dbg !761, !range !685
  %mul = mul nuw nsw i32 %1, %0, !dbg !764
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !765, !range !716
  %add = add nuw nsw i32 %mul, %2, !dbg !768
  tail call void @llvm.dbg.value(metadata i32 %add, i64 0, metadata !752, metadata !598), !dbg !769
  %cmp = icmp ult i32 %add, 4096, !dbg !770
  br i1 %cmp, label %if.then, label %if.end, !dbg !771

if.then:                                          ; preds = %entry
  %idxprom23 = zext i32 %add to i64, !dbg !772
  %arrayidx = getelementptr inbounds float, float* %q, i64 %idxprom23, !dbg !772
  store float 0.000000e+00, float* %arrayidx, align 4, !dbg !773, !tbaa !723
  call void @_Z6print3i(i32 123), !dbg !773
  call void @_Z6print1i(i32 2), !dbg !773
  %3 = bitcast float* %arrayidx to i8*, !dbg !774
  call void @_Z6print5Pviii(i8* %3, i32 32, i32 123, i32 2), !dbg !773
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !753, metadata !598), !dbg !774
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !753, metadata !598), !dbg !774
  %mul4 = shl i32 %add, 12
  br label %for.body, !dbg !775

for.body:                                         ; preds = %for.body, %if.then
  %4 = phi float [ 0.000000e+00, %if.then ], [ %add13.1, %for.body ], !dbg !778
  %j.024 = phi i32 [ 0, %if.then ], [ %inc.1, %for.body ]
  %add5 = add nuw nsw i32 %j.024, %mul4, !dbg !781
  %idxprom6 = sext i32 %add5 to i64, !dbg !782
  %arrayidx7 = getelementptr inbounds float, float* %A, i64 %idxprom6, !dbg !782
  %5 = load float, float* %arrayidx7, align 4, !dbg !782, !tbaa !723
  call void @_Z6print3i(i32 128), !dbg !782
  call void @_Z6print1i(i32 1), !dbg !782
  %6 = bitcast float* %arrayidx7 to i8*, !dbg !783
  call void @_Z6print5Pviii(i8* %6, i32 32, i32 128, i32 1), !dbg !782
  %idxprom8 = sext i32 %j.024 to i64, !dbg !783
  %arrayidx9 = getelementptr inbounds float, float* %p, i64 %idxprom8, !dbg !783
  %7 = load float, float* %arrayidx9, align 4, !dbg !783, !tbaa !723
  call void @_Z6print3i(i32 128), !dbg !783
  call void @_Z6print1i(i32 1), !dbg !783
  %8 = bitcast float* %arrayidx9 to i8*, !dbg !784
  call void @_Z6print5Pviii(i8* %8, i32 32, i32 128, i32 1), !dbg !783
  %mul10 = fmul float %5, %7, !dbg !784
  %add13 = fadd float %4, %mul10, !dbg !778
  store float %add13, float* %arrayidx, align 4, !dbg !778, !tbaa !723
  call void @_Z6print3i(i32 128), !dbg !778
  call void @_Z6print1i(i32 2), !dbg !778
  %9 = bitcast float* %arrayidx to i8*, !dbg !785
  call void @_Z6print5Pviii(i8* %9, i32 32, i32 128, i32 2), !dbg !778
  %inc = or i32 %j.024, 1, !dbg !785
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !753, metadata !598), !dbg !774
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !753, metadata !598), !dbg !774
  %add5.1 = add nuw nsw i32 %inc, %mul4, !dbg !781
  %idxprom6.1 = sext i32 %add5.1 to i64, !dbg !782
  %arrayidx7.1 = getelementptr inbounds float, float* %A, i64 %idxprom6.1, !dbg !782
  %10 = load float, float* %arrayidx7.1, align 4, !dbg !782, !tbaa !723
  call void @_Z6print3i(i32 128), !dbg !782
  call void @_Z6print1i(i32 1), !dbg !782
  %11 = bitcast float* %arrayidx7.1 to i8*, !dbg !783
  call void @_Z6print5Pviii(i8* %11, i32 32, i32 128, i32 1), !dbg !782
  %idxprom8.1 = sext i32 %inc to i64, !dbg !783
  %arrayidx9.1 = getelementptr inbounds float, float* %p, i64 %idxprom8.1, !dbg !783
  %12 = load float, float* %arrayidx9.1, align 4, !dbg !783, !tbaa !723
  call void @_Z6print3i(i32 128), !dbg !783
  call void @_Z6print1i(i32 1), !dbg !783
  %13 = bitcast float* %arrayidx9.1 to i8*, !dbg !784
  call void @_Z6print5Pviii(i8* %13, i32 32, i32 128, i32 1), !dbg !783
  %mul10.1 = fmul float %10, %12, !dbg !784
  %add13.1 = fadd float %add13, %mul10.1, !dbg !778
  store float %add13.1, float* %arrayidx, align 4, !dbg !778, !tbaa !723
  call void @_Z6print3i(i32 128), !dbg !778
  call void @_Z6print1i(i32 2), !dbg !778
  %14 = bitcast float* %arrayidx to i8*, !dbg !785
  call void @_Z6print5Pviii(i8* %14, i32 32, i32 128, i32 2), !dbg !778
  %inc.1 = add nsw i32 %j.024, 2, !dbg !785
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !753, metadata !598), !dbg !774
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !753, metadata !598), !dbg !774
  %exitcond.1 = icmp eq i32 %inc.1, 4096, !dbg !787
  br i1 %exitcond.1, label %if.end.loopexit, label %for.body, !dbg !775, !llvm.loop !789

if.end.loopexit:                                  ; preds = %for.body
  br label %if.end, !dbg !792

if.end:                                           ; preds = %if.end.loopexit, %entry
  call void @takeString(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @1, i32 0, i32 0), i32 3), !dbg !792
  call void @RetKernel(), !dbg !792
  ret void, !dbg !792
}

; Function Attrs: nounwind
define void @takeString(i8* %p, i32 %action) local_unnamed_addr #0 !dbg !793 {
entry:
  %tmp = alloca %printf_args, align 8
  %tmp13 = alloca %printf_args, align 8
  %tmp17 = alloca %printf_args, align 8
  %tmp19 = alloca %printf_args, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !797, metadata !598), !dbg !799
  tail call void @llvm.dbg.value(metadata i32 %action, i64 0, metadata !798, metadata !598), !dbg !800
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !801, !range !716
  %cmp = icmp eq i32 %0, 0, !dbg !805
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !806

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !807, !range !639
  %cmp2 = icmp eq i32 %1, 0, !dbg !811
  br i1 %cmp2, label %lor.lhs.false3, label %return, !dbg !812

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !813, !range !716
  %cmp5 = icmp eq i32 %2, 0, !dbg !817
  br i1 %cmp5, label %lor.lhs.false6, label %return, !dbg !818

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !819, !range !639
  %cmp8 = icmp eq i32 %3, 0, !dbg !823
  br i1 %cmp8, label %if.end, label %return, !dbg !824

if.end:                                           ; preds = %lor.lhs.false6
  switch i32 %action, label %if.else18 [
    i32 1, label %if.then10
    i32 2, label %if.then12
    i32 3, label %if.then16
  ], !dbg !826

if.then10:                                        ; preds = %if.end
  %4 = getelementptr inbounds %printf_args, %printf_args* %tmp, i64 0, i32 0, !dbg !827
  store i8* %p, i8** %4, align 8, !dbg !827
  %5 = bitcast %printf_args* %tmp to i8*, !dbg !827
  %6 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i64 0, i64 0), i8* nonnull %5) #3, !dbg !827
  br label %return, !dbg !827

if.then12:                                        ; preds = %if.end
  %7 = getelementptr inbounds %printf_args, %printf_args* %tmp13, i64 0, i32 0, !dbg !829
  store i8* %p, i8** %7, align 8, !dbg !829
  %8 = bitcast %printf_args* %tmp13 to i8*, !dbg !829
  %9 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.1, i64 0, i64 0), i8* nonnull %8) #3, !dbg !829
  br label %return, !dbg !829

if.then16:                                        ; preds = %if.end
  %10 = getelementptr inbounds %printf_args, %printf_args* %tmp17, i64 0, i32 0, !dbg !831
  store i8* %p, i8** %10, align 8, !dbg !831
  %11 = bitcast %printf_args* %tmp17 to i8*, !dbg !831
  %12 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i8* nonnull %11) #3, !dbg !831
  br label %return, !dbg !831

if.else18:                                        ; preds = %if.end
  %13 = getelementptr inbounds %printf_args, %printf_args* %tmp19, i64 0, i32 0, !dbg !833
  store i8* %p, i8** %13, align 8, !dbg !833
  %14 = bitcast %printf_args* %tmp19 to i8*, !dbg !833
  %15 = call i32 @vprintf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.3, i64 0, i64 0), i8* nonnull %14) #3, !dbg !833
  br label %return

return:                                           ; preds = %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry, %if.then10, %if.then16, %if.else18, %if.then12
  ret void, !dbg !834
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.y() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #1

declare i32 @vprintf(i8*, i8*) local_unnamed_addr

; Function Attrs: nounwind
define void @_Z10storeLinesPvssi(i8* %p, i16 signext %size, i16 signext %line, i32 %op) local_unnamed_addr #0 !dbg !835 {
entry:
  %tmp = alloca %printf_args.3, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !839, metadata !598), !dbg !846
  tail call void @llvm.dbg.value(metadata i16 %size, i64 0, metadata !840, metadata !598), !dbg !847
  tail call void @llvm.dbg.value(metadata i16 %line, i64 0, metadata !841, metadata !598), !dbg !848
  tail call void @llvm.dbg.value(metadata i32 %op, i64 0, metadata !842, metadata !598), !dbg !849
  tail call void @llvm.dbg.value(metadata i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), i64 0, metadata !850, metadata !598), !dbg !857
  tail call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !856, metadata !598), !dbg !859
  tail call void @llvm.dbg.value(metadata i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), i64 0, metadata !860, metadata !598), !dbg !864
  tail call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !863, metadata !598), !dbg !866
  %0 = atomicrmw add i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), i32 1 seq_cst, !dbg !867
  tail call void @llvm.dbg.value(metadata i32 %0, i64 0, metadata !843, metadata !598), !dbg !868
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !844, metadata !598), !dbg !869
  tail call void @llvm.dbg.value(metadata i64* addrspacecast (i64 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 0, metadata !845, metadata !598), !dbg !870
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !871, !range !639
  %conv = trunc i32 %1 to i16, !dbg !873
  %mul = mul nsw i32 %0, 12, !dbg !874
  %idxprom = sext i32 %mul to i64, !dbg !875
  %arrayidx = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom, !dbg !875
  store i16 %conv, i16* %arrayidx, align 2, !dbg !876, !tbaa !877
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !879, !range !639
  %conv3 = trunc i32 %2 to i16, !dbg !881
  %add5 = or i32 %mul, 1, !dbg !882
  %idxprom6 = sext i32 %add5 to i64, !dbg !883
  %arrayidx7 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom6, !dbg !883
  store i16 %conv3, i16* %arrayidx7, align 2, !dbg !884, !tbaa !877
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !885, !range !716
  %conv9 = trunc i32 %3 to i16, !dbg !887
  %add11 = or i32 %mul, 2, !dbg !888
  %idxprom12 = sext i32 %add11 to i64, !dbg !889
  %arrayidx13 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom12, !dbg !889
  store i16 %conv9, i16* %arrayidx13, align 2, !dbg !890, !tbaa !877
  %4 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !891, !range !716
  %conv15 = trunc i32 %4 to i16, !dbg !893
  %add17 = or i32 %mul, 3, !dbg !894
  %idxprom18 = sext i32 %add17 to i64, !dbg !895
  %arrayidx19 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom18, !dbg !895
  store i16 %conv15, i16* %arrayidx19, align 2, !dbg !896, !tbaa !877
  %5 = ptrtoint i8* %p to i64, !dbg !897
  %mul20 = mul nsw i32 %0, 3, !dbg !898
  %add21 = add nsw i32 %mul20, 1, !dbg !899
  %idxprom22 = sext i32 %add21 to i64, !dbg !900
  %arrayidx23 = getelementptr inbounds i64, i64* addrspacecast (i64 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 %idxprom22, !dbg !900
  store i64 %5, i64* %arrayidx23, align 8, !dbg !901, !tbaa !902
  %add25 = add nsw i32 %mul, 8, !dbg !904
  %idxprom26 = sext i32 %add25 to i64, !dbg !905
  %arrayidx27 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom26, !dbg !905
  store i16 %size, i16* %arrayidx27, align 2, !dbg !906, !tbaa !877
  %add29 = add nsw i32 %mul, 9, !dbg !907
  %idxprom30 = sext i32 %add29 to i64, !dbg !908
  %arrayidx31 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom30, !dbg !908
  store i16 %line, i16* %arrayidx31, align 2, !dbg !909, !tbaa !877
  %mul32 = mul nsw i32 %0, 6, !dbg !910
  %add33 = add nsw i32 %mul32, 5, !dbg !911
  %idxprom34 = sext i32 %add33 to i64, !dbg !912
  %arrayidx3552 = getelementptr inbounds [25165824 x i32], [25165824 x i32] addrspace(1)* @buffer_oN_DeViCe, i64 0, i64 %idxprom34, !dbg !912
  %arrayidx35 = addrspacecast i32 addrspace(1)* %arrayidx3552 to i32*, !dbg !912
  store i32 %op, i32* %arrayidx35, align 4, !dbg !913, !tbaa !914
  %cmp = icmp slt i32 %0, 5, !dbg !916
  br i1 %cmp, label %if.then, label %if.end, !dbg !918

if.then:                                          ; preds = %entry
  %6 = getelementptr inbounds %printf_args.3, %printf_args.3* %tmp, i64 0, i32 0, !dbg !919
  store i32* getelementptr ([25165824 x i32], [25165824 x i32]* addrspacecast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to [25165824 x i32]*), i64 0, i64 0), i32** %6, align 8, !dbg !919
  %7 = bitcast %printf_args.3* %tmp to i8*, !dbg !919
  %8 = call i32 @vprintf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i64 0, i64 0), i8* nonnull %7) #3, !dbg !919
  br label %if.end, !dbg !919

if.end:                                           ; preds = %if.then, %entry
  ret void, !dbg !920
}

; Function Attrs: nounwind
define void @_Z9dumpLinesv() local_unnamed_addr #0 !dbg !921 {
entry:
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !924, !range !716
  %cmp = icmp eq i32 %0, 0, !dbg !927
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !928

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !929, !range !639
  %cmp2 = icmp eq i32 %1, 0, !dbg !932
  br i1 %cmp2, label %lor.lhs.false3, label %return, !dbg !933

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !934, !range !716
  %cmp5 = icmp eq i32 %2, 0, !dbg !937
  br i1 %cmp5, label %lor.lhs.false6, label %return, !dbg !938

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !939, !range !639
  %cmp8 = icmp eq i32 %3, 0, !dbg !942
  br i1 %cmp8, label %for.cond.preheader, label %return, !dbg !943

for.cond.preheader:                               ; preds = %lor.lhs.false6
  %4 = tail call i32 @vprintf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.5, i64 0, i64 0), i8* null) #3, !dbg !945
  br label %return, !dbg !946

return:                                           ; preds = %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry, %for.cond.preheader
  ret void, !dbg !947
}

; Function Attrs: nounwind
define void @_Z6print1i(i32 %a) local_unnamed_addr #0 !dbg !949 {
entry:
  %tmp = alloca %printf_args.4, align 8
  %tmp18 = alloca %printf_args.4, align 8
  tail call void @llvm.dbg.value(metadata i32 %a, i64 0, metadata !951, metadata !598), !dbg !952
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !953, !range !716
  %cmp = icmp eq i32 %0, 0, !dbg !956
  br i1 %cmp, label %lor.lhs.false, label %if.end21, !dbg !957

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !958, !range !716
  %cmp2 = icmp eq i32 %1, 0, !dbg !961
  br i1 %cmp2, label %if.end, label %if.end21, !dbg !962

if.end:                                           ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !964, !range !639
  %cmp4 = icmp eq i32 %2, 0, !dbg !967
  br i1 %cmp4, label %lor.lhs.false5, label %if.end21, !dbg !968

lor.lhs.false5:                                   ; preds = %if.end
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !969, !range !639
  %cmp7 = icmp eq i32 %3, 0, !dbg !972
  br i1 %cmp7, label %if.end9, label %if.end21, !dbg !973

if.end9:                                          ; preds = %lor.lhs.false5
  switch i32 %a, label %if.else19 [
    i32 1, label %if.then11
    i32 2, label %if.then15
  ], !dbg !974

if.then11:                                        ; preds = %if.end9
  %4 = getelementptr inbounds %printf_args.4, %printf_args.4* %tmp, i64 0, i32 0, !dbg !975
  store i32 0, i32* %4, align 8, !dbg !975
  %5 = getelementptr inbounds %printf_args.4, %printf_args.4* %tmp, i64 0, i32 1, !dbg !975
  store i32 0, i32* %5, align 4, !dbg !975
  %6 = bitcast %printf_args.4* %tmp to i8*, !dbg !975
  %7 = call i32 @vprintf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.6, i64 0, i64 0), i8* nonnull %6) #3, !dbg !977
  br label %if.end21, !dbg !975

if.then15:                                        ; preds = %if.end9
  %8 = getelementptr inbounds %printf_args.4, %printf_args.4* %tmp18, i64 0, i32 0, !dbg !979
  store i32 0, i32* %8, align 8, !dbg !979
  %9 = getelementptr inbounds %printf_args.4, %printf_args.4* %tmp18, i64 0, i32 1, !dbg !979
  store i32 0, i32* %9, align 4, !dbg !979
  %10 = bitcast %printf_args.4* %tmp18 to i8*, !dbg !979
  %11 = call i32 @vprintf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %10) #3, !dbg !981
  br label %if.end21, !dbg !979

if.else19:                                        ; preds = %if.end9
  %12 = tail call i32 @vprintf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.8, i64 0, i64 0), i8* null) #3, !dbg !983
  br label %if.end21

if.end21:                                         ; preds = %lor.lhs.false5, %if.end, %lor.lhs.false, %entry, %if.then15, %if.else19, %if.then11
  ret void, !dbg !984
}

; Function Attrs: nounwind
define void @_Z6print2v() local_unnamed_addr #0 !dbg !985 {
entry:
  %tmp = alloca %printf_args.4, align 8
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !986, !range !716
  %cmp = icmp eq i32 %0, 0, !dbg !989
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !990

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !991, !range !716
  %cmp2 = icmp eq i32 %1, 0, !dbg !994
  br i1 %cmp2, label %if.end, label %return, !dbg !995

if.end:                                           ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !997, !range !639
  %cmp4 = icmp eq i32 %2, 0, !dbg !1000
  br i1 %cmp4, label %lor.lhs.false5, label %return, !dbg !1001

lor.lhs.false5:                                   ; preds = %if.end
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !1002, !range !639
  %cmp7 = icmp eq i32 %3, 0, !dbg !1005
  br i1 %cmp7, label %if.end9, label %return, !dbg !1006

if.end9:                                          ; preds = %lor.lhs.false5
  %4 = getelementptr inbounds %printf_args.4, %printf_args.4* %tmp, i64 0, i32 0, !dbg !1007
  store i32 0, i32* %4, align 8, !dbg !1007
  %5 = getelementptr inbounds %printf_args.4, %printf_args.4* %tmp, i64 0, i32 1, !dbg !1007
  store i32 0, i32* %5, align 4, !dbg !1007
  %6 = bitcast %printf_args.4* %tmp to i8*, !dbg !1007
  %7 = call i32 @vprintf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %6) #3, !dbg !1008
  br label %return, !dbg !1010

return:                                           ; preds = %lor.lhs.false5, %if.end, %lor.lhs.false, %entry, %if.end9
  ret void, !dbg !1011
}

; Function Attrs: nounwind
define void @_Z6print3i(i32 %a) local_unnamed_addr #0 !dbg !1012 {
entry:
  %tmp = alloca %printf_args.7, align 8
  tail call void @llvm.dbg.value(metadata i32 %a, i64 0, metadata !1014, metadata !598), !dbg !1015
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !1016, !range !716
  %cmp = icmp eq i32 %0, 0, !dbg !1019
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !1020

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !1021, !range !716
  %cmp2 = icmp eq i32 %1, 0, !dbg !1024
  br i1 %cmp2, label %if.end, label %return, !dbg !1025

if.end:                                           ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !1027, !range !639
  %cmp4 = icmp eq i32 %2, 0, !dbg !1030
  br i1 %cmp4, label %lor.lhs.false5, label %return, !dbg !1031

lor.lhs.false5:                                   ; preds = %if.end
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !1032, !range !639
  %cmp7 = icmp eq i32 %3, 0, !dbg !1035
  br i1 %cmp7, label %if.end9, label %return, !dbg !1036

if.end9:                                          ; preds = %lor.lhs.false5
  %4 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp, i64 0, i32 0, !dbg !1037
  store i32 %a, i32* %4, align 8, !dbg !1037
  %5 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp, i64 0, i32 1, !dbg !1037
  store i32 0, i32* %5, align 4, !dbg !1037
  %6 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp, i64 0, i32 2, !dbg !1037
  store i32 0, i32* %6, align 8, !dbg !1037
  %7 = bitcast %printf_args.7* %tmp to i8*, !dbg !1037
  %8 = call i32 @vprintf(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.9, i64 0, i64 0), i8* nonnull %7) #3, !dbg !1038
  br label %return, !dbg !1040

return:                                           ; preds = %lor.lhs.false5, %if.end, %lor.lhs.false, %entry, %if.end9
  ret void, !dbg !1041
}

; Function Attrs: nounwind
define void @_Z6print5Pviii(i8* %p, i32 %bits, i32 %line, i32 %op) local_unnamed_addr #0 !dbg !1042 {
entry:
  %tmp.i = alloca %printf_args.3, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1046, metadata !598), !dbg !1050
  tail call void @llvm.dbg.value(metadata i32 %bits, i64 0, metadata !1047, metadata !598), !dbg !1051
  tail call void @llvm.dbg.value(metadata i32 %line, i64 0, metadata !1048, metadata !598), !dbg !1052
  tail call void @llvm.dbg.value(metadata i32 %op, i64 0, metadata !1049, metadata !598), !dbg !1053
  %div = sdiv i32 %bits, 8, !dbg !1054
  %conv = trunc i32 %div to i16, !dbg !1055
  %conv1 = trunc i32 %line to i16, !dbg !1056
  %0 = bitcast %printf_args.3* %tmp.i to i8*, !dbg !1057
  call void @llvm.lifetime.start(i64 8, i8* nonnull %0), !dbg !1057
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !839, metadata !598) #3, !dbg !1057
  tail call void @llvm.dbg.value(metadata i16 %conv, i64 0, metadata !840, metadata !598) #3, !dbg !1059
  tail call void @llvm.dbg.value(metadata i16 %conv1, i64 0, metadata !841, metadata !598) #3, !dbg !1060
  tail call void @llvm.dbg.value(metadata i32 %op, i64 0, metadata !842, metadata !598) #3, !dbg !1061
  tail call void @llvm.dbg.value(metadata i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), i64 0, metadata !850, metadata !598) #3, !dbg !1062
  tail call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !856, metadata !598) #3, !dbg !1064
  tail call void @llvm.dbg.value(metadata i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), i64 0, metadata !860, metadata !598) #3, !dbg !1065
  tail call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !863, metadata !598) #3, !dbg !1067
  %1 = atomicrmw add i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), i32 1 seq_cst, !dbg !1068
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !843, metadata !598) #3, !dbg !1069
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !844, metadata !598) #3, !dbg !1070
  tail call void @llvm.dbg.value(metadata i64* addrspacecast (i64 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 0, metadata !845, metadata !598) #3, !dbg !1071
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !1072, !range !639
  %conv.i = trunc i32 %2 to i16, !dbg !1074
  %mul.i = mul nsw i32 %1, 12, !dbg !1075
  %idxprom.i = sext i32 %mul.i to i64, !dbg !1076
  %arrayidx.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom.i, !dbg !1076
  store i16 %conv.i, i16* %arrayidx.i, align 2, !dbg !1077, !tbaa !877
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !1078, !range !639
  %conv3.i = trunc i32 %3 to i16, !dbg !1080
  %add5.i = or i32 %mul.i, 1, !dbg !1081
  %idxprom6.i = sext i32 %add5.i to i64, !dbg !1082
  %arrayidx7.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom6.i, !dbg !1082
  store i16 %conv3.i, i16* %arrayidx7.i, align 2, !dbg !1083, !tbaa !877
  %4 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !1084, !range !716
  %conv9.i = trunc i32 %4 to i16, !dbg !1086
  %add11.i = or i32 %mul.i, 2, !dbg !1087
  %idxprom12.i = sext i32 %add11.i to i64, !dbg !1088
  %arrayidx13.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom12.i, !dbg !1088
  store i16 %conv9.i, i16* %arrayidx13.i, align 2, !dbg !1089, !tbaa !877
  %5 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !1090, !range !716
  %conv15.i = trunc i32 %5 to i16, !dbg !1092
  %add17.i = or i32 %mul.i, 3, !dbg !1093
  %idxprom18.i = sext i32 %add17.i to i64, !dbg !1094
  %arrayidx19.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom18.i, !dbg !1094
  store i16 %conv15.i, i16* %arrayidx19.i, align 2, !dbg !1095, !tbaa !877
  %6 = ptrtoint i8* %p to i64, !dbg !1096
  %mul20.i = mul nsw i32 %1, 3, !dbg !1097
  %add21.i = add nsw i32 %mul20.i, 1, !dbg !1098
  %idxprom22.i = sext i32 %add21.i to i64, !dbg !1099
  %arrayidx23.i = getelementptr inbounds i64, i64* addrspacecast (i64 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 %idxprom22.i, !dbg !1099
  store i64 %6, i64* %arrayidx23.i, align 8, !dbg !1100, !tbaa !902
  %add25.i = add nsw i32 %mul.i, 8, !dbg !1101
  %idxprom26.i = sext i32 %add25.i to i64, !dbg !1102
  %arrayidx27.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom26.i, !dbg !1102
  store i16 %conv, i16* %arrayidx27.i, align 2, !dbg !1103, !tbaa !877
  %add29.i = add nsw i32 %mul.i, 9, !dbg !1104
  %idxprom30.i = sext i32 %add29.i to i64, !dbg !1105
  %arrayidx31.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom30.i, !dbg !1105
  store i16 %conv1, i16* %arrayidx31.i, align 2, !dbg !1106, !tbaa !877
  %mul32.i = mul nsw i32 %1, 6, !dbg !1107
  %add33.i = add nsw i32 %mul32.i, 5, !dbg !1108
  %idxprom34.i = sext i32 %add33.i to i64, !dbg !1109
  %arrayidx3552.i = getelementptr inbounds [25165824 x i32], [25165824 x i32] addrspace(1)* @buffer_oN_DeViCe, i64 0, i64 %idxprom34.i, !dbg !1109
  %arrayidx35.i = addrspacecast i32 addrspace(1)* %arrayidx3552.i to i32*, !dbg !1109
  store i32 %op, i32* %arrayidx35.i, align 4, !dbg !1110, !tbaa !914
  %cmp.i = icmp slt i32 %1, 5, !dbg !1111
  br i1 %cmp.i, label %if.then.i, label %_Z10storeLinesPvssi.exit, !dbg !1112

if.then.i:                                        ; preds = %entry
  %7 = getelementptr inbounds %printf_args.3, %printf_args.3* %tmp.i, i64 0, i32 0, !dbg !1113
  store i32* getelementptr ([25165824 x i32], [25165824 x i32]* addrspacecast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to [25165824 x i32]*), i64 0, i64 0), i32** %7, align 8, !dbg !1113
  %8 = call i32 @vprintf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i64 0, i64 0), i8* nonnull %0) #3, !dbg !1113
  br label %_Z10storeLinesPvssi.exit, !dbg !1113

_Z10storeLinesPvssi.exit:                         ; preds = %entry, %if.then.i
  call void @llvm.lifetime.end(i64 8, i8* nonnull %0), !dbg !1114
  ret void, !dbg !1115
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #2

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #2

; Function Attrs: nounwind
define void @RetKernel() local_unnamed_addr #0 !dbg !1116 {
entry:
  %tmp = alloca %printf_args.8, align 8
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3, !dbg !1119, !range !716
  %cmp = icmp eq i32 %0, 0, !dbg !1122
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !1123

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3, !dbg !1124, !range !639
  %cmp2 = icmp eq i32 %1, 0, !dbg !1127
  br i1 %cmp2, label %lor.lhs.false3, label %return, !dbg !1128

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #3, !dbg !1129, !range !716
  %cmp5 = icmp eq i32 %2, 0, !dbg !1132
  br i1 %cmp5, label %lor.lhs.false6, label %return, !dbg !1133

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #3, !dbg !1134, !range !639
  %cmp8 = icmp eq i32 %3, 0, !dbg !1137
  br i1 %cmp8, label %if.end, label %return, !dbg !1138

if.end:                                           ; preds = %lor.lhs.false6
  %4 = load i32, i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), align 4, !dbg !1140, !tbaa !914
  store i32 %4, i32* getelementptr inbounds ([25165824 x i32], [25165824 x i32]* addrspacecast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to [25165824 x i32]*), i64 0, i64 5), align 4, !dbg !1141, !tbaa !914
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !1118, metadata !598), !dbg !1142
  %5 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #3, !dbg !1143, !range !685
  %conv = trunc i32 %5 to i16, !dbg !1146
  store i16 %conv, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), align 4, !dbg !1147, !tbaa !877
  %6 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.y() #3, !dbg !1148, !range !685
  %conv11 = trunc i32 %6 to i16, !dbg !1151
  store i16 %conv11, i16* getelementptr (i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 1), align 2, !dbg !1152, !tbaa !877
  %7 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #3, !dbg !1153, !range !1181
  %conv14 = trunc i32 %7 to i16, !dbg !1182
  store i16 %conv14, i16* bitcast (i32* getelementptr ([25165824 x i32], [25165824 x i32]* addrspacecast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to [25165824 x i32]*), i64 0, i64 1) to i16*), align 2, !dbg !1183, !tbaa !877
  %8 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.y() #3, !dbg !1184, !range !1181
  %conv17 = trunc i32 %8 to i16, !dbg !1187
  store i16 %conv17, i16* getelementptr (i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([25165824 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 3), align 2, !dbg !1188, !tbaa !877
  %9 = getelementptr inbounds %printf_args.8, %printf_args.8* %tmp, i64 0, i32 0, !dbg !1189
  store i32 %4, i32* %9, align 8, !dbg !1189
  %10 = bitcast %printf_args.8* %tmp to i8*, !dbg !1189
  %11 = call i32 @vprintf(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.10, i64 0, i64 0), i8* nonnull %10) #3, !dbg !1189
  store i32 1, i32* addrspacecast (i32 addrspace(1)* @ccnntt to i32*), align 4, !dbg !1190, !tbaa !914
  br label %return, !dbg !1191

return:                                           ; preds = %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry, %if.end
  ret void, !dbg !1192
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.y() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.y() #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { argmemonly nounwind }
attributes #3 = { nounwind }

!llvm.dbg.cu = !{!572, !2}
!nvvm.annotations = !{!574, !575, !576, !577, !576, !578, !578, !578, !578, !579, !579, !578, !576, !577, !576, !578, !578, !578, !578, !579, !579, !578}
!llvm.ident = !{!580, !580}
!nvvm.internalize.after.link = !{}
!nvvmir.version = !{!581, !581}
!llvm.module.flags = !{!582, !583, !584}

!0 = !DIGlobalVariableExpression(var: !1)
!1 = distinct !DIGlobalVariable(name: "ccnntt", scope: !2, file: !3, line: 25, type: !8, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C_plus_plus, file: !3, producer: "clang version 5.0.0 (trunk 294196)", isOptimized: true, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, retainedTypes: !5, globals: !15, imports: !16)
!3 = !DIFile(filename: "/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/ansf.cu", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!4 = !{}
!5 = !{!6, !8, !9, !11, !12, !10, !13}
!6 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !7, size: 64)
!7 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!8 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!9 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !10, size: 64)
!10 = !DIBasicType(name: "short", size: 16, encoding: DW_ATE_signed)
!11 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !12, size: 64)
!12 = !DIBasicType(name: "long int", size: 64, encoding: DW_ATE_signed)
!13 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !14, size: 64)
!14 = !DIDerivedType(tag: DW_TAG_volatile_type, baseType: !8)
!15 = !{!0}
!16 = !{!17, !24, !29, !31, !33, !35, !37, !41, !43, !45, !47, !49, !51, !53, !55, !57, !59, !61, !63, !65, !67, !69, !73, !75, !77, !79, !83, !88, !90, !92, !97, !101, !103, !105, !107, !109, !111, !113, !115, !117, !121, !125, !127, !129, !133, !135, !137, !139, !141, !143, !147, !149, !151, !156, !163, !167, !169, !171, !175, !177, !179, !183, !185, !187, !191, !193, !195, !197, !199, !201, !203, !205, !207, !209, !214, !216, !218, !222, !224, !226, !228, !230, !232, !234, !236, !240, !244, !246, !248, !253, !255, !257, !259, !261, !263, !265, !269, !275, !279, !283, !288, !291, !295, !299, !314, !318, !322, !326, !330, !334, !336, !340, !344, !348, !356, !360, !364, !368, !372, !377, !383, !387, !391, !393, !401, !405, !413, !415, !417, !421, !425, !429, !434, !438, !443, !444, !445, !446, !449, !450, !451, !452, !453, !454, !455, !458, !460, !462, !464, !466, !468, !470, !472, !475, !477, !479, !481, !483, !485, !487, !489, !491, !493, !495, !497, !499, !501, !503, !505, !507, !509, !511, !513, !515, !517, !519, !521, !523, !525, !527, !529, !531, !533, !535, !537, !539, !543, !544, !546, !548, !550, !552, !554, !556, !558, !560, !562, !564, !566, !568, !570}
!17 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !20, line: 201)
!18 = !DINamespace(name: "std", scope: null, file: !19, line: 195)
!19 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_math_forward_declares.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!20 = !DISubprogram(name: "abs", linkageName: "_ZL3absx", scope: !19, file: !19, line: 44, type: !21, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!21 = !DISubroutineType(types: !22)
!22 = !{!23, !23}
!23 = !DIBasicType(name: "long long int", size: 64, encoding: DW_ATE_signed)
!24 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !25, line: 202)
!25 = !DISubprogram(name: "acos", linkageName: "_ZL4acosf", scope: !19, file: !19, line: 46, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!26 = !DISubroutineType(types: !27)
!27 = !{!28, !28}
!28 = !DIBasicType(name: "float", size: 32, encoding: DW_ATE_float)
!29 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !30, line: 203)
!30 = !DISubprogram(name: "acosh", linkageName: "_ZL5acoshf", scope: !19, file: !19, line: 48, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!31 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !32, line: 204)
!32 = !DISubprogram(name: "asin", linkageName: "_ZL4asinf", scope: !19, file: !19, line: 50, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!33 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !34, line: 205)
!34 = !DISubprogram(name: "asinh", linkageName: "_ZL5asinhf", scope: !19, file: !19, line: 52, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!35 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !36, line: 206)
!36 = !DISubprogram(name: "atan", linkageName: "_ZL4atanf", scope: !19, file: !19, line: 56, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!37 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !38, line: 207)
!38 = !DISubprogram(name: "atan2", linkageName: "_ZL5atan2ff", scope: !19, file: !19, line: 54, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!39 = !DISubroutineType(types: !40)
!40 = !{!28, !28, !28}
!41 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !42, line: 208)
!42 = !DISubprogram(name: "atanh", linkageName: "_ZL5atanhf", scope: !19, file: !19, line: 58, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!43 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !44, line: 209)
!44 = !DISubprogram(name: "cbrt", linkageName: "_ZL4cbrtf", scope: !19, file: !19, line: 60, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!45 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !46, line: 210)
!46 = !DISubprogram(name: "ceil", linkageName: "_ZL4ceilf", scope: !19, file: !19, line: 62, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!47 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !48, line: 211)
!48 = !DISubprogram(name: "copysign", linkageName: "_ZL8copysignff", scope: !19, file: !19, line: 64, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!49 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !50, line: 212)
!50 = !DISubprogram(name: "cos", linkageName: "_ZL3cosf", scope: !19, file: !19, line: 66, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!51 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !52, line: 213)
!52 = !DISubprogram(name: "cosh", linkageName: "_ZL4coshf", scope: !19, file: !19, line: 68, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!53 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !54, line: 214)
!54 = !DISubprogram(name: "erf", linkageName: "_ZL3erff", scope: !19, file: !19, line: 72, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!55 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !56, line: 215)
!56 = !DISubprogram(name: "erfc", linkageName: "_ZL4erfcf", scope: !19, file: !19, line: 70, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!57 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !58, line: 216)
!58 = !DISubprogram(name: "exp", linkageName: "_ZL3expf", scope: !19, file: !19, line: 76, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!59 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !60, line: 217)
!60 = !DISubprogram(name: "exp2", linkageName: "_ZL4exp2f", scope: !19, file: !19, line: 74, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!61 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !62, line: 218)
!62 = !DISubprogram(name: "expm1", linkageName: "_ZL5expm1f", scope: !19, file: !19, line: 78, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!63 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !64, line: 219)
!64 = !DISubprogram(name: "fabs", linkageName: "_ZL4fabsf", scope: !19, file: !19, line: 80, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!65 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !66, line: 220)
!66 = !DISubprogram(name: "fdim", linkageName: "_ZL4fdimff", scope: !19, file: !19, line: 82, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!67 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !68, line: 221)
!68 = !DISubprogram(name: "floor", linkageName: "_ZL5floorf", scope: !19, file: !19, line: 84, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!69 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !70, line: 222)
!70 = !DISubprogram(name: "fma", linkageName: "_ZL3fmafff", scope: !19, file: !19, line: 86, type: !71, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!71 = !DISubroutineType(types: !72)
!72 = !{!28, !28, !28, !28}
!73 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !74, line: 223)
!74 = !DISubprogram(name: "fmax", linkageName: "_ZL4fmaxff", scope: !19, file: !19, line: 88, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!75 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !76, line: 224)
!76 = !DISubprogram(name: "fmin", linkageName: "_ZL4fminff", scope: !19, file: !19, line: 90, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!77 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !78, line: 225)
!78 = !DISubprogram(name: "fmod", linkageName: "_ZL4fmodff", scope: !19, file: !19, line: 92, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!79 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !80, line: 226)
!80 = !DISubprogram(name: "fpclassify", linkageName: "_ZL10fpclassifyf", scope: !19, file: !19, line: 94, type: !81, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!81 = !DISubroutineType(types: !82)
!82 = !{!8, !28}
!83 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !84, line: 227)
!84 = !DISubprogram(name: "frexp", linkageName: "_ZL5frexpfPi", scope: !19, file: !19, line: 96, type: !85, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!85 = !DISubroutineType(types: !86)
!86 = !{!28, !28, !87}
!87 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !8, size: 64)
!88 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !89, line: 228)
!89 = !DISubprogram(name: "hypot", linkageName: "_ZL5hypotff", scope: !19, file: !19, line: 98, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!90 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !91, line: 229)
!91 = !DISubprogram(name: "ilogb", linkageName: "_ZL5ilogbf", scope: !19, file: !19, line: 100, type: !81, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!92 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !93, line: 230)
!93 = !DISubprogram(name: "isfinite", linkageName: "_ZL8isfinitef", scope: !19, file: !19, line: 102, type: !94, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!94 = !DISubroutineType(types: !95)
!95 = !{!96, !28}
!96 = !DIBasicType(name: "bool", size: 8, encoding: DW_ATE_boolean)
!97 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !98, line: 231)
!98 = !DISubprogram(name: "isgreater", linkageName: "_ZL9isgreaterff", scope: !19, file: !19, line: 106, type: !99, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!99 = !DISubroutineType(types: !100)
!100 = !{!96, !28, !28}
!101 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !102, line: 232)
!102 = !DISubprogram(name: "isgreaterequal", linkageName: "_ZL14isgreaterequalff", scope: !19, file: !19, line: 105, type: !99, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!103 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !104, line: 233)
!104 = !DISubprogram(name: "isinf", linkageName: "_ZL5isinff", scope: !19, file: !19, line: 108, type: !94, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!105 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !106, line: 234)
!106 = !DISubprogram(name: "isless", linkageName: "_ZL6islessff", scope: !19, file: !19, line: 112, type: !99, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!107 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !108, line: 235)
!108 = !DISubprogram(name: "islessequal", linkageName: "_ZL11islessequalff", scope: !19, file: !19, line: 111, type: !99, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!109 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !110, line: 236)
!110 = !DISubprogram(name: "islessgreater", linkageName: "_ZL13islessgreaterff", scope: !19, file: !19, line: 114, type: !99, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!111 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !112, line: 237)
!112 = !DISubprogram(name: "isnan", linkageName: "_ZL5isnanf", scope: !19, file: !19, line: 116, type: !94, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!113 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !114, line: 238)
!114 = !DISubprogram(name: "isnormal", linkageName: "_ZL8isnormalf", scope: !19, file: !19, line: 118, type: !94, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!115 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !116, line: 239)
!116 = !DISubprogram(name: "isunordered", linkageName: "_ZL11isunorderedff", scope: !19, file: !19, line: 120, type: !99, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!117 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !118, line: 240)
!118 = !DISubprogram(name: "labs", linkageName: "_ZL4labsl", scope: !19, file: !19, line: 121, type: !119, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!119 = !DISubroutineType(types: !120)
!120 = !{!12, !12}
!121 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !122, line: 241)
!122 = !DISubprogram(name: "ldexp", linkageName: "_ZL5ldexpfi", scope: !19, file: !19, line: 123, type: !123, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!123 = !DISubroutineType(types: !124)
!124 = !{!28, !28, !8}
!125 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !126, line: 242)
!126 = !DISubprogram(name: "lgamma", linkageName: "_ZL6lgammaf", scope: !19, file: !19, line: 125, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!127 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !128, line: 243)
!128 = !DISubprogram(name: "llabs", linkageName: "_ZL5llabsx", scope: !19, file: !19, line: 126, type: !21, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!129 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !130, line: 244)
!130 = !DISubprogram(name: "llrint", linkageName: "_ZL6llrintf", scope: !19, file: !19, line: 128, type: !131, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!131 = !DISubroutineType(types: !132)
!132 = !{!23, !28}
!133 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !134, line: 245)
!134 = !DISubprogram(name: "log", linkageName: "_ZL3logf", scope: !19, file: !19, line: 138, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!135 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !136, line: 246)
!136 = !DISubprogram(name: "log10", linkageName: "_ZL5log10f", scope: !19, file: !19, line: 130, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!137 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !138, line: 247)
!138 = !DISubprogram(name: "log1p", linkageName: "_ZL5log1pf", scope: !19, file: !19, line: 132, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!139 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !140, line: 248)
!140 = !DISubprogram(name: "log2", linkageName: "_ZL4log2f", scope: !19, file: !19, line: 134, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!141 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !142, line: 249)
!142 = !DISubprogram(name: "logb", linkageName: "_ZL4logbf", scope: !19, file: !19, line: 136, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!143 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !144, line: 250)
!144 = !DISubprogram(name: "lrint", linkageName: "_ZL5lrintf", scope: !19, file: !19, line: 140, type: !145, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!145 = !DISubroutineType(types: !146)
!146 = !{!12, !28}
!147 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !148, line: 251)
!148 = !DISubprogram(name: "lround", linkageName: "_ZL6lroundf", scope: !19, file: !19, line: 142, type: !145, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!149 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !150, line: 252)
!150 = !DISubprogram(name: "llround", linkageName: "_ZL7llroundf", scope: !19, file: !19, line: 143, type: !131, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!151 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !152, line: 253)
!152 = !DISubprogram(name: "modf", linkageName: "_ZL4modffPf", scope: !19, file: !19, line: 145, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!153 = !DISubroutineType(types: !154)
!154 = !{!28, !28, !155}
!155 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !28, size: 64)
!156 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !157, line: 254)
!157 = !DISubprogram(name: "nan", linkageName: "_ZL3nanPKc", scope: !19, file: !19, line: 146, type: !158, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!158 = !DISubroutineType(types: !159)
!159 = !{!160, !161}
!160 = !DIBasicType(name: "double", size: 64, encoding: DW_ATE_float)
!161 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !162, size: 64)
!162 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !7)
!163 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !164, line: 255)
!164 = !DISubprogram(name: "nanf", linkageName: "_ZL4nanfPKc", scope: !19, file: !19, line: 147, type: !165, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!165 = !DISubroutineType(types: !166)
!166 = !{!28, !161}
!167 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !168, line: 256)
!168 = !DISubprogram(name: "nearbyint", linkageName: "_ZL9nearbyintf", scope: !19, file: !19, line: 149, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!169 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !170, line: 257)
!170 = !DISubprogram(name: "nextafter", linkageName: "_ZL9nextafterff", scope: !19, file: !19, line: 151, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!171 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !172, line: 258)
!172 = !DISubprogram(name: "nexttoward", linkageName: "_ZL10nexttowardfd", scope: !19, file: !19, line: 153, type: !173, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!173 = !DISubroutineType(types: !174)
!174 = !{!28, !28, !160}
!175 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !176, line: 259)
!176 = !DISubprogram(name: "pow", linkageName: "_ZL3powfi", scope: !19, file: !19, line: 158, type: !123, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!177 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !178, line: 260)
!178 = !DISubprogram(name: "remainder", linkageName: "_ZL9remainderff", scope: !19, file: !19, line: 160, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!179 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !180, line: 261)
!180 = !DISubprogram(name: "remquo", linkageName: "_ZL6remquoffPi", scope: !19, file: !19, line: 162, type: !181, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!181 = !DISubroutineType(types: !182)
!182 = !{!28, !28, !28, !87}
!183 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !184, line: 262)
!184 = !DISubprogram(name: "rint", linkageName: "_ZL4rintf", scope: !19, file: !19, line: 164, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!185 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !186, line: 263)
!186 = !DISubprogram(name: "round", linkageName: "_ZL5roundf", scope: !19, file: !19, line: 166, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!187 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !188, line: 264)
!188 = !DISubprogram(name: "scalbln", linkageName: "_ZL7scalblnfl", scope: !19, file: !19, line: 168, type: !189, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!189 = !DISubroutineType(types: !190)
!190 = !{!28, !28, !12}
!191 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !192, line: 265)
!192 = !DISubprogram(name: "scalbn", linkageName: "_ZL6scalbnfi", scope: !19, file: !19, line: 170, type: !123, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!193 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !194, line: 266)
!194 = !DISubprogram(name: "signbit", linkageName: "_ZL7signbitf", scope: !19, file: !19, line: 172, type: !94, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!195 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !196, line: 267)
!196 = !DISubprogram(name: "sin", linkageName: "_ZL3sinf", scope: !19, file: !19, line: 174, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!197 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !198, line: 268)
!198 = !DISubprogram(name: "sinh", linkageName: "_ZL4sinhf", scope: !19, file: !19, line: 176, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!199 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !200, line: 269)
!200 = !DISubprogram(name: "sqrt", linkageName: "_ZL4sqrtf", scope: !19, file: !19, line: 178, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!201 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !202, line: 270)
!202 = !DISubprogram(name: "tan", linkageName: "_ZL3tanf", scope: !19, file: !19, line: 180, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!203 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !204, line: 271)
!204 = !DISubprogram(name: "tanh", linkageName: "_ZL4tanhf", scope: !19, file: !19, line: 182, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!205 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !206, line: 272)
!206 = !DISubprogram(name: "tgamma", linkageName: "_ZL6tgammaf", scope: !19, file: !19, line: 184, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!207 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !208, line: 273)
!208 = !DISubprogram(name: "trunc", linkageName: "_ZL5truncf", scope: !19, file: !19, line: 186, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!209 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !210, line: 102)
!210 = !DISubprogram(name: "acos", scope: !211, file: !211, line: 54, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!211 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/mathcalls.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!212 = !DISubroutineType(types: !213)
!213 = !{!160, !160}
!214 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !215, line: 121)
!215 = !DISubprogram(name: "asin", scope: !211, file: !211, line: 56, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!216 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !217, line: 140)
!217 = !DISubprogram(name: "atan", scope: !211, file: !211, line: 58, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!218 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !219, line: 159)
!219 = !DISubprogram(name: "atan2", scope: !211, file: !211, line: 60, type: !220, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!220 = !DISubroutineType(types: !221)
!221 = !{!160, !160, !160}
!222 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !223, line: 180)
!223 = !DISubprogram(name: "ceil", scope: !211, file: !211, line: 178, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!224 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !225, line: 199)
!225 = !DISubprogram(name: "cos", scope: !211, file: !211, line: 63, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!226 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !227, line: 218)
!227 = !DISubprogram(name: "cosh", scope: !211, file: !211, line: 72, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!228 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !229, line: 237)
!229 = !DISubprogram(name: "exp", scope: !211, file: !211, line: 100, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!230 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !231, line: 256)
!231 = !DISubprogram(name: "fabs", scope: !211, file: !211, line: 181, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!232 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !233, line: 275)
!233 = !DISubprogram(name: "floor", scope: !211, file: !211, line: 184, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!234 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !235, line: 294)
!235 = !DISubprogram(name: "fmod", scope: !211, file: !211, line: 187, type: !220, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!236 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !237, line: 315)
!237 = !DISubprogram(name: "frexp", scope: !211, file: !211, line: 103, type: !238, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!238 = !DISubroutineType(types: !239)
!239 = !{!160, !160, !87}
!240 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !241, line: 334)
!241 = !DISubprogram(name: "ldexp", scope: !211, file: !211, line: 106, type: !242, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!242 = !DISubroutineType(types: !243)
!243 = !{!160, !160, !8}
!244 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !245, line: 353)
!245 = !DISubprogram(name: "log", scope: !211, file: !211, line: 109, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!246 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !247, line: 372)
!247 = !DISubprogram(name: "log10", scope: !211, file: !211, line: 112, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!248 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !249, line: 391)
!249 = !DISubprogram(name: "modf", scope: !211, file: !211, line: 115, type: !250, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!250 = !DISubroutineType(types: !251)
!251 = !{!160, !160, !252}
!252 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !160, size: 64)
!253 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !254, line: 403)
!254 = !DISubprogram(name: "pow", scope: !211, file: !211, line: 153, type: !220, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!255 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !256, line: 440)
!256 = !DISubprogram(name: "sin", scope: !211, file: !211, line: 65, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!257 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !258, line: 459)
!258 = !DISubprogram(name: "sinh", scope: !211, file: !211, line: 74, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!259 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !260, line: 478)
!260 = !DISubprogram(name: "sqrt", scope: !211, file: !211, line: 156, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!261 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !262, line: 497)
!262 = !DISubprogram(name: "tan", scope: !211, file: !211, line: 67, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!263 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !264, line: 516)
!264 = !DISubprogram(name: "tanh", scope: !211, file: !211, line: 76, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!265 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !266, line: 118)
!266 = !DIDerivedType(tag: DW_TAG_typedef, name: "div_t", file: !267, line: 101, baseType: !268)
!267 = !DIFile(filename: "/usr/include/stdlib.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!268 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !267, line: 97, flags: DIFlagFwdDecl, identifier: "_ZTS5div_t")
!269 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !270, line: 119)
!270 = !DIDerivedType(tag: DW_TAG_typedef, name: "ldiv_t", file: !267, line: 109, baseType: !271)
!271 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !267, line: 105, size: 128, elements: !272, identifier: "_ZTS6ldiv_t")
!272 = !{!273, !274}
!273 = !DIDerivedType(tag: DW_TAG_member, name: "quot", scope: !271, file: !267, line: 107, baseType: !12, size: 64)
!274 = !DIDerivedType(tag: DW_TAG_member, name: "rem", scope: !271, file: !267, line: 108, baseType: !12, size: 64, offset: 64)
!275 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !276, line: 121)
!276 = !DISubprogram(name: "abort", scope: !267, file: !267, line: 515, type: !277, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!277 = !DISubroutineType(types: !278)
!278 = !{null}
!279 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !280, line: 122)
!280 = !DISubprogram(name: "abs", scope: !267, file: !267, line: 775, type: !281, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!281 = !DISubroutineType(types: !282)
!282 = !{!8, !8}
!283 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !284, line: 123)
!284 = !DISubprogram(name: "atexit", scope: !267, file: !267, line: 519, type: !285, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!285 = !DISubroutineType(types: !286)
!286 = !{!8, !287}
!287 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !277, size: 64)
!288 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !289, line: 129)
!289 = !DISubprogram(name: "atof", scope: !290, file: !290, line: 26, type: !158, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!290 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/stdlib-float.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!291 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !292, line: 130)
!292 = !DISubprogram(name: "atoi", scope: !267, file: !267, line: 278, type: !293, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!293 = !DISubroutineType(types: !294)
!294 = !{!8, !161}
!295 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !296, line: 131)
!296 = !DISubprogram(name: "atol", scope: !267, file: !267, line: 283, type: !297, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!297 = !DISubroutineType(types: !298)
!298 = !{!12, !161}
!299 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !300, line: 132)
!300 = !DISubprogram(name: "bsearch", scope: !301, file: !301, line: 20, type: !302, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!301 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/stdlib-bsearch.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!302 = !DISubroutineType(types: !303)
!303 = !{!304, !305, !305, !307, !307, !310}
!304 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 64)
!305 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !306, size: 64)
!306 = !DIDerivedType(tag: DW_TAG_const_type, baseType: null)
!307 = !DIDerivedType(tag: DW_TAG_typedef, name: "size_t", file: !308, line: 62, baseType: !309)
!308 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/stddef.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!309 = !DIBasicType(name: "long unsigned int", size: 64, encoding: DW_ATE_unsigned)
!310 = !DIDerivedType(tag: DW_TAG_typedef, name: "__compar_fn_t", file: !267, line: 742, baseType: !311)
!311 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !312, size: 64)
!312 = !DISubroutineType(types: !313)
!313 = !{!8, !305, !305}
!314 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !315, line: 133)
!315 = !DISubprogram(name: "calloc", scope: !267, file: !267, line: 468, type: !316, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!316 = !DISubroutineType(types: !317)
!317 = !{!304, !307, !307}
!318 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !319, line: 134)
!319 = !DISubprogram(name: "div", scope: !267, file: !267, line: 789, type: !320, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!320 = !DISubroutineType(types: !321)
!321 = !{!266, !8, !8}
!322 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !323, line: 135)
!323 = !DISubprogram(name: "exit", scope: !267, file: !267, line: 543, type: !324, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!324 = !DISubroutineType(types: !325)
!325 = !{null, !8}
!326 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !327, line: 136)
!327 = !DISubprogram(name: "free", scope: !267, file: !267, line: 483, type: !328, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!328 = !DISubroutineType(types: !329)
!329 = !{null, !304}
!330 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !331, line: 137)
!331 = !DISubprogram(name: "getenv", scope: !267, file: !267, line: 564, type: !332, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!332 = !DISubroutineType(types: !333)
!333 = !{!6, !161}
!334 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !335, line: 138)
!335 = !DISubprogram(name: "labs", scope: !267, file: !267, line: 776, type: !119, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!336 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !337, line: 139)
!337 = !DISubprogram(name: "ldiv", scope: !267, file: !267, line: 791, type: !338, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!338 = !DISubroutineType(types: !339)
!339 = !{!270, !12, !12}
!340 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !341, line: 140)
!341 = !DISubprogram(name: "malloc", scope: !267, file: !267, line: 466, type: !342, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!342 = !DISubroutineType(types: !343)
!343 = !{!304, !307}
!344 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !345, line: 142)
!345 = !DISubprogram(name: "mblen", scope: !267, file: !267, line: 863, type: !346, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!346 = !DISubroutineType(types: !347)
!347 = !{!8, !161, !307}
!348 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !349, line: 143)
!349 = !DISubprogram(name: "mbstowcs", scope: !267, file: !267, line: 874, type: !350, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!350 = !DISubroutineType(types: !351)
!351 = !{!307, !352, !355, !307}
!352 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !353)
!353 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !354, size: 64)
!354 = !DIBasicType(name: "wchar_t", size: 32, encoding: DW_ATE_signed)
!355 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !161)
!356 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !357, line: 144)
!357 = !DISubprogram(name: "mbtowc", scope: !267, file: !267, line: 866, type: !358, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!358 = !DISubroutineType(types: !359)
!359 = !{!8, !352, !355, !307}
!360 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !361, line: 146)
!361 = !DISubprogram(name: "qsort", scope: !267, file: !267, line: 765, type: !362, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!362 = !DISubroutineType(types: !363)
!363 = !{null, !304, !307, !307, !310}
!364 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !365, line: 152)
!365 = !DISubprogram(name: "rand", scope: !267, file: !267, line: 374, type: !366, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!366 = !DISubroutineType(types: !367)
!367 = !{!8}
!368 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !369, line: 153)
!369 = !DISubprogram(name: "realloc", scope: !267, file: !267, line: 480, type: !370, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!370 = !DISubroutineType(types: !371)
!371 = !{!304, !304, !307}
!372 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !373, line: 154)
!373 = !DISubprogram(name: "srand", scope: !267, file: !267, line: 376, type: !374, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!374 = !DISubroutineType(types: !375)
!375 = !{null, !376}
!376 = !DIBasicType(name: "unsigned int", size: 32, encoding: DW_ATE_unsigned)
!377 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !378, line: 155)
!378 = !DISubprogram(name: "strtod", scope: !267, file: !267, line: 164, type: !379, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!379 = !DISubroutineType(types: !380)
!380 = !{!160, !355, !381}
!381 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !382)
!382 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !6, size: 64)
!383 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !384, line: 156)
!384 = !DISubprogram(name: "strtol", scope: !267, file: !267, line: 183, type: !385, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!385 = !DISubroutineType(types: !386)
!386 = !{!12, !355, !381, !8}
!387 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !388, line: 157)
!388 = !DISubprogram(name: "strtoul", scope: !267, file: !267, line: 187, type: !389, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!389 = !DISubroutineType(types: !390)
!390 = !{!309, !355, !381, !8}
!391 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !392, line: 158)
!392 = !DISubprogram(name: "system", scope: !267, file: !267, line: 717, type: !293, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!393 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !394, line: 160)
!394 = !DISubprogram(name: "wcstombs", scope: !267, file: !267, line: 877, type: !395, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!395 = !DISubroutineType(types: !396)
!396 = !{!307, !397, !398, !307}
!397 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !6)
!398 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !399)
!399 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !400, size: 64)
!400 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !354)
!401 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !402, line: 161)
!402 = !DISubprogram(name: "wctomb", scope: !267, file: !267, line: 870, type: !403, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!403 = !DISubroutineType(types: !404)
!404 = !{!8, !6, !354}
!405 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !408, line: 201)
!406 = !DINamespace(name: "__gnu_cxx", scope: null, file: !407, line: 68)
!407 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/bits/cpp_type_traits.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!408 = !DIDerivedType(tag: DW_TAG_typedef, name: "lldiv_t", file: !267, line: 121, baseType: !409)
!409 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !267, line: 117, size: 128, elements: !410, identifier: "_ZTS7lldiv_t")
!410 = !{!411, !412}
!411 = !DIDerivedType(tag: DW_TAG_member, name: "quot", scope: !409, file: !267, line: 119, baseType: !23, size: 64)
!412 = !DIDerivedType(tag: DW_TAG_member, name: "rem", scope: !409, file: !267, line: 120, baseType: !23, size: 64, offset: 64)
!413 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !414, line: 207)
!414 = !DISubprogram(name: "_Exit", scope: !267, file: !267, line: 557, type: !324, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!415 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !416, line: 211)
!416 = !DISubprogram(name: "llabs", scope: !267, file: !267, line: 780, type: !21, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!417 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !418, line: 217)
!418 = !DISubprogram(name: "lldiv", scope: !267, file: !267, line: 797, type: !419, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!419 = !DISubroutineType(types: !420)
!420 = !{!408, !23, !23}
!421 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !422, line: 228)
!422 = !DISubprogram(name: "atoll", scope: !267, file: !267, line: 292, type: !423, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!423 = !DISubroutineType(types: !424)
!424 = !{!23, !161}
!425 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !426, line: 229)
!426 = !DISubprogram(name: "strtoll", scope: !267, file: !267, line: 209, type: !427, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!427 = !DISubroutineType(types: !428)
!428 = !{!23, !355, !381, !8}
!429 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !430, line: 230)
!430 = !DISubprogram(name: "strtoull", scope: !267, file: !267, line: 214, type: !431, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!431 = !DISubroutineType(types: !432)
!432 = !{!433, !355, !381, !8}
!433 = !DIBasicType(name: "long long unsigned int", size: 64, encoding: DW_ATE_unsigned)
!434 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !435, line: 232)
!435 = !DISubprogram(name: "strtof", scope: !267, file: !267, line: 172, type: !436, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!436 = !DISubroutineType(types: !437)
!437 = !{!28, !355, !381}
!438 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !406, entity: !439, line: 233)
!439 = !DISubprogram(name: "strtold", scope: !267, file: !267, line: 175, type: !440, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!440 = !DISubroutineType(types: !441)
!441 = !{!442, !355, !381}
!442 = !DIBasicType(name: "long double", size: 64, encoding: DW_ATE_float)
!443 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !408, line: 241)
!444 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !414, line: 243)
!445 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !416, line: 245)
!446 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !447, line: 246)
!447 = !DISubprogram(name: "div", linkageName: "_ZN9__gnu_cxx3divExx", scope: !406, file: !448, line: 214, type: !419, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!448 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/cstdlib", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!449 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !418, line: 247)
!450 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !422, line: 249)
!451 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !435, line: 250)
!452 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !426, line: 251)
!453 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !430, line: 252)
!454 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !439, line: 253)
!455 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !456, line: 418)
!456 = !DISubprogram(name: "acosf", linkageName: "_ZL5acosff", scope: !457, file: !457, line: 1126, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!457 = !DIFile(filename: "/usr/local/cuda/include/math_functions.hpp", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!458 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !459, line: 419)
!459 = !DISubprogram(name: "acoshf", linkageName: "_ZL6acoshff", scope: !457, file: !457, line: 1154, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!460 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !461, line: 420)
!461 = !DISubprogram(name: "asinf", linkageName: "_ZL5asinff", scope: !457, file: !457, line: 1121, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!462 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !463, line: 421)
!463 = !DISubprogram(name: "asinhf", linkageName: "_ZL6asinhff", scope: !457, file: !457, line: 1159, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!464 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !465, line: 422)
!465 = !DISubprogram(name: "atan2f", linkageName: "_ZL6atan2fff", scope: !457, file: !457, line: 1111, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!466 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !467, line: 423)
!467 = !DISubprogram(name: "atanf", linkageName: "_ZL5atanff", scope: !457, file: !457, line: 1116, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!468 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !469, line: 424)
!469 = !DISubprogram(name: "atanhf", linkageName: "_ZL6atanhff", scope: !457, file: !457, line: 1164, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!470 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !471, line: 425)
!471 = !DISubprogram(name: "cbrtf", linkageName: "_ZL5cbrtff", scope: !457, file: !457, line: 1199, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!472 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !473, line: 426)
!473 = !DISubprogram(name: "ceilf", linkageName: "_ZL5ceilff", scope: !474, file: !474, line: 647, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!474 = !DIFile(filename: "/usr/local/cuda/include/device_functions.hpp", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!475 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !476, line: 427)
!476 = !DISubprogram(name: "copysignf", linkageName: "_ZL9copysignfff", scope: !457, file: !457, line: 973, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!477 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !478, line: 428)
!478 = !DISubprogram(name: "cosf", linkageName: "_ZL4cosff", scope: !457, file: !457, line: 1027, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!479 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !480, line: 429)
!480 = !DISubprogram(name: "coshf", linkageName: "_ZL5coshff", scope: !457, file: !457, line: 1096, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!481 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !482, line: 430)
!482 = !DISubprogram(name: "erfcf", linkageName: "_ZL5erfcff", scope: !457, file: !457, line: 1259, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!483 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !484, line: 431)
!484 = !DISubprogram(name: "erff", linkageName: "_ZL4erfff", scope: !457, file: !457, line: 1249, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!485 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !486, line: 432)
!486 = !DISubprogram(name: "exp2f", linkageName: "_ZL5exp2ff", scope: !474, file: !474, line: 637, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!487 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !488, line: 433)
!488 = !DISubprogram(name: "expf", linkageName: "_ZL4expff", scope: !457, file: !457, line: 1078, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!489 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !490, line: 434)
!490 = !DISubprogram(name: "expm1f", linkageName: "_ZL6expm1ff", scope: !457, file: !457, line: 1169, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!491 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !492, line: 435)
!492 = !DISubprogram(name: "fabsf", linkageName: "_ZL5fabsff", scope: !474, file: !474, line: 582, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!493 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !494, line: 436)
!494 = !DISubprogram(name: "fdimf", linkageName: "_ZL5fdimfff", scope: !457, file: !457, line: 1385, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!495 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !496, line: 437)
!496 = !DISubprogram(name: "floorf", linkageName: "_ZL6floorff", scope: !474, file: !474, line: 572, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!497 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !498, line: 438)
!498 = !DISubprogram(name: "fmaf", linkageName: "_ZL4fmaffff", scope: !457, file: !457, line: 1337, type: !71, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!499 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !500, line: 439)
!500 = !DISubprogram(name: "fmaxf", linkageName: "_ZL5fmaxfff", scope: !474, file: !474, line: 602, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!501 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !502, line: 440)
!502 = !DISubprogram(name: "fminf", linkageName: "_ZL5fminfff", scope: !474, file: !474, line: 597, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!503 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !504, line: 441)
!504 = !DISubprogram(name: "fmodf", linkageName: "_ZL5fmodfff", scope: !457, file: !457, line: 1322, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!505 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !506, line: 442)
!506 = !DISubprogram(name: "frexpf", linkageName: "_ZL6frexpffPi", scope: !457, file: !457, line: 1312, type: !85, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!507 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !508, line: 443)
!508 = !DISubprogram(name: "hypotf", linkageName: "_ZL6hypotfff", scope: !457, file: !457, line: 1174, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!509 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !510, line: 444)
!510 = !DISubprogram(name: "ilogbf", linkageName: "_ZL6ilogbff", scope: !457, file: !457, line: 1390, type: !81, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!511 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !512, line: 445)
!512 = !DISubprogram(name: "ldexpf", linkageName: "_ZL6ldexpffi", scope: !457, file: !457, line: 1289, type: !123, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!513 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !514, line: 446)
!514 = !DISubprogram(name: "lgammaf", linkageName: "_ZL7lgammaff", scope: !457, file: !457, line: 1284, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!515 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !516, line: 447)
!516 = !DISubprogram(name: "llrintf", linkageName: "_ZL7llrintff", scope: !457, file: !457, line: 933, type: !131, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!517 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !518, line: 448)
!518 = !DISubprogram(name: "llroundf", linkageName: "_ZL8llroundff", scope: !457, file: !457, line: 1371, type: !131, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!519 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !520, line: 449)
!520 = !DISubprogram(name: "log10f", linkageName: "_ZL6log10ff", scope: !457, file: !457, line: 1140, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!521 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !522, line: 450)
!522 = !DISubprogram(name: "log1pf", linkageName: "_ZL6log1pff", scope: !457, file: !457, line: 1149, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!523 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !524, line: 451)
!524 = !DISubprogram(name: "log2f", linkageName: "_ZL5log2ff", scope: !457, file: !457, line: 1069, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!525 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !526, line: 452)
!526 = !DISubprogram(name: "logbf", linkageName: "_ZL5logbff", scope: !457, file: !457, line: 1395, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!527 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !528, line: 453)
!528 = !DISubprogram(name: "logf", linkageName: "_ZL4logff", scope: !457, file: !457, line: 1131, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!529 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !530, line: 454)
!530 = !DISubprogram(name: "lrintf", linkageName: "_ZL6lrintff", scope: !457, file: !457, line: 924, type: !145, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!531 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !532, line: 455)
!532 = !DISubprogram(name: "lroundf", linkageName: "_ZL7lroundff", scope: !457, file: !457, line: 1376, type: !145, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!533 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !534, line: 456)
!534 = !DISubprogram(name: "modff", linkageName: "_ZL5modfffPf", scope: !457, file: !457, line: 1317, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!535 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !536, line: 457)
!536 = !DISubprogram(name: "nearbyintf", linkageName: "_ZL10nearbyintff", scope: !457, file: !457, line: 938, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!537 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !538, line: 458)
!538 = !DISubprogram(name: "nextafterf", linkageName: "_ZL10nextafterfff", scope: !457, file: !457, line: 1002, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!539 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !540, line: 459)
!540 = !DISubprogram(name: "nexttowardf", scope: !211, file: !211, line: 284, type: !541, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!541 = !DISubroutineType(types: !542)
!542 = !{!28, !28, !442}
!543 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !540, line: 460)
!544 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !545, line: 461)
!545 = !DISubprogram(name: "powf", linkageName: "_ZL4powfff", scope: !457, file: !457, line: 1352, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!546 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !547, line: 462)
!547 = !DISubprogram(name: "remainderf", linkageName: "_ZL10remainderfff", scope: !457, file: !457, line: 1327, type: !39, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!548 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !549, line: 463)
!549 = !DISubprogram(name: "remquof", linkageName: "_ZL7remquofffPi", scope: !457, file: !457, line: 1332, type: !181, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!550 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !551, line: 464)
!551 = !DISubprogram(name: "rintf", linkageName: "_ZL5rintff", scope: !457, file: !457, line: 919, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!552 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !553, line: 465)
!553 = !DISubprogram(name: "roundf", linkageName: "_ZL6roundff", scope: !457, file: !457, line: 1366, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!554 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !555, line: 466)
!555 = !DISubprogram(name: "scalblnf", linkageName: "_ZL8scalblnffl", scope: !457, file: !457, line: 1299, type: !189, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!556 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !557, line: 467)
!557 = !DISubprogram(name: "scalbnf", linkageName: "_ZL7scalbnffi", scope: !457, file: !457, line: 1294, type: !123, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!558 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !559, line: 468)
!559 = !DISubprogram(name: "sinf", linkageName: "_ZL4sinff", scope: !457, file: !457, line: 1018, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!560 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !561, line: 469)
!561 = !DISubprogram(name: "sinhf", linkageName: "_ZL5sinhff", scope: !457, file: !457, line: 1101, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!562 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !563, line: 470)
!563 = !DISubprogram(name: "sqrtf", linkageName: "_ZL5sqrtff", scope: !474, file: !474, line: 887, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!564 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !565, line: 471)
!565 = !DISubprogram(name: "tanf", linkageName: "_ZL4tanff", scope: !457, file: !457, line: 1060, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!566 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !567, line: 472)
!567 = !DISubprogram(name: "tanhf", linkageName: "_ZL5tanhff", scope: !457, file: !457, line: 1106, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!568 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !569, line: 473)
!569 = !DISubprogram(name: "tgammaf", linkageName: "_ZL7tgammaff", scope: !457, file: !457, line: 1361, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!570 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !18, entity: !571, line: 474)
!571 = !DISubprogram(name: "truncf", linkageName: "_ZL6truncff", scope: !474, file: !474, line: 642, type: !26, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!572 = distinct !DICompileUnit(language: DW_LANG_C_plus_plus, file: !573, producer: "clang version 5.0.0 (trunk 294196)", isOptimized: true, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, imports: !16)
!573 = !DIFile(filename: "bicg.cu", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!574 = !{void (float*, float*, float*)* @_Z12bicg_kernel1PfS_S_, !"kernel", i32 1}
!575 = !{void (float*, float*, float*)* @_Z12bicg_kernel2PfS_S_, !"kernel", i32 1}
!576 = !{null, !"align", i32 8}
!577 = !{null, !"align", i32 8, !"align", i32 65544, !"align", i32 131080}
!578 = !{null, !"align", i32 16}
!579 = !{null, !"align", i32 16, !"align", i32 65552, !"align", i32 131088}
!580 = !{!"clang version 5.0.0 (trunk 294196)"}
!581 = !{i32 1, i32 2}
!582 = !{i32 2, !"Dwarf Version", i32 4}
!583 = !{i32 2, !"Debug Info Version", i32 3}
!584 = !{i32 4, !"nvvm-reflect-ftz", i32 0}
!585 = distinct !DISubprogram(name: "bicg_kernel1", linkageName: "_Z12bicg_kernel1PfS_S_", scope: !573, file: !573, line: 99, type: !586, isLocal: false, isDefinition: true, scopeLine: 100, flags: DIFlagPrototyped, isOptimized: true, unit: !572, variables: !590)
!586 = !DISubroutineType(types: !587)
!587 = !{null, !588, !588, !588}
!588 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !589, size: 64)
!589 = !DIDerivedType(tag: DW_TAG_typedef, name: "DATA_TYPE", file: !573, line: 37, baseType: !28)
!590 = !{!591, !592, !593, !594, !595}
!591 = !DILocalVariable(name: "A", arg: 1, scope: !585, file: !573, line: 99, type: !588)
!592 = !DILocalVariable(name: "r", arg: 2, scope: !585, file: !573, line: 99, type: !588)
!593 = !DILocalVariable(name: "s", arg: 3, scope: !585, file: !573, line: 99, type: !588)
!594 = !DILocalVariable(name: "j", scope: !585, file: !573, line: 101, type: !8)
!595 = !DILocalVariable(name: "i", scope: !596, file: !573, line: 107, type: !8)
!596 = distinct !DILexicalBlock(scope: !597, file: !573, line: 104, column: 2)
!597 = distinct !DILexicalBlock(scope: !585, file: !573, line: 103, column: 6)
!598 = !DIExpression()
!599 = !DILocation(line: 99, column: 41, scope: !585)
!600 = !DILocation(line: 99, column: 55, scope: !585)
!601 = !DILocation(line: 99, column: 69, scope: !585)
!602 = !DILocation(line: 78, column: 3, scope: !603, inlinedAt: !638)
!603 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !605, file: !604, line: 78, type: !608, isLocal: false, isDefinition: true, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true, unit: !572, declaration: !607, variables: !4)
!604 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_builtin_vars.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!605 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_blockIdx_t", file: !604, line: 77, size: 8, elements: !606, identifier: "_ZTS25__cuda_builtin_blockIdx_t")
!606 = !{!607, !610, !611, !612, !623, !627, !631, !634}
!607 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !605, file: !604, line: 78, type: !608, isLocal: false, isDefinition: false, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true)
!608 = !DISubroutineType(types: !609)
!609 = !{!376}
!610 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_yEv", scope: !605, file: !604, line: 79, type: !608, isLocal: false, isDefinition: false, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true)
!611 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_zEv", scope: !605, file: !604, line: 80, type: !608, isLocal: false, isDefinition: false, scopeLine: 80, flags: DIFlagPrototyped, isOptimized: true)
!612 = !DISubprogram(name: "operator uint3", linkageName: "_ZNK25__cuda_builtin_blockIdx_tcv5uint3Ev", scope: !605, file: !604, line: 83, type: !613, isLocal: false, isDefinition: false, scopeLine: 83, flags: DIFlagPrototyped, isOptimized: true)
!613 = !DISubroutineType(types: !614)
!614 = !{!615, !621}
!615 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "uint3", file: !616, line: 190, size: 96, elements: !617, identifier: "_ZTS5uint3")
!616 = !DIFile(filename: "/usr/local/cuda/include/vector_types.h", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!617 = !{!618, !619, !620}
!618 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !615, file: !616, line: 192, baseType: !376, size: 32)
!619 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !615, file: !616, line: 192, baseType: !376, size: 32, offset: 32)
!620 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !615, file: !616, line: 192, baseType: !376, size: 32, offset: 64)
!621 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !622, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!622 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !605)
!623 = !DISubprogram(name: "__cuda_builtin_blockIdx_t", scope: !605, file: !604, line: 85, type: !624, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!624 = !DISubroutineType(types: !625)
!625 = !{null, !626}
!626 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !605, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!627 = !DISubprogram(name: "__cuda_builtin_blockIdx_t", scope: !605, file: !604, line: 85, type: !628, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!628 = !DISubroutineType(types: !629)
!629 = !{null, !626, !630}
!630 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !622, size: 64)
!631 = !DISubprogram(name: "operator=", linkageName: "_ZNK25__cuda_builtin_blockIdx_taSERKS_", scope: !605, file: !604, line: 85, type: !632, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!632 = !DISubroutineType(types: !633)
!633 = !{null, !621, !630}
!634 = !DISubprogram(name: "operator&", linkageName: "_ZNK25__cuda_builtin_blockIdx_tadEv", scope: !605, file: !604, line: 85, type: !635, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!635 = !DISubroutineType(types: !636)
!636 = !{!637, !621}
!637 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !605, size: 64)
!638 = distinct !DILocation(line: 101, column: 10, scope: !585)
!639 = !{i32 0, i32 65535}
!640 = !DILocation(line: 89, column: 3, scope: !641, inlinedAt: !683)
!641 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_xEv", scope: !642, file: !604, line: 89, type: !608, isLocal: false, isDefinition: true, scopeLine: 89, flags: DIFlagPrototyped, isOptimized: true, unit: !572, declaration: !644, variables: !4)
!642 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_blockDim_t", file: !604, line: 88, size: 8, elements: !643, identifier: "_ZTS25__cuda_builtin_blockDim_t")
!643 = !{!644, !645, !646, !647, !668, !672, !676, !679}
!644 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_xEv", scope: !642, file: !604, line: 89, type: !608, isLocal: false, isDefinition: false, scopeLine: 89, flags: DIFlagPrototyped, isOptimized: true)
!645 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_yEv", scope: !642, file: !604, line: 90, type: !608, isLocal: false, isDefinition: false, scopeLine: 90, flags: DIFlagPrototyped, isOptimized: true)
!646 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_zEv", scope: !642, file: !604, line: 91, type: !608, isLocal: false, isDefinition: false, scopeLine: 91, flags: DIFlagPrototyped, isOptimized: true)
!647 = !DISubprogram(name: "operator dim3", linkageName: "_ZNK25__cuda_builtin_blockDim_tcv4dim3Ev", scope: !642, file: !604, line: 94, type: !648, isLocal: false, isDefinition: false, scopeLine: 94, flags: DIFlagPrototyped, isOptimized: true)
!648 = !DISubroutineType(types: !649)
!649 = !{!650, !666}
!650 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "dim3", file: !616, line: 417, size: 96, elements: !651, identifier: "_ZTS4dim3")
!651 = !{!652, !653, !654, !655, !659, !663}
!652 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !650, file: !616, line: 419, baseType: !376, size: 32)
!653 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !650, file: !616, line: 419, baseType: !376, size: 32, offset: 32)
!654 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !650, file: !616, line: 419, baseType: !376, size: 32, offset: 64)
!655 = !DISubprogram(name: "dim3", scope: !650, file: !616, line: 421, type: !656, isLocal: false, isDefinition: false, scopeLine: 421, flags: DIFlagPrototyped, isOptimized: true)
!656 = !DISubroutineType(types: !657)
!657 = !{null, !658, !376, !376, !376}
!658 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !650, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!659 = !DISubprogram(name: "dim3", scope: !650, file: !616, line: 422, type: !660, isLocal: false, isDefinition: false, scopeLine: 422, flags: DIFlagPrototyped, isOptimized: true)
!660 = !DISubroutineType(types: !661)
!661 = !{null, !658, !662}
!662 = !DIDerivedType(tag: DW_TAG_typedef, name: "uint3", file: !616, line: 383, baseType: !615)
!663 = !DISubprogram(name: "operator uint3", linkageName: "_ZN4dim3cv5uint3Ev", scope: !650, file: !616, line: 423, type: !664, isLocal: false, isDefinition: false, scopeLine: 423, flags: DIFlagPrototyped, isOptimized: true)
!664 = !DISubroutineType(types: !665)
!665 = !{!662, !658}
!666 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !667, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!667 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !642)
!668 = !DISubprogram(name: "__cuda_builtin_blockDim_t", scope: !642, file: !604, line: 96, type: !669, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!669 = !DISubroutineType(types: !670)
!670 = !{null, !671}
!671 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !642, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!672 = !DISubprogram(name: "__cuda_builtin_blockDim_t", scope: !642, file: !604, line: 96, type: !673, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!673 = !DISubroutineType(types: !674)
!674 = !{null, !671, !675}
!675 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !667, size: 64)
!676 = !DISubprogram(name: "operator=", linkageName: "_ZNK25__cuda_builtin_blockDim_taSERKS_", scope: !642, file: !604, line: 96, type: !677, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!677 = !DISubroutineType(types: !678)
!678 = !{null, !666, !675}
!679 = !DISubprogram(name: "operator&", linkageName: "_ZNK25__cuda_builtin_blockDim_tadEv", scope: !642, file: !604, line: 96, type: !680, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!680 = !DISubroutineType(types: !681)
!681 = !{!682, !666}
!682 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !642, size: 64)
!683 = distinct !DILocation(line: 101, column: 23, scope: !684)
!684 = !DILexicalBlockFile(scope: !585, file: !573, discriminator: 1)
!685 = !{i32 1, i32 1025}
!686 = !DILocation(line: 101, column: 21, scope: !585)
!687 = !DILocation(line: 67, column: 3, scope: !688, inlinedAt: !714)
!688 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !689, file: !604, line: 67, type: !608, isLocal: false, isDefinition: true, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true, unit: !572, declaration: !691, variables: !4)
!689 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_threadIdx_t", file: !604, line: 66, size: 8, elements: !690, identifier: "_ZTS26__cuda_builtin_threadIdx_t")
!690 = !{!691, !692, !693, !694, !699, !703, !707, !710}
!691 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !689, file: !604, line: 67, type: !608, isLocal: false, isDefinition: false, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true)
!692 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_yEv", scope: !689, file: !604, line: 68, type: !608, isLocal: false, isDefinition: false, scopeLine: 68, flags: DIFlagPrototyped, isOptimized: true)
!693 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_zEv", scope: !689, file: !604, line: 69, type: !608, isLocal: false, isDefinition: false, scopeLine: 69, flags: DIFlagPrototyped, isOptimized: true)
!694 = !DISubprogram(name: "operator uint3", linkageName: "_ZNK26__cuda_builtin_threadIdx_tcv5uint3Ev", scope: !689, file: !604, line: 72, type: !695, isLocal: false, isDefinition: false, scopeLine: 72, flags: DIFlagPrototyped, isOptimized: true)
!695 = !DISubroutineType(types: !696)
!696 = !{!615, !697}
!697 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !698, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!698 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !689)
!699 = !DISubprogram(name: "__cuda_builtin_threadIdx_t", scope: !689, file: !604, line: 74, type: !700, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!700 = !DISubroutineType(types: !701)
!701 = !{null, !702}
!702 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !689, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!703 = !DISubprogram(name: "__cuda_builtin_threadIdx_t", scope: !689, file: !604, line: 74, type: !704, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!704 = !DISubroutineType(types: !705)
!705 = !{null, !702, !706}
!706 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !698, size: 64)
!707 = !DISubprogram(name: "operator=", linkageName: "_ZNK26__cuda_builtin_threadIdx_taSERKS_", scope: !689, file: !604, line: 74, type: !708, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!708 = !DISubroutineType(types: !709)
!709 = !{null, !697, !706}
!710 = !DISubprogram(name: "operator&", linkageName: "_ZNK26__cuda_builtin_threadIdx_tadEv", scope: !689, file: !604, line: 74, type: !711, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!711 = !DISubroutineType(types: !712)
!712 = !{!713, !697}
!713 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !689, size: 64)
!714 = distinct !DILocation(line: 101, column: 36, scope: !715)
!715 = !DILexicalBlockFile(scope: !585, file: !573, discriminator: 2)
!716 = !{i32 0, i32 1024}
!717 = !DILocation(line: 101, column: 34, scope: !585)
!718 = !DILocation(line: 101, column: 6, scope: !585)
!719 = !DILocation(line: 103, column: 8, scope: !597)
!720 = !DILocation(line: 103, column: 6, scope: !585)
!721 = !DILocation(line: 105, column: 3, scope: !596)
!722 = !DILocation(line: 105, column: 8, scope: !596)
!723 = !{!724, !724, i64 0}
!724 = !{!"float", !725, i64 0}
!725 = !{!"omnipotent char", !726, i64 0}
!726 = !{!"Simple C++ TBAA"}
!727 = !DILocation(line: 107, column: 7, scope: !596)
!728 = !DILocation(line: 108, column: 3, scope: !729)
!729 = !DILexicalBlockFile(scope: !730, file: !573, discriminator: 1)
!730 = distinct !DILexicalBlock(scope: !596, file: !573, line: 108, column: 3)
!731 = !DILocation(line: 110, column: 9, scope: !732)
!732 = distinct !DILexicalBlock(scope: !733, file: !573, line: 109, column: 3)
!733 = distinct !DILexicalBlock(scope: !730, file: !573, line: 108, column: 3)
!734 = !DILocation(line: 110, column: 16, scope: !732)
!735 = !DILocation(line: 110, column: 21, scope: !732)
!736 = !DILocation(line: 110, column: 12, scope: !732)
!737 = !DILocation(line: 110, column: 28, scope: !732)
!738 = !DILocation(line: 110, column: 26, scope: !732)
!739 = !DILocation(line: 108, column: 23, scope: !740)
!740 = !DILexicalBlockFile(scope: !733, file: !573, discriminator: 2)
!741 = !DILocation(line: 108, column: 16, scope: !742)
!742 = !DILexicalBlockFile(scope: !733, file: !573, discriminator: 1)
!743 = distinct !{!743, !744, !745}
!744 = !DILocation(line: 108, column: 3, scope: !730)
!745 = !DILocation(line: 111, column: 3, scope: !730)
!746 = !DILocation(line: 113, column: 1, scope: !585)
!747 = distinct !DISubprogram(name: "bicg_kernel2", linkageName: "_Z12bicg_kernel2PfS_S_", scope: !573, file: !573, line: 117, type: !586, isLocal: false, isDefinition: true, scopeLine: 118, flags: DIFlagPrototyped, isOptimized: true, unit: !572, variables: !748)
!748 = !{!749, !750, !751, !752, !753}
!749 = !DILocalVariable(name: "A", arg: 1, scope: !747, file: !573, line: 117, type: !588)
!750 = !DILocalVariable(name: "p", arg: 2, scope: !747, file: !573, line: 117, type: !588)
!751 = !DILocalVariable(name: "q", arg: 3, scope: !747, file: !573, line: 117, type: !588)
!752 = !DILocalVariable(name: "i", scope: !747, file: !573, line: 119, type: !8)
!753 = !DILocalVariable(name: "j", scope: !754, file: !573, line: 125, type: !8)
!754 = distinct !DILexicalBlock(scope: !755, file: !573, line: 122, column: 2)
!755 = distinct !DILexicalBlock(scope: !747, file: !573, line: 121, column: 6)
!756 = !DILocation(line: 117, column: 41, scope: !747)
!757 = !DILocation(line: 117, column: 55, scope: !747)
!758 = !DILocation(line: 117, column: 69, scope: !747)
!759 = !DILocation(line: 78, column: 3, scope: !603, inlinedAt: !760)
!760 = distinct !DILocation(line: 119, column: 10, scope: !747)
!761 = !DILocation(line: 89, column: 3, scope: !641, inlinedAt: !762)
!762 = distinct !DILocation(line: 119, column: 23, scope: !763)
!763 = !DILexicalBlockFile(scope: !747, file: !573, discriminator: 1)
!764 = !DILocation(line: 119, column: 21, scope: !747)
!765 = !DILocation(line: 67, column: 3, scope: !688, inlinedAt: !766)
!766 = distinct !DILocation(line: 119, column: 36, scope: !767)
!767 = !DILexicalBlockFile(scope: !747, file: !573, discriminator: 2)
!768 = !DILocation(line: 119, column: 34, scope: !747)
!769 = !DILocation(line: 119, column: 6, scope: !747)
!770 = !DILocation(line: 121, column: 8, scope: !755)
!771 = !DILocation(line: 121, column: 6, scope: !747)
!772 = !DILocation(line: 123, column: 3, scope: !754)
!773 = !DILocation(line: 123, column: 8, scope: !754)
!774 = !DILocation(line: 125, column: 7, scope: !754)
!775 = !DILocation(line: 126, column: 3, scope: !776)
!776 = !DILexicalBlockFile(scope: !777, file: !573, discriminator: 1)
!777 = distinct !DILexicalBlock(scope: !754, file: !573, line: 126, column: 3)
!778 = !DILocation(line: 128, column: 9, scope: !779)
!779 = distinct !DILexicalBlock(scope: !780, file: !573, line: 127, column: 3)
!780 = distinct !DILexicalBlock(scope: !777, file: !573, line: 126, column: 3)
!781 = !DILocation(line: 128, column: 21, scope: !779)
!782 = !DILocation(line: 128, column: 12, scope: !779)
!783 = !DILocation(line: 128, column: 28, scope: !779)
!784 = !DILocation(line: 128, column: 26, scope: !779)
!785 = !DILocation(line: 126, column: 21, scope: !786)
!786 = !DILexicalBlockFile(scope: !780, file: !573, discriminator: 2)
!787 = !DILocation(line: 126, column: 14, scope: !788)
!788 = !DILexicalBlockFile(scope: !780, file: !573, discriminator: 1)
!789 = distinct !{!789, !790, !791}
!790 = !DILocation(line: 126, column: 3, scope: !777)
!791 = !DILocation(line: 129, column: 3, scope: !777)
!792 = !DILocation(line: 131, column: 1, scope: !747)
!793 = distinct !DISubprogram(name: "takeString", scope: !3, file: !3, line: 34, type: !794, isLocal: false, isDefinition: true, scopeLine: 35, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !796)
!794 = !DISubroutineType(types: !795)
!795 = !{null, !304, !8}
!796 = !{!797, !798}
!797 = !DILocalVariable(name: "p", arg: 1, scope: !793, file: !3, line: 34, type: !304)
!798 = !DILocalVariable(name: "action", arg: 2, scope: !793, file: !3, line: 34, type: !8)
!799 = !DILocation(line: 34, column: 34, scope: !793)
!800 = !DILocation(line: 34, column: 41, scope: !793)
!801 = !DILocation(line: 67, column: 3, scope: !802, inlinedAt: !803)
!802 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !689, file: !604, line: 67, type: !608, isLocal: false, isDefinition: true, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !691, variables: !4)
!803 = distinct !DILocation(line: 36, column: 6, scope: !804)
!804 = distinct !DILexicalBlock(scope: !793, file: !3, line: 36, column: 6)
!805 = !DILocation(line: 36, column: 18, scope: !804)
!806 = !DILocation(line: 36, column: 23, scope: !804)
!807 = !DILocation(line: 78, column: 3, scope: !808, inlinedAt: !809)
!808 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !605, file: !604, line: 78, type: !608, isLocal: false, isDefinition: true, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !607, variables: !4)
!809 = distinct !DILocation(line: 36, column: 26, scope: !810)
!810 = !DILexicalBlockFile(scope: !804, file: !3, discriminator: 1)
!811 = !DILocation(line: 36, column: 37, scope: !810)
!812 = !DILocation(line: 36, column: 42, scope: !810)
!813 = !DILocation(line: 68, column: 3, scope: !814, inlinedAt: !815)
!814 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_yEv", scope: !689, file: !604, line: 68, type: !608, isLocal: false, isDefinition: true, scopeLine: 68, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !692, variables: !4)
!815 = distinct !DILocation(line: 36, column: 45, scope: !816)
!816 = !DILexicalBlockFile(scope: !804, file: !3, discriminator: 2)
!817 = !DILocation(line: 36, column: 57, scope: !816)
!818 = !DILocation(line: 36, column: 62, scope: !816)
!819 = !DILocation(line: 79, column: 3, scope: !820, inlinedAt: !821)
!820 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_yEv", scope: !605, file: !604, line: 79, type: !608, isLocal: false, isDefinition: true, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !610, variables: !4)
!821 = distinct !DILocation(line: 36, column: 65, scope: !822)
!822 = !DILexicalBlockFile(scope: !804, file: !3, discriminator: 3)
!823 = !DILocation(line: 36, column: 76, scope: !822)
!824 = !DILocation(line: 36, column: 6, scope: !825)
!825 = !DILexicalBlockFile(scope: !793, file: !3, discriminator: 3)
!826 = !DILocation(line: 38, column: 6, scope: !793)
!827 = !DILocation(line: 39, column: 3, scope: !828)
!828 = distinct !DILexicalBlock(scope: !793, file: !3, line: 38, column: 6)
!829 = !DILocation(line: 41, column: 3, scope: !830)
!830 = distinct !DILexicalBlock(scope: !828, file: !3, line: 40, column: 11)
!831 = !DILocation(line: 43, column: 3, scope: !832)
!832 = distinct !DILexicalBlock(scope: !830, file: !3, line: 42, column: 11)
!833 = !DILocation(line: 45, column: 3, scope: !832)
!834 = !DILocation(line: 47, column: 1, scope: !793)
!835 = distinct !DISubprogram(name: "storeLines", linkageName: "_Z10storeLinesPvssi", scope: !3, file: !3, line: 50, type: !836, isLocal: false, isDefinition: true, scopeLine: 51, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !838)
!836 = !DISubroutineType(types: !837)
!837 = !{null, !304, !10, !10, !8}
!838 = !{!839, !840, !841, !842, !843, !844, !845}
!839 = !DILocalVariable(name: "p", arg: 1, scope: !835, file: !3, line: 50, type: !304)
!840 = !DILocalVariable(name: "size", arg: 2, scope: !835, file: !3, line: 50, type: !10)
!841 = !DILocalVariable(name: "line", arg: 3, scope: !835, file: !3, line: 50, type: !10)
!842 = !DILocalVariable(name: "op", arg: 4, scope: !835, file: !3, line: 50, type: !8)
!843 = !DILocalVariable(name: "bid", scope: !835, file: !3, line: 52, type: !8)
!844 = !DILocalVariable(name: "buffer_oN_DeViCe_short", scope: !835, file: !3, line: 60, type: !9)
!845 = !DILocalVariable(name: "buffer_oN_DeViCe_long", scope: !835, file: !3, line: 61, type: !11)
!846 = !DILocation(line: 50, column: 34, scope: !835)
!847 = !DILocation(line: 50, column: 43, scope: !835)
!848 = !DILocation(line: 50, column: 64, scope: !835)
!849 = !DILocation(line: 50, column: 74, scope: !835)
!850 = !DILocalVariable(name: "address", arg: 1, scope: !851, file: !852, line: 78, type: !87)
!851 = distinct !DISubprogram(name: "atomicAdd", linkageName: "_ZL9atomicAddPii", scope: !852, file: !852, line: 78, type: !853, isLocal: true, isDefinition: true, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !855)
!852 = !DIFile(filename: "/usr/local/cuda/include/device_atomic_functions.hpp", directory: "/home/dshen/benchmarks/polybench-gpu-1.0/cuda/BICG")
!853 = !DISubroutineType(types: !854)
!854 = !{!8, !87, !8}
!855 = !{!850, !856}
!856 = !DILocalVariable(name: "val", arg: 2, scope: !851, file: !852, line: 78, type: !8)
!857 = !DILocation(line: 78, column: 53, scope: !851, inlinedAt: !858)
!858 = distinct !DILocation(line: 52, column: 12, scope: !835)
!859 = !DILocation(line: 78, column: 66, scope: !851, inlinedAt: !858)
!860 = !DILocalVariable(name: "p", arg: 1, scope: !861, file: !474, line: 1512, type: !87)
!861 = distinct !DISubprogram(name: "__iAtomicAdd", linkageName: "_ZL12__iAtomicAddPii", scope: !474, file: !474, line: 1512, type: !853, isLocal: true, isDefinition: true, scopeLine: 1513, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !862)
!862 = !{!860, !863}
!863 = !DILocalVariable(name: "val", arg: 2, scope: !861, file: !474, line: 1512, type: !8)
!864 = !DILocation(line: 1512, column: 23, scope: !861, inlinedAt: !865)
!865 = distinct !DILocation(line: 80, column: 10, scope: !851, inlinedAt: !858)
!866 = !DILocation(line: 1512, column: 30, scope: !861, inlinedAt: !865)
!867 = !DILocation(line: 1514, column: 10, scope: !861, inlinedAt: !865)
!868 = !DILocation(line: 52, column: 6, scope: !835)
!869 = !DILocation(line: 60, column: 9, scope: !835)
!870 = !DILocation(line: 61, column: 8, scope: !835)
!871 = !DILocation(line: 78, column: 3, scope: !808, inlinedAt: !872)
!872 = distinct !DILocation(line: 63, column: 37, scope: !835)
!873 = !DILocation(line: 63, column: 37, scope: !835)
!874 = !DILocation(line: 63, column: 28, scope: !835)
!875 = !DILocation(line: 63, column: 2, scope: !835)
!876 = !DILocation(line: 63, column: 35, scope: !835)
!877 = !{!878, !878, i64 0}
!878 = !{!"short", !725, i64 0}
!879 = !DILocation(line: 79, column: 3, scope: !820, inlinedAt: !880)
!880 = distinct !DILocation(line: 64, column: 37, scope: !835)
!881 = !DILocation(line: 64, column: 37, scope: !835)
!882 = !DILocation(line: 64, column: 31, scope: !835)
!883 = !DILocation(line: 64, column: 2, scope: !835)
!884 = !DILocation(line: 64, column: 35, scope: !835)
!885 = !DILocation(line: 67, column: 3, scope: !802, inlinedAt: !886)
!886 = distinct !DILocation(line: 65, column: 37, scope: !835)
!887 = !DILocation(line: 65, column: 37, scope: !835)
!888 = !DILocation(line: 65, column: 31, scope: !835)
!889 = !DILocation(line: 65, column: 2, scope: !835)
!890 = !DILocation(line: 65, column: 35, scope: !835)
!891 = !DILocation(line: 68, column: 3, scope: !814, inlinedAt: !892)
!892 = distinct !DILocation(line: 66, column: 37, scope: !835)
!893 = !DILocation(line: 66, column: 37, scope: !835)
!894 = !DILocation(line: 66, column: 31, scope: !835)
!895 = !DILocation(line: 66, column: 2, scope: !835)
!896 = !DILocation(line: 66, column: 35, scope: !835)
!897 = !DILocation(line: 67, column: 35, scope: !835)
!898 = !DILocation(line: 67, column: 27, scope: !835)
!899 = !DILocation(line: 67, column: 29, scope: !835)
!900 = !DILocation(line: 67, column: 2, scope: !835)
!901 = !DILocation(line: 67, column: 33, scope: !835)
!902 = !{!903, !903, i64 0}
!903 = !{!"long", !725, i64 0}
!904 = !DILocation(line: 68, column: 31, scope: !835)
!905 = !DILocation(line: 68, column: 2, scope: !835)
!906 = !DILocation(line: 68, column: 35, scope: !835)
!907 = !DILocation(line: 69, column: 31, scope: !835)
!908 = !DILocation(line: 69, column: 2, scope: !835)
!909 = !DILocation(line: 69, column: 35, scope: !835)
!910 = !DILocation(line: 70, column: 22, scope: !835)
!911 = !DILocation(line: 70, column: 24, scope: !835)
!912 = !DILocation(line: 70, column: 2, scope: !835)
!913 = !DILocation(line: 70, column: 28, scope: !835)
!914 = !{!915, !915, i64 0}
!915 = !{!"int", !725, i64 0}
!916 = !DILocation(line: 75, column: 10, scope: !917)
!917 = distinct !DILexicalBlock(scope: !835, file: !3, line: 75, column: 6)
!918 = !DILocation(line: 75, column: 6, scope: !835)
!919 = !DILocation(line: 76, column: 3, scope: !917)
!920 = !DILocation(line: 80, column: 1, scope: !835)
!921 = distinct !DISubprogram(name: "dumpLines", linkageName: "_Z9dumpLinesv", scope: !3, file: !3, line: 82, type: !277, isLocal: false, isDefinition: true, scopeLine: 83, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !922)
!922 = !{!923}
!923 = !DILocalVariable(name: "ii", scope: !921, file: !3, line: 86, type: !8)
!924 = !DILocation(line: 67, column: 3, scope: !802, inlinedAt: !925)
!925 = distinct !DILocation(line: 84, column: 6, scope: !926)
!926 = distinct !DILexicalBlock(scope: !921, file: !3, line: 84, column: 6)
!927 = !DILocation(line: 84, column: 18, scope: !926)
!928 = !DILocation(line: 84, column: 23, scope: !926)
!929 = !DILocation(line: 78, column: 3, scope: !808, inlinedAt: !930)
!930 = distinct !DILocation(line: 84, column: 26, scope: !931)
!931 = !DILexicalBlockFile(scope: !926, file: !3, discriminator: 1)
!932 = !DILocation(line: 84, column: 37, scope: !931)
!933 = !DILocation(line: 84, column: 42, scope: !931)
!934 = !DILocation(line: 68, column: 3, scope: !814, inlinedAt: !935)
!935 = distinct !DILocation(line: 84, column: 45, scope: !936)
!936 = !DILexicalBlockFile(scope: !926, file: !3, discriminator: 2)
!937 = !DILocation(line: 84, column: 57, scope: !936)
!938 = !DILocation(line: 84, column: 62, scope: !936)
!939 = !DILocation(line: 79, column: 3, scope: !820, inlinedAt: !940)
!940 = distinct !DILocation(line: 84, column: 65, scope: !941)
!941 = !DILexicalBlockFile(scope: !926, file: !3, discriminator: 3)
!942 = !DILocation(line: 84, column: 76, scope: !941)
!943 = !DILocation(line: 84, column: 6, scope: !944)
!944 = !DILexicalBlockFile(scope: !921, file: !3, discriminator: 3)
!945 = !DILocation(line: 91, column: 2, scope: !921)
!946 = !DILocation(line: 102, column: 1, scope: !921)
!947 = !DILocation(line: 102, column: 1, scope: !948)
!948 = !DILexicalBlockFile(scope: !921, file: !3, discriminator: 1)
!949 = distinct !DISubprogram(name: "print1", linkageName: "_Z6print1i", scope: !3, file: !3, line: 104, type: !324, isLocal: false, isDefinition: true, scopeLine: 105, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !950)
!950 = !{!951}
!951 = !DILocalVariable(name: "a", arg: 1, scope: !949, file: !3, line: 104, type: !8)
!952 = !DILocation(line: 104, column: 28, scope: !949)
!953 = !DILocation(line: 67, column: 3, scope: !802, inlinedAt: !954)
!954 = distinct !DILocation(line: 106, column: 6, scope: !955)
!955 = distinct !DILexicalBlock(scope: !949, file: !3, line: 106, column: 6)
!956 = !DILocation(line: 106, column: 18, scope: !955)
!957 = !DILocation(line: 106, column: 23, scope: !955)
!958 = !DILocation(line: 68, column: 3, scope: !814, inlinedAt: !959)
!959 = distinct !DILocation(line: 106, column: 26, scope: !960)
!960 = !DILexicalBlockFile(scope: !955, file: !3, discriminator: 1)
!961 = !DILocation(line: 106, column: 38, scope: !960)
!962 = !DILocation(line: 106, column: 6, scope: !963)
!963 = !DILexicalBlockFile(scope: !949, file: !3, discriminator: 1)
!964 = !DILocation(line: 78, column: 3, scope: !808, inlinedAt: !965)
!965 = distinct !DILocation(line: 107, column: 6, scope: !966)
!966 = distinct !DILexicalBlock(scope: !949, file: !3, line: 107, column: 6)
!967 = !DILocation(line: 107, column: 17, scope: !966)
!968 = !DILocation(line: 107, column: 22, scope: !966)
!969 = !DILocation(line: 79, column: 3, scope: !820, inlinedAt: !970)
!970 = distinct !DILocation(line: 107, column: 25, scope: !971)
!971 = !DILexicalBlockFile(scope: !966, file: !3, discriminator: 1)
!972 = !DILocation(line: 107, column: 36, scope: !971)
!973 = !DILocation(line: 107, column: 6, scope: !963)
!974 = !DILocation(line: 109, column: 6, scope: !949)
!975 = !DILocation(line: 110, column: 3, scope: !976)
!976 = distinct !DILexicalBlock(scope: !949, file: !3, line: 109, column: 6)
!977 = !DILocation(line: 110, column: 3, scope: !978)
!978 = !DILexicalBlockFile(scope: !976, file: !3, discriminator: 2)
!979 = !DILocation(line: 112, column: 3, scope: !980)
!980 = distinct !DILexicalBlock(scope: !976, file: !3, line: 111, column: 11)
!981 = !DILocation(line: 112, column: 3, scope: !982)
!982 = !DILexicalBlockFile(scope: !980, file: !3, discriminator: 2)
!983 = !DILocation(line: 114, column: 3, scope: !980)
!984 = !DILocation(line: 116, column: 1, scope: !949)
!985 = distinct !DISubprogram(name: "print2", linkageName: "_Z6print2v", scope: !3, file: !3, line: 118, type: !277, isLocal: false, isDefinition: true, scopeLine: 119, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !4)
!986 = !DILocation(line: 67, column: 3, scope: !802, inlinedAt: !987)
!987 = distinct !DILocation(line: 120, column: 6, scope: !988)
!988 = distinct !DILexicalBlock(scope: !985, file: !3, line: 120, column: 6)
!989 = !DILocation(line: 120, column: 18, scope: !988)
!990 = !DILocation(line: 120, column: 23, scope: !988)
!991 = !DILocation(line: 68, column: 3, scope: !814, inlinedAt: !992)
!992 = distinct !DILocation(line: 120, column: 26, scope: !993)
!993 = !DILexicalBlockFile(scope: !988, file: !3, discriminator: 1)
!994 = !DILocation(line: 120, column: 38, scope: !993)
!995 = !DILocation(line: 120, column: 6, scope: !996)
!996 = !DILexicalBlockFile(scope: !985, file: !3, discriminator: 1)
!997 = !DILocation(line: 78, column: 3, scope: !808, inlinedAt: !998)
!998 = distinct !DILocation(line: 121, column: 6, scope: !999)
!999 = distinct !DILexicalBlock(scope: !985, file: !3, line: 121, column: 6)
!1000 = !DILocation(line: 121, column: 17, scope: !999)
!1001 = !DILocation(line: 121, column: 22, scope: !999)
!1002 = !DILocation(line: 79, column: 3, scope: !820, inlinedAt: !1003)
!1003 = distinct !DILocation(line: 121, column: 25, scope: !1004)
!1004 = !DILexicalBlockFile(scope: !999, file: !3, discriminator: 1)
!1005 = !DILocation(line: 121, column: 36, scope: !1004)
!1006 = !DILocation(line: 121, column: 6, scope: !996)
!1007 = !DILocation(line: 122, column: 9, scope: !985)
!1008 = !DILocation(line: 122, column: 9, scope: !1009)
!1009 = !DILexicalBlockFile(scope: !985, file: !3, discriminator: 2)
!1010 = !DILocation(line: 123, column: 1, scope: !985)
!1011 = !DILocation(line: 123, column: 1, scope: !996)
!1012 = distinct !DISubprogram(name: "print3", linkageName: "_Z6print3i", scope: !3, file: !3, line: 125, type: !324, isLocal: false, isDefinition: true, scopeLine: 126, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1013)
!1013 = !{!1014}
!1014 = !DILocalVariable(name: "a", arg: 1, scope: !1012, file: !3, line: 125, type: !8)
!1015 = !DILocation(line: 125, column: 28, scope: !1012)
!1016 = !DILocation(line: 67, column: 3, scope: !802, inlinedAt: !1017)
!1017 = distinct !DILocation(line: 127, column: 6, scope: !1018)
!1018 = distinct !DILexicalBlock(scope: !1012, file: !3, line: 127, column: 6)
!1019 = !DILocation(line: 127, column: 18, scope: !1018)
!1020 = !DILocation(line: 127, column: 23, scope: !1018)
!1021 = !DILocation(line: 68, column: 3, scope: !814, inlinedAt: !1022)
!1022 = distinct !DILocation(line: 127, column: 26, scope: !1023)
!1023 = !DILexicalBlockFile(scope: !1018, file: !3, discriminator: 1)
!1024 = !DILocation(line: 127, column: 38, scope: !1023)
!1025 = !DILocation(line: 127, column: 6, scope: !1026)
!1026 = !DILexicalBlockFile(scope: !1012, file: !3, discriminator: 1)
!1027 = !DILocation(line: 78, column: 3, scope: !808, inlinedAt: !1028)
!1028 = distinct !DILocation(line: 128, column: 6, scope: !1029)
!1029 = distinct !DILexicalBlock(scope: !1012, file: !3, line: 128, column: 6)
!1030 = !DILocation(line: 128, column: 17, scope: !1029)
!1031 = !DILocation(line: 128, column: 22, scope: !1029)
!1032 = !DILocation(line: 79, column: 3, scope: !820, inlinedAt: !1033)
!1033 = distinct !DILocation(line: 128, column: 25, scope: !1034)
!1034 = !DILexicalBlockFile(scope: !1029, file: !3, discriminator: 1)
!1035 = !DILocation(line: 128, column: 36, scope: !1034)
!1036 = !DILocation(line: 128, column: 6, scope: !1026)
!1037 = !DILocation(line: 129, column: 9, scope: !1012)
!1038 = !DILocation(line: 129, column: 9, scope: !1039)
!1039 = !DILexicalBlockFile(scope: !1012, file: !3, discriminator: 2)
!1040 = !DILocation(line: 130, column: 1, scope: !1012)
!1041 = !DILocation(line: 130, column: 1, scope: !1026)
!1042 = distinct !DISubprogram(name: "print5", linkageName: "_Z6print5Pviii", scope: !3, file: !3, line: 132, type: !1043, isLocal: false, isDefinition: true, scopeLine: 133, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1045)
!1043 = !DISubroutineType(types: !1044)
!1044 = !{null, !304, !8, !8, !8}
!1045 = !{!1046, !1047, !1048, !1049}
!1046 = !DILocalVariable(name: "p", arg: 1, scope: !1042, file: !3, line: 132, type: !304)
!1047 = !DILocalVariable(name: "bits", arg: 2, scope: !1042, file: !3, line: 132, type: !8)
!1048 = !DILocalVariable(name: "line", arg: 3, scope: !1042, file: !3, line: 132, type: !8)
!1049 = !DILocalVariable(name: "op", arg: 4, scope: !1042, file: !3, line: 132, type: !8)
!1050 = !DILocation(line: 132, column: 30, scope: !1042)
!1051 = !DILocation(line: 132, column: 37, scope: !1042)
!1052 = !DILocation(line: 132, column: 47, scope: !1042)
!1053 = !DILocation(line: 132, column: 57, scope: !1042)
!1054 = !DILocation(line: 136, column: 35, scope: !1042)
!1055 = !DILocation(line: 136, column: 30, scope: !1042)
!1056 = !DILocation(line: 136, column: 47, scope: !1042)
!1057 = !DILocation(line: 50, column: 34, scope: !835, inlinedAt: !1058)
!1058 = distinct !DILocation(line: 136, column: 9, scope: !1042)
!1059 = !DILocation(line: 50, column: 43, scope: !835, inlinedAt: !1058)
!1060 = !DILocation(line: 50, column: 64, scope: !835, inlinedAt: !1058)
!1061 = !DILocation(line: 50, column: 74, scope: !835, inlinedAt: !1058)
!1062 = !DILocation(line: 78, column: 53, scope: !851, inlinedAt: !1063)
!1063 = distinct !DILocation(line: 52, column: 12, scope: !835, inlinedAt: !1058)
!1064 = !DILocation(line: 78, column: 66, scope: !851, inlinedAt: !1063)
!1065 = !DILocation(line: 1512, column: 23, scope: !861, inlinedAt: !1066)
!1066 = distinct !DILocation(line: 80, column: 10, scope: !851, inlinedAt: !1063)
!1067 = !DILocation(line: 1512, column: 30, scope: !861, inlinedAt: !1066)
!1068 = !DILocation(line: 1514, column: 10, scope: !861, inlinedAt: !1066)
!1069 = !DILocation(line: 52, column: 6, scope: !835, inlinedAt: !1058)
!1070 = !DILocation(line: 60, column: 9, scope: !835, inlinedAt: !1058)
!1071 = !DILocation(line: 61, column: 8, scope: !835, inlinedAt: !1058)
!1072 = !DILocation(line: 78, column: 3, scope: !808, inlinedAt: !1073)
!1073 = distinct !DILocation(line: 63, column: 37, scope: !835, inlinedAt: !1058)
!1074 = !DILocation(line: 63, column: 37, scope: !835, inlinedAt: !1058)
!1075 = !DILocation(line: 63, column: 28, scope: !835, inlinedAt: !1058)
!1076 = !DILocation(line: 63, column: 2, scope: !835, inlinedAt: !1058)
!1077 = !DILocation(line: 63, column: 35, scope: !835, inlinedAt: !1058)
!1078 = !DILocation(line: 79, column: 3, scope: !820, inlinedAt: !1079)
!1079 = distinct !DILocation(line: 64, column: 37, scope: !835, inlinedAt: !1058)
!1080 = !DILocation(line: 64, column: 37, scope: !835, inlinedAt: !1058)
!1081 = !DILocation(line: 64, column: 31, scope: !835, inlinedAt: !1058)
!1082 = !DILocation(line: 64, column: 2, scope: !835, inlinedAt: !1058)
!1083 = !DILocation(line: 64, column: 35, scope: !835, inlinedAt: !1058)
!1084 = !DILocation(line: 67, column: 3, scope: !802, inlinedAt: !1085)
!1085 = distinct !DILocation(line: 65, column: 37, scope: !835, inlinedAt: !1058)
!1086 = !DILocation(line: 65, column: 37, scope: !835, inlinedAt: !1058)
!1087 = !DILocation(line: 65, column: 31, scope: !835, inlinedAt: !1058)
!1088 = !DILocation(line: 65, column: 2, scope: !835, inlinedAt: !1058)
!1089 = !DILocation(line: 65, column: 35, scope: !835, inlinedAt: !1058)
!1090 = !DILocation(line: 68, column: 3, scope: !814, inlinedAt: !1091)
!1091 = distinct !DILocation(line: 66, column: 37, scope: !835, inlinedAt: !1058)
!1092 = !DILocation(line: 66, column: 37, scope: !835, inlinedAt: !1058)
!1093 = !DILocation(line: 66, column: 31, scope: !835, inlinedAt: !1058)
!1094 = !DILocation(line: 66, column: 2, scope: !835, inlinedAt: !1058)
!1095 = !DILocation(line: 66, column: 35, scope: !835, inlinedAt: !1058)
!1096 = !DILocation(line: 67, column: 35, scope: !835, inlinedAt: !1058)
!1097 = !DILocation(line: 67, column: 27, scope: !835, inlinedAt: !1058)
!1098 = !DILocation(line: 67, column: 29, scope: !835, inlinedAt: !1058)
!1099 = !DILocation(line: 67, column: 2, scope: !835, inlinedAt: !1058)
!1100 = !DILocation(line: 67, column: 33, scope: !835, inlinedAt: !1058)
!1101 = !DILocation(line: 68, column: 31, scope: !835, inlinedAt: !1058)
!1102 = !DILocation(line: 68, column: 2, scope: !835, inlinedAt: !1058)
!1103 = !DILocation(line: 68, column: 35, scope: !835, inlinedAt: !1058)
!1104 = !DILocation(line: 69, column: 31, scope: !835, inlinedAt: !1058)
!1105 = !DILocation(line: 69, column: 2, scope: !835, inlinedAt: !1058)
!1106 = !DILocation(line: 69, column: 35, scope: !835, inlinedAt: !1058)
!1107 = !DILocation(line: 70, column: 22, scope: !835, inlinedAt: !1058)
!1108 = !DILocation(line: 70, column: 24, scope: !835, inlinedAt: !1058)
!1109 = !DILocation(line: 70, column: 2, scope: !835, inlinedAt: !1058)
!1110 = !DILocation(line: 70, column: 28, scope: !835, inlinedAt: !1058)
!1111 = !DILocation(line: 75, column: 10, scope: !917, inlinedAt: !1058)
!1112 = !DILocation(line: 75, column: 6, scope: !835, inlinedAt: !1058)
!1113 = !DILocation(line: 76, column: 3, scope: !917, inlinedAt: !1058)
!1114 = !DILocation(line: 80, column: 1, scope: !835, inlinedAt: !1058)
!1115 = !DILocation(line: 137, column: 1, scope: !1042)
!1116 = distinct !DISubprogram(name: "RetKernel", scope: !3, file: !3, line: 140, type: !277, isLocal: false, isDefinition: true, scopeLine: 141, flags: DIFlagPrototyped, isOptimized: true, unit: !2, variables: !1117)
!1117 = !{!1118}
!1118 = !DILocalVariable(name: "buffer_oN_DeViCe_short", scope: !1116, file: !3, line: 147, type: !9)
!1119 = !DILocation(line: 67, column: 3, scope: !802, inlinedAt: !1120)
!1120 = distinct !DILocation(line: 143, column: 6, scope: !1121)
!1121 = distinct !DILexicalBlock(scope: !1116, file: !3, line: 143, column: 6)
!1122 = !DILocation(line: 143, column: 18, scope: !1121)
!1123 = !DILocation(line: 143, column: 23, scope: !1121)
!1124 = !DILocation(line: 78, column: 3, scope: !808, inlinedAt: !1125)
!1125 = distinct !DILocation(line: 143, column: 26, scope: !1126)
!1126 = !DILexicalBlockFile(scope: !1121, file: !3, discriminator: 1)
!1127 = !DILocation(line: 143, column: 37, scope: !1126)
!1128 = !DILocation(line: 143, column: 42, scope: !1126)
!1129 = !DILocation(line: 68, column: 3, scope: !814, inlinedAt: !1130)
!1130 = distinct !DILocation(line: 143, column: 45, scope: !1131)
!1131 = !DILexicalBlockFile(scope: !1121, file: !3, discriminator: 2)
!1132 = !DILocation(line: 143, column: 57, scope: !1131)
!1133 = !DILocation(line: 143, column: 62, scope: !1131)
!1134 = !DILocation(line: 79, column: 3, scope: !820, inlinedAt: !1135)
!1135 = distinct !DILocation(line: 143, column: 65, scope: !1136)
!1136 = !DILexicalBlockFile(scope: !1121, file: !3, discriminator: 3)
!1137 = !DILocation(line: 143, column: 76, scope: !1136)
!1138 = !DILocation(line: 143, column: 6, scope: !1139)
!1139 = !DILexicalBlockFile(scope: !1116, file: !3, discriminator: 3)
!1140 = !DILocation(line: 145, column: 26, scope: !1116)
!1141 = !DILocation(line: 145, column: 24, scope: !1116)
!1142 = !DILocation(line: 147, column: 9, scope: !1116)
!1143 = !DILocation(line: 89, column: 3, scope: !1144, inlinedAt: !1145)
!1144 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_xEv", scope: !642, file: !604, line: 89, type: !608, isLocal: false, isDefinition: true, scopeLine: 89, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !644, variables: !4)
!1145 = distinct !DILocation(line: 148, column: 32, scope: !1116)
!1146 = !DILocation(line: 148, column: 32, scope: !1116)
!1147 = !DILocation(line: 148, column: 30, scope: !1116)
!1148 = !DILocation(line: 90, column: 3, scope: !1149, inlinedAt: !1150)
!1149 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_yEv", scope: !642, file: !604, line: 90, type: !608, isLocal: false, isDefinition: true, scopeLine: 90, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !645, variables: !4)
!1150 = distinct !DILocation(line: 149, column: 32, scope: !1116)
!1151 = !DILocation(line: 149, column: 32, scope: !1116)
!1152 = !DILocation(line: 149, column: 30, scope: !1116)
!1153 = !DILocation(line: 100, column: 3, scope: !1154, inlinedAt: !1180)
!1154 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_xEv", scope: !1155, file: !604, line: 100, type: !608, isLocal: false, isDefinition: true, scopeLine: 100, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1157, variables: !4)
!1155 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_gridDim_t", file: !604, line: 99, size: 8, elements: !1156, identifier: "_ZTS24__cuda_builtin_gridDim_t")
!1156 = !{!1157, !1158, !1159, !1160, !1165, !1169, !1173, !1176}
!1157 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_xEv", scope: !1155, file: !604, line: 100, type: !608, isLocal: false, isDefinition: false, scopeLine: 100, flags: DIFlagPrototyped, isOptimized: true)
!1158 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_yEv", scope: !1155, file: !604, line: 101, type: !608, isLocal: false, isDefinition: false, scopeLine: 101, flags: DIFlagPrototyped, isOptimized: true)
!1159 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_zEv", scope: !1155, file: !604, line: 102, type: !608, isLocal: false, isDefinition: false, scopeLine: 102, flags: DIFlagPrototyped, isOptimized: true)
!1160 = !DISubprogram(name: "operator dim3", linkageName: "_ZNK24__cuda_builtin_gridDim_tcv4dim3Ev", scope: !1155, file: !604, line: 105, type: !1161, isLocal: false, isDefinition: false, scopeLine: 105, flags: DIFlagPrototyped, isOptimized: true)
!1161 = !DISubroutineType(types: !1162)
!1162 = !{!650, !1163}
!1163 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1164, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1164 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1155)
!1165 = !DISubprogram(name: "__cuda_builtin_gridDim_t", scope: !1155, file: !604, line: 107, type: !1166, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1166 = !DISubroutineType(types: !1167)
!1167 = !{null, !1168}
!1168 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1155, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1169 = !DISubprogram(name: "__cuda_builtin_gridDim_t", scope: !1155, file: !604, line: 107, type: !1170, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1170 = !DISubroutineType(types: !1171)
!1171 = !{null, !1168, !1172}
!1172 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1164, size: 64)
!1173 = !DISubprogram(name: "operator=", linkageName: "_ZNK24__cuda_builtin_gridDim_taSERKS_", scope: !1155, file: !604, line: 107, type: !1174, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1174 = !DISubroutineType(types: !1175)
!1175 = !{null, !1163, !1172}
!1176 = !DISubprogram(name: "operator&", linkageName: "_ZNK24__cuda_builtin_gridDim_tadEv", scope: !1155, file: !604, line: 107, type: !1177, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1177 = !DISubroutineType(types: !1178)
!1178 = !{!1179, !1163}
!1179 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1155, size: 64)
!1180 = distinct !DILocation(line: 150, column: 32, scope: !1116)
!1181 = !{i32 1, i32 65536}
!1182 = !DILocation(line: 150, column: 32, scope: !1116)
!1183 = !DILocation(line: 150, column: 30, scope: !1116)
!1184 = !DILocation(line: 101, column: 3, scope: !1185, inlinedAt: !1186)
!1185 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_yEv", scope: !1155, file: !604, line: 101, type: !608, isLocal: false, isDefinition: true, scopeLine: 101, flags: DIFlagPrototyped, isOptimized: true, unit: !2, declaration: !1158, variables: !4)
!1186 = distinct !DILocation(line: 151, column: 32, scope: !1116)
!1187 = !DILocation(line: 151, column: 32, scope: !1116)
!1188 = !DILocation(line: 151, column: 30, scope: !1116)
!1189 = !DILocation(line: 152, column: 2, scope: !1116)
!1190 = !DILocation(line: 158, column: 9, scope: !1116)
!1191 = !DILocation(line: 159, column: 1, scope: !1116)
!1192 = !DILocation(line: 159, column: 1, scope: !1193)
!1193 = !DILexicalBlockFile(scope: !1116, file: !3, discriminator: 1)
