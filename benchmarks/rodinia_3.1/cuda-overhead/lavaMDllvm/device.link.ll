; ModuleID = '<stdin>'
source_filename = "llvm-link"
target datalayout = "e-i64:64-v16:16-v32:32-n16:32:64"
target triple = "nvptx64-nvidia-cuda"

%struct.FOUR_VECTOR = type { double, double, double, double }
%struct.CallSite_t = type { i32, i16, i16 }
%struct.par_str = type { double }
%struct.dim_str = type { i32, i32, i32, i32, i64, i64, i64, i64, i64 }
%struct.box_str = type { i32, i32, i32, i32, i64, i32, [26 x %struct.nei_str] }
%struct.nei_str = type { i32, i32, i32, i32, i64 }
%struct.THREE_VECTOR = type { double, double, double }
%printf_args = type { i32 }
%printf_args.1 = type { i32, i32, i32, i32 }
%printf_args.2 = type { i8* }
%printf_args.6 = type { i32, i32 }
%printf_args.7 = type { i32, i32, i32, i32, i32 }
%struct.BBlog_t = type { i16, i16, i16, i16, i64, i32, i32, i32 }
%printf_args.8 = type { i32* }
%printf_args.13 = type { i64 }
%printf_args.15 = type { i32, i32, i64, i64, i64 }

@_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9rA_shared = internal unnamed_addr addrspace(3) global [100 x %struct.FOUR_VECTOR] zeroinitializer, align 8, !dbg !0
@_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9rB_shared = internal unnamed_addr addrspace(3) global [100 x %struct.FOUR_VECTOR] zeroinitializer, align 8, !dbg !62
@_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9qB_shared = internal unnamed_addr addrspace(3) global [100 x double] zeroinitializer, align 8, !dbg !67
@CTALB = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !662
@CTAUB = local_unnamed_addr addrspace(1) externally_initialized global i32 999, align 4, !dbg !688
@CONSTANCE = local_unnamed_addr addrspace(1) externally_initialized global i32 128, align 4, !dbg !690
@VERBOSE = local_unnamed_addr addrspace(1) externally_initialized global i8 0, align 1, !dbg !692
@ccnntt = addrspace(1) externally_initialized global i64 1, align 8, !dbg !694
@bbccnntt = addrspace(1) externally_initialized global i64 1, align 8, !dbg !696
@done1 = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !698
@done2 = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !700
@funcDic = addrspace(1) externally_initialized global [15 x [31 x i8]] zeroinitializer, align 1, !dbg !702
@dicHeight = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !708
@globalCallStack = local_unnamed_addr addrspace(1) externally_initialized global [10 x %struct.CallSite_t*] zeroinitializer, align 8, !dbg !710
@stackHeight = local_unnamed_addr addrspace(1) externally_initialized global i32* null, align 8, !dbg !722
@contextDic = addrspace(1) externally_initialized global [20 x [5 x %struct.CallSite_t]] zeroinitializer, align 4, !dbg !724
@cHeight = local_unnamed_addr addrspace(1) externally_initialized global i32 0, align 4, !dbg !730
@.str = private unnamed_addr constant [35 x i8] c"height != 1 && \22stack height == 1\22\00", align 1
@__PRETTY_FUNCTION__._Z15updateCallStackiissi = private unnamed_addr constant [50 x i8] c"void updateCallStack(int, int, short, short, int)\00", align 1
@.str.2 = private unnamed_addr constant [20 x i8] c"first ever. tid=%d\0A\00", align 1
@.str.3 = private unnamed_addr constant [30 x i8] c"same caller different callee\0A\00", align 1
@.str.4 = private unnamed_addr constant [14 x i8] c"call squence\0A\00", align 1
@.str.5 = private unnamed_addr constant [50 x i8] c"(0==-1) && \22!! undefined things happeened here\5Cn\22\00", align 1
@.str.1 = private unnamed_addr constant [59 x i8] c"/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/ansf.cu\00", align 1
@.str.6 = private unnamed_addr constant [37 x i8] c" d::: current call stack height: %d\0A\00", align 1
@.str.7 = private unnamed_addr constant [30 x i8] c" %d: call site: %d, (%d, %d)\0A\00", align 1
@.str.8 = private unnamed_addr constant [15 x i8] c"d: caller: %s\0A\00", align 1
@.str.9 = private unnamed_addr constant [15 x i8] c"d: callee: %s\0A\00", align 1
@.str.10 = private unnamed_addr constant [15 x i8] c"d: return: %s\0A\00", align 1
@.str.11 = private unnamed_addr constant [18 x i8] c"d: undefined: %s\0A\00", align 1
@.str.12 = private unnamed_addr constant [11 x i8] c"id<cHeight\00", align 1
@__PRETTY_FUNCTION__._Z8cxtprinti = private unnamed_addr constant [19 x i8] c"void cxtprint(int)\00", align 1
@.str.13 = private unnamed_addr constant [41 x i8] c"d::: requested context id: %d out of %d\0A\00", align 1
@.str.14 = private unnamed_addr constant [47 x i8] c"d::::::: current context [%d][%d]: %d, %d, %d\0A\00", align 1
@.str.15 = private unnamed_addr constant [5 x i8] c"i<15\00", align 1
@__PRETTY_FUNCTION__._Z6cxtcpyP10CallSite_tS0_i = private unnamed_addr constant [45 x i8] c"void cxtcpy(CallSite_t *, CallSite_t *, int)\00", align 1
@.str.16 = private unnamed_addr constant [36 x i8] c"bbccnntt < 24*64*1024*1024/24 - 128\00", align 1
@__PRETTY_FUNCTION__.passBasicBlock = private unnamed_addr constant [43 x i8] c"void passBasicBlock(void *, int, int, int)\00", align 1
@buffer_oN_DeViCe = external addrspace(1) global [402653184 x i32], align 4
@.str.17 = private unnamed_addr constant [34 x i8] c"ccnntt < 24*64*1024*1024/24 - 128\00", align 1
@__PRETTY_FUNCTION__._Z10storeLinesPvssss = private unnamed_addr constant [52 x i8] c"void storeLines(void *, short, short, short, short)\00", align 1
@.str.18 = private unnamed_addr constant [22 x i8] c"d: buffer handle: %p\0A\00", align 1
@.str.19 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.20 = private unnamed_addr constant [24 x i8] c"d: load by CTA (%d,%d)\0A\00", align 1
@.str.21 = private unnamed_addr constant [25 x i8] c"d: store by CTA (%d,%d)\0A\00", align 1
@.str.22 = private unnamed_addr constant [23 x i8] c"d: !!! undefined !!! \0A\00", align 1
@.str.23 = private unnamed_addr constant [47 x i8] c"d: source line: %d\09 column: %d by CTA (%d,%d)\0A\00", align 1
@.str.24 = private unnamed_addr constant [49 x i8] c"d: Kernel Returns: collected [ %llu ] entries. \0A\00", align 1
@.str.25 = private unnamed_addr constant [57 x i8] c"size of function dic: %d %d %lu -> %lu , roundec to %lu\0A\00", align 1
@.str.26 = private unnamed_addr constant [56 x i8] c"size of context dic: %d %d %lu -> %lu , rounded to %lu\0A\00", align 1

; Function Attrs: convergent nounwind
define void @_Z15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_(%struct.par_str* byval nocapture readonly align 8 %d_par_gpu, %struct.dim_str* byval nocapture readonly align 8 %d_dim_gpu, %struct.box_str* nocapture readonly %d_box_gpu, %struct.FOUR_VECTOR* nocapture readonly %d_rv_gpu, double* nocapture readonly %d_qv_gpu, %struct.FOUR_VECTOR* nocapture %d_fv_gpu) local_unnamed_addr #0 !dbg !2 {
entry:
  tail call void @llvm.dbg.declare(metadata %struct.par_str* %d_par_gpu, metadata !627, metadata !1087), !dbg !1088
  tail call void @llvm.dbg.declare(metadata %struct.dim_str* %d_dim_gpu, metadata !628, metadata !1087), !dbg !1089
  tail call void @llvm.dbg.value(metadata %struct.box_str* %d_box_gpu, i64 0, metadata !629, metadata !1087), !dbg !1090
  tail call void @llvm.dbg.value(metadata %struct.FOUR_VECTOR* %d_rv_gpu, i64 0, metadata !630, metadata !1087), !dbg !1091
  tail call void @llvm.dbg.value(metadata double* %d_qv_gpu, i64 0, metadata !631, metadata !1087), !dbg !1092
  tail call void @llvm.dbg.value(metadata %struct.FOUR_VECTOR* %d_fv_gpu, i64 0, metadata !632, metadata !1087), !dbg !1093
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !1094, !range !1131
  tail call void @llvm.dbg.value(metadata i32 %0, i64 0, metadata !633, metadata !1087), !dbg !1132
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !1133, !range !1161
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !634, metadata !1087), !dbg !1162
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !1087), !dbg !1163
  %conv231 = zext i32 %0 to i64, !dbg !1164
  %number_boxes = getelementptr inbounds %struct.dim_str, %struct.dim_str* %d_dim_gpu, i64 0, i32 4, !dbg !1165
  %2 = load i64, i64* %number_boxes, align 8, !dbg !1165, !tbaa !1166
  %cmp = icmp sgt i64 %2, %conv231, !dbg !1172
  br i1 %cmp, label %if.then, label %if.end145, !dbg !1173

if.then:                                          ; preds = %entry
  %alpha = getelementptr inbounds %struct.par_str, %struct.par_str* %d_par_gpu, i64 0, i32 0, !dbg !1174
  %3 = load double, double* %alpha, align 8, !dbg !1174, !tbaa !1175
  %mul = fmul double %3, 2.000000e+00, !dbg !1178
  %mul3 = fmul double %3, %mul, !dbg !1179
  tail call void @llvm.dbg.value(metadata double %mul3, i64 0, metadata !636, metadata !1087), !dbg !1180
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !643, metadata !1087), !dbg !1181
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !647, metadata !1087), !dbg !1182
  tail call void @llvm.dbg.declare(metadata %struct.THREE_VECTOR* undef, metadata !655, metadata !1087), !dbg !1183
  %offset = getelementptr inbounds %struct.box_str, %struct.box_str* %d_box_gpu, i64 %conv231, i32 4, !dbg !1184
  %4 = load i64, i64* %offset, align 8, !dbg !1184, !tbaa !1185
  %sext = shl i64 %4, 32, !dbg !1187
  %idxprom5 = ashr exact i64 %sext, 32, !dbg !1187
  tail call void @llvm.dbg.value(metadata %struct.FOUR_VECTOR* %arrayidx6, i64 0, metadata !640, metadata !1087), !dbg !1188
  %arrayidx8 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %d_fv_gpu, i64 %idxprom5, !dbg !1189
  tail call void @llvm.dbg.value(metadata %struct.FOUR_VECTOR* %arrayidx8, i64 0, metadata !641, metadata !1087), !dbg !1190
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !1087), !dbg !1163
  %cmp9239 = icmp ult i32 %1, 100, !dbg !1191
  br i1 %cmp9239, label %while.end.loopexit, label %while.end, !dbg !1193

while.end.loopexit:                               ; preds = %if.then
  %arrayidx6 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %d_rv_gpu, i64 %idxprom5, !dbg !1187
  %idxprom10243 = zext i32 %1 to i64, !dbg !1194
  %arrayidx11 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx6, i64 %idxprom10243, !dbg !1194
  %arrayidx13230 = getelementptr inbounds [100 x %struct.FOUR_VECTOR], [100 x %struct.FOUR_VECTOR] addrspace(3)* @_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9rA_shared, i64 0, i64 %idxprom10243, !dbg !1196
  %5 = bitcast %struct.FOUR_VECTOR addrspace(3)* %arrayidx13230 to i8 addrspace(3)*, !dbg !1197
  %6 = addrspacecast i8 addrspace(3)* %5 to i8*, !dbg !1197
  %7 = bitcast %struct.FOUR_VECTOR* %arrayidx11 to i8*, !dbg !1197
  tail call void @llvm.memcpy.p0i8.p0i8.i64(i8* %6, i8* %7, i64 32, i32 8, i1 false), !dbg !1197, !tbaa.struct !1198
  br label %while.end, !dbg !1163

while.end:                                        ; preds = %while.end.loopexit, %if.then
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !1087), !dbg !1163
  tail call void @llvm.nvvm.barrier0(), !dbg !1200
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !643, metadata !1087), !dbg !1181
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !1087), !dbg !1163
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !643, metadata !1087), !dbg !1181
  %nn = getelementptr inbounds %struct.box_str, %struct.box_str* %d_box_gpu, i64 %conv231, i32 5, !dbg !1201
  %8 = load i32, i32* %nn, align 8, !dbg !1201, !tbaa !1205
  %cmp17237 = icmp slt i32 %8, 0, !dbg !1206
  br i1 %cmp17237, label %if.end145, label %for.body.preheader, !dbg !1207

for.body.preheader:                               ; preds = %while.end
  %idxprom35242 = zext i32 %1 to i64
  %arrayidx38228 = getelementptr inbounds [100 x %struct.FOUR_VECTOR], [100 x %struct.FOUR_VECTOR] addrspace(3)* @_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9rB_shared, i64 0, i64 %idxprom35242
  %9 = bitcast %struct.FOUR_VECTOR addrspace(3)* %arrayidx38228 to i8 addrspace(3)*
  %10 = addrspacecast i8 addrspace(3)* %9 to i8*
  %arrayidx42229 = getelementptr inbounds [100 x double], [100 x double] addrspace(3)* @_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9qB_shared, i64 0, i64 %idxprom35242
  %11 = bitcast double addrspace(3)* %arrayidx42229 to i64 addrspace(3)*
  %12 = addrspacecast i64 addrspace(3)* %11 to i64*
  %idxprom51241 = zext i32 %1 to i64
  %arrayidx52225 = getelementptr inbounds [100 x %struct.FOUR_VECTOR], [100 x %struct.FOUR_VECTOR] addrspace(3)* @_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9rA_shared, i64 0, i64 %idxprom51241
  %arrayidx52 = addrspacecast %struct.FOUR_VECTOR addrspace(3)* %arrayidx52225 to %struct.FOUR_VECTOR*
  %v = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx52, i64 0, i32 0
  %x = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx52, i64 0, i32 1
  %y = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx52, i64 0, i32 2
  %z = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx52, i64 0, i32 3
  %arrayidx116 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx8, i64 %idxprom51241
  %v117 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx116, i64 0, i32 0
  %x124 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx116, i64 0, i32 1
  %y131 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx116, i64 0, i32 2
  %z138 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx116, i64 0, i32 3
  br label %for.body, !dbg !1209

for.body:                                         ; preds = %for.body.preheader, %while.end141
  %k.0238 = phi i32 [ %inc143, %while.end141 ], [ 0, %for.body.preheader ]
  %cmp18 = icmp eq i32 %k.0238, 0, !dbg !1209
  tail call void @llvm.dbg.value(metadata i32 %13, i64 0, metadata !642, metadata !1087), !dbg !1212
  br i1 %cmp18, label %if.end, label %if.else, !dbg !1213

if.else:                                          ; preds = %for.body
  %sub = add nsw i32 %k.0238, -1, !dbg !1214
  %idxprom22 = sext i32 %sub to i64, !dbg !1216
  %number = getelementptr inbounds %struct.box_str, %struct.box_str* %d_box_gpu, i64 %conv231, i32 6, i64 %idxprom22, i32 3, !dbg !1217
  %13 = load i32, i32* %number, align 4, !dbg !1217, !tbaa !1218
  br label %if.end

if.end:                                           ; preds = %for.body, %if.else
  %pointer.0 = phi i32 [ %13, %if.else ], [ %0, %for.body ]
  tail call void @llvm.dbg.value(metadata i32 %pointer.0, i64 0, metadata !642, metadata !1087), !dbg !1212
  %idxprom24 = sext i32 %pointer.0 to i64, !dbg !1220
  %offset26 = getelementptr inbounds %struct.box_str, %struct.box_str* %d_box_gpu, i64 %idxprom24, i32 4, !dbg !1221
  %14 = load i64, i64* %offset26, align 8, !dbg !1221, !tbaa !1185
  %sext224 = shl i64 %14, 32, !dbg !1222
  %idxprom28 = ashr exact i64 %sext224, 32, !dbg !1222
  tail call void @llvm.dbg.value(metadata %struct.FOUR_VECTOR* %arrayidx29, i64 0, metadata !645, metadata !1087), !dbg !1223
  tail call void @llvm.dbg.value(metadata double* %arrayidx31, i64 0, metadata !646, metadata !1087), !dbg !1224
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !1087), !dbg !1163
  br i1 %cmp9239, label %while.end44.loopexit, label %while.end44, !dbg !1225

while.end44.loopexit:                             ; preds = %if.end
  %arrayidx31 = getelementptr inbounds double, double* %d_qv_gpu, i64 %idxprom28, !dbg !1227
  %arrayidx29 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %d_rv_gpu, i64 %idxprom28, !dbg !1222
  %arrayidx36 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx29, i64 %idxprom35242, !dbg !1228
  %15 = bitcast %struct.FOUR_VECTOR* %arrayidx36 to i8*, !dbg !1230
  tail call void @llvm.memcpy.p0i8.p0i8.i64(i8* %10, i8* %15, i64 32, i32 8, i1 false), !dbg !1230, !tbaa.struct !1198
  %arrayidx40 = getelementptr inbounds double, double* %arrayidx31, i64 %idxprom35242, !dbg !1231
  %16 = bitcast double* %arrayidx40 to i64*, !dbg !1231
  %17 = load i64, i64* %16, align 8, !dbg !1231, !tbaa !1199
  store i64 %17, i64* %12, align 8, !dbg !1232, !tbaa !1199
  br label %while.end44, !dbg !1163

while.end44:                                      ; preds = %while.end44.loopexit, %if.end
  %cmp9239.pr = phi i1 [ %cmp9239, %while.end44.loopexit ], [ false, %if.end ]
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !1087), !dbg !1163
  tail call void @llvm.nvvm.barrier0(), !dbg !1233
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !1087), !dbg !1163
  br i1 %cmp9239.pr, label %for.body50.preheader, label %while.end141, !dbg !1234

for.body50.preheader:                             ; preds = %while.end44
  br label %for.body50, !dbg !1235

for.body50:                                       ; preds = %for.body50.preheader, %_ZL3expd.exit
  %j.0234 = phi i32 [ %inc, %_ZL3expd.exit ], [ 0, %for.body50.preheader ]
  %18 = load double, double* %v, align 8, !dbg !1235, !tbaa !1240
  %idxprom53 = sext i32 %j.0234 to i64, !dbg !1242
  %arrayidx54226 = getelementptr inbounds [100 x %struct.FOUR_VECTOR], [100 x %struct.FOUR_VECTOR] addrspace(3)* @_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9rB_shared, i64 0, i64 %idxprom53, !dbg !1242
  %arrayidx54 = addrspacecast %struct.FOUR_VECTOR addrspace(3)* %arrayidx54226 to %struct.FOUR_VECTOR*, !dbg !1242
  %v55 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx54, i64 0, i32 0, !dbg !1243
  %19 = load double, double* %v55, align 8, !dbg !1243, !tbaa !1240
  %add56 = fadd double %18, %19, !dbg !1244
  %20 = load double, double* %x, align 8, !dbg !1245, !tbaa !1246
  %x61 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx54, i64 0, i32 1, !dbg !1245
  %21 = load double, double* %x61, align 8, !dbg !1245, !tbaa !1246
  %mul62 = fmul double %20, %21, !dbg !1245
  %22 = load double, double* %y, align 8, !dbg !1245, !tbaa !1247
  %y67 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx54, i64 0, i32 2, !dbg !1245
  %23 = load double, double* %y67, align 8, !dbg !1245, !tbaa !1247
  %mul68 = fmul double %22, %23, !dbg !1245
  %add69 = fadd double %mul62, %mul68, !dbg !1245
  %24 = load double, double* %z, align 8, !dbg !1245, !tbaa !1248
  %z74 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx54, i64 0, i32 3, !dbg !1245
  %25 = load double, double* %z74, align 8, !dbg !1245, !tbaa !1248
  %mul75 = fmul double %24, %25, !dbg !1245
  %add76 = fadd double %add69, %mul75, !dbg !1245
  %sub77 = fsub double %add56, %add76, !dbg !1249
  tail call void @llvm.dbg.value(metadata double %sub77, i64 0, metadata !648, metadata !1087), !dbg !1250
  %mul78 = fmul double %mul3, %sub77, !dbg !1251
  tail call void @llvm.dbg.value(metadata double %mul78, i64 0, metadata !649, metadata !1087), !dbg !1252
  %sub79 = fsub double -0.000000e+00, %mul78, !dbg !1253
  tail call void @llvm.dbg.value(metadata double %sub79, i64 0, metadata !1254, metadata !1087) #10, !dbg !1258
  %26 = tail call i32 @llvm.nvvm.d2i.hi(double %sub79) #10, !dbg !1260
  %27 = bitcast i32 %26 to float, !dbg !1260
  %28 = tail call float @llvm.fabs.f32(float %27) #10, !dbg !1260
  %29 = fcmp olt float %28, 0x4010E92220000000, !dbg !1260
  br i1 %29, label %30, label %62, !dbg !1260

; <label>:30:                                     ; preds = %for.body50
  %31 = fmul double %mul78, 0x3FF71547652B82FE, !dbg !1260
  %32 = fsub double 0x4338000000000000, %31, !dbg !1260
  %33 = tail call i32 @llvm.nvvm.d2i.lo(double %32) #10, !dbg !1260
  %34 = fadd double %32, 0xC338000000000000, !dbg !1260
  %35 = tail call double @llvm.fma.f64(double %34, double 0xBFE62E42FEFA39EF, double %sub79) #10, !dbg !1260
  %36 = tail call double @llvm.fma.f64(double %34, double 0xBC7ABC9E3B39803F, double %35) #10, !dbg !1260
  %37 = tail call double @llvm.fma.f64(double %36, double 0x3E5ADE1569CE2BDF, double 0x3E928AF3FCA213EA) #10, !dbg !1260
  %38 = tail call double @llvm.fma.f64(double %37, double %36, double 0x3EC71DEE62401315) #10, !dbg !1260
  %39 = tail call double @llvm.fma.f64(double %38, double %36, double 0x3EFA01997C89EB71) #10, !dbg !1260
  %40 = tail call double @llvm.fma.f64(double %39, double %36, double 0x3F2A01A014761F65) #10, !dbg !1260
  %41 = tail call double @llvm.fma.f64(double %40, double %36, double 0x3F56C16C1852B7AF) #10, !dbg !1260
  %42 = tail call double @llvm.fma.f64(double %41, double %36, double 0x3F81111111122322) #10, !dbg !1260
  %43 = tail call double @llvm.fma.f64(double %42, double %36, double 0x3FA55555555502A1) #10, !dbg !1260
  %44 = tail call double @llvm.fma.f64(double %43, double %36, double 0x3FC5555555555511) #10, !dbg !1260
  %45 = tail call double @llvm.fma.f64(double %44, double %36, double 0x3FE000000000000B) #10, !dbg !1260
  %46 = tail call double @llvm.fma.f64(double %45, double %36, double 1.000000e+00) #10, !dbg !1260
  %47 = tail call double @llvm.fma.f64(double %46, double %36, double 1.000000e+00) #10, !dbg !1260
  %neg.i.i = sub i32 0, %33, !dbg !1260
  %abs.cond.i.i = icmp sgt i32 %33, -1, !dbg !1260
  %abs.i.i = select i1 %abs.cond.i.i, i32 %33, i32 %neg.i.i, !dbg !1260
  %48 = icmp slt i32 %abs.i.i, 1023, !dbg !1260
  br i1 %48, label %49, label %52, !dbg !1260

; <label>:49:                                     ; preds = %30
  %50 = shl i32 %33, 20, !dbg !1260
  %51 = add nsw i32 %50, 1072693248, !dbg !1260
  br label %__internal_exp_kernel.exit.i.i, !dbg !1260

; <label>:52:                                     ; preds = %30
  %53 = add nsw i32 %33, 2046, !dbg !1260
  %54 = lshr i32 %53, 1, !dbg !1260
  %55 = shl i32 %54, 20, !dbg !1260
  %56 = shl i32 %53, 20, !dbg !1260
  %57 = sub i32 %56, %55, !dbg !1260
  %58 = tail call double @llvm.nvvm.lohi.i2d(i32 0, i32 %55) #10, !dbg !1260
  %59 = fmul double %47, %58, !dbg !1260
  br label %__internal_exp_kernel.exit.i.i, !dbg !1260

__internal_exp_kernel.exit.i.i:                   ; preds = %52, %49
  %a.addr.0.i.i.i.i = phi double [ %47, %49 ], [ %59, %52 ], !dbg !1260
  %k.0.i.i.i.i = phi i32 [ %51, %49 ], [ %57, %52 ], !dbg !1260
  %60 = tail call double @llvm.nvvm.lohi.i2d(i32 0, i32 %k.0.i.i.i.i) #10, !dbg !1260
  %61 = fmul double %a.addr.0.i.i.i.i, %60, !dbg !1260
  br label %_ZL3expd.exit, !dbg !1260

; <label>:62:                                     ; preds = %for.body50
  %63 = icmp slt i32 %26, 0, !dbg !1260
  %64 = select i1 %63, double 0.000000e+00, double 0x7FF0000000000000, !dbg !1260
  %65 = tail call double @llvm.fabs.f64(double %sub79) #10, !dbg !1260
  %66 = fcmp ugt double %65, 0x7FF0000000000000, !dbg !1260
  %67 = fsub double %sub79, %mul78, !dbg !1260
  %..i.i = select i1 %66, double %67, double %64, !dbg !1260
  br label %_ZL3expd.exit, !dbg !1260

_ZL3expd.exit:                                    ; preds = %__internal_exp_kernel.exit.i.i, %62
  %t.1.i.i = phi double [ %61, %__internal_exp_kernel.exit.i.i ], [ %..i.i, %62 ], !dbg !1260
  tail call void @llvm.dbg.value(metadata double %t.1.i.i, i64 0, metadata !650, metadata !1087), !dbg !1261
  %mul81 = fmul double %t.1.i.i, 2.000000e+00, !dbg !1262
  tail call void @llvm.dbg.value(metadata double %mul81, i64 0, metadata !651, metadata !1087), !dbg !1263
  %sub88 = fsub double %20, %21, !dbg !1264
  tail call void @llvm.dbg.value(metadata double %sub88, i64 0, metadata !655, metadata !1265), !dbg !1183
  %mul91 = fmul double %mul81, %sub88, !dbg !1266
  tail call void @llvm.dbg.value(metadata double %mul91, i64 0, metadata !652, metadata !1087), !dbg !1267
  %sub98 = fsub double %22, %23, !dbg !1268
  tail call void @llvm.dbg.value(metadata double %sub98, i64 0, metadata !655, metadata !1269), !dbg !1183
  %mul101 = fmul double %mul81, %sub98, !dbg !1270
  tail call void @llvm.dbg.value(metadata double %mul101, i64 0, metadata !653, metadata !1087), !dbg !1271
  %sub108 = fsub double %24, %25, !dbg !1272
  tail call void @llvm.dbg.value(metadata double %sub108, i64 0, metadata !655, metadata !1273), !dbg !1183
  %mul111 = fmul double %mul81, %sub108, !dbg !1274
  tail call void @llvm.dbg.value(metadata double %mul111, i64 0, metadata !654, metadata !1087), !dbg !1275
  %arrayidx113227 = getelementptr inbounds [100 x double], [100 x double] addrspace(3)* @_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9qB_shared, i64 0, i64 %idxprom53, !dbg !1276
  %arrayidx113 = addrspacecast double addrspace(3)* %arrayidx113227 to double*, !dbg !1276
  %68 = load double, double* %arrayidx113, align 8, !dbg !1276, !tbaa !1199
  %mul114 = fmul double %t.1.i.i, %68, !dbg !1277
  %69 = load double, double* %v117, align 8, !dbg !1278, !tbaa !1240
  %add118 = fadd double %69, %mul114, !dbg !1278
  store double %add118, double* %v117, align 8, !dbg !1278, !tbaa !1240
  %70 = load double, double* %arrayidx113, align 8, !dbg !1279, !tbaa !1199
  %mul121 = fmul double %mul91, %70, !dbg !1280
  %71 = load double, double* %x124, align 8, !dbg !1281, !tbaa !1246
  %add125 = fadd double %71, %mul121, !dbg !1281
  store double %add125, double* %x124, align 8, !dbg !1281, !tbaa !1246
  %72 = load double, double* %arrayidx113, align 8, !dbg !1282, !tbaa !1199
  %mul128 = fmul double %mul101, %72, !dbg !1283
  %73 = load double, double* %y131, align 8, !dbg !1284, !tbaa !1247
  %add132 = fadd double %73, %mul128, !dbg !1284
  store double %add132, double* %y131, align 8, !dbg !1284, !tbaa !1247
  %74 = load double, double* %arrayidx113, align 8, !dbg !1285, !tbaa !1199
  %mul135 = fmul double %mul111, %74, !dbg !1286
  %75 = load double, double* %z138, align 8, !dbg !1287, !tbaa !1248
  %add139 = fadd double %75, %mul135, !dbg !1287
  store double %add139, double* %z138, align 8, !dbg !1287, !tbaa !1248
  %inc = add nuw nsw i32 %j.0234, 1, !dbg !1288
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !647, metadata !1087), !dbg !1182
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !647, metadata !1087), !dbg !1182
  %exitcond = icmp eq i32 %inc, 100, !dbg !1290
  br i1 %exitcond, label %while.end141.loopexit, label %for.body50, !dbg !1292, !llvm.loop !1294

while.end141.loopexit:                            ; preds = %_ZL3expd.exit
  br label %while.end141, !dbg !1163

while.end141:                                     ; preds = %while.end141.loopexit, %while.end44
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !1087), !dbg !1163
  tail call void @llvm.nvvm.barrier0(), !dbg !1297
  %inc143 = add nuw nsw i32 %k.0238, 1, !dbg !1298
  tail call void @llvm.dbg.value(metadata i32 %inc143, i64 0, metadata !643, metadata !1087), !dbg !1181
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !1087), !dbg !1163
  tail call void @llvm.dbg.value(metadata i32 %inc143, i64 0, metadata !643, metadata !1087), !dbg !1181
  %76 = load i32, i32* %nn, align 8, !dbg !1201, !tbaa !1205
  %cmp17 = icmp slt i32 %k.0238, %76, !dbg !1206
  br i1 %cmp17, label %for.body, label %if.end145.loopexit, !dbg !1207, !llvm.loop !1300

if.end145.loopexit:                               ; preds = %while.end141
  br label %if.end145, !dbg !1303

if.end145:                                        ; preds = %if.end145.loopexit, %while.end, %entry
  ret void, !dbg !1303
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

; Function Attrs: nounwind readnone
declare void @llvm.dbg.value(metadata, i64, metadata, metadata) #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.x() #1

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture writeonly, i8* nocapture readonly, i64, i32, i1) #2

; Function Attrs: convergent nounwind
declare void @llvm.nvvm.barrier0() #3

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.d2i.hi(double) #1

; Function Attrs: nounwind readnone
declare float @llvm.fabs.f32(float) #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.d2i.lo(double) #1

; Function Attrs: nounwind readnone
declare double @llvm.fma.f64(double, double, double) #1

; Function Attrs: nounwind readnone
declare double @llvm.nvvm.lohi.i2d(i32, i32) #1

; Function Attrs: nounwind readnone
declare double @llvm.fabs.f64(double) #1

; Function Attrs: nounwind
define void @_Z8mystrcpyPcS_(i8* nocapture %dst, i8* nocapture readonly %src) local_unnamed_addr #4 !dbg !1304 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %dst, i64 0, metadata !1308, metadata !1087), !dbg !1311
  tail call void @llvm.dbg.value(metadata i8* %src, i64 0, metadata !1309, metadata !1087), !dbg !1312
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1310, metadata !1087), !dbg !1313
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1310, metadata !1087), !dbg !1313
  %0 = load i8, i8* %src, align 1, !dbg !1314, !tbaa !1316
  %cmp16 = icmp eq i8 %0, 0, !dbg !1317
  br i1 %cmp16, label %while.end, label %while.body.preheader, !dbg !1318

while.body.preheader:                             ; preds = %entry
  br label %while.body, !dbg !1320

while.body:                                       ; preds = %while.body.preheader, %while.body
  %arrayidx518 = phi i8* [ %arrayidx5, %while.body ], [ %dst, %while.body.preheader ]
  %1 = phi i8 [ %2, %while.body ], [ %0, %while.body.preheader ]
  %cnt.017 = phi i32 [ %inc, %while.body ], [ 0, %while.body.preheader ]
  store i8 %1, i8* %arrayidx518, align 1, !dbg !1320, !tbaa !1316
  %inc = add nuw nsw i32 %cnt.017, 1, !dbg !1322
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1310, metadata !1087), !dbg !1313
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1310, metadata !1087), !dbg !1313
  %idxprom = sext i32 %inc to i64, !dbg !1314
  %arrayidx = getelementptr inbounds i8, i8* %src, i64 %idxprom, !dbg !1314
  %2 = load i8, i8* %arrayidx, align 1, !dbg !1314, !tbaa !1316
  %cmp = icmp ne i8 %2, 0, !dbg !1317
  %cmp1 = icmp slt i32 %inc, 30, !dbg !1323
  %3 = and i1 %cmp1, %cmp, !dbg !1325
  %arrayidx5 = getelementptr inbounds i8, i8* %dst, i64 %idxprom
  br i1 %3, label %while.body, label %while.end.loopexit, !dbg !1318, !llvm.loop !1326

while.end.loopexit:                               ; preds = %while.body
  br label %while.end, !dbg !1329

while.end:                                        ; preds = %while.end.loopexit, %entry
  %arrayidx5.lcssa = phi i8* [ %dst, %entry ], [ %arrayidx5, %while.end.loopexit ]
  store i8 0, i8* %arrayidx5.lcssa, align 1, !dbg !1329, !tbaa !1316
  ret void, !dbg !1330
}

; Function Attrs: nounwind readonly
define zeroext i1 @_Z8mystrcmpPcS_(i8* nocapture readonly %dst, i8* nocapture readonly %src) local_unnamed_addr #5 !dbg !1331 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %dst, i64 0, metadata !1335, metadata !1087), !dbg !1338
  tail call void @llvm.dbg.value(metadata i8* %src, i64 0, metadata !1336, metadata !1087), !dbg !1339
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1337, metadata !1087), !dbg !1340
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1337, metadata !1087), !dbg !1340
  br label %while.body, !dbg !1341

while.cond:                                       ; preds = %if.end
  %inc = add nuw nsw i32 %cnt.023, 1, !dbg !1343
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1337, metadata !1087), !dbg !1340
  %idxprom.1 = sext i32 %inc to i64, !dbg !1345
  %arrayidx.1 = getelementptr inbounds i8, i8* %dst, i64 %idxprom.1, !dbg !1345
  %0 = load i8, i8* %arrayidx.1, align 1, !dbg !1345, !tbaa !1316
  %1 = getelementptr inbounds i8, i8* %src, i64 %idxprom.1, !dbg !1347
  %2 = load i8, i8* %1, align 1, !dbg !1347, !tbaa !1316
  %3 = or i8 %2, %0, !dbg !1349
  %4 = icmp eq i8 %3, 0, !dbg !1349
  br i1 %4, label %cleanup, label %if.end.1, !dbg !1349

while.body:                                       ; preds = %while.cond.2, %entry
  %cnt.023 = phi i32 [ 0, %entry ], [ %inc.2, %while.cond.2 ]
  %idxprom = sext i32 %cnt.023 to i64, !dbg !1345
  %arrayidx = getelementptr inbounds i8, i8* %dst, i64 %idxprom, !dbg !1345
  %5 = load i8, i8* %arrayidx, align 1, !dbg !1345, !tbaa !1316
  %6 = getelementptr inbounds i8, i8* %src, i64 %idxprom, !dbg !1347
  %7 = load i8, i8* %6, align 1, !dbg !1347, !tbaa !1316
  %8 = or i8 %7, %5, !dbg !1349
  %9 = icmp eq i8 %8, 0, !dbg !1349
  br i1 %9, label %cleanup, label %if.end, !dbg !1349

if.end:                                           ; preds = %while.body
  %cmp12 = icmp eq i8 %5, %7, !dbg !1350
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1337, metadata !1087), !dbg !1340
  br i1 %cmp12, label %while.cond, label %cleanup, !dbg !1352

cleanup:                                          ; preds = %while.cond.2, %if.end.2, %while.cond.1, %if.end.1, %while.cond, %if.end, %while.body
  %retval.0 = phi i1 [ true, %while.body ], [ false, %if.end ], [ true, %while.cond ], [ false, %if.end.1 ], [ true, %while.cond.1 ], [ false, %if.end.2 ], [ true, %while.cond.2 ]
  ret i1 %retval.0, !dbg !1353

if.end.1:                                         ; preds = %while.cond
  %cmp12.1 = icmp eq i8 %0, %2, !dbg !1350
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1337, metadata !1087), !dbg !1340
  br i1 %cmp12.1, label %while.cond.1, label %cleanup, !dbg !1352

while.cond.1:                                     ; preds = %if.end.1
  %inc.1 = add nsw i32 %cnt.023, 2, !dbg !1343
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1337, metadata !1087), !dbg !1340
  %idxprom.2 = sext i32 %inc.1 to i64, !dbg !1345
  %arrayidx.2 = getelementptr inbounds i8, i8* %dst, i64 %idxprom.2, !dbg !1345
  %10 = load i8, i8* %arrayidx.2, align 1, !dbg !1345, !tbaa !1316
  %11 = getelementptr inbounds i8, i8* %src, i64 %idxprom.2, !dbg !1347
  %12 = load i8, i8* %11, align 1, !dbg !1347, !tbaa !1316
  %13 = or i8 %12, %10, !dbg !1349
  %14 = icmp eq i8 %13, 0, !dbg !1349
  br i1 %14, label %cleanup, label %if.end.2, !dbg !1349

if.end.2:                                         ; preds = %while.cond.1
  %cmp12.2 = icmp eq i8 %10, %12, !dbg !1350
  %inc.2 = add nsw i32 %cnt.023, 3, !dbg !1343
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1337, metadata !1087), !dbg !1340
  br i1 %cmp12.2, label %while.cond.2, label %cleanup, !dbg !1352

while.cond.2:                                     ; preds = %if.end.2
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1337, metadata !1087), !dbg !1340
  %cmp.2 = icmp slt i32 %inc.2, 30, !dbg !1354
  br i1 %cmp.2, label %while.body, label %cleanup, !dbg !1341, !llvm.loop !1355
}

; Function Attrs: nounwind
define i32 @_Z9getFuncIDPc(i8* nocapture readonly %func) local_unnamed_addr #4 !dbg !1358 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %func, i64 0, metadata !1362, metadata !1087), !dbg !1368
  %0 = load i32, i32* addrspacecast (i32 addrspace(1)* @dicHeight to i32*), align 4, !dbg !1369, !tbaa !1371
  %cmp = icmp eq i32 %0, 0, !dbg !1372
  br i1 %cmp, label %if.then, label %for.cond.preheader, !dbg !1373

for.cond.preheader:                               ; preds = %entry
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1363, metadata !1087), !dbg !1374
  %cmp152 = icmp sgt i32 %0, 0, !dbg !1375
  br i1 %cmp152, label %for.body.preheader, label %for.end, !dbg !1377

for.body.preheader:                               ; preds = %for.cond.preheader
  br label %for.body, !dbg !1379

if.then:                                          ; preds = %entry
  tail call void @llvm.dbg.value(metadata i8* getelementptr ([15 x [31 x i8]], [15 x [31 x i8]]* addrspacecast ([15 x [31 x i8]] addrspace(1)* @funcDic to [15 x [31 x i8]]*), i64 0, i64 0, i64 0), i64 0, metadata !1308, metadata !1087), !dbg !1380
  tail call void @llvm.dbg.value(metadata i8* %func, i64 0, metadata !1309, metadata !1087), !dbg !1383
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1310, metadata !1087), !dbg !1384
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1310, metadata !1087), !dbg !1384
  %1 = load i8, i8* %func, align 1, !dbg !1385, !tbaa !1316
  %cmp16.i = icmp eq i8 %1, 0, !dbg !1386
  br i1 %cmp16.i, label %return.sink.split, label %while.body.i.preheader, !dbg !1387

while.body.i.preheader:                           ; preds = %if.then
  br label %while.body.i, !dbg !1388

while.body.i:                                     ; preds = %while.body.i.preheader, %while.body.i
  %arrayidx518.i = phi i8* [ %arrayidx5.i, %while.body.i ], [ getelementptr ([15 x [31 x i8]], [15 x [31 x i8]]* addrspacecast ([15 x [31 x i8]] addrspace(1)* @funcDic to [15 x [31 x i8]]*), i64 0, i64 0, i64 0), %while.body.i.preheader ]
  %2 = phi i8 [ %3, %while.body.i ], [ %1, %while.body.i.preheader ]
  %cnt.017.i = phi i32 [ %inc.i, %while.body.i ], [ 0, %while.body.i.preheader ]
  store i8 %2, i8* %arrayidx518.i, align 1, !dbg !1388, !tbaa !1316
  %inc.i = add nuw nsw i32 %cnt.017.i, 1, !dbg !1389
  tail call void @llvm.dbg.value(metadata i32 %inc.i, i64 0, metadata !1310, metadata !1087), !dbg !1384
  tail call void @llvm.dbg.value(metadata i32 %inc.i, i64 0, metadata !1310, metadata !1087), !dbg !1384
  %idxprom.i = sext i32 %inc.i to i64, !dbg !1385
  %arrayidx.i = getelementptr inbounds i8, i8* %func, i64 %idxprom.i, !dbg !1385
  %3 = load i8, i8* %arrayidx.i, align 1, !dbg !1385, !tbaa !1316
  %cmp.i = icmp ne i8 %3, 0, !dbg !1386
  %cmp1.i = icmp slt i32 %inc.i, 30, !dbg !1390
  %4 = and i1 %cmp1.i, %cmp.i, !dbg !1391
  %arrayidx5.i50 = getelementptr [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 0, i64 %idxprom.i
  %arrayidx5.i = addrspacecast i8 addrspace(1)* %arrayidx5.i50 to i8*
  br i1 %4, label %while.body.i, label %return.sink.split.loopexit, !dbg !1387, !llvm.loop !1326

for.body:                                         ; preds = %for.body.preheader, %for.inc
  %i.053 = phi i32 [ %inc4, %for.inc ], [ 0, %for.body.preheader ]
  %idxprom = sext i32 %i.053 to i64, !dbg !1379
  tail call void @llvm.dbg.value(metadata i8* %func, i64 0, metadata !1336, metadata !1087), !dbg !1392
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1337, metadata !1087), !dbg !1394
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1337, metadata !1087), !dbg !1394
  br label %while.body.i37, !dbg !1395

while.cond.i:                                     ; preds = %if.end.i
  %inc.i38 = or i32 %cnt.023.i, 1, !dbg !1396
  tail call void @llvm.dbg.value(metadata i32 %inc.i38, i64 0, metadata !1337, metadata !1087), !dbg !1394
  %idxprom.i35.1 = sext i32 %inc.i38 to i64, !dbg !1397
  %arrayidx.i3649.1 = getelementptr inbounds [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 %idxprom, i64 %idxprom.i35.1, !dbg !1397
  %arrayidx.i36.1 = addrspacecast i8 addrspace(1)* %arrayidx.i3649.1 to i8*, !dbg !1397
  %5 = load i8, i8* %arrayidx.i36.1, align 1, !dbg !1397, !tbaa !1316
  %6 = getelementptr inbounds i8, i8* %func, i64 %idxprom.i35.1, !dbg !1398
  %7 = load i8, i8* %6, align 1, !dbg !1398, !tbaa !1316
  %8 = or i8 %7, %5, !dbg !1399
  %9 = icmp eq i8 %8, 0, !dbg !1399
  br i1 %9, label %return.loopexit, label %if.end.i.1, !dbg !1399

while.body.i37:                                   ; preds = %while.cond.i.1, %for.body
  %cnt.023.i = phi i32 [ 0, %for.body ], [ %inc.i38.1, %while.cond.i.1 ]
  %idxprom.i35 = sext i32 %cnt.023.i to i64, !dbg !1397
  %arrayidx.i3649 = getelementptr inbounds [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 %idxprom, i64 %idxprom.i35, !dbg !1397
  %arrayidx.i36 = addrspacecast i8 addrspace(1)* %arrayidx.i3649 to i8*, !dbg !1397
  %10 = load i8, i8* %arrayidx.i36, align 1, !dbg !1397, !tbaa !1316
  %11 = getelementptr inbounds i8, i8* %func, i64 %idxprom.i35, !dbg !1398
  %12 = load i8, i8* %11, align 1, !dbg !1398, !tbaa !1316
  %13 = or i8 %12, %10, !dbg !1399
  %14 = icmp eq i8 %13, 0, !dbg !1399
  br i1 %14, label %return.loopexit, label %if.end.i, !dbg !1399

if.end.i:                                         ; preds = %while.body.i37
  %cmp12.i = icmp eq i8 %10, %12, !dbg !1400
  tail call void @llvm.dbg.value(metadata i32 %inc.i38, i64 0, metadata !1337, metadata !1087), !dbg !1394
  br i1 %cmp12.i, label %while.cond.i, label %for.inc, !dbg !1401

for.inc:                                          ; preds = %if.end.i.1, %if.end.i
  %inc4 = add nuw nsw i32 %i.053, 1, !dbg !1402
  tail call void @llvm.dbg.value(metadata i32 %inc4, i64 0, metadata !1363, metadata !1087), !dbg !1374
  tail call void @llvm.dbg.value(metadata i32 %inc4, i64 0, metadata !1363, metadata !1087), !dbg !1374
  %cmp1 = icmp slt i32 %inc4, %0, !dbg !1375
  br i1 %cmp1, label %for.body, label %for.end.loopexit, !dbg !1377, !llvm.loop !1404

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end, !dbg !1407

for.end:                                          ; preds = %for.end.loopexit, %for.cond.preheader
  %idxprom7 = sext i32 %0 to i64, !dbg !1407
  %arraydecay919 = getelementptr inbounds [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 %idxprom7, i64 0, !dbg !1407
  %arraydecay9 = addrspacecast i8 addrspace(1)* %arraydecay919 to i8*, !dbg !1407
  tail call void @llvm.dbg.value(metadata i8* %arraydecay9, i64 0, metadata !1308, metadata !1087), !dbg !1408
  tail call void @llvm.dbg.value(metadata i8* %func, i64 0, metadata !1309, metadata !1087), !dbg !1410
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1310, metadata !1087), !dbg !1411
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1310, metadata !1087), !dbg !1411
  %15 = load i8, i8* %func, align 1, !dbg !1412, !tbaa !1316
  %cmp16.i22 = icmp eq i8 %15, 0, !dbg !1413
  br i1 %cmp16.i22, label %return.sink.split, label %while.body.i31.preheader, !dbg !1414

while.body.i31.preheader:                         ; preds = %for.end
  br label %while.body.i31, !dbg !1415

while.body.i31:                                   ; preds = %while.body.i31.preheader, %while.body.i31
  %arrayidx518.i23 = phi i8* [ %arrayidx5.i30, %while.body.i31 ], [ %arraydecay9, %while.body.i31.preheader ]
  %16 = phi i8 [ %17, %while.body.i31 ], [ %15, %while.body.i31.preheader ]
  %cnt.017.i24 = phi i32 [ %inc.i25, %while.body.i31 ], [ 0, %while.body.i31.preheader ]
  store i8 %16, i8* %arrayidx518.i23, align 1, !dbg !1415, !tbaa !1316
  %inc.i25 = add nuw nsw i32 %cnt.017.i24, 1, !dbg !1416
  tail call void @llvm.dbg.value(metadata i32 %inc.i25, i64 0, metadata !1310, metadata !1087), !dbg !1411
  tail call void @llvm.dbg.value(metadata i32 %inc.i25, i64 0, metadata !1310, metadata !1087), !dbg !1411
  %idxprom.i26 = sext i32 %inc.i25 to i64, !dbg !1412
  %arrayidx.i27 = getelementptr inbounds i8, i8* %func, i64 %idxprom.i26, !dbg !1412
  %17 = load i8, i8* %arrayidx.i27, align 1, !dbg !1412, !tbaa !1316
  %cmp.i28 = icmp ne i8 %17, 0, !dbg !1413
  %cmp1.i29 = icmp slt i32 %inc.i25, 30, !dbg !1417
  %18 = and i1 %cmp1.i29, %cmp.i28, !dbg !1418
  %arrayidx5.i3048 = getelementptr inbounds [15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 %idxprom7, i64 %idxprom.i26
  %arrayidx5.i30 = addrspacecast i8 addrspace(1)* %arrayidx5.i3048 to i8*
  br i1 %18, label %while.body.i31, label %return.sink.split.loopexit58, !dbg !1414, !llvm.loop !1326

return.sink.split.loopexit:                       ; preds = %while.body.i
  br label %return.sink.split, !dbg !1419

return.sink.split.loopexit58:                     ; preds = %while.body.i31
  br label %return.sink.split, !dbg !1419

return.sink.split:                                ; preds = %return.sink.split.loopexit58, %return.sink.split.loopexit, %if.then, %for.end
  %arrayidx5.lcssa.i.sink = phi i8* [ %arraydecay9, %for.end ], [ getelementptr ([15 x [31 x i8]], [15 x [31 x i8]]* addrspacecast ([15 x [31 x i8]] addrspace(1)* @funcDic to [15 x [31 x i8]]*), i64 0, i64 0, i64 0), %if.then ], [ %arrayidx5.i, %return.sink.split.loopexit ], [ %arrayidx5.i30, %return.sink.split.loopexit58 ]
  %retval.3.ph = phi i32 [ %0, %for.end ], [ 0, %if.then ], [ 0, %return.sink.split.loopexit ], [ %0, %return.sink.split.loopexit58 ]
  store i8 0, i8* %arrayidx5.lcssa.i.sink, align 1, !dbg !1419, !tbaa !1316
  %inc10 = add nsw i32 %0, 1
  store i32 %inc10, i32* addrspacecast (i32 addrspace(1)* @dicHeight to i32*), align 4, !tbaa !1371
  br label %return, !dbg !1420

return.loopexit:                                  ; preds = %while.cond.i.1, %while.cond.i, %while.body.i37
  br label %return, !dbg !1420

return:                                           ; preds = %return.loopexit, %return.sink.split
  %retval.3 = phi i32 [ %retval.3.ph, %return.sink.split ], [ %i.053, %return.loopexit ]
  ret i32 %retval.3, !dbg !1420

if.end.i.1:                                       ; preds = %while.cond.i
  %cmp12.i.1 = icmp eq i8 %5, %7, !dbg !1400
  %inc.i38.1 = add nsw i32 %cnt.023.i, 2, !dbg !1396
  tail call void @llvm.dbg.value(metadata i32 %inc.i38, i64 0, metadata !1337, metadata !1087), !dbg !1394
  br i1 %cmp12.i.1, label %while.cond.i.1, label %for.inc, !dbg !1401

while.cond.i.1:                                   ; preds = %if.end.i.1
  tail call void @llvm.dbg.value(metadata i32 %inc.i38, i64 0, metadata !1337, metadata !1087), !dbg !1394
  %cmp.i34.1 = icmp slt i32 %inc.i38.1, 30, !dbg !1421
  br i1 %cmp.i34.1, label %while.body.i37, label %return.loopexit, !dbg !1395, !llvm.loop !1355
}

; Function Attrs: convergent nounwind
define void @_Z15updateCallStackiissi(i32 %caller, i32 %callee, i16 signext %sline, i16 signext %scolm, i32 %tid) local_unnamed_addr #0 !dbg !1422 {
entry:
  %tmp = alloca %printf_args, align 8
  tail call void @llvm.dbg.value(metadata i32 %caller, i64 0, metadata !1426, metadata !1087), !dbg !1438
  tail call void @llvm.dbg.value(metadata i32 %callee, i64 0, metadata !1427, metadata !1087), !dbg !1439
  tail call void @llvm.dbg.value(metadata i16 %sline, i64 0, metadata !1428, metadata !1087), !dbg !1440
  tail call void @llvm.dbg.value(metadata i16 %scolm, i64 0, metadata !1429, metadata !1087), !dbg !1441
  tail call void @llvm.dbg.value(metadata i32 %tid, i64 0, metadata !1430, metadata !1087), !dbg !1442
  %idxprom = sext i32 %tid to i64, !dbg !1443
  %arrayidx177 = getelementptr inbounds [10 x %struct.CallSite_t*], [10 x %struct.CallSite_t*] addrspace(1)* @globalCallStack, i64 0, i64 %idxprom, !dbg !1443
  %arrayidx = addrspacecast %struct.CallSite_t* addrspace(1)* %arrayidx177 to %struct.CallSite_t**, !dbg !1443
  %0 = load %struct.CallSite_t*, %struct.CallSite_t** %arrayidx, align 8, !dbg !1443, !tbaa !1444
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %0, i64 0, metadata !1431, metadata !1087), !dbg !1446
  %1 = load i32*, i32** addrspacecast (i32* addrspace(1)* @stackHeight to i32**), align 8, !dbg !1447, !tbaa !1444
  %arrayidx2 = getelementptr inbounds i32, i32* %1, i64 %idxprom, !dbg !1447
  tail call void @llvm.dbg.value(metadata i32* %arrayidx2, i64 0, metadata !1432, metadata !1087), !dbg !1448
  %2 = load i32, i32* %arrayidx2, align 4, !dbg !1449, !tbaa !1371
  switch i32 %2, label %if.end [
    i32 1, label %cond.false
    i32 0, label %if.then
  ], !dbg !1449

cond.false:                                       ; preds = %entry
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str, i64 0, i64 0), i32 121, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @__PRETTY_FUNCTION__._Z15updateCallStackiissi, i64 0, i64 0)) #3, !dbg !1450
  unreachable

if.then:                                          ; preds = %entry
  %3 = getelementptr inbounds %printf_args, %printf_args* %tmp, i64 0, i32 0, !dbg !1452
  store i32 %tid, i32* %3, align 8, !dbg !1452
  %4 = bitcast %printf_args* %tmp to i8*, !dbg !1452
  %5 = call i32 @vprintf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i64 0, i64 0), i8* nonnull %4) #10, !dbg !1452
  %id = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 0, !dbg !1455
  store i32 %caller, i32* %id, align 4, !dbg !1456, !tbaa !1457
  %sline6 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 1, !dbg !1460
  store i16 %sline, i16* %sline6, align 4, !dbg !1461, !tbaa !1462
  %scolm8 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 2, !dbg !1463
  store i16 %scolm, i16* %scolm8, align 2, !dbg !1464, !tbaa !1465
  %id10 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 1, i32 0, !dbg !1466
  store i32 %callee, i32* %id10, align 4, !dbg !1467, !tbaa !1457
  %sline12 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 1, i32 1, !dbg !1468
  store i16 -1, i16* %sline12, align 4, !dbg !1469, !tbaa !1462
  %scolm14 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 1, i32 2, !dbg !1470
  store i16 -1, i16* %scolm14, align 2, !dbg !1471, !tbaa !1465
  store i32 2, i32* %arrayidx2, align 4, !dbg !1472, !tbaa !1371
  br label %cleanup100, !dbg !1473

if.end:                                           ; preds = %entry
  %sub = add nsw i32 %2, -2, !dbg !1474
  %idxprom16 = sext i32 %sub to i64, !dbg !1475
  %id18 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom16, i32 0, !dbg !1476
  %6 = load i32, i32* %id18, align 4, !dbg !1476, !tbaa !1457
  tail call void @llvm.dbg.value(metadata i32 %6, i64 0, metadata !1434, metadata !1087), !dbg !1477
  %sub20 = add nsw i32 %2, -1, !dbg !1478
  %idxprom21 = sext i32 %sub20 to i64, !dbg !1479
  %id23 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom21, i32 0, !dbg !1480
  %7 = load i32, i32* %id23, align 4, !dbg !1480, !tbaa !1457
  tail call void @llvm.dbg.value(metadata i32 %7, i64 0, metadata !1435, metadata !1087), !dbg !1481
  %cmp24 = icmp eq i32 %6, %caller, !dbg !1482
  %cmp25 = icmp eq i32 %7, %callee, !dbg !1484
  %or.cond = and i1 %cmp24, %cmp25, !dbg !1486
  br i1 %or.cond, label %if.then26, label %if.else, !dbg !1486

if.then26:                                        ; preds = %if.end
  %sline30 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom16, i32 1, !dbg !1487
  store i16 %sline, i16* %sline30, align 4, !dbg !1489, !tbaa !1462
  %scolm34 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom16, i32 2, !dbg !1490
  store i16 %scolm, i16* %scolm34, align 2, !dbg !1491, !tbaa !1465
  br label %cleanup100, !dbg !1492

if.else:                                          ; preds = %if.end
  %cmp24.not = xor i1 %cmp24, true, !dbg !1493
  %or.cond178 = or i1 %cmp25, %cmp24.not, !dbg !1493
  br i1 %or.cond178, label %if.else51, label %if.then38, !dbg !1493

if.then38:                                        ; preds = %if.else
  %8 = tail call i32 @vprintf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.3, i64 0, i64 0), i8* null) #10, !dbg !1495
  %9 = load i32, i32* %arrayidx2, align 4, !dbg !1497, !tbaa !1371
  %sub39 = add nsw i32 %9, -1, !dbg !1498
  %idxprom40 = sext i32 %sub39 to i64, !dbg !1499
  %id42 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom40, i32 0, !dbg !1500
  store i32 %callee, i32* %id42, align 4, !dbg !1501, !tbaa !1457
  %10 = load i32, i32* %arrayidx2, align 4, !dbg !1502, !tbaa !1371
  %sub43 = add nsw i32 %10, -2, !dbg !1503
  %idxprom44 = sext i32 %sub43 to i64, !dbg !1504
  %sline46 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom44, i32 1, !dbg !1505
  store i16 %sline, i16* %sline46, align 4, !dbg !1506, !tbaa !1462
  %scolm50 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom44, i32 2, !dbg !1507
  store i16 %scolm, i16* %scolm50, align 2, !dbg !1508, !tbaa !1465
  br label %cleanup100, !dbg !1509

if.else51:                                        ; preds = %if.else
  %cmp52 = icmp eq i32 %7, %caller, !dbg !1510
  br i1 %cmp52, label %if.then53, label %for.cond.preheader, !dbg !1512

for.cond.preheader:                               ; preds = %if.else51
  br label %for.cond

if.then53:                                        ; preds = %if.else51
  %11 = tail call i32 @vprintf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.4, i64 0, i64 0), i8* null) #10, !dbg !1513
  %12 = load i32, i32* %arrayidx2, align 4, !dbg !1515, !tbaa !1371
  %sub54 = add nsw i32 %12, -1, !dbg !1516
  %idxprom55 = sext i32 %sub54 to i64, !dbg !1517
  %sline57 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom55, i32 1, !dbg !1518
  store i16 %sline, i16* %sline57, align 4, !dbg !1519, !tbaa !1462
  %scolm61 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom55, i32 2, !dbg !1520
  store i16 %scolm, i16* %scolm61, align 2, !dbg !1521, !tbaa !1465
  %idxprom62 = sext i32 %12 to i64, !dbg !1522
  %id64 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom62, i32 0, !dbg !1523
  store i32 %callee, i32* %id64, align 4, !dbg !1524, !tbaa !1457
  %13 = load i32, i32* %arrayidx2, align 4, !dbg !1525, !tbaa !1371
  %idxprom65 = sext i32 %13 to i64, !dbg !1526
  %sline67 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom65, i32 1, !dbg !1527
  store i16 -1, i16* %sline67, align 4, !dbg !1528, !tbaa !1462
  %scolm70 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom65, i32 2, !dbg !1529
  store i16 -1, i16* %scolm70, align 2, !dbg !1530, !tbaa !1465
  %inc = add nsw i32 %13, 1, !dbg !1531
  store i32 %inc, i32* %arrayidx2, align 4, !dbg !1531, !tbaa !1371
  br label %cleanup100, !dbg !1532

for.cond:                                         ; preds = %for.cond.preheader, %for.body
  %i.0.in = phi i32 [ %i.0, %for.body ], [ %2, %for.cond.preheader ]
  %i.0 = add nsw i32 %i.0.in, -1
  tail call void @llvm.dbg.value(metadata i32 %i.0, i64 0, metadata !1436, metadata !1087), !dbg !1533
  %cmp76 = icmp sgt i32 %i.0.in, 0, !dbg !1534
  br i1 %cmp76, label %for.body, label %for.end, !dbg !1537

for.body:                                         ; preds = %for.cond
  %idxprom77 = sext i32 %i.0 to i64, !dbg !1539
  %id79 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom77, i32 0, !dbg !1542
  %14 = load i32, i32* %id79, align 4, !dbg !1542, !tbaa !1457
  %cmp80 = icmp eq i32 %14, %caller, !dbg !1543
  br i1 %cmp80, label %if.then81, label %for.cond, !dbg !1544, !llvm.loop !1545

if.then81:                                        ; preds = %for.body
  store i32 %i.0.in, i32* %arrayidx2, align 4, !dbg !1548, !tbaa !1371
  store i32 %callee, i32* %id79, align 4, !dbg !1550, !tbaa !1457
  %sline87 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom77, i32 1, !dbg !1551
  %scolm90 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom77, i32 2, !dbg !1552
  store i16 %sline, i16* %sline87, align 4, !dbg !1553, !tbaa !1462
  store i16 %scolm, i16* %scolm90, align 2, !dbg !1554, !tbaa !1465
  br label %cleanup100

for.end:                                          ; preds = %for.cond
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.5, i64 0, i64 0), i32 183, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @__PRETTY_FUNCTION__._Z15updateCallStackiissi, i64 0, i64 0)) #3, !dbg !1555
  unreachable

cleanup100:                                       ; preds = %if.then26, %if.then38, %if.then53, %if.then81, %if.then
  ret void, !dbg !1556
}

; Function Attrs: convergent inlinehint noreturn nounwind
define internal fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* %__message, i32 %__line, i8* %__function) unnamed_addr #6 !dbg !1558 {
entry:
  tail call void @__assertfail(i8* %__message, i8* getelementptr inbounds ([59 x i8], [59 x i8]* @.str.1, i64 0, i64 0), i32 %__line, i8* %__function, i64 1) #11, !dbg !1567
  unreachable, !dbg !1567
}

declare i32 @vprintf(i8*, i8*) local_unnamed_addr

; Function Attrs: convergent noreturn nounwind
declare void @__assertfail(i8*, i8*, i32, i8*, i64) local_unnamed_addr #7

; Function Attrs: nounwind
define void @_Z14printCallStacki(i32 %tid) local_unnamed_addr #4 !dbg !1568 {
entry:
  %tmp = alloca %printf_args, align 8
  %tmp12 = alloca %printf_args.1, align 8
  tail call void @llvm.dbg.value(metadata i32 %tid, i64 0, metadata !1570, metadata !1087), !dbg !1575
  %idxprom = sext i32 %tid to i64, !dbg !1576
  %arrayidx27 = getelementptr inbounds [10 x %struct.CallSite_t*], [10 x %struct.CallSite_t*] addrspace(1)* @globalCallStack, i64 0, i64 %idxprom, !dbg !1576
  %arrayidx = addrspacecast %struct.CallSite_t* addrspace(1)* %arrayidx27 to %struct.CallSite_t**, !dbg !1576
  %0 = load %struct.CallSite_t*, %struct.CallSite_t** %arrayidx, align 8, !dbg !1576, !tbaa !1444
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %0, i64 0, metadata !1571, metadata !1087), !dbg !1577
  %1 = load i32*, i32** addrspacecast (i32* addrspace(1)* @stackHeight to i32**), align 8, !dbg !1578, !tbaa !1444
  %arrayidx2 = getelementptr inbounds i32, i32* %1, i64 %idxprom, !dbg !1578
  %2 = load i32, i32* %arrayidx2, align 4, !dbg !1578, !tbaa !1371
  tail call void @llvm.dbg.value(metadata i32 %2, i64 0, metadata !1572, metadata !1087), !dbg !1579
  %3 = getelementptr inbounds %printf_args, %printf_args* %tmp, i64 0, i32 0, !dbg !1580
  store i32 %2, i32* %3, align 8, !dbg !1580
  %4 = bitcast %printf_args* %tmp to i8*, !dbg !1580
  %5 = call i32 @vprintf(i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.6, i64 0, i64 0), i8* nonnull %4) #10, !dbg !1580
  %cmp = icmp sgt i32 %2, 0, !dbg !1581
  call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1573, metadata !1087), !dbg !1583
  br i1 %cmp, label %for.body.lr.ph, label %cleanup, !dbg !1584

for.body.lr.ph:                                   ; preds = %entry
  %6 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp12, i64 0, i32 0
  %7 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp12, i64 0, i32 1
  %8 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp12, i64 0, i32 2
  %9 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp12, i64 0, i32 3
  %10 = bitcast %printf_args.1* %tmp12 to i8*
  %xtraiter = and i32 %2, 1, !dbg !1585
  %lcmp.mod = icmp eq i32 %xtraiter, 0, !dbg !1585
  br i1 %lcmp.mod, label %for.body.prol.loopexit, label %for.body.prol.preheader, !dbg !1585

for.body.prol.preheader:                          ; preds = %for.body.lr.ph
  br label %for.body.prol, !dbg !1585

for.body.prol:                                    ; preds = %for.body.prol.preheader
  %id.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 0, !dbg !1587
  %11 = load i32, i32* %id.prol, align 4, !dbg !1587, !tbaa !1457
  %sline.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 1, !dbg !1589
  %12 = load i16, i16* %sline.prol, align 4, !dbg !1589, !tbaa !1462
  %conv.prol = sext i16 %12 to i32, !dbg !1590
  %scolm.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 0, i32 2, !dbg !1591
  %13 = load i16, i16* %scolm.prol, align 2, !dbg !1591, !tbaa !1465
  %conv11.prol = sext i16 %13 to i32, !dbg !1592
  store i32 0, i32* %6, align 8, !dbg !1593
  store i32 %11, i32* %7, align 4, !dbg !1593
  store i32 %conv.prol, i32* %8, align 8, !dbg !1593
  store i32 %conv11.prol, i32* %9, align 4, !dbg !1593
  %14 = call i32 @vprintf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %10) #10, !dbg !1593
  call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !1573, metadata !1087), !dbg !1583
  call void @llvm.dbg.value(metadata i32 1, i64 0, metadata !1573, metadata !1087), !dbg !1583
  br label %for.body.prol.loopexit, !dbg !1585

for.body.prol.loopexit:                           ; preds = %for.body.lr.ph, %for.body.prol
  %i.029.unr = phi i32 [ 0, %for.body.lr.ph ], [ 1, %for.body.prol ]
  %15 = icmp eq i32 %2, 1, !dbg !1585
  br i1 %15, label %cleanup.loopexit, label %for.body.lr.ph.new, !dbg !1585

for.body.lr.ph.new:                               ; preds = %for.body.prol.loopexit
  br label %for.body, !dbg !1585

for.body:                                         ; preds = %for.body, %for.body.lr.ph.new
  %i.029 = phi i32 [ %i.029.unr, %for.body.lr.ph.new ], [ %inc.1, %for.body ]
  %idxprom5 = sext i32 %i.029 to i64, !dbg !1594
  %id = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5, i32 0, !dbg !1587
  %16 = load i32, i32* %id, align 4, !dbg !1587, !tbaa !1457
  %sline = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5, i32 1, !dbg !1589
  %17 = load i16, i16* %sline, align 4, !dbg !1589, !tbaa !1462
  %conv = sext i16 %17 to i32, !dbg !1590
  %scolm = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5, i32 2, !dbg !1591
  %18 = load i16, i16* %scolm, align 2, !dbg !1591, !tbaa !1465
  %conv11 = sext i16 %18 to i32, !dbg !1592
  store i32 %i.029, i32* %6, align 8, !dbg !1593
  store i32 %16, i32* %7, align 4, !dbg !1593
  store i32 %conv, i32* %8, align 8, !dbg !1593
  store i32 %conv11, i32* %9, align 4, !dbg !1593
  %19 = call i32 @vprintf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %10) #10, !dbg !1593
  %inc = add nuw nsw i32 %i.029, 1, !dbg !1595
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1573, metadata !1087), !dbg !1583
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1573, metadata !1087), !dbg !1583
  %idxprom5.1 = sext i32 %inc to i64, !dbg !1594
  %id.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5.1, i32 0, !dbg !1587
  %20 = load i32, i32* %id.1, align 4, !dbg !1587, !tbaa !1457
  %sline.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5.1, i32 1, !dbg !1589
  %21 = load i16, i16* %sline.1, align 4, !dbg !1589, !tbaa !1462
  %conv.1 = sext i16 %21 to i32, !dbg !1590
  %scolm.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %0, i64 %idxprom5.1, i32 2, !dbg !1591
  %22 = load i16, i16* %scolm.1, align 2, !dbg !1591, !tbaa !1465
  %conv11.1 = sext i16 %22 to i32, !dbg !1592
  store i32 %inc, i32* %6, align 8, !dbg !1593
  store i32 %20, i32* %7, align 4, !dbg !1593
  store i32 %conv.1, i32* %8, align 8, !dbg !1593
  store i32 %conv11.1, i32* %9, align 4, !dbg !1593
  %23 = call i32 @vprintf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.7, i64 0, i64 0), i8* nonnull %10) #10, !dbg !1593
  %inc.1 = add nsw i32 %i.029, 2, !dbg !1595
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1573, metadata !1087), !dbg !1583
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1573, metadata !1087), !dbg !1583
  %exitcond.1 = icmp eq i32 %inc.1, %2, !dbg !1597
  br i1 %exitcond.1, label %cleanup.loopexit.unr-lcssa, label %for.body, !dbg !1585, !llvm.loop !1599

cleanup.loopexit.unr-lcssa:                       ; preds = %for.body
  br label %cleanup.loopexit, !dbg !1602

cleanup.loopexit:                                 ; preds = %for.body.prol.loopexit, %cleanup.loopexit.unr-lcssa
  br label %cleanup, !dbg !1602

cleanup:                                          ; preds = %cleanup.loopexit, %entry
  ret void, !dbg !1602
}

; Function Attrs: convergent nounwind
define i32 @_Z6holdonl(i64 %c) local_unnamed_addr #0 !dbg !1604 {
entry:
  tail call void @llvm.dbg.value(metadata i64 %c, i64 0, metadata !1611, metadata !1087), !dbg !1614
  %0 = tail call i32 asm sideeffect "mov.u32 \09$0, %clock;", "=r"() #3, !dbg !1615, !srcloc !1620
  tail call void @llvm.dbg.value(metadata i32 %0, i64 0, metadata !1618, metadata !1087) #10, !dbg !1621
  %conv = sext i32 %0 to i64, !dbg !1622
  tail call void @llvm.dbg.value(metadata i64 %conv, i64 0, metadata !1612, metadata !1087), !dbg !1623
  tail call void @llvm.dbg.value(metadata i64 0, i64 0, metadata !1613, metadata !1087), !dbg !1624
  tail call void @llvm.dbg.value(metadata i64 0, i64 0, metadata !1613, metadata !1087), !dbg !1624
  %cmp7 = icmp sgt i64 %c, 0, !dbg !1625
  br i1 %cmp7, label %while.body.preheader, label %while.end, !dbg !1627

while.body.preheader:                             ; preds = %entry
  br label %while.body, !dbg !1628

while.body:                                       ; preds = %while.body.preheader, %while.body
  %1 = tail call i32 asm sideeffect "mov.u32 \09$0, %clock;", "=r"() #3, !dbg !1628, !srcloc !1620
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !1618, metadata !1087) #10, !dbg !1630
  %conv2 = sext i32 %1 to i64, !dbg !1631
  %sub = sub nsw i64 %conv2, %conv, !dbg !1632
  tail call void @llvm.dbg.value(metadata i64 %sub, i64 0, metadata !1613, metadata !1087), !dbg !1624
  tail call void @llvm.dbg.value(metadata i64 %sub, i64 0, metadata !1613, metadata !1087), !dbg !1624
  %cmp = icmp slt i64 %sub, %c, !dbg !1625
  br i1 %cmp, label %while.body, label %while.end.loopexit, !dbg !1627, !llvm.loop !1633

while.end.loopexit:                               ; preds = %while.body
  %phitmp = trunc i64 %sub to i32, !dbg !1636
  br label %while.end, !dbg !1636

while.end:                                        ; preds = %while.end.loopexit, %entry
  %clock_offset.0.lcssa = phi i32 [ 0, %entry ], [ %phitmp, %while.end.loopexit ]
  ret i32 %clock_offset.0.lcssa, !dbg !1637
}

; Function Attrs: norecurse nounwind readnone
define void @InitKernel() local_unnamed_addr #8 !dbg !1638 {
entry:
  ret void, !dbg !1639
}

; Function Attrs: nounwind readnone
define void @callFunc(i8* nocapture %er, i8* nocapture %ee, i32 %sline, i32 %scolm) local_unnamed_addr #9 !dbg !1640 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %er, i64 0, metadata !1644, metadata !1087), !dbg !1648
  tail call void @llvm.dbg.value(metadata i8* %ee, i64 0, metadata !1645, metadata !1087), !dbg !1649
  tail call void @llvm.dbg.value(metadata i32 %sline, i64 0, metadata !1646, metadata !1087), !dbg !1650
  tail call void @llvm.dbg.value(metadata i32 %scolm, i64 0, metadata !1647, metadata !1087), !dbg !1651
  ret void, !dbg !1652
}

; Function Attrs: nounwind
define void @takeString(i8* %p, i32 %action) local_unnamed_addr #4 !dbg !1653 {
entry:
  %tmp = alloca %printf_args.2, align 8
  %tmp14 = alloca %printf_args.2, align 8
  %tmp18 = alloca %printf_args.2, align 8
  %tmp20 = alloca %printf_args.2, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1657, metadata !1087), !dbg !1659
  tail call void @llvm.dbg.value(metadata i32 %action, i64 0, metadata !1658, metadata !1087), !dbg !1660
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !1661, !range !1161
  %cmp = icmp eq i32 %0, 0, !dbg !1665
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !1666

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !1667, !range !1131
  %cmp2 = icmp eq i32 %1, 0, !dbg !1671
  br i1 %cmp2, label %lor.lhs.false3, label %return, !dbg !1672

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !1673, !range !1161
  %cmp5 = icmp eq i32 %2, 0, !dbg !1677
  br i1 %cmp5, label %lor.lhs.false6, label %return, !dbg !1678

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !1679, !range !1131
  %cmp8 = icmp eq i32 %3, 0, !dbg !1683
  br i1 %cmp8, label %if.end, label %return, !dbg !1684

if.end:                                           ; preds = %lor.lhs.false6
  %4 = load i8, i8* addrspacecast (i8 addrspace(1)* @VERBOSE to i8*), align 1, !dbg !1686, !tbaa !1688, !range !1690
  %tobool = icmp eq i8 %4, 0, !dbg !1686
  br i1 %tobool, label %return, label %if.then9, !dbg !1691

if.then9:                                         ; preds = %if.end
  switch i32 %action, label %if.else19 [
    i32 1, label %if.then11
    i32 2, label %if.then13
    i32 3, label %if.then17
  ], !dbg !1692

if.then11:                                        ; preds = %if.then9
  %5 = getelementptr inbounds %printf_args.2, %printf_args.2* %tmp, i64 0, i32 0, !dbg !1694
  store i8* %p, i8** %5, align 8, !dbg !1694
  %6 = bitcast %printf_args.2* %tmp to i8*, !dbg !1694
  %7 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.8, i64 0, i64 0), i8* nonnull %6) #10, !dbg !1694
  br label %return, !dbg !1694

if.then13:                                        ; preds = %if.then9
  %8 = getelementptr inbounds %printf_args.2, %printf_args.2* %tmp14, i64 0, i32 0, !dbg !1696
  store i8* %p, i8** %8, align 8, !dbg !1696
  %9 = bitcast %printf_args.2* %tmp14 to i8*, !dbg !1696
  %10 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.9, i64 0, i64 0), i8* nonnull %9) #10, !dbg !1696
  br label %return, !dbg !1696

if.then17:                                        ; preds = %if.then9
  %11 = getelementptr inbounds %printf_args.2, %printf_args.2* %tmp18, i64 0, i32 0, !dbg !1698
  store i8* %p, i8** %11, align 8, !dbg !1698
  %12 = bitcast %printf_args.2* %tmp18 to i8*, !dbg !1698
  %13 = call i32 @vprintf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.10, i64 0, i64 0), i8* nonnull %12) #10, !dbg !1698
  br label %return, !dbg !1698

if.else19:                                        ; preds = %if.then9
  %14 = getelementptr inbounds %printf_args.2, %printf_args.2* %tmp20, i64 0, i32 0, !dbg !1700
  store i8* %p, i8** %14, align 8, !dbg !1700
  %15 = bitcast %printf_args.2* %tmp20 to i8*, !dbg !1700
  %16 = call i32 @vprintf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.11, i64 0, i64 0), i8* nonnull %15) #10, !dbg !1700
  br label %return

return:                                           ; preds = %if.end, %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry, %if.then13, %if.else19, %if.then17, %if.then11
  ret void, !dbg !1701
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.y() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #1

; Function Attrs: convergent nounwind
define void @_Z8cxtprinti(i32 %id) local_unnamed_addr #0 !dbg !1702 {
entry:
  %tmp = alloca %printf_args.6, align 8
  %tmp22 = alloca %printf_args.7, align 8
  tail call void @llvm.dbg.value(metadata i32 %id, i64 0, metadata !1704, metadata !1087), !dbg !1707
  %0 = load i32, i32* addrspacecast (i32 addrspace(1)* @cHeight to i32*), align 4, !dbg !1708, !tbaa !1371
  %cmp = icmp sgt i32 %0, %id, !dbg !1708
  br i1 %cmp, label %cond.end, label %cond.false, !dbg !1708

cond.false:                                       ; preds = %entry
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.12, i64 0, i64 0), i32 338, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @__PRETTY_FUNCTION__._Z8cxtprinti, i64 0, i64 0)) #3, !dbg !1709
  unreachable

cond.end:                                         ; preds = %entry
  %cmp1 = icmp slt i32 %id, 0, !dbg !1711
  br i1 %cmp1, label %return, label %if.end, !dbg !1713

if.end:                                           ; preds = %cond.end
  %1 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 0, !dbg !1714
  store i32 %id, i32* %1, align 8, !dbg !1714
  %2 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 1, !dbg !1714
  store i32 %0, i32* %2, align 4, !dbg !1714
  %3 = bitcast %printf_args.6* %tmp to i8*, !dbg !1714
  %4 = call i32 @vprintf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.13, i64 0, i64 0), i8* nonnull %3) #10, !dbg !1714
  call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1705, metadata !1087), !dbg !1715
  %idxprom = sext i32 %id to i64, !dbg !1716
  call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1705, metadata !1087), !dbg !1715
  %5 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 0
  %6 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 1
  %7 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 2
  %8 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 3
  %9 = getelementptr inbounds %printf_args.7, %printf_args.7* %tmp22, i64 0, i32 4
  %10 = bitcast %printf_args.7* %tmp22 to i8*
  br label %land.rhs, !dbg !1719

land.rhs:                                         ; preds = %if.end, %for.body
  %i.039 = phi i32 [ 0, %if.end ], [ %inc, %for.body ]
  %idxprom4 = sext i32 %i.039 to i64, !dbg !1716
  %arrayidx538 = getelementptr inbounds [20 x [5 x %struct.CallSite_t]], [20 x [5 x %struct.CallSite_t]] addrspace(1)* @contextDic, i64 0, i64 %idxprom, i64 %idxprom4, !dbg !1716
  %arrayidx5 = addrspacecast %struct.CallSite_t addrspace(1)* %arrayidx538 to %struct.CallSite_t*, !dbg !1716
  %id6 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %arrayidx5, i64 0, i32 0, !dbg !1721
  %11 = load i32, i32* %id6, align 4, !dbg !1721, !tbaa !1457
  %cmp7 = icmp eq i32 %11, -1, !dbg !1722
  br i1 %cmp7, label %return.loopexit, label %for.body, !dbg !1723

for.body:                                         ; preds = %land.rhs
  %sline = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %arrayidx5, i64 0, i32 1, !dbg !1725
  %12 = load i16, i16* %sline, align 4, !dbg !1725, !tbaa !1462
  %conv = sext i16 %12 to i32, !dbg !1727
  %scolm = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %arrayidx5, i64 0, i32 2, !dbg !1728
  %13 = load i16, i16* %scolm, align 2, !dbg !1728, !tbaa !1465
  %conv21 = sext i16 %13 to i32, !dbg !1729
  store i32 %id, i32* %5, align 8, !dbg !1730
  store i32 %i.039, i32* %6, align 4, !dbg !1730
  store i32 %11, i32* %7, align 8, !dbg !1730
  store i32 %conv, i32* %8, align 4, !dbg !1730
  store i32 %conv21, i32* %9, align 8, !dbg !1730
  %14 = call i32 @vprintf(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.14, i64 0, i64 0), i8* nonnull %10) #10, !dbg !1730
  %inc = add nuw nsw i32 %i.039, 1, !dbg !1731
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1705, metadata !1087), !dbg !1715
  call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1705, metadata !1087), !dbg !1715
  %cmp3 = icmp slt i32 %inc, 15, !dbg !1733
  br i1 %cmp3, label %land.rhs, label %return.loopexit, !dbg !1719, !llvm.loop !1734

return.loopexit:                                  ; preds = %land.rhs, %for.body
  br label %return, !dbg !1737

return:                                           ; preds = %return.loopexit, %cond.end
  ret void, !dbg !1737
}

; Function Attrs: convergent nounwind
define void @_Z6cxtcpyP10CallSite_tS0_i(%struct.CallSite_t* nocapture %dst, %struct.CallSite_t* nocapture readonly %src, i32 %height) local_unnamed_addr #0 !dbg !1738 {
entry:
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %dst, i64 0, metadata !1742, metadata !1087), !dbg !1746
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %src, i64 0, metadata !1743, metadata !1087), !dbg !1747
  tail call void @llvm.dbg.value(metadata i32 %height, i64 0, metadata !1744, metadata !1087), !dbg !1748
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1745, metadata !1087), !dbg !1749
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1745, metadata !1087), !dbg !1749
  %cmp13 = icmp sgt i32 %height, 0, !dbg !1750
  br i1 %cmp13, label %for.body.preheader, label %cond.end, !dbg !1754

for.body.preheader:                               ; preds = %entry
  %0 = add i32 %height, -1, !dbg !1756
  %xtraiter = and i32 %height, 3, !dbg !1756
  %lcmp.mod = icmp eq i32 %xtraiter, 0, !dbg !1756
  br i1 %lcmp.mod, label %for.body.prol.loopexit, label %for.body.prol.preheader, !dbg !1756

for.body.prol.preheader:                          ; preds = %for.body.preheader
  br label %for.body.prol, !dbg !1756

for.body.prol:                                    ; preds = %for.body.prol, %for.body.prol.preheader
  %i.014.prol = phi i32 [ %inc.prol, %for.body.prol ], [ 0, %for.body.prol.preheader ]
  %prol.iter = phi i32 [ %prol.iter.sub, %for.body.prol ], [ %xtraiter, %for.body.prol.preheader ]
  %idxprom.prol = sext i32 %i.014.prol to i64, !dbg !1756
  %arrayidx.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom.prol, !dbg !1756
  %arrayidx2.prol = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom.prol, !dbg !1757
  %1 = bitcast %struct.CallSite_t* %arrayidx.prol to i64*, !dbg !1758
  %2 = bitcast %struct.CallSite_t* %arrayidx2.prol to i64*, !dbg !1758
  %3 = load i64, i64* %1, align 4, !dbg !1758
  store i64 %3, i64* %2, align 4, !dbg !1758
  %inc.prol = add nuw nsw i32 %i.014.prol, 1, !dbg !1759
  tail call void @llvm.dbg.value(metadata i32 %inc.prol, i64 0, metadata !1745, metadata !1087), !dbg !1749
  tail call void @llvm.dbg.value(metadata i32 %inc.prol, i64 0, metadata !1745, metadata !1087), !dbg !1749
  %prol.iter.sub = add i32 %prol.iter, -1, !dbg !1754
  %prol.iter.cmp = icmp eq i32 %prol.iter.sub, 0, !dbg !1754
  br i1 %prol.iter.cmp, label %for.body.prol.loopexit.unr-lcssa, label %for.body.prol, !dbg !1754, !llvm.loop !1761

for.body.prol.loopexit.unr-lcssa:                 ; preds = %for.body.prol
  br label %for.body.prol.loopexit, !dbg !1756

for.body.prol.loopexit:                           ; preds = %for.body.preheader, %for.body.prol.loopexit.unr-lcssa
  %i.014.unr = phi i32 [ 0, %for.body.preheader ], [ %inc.prol, %for.body.prol.loopexit.unr-lcssa ]
  %4 = icmp ult i32 %0, 3, !dbg !1756
  br i1 %4, label %for.end, label %for.body.preheader.new, !dbg !1756

for.body.preheader.new:                           ; preds = %for.body.prol.loopexit
  br label %for.body, !dbg !1756

for.body:                                         ; preds = %for.body, %for.body.preheader.new
  %i.014 = phi i32 [ %i.014.unr, %for.body.preheader.new ], [ %inc.3, %for.body ]
  %idxprom = sext i32 %i.014 to i64, !dbg !1756
  %arrayidx = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom, !dbg !1756
  %arrayidx2 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom, !dbg !1757
  %5 = bitcast %struct.CallSite_t* %arrayidx to i64*, !dbg !1758
  %6 = bitcast %struct.CallSite_t* %arrayidx2 to i64*, !dbg !1758
  %7 = load i64, i64* %5, align 4, !dbg !1758
  store i64 %7, i64* %6, align 4, !dbg !1758
  %inc = add nuw nsw i32 %i.014, 1, !dbg !1759
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1745, metadata !1087), !dbg !1749
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1745, metadata !1087), !dbg !1749
  %idxprom.1 = sext i32 %inc to i64, !dbg !1756
  %arrayidx.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom.1, !dbg !1756
  %arrayidx2.1 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom.1, !dbg !1757
  %8 = bitcast %struct.CallSite_t* %arrayidx.1 to i64*, !dbg !1758
  %9 = bitcast %struct.CallSite_t* %arrayidx2.1 to i64*, !dbg !1758
  %10 = load i64, i64* %8, align 4, !dbg !1758
  store i64 %10, i64* %9, align 4, !dbg !1758
  %inc.1 = add nsw i32 %i.014, 2, !dbg !1759
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1745, metadata !1087), !dbg !1749
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1745, metadata !1087), !dbg !1749
  %idxprom.2 = sext i32 %inc.1 to i64, !dbg !1756
  %arrayidx.2 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom.2, !dbg !1756
  %arrayidx2.2 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom.2, !dbg !1757
  %11 = bitcast %struct.CallSite_t* %arrayidx.2 to i64*, !dbg !1758
  %12 = bitcast %struct.CallSite_t* %arrayidx2.2 to i64*, !dbg !1758
  %13 = load i64, i64* %11, align 4, !dbg !1758
  store i64 %13, i64* %12, align 4, !dbg !1758
  %inc.2 = add nsw i32 %i.014, 3, !dbg !1759
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1745, metadata !1087), !dbg !1749
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1745, metadata !1087), !dbg !1749
  %idxprom.3 = sext i32 %inc.2 to i64, !dbg !1756
  %arrayidx.3 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom.3, !dbg !1756
  %arrayidx2.3 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom.3, !dbg !1757
  %14 = bitcast %struct.CallSite_t* %arrayidx.3 to i64*, !dbg !1758
  %15 = bitcast %struct.CallSite_t* %arrayidx2.3 to i64*, !dbg !1758
  %16 = load i64, i64* %14, align 4, !dbg !1758
  store i64 %16, i64* %15, align 4, !dbg !1758
  %inc.3 = add nsw i32 %i.014, 4, !dbg !1759
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1745, metadata !1087), !dbg !1749
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1745, metadata !1087), !dbg !1749
  %exitcond.3 = icmp eq i32 %inc.3, %height, !dbg !1750
  br i1 %exitcond.3, label %for.end.unr-lcssa, label %for.body, !dbg !1754, !llvm.loop !1763

for.end.unr-lcssa:                                ; preds = %for.body
  br label %for.end, !dbg !1766

for.end:                                          ; preds = %for.body.prol.loopexit, %for.end.unr-lcssa
  %cmp3 = icmp slt i32 %height, 15, !dbg !1766
  br i1 %cmp3, label %cond.end, label %cond.false, !dbg !1766

cond.false:                                       ; preds = %for.end
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.15, i64 0, i64 0), i32 359, i8* getelementptr inbounds ([45 x i8], [45 x i8]* @__PRETTY_FUNCTION__._Z6cxtcpyP10CallSite_tS0_i, i64 0, i64 0)) #3, !dbg !1767
  unreachable

cond.end:                                         ; preds = %entry, %for.end
  %i.0.lcssa16 = phi i32 [ %height, %for.end ], [ 0, %entry ]
  %idxprom4 = sext i32 %i.0.lcssa16 to i64, !dbg !1769
  %id = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom4, i32 0, !dbg !1770
  store i32 -1, i32* %id, align 4, !dbg !1771, !tbaa !1457
  ret void, !dbg !1772
}

; Function Attrs: nounwind readonly
define zeroext i1 @_Z6cxtcmpP10CallSite_tS0_i(%struct.CallSite_t* nocapture readonly %dst, %struct.CallSite_t* nocapture readonly %src, i32 %height) local_unnamed_addr #5 !dbg !1773 {
entry:
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %dst, i64 0, metadata !1777, metadata !1087), !dbg !1782
  tail call void @llvm.dbg.value(metadata %struct.CallSite_t* %src, i64 0, metadata !1778, metadata !1087), !dbg !1783
  tail call void @llvm.dbg.value(metadata i32 %height, i64 0, metadata !1779, metadata !1087), !dbg !1784
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1780, metadata !1087), !dbg !1785
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1780, metadata !1087), !dbg !1785
  %cmp9 = icmp sgt i32 %height, 0, !dbg !1786
  br i1 %cmp9, label %for.body.preheader, label %cleanup, !dbg !1789

for.body.preheader:                               ; preds = %entry
  br label %for.body, !dbg !1791

for.cond:                                         ; preds = %for.body
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1780, metadata !1087), !dbg !1785
  %cmp = icmp slt i32 %inc, %height, !dbg !1786
  br i1 %cmp, label %for.body, label %cleanup.loopexit, !dbg !1789, !llvm.loop !1793

for.body:                                         ; preds = %for.body.preheader, %for.cond
  %i.010 = phi i32 [ %inc, %for.cond ], [ 0, %for.body.preheader ]
  %idxprom = sext i32 %i.010 to i64, !dbg !1791
  %id = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %dst, i64 %idxprom, i32 0, !dbg !1796
  %0 = load i32, i32* %id, align 4, !dbg !1796, !tbaa !1457
  %id3 = getelementptr inbounds %struct.CallSite_t, %struct.CallSite_t* %src, i64 %idxprom, i32 0, !dbg !1797
  %1 = load i32, i32* %id3, align 4, !dbg !1797, !tbaa !1457
  %cmp4 = icmp eq i32 %0, %1, !dbg !1798
  %inc = add nuw nsw i32 %i.010, 1, !dbg !1799
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1780, metadata !1087), !dbg !1785
  br i1 %cmp4, label %for.cond, label %cleanup.loopexit, !dbg !1801

cleanup.loopexit:                                 ; preds = %for.cond, %for.body
  %.ph = phi i1 [ true, %for.cond ], [ false, %for.body ]
  br label %cleanup, !dbg !1802

cleanup:                                          ; preds = %cleanup.loopexit, %entry
  %2 = phi i1 [ true, %entry ], [ %.ph, %cleanup.loopexit ]
  ret i1 %2, !dbg !1802
}

; Function Attrs: norecurse nounwind readnone
define i32 @getContextID() local_unnamed_addr #8 !dbg !1803 {
entry:
  ret i32 -1, !dbg !1804
}

; Function Attrs: convergent nounwind
define void @passBasicBlock(i8* nocapture readonly %p, i32 %action, i32 %sline, i32 %scolm) local_unnamed_addr #0 !dbg !1805 {
entry:
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1809, metadata !1087), !dbg !1824
  tail call void @llvm.dbg.value(metadata i32 %action, i64 0, metadata !1810, metadata !1087), !dbg !1825
  tail call void @llvm.dbg.value(metadata i32 %sline, i64 0, metadata !1811, metadata !1087), !dbg !1826
  tail call void @llvm.dbg.value(metadata i32 %scolm, i64 0, metadata !1812, metadata !1087), !dbg !1827
  %0 = load i64, i64* addrspacecast (i64 addrspace(1)* @bbccnntt to i64*), align 8, !dbg !1828, !tbaa !1829
  %cmp = icmp ult i64 %0, 67108736, !dbg !1828
  br i1 %cmp, label %cond.end, label %cond.false, !dbg !1828

cond.false:                                       ; preds = %entry
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.16, i64 0, i64 0), i32 437, i8* getelementptr inbounds ([43 x i8], [43 x i8]* @__PRETTY_FUNCTION__.passBasicBlock, i64 0, i64 0)) #3, !dbg !1831
  unreachable

cond.end:                                         ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !1833, !range !1131
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !1836, !range !1131
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #10, !dbg !1839, !range !1884
  %mul = mul nuw i32 %3, %2, !dbg !1885
  %add = add i32 %mul, %1, !dbg !1886
  %4 = load i32, i32* addrspacecast (i32 addrspace(1)* @CTALB to i32*), align 4, !dbg !1887, !tbaa !1371
  %cmp3 = icmp ult i32 %add, %4, !dbg !1888
  br i1 %cmp3, label %return, label %lor.lhs.false, !dbg !1889

lor.lhs.false:                                    ; preds = %cond.end
  %5 = load i32, i32* addrspacecast (i32 addrspace(1)* @CTAUB to i32*), align 4, !dbg !1890, !tbaa !1371
  %cmp9 = icmp ugt i32 %add, %5, !dbg !1892
  br i1 %cmp9, label %return, label %if.end, !dbg !1893

if.end:                                           ; preds = %lor.lhs.false
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1813, metadata !1087), !dbg !1895
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1896, metadata !1087), !dbg !1904
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1906, metadata !1087), !dbg !1910
  %6 = atomicrmw add i64* addrspacecast (i64 addrspace(1)* @bbccnntt to i64*), i64 1 seq_cst, !dbg !1912
  tail call void @llvm.dbg.value(metadata i64 0, i64 0, metadata !1815, metadata !1087), !dbg !1913
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1816, metadata !1087), !dbg !1914
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1817, metadata !1087), !dbg !1915
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1818, metadata !1087), !dbg !1916
  tail call void @llvm.dbg.value(metadata i64 0, i64 0, metadata !1815, metadata !1087), !dbg !1913
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1817, metadata !1087), !dbg !1915
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !1818, metadata !1087), !dbg !1916
  %7 = load i8, i8* %p, align 1, !dbg !1917, !tbaa !1316
  %cmp1288 = icmp eq i8 %7, 0, !dbg !1919
  br i1 %cmp1288, label %for.cond.cleanup, label %for.body.preheader, !dbg !1920

for.body.preheader:                               ; preds = %if.end
  br label %for.body, !dbg !1922

for.cond.cleanup.loopexit:                        ; preds = %cleanup
  br label %for.cond.cleanup, !dbg !1924

for.cond.cleanup:                                 ; preds = %for.cond.cleanup.loopexit, %if.end
  %key.0.lcssa = phi i64 [ 0, %if.end ], [ %key.1, %for.cond.cleanup.loopexit ]
  tail call void @llvm.dbg.value(metadata %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 0, metadata !1823, metadata !1087), !dbg !1924
  %sext = shl i64 %6, 32, !dbg !1925
  %idxprom = ashr exact i64 %sext, 32, !dbg !1925
  %key27 = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 4, !dbg !1926
  store i64 %key.0.lcssa, i64* %key27, align 8, !dbg !1927, !tbaa !1928
  %8 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !1930, !range !1161
  %conv29 = trunc i32 %8 to i16, !dbg !1932
  %tidx = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 2, !dbg !1933
  store i16 %conv29, i16* %tidx, align 4, !dbg !1934, !tbaa !1935
  %9 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !1936, !range !1161
  %conv33 = trunc i32 %9 to i16, !dbg !1938
  %tidy = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 3, !dbg !1939
  store i16 %conv33, i16* %tidy, align 2, !dbg !1940, !tbaa !1941
  %conv37 = trunc i32 %1 to i16, !dbg !1942
  %bidx = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 0, !dbg !1943
  store i16 %conv37, i16* %bidx, align 8, !dbg !1944, !tbaa !1945
  %conv41 = trunc i32 %2 to i16, !dbg !1946
  %bidy = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 1, !dbg !1947
  store i16 %conv41, i16* %bidy, align 2, !dbg !1948, !tbaa !1949
  %sline46 = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 5, !dbg !1950
  store i32 %sline, i32* %sline46, align 8, !dbg !1951, !tbaa !1952
  %scolm49 = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 6, !dbg !1953
  store i32 %scolm, i32* %scolm49, align 4, !dbg !1954, !tbaa !1955
  %cid = getelementptr inbounds %struct.BBlog_t, %struct.BBlog_t* addrspacecast (%struct.BBlog_t addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to %struct.BBlog_t addrspace(1)*) to %struct.BBlog_t*), i64 %idxprom, i32 7, !dbg !1956
  store i32 -1, i32* %cid, align 8, !dbg !1957, !tbaa !1958
  br label %return

for.body:                                         ; preds = %for.body.preheader, %cleanup
  %10 = phi i8 [ %13, %cleanup ], [ %7, %for.body.preheader ]
  %key.091 = phi i64 [ %key.1, %cleanup ], [ 0, %for.body.preheader ]
  %factor.090 = phi i64 [ %factor.1, %cleanup ], [ 1, %for.body.preheader ]
  %i.089 = phi i32 [ %inc, %cleanup ], [ 0, %for.body.preheader ]
  %.off = add i8 %10, -48, !dbg !1922
  %11 = icmp ugt i8 %.off, 75, !dbg !1922
  tail call void @llvm.dbg.value(metadata i64 %add23, i64 0, metadata !1815, metadata !1087), !dbg !1913
  tail call void @llvm.dbg.value(metadata i64 %mul25, i64 0, metadata !1817, metadata !1087), !dbg !1915
  br i1 %11, label %cleanup, label %if.end20, !dbg !1922

if.end20:                                         ; preds = %for.body
  %conv21 = sext i8 %10 to i64, !dbg !1959
  %mul22 = mul nsw i64 %factor.090, %conv21, !dbg !1960
  %add23 = add i64 %mul22, %key.091, !dbg !1961
  %12 = load i32, i32* addrspacecast (i32 addrspace(1)* @CONSTANCE to i32*), align 4, !dbg !1962, !tbaa !1371
  %conv24 = sext i32 %12 to i64, !dbg !1962
  %mul25 = mul nsw i64 %factor.090, %conv24, !dbg !1963
  br label %cleanup, !dbg !1964

cleanup:                                          ; preds = %for.body, %if.end20
  %factor.1 = phi i64 [ %mul25, %if.end20 ], [ %factor.090, %for.body ]
  %key.1 = phi i64 [ %add23, %if.end20 ], [ %key.091, %for.body ]
  tail call void @llvm.dbg.value(metadata i64 %key.1, i64 0, metadata !1815, metadata !1087), !dbg !1913
  tail call void @llvm.dbg.value(metadata i64 %factor.1, i64 0, metadata !1817, metadata !1087), !dbg !1915
  %inc = add nuw nsw i32 %i.089, 1, !dbg !1965
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1818, metadata !1087), !dbg !1916
  tail call void @llvm.dbg.value(metadata i64 %key.1, i64 0, metadata !1815, metadata !1087), !dbg !1913
  tail call void @llvm.dbg.value(metadata i64 %factor.1, i64 0, metadata !1817, metadata !1087), !dbg !1915
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !1818, metadata !1087), !dbg !1916
  %idx.ext = sext i32 %inc to i64, !dbg !1967
  %add.ptr = getelementptr inbounds i8, i8* %p, i64 %idx.ext, !dbg !1967
  %13 = load i8, i8* %add.ptr, align 1, !dbg !1917, !tbaa !1316
  %cmp12 = icmp eq i8 %13, 0, !dbg !1919
  br i1 %cmp12, label %for.cond.cleanup.loopexit, label %for.body, !dbg !1920, !llvm.loop !1968

return:                                           ; preds = %cond.end, %lor.lhs.false, %for.cond.cleanup
  ret void, !dbg !1971
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #1

; Function Attrs: convergent nounwind
define void @_Z10storeLinesPvssss(i8* %p, i16 signext %size, i16 signext %line, i16 signext %colmn, i16 signext %op) local_unnamed_addr #0 !dbg !1972 {
entry:
  %tmp = alloca %printf_args.8, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1976, metadata !1087), !dbg !1984
  tail call void @llvm.dbg.value(metadata i16 %size, i64 0, metadata !1977, metadata !1087), !dbg !1985
  tail call void @llvm.dbg.value(metadata i16 %line, i64 0, metadata !1978, metadata !1087), !dbg !1986
  tail call void @llvm.dbg.value(metadata i16 %colmn, i64 0, metadata !1979, metadata !1087), !dbg !1987
  tail call void @llvm.dbg.value(metadata i16 %op, i64 0, metadata !1980, metadata !1087), !dbg !1988
  %0 = load i64, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !1989, !tbaa !1829
  %cmp = icmp ult i64 %0, 67108736, !dbg !1989
  br i1 %cmp, label %cond.end, label %cond.false, !dbg !1989

cond.false:                                       ; preds = %entry
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.17, i64 0, i64 0), i32 498, i8* getelementptr inbounds ([52 x i8], [52 x i8]* @__PRETTY_FUNCTION__._Z10storeLinesPvssss, i64 0, i64 0)) #3, !dbg !1990
  unreachable

cond.end:                                         ; preds = %entry
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1896, metadata !1087), !dbg !1992
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1906, metadata !1087), !dbg !1994
  %1 = atomicrmw add i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), i64 1 seq_cst, !dbg !1996
  %conv = trunc i64 %1 to i32, !dbg !1997
  tail call void @llvm.dbg.value(metadata i32 %conv, i64 0, metadata !1981, metadata !1087), !dbg !1998
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !1982, metadata !1087), !dbg !1999
  tail call void @llvm.dbg.value(metadata i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 0, metadata !1983, metadata !1087), !dbg !2000
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !2001, !range !1131
  %conv2 = trunc i32 %2 to i16, !dbg !2003
  %mul = mul nsw i32 %conv, 12, !dbg !2004
  %idxprom = sext i32 %mul to i64, !dbg !2005
  %arrayidx = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom, !dbg !2005
  store i16 %conv2, i16* %arrayidx, align 2, !dbg !2006, !tbaa !2007
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !2008, !range !1131
  %conv4 = trunc i32 %3 to i16, !dbg !2010
  %add6 = or i32 %mul, 1, !dbg !2011
  %idxprom7 = sext i32 %add6 to i64, !dbg !2012
  %arrayidx8 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom7, !dbg !2012
  store i16 %conv4, i16* %arrayidx8, align 2, !dbg !2013, !tbaa !2007
  %4 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !2014, !range !1161
  %conv10 = trunc i32 %4 to i16, !dbg !2016
  %add12 = or i32 %mul, 2, !dbg !2017
  %idxprom13 = sext i32 %add12 to i64, !dbg !2018
  %arrayidx14 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom13, !dbg !2018
  store i16 %conv10, i16* %arrayidx14, align 2, !dbg !2019, !tbaa !2007
  %5 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !2020, !range !1161
  %conv16 = trunc i32 %5 to i16, !dbg !2022
  %add18 = or i32 %mul, 3, !dbg !2023
  %idxprom19 = sext i32 %add18 to i64, !dbg !2024
  %arrayidx20 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom19, !dbg !2024
  store i16 %conv16, i16* %arrayidx20, align 2, !dbg !2025, !tbaa !2007
  %6 = ptrtoint i8* %p to i64, !dbg !2026
  %add22 = mul i64 %1, 12884901888, !dbg !2027
  %sext = add i64 %add22, 4294967296, !dbg !2027
  %idxprom23 = ashr exact i64 %sext, 32, !dbg !2027
  %arrayidx24 = getelementptr inbounds i64, i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 %idxprom23, !dbg !2027
  store i64 %6, i64* %arrayidx24, align 8, !dbg !2028, !tbaa !2029
  %add26 = add nsw i32 %mul, 8, !dbg !2030
  %idxprom27 = sext i32 %add26 to i64, !dbg !2031
  %arrayidx28 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom27, !dbg !2031
  store i16 %size, i16* %arrayidx28, align 2, !dbg !2032, !tbaa !2007
  %add30 = add nsw i32 %mul, 9, !dbg !2033
  %idxprom31 = sext i32 %add30 to i64, !dbg !2034
  %arrayidx32 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom31, !dbg !2034
  store i16 %line, i16* %arrayidx32, align 2, !dbg !2035, !tbaa !2007
  %add34 = add nsw i32 %mul, 10, !dbg !2036
  %idxprom35 = sext i32 %add34 to i64, !dbg !2037
  %arrayidx36 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom35, !dbg !2037
  store i16 %colmn, i16* %arrayidx36, align 2, !dbg !2038, !tbaa !2007
  %add38 = add nsw i32 %mul, 11, !dbg !2039
  %idxprom39 = sext i32 %add38 to i64, !dbg !2040
  %arrayidx40 = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom39, !dbg !2040
  store i16 %op, i16* %arrayidx40, align 2, !dbg !2041, !tbaa !2007
  %cmp42 = icmp slt i32 %conv, 5, !dbg !2042
  br i1 %cmp42, label %if.then, label %if.end, !dbg !2044

if.then:                                          ; preds = %cond.end
  %7 = getelementptr inbounds %printf_args.8, %printf_args.8* %tmp, i64 0, i32 0, !dbg !2045
  store i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 0), i32** %7, align 8, !dbg !2045
  %8 = bitcast %printf_args.8* %tmp to i8*, !dbg !2045
  %9 = call i32 @vprintf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.18, i64 0, i64 0), i8* nonnull %8) #10, !dbg !2045
  br label %if.end, !dbg !2045

if.end:                                           ; preds = %if.then, %cond.end
  ret void, !dbg !2046
}

; Function Attrs: nounwind
define void @_Z9dumpLinesv() local_unnamed_addr #4 !dbg !2047 {
entry:
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !2050, !range !1161
  %cmp = icmp eq i32 %0, 0, !dbg !2053
  br i1 %cmp, label %lor.lhs.false, label %return, !dbg !2054

lor.lhs.false:                                    ; preds = %entry
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !2055, !range !1131
  %cmp2 = icmp eq i32 %1, 0, !dbg !2058
  br i1 %cmp2, label %lor.lhs.false3, label %return, !dbg !2059

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !2060, !range !1161
  %cmp5 = icmp eq i32 %2, 0, !dbg !2063
  br i1 %cmp5, label %lor.lhs.false6, label %return, !dbg !2064

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !2065, !range !1131
  %cmp8 = icmp eq i32 %3, 0, !dbg !2068
  br i1 %cmp8, label %for.cond.preheader, label %return, !dbg !2069

for.cond.preheader:                               ; preds = %lor.lhs.false6
  %4 = tail call i32 @vprintf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.19, i64 0, i64 0), i8* null) #10, !dbg !2071
  br label %return, !dbg !2072

return:                                           ; preds = %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry, %for.cond.preheader
  ret void, !dbg !2073
}

; Function Attrs: nounwind
define void @_Z6print1i(i32 %a) local_unnamed_addr #4 !dbg !2075 {
entry:
  %tmp = alloca %printf_args.6, align 8
  %tmp14 = alloca %printf_args.6, align 8
  tail call void @llvm.dbg.value(metadata i32 %a, i64 0, metadata !2077, metadata !1087), !dbg !2078
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !2079, !range !1161
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !2082, !range !1161
  %add = add nuw nsw i32 %1, %0, !dbg !2085
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !2086, !range !1131
  %add3 = add nuw nsw i32 %add, %2, !dbg !2089
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !2090, !range !1131
  %add5 = sub nsw i32 0, %3, !dbg !2093
  %cmp = icmp eq i32 %add3, %add5, !dbg !2093
  br i1 %cmp, label %land.lhs.true, label %if.end17, !dbg !2094

land.lhs.true:                                    ; preds = %entry
  %4 = load i8, i8* addrspacecast (i8 addrspace(1)* @VERBOSE to i8*), align 1, !dbg !2095, !tbaa !1688, !range !1690
  %tobool = icmp eq i8 %4, 0, !dbg !2095
  br i1 %tobool, label %if.end17, label %if.then, !dbg !2097

if.then:                                          ; preds = %land.lhs.true
  switch i32 %a, label %if.else15 [
    i32 1, label %if.then7
    i32 2, label %if.then11
  ], !dbg !2099

if.then7:                                         ; preds = %if.then
  %5 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 0, !dbg !2101
  store i32 %2, i32* %5, align 8, !dbg !2101
  %6 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 1, !dbg !2101
  store i32 %3, i32* %6, align 4, !dbg !2101
  %7 = bitcast %printf_args.6* %tmp to i8*, !dbg !2101
  %8 = call i32 @vprintf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.20, i64 0, i64 0), i8* nonnull %7) #10, !dbg !2103
  br label %if.end17, !dbg !2101

if.then11:                                        ; preds = %if.then
  %9 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp14, i64 0, i32 0, !dbg !2105
  store i32 %2, i32* %9, align 8, !dbg !2105
  %10 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp14, i64 0, i32 1, !dbg !2105
  store i32 %3, i32* %10, align 4, !dbg !2105
  %11 = bitcast %printf_args.6* %tmp14 to i8*, !dbg !2105
  %12 = call i32 @vprintf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.21, i64 0, i64 0), i8* nonnull %11) #10, !dbg !2107
  br label %if.end17, !dbg !2105

if.else15:                                        ; preds = %if.then
  %13 = tail call i32 @vprintf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.22, i64 0, i64 0), i8* null) #10, !dbg !2109
  br label %if.end17

if.end17:                                         ; preds = %land.lhs.true, %if.then7, %if.else15, %if.then11, %entry
  ret void, !dbg !2110
}

; Function Attrs: nounwind
define void @_Z6print2v() local_unnamed_addr #4 !dbg !2111 {
entry:
  %tmp = alloca %printf_args.6, align 8
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !2112, !range !1161
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !2115, !range !1161
  %add = add nuw nsw i32 %1, %0, !dbg !2118
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !2119, !range !1131
  %add3 = add nuw nsw i32 %add, %2, !dbg !2122
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !2123, !range !1131
  %add5 = sub nsw i32 0, %3, !dbg !2126
  %cmp = icmp eq i32 %add3, %add5, !dbg !2126
  br i1 %cmp, label %land.lhs.true, label %if.end, !dbg !2127

land.lhs.true:                                    ; preds = %entry
  %4 = load i8, i8* addrspacecast (i8 addrspace(1)* @VERBOSE to i8*), align 1, !dbg !2128, !tbaa !1688, !range !1690
  %tobool = icmp eq i8 %4, 0, !dbg !2128
  br i1 %tobool, label %if.end, label %if.then, !dbg !2130

if.then:                                          ; preds = %land.lhs.true
  %5 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 0, !dbg !2132
  store i32 %2, i32* %5, align 8, !dbg !2132
  %6 = getelementptr inbounds %printf_args.6, %printf_args.6* %tmp, i64 0, i32 1, !dbg !2132
  store i32 %3, i32* %6, align 4, !dbg !2132
  %7 = bitcast %printf_args.6* %tmp to i8*, !dbg !2132
  %8 = call i32 @vprintf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.21, i64 0, i64 0), i8* nonnull %7) #10, !dbg !2133
  br label %if.end, !dbg !2132

if.end:                                           ; preds = %land.lhs.true, %if.then, %entry
  ret void, !dbg !2134
}

; Function Attrs: nounwind
define void @_Z6print3ii(i32 %line, i32 %col) local_unnamed_addr #4 !dbg !2135 {
entry:
  %tmp = alloca %printf_args.1, align 8
  tail call void @llvm.dbg.value(metadata i32 %line, i64 0, metadata !2139, metadata !1087), !dbg !2141
  tail call void @llvm.dbg.value(metadata i32 %col, i64 0, metadata !2140, metadata !1087), !dbg !2142
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !2143, !range !1161
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !2146, !range !1161
  %add = add nuw nsw i32 %1, %0, !dbg !2149
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !2150, !range !1131
  %add3 = add nuw nsw i32 %add, %2, !dbg !2153
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !2154, !range !1131
  %add5 = sub nsw i32 0, %3, !dbg !2157
  %cmp = icmp eq i32 %add3, %add5, !dbg !2157
  br i1 %cmp, label %land.lhs.true, label %if.end, !dbg !2158

land.lhs.true:                                    ; preds = %entry
  %4 = load i8, i8* addrspacecast (i8 addrspace(1)* @VERBOSE to i8*), align 1, !dbg !2159, !tbaa !1688, !range !1690
  %tobool = icmp eq i8 %4, 0, !dbg !2159
  br i1 %tobool, label %if.end, label %if.then, !dbg !2161

if.then:                                          ; preds = %land.lhs.true
  %5 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp, i64 0, i32 0, !dbg !2163
  store i32 %line, i32* %5, align 8, !dbg !2163
  %6 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp, i64 0, i32 1, !dbg !2163
  store i32 %col, i32* %6, align 4, !dbg !2163
  %7 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp, i64 0, i32 2, !dbg !2163
  store i32 %2, i32* %7, align 8, !dbg !2163
  %8 = getelementptr inbounds %printf_args.1, %printf_args.1* %tmp, i64 0, i32 3, !dbg !2163
  store i32 %3, i32* %8, align 4, !dbg !2163
  %9 = bitcast %printf_args.1* %tmp to i8*, !dbg !2163
  %10 = call i32 @vprintf(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.23, i64 0, i64 0), i8* nonnull %9) #10, !dbg !2164
  br label %if.end, !dbg !2163

if.end:                                           ; preds = %land.lhs.true, %if.then, %entry
  ret void, !dbg !2165
}

; Function Attrs: convergent nounwind
define void @print5(i8* %p, i32 %bits, i32 %sline, i32 %scolm, i32 %op) local_unnamed_addr #0 !dbg !2166 {
entry:
  %tmp.i = alloca %printf_args.8, align 8
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !2170, metadata !1087), !dbg !2175
  tail call void @llvm.dbg.value(metadata i32 %bits, i64 0, metadata !2171, metadata !1087), !dbg !2176
  tail call void @llvm.dbg.value(metadata i32 %sline, i64 0, metadata !2172, metadata !1087), !dbg !2177
  tail call void @llvm.dbg.value(metadata i32 %scolm, i64 0, metadata !2173, metadata !1087), !dbg !2178
  tail call void @llvm.dbg.value(metadata i32 %op, i64 0, metadata !2174, metadata !1087), !dbg !2179
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !2180, !range !1131
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !2183, !range !1131
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #10, !dbg !2186, !range !1884
  %mul = mul nuw i32 %2, %1, !dbg !2189
  %add = add i32 %mul, %0, !dbg !2190
  %3 = load i32, i32* addrspacecast (i32 addrspace(1)* @CTALB to i32*), align 4, !dbg !2191, !tbaa !1371
  %cmp = icmp ult i32 %add, %3, !dbg !2192
  br i1 %cmp, label %return, label %lor.lhs.false, !dbg !2193

lor.lhs.false:                                    ; preds = %entry
  %4 = load i32, i32* addrspacecast (i32 addrspace(1)* @CTAUB to i32*), align 4, !dbg !2194, !tbaa !1371
  %cmp8 = icmp ugt i32 %add, %4, !dbg !2196
  br i1 %cmp8, label %return, label %if.end, !dbg !2197

if.end:                                           ; preds = %lor.lhs.false
  %5 = bitcast %printf_args.8* %tmp.i to i8*, !dbg !2199
  call void @llvm.lifetime.start(i64 8, i8* nonnull %5), !dbg !2199
  tail call void @llvm.dbg.value(metadata i8* %p, i64 0, metadata !1976, metadata !1087) #10, !dbg !2199
  tail call void @llvm.dbg.value(metadata i16 %conv, i64 0, metadata !1977, metadata !1087) #10, !dbg !2201
  tail call void @llvm.dbg.value(metadata i16 %conv9, i64 0, metadata !1978, metadata !1087) #10, !dbg !2202
  tail call void @llvm.dbg.value(metadata i16 %conv10, i64 0, metadata !1979, metadata !1087) #10, !dbg !2203
  tail call void @llvm.dbg.value(metadata i16 %conv11, i64 0, metadata !1980, metadata !1087) #10, !dbg !2204
  %6 = load i64, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !2205, !tbaa !1829
  %cmp.i = icmp ult i64 %6, 67108736, !dbg !2205
  br i1 %cmp.i, label %cond.end.i, label %cond.false.i, !dbg !2205

cond.false.i:                                     ; preds = %if.end
  tail call fastcc void @_ZL13__assert_failPKcS0_jS0_(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.17, i64 0, i64 0), i32 498, i8* getelementptr inbounds ([52 x i8], [52 x i8]* @__PRETTY_FUNCTION__._Z10storeLinesPvssss, i64 0, i64 0)) #3, !dbg !2206
  unreachable

cond.end.i:                                       ; preds = %if.end
  %conv11 = trunc i32 %op to i16, !dbg !2207
  %conv10 = trunc i32 %scolm to i16, !dbg !2208
  %conv9 = trunc i32 %sline to i16, !dbg !2209
  %div = sdiv i32 %bits, 8, !dbg !2210
  %conv = trunc i32 %div to i16, !dbg !2211
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1896, metadata !1087) #10, !dbg !2212
  tail call void @llvm.dbg.value(metadata i64 1, i64 0, metadata !1906, metadata !1087) #10, !dbg !2214
  %7 = atomicrmw add i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), i64 1 seq_cst, !dbg !2216
  %conv.i = trunc i64 %7 to i32, !dbg !2217
  tail call void @llvm.dbg.value(metadata i32 %conv.i, i64 0, metadata !1981, metadata !1087) #10, !dbg !2218
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !1982, metadata !1087) #10, !dbg !2219
  tail call void @llvm.dbg.value(metadata i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 0, metadata !1983, metadata !1087) #10, !dbg !2220
  %conv2.i = trunc i32 %0 to i16, !dbg !2221
  %mul.i = mul nsw i32 %conv.i, 12, !dbg !2222
  %idxprom.i = sext i32 %mul.i to i64, !dbg !2223
  %arrayidx.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom.i, !dbg !2223
  store i16 %conv2.i, i16* %arrayidx.i, align 2, !dbg !2224, !tbaa !2007
  %conv4.i = trunc i32 %1 to i16, !dbg !2225
  %add6.i = or i32 %mul.i, 1, !dbg !2226
  %idxprom7.i = sext i32 %add6.i to i64, !dbg !2227
  %arrayidx8.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom7.i, !dbg !2227
  store i16 %conv4.i, i16* %arrayidx8.i, align 2, !dbg !2228, !tbaa !2007
  %8 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !2229, !range !1161
  %conv10.i = trunc i32 %8 to i16, !dbg !2231
  %add12.i = or i32 %mul.i, 2, !dbg !2232
  %idxprom13.i = sext i32 %add12.i to i64, !dbg !2233
  %arrayidx14.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom13.i, !dbg !2233
  store i16 %conv10.i, i16* %arrayidx14.i, align 2, !dbg !2234, !tbaa !2007
  %9 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !2235, !range !1161
  %conv16.i = trunc i32 %9 to i16, !dbg !2237
  %add18.i = or i32 %mul.i, 3, !dbg !2238
  %idxprom19.i = sext i32 %add18.i to i64, !dbg !2239
  %arrayidx20.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom19.i, !dbg !2239
  store i16 %conv16.i, i16* %arrayidx20.i, align 2, !dbg !2240, !tbaa !2007
  %10 = ptrtoint i8* %p to i64, !dbg !2241
  %add22.i = mul i64 %7, 12884901888, !dbg !2242
  %sext.i = add i64 %add22.i, 4294967296, !dbg !2242
  %idxprom23.i = ashr exact i64 %sext.i, 32, !dbg !2242
  %arrayidx24.i = getelementptr inbounds i64, i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 %idxprom23.i, !dbg !2242
  store i64 %10, i64* %arrayidx24.i, align 8, !dbg !2243, !tbaa !2029
  %add26.i = add nsw i32 %mul.i, 8, !dbg !2244
  %idxprom27.i = sext i32 %add26.i to i64, !dbg !2245
  %arrayidx28.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom27.i, !dbg !2245
  store i16 %conv, i16* %arrayidx28.i, align 2, !dbg !2246, !tbaa !2007
  %add30.i = add nsw i32 %mul.i, 9, !dbg !2247
  %idxprom31.i = sext i32 %add30.i to i64, !dbg !2248
  %arrayidx32.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom31.i, !dbg !2248
  store i16 %conv9, i16* %arrayidx32.i, align 2, !dbg !2249, !tbaa !2007
  %add34.i = add nsw i32 %mul.i, 10, !dbg !2250
  %idxprom35.i = sext i32 %add34.i to i64, !dbg !2251
  %arrayidx36.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom35.i, !dbg !2251
  store i16 %conv10, i16* %arrayidx36.i, align 2, !dbg !2252, !tbaa !2007
  %add38.i = add nsw i32 %mul.i, 11, !dbg !2253
  %idxprom39.i = sext i32 %add38.i to i64, !dbg !2254
  %arrayidx40.i = getelementptr inbounds i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 %idxprom39.i, !dbg !2254
  store i16 %conv11, i16* %arrayidx40.i, align 2, !dbg !2255, !tbaa !2007
  %cmp42.i = icmp slt i32 %conv.i, 5, !dbg !2256
  br i1 %cmp42.i, label %if.then.i, label %_Z10storeLinesPvssss.exit, !dbg !2257

if.then.i:                                        ; preds = %cond.end.i
  %11 = getelementptr inbounds %printf_args.8, %printf_args.8* %tmp.i, i64 0, i32 0, !dbg !2258
  store i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 0), i32** %11, align 8, !dbg !2258
  %12 = call i32 @vprintf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.18, i64 0, i64 0), i8* nonnull %5) #10, !dbg !2258
  br label %_Z10storeLinesPvssss.exit, !dbg !2258

_Z10storeLinesPvssss.exit:                        ; preds = %cond.end.i, %if.then.i
  call void @llvm.lifetime.end(i64 8, i8* nonnull %5), !dbg !2259
  br label %return, !dbg !2260

return:                                           ; preds = %entry, %lor.lhs.false, %_Z10storeLinesPvssss.exit
  ret void, !dbg !2261
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #2

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #2

; Function Attrs: nounwind
define void @RetKernel() local_unnamed_addr #4 !dbg !2262 {
entry:
  %tmp = alloca %printf_args.13, align 8
  %tmp16 = alloca %printf_args.13, align 8
  %tmp22 = alloca %printf_args.15, align 8
  %tmp23 = alloca %printf_args.15, align 8
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #10, !dbg !2273, !range !1161
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.y() #10, !dbg !2275, !range !1161
  %add = add nuw nsw i32 %1, %0, !dbg !2278
  %2 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #10, !dbg !2279, !range !1131
  %add3 = add nuw nsw i32 %add, %2, !dbg !2282
  %3 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #10, !dbg !2283, !range !1131
  %add5 = sub nsw i32 0, %3, !dbg !2286
  %cmp = icmp eq i32 %add3, %add5, !dbg !2286
  br i1 %cmp, label %if.then, label %if.end, !dbg !2287

if.then:                                          ; preds = %entry
  tail call void @llvm.dbg.value(metadata i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 0, metadata !2264, metadata !1087), !dbg !2288
  %4 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #10, !dbg !2289, !range !2317
  %conv = trunc i32 %4 to i16, !dbg !2318
  store i16 %conv, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), align 4, !dbg !2319, !tbaa !2007
  %5 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.y() #10, !dbg !2320, !range !2317
  %conv8 = trunc i32 %5 to i16, !dbg !2323
  store i16 %conv8, i16* getelementptr (i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 1), align 2, !dbg !2324, !tbaa !2007
  %6 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #10, !dbg !2325, !range !1884
  %conv11 = trunc i32 %6 to i16, !dbg !2327
  store i16 %conv11, i16* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 1) to i16*), align 2, !dbg !2328, !tbaa !2007
  %7 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.y() #10, !dbg !2329, !range !1884
  %conv14 = trunc i32 %7 to i16, !dbg !2332
  store i16 %conv14, i16* getelementptr (i16, i16* addrspacecast (i16 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i16 addrspace(1)*) to i16*), i64 3), align 2, !dbg !2333, !tbaa !2007
  %8 = load i64, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !2334, !tbaa !1829
  %9 = getelementptr inbounds %printf_args.13, %printf_args.13* %tmp, i64 0, i32 0, !dbg !2335
  store i64 %8, i64* %9, align 8, !dbg !2335
  %10 = bitcast %printf_args.13* %tmp to i8*, !dbg !2335
  %11 = call i32 @vprintf(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.24, i64 0, i64 0), i8* nonnull %10) #10, !dbg !2335
  %12 = load i64, i64* addrspacecast (i64 addrspace(1)* @bbccnntt to i64*), align 8, !dbg !2336, !tbaa !1829
  %13 = getelementptr inbounds %printf_args.13, %printf_args.13* %tmp16, i64 0, i32 0, !dbg !2337
  store i64 %12, i64* %13, align 8, !dbg !2337
  %14 = bitcast %printf_args.13* %tmp16 to i8*, !dbg !2337
  %15 = call i32 @vprintf(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.24, i64 0, i64 0), i8* nonnull %14) #10, !dbg !2337
  call void @llvm.dbg.value(metadata i64* addrspacecast (i64 addrspace(1)* bitcast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to i64 addrspace(1)*) to i64*), i64 0, metadata !2269, metadata !1087), !dbg !2338
  %16 = load i64, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !2339, !tbaa !1829
  store i64 %16, i64* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 2) to i64*), align 8, !dbg !2340, !tbaa !2029
  call void @llvm.dbg.value(metadata i64 1024, i64 0, metadata !2270, metadata !1087), !dbg !2341
  call void @llvm.dbg.value(metadata i64 3072, i64 0, metadata !2271, metadata !1087), !dbg !2342
  %17 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp22, i64 0, i32 0, !dbg !2343
  store i32 15, i32* %17, align 8, !dbg !2343
  %18 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp22, i64 0, i32 1, !dbg !2343
  store i32 31, i32* %18, align 4, !dbg !2343
  %19 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp22, i64 0, i32 2, !dbg !2343
  store i64 1, i64* %19, align 8, !dbg !2343
  %20 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp22, i64 0, i32 3, !dbg !2343
  store i64 465, i64* %20, align 8, !dbg !2343
  %21 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp22, i64 0, i32 4, !dbg !2343
  store i64 1024, i64* %21, align 8, !dbg !2343
  %22 = bitcast %printf_args.15* %tmp22 to i8*, !dbg !2343
  %23 = call i32 @vprintf(i8* getelementptr inbounds ([57 x i8], [57 x i8]* @.str.25, i64 0, i64 0), i8* nonnull %22) #10, !dbg !2343
  %24 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 0, !dbg !2344
  store i32 15, i32* %24, align 8, !dbg !2344
  %25 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 1, !dbg !2344
  store i32 15, i32* %25, align 4, !dbg !2344
  %26 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 2, !dbg !2344
  store i64 8, i64* %26, align 8, !dbg !2344
  %27 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 3, !dbg !2344
  store i64 1800, i64* %27, align 8, !dbg !2344
  %28 = getelementptr inbounds %printf_args.15, %printf_args.15* %tmp23, i64 0, i32 4, !dbg !2344
  store i64 3072, i64* %28, align 8, !dbg !2344
  %29 = bitcast %printf_args.15* %tmp23 to i8*, !dbg !2344
  %30 = call i32 @vprintf(i8* getelementptr inbounds ([56 x i8], [56 x i8]* @.str.26, i64 0, i64 0), i8* nonnull %29) #10, !dbg !2344
  call void @llvm.dbg.value(metadata i8* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i32 0, i64 402652928) to i8*), i64 0, metadata !2272, metadata !1087), !dbg !2345
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 402652928) to i8*), i8* addrspacecast (i8 addrspace(1)* getelementptr inbounds ([15 x [31 x i8]], [15 x [31 x i8]] addrspace(1)* @funcDic, i64 0, i64 0, i64 0) to i8*), i64 465, i32 1, i1 false) #10, !dbg !2346
  call void @llvm.dbg.value(metadata i8* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i32 0, i64 402652416) to i8*), i64 0, metadata !2272, metadata !1087), !dbg !2345
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* bitcast (i32* getelementptr ([402653184 x i32], [402653184 x i32]* addrspacecast ([402653184 x i32] addrspace(1)* @buffer_oN_DeViCe to [402653184 x i32]*), i64 0, i64 402652416) to i8*), i8* addrspacecast (i8 addrspace(1)* bitcast ([20 x [5 x %struct.CallSite_t]] addrspace(1)* @contextDic to i8 addrspace(1)*) to i8*), i64 1800, i32 1, i1 false) #10, !dbg !2355
  store i64 1, i64* addrspacecast (i64 addrspace(1)* @ccnntt to i64*), align 8, !dbg !2357, !tbaa !1829
  store i64 1, i64* addrspacecast (i64 addrspace(1)* @bbccnntt to i64*), align 8, !dbg !2358, !tbaa !1829
  br label %if.end, !dbg !2359

if.end:                                           ; preds = %if.then, %entry
  ret void, !dbg !2360
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.y() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.y() #1

attributes #0 = { convergent nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { argmemonly nounwind }
attributes #3 = { convergent nounwind }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readonly "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { convergent inlinehint noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { convergent noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { norecurse nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { nounwind }
attributes #11 = { convergent noreturn nounwind }

!llvm.dbg.cu = !{!57, !664}
!nvvm.annotations = !{!1077, !1078, !1079, !1078, !1080, !1080, !1080, !1080, !1081, !1081, !1080, !1078, !1079, !1078, !1080, !1080, !1080, !1080, !1081, !1081, !1080}
!llvm.ident = !{!1082, !1082}
!nvvm.internalize.after.link = !{}
!nvvmir.version = !{!1083, !1083}
!llvm.module.flags = !{!1084, !1085, !1086}

!0 = !DIGlobalVariableExpression(var: !1)
!1 = distinct !DIGlobalVariable(name: "rA_shared", scope: !2, file: !3, line: 41, type: !64, isLocal: true, isDefinition: true)
!2 = distinct !DISubprogram(name: "kernel_gpu_cuda", linkageName: "_Z15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_", scope: !3, file: !3, line: 5, type: !4, isLocal: false, isDefinition: true, scopeLine: 11, flags: DIFlagPrototyped, isOptimized: true, unit: !57, variables: !626)
!3 = !DIFile(filename: "./kernel/./kernel_gpu_cuda.cu", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!4 = !DISubroutineType(types: !5)
!5 = !{null, !6, !12, !26, !48, !56, !48}
!6 = !DIDerivedType(tag: DW_TAG_typedef, name: "par_str", file: !7, line: 70, baseType: !8)
!7 = !DIFile(filename: "./kernel/./../main.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!8 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "par_str", file: !7, line: 65, size: 64, elements: !9, identifier: "_ZTS7par_str")
!9 = !{!10}
!10 = !DIDerivedType(tag: DW_TAG_member, name: "alpha", scope: !8, file: !7, line: 68, baseType: !11, size: 64)
!11 = !DIBasicType(name: "double", size: 64, encoding: DW_ATE_float)
!12 = !DIDerivedType(tag: DW_TAG_typedef, name: "dim_str", file: !7, line: 88, baseType: !13)
!13 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "dim_str", file: !7, line: 72, size: 448, elements: !14, identifier: "_ZTS7dim_str")
!14 = !{!15, !17, !18, !19, !20, !22, !23, !24, !25}
!15 = !DIDerivedType(tag: DW_TAG_member, name: "cur_arg", scope: !13, file: !7, line: 76, baseType: !16, size: 32)
!16 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!17 = !DIDerivedType(tag: DW_TAG_member, name: "arch_arg", scope: !13, file: !7, line: 77, baseType: !16, size: 32, offset: 32)
!18 = !DIDerivedType(tag: DW_TAG_member, name: "cores_arg", scope: !13, file: !7, line: 78, baseType: !16, size: 32, offset: 64)
!19 = !DIDerivedType(tag: DW_TAG_member, name: "boxes1d_arg", scope: !13, file: !7, line: 79, baseType: !16, size: 32, offset: 96)
!20 = !DIDerivedType(tag: DW_TAG_member, name: "number_boxes", scope: !13, file: !7, line: 82, baseType: !21, size: 64, offset: 128)
!21 = !DIBasicType(name: "long int", size: 64, encoding: DW_ATE_signed)
!22 = !DIDerivedType(tag: DW_TAG_member, name: "box_mem", scope: !13, file: !7, line: 83, baseType: !21, size: 64, offset: 192)
!23 = !DIDerivedType(tag: DW_TAG_member, name: "space_elem", scope: !13, file: !7, line: 84, baseType: !21, size: 64, offset: 256)
!24 = !DIDerivedType(tag: DW_TAG_member, name: "space_mem", scope: !13, file: !7, line: 85, baseType: !21, size: 64, offset: 320)
!25 = !DIDerivedType(tag: DW_TAG_member, name: "space_mem2", scope: !13, file: !7, line: 86, baseType: !21, size: 64, offset: 384)
!26 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !27, size: 64)
!27 = !DIDerivedType(tag: DW_TAG_typedef, name: "box_str", file: !7, line: 63, baseType: !28)
!28 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "box_str", file: !7, line: 51, size: 5248, elements: !29, identifier: "_ZTS7box_str")
!29 = !{!30, !31, !32, !33, !34, !35, !36}
!30 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !28, file: !7, line: 55, baseType: !16, size: 32)
!31 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !28, file: !7, line: 55, baseType: !16, size: 32, offset: 32)
!32 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !28, file: !7, line: 55, baseType: !16, size: 32, offset: 64)
!33 = !DIDerivedType(tag: DW_TAG_member, name: "number", scope: !28, file: !7, line: 56, baseType: !16, size: 32, offset: 96)
!34 = !DIDerivedType(tag: DW_TAG_member, name: "offset", scope: !28, file: !7, line: 57, baseType: !21, size: 64, offset: 128)
!35 = !DIDerivedType(tag: DW_TAG_member, name: "nn", scope: !28, file: !7, line: 60, baseType: !16, size: 32, offset: 192)
!36 = !DIDerivedType(tag: DW_TAG_member, name: "nei", scope: !28, file: !7, line: 61, baseType: !37, size: 4992, offset: 256)
!37 = !DICompositeType(tag: DW_TAG_array_type, baseType: !38, size: 4992, elements: !46)
!38 = !DIDerivedType(tag: DW_TAG_typedef, name: "nei_str", file: !7, line: 49, baseType: !39)
!39 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "nei_str", file: !7, line: 41, size: 192, elements: !40, identifier: "_ZTS7nei_str")
!40 = !{!41, !42, !43, !44, !45}
!41 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !39, file: !7, line: 45, baseType: !16, size: 32)
!42 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !39, file: !7, line: 45, baseType: !16, size: 32, offset: 32)
!43 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !39, file: !7, line: 45, baseType: !16, size: 32, offset: 64)
!44 = !DIDerivedType(tag: DW_TAG_member, name: "number", scope: !39, file: !7, line: 46, baseType: !16, size: 32, offset: 96)
!45 = !DIDerivedType(tag: DW_TAG_member, name: "offset", scope: !39, file: !7, line: 47, baseType: !21, size: 64, offset: 128)
!46 = !{!47}
!47 = !DISubrange(count: 26)
!48 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !49, size: 64)
!49 = !DIDerivedType(tag: DW_TAG_typedef, name: "FOUR_VECTOR", file: !7, line: 39, baseType: !50)
!50 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !7, line: 35, size: 256, elements: !51, identifier: "_ZTS11FOUR_VECTOR")
!51 = !{!52, !53, !54, !55}
!52 = !DIDerivedType(tag: DW_TAG_member, name: "v", scope: !50, file: !7, line: 37, baseType: !11, size: 64)
!53 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !50, file: !7, line: 37, baseType: !11, size: 64, offset: 64)
!54 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !50, file: !7, line: 37, baseType: !11, size: 64, offset: 128)
!55 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !50, file: !7, line: 37, baseType: !11, size: 64, offset: 192)
!56 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !11, size: 64)
!57 = distinct !DICompileUnit(language: DW_LANG_C_plus_plus, file: !58, producer: "clang version 5.0.0 (trunk 294196)", isOptimized: true, runtimeVersion: 0, emissionKind: FullDebug, enums: !59, retainedTypes: !60, globals: !61, imports: !70)
!58 = !DIFile(filename: "./kernel/kernel_gpu_cuda_wrapper.cu", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!59 = !{}
!60 = !{!11}
!61 = !{!0, !62, !67}
!62 = !DIGlobalVariableExpression(var: !63)
!63 = distinct !DIGlobalVariable(name: "rB_shared", scope: !2, file: !3, line: 50, type: !64, isLocal: true, isDefinition: true)
!64 = !DICompositeType(tag: DW_TAG_array_type, baseType: !49, size: 25600, elements: !65)
!65 = !{!66}
!66 = !DISubrange(count: 100)
!67 = !DIGlobalVariableExpression(var: !68)
!68 = distinct !DIGlobalVariable(name: "qB_shared", scope: !2, file: !3, line: 51, type: !69, isLocal: true, isDefinition: true)
!69 = !DICompositeType(tag: DW_TAG_array_type, baseType: !11, size: 6400, elements: !65)
!70 = !{!71, !78, !83, !85, !87, !89, !91, !95, !97, !99, !101, !103, !105, !107, !109, !111, !113, !115, !117, !119, !121, !123, !127, !129, !131, !133, !137, !142, !144, !146, !151, !155, !157, !159, !161, !163, !165, !167, !169, !171, !175, !179, !181, !183, !187, !189, !191, !193, !195, !197, !201, !203, !205, !210, !217, !221, !223, !225, !229, !231, !233, !237, !239, !241, !245, !247, !249, !251, !253, !255, !257, !259, !261, !263, !268, !270, !272, !276, !278, !280, !282, !284, !286, !288, !290, !294, !298, !300, !302, !306, !308, !310, !312, !314, !316, !318, !322, !328, !332, !336, !341, !344, !348, !352, !367, !371, !375, !379, !383, !388, !390, !394, !398, !402, !410, !414, !418, !422, !426, !431, !437, !441, !445, !447, !455, !459, !467, !469, !471, !475, !479, !483, !488, !492, !497, !498, !499, !500, !503, !504, !505, !506, !507, !508, !509, !512, !514, !516, !518, !520, !522, !524, !526, !529, !531, !533, !535, !537, !539, !541, !543, !545, !547, !549, !551, !553, !555, !557, !559, !561, !563, !565, !567, !569, !571, !573, !575, !577, !579, !581, !583, !585, !587, !589, !591, !593, !597, !598, !600, !602, !604, !606, !608, !610, !612, !614, !616, !618, !620, !622, !624}
!71 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !74, line: 201)
!72 = !DINamespace(name: "std", scope: null, file: !73, line: 195)
!73 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_math_forward_declares.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!74 = !DISubprogram(name: "abs", linkageName: "_ZL3absx", scope: !73, file: !73, line: 44, type: !75, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!75 = !DISubroutineType(types: !76)
!76 = !{!77, !77}
!77 = !DIBasicType(name: "long long int", size: 64, encoding: DW_ATE_signed)
!78 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !79, line: 202)
!79 = !DISubprogram(name: "acos", linkageName: "_ZL4acosf", scope: !73, file: !73, line: 46, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!80 = !DISubroutineType(types: !81)
!81 = !{!82, !82}
!82 = !DIBasicType(name: "float", size: 32, encoding: DW_ATE_float)
!83 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !84, line: 203)
!84 = !DISubprogram(name: "acosh", linkageName: "_ZL5acoshf", scope: !73, file: !73, line: 48, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!85 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !86, line: 204)
!86 = !DISubprogram(name: "asin", linkageName: "_ZL4asinf", scope: !73, file: !73, line: 50, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!87 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !88, line: 205)
!88 = !DISubprogram(name: "asinh", linkageName: "_ZL5asinhf", scope: !73, file: !73, line: 52, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!89 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !90, line: 206)
!90 = !DISubprogram(name: "atan", linkageName: "_ZL4atanf", scope: !73, file: !73, line: 56, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!91 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !92, line: 207)
!92 = !DISubprogram(name: "atan2", linkageName: "_ZL5atan2ff", scope: !73, file: !73, line: 54, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!93 = !DISubroutineType(types: !94)
!94 = !{!82, !82, !82}
!95 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !96, line: 208)
!96 = !DISubprogram(name: "atanh", linkageName: "_ZL5atanhf", scope: !73, file: !73, line: 58, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!97 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !98, line: 209)
!98 = !DISubprogram(name: "cbrt", linkageName: "_ZL4cbrtf", scope: !73, file: !73, line: 60, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!99 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !100, line: 210)
!100 = !DISubprogram(name: "ceil", linkageName: "_ZL4ceilf", scope: !73, file: !73, line: 62, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!101 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !102, line: 211)
!102 = !DISubprogram(name: "copysign", linkageName: "_ZL8copysignff", scope: !73, file: !73, line: 64, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!103 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !104, line: 212)
!104 = !DISubprogram(name: "cos", linkageName: "_ZL3cosf", scope: !73, file: !73, line: 66, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!105 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !106, line: 213)
!106 = !DISubprogram(name: "cosh", linkageName: "_ZL4coshf", scope: !73, file: !73, line: 68, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!107 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !108, line: 214)
!108 = !DISubprogram(name: "erf", linkageName: "_ZL3erff", scope: !73, file: !73, line: 72, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!109 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !110, line: 215)
!110 = !DISubprogram(name: "erfc", linkageName: "_ZL4erfcf", scope: !73, file: !73, line: 70, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!111 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !112, line: 216)
!112 = !DISubprogram(name: "exp", linkageName: "_ZL3expf", scope: !73, file: !73, line: 76, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!113 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !114, line: 217)
!114 = !DISubprogram(name: "exp2", linkageName: "_ZL4exp2f", scope: !73, file: !73, line: 74, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!115 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !116, line: 218)
!116 = !DISubprogram(name: "expm1", linkageName: "_ZL5expm1f", scope: !73, file: !73, line: 78, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!117 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !118, line: 219)
!118 = !DISubprogram(name: "fabs", linkageName: "_ZL4fabsf", scope: !73, file: !73, line: 80, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!119 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !120, line: 220)
!120 = !DISubprogram(name: "fdim", linkageName: "_ZL4fdimff", scope: !73, file: !73, line: 82, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!121 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !122, line: 221)
!122 = !DISubprogram(name: "floor", linkageName: "_ZL5floorf", scope: !73, file: !73, line: 84, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!123 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !124, line: 222)
!124 = !DISubprogram(name: "fma", linkageName: "_ZL3fmafff", scope: !73, file: !73, line: 86, type: !125, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!125 = !DISubroutineType(types: !126)
!126 = !{!82, !82, !82, !82}
!127 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !128, line: 223)
!128 = !DISubprogram(name: "fmax", linkageName: "_ZL4fmaxff", scope: !73, file: !73, line: 88, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!129 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !130, line: 224)
!130 = !DISubprogram(name: "fmin", linkageName: "_ZL4fminff", scope: !73, file: !73, line: 90, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!131 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !132, line: 225)
!132 = !DISubprogram(name: "fmod", linkageName: "_ZL4fmodff", scope: !73, file: !73, line: 92, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!133 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !134, line: 226)
!134 = !DISubprogram(name: "fpclassify", linkageName: "_ZL10fpclassifyf", scope: !73, file: !73, line: 94, type: !135, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!135 = !DISubroutineType(types: !136)
!136 = !{!16, !82}
!137 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !138, line: 227)
!138 = !DISubprogram(name: "frexp", linkageName: "_ZL5frexpfPi", scope: !73, file: !73, line: 96, type: !139, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!139 = !DISubroutineType(types: !140)
!140 = !{!82, !82, !141}
!141 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !16, size: 64)
!142 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !143, line: 228)
!143 = !DISubprogram(name: "hypot", linkageName: "_ZL5hypotff", scope: !73, file: !73, line: 98, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!144 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !145, line: 229)
!145 = !DISubprogram(name: "ilogb", linkageName: "_ZL5ilogbf", scope: !73, file: !73, line: 100, type: !135, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!146 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !147, line: 230)
!147 = !DISubprogram(name: "isfinite", linkageName: "_ZL8isfinitef", scope: !73, file: !73, line: 102, type: !148, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!148 = !DISubroutineType(types: !149)
!149 = !{!150, !82}
!150 = !DIBasicType(name: "bool", size: 8, encoding: DW_ATE_boolean)
!151 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !152, line: 231)
!152 = !DISubprogram(name: "isgreater", linkageName: "_ZL9isgreaterff", scope: !73, file: !73, line: 106, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!153 = !DISubroutineType(types: !154)
!154 = !{!150, !82, !82}
!155 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !156, line: 232)
!156 = !DISubprogram(name: "isgreaterequal", linkageName: "_ZL14isgreaterequalff", scope: !73, file: !73, line: 105, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!157 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !158, line: 233)
!158 = !DISubprogram(name: "isinf", linkageName: "_ZL5isinff", scope: !73, file: !73, line: 108, type: !148, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!159 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !160, line: 234)
!160 = !DISubprogram(name: "isless", linkageName: "_ZL6islessff", scope: !73, file: !73, line: 112, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!161 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !162, line: 235)
!162 = !DISubprogram(name: "islessequal", linkageName: "_ZL11islessequalff", scope: !73, file: !73, line: 111, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!163 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !164, line: 236)
!164 = !DISubprogram(name: "islessgreater", linkageName: "_ZL13islessgreaterff", scope: !73, file: !73, line: 114, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!165 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !166, line: 237)
!166 = !DISubprogram(name: "isnan", linkageName: "_ZL5isnanf", scope: !73, file: !73, line: 116, type: !148, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!167 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !168, line: 238)
!168 = !DISubprogram(name: "isnormal", linkageName: "_ZL8isnormalf", scope: !73, file: !73, line: 118, type: !148, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!169 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !170, line: 239)
!170 = !DISubprogram(name: "isunordered", linkageName: "_ZL11isunorderedff", scope: !73, file: !73, line: 120, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!171 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !172, line: 240)
!172 = !DISubprogram(name: "labs", linkageName: "_ZL4labsl", scope: !73, file: !73, line: 121, type: !173, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!173 = !DISubroutineType(types: !174)
!174 = !{!21, !21}
!175 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !176, line: 241)
!176 = !DISubprogram(name: "ldexp", linkageName: "_ZL5ldexpfi", scope: !73, file: !73, line: 123, type: !177, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!177 = !DISubroutineType(types: !178)
!178 = !{!82, !82, !16}
!179 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !180, line: 242)
!180 = !DISubprogram(name: "lgamma", linkageName: "_ZL6lgammaf", scope: !73, file: !73, line: 125, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!181 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !182, line: 243)
!182 = !DISubprogram(name: "llabs", linkageName: "_ZL5llabsx", scope: !73, file: !73, line: 126, type: !75, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!183 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !184, line: 244)
!184 = !DISubprogram(name: "llrint", linkageName: "_ZL6llrintf", scope: !73, file: !73, line: 128, type: !185, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!185 = !DISubroutineType(types: !186)
!186 = !{!77, !82}
!187 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !188, line: 245)
!188 = !DISubprogram(name: "log", linkageName: "_ZL3logf", scope: !73, file: !73, line: 138, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!189 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !190, line: 246)
!190 = !DISubprogram(name: "log10", linkageName: "_ZL5log10f", scope: !73, file: !73, line: 130, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!191 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !192, line: 247)
!192 = !DISubprogram(name: "log1p", linkageName: "_ZL5log1pf", scope: !73, file: !73, line: 132, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!193 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !194, line: 248)
!194 = !DISubprogram(name: "log2", linkageName: "_ZL4log2f", scope: !73, file: !73, line: 134, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!195 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !196, line: 249)
!196 = !DISubprogram(name: "logb", linkageName: "_ZL4logbf", scope: !73, file: !73, line: 136, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!197 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !198, line: 250)
!198 = !DISubprogram(name: "lrint", linkageName: "_ZL5lrintf", scope: !73, file: !73, line: 140, type: !199, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!199 = !DISubroutineType(types: !200)
!200 = !{!21, !82}
!201 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !202, line: 251)
!202 = !DISubprogram(name: "lround", linkageName: "_ZL6lroundf", scope: !73, file: !73, line: 142, type: !199, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!203 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !204, line: 252)
!204 = !DISubprogram(name: "llround", linkageName: "_ZL7llroundf", scope: !73, file: !73, line: 143, type: !185, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!205 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !206, line: 253)
!206 = !DISubprogram(name: "modf", linkageName: "_ZL4modffPf", scope: !73, file: !73, line: 145, type: !207, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!207 = !DISubroutineType(types: !208)
!208 = !{!82, !82, !209}
!209 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !82, size: 64)
!210 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !211, line: 254)
!211 = !DISubprogram(name: "nan", linkageName: "_ZL3nanPKc", scope: !73, file: !73, line: 146, type: !212, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!212 = !DISubroutineType(types: !213)
!213 = !{!11, !214}
!214 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !215, size: 64)
!215 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !216)
!216 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!217 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !218, line: 255)
!218 = !DISubprogram(name: "nanf", linkageName: "_ZL4nanfPKc", scope: !73, file: !73, line: 147, type: !219, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!219 = !DISubroutineType(types: !220)
!220 = !{!82, !214}
!221 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !222, line: 256)
!222 = !DISubprogram(name: "nearbyint", linkageName: "_ZL9nearbyintf", scope: !73, file: !73, line: 149, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!223 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !224, line: 257)
!224 = !DISubprogram(name: "nextafter", linkageName: "_ZL9nextafterff", scope: !73, file: !73, line: 151, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!225 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !226, line: 258)
!226 = !DISubprogram(name: "nexttoward", linkageName: "_ZL10nexttowardfd", scope: !73, file: !73, line: 153, type: !227, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!227 = !DISubroutineType(types: !228)
!228 = !{!82, !82, !11}
!229 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !230, line: 259)
!230 = !DISubprogram(name: "pow", linkageName: "_ZL3powfi", scope: !73, file: !73, line: 158, type: !177, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!231 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !232, line: 260)
!232 = !DISubprogram(name: "remainder", linkageName: "_ZL9remainderff", scope: !73, file: !73, line: 160, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!233 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !234, line: 261)
!234 = !DISubprogram(name: "remquo", linkageName: "_ZL6remquoffPi", scope: !73, file: !73, line: 162, type: !235, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!235 = !DISubroutineType(types: !236)
!236 = !{!82, !82, !82, !141}
!237 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !238, line: 262)
!238 = !DISubprogram(name: "rint", linkageName: "_ZL4rintf", scope: !73, file: !73, line: 164, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!239 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !240, line: 263)
!240 = !DISubprogram(name: "round", linkageName: "_ZL5roundf", scope: !73, file: !73, line: 166, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!241 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !242, line: 264)
!242 = !DISubprogram(name: "scalbln", linkageName: "_ZL7scalblnfl", scope: !73, file: !73, line: 168, type: !243, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!243 = !DISubroutineType(types: !244)
!244 = !{!82, !82, !21}
!245 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !246, line: 265)
!246 = !DISubprogram(name: "scalbn", linkageName: "_ZL6scalbnfi", scope: !73, file: !73, line: 170, type: !177, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!247 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !248, line: 266)
!248 = !DISubprogram(name: "signbit", linkageName: "_ZL7signbitf", scope: !73, file: !73, line: 172, type: !148, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!249 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !250, line: 267)
!250 = !DISubprogram(name: "sin", linkageName: "_ZL3sinf", scope: !73, file: !73, line: 174, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!251 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !252, line: 268)
!252 = !DISubprogram(name: "sinh", linkageName: "_ZL4sinhf", scope: !73, file: !73, line: 176, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!253 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !254, line: 269)
!254 = !DISubprogram(name: "sqrt", linkageName: "_ZL4sqrtf", scope: !73, file: !73, line: 178, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!255 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !256, line: 270)
!256 = !DISubprogram(name: "tan", linkageName: "_ZL3tanf", scope: !73, file: !73, line: 180, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!257 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !258, line: 271)
!258 = !DISubprogram(name: "tanh", linkageName: "_ZL4tanhf", scope: !73, file: !73, line: 182, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!259 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !260, line: 272)
!260 = !DISubprogram(name: "tgamma", linkageName: "_ZL6tgammaf", scope: !73, file: !73, line: 184, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!261 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !262, line: 273)
!262 = !DISubprogram(name: "trunc", linkageName: "_ZL5truncf", scope: !73, file: !73, line: 186, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!263 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !264, line: 102)
!264 = !DISubprogram(name: "acos", scope: !265, file: !265, line: 54, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!265 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/mathcalls.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!266 = !DISubroutineType(types: !267)
!267 = !{!11, !11}
!268 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !269, line: 121)
!269 = !DISubprogram(name: "asin", scope: !265, file: !265, line: 56, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!270 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !271, line: 140)
!271 = !DISubprogram(name: "atan", scope: !265, file: !265, line: 58, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!272 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !273, line: 159)
!273 = !DISubprogram(name: "atan2", scope: !265, file: !265, line: 60, type: !274, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!274 = !DISubroutineType(types: !275)
!275 = !{!11, !11, !11}
!276 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !277, line: 180)
!277 = !DISubprogram(name: "ceil", scope: !265, file: !265, line: 178, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!278 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !279, line: 199)
!279 = !DISubprogram(name: "cos", scope: !265, file: !265, line: 63, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!280 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !281, line: 218)
!281 = !DISubprogram(name: "cosh", scope: !265, file: !265, line: 72, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!282 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !283, line: 237)
!283 = !DISubprogram(name: "exp", scope: !265, file: !265, line: 100, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!284 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !285, line: 256)
!285 = !DISubprogram(name: "fabs", scope: !265, file: !265, line: 181, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!286 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !287, line: 275)
!287 = !DISubprogram(name: "floor", scope: !265, file: !265, line: 184, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!288 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !289, line: 294)
!289 = !DISubprogram(name: "fmod", scope: !265, file: !265, line: 187, type: !274, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!290 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !291, line: 315)
!291 = !DISubprogram(name: "frexp", scope: !265, file: !265, line: 103, type: !292, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!292 = !DISubroutineType(types: !293)
!293 = !{!11, !11, !141}
!294 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !295, line: 334)
!295 = !DISubprogram(name: "ldexp", scope: !265, file: !265, line: 106, type: !296, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!296 = !DISubroutineType(types: !297)
!297 = !{!11, !11, !16}
!298 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !299, line: 353)
!299 = !DISubprogram(name: "log", scope: !265, file: !265, line: 109, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!300 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !301, line: 372)
!301 = !DISubprogram(name: "log10", scope: !265, file: !265, line: 112, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!302 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !303, line: 391)
!303 = !DISubprogram(name: "modf", scope: !265, file: !265, line: 115, type: !304, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!304 = !DISubroutineType(types: !305)
!305 = !{!11, !11, !56}
!306 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !307, line: 403)
!307 = !DISubprogram(name: "pow", scope: !265, file: !265, line: 153, type: !274, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!308 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !309, line: 440)
!309 = !DISubprogram(name: "sin", scope: !265, file: !265, line: 65, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!310 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !311, line: 459)
!311 = !DISubprogram(name: "sinh", scope: !265, file: !265, line: 74, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!312 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !313, line: 478)
!313 = !DISubprogram(name: "sqrt", scope: !265, file: !265, line: 156, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!314 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !315, line: 497)
!315 = !DISubprogram(name: "tan", scope: !265, file: !265, line: 67, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!316 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !317, line: 516)
!317 = !DISubprogram(name: "tanh", scope: !265, file: !265, line: 76, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!318 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !319, line: 118)
!319 = !DIDerivedType(tag: DW_TAG_typedef, name: "div_t", file: !320, line: 101, baseType: !321)
!320 = !DIFile(filename: "/usr/include/stdlib.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!321 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !320, line: 97, flags: DIFlagFwdDecl, identifier: "_ZTS5div_t")
!322 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !323, line: 119)
!323 = !DIDerivedType(tag: DW_TAG_typedef, name: "ldiv_t", file: !320, line: 109, baseType: !324)
!324 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !320, line: 105, size: 128, elements: !325, identifier: "_ZTS6ldiv_t")
!325 = !{!326, !327}
!326 = !DIDerivedType(tag: DW_TAG_member, name: "quot", scope: !324, file: !320, line: 107, baseType: !21, size: 64)
!327 = !DIDerivedType(tag: DW_TAG_member, name: "rem", scope: !324, file: !320, line: 108, baseType: !21, size: 64, offset: 64)
!328 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !329, line: 121)
!329 = !DISubprogram(name: "abort", scope: !320, file: !320, line: 515, type: !330, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!330 = !DISubroutineType(types: !331)
!331 = !{null}
!332 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !333, line: 122)
!333 = !DISubprogram(name: "abs", scope: !320, file: !320, line: 775, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!334 = !DISubroutineType(types: !335)
!335 = !{!16, !16}
!336 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !337, line: 123)
!337 = !DISubprogram(name: "atexit", scope: !320, file: !320, line: 519, type: !338, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!338 = !DISubroutineType(types: !339)
!339 = !{!16, !340}
!340 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !330, size: 64)
!341 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !342, line: 129)
!342 = !DISubprogram(name: "atof", scope: !343, file: !343, line: 26, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!343 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/stdlib-float.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!344 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !345, line: 130)
!345 = !DISubprogram(name: "atoi", scope: !320, file: !320, line: 278, type: !346, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!346 = !DISubroutineType(types: !347)
!347 = !{!16, !214}
!348 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !349, line: 131)
!349 = !DISubprogram(name: "atol", scope: !320, file: !320, line: 283, type: !350, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!350 = !DISubroutineType(types: !351)
!351 = !{!21, !214}
!352 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !353, line: 132)
!353 = !DISubprogram(name: "bsearch", scope: !354, file: !354, line: 20, type: !355, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!354 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/stdlib-bsearch.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!355 = !DISubroutineType(types: !356)
!356 = !{!357, !358, !358, !360, !360, !363}
!357 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 64)
!358 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !359, size: 64)
!359 = !DIDerivedType(tag: DW_TAG_const_type, baseType: null)
!360 = !DIDerivedType(tag: DW_TAG_typedef, name: "size_t", file: !361, line: 62, baseType: !362)
!361 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/stddef.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!362 = !DIBasicType(name: "long unsigned int", size: 64, encoding: DW_ATE_unsigned)
!363 = !DIDerivedType(tag: DW_TAG_typedef, name: "__compar_fn_t", file: !320, line: 742, baseType: !364)
!364 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !365, size: 64)
!365 = !DISubroutineType(types: !366)
!366 = !{!16, !358, !358}
!367 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !368, line: 133)
!368 = !DISubprogram(name: "calloc", scope: !320, file: !320, line: 468, type: !369, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!369 = !DISubroutineType(types: !370)
!370 = !{!357, !360, !360}
!371 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !372, line: 134)
!372 = !DISubprogram(name: "div", scope: !320, file: !320, line: 789, type: !373, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!373 = !DISubroutineType(types: !374)
!374 = !{!319, !16, !16}
!375 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !376, line: 135)
!376 = !DISubprogram(name: "exit", scope: !320, file: !320, line: 543, type: !377, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!377 = !DISubroutineType(types: !378)
!378 = !{null, !16}
!379 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !380, line: 136)
!380 = !DISubprogram(name: "free", scope: !320, file: !320, line: 483, type: !381, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!381 = !DISubroutineType(types: !382)
!382 = !{null, !357}
!383 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !384, line: 137)
!384 = !DISubprogram(name: "getenv", scope: !320, file: !320, line: 564, type: !385, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!385 = !DISubroutineType(types: !386)
!386 = !{!387, !214}
!387 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !216, size: 64)
!388 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !389, line: 138)
!389 = !DISubprogram(name: "labs", scope: !320, file: !320, line: 776, type: !173, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!390 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !391, line: 139)
!391 = !DISubprogram(name: "ldiv", scope: !320, file: !320, line: 791, type: !392, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!392 = !DISubroutineType(types: !393)
!393 = !{!323, !21, !21}
!394 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !395, line: 140)
!395 = !DISubprogram(name: "malloc", scope: !320, file: !320, line: 466, type: !396, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!396 = !DISubroutineType(types: !397)
!397 = !{!357, !360}
!398 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !399, line: 142)
!399 = !DISubprogram(name: "mblen", scope: !320, file: !320, line: 863, type: !400, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!400 = !DISubroutineType(types: !401)
!401 = !{!16, !214, !360}
!402 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !403, line: 143)
!403 = !DISubprogram(name: "mbstowcs", scope: !320, file: !320, line: 874, type: !404, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!404 = !DISubroutineType(types: !405)
!405 = !{!360, !406, !409, !360}
!406 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !407)
!407 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !408, size: 64)
!408 = !DIBasicType(name: "wchar_t", size: 32, encoding: DW_ATE_signed)
!409 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !214)
!410 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !411, line: 144)
!411 = !DISubprogram(name: "mbtowc", scope: !320, file: !320, line: 866, type: !412, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!412 = !DISubroutineType(types: !413)
!413 = !{!16, !406, !409, !360}
!414 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !415, line: 146)
!415 = !DISubprogram(name: "qsort", scope: !320, file: !320, line: 765, type: !416, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!416 = !DISubroutineType(types: !417)
!417 = !{null, !357, !360, !360, !363}
!418 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !419, line: 152)
!419 = !DISubprogram(name: "rand", scope: !320, file: !320, line: 374, type: !420, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!420 = !DISubroutineType(types: !421)
!421 = !{!16}
!422 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !423, line: 153)
!423 = !DISubprogram(name: "realloc", scope: !320, file: !320, line: 480, type: !424, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!424 = !DISubroutineType(types: !425)
!425 = !{!357, !357, !360}
!426 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !427, line: 154)
!427 = !DISubprogram(name: "srand", scope: !320, file: !320, line: 376, type: !428, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!428 = !DISubroutineType(types: !429)
!429 = !{null, !430}
!430 = !DIBasicType(name: "unsigned int", size: 32, encoding: DW_ATE_unsigned)
!431 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !432, line: 155)
!432 = !DISubprogram(name: "strtod", scope: !320, file: !320, line: 164, type: !433, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!433 = !DISubroutineType(types: !434)
!434 = !{!11, !409, !435}
!435 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !436)
!436 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !387, size: 64)
!437 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !438, line: 156)
!438 = !DISubprogram(name: "strtol", scope: !320, file: !320, line: 183, type: !439, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!439 = !DISubroutineType(types: !440)
!440 = !{!21, !409, !435, !16}
!441 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !442, line: 157)
!442 = !DISubprogram(name: "strtoul", scope: !320, file: !320, line: 187, type: !443, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!443 = !DISubroutineType(types: !444)
!444 = !{!362, !409, !435, !16}
!445 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !446, line: 158)
!446 = !DISubprogram(name: "system", scope: !320, file: !320, line: 717, type: !346, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!447 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !448, line: 160)
!448 = !DISubprogram(name: "wcstombs", scope: !320, file: !320, line: 877, type: !449, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!449 = !DISubroutineType(types: !450)
!450 = !{!360, !451, !452, !360}
!451 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !387)
!452 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !453)
!453 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !454, size: 64)
!454 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !408)
!455 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !456, line: 161)
!456 = !DISubprogram(name: "wctomb", scope: !320, file: !320, line: 870, type: !457, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!457 = !DISubroutineType(types: !458)
!458 = !{!16, !387, !408}
!459 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !462, line: 201)
!460 = !DINamespace(name: "__gnu_cxx", scope: null, file: !461, line: 68)
!461 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/bits/cpp_type_traits.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!462 = !DIDerivedType(tag: DW_TAG_typedef, name: "lldiv_t", file: !320, line: 121, baseType: !463)
!463 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !320, line: 117, size: 128, elements: !464, identifier: "_ZTS7lldiv_t")
!464 = !{!465, !466}
!465 = !DIDerivedType(tag: DW_TAG_member, name: "quot", scope: !463, file: !320, line: 119, baseType: !77, size: 64)
!466 = !DIDerivedType(tag: DW_TAG_member, name: "rem", scope: !463, file: !320, line: 120, baseType: !77, size: 64, offset: 64)
!467 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !468, line: 207)
!468 = !DISubprogram(name: "_Exit", scope: !320, file: !320, line: 557, type: !377, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!469 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !470, line: 211)
!470 = !DISubprogram(name: "llabs", scope: !320, file: !320, line: 780, type: !75, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!471 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !472, line: 217)
!472 = !DISubprogram(name: "lldiv", scope: !320, file: !320, line: 797, type: !473, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!473 = !DISubroutineType(types: !474)
!474 = !{!462, !77, !77}
!475 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !476, line: 228)
!476 = !DISubprogram(name: "atoll", scope: !320, file: !320, line: 292, type: !477, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!477 = !DISubroutineType(types: !478)
!478 = !{!77, !214}
!479 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !480, line: 229)
!480 = !DISubprogram(name: "strtoll", scope: !320, file: !320, line: 209, type: !481, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!481 = !DISubroutineType(types: !482)
!482 = !{!77, !409, !435, !16}
!483 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !484, line: 230)
!484 = !DISubprogram(name: "strtoull", scope: !320, file: !320, line: 214, type: !485, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!485 = !DISubroutineType(types: !486)
!486 = !{!487, !409, !435, !16}
!487 = !DIBasicType(name: "long long unsigned int", size: 64, encoding: DW_ATE_unsigned)
!488 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !489, line: 232)
!489 = !DISubprogram(name: "strtof", scope: !320, file: !320, line: 172, type: !490, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!490 = !DISubroutineType(types: !491)
!491 = !{!82, !409, !435}
!492 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !493, line: 233)
!493 = !DISubprogram(name: "strtold", scope: !320, file: !320, line: 175, type: !494, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!494 = !DISubroutineType(types: !495)
!495 = !{!496, !409, !435}
!496 = !DIBasicType(name: "long double", size: 64, encoding: DW_ATE_float)
!497 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !462, line: 241)
!498 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !468, line: 243)
!499 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !470, line: 245)
!500 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !501, line: 246)
!501 = !DISubprogram(name: "div", linkageName: "_ZN9__gnu_cxx3divExx", scope: !460, file: !502, line: 214, type: !473, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!502 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/cstdlib", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!503 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !472, line: 247)
!504 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !476, line: 249)
!505 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !489, line: 250)
!506 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !480, line: 251)
!507 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !484, line: 252)
!508 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !493, line: 253)
!509 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !510, line: 418)
!510 = !DISubprogram(name: "acosf", linkageName: "_ZL5acosff", scope: !511, file: !511, line: 1126, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!511 = !DIFile(filename: "/usr/local/cuda/include/math_functions.hpp", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!512 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !513, line: 419)
!513 = !DISubprogram(name: "acoshf", linkageName: "_ZL6acoshff", scope: !511, file: !511, line: 1154, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!514 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !515, line: 420)
!515 = !DISubprogram(name: "asinf", linkageName: "_ZL5asinff", scope: !511, file: !511, line: 1121, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!516 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !517, line: 421)
!517 = !DISubprogram(name: "asinhf", linkageName: "_ZL6asinhff", scope: !511, file: !511, line: 1159, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!518 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !519, line: 422)
!519 = !DISubprogram(name: "atan2f", linkageName: "_ZL6atan2fff", scope: !511, file: !511, line: 1111, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!520 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !521, line: 423)
!521 = !DISubprogram(name: "atanf", linkageName: "_ZL5atanff", scope: !511, file: !511, line: 1116, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!522 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !523, line: 424)
!523 = !DISubprogram(name: "atanhf", linkageName: "_ZL6atanhff", scope: !511, file: !511, line: 1164, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!524 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !525, line: 425)
!525 = !DISubprogram(name: "cbrtf", linkageName: "_ZL5cbrtff", scope: !511, file: !511, line: 1199, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!526 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !527, line: 426)
!527 = !DISubprogram(name: "ceilf", linkageName: "_ZL5ceilff", scope: !528, file: !528, line: 647, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!528 = !DIFile(filename: "/usr/local/cuda/include/device_functions.hpp", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!529 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !530, line: 427)
!530 = !DISubprogram(name: "copysignf", linkageName: "_ZL9copysignfff", scope: !511, file: !511, line: 973, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!531 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !532, line: 428)
!532 = !DISubprogram(name: "cosf", linkageName: "_ZL4cosff", scope: !511, file: !511, line: 1027, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!533 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !534, line: 429)
!534 = !DISubprogram(name: "coshf", linkageName: "_ZL5coshff", scope: !511, file: !511, line: 1096, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!535 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !536, line: 430)
!536 = !DISubprogram(name: "erfcf", linkageName: "_ZL5erfcff", scope: !511, file: !511, line: 1259, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!537 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !538, line: 431)
!538 = !DISubprogram(name: "erff", linkageName: "_ZL4erfff", scope: !511, file: !511, line: 1249, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!539 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !540, line: 432)
!540 = !DISubprogram(name: "exp2f", linkageName: "_ZL5exp2ff", scope: !528, file: !528, line: 637, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!541 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !542, line: 433)
!542 = !DISubprogram(name: "expf", linkageName: "_ZL4expff", scope: !511, file: !511, line: 1078, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!543 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !544, line: 434)
!544 = !DISubprogram(name: "expm1f", linkageName: "_ZL6expm1ff", scope: !511, file: !511, line: 1169, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!545 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !546, line: 435)
!546 = !DISubprogram(name: "fabsf", linkageName: "_ZL5fabsff", scope: !528, file: !528, line: 582, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!547 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !548, line: 436)
!548 = !DISubprogram(name: "fdimf", linkageName: "_ZL5fdimfff", scope: !511, file: !511, line: 1385, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!549 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !550, line: 437)
!550 = !DISubprogram(name: "floorf", linkageName: "_ZL6floorff", scope: !528, file: !528, line: 572, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!551 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !552, line: 438)
!552 = !DISubprogram(name: "fmaf", linkageName: "_ZL4fmaffff", scope: !511, file: !511, line: 1337, type: !125, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!553 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !554, line: 439)
!554 = !DISubprogram(name: "fmaxf", linkageName: "_ZL5fmaxfff", scope: !528, file: !528, line: 602, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!555 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !556, line: 440)
!556 = !DISubprogram(name: "fminf", linkageName: "_ZL5fminfff", scope: !528, file: !528, line: 597, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!557 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !558, line: 441)
!558 = !DISubprogram(name: "fmodf", linkageName: "_ZL5fmodfff", scope: !511, file: !511, line: 1322, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!559 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !560, line: 442)
!560 = !DISubprogram(name: "frexpf", linkageName: "_ZL6frexpffPi", scope: !511, file: !511, line: 1312, type: !139, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!561 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !562, line: 443)
!562 = !DISubprogram(name: "hypotf", linkageName: "_ZL6hypotfff", scope: !511, file: !511, line: 1174, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!563 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !564, line: 444)
!564 = !DISubprogram(name: "ilogbf", linkageName: "_ZL6ilogbff", scope: !511, file: !511, line: 1390, type: !135, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!565 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !566, line: 445)
!566 = !DISubprogram(name: "ldexpf", linkageName: "_ZL6ldexpffi", scope: !511, file: !511, line: 1289, type: !177, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!567 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !568, line: 446)
!568 = !DISubprogram(name: "lgammaf", linkageName: "_ZL7lgammaff", scope: !511, file: !511, line: 1284, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!569 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !570, line: 447)
!570 = !DISubprogram(name: "llrintf", linkageName: "_ZL7llrintff", scope: !511, file: !511, line: 933, type: !185, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!571 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !572, line: 448)
!572 = !DISubprogram(name: "llroundf", linkageName: "_ZL8llroundff", scope: !511, file: !511, line: 1371, type: !185, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!573 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !574, line: 449)
!574 = !DISubprogram(name: "log10f", linkageName: "_ZL6log10ff", scope: !511, file: !511, line: 1140, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!575 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !576, line: 450)
!576 = !DISubprogram(name: "log1pf", linkageName: "_ZL6log1pff", scope: !511, file: !511, line: 1149, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!577 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !578, line: 451)
!578 = !DISubprogram(name: "log2f", linkageName: "_ZL5log2ff", scope: !511, file: !511, line: 1069, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!579 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !580, line: 452)
!580 = !DISubprogram(name: "logbf", linkageName: "_ZL5logbff", scope: !511, file: !511, line: 1395, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!581 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !582, line: 453)
!582 = !DISubprogram(name: "logf", linkageName: "_ZL4logff", scope: !511, file: !511, line: 1131, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!583 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !584, line: 454)
!584 = !DISubprogram(name: "lrintf", linkageName: "_ZL6lrintff", scope: !511, file: !511, line: 924, type: !199, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!585 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !586, line: 455)
!586 = !DISubprogram(name: "lroundf", linkageName: "_ZL7lroundff", scope: !511, file: !511, line: 1376, type: !199, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!587 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !588, line: 456)
!588 = !DISubprogram(name: "modff", linkageName: "_ZL5modfffPf", scope: !511, file: !511, line: 1317, type: !207, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!589 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !590, line: 457)
!590 = !DISubprogram(name: "nearbyintf", linkageName: "_ZL10nearbyintff", scope: !511, file: !511, line: 938, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!591 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !592, line: 458)
!592 = !DISubprogram(name: "nextafterf", linkageName: "_ZL10nextafterfff", scope: !511, file: !511, line: 1002, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!593 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !594, line: 459)
!594 = !DISubprogram(name: "nexttowardf", scope: !265, file: !265, line: 284, type: !595, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!595 = !DISubroutineType(types: !596)
!596 = !{!82, !82, !496}
!597 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !594, line: 460)
!598 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !599, line: 461)
!599 = !DISubprogram(name: "powf", linkageName: "_ZL4powfff", scope: !511, file: !511, line: 1352, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!600 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !601, line: 462)
!601 = !DISubprogram(name: "remainderf", linkageName: "_ZL10remainderfff", scope: !511, file: !511, line: 1327, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!602 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !603, line: 463)
!603 = !DISubprogram(name: "remquof", linkageName: "_ZL7remquofffPi", scope: !511, file: !511, line: 1332, type: !235, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!604 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !605, line: 464)
!605 = !DISubprogram(name: "rintf", linkageName: "_ZL5rintff", scope: !511, file: !511, line: 919, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!606 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !607, line: 465)
!607 = !DISubprogram(name: "roundf", linkageName: "_ZL6roundff", scope: !511, file: !511, line: 1366, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!608 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !609, line: 466)
!609 = !DISubprogram(name: "scalblnf", linkageName: "_ZL8scalblnffl", scope: !511, file: !511, line: 1299, type: !243, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!610 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !611, line: 467)
!611 = !DISubprogram(name: "scalbnf", linkageName: "_ZL7scalbnffi", scope: !511, file: !511, line: 1294, type: !177, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!612 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !613, line: 468)
!613 = !DISubprogram(name: "sinf", linkageName: "_ZL4sinff", scope: !511, file: !511, line: 1018, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!614 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !615, line: 469)
!615 = !DISubprogram(name: "sinhf", linkageName: "_ZL5sinhff", scope: !511, file: !511, line: 1101, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!616 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !617, line: 470)
!617 = !DISubprogram(name: "sqrtf", linkageName: "_ZL5sqrtff", scope: !528, file: !528, line: 887, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!618 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !619, line: 471)
!619 = !DISubprogram(name: "tanf", linkageName: "_ZL4tanff", scope: !511, file: !511, line: 1060, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!620 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !621, line: 472)
!621 = !DISubprogram(name: "tanhf", linkageName: "_ZL5tanhff", scope: !511, file: !511, line: 1106, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!622 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !623, line: 473)
!623 = !DISubprogram(name: "tgammaf", linkageName: "_ZL7tgammaff", scope: !511, file: !511, line: 1361, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!624 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !625, line: 474)
!625 = !DISubprogram(name: "truncf", linkageName: "_ZL6truncff", scope: !528, file: !528, line: 642, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!626 = !{!627, !628, !629, !630, !631, !632, !633, !634, !635, !636, !639, !640, !641, !642, !643, !644, !645, !646, !647, !648, !649, !650, !651, !652, !653, !654, !655}
!627 = !DILocalVariable(name: "d_par_gpu", arg: 1, scope: !2, file: !3, line: 5, type: !6)
!628 = !DILocalVariable(name: "d_dim_gpu", arg: 2, scope: !2, file: !3, line: 6, type: !12)
!629 = !DILocalVariable(name: "d_box_gpu", arg: 3, scope: !2, file: !3, line: 7, type: !26)
!630 = !DILocalVariable(name: "d_rv_gpu", arg: 4, scope: !2, file: !3, line: 8, type: !48)
!631 = !DILocalVariable(name: "d_qv_gpu", arg: 5, scope: !2, file: !3, line: 9, type: !56)
!632 = !DILocalVariable(name: "d_fv_gpu", arg: 6, scope: !2, file: !3, line: 10, type: !48)
!633 = !DILocalVariable(name: "bx", scope: !2, file: !3, line: 17, type: !16)
!634 = !DILocalVariable(name: "tx", scope: !2, file: !3, line: 18, type: !16)
!635 = !DILocalVariable(name: "wtx", scope: !2, file: !3, line: 21, type: !16)
!636 = !DILocalVariable(name: "a2", scope: !637, file: !3, line: 35, type: !11)
!637 = distinct !DILexicalBlock(scope: !638, file: !3, line: 27, column: 31)
!638 = distinct !DILexicalBlock(scope: !2, file: !3, line: 27, column: 5)
!639 = !DILocalVariable(name: "first_i", scope: !637, file: !3, line: 38, type: !16)
!640 = !DILocalVariable(name: "rA", scope: !637, file: !3, line: 39, type: !48)
!641 = !DILocalVariable(name: "fA", scope: !637, file: !3, line: 40, type: !48)
!642 = !DILocalVariable(name: "pointer", scope: !637, file: !3, line: 44, type: !16)
!643 = !DILocalVariable(name: "k", scope: !637, file: !3, line: 45, type: !16)
!644 = !DILocalVariable(name: "first_j", scope: !637, file: !3, line: 46, type: !16)
!645 = !DILocalVariable(name: "rB", scope: !637, file: !3, line: 47, type: !48)
!646 = !DILocalVariable(name: "qB", scope: !637, file: !3, line: 48, type: !56)
!647 = !DILocalVariable(name: "j", scope: !637, file: !3, line: 49, type: !16)
!648 = !DILocalVariable(name: "r2", scope: !637, file: !3, line: 54, type: !11)
!649 = !DILocalVariable(name: "u2", scope: !637, file: !3, line: 55, type: !11)
!650 = !DILocalVariable(name: "vij", scope: !637, file: !3, line: 56, type: !11)
!651 = !DILocalVariable(name: "fs", scope: !637, file: !3, line: 57, type: !11)
!652 = !DILocalVariable(name: "fxij", scope: !637, file: !3, line: 58, type: !11)
!653 = !DILocalVariable(name: "fyij", scope: !637, file: !3, line: 59, type: !11)
!654 = !DILocalVariable(name: "fzij", scope: !637, file: !3, line: 60, type: !11)
!655 = !DILocalVariable(name: "d", scope: !637, file: !3, line: 61, type: !656)
!656 = !DIDerivedType(tag: DW_TAG_typedef, name: "THREE_VECTOR", file: !7, line: 33, baseType: !657)
!657 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !7, line: 29, size: 192, elements: !658, identifier: "_ZTS12THREE_VECTOR")
!658 = !{!659, !660, !661}
!659 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !657, file: !7, line: 31, baseType: !11, size: 64)
!660 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !657, file: !7, line: 31, baseType: !11, size: 64, offset: 64)
!661 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !657, file: !7, line: 31, baseType: !11, size: 64, offset: 128)
!662 = !DIGlobalVariableExpression(var: !663)
!663 = distinct !DIGlobalVariable(name: "CTALB", scope: !664, file: !665, line: 15, type: !16, isLocal: false, isDefinition: true)
!664 = distinct !DICompileUnit(language: DW_LANG_C_plus_plus, file: !665, producer: "clang version 5.0.0 (trunk 294196)", isOptimized: true, runtimeVersion: 0, emissionKind: FullDebug, enums: !59, retainedTypes: !666, globals: !687, imports: !732)
!665 = !DIFile(filename: "/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/ansf.cu", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!666 = !{!387, !16, !667, !681, !682, !21, !673, !683, !77, !685}
!667 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !668, size: 64)
!668 = !DIDerivedType(tag: DW_TAG_typedef, name: "BBlog_t", file: !669, line: 37, baseType: !670)
!669 = !DIFile(filename: "/home/dshen/llvm/llvm/lib/Transforms/Hello/compile/types.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!670 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "BBlog_t", file: !669, line: 27, size: 256, elements: !671, identifier: "_ZTS7BBlog_t")
!671 = !{!672, !674, !675, !676, !677, !678, !679, !680}
!672 = !DIDerivedType(tag: DW_TAG_member, name: "bidx", scope: !670, file: !669, line: 28, baseType: !673, size: 16)
!673 = !DIBasicType(name: "short", size: 16, encoding: DW_ATE_signed)
!674 = !DIDerivedType(tag: DW_TAG_member, name: "bidy", scope: !670, file: !669, line: 29, baseType: !673, size: 16, offset: 16)
!675 = !DIDerivedType(tag: DW_TAG_member, name: "tidx", scope: !670, file: !669, line: 30, baseType: !673, size: 16, offset: 32)
!676 = !DIDerivedType(tag: DW_TAG_member, name: "tidy", scope: !670, file: !669, line: 31, baseType: !673, size: 16, offset: 48)
!677 = !DIDerivedType(tag: DW_TAG_member, name: "key", scope: !670, file: !669, line: 32, baseType: !487, size: 64, offset: 64)
!678 = !DIDerivedType(tag: DW_TAG_member, name: "sline", scope: !670, file: !669, line: 33, baseType: !16, size: 32, offset: 128)
!679 = !DIDerivedType(tag: DW_TAG_member, name: "scolm", scope: !670, file: !669, line: 34, baseType: !16, size: 32, offset: 160)
!680 = !DIDerivedType(tag: DW_TAG_member, name: "cid", scope: !670, file: !669, line: 35, baseType: !16, size: 32, offset: 192)
!681 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !673, size: 64)
!682 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !21, size: 64)
!683 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !684, size: 64)
!684 = !DIDerivedType(tag: DW_TAG_volatile_type, baseType: !77)
!685 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !686, size: 64)
!686 = !DIBasicType(name: "unsigned char", size: 8, encoding: DW_ATE_unsigned_char)
!687 = !{!662, !688, !690, !692, !694, !696, !698, !700, !702, !708, !710, !722, !724, !730}
!688 = !DIGlobalVariableExpression(var: !689)
!689 = distinct !DIGlobalVariable(name: "CTAUB", scope: !664, file: !665, line: 16, type: !16, isLocal: false, isDefinition: true)
!690 = !DIGlobalVariableExpression(var: !691)
!691 = distinct !DIGlobalVariable(name: "CONSTANCE", scope: !664, file: !665, line: 17, type: !16, isLocal: false, isDefinition: true)
!692 = !DIGlobalVariableExpression(var: !693)
!693 = distinct !DIGlobalVariable(name: "VERBOSE", scope: !664, file: !665, line: 22, type: !150, isLocal: false, isDefinition: true)
!694 = !DIGlobalVariableExpression(var: !695)
!695 = distinct !DIGlobalVariable(name: "ccnntt", scope: !664, file: !665, line: 35, type: !487, isLocal: false, isDefinition: true)
!696 = !DIGlobalVariableExpression(var: !697)
!697 = distinct !DIGlobalVariable(name: "bbccnntt", scope: !664, file: !665, line: 36, type: !487, isLocal: false, isDefinition: true)
!698 = !DIGlobalVariableExpression(var: !699)
!699 = distinct !DIGlobalVariable(name: "done1", scope: !664, file: !665, line: 42, type: !16, isLocal: false, isDefinition: true)
!700 = !DIGlobalVariableExpression(var: !701)
!701 = distinct !DIGlobalVariable(name: "done2", scope: !664, file: !665, line: 43, type: !16, isLocal: false, isDefinition: true)
!702 = !DIGlobalVariableExpression(var: !703)
!703 = distinct !DIGlobalVariable(name: "funcDic", scope: !664, file: !665, line: 46, type: !704, isLocal: false, isDefinition: true)
!704 = !DICompositeType(tag: DW_TAG_array_type, baseType: !216, size: 3720, elements: !705)
!705 = !{!706, !707}
!706 = !DISubrange(count: 15)
!707 = !DISubrange(count: 31)
!708 = !DIGlobalVariableExpression(var: !709)
!709 = distinct !DIGlobalVariable(name: "dicHeight", scope: !664, file: !665, line: 47, type: !16, isLocal: false, isDefinition: true)
!710 = !DIGlobalVariableExpression(var: !711)
!711 = distinct !DIGlobalVariable(name: "globalCallStack", scope: !664, file: !665, line: 49, type: !712, isLocal: false, isDefinition: true)
!712 = !DICompositeType(tag: DW_TAG_array_type, baseType: !713, size: 640, elements: !720)
!713 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !714, size: 64)
!714 = !DIDerivedType(tag: DW_TAG_typedef, name: "CallSite_t", file: !669, line: 12, baseType: !715)
!715 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "CallSite_t", file: !669, line: 8, size: 64, elements: !716, identifier: "_ZTS10CallSite_t")
!716 = !{!717, !718, !719}
!717 = !DIDerivedType(tag: DW_TAG_member, name: "id", scope: !715, file: !669, line: 9, baseType: !16, size: 32)
!718 = !DIDerivedType(tag: DW_TAG_member, name: "sline", scope: !715, file: !669, line: 10, baseType: !673, size: 16, offset: 32)
!719 = !DIDerivedType(tag: DW_TAG_member, name: "scolm", scope: !715, file: !669, line: 11, baseType: !673, size: 16, offset: 48)
!720 = !{!721}
!721 = !DISubrange(count: 10)
!722 = !DIGlobalVariableExpression(var: !723)
!723 = distinct !DIGlobalVariable(name: "stackHeight", scope: !664, file: !665, line: 50, type: !141, isLocal: false, isDefinition: true)
!724 = !DIGlobalVariableExpression(var: !725)
!725 = distinct !DIGlobalVariable(name: "contextDic", scope: !664, file: !665, line: 56, type: !726, isLocal: false, isDefinition: true)
!726 = !DICompositeType(tag: DW_TAG_array_type, baseType: !714, size: 6400, elements: !727)
!727 = !{!728, !729}
!728 = !DISubrange(count: 20)
!729 = !DISubrange(count: 5)
!730 = !DIGlobalVariableExpression(var: !731)
!731 = distinct !DIGlobalVariable(name: "cHeight", scope: !664, file: !665, line: 57, type: !16, isLocal: false, isDefinition: true)
!732 = !{!71, !78, !83, !85, !87, !89, !91, !95, !97, !99, !101, !103, !105, !107, !109, !111, !113, !115, !117, !119, !121, !123, !127, !129, !131, !133, !137, !142, !144, !146, !151, !155, !157, !159, !161, !163, !165, !167, !169, !171, !175, !179, !181, !183, !187, !189, !191, !193, !195, !197, !201, !203, !205, !210, !217, !221, !223, !225, !229, !231, !233, !237, !239, !241, !245, !247, !249, !251, !253, !255, !257, !259, !261, !263, !268, !270, !272, !276, !278, !280, !282, !284, !286, !288, !290, !294, !298, !300, !302, !306, !308, !310, !312, !314, !316, !318, !322, !328, !332, !336, !341, !344, !348, !352, !367, !371, !375, !379, !383, !388, !390, !394, !398, !402, !410, !414, !418, !422, !426, !431, !437, !441, !445, !447, !455, !459, !467, !469, !471, !475, !479, !483, !488, !492, !497, !498, !499, !500, !503, !504, !505, !506, !507, !508, !509, !512, !514, !516, !518, !520, !522, !524, !526, !529, !531, !533, !535, !537, !539, !541, !543, !545, !547, !549, !551, !553, !555, !557, !559, !561, !563, !565, !567, !569, !571, !573, !575, !577, !579, !581, !583, !585, !587, !589, !591, !593, !597, !598, !600, !602, !604, !606, !608, !610, !612, !614, !616, !618, !620, !622, !624, !733, !738, !740, !744, !752, !757, !761, !765, !769, !773, !775, !777, !781, !787, !791, !797, !803, !805, !809, !813, !817, !821, !828, !830, !834, !838, !842, !844, !848, !852, !856, !858, !860, !864, !872, !876, !880, !884, !886, !892, !894, !900, !904, !908, !912, !916, !920, !924, !926, !928, !932, !936, !940, !942, !946, !950, !952, !954, !958, !962, !966, !970, !971, !972, !973, !977, !980, !984, !989, !992, !994, !996, !998, !1000, !1002, !1004, !1006, !1008, !1010, !1012, !1014, !1016, !1019, !1021, !1028, !1030, !1031, !1033, !1035, !1037, !1039, !1043, !1045, !1047, !1049, !1051, !1053, !1055, !1057, !1059, !1063, !1067, !1069, !1073}
!733 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !734, line: 64)
!734 = !DIDerivedType(tag: DW_TAG_typedef, name: "mbstate_t", file: !735, line: 106, baseType: !736)
!735 = !DIFile(filename: "/usr/include/wchar.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!736 = !DIDerivedType(tag: DW_TAG_typedef, name: "__mbstate_t", file: !735, line: 94, baseType: !737)
!737 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !735, line: 82, flags: DIFlagFwdDecl, identifier: "_ZTS11__mbstate_t")
!738 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !739, line: 139)
!739 = !DIDerivedType(tag: DW_TAG_typedef, name: "wint_t", file: !361, line: 132, baseType: !430)
!740 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !741, line: 141)
!741 = !DISubprogram(name: "btowc", scope: !735, file: !735, line: 388, type: !742, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!742 = !DISubroutineType(types: !743)
!743 = !{!739, !16}
!744 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !745, line: 142)
!745 = !DISubprogram(name: "fgetwc", scope: !735, file: !735, line: 745, type: !746, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!746 = !DISubroutineType(types: !747)
!747 = !{!739, !748}
!748 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !749, size: 64)
!749 = !DIDerivedType(tag: DW_TAG_typedef, name: "__FILE", file: !750, line: 64, baseType: !751)
!750 = !DIFile(filename: "/usr/include/stdio.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!751 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "_IO_FILE", file: !750, line: 44, flags: DIFlagFwdDecl, identifier: "_ZTS8_IO_FILE")
!752 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !753, line: 143)
!753 = !DISubprogram(name: "fgetws", scope: !735, file: !735, line: 774, type: !754, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!754 = !DISubroutineType(types: !755)
!755 = !{!407, !406, !16, !756}
!756 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !748)
!757 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !758, line: 144)
!758 = !DISubprogram(name: "fputwc", scope: !735, file: !735, line: 759, type: !759, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!759 = !DISubroutineType(types: !760)
!760 = !{!739, !408, !748}
!761 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !762, line: 145)
!762 = !DISubprogram(name: "fputws", scope: !735, file: !735, line: 781, type: !763, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!763 = !DISubroutineType(types: !764)
!764 = !{!16, !452, !756}
!765 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !766, line: 146)
!766 = !DISubprogram(name: "fwide", scope: !735, file: !735, line: 587, type: !767, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!767 = !DISubroutineType(types: !768)
!768 = !{!16, !748, !16}
!769 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !770, line: 147)
!770 = !DISubprogram(name: "fwprintf", scope: !735, file: !735, line: 594, type: !771, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!771 = !DISubroutineType(types: !772)
!772 = !{!16, !756, !452, null}
!773 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !774, line: 148)
!774 = !DISubprogram(name: "fwscanf", scope: !735, file: !735, line: 635, type: !771, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!775 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !776, line: 149)
!776 = !DISubprogram(name: "getwc", scope: !735, file: !735, line: 746, type: !746, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!777 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !778, line: 150)
!778 = !DISubprogram(name: "getwchar", scope: !735, file: !735, line: 752, type: !779, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!779 = !DISubroutineType(types: !780)
!780 = !{!739}
!781 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !782, line: 151)
!782 = !DISubprogram(name: "mbrlen", scope: !735, file: !735, line: 399, type: !783, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!783 = !DISubroutineType(types: !784)
!784 = !{!360, !409, !360, !785}
!785 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !786)
!786 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !734, size: 64)
!787 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !788, line: 152)
!788 = !DISubprogram(name: "mbrtowc", scope: !735, file: !735, line: 365, type: !789, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!789 = !DISubroutineType(types: !790)
!790 = !{!360, !406, !409, !360, !785}
!791 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !792, line: 153)
!792 = !DISubprogram(name: "mbsinit", scope: !735, file: !735, line: 361, type: !793, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!793 = !DISubroutineType(types: !794)
!794 = !{!16, !795}
!795 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !796, size: 64)
!796 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !734)
!797 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !798, line: 154)
!798 = !DISubprogram(name: "mbsrtowcs", scope: !735, file: !735, line: 408, type: !799, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!799 = !DISubroutineType(types: !800)
!800 = !{!360, !406, !801, !360, !785}
!801 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !802)
!802 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !214, size: 64)
!803 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !804, line: 155)
!804 = !DISubprogram(name: "putwc", scope: !735, file: !735, line: 760, type: !759, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!805 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !806, line: 156)
!806 = !DISubprogram(name: "putwchar", scope: !735, file: !735, line: 766, type: !807, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!807 = !DISubroutineType(types: !808)
!808 = !{!739, !408}
!809 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !810, line: 158)
!810 = !DISubprogram(name: "swprintf", scope: !735, file: !735, line: 604, type: !811, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!811 = !DISubroutineType(types: !812)
!812 = !{!16, !406, !360, !452, null}
!813 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !814, line: 160)
!814 = !DISubprogram(name: "swscanf", scope: !735, file: !735, line: 645, type: !815, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!815 = !DISubroutineType(types: !816)
!816 = !{!16, !452, !452, null}
!817 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !818, line: 161)
!818 = !DISubprogram(name: "ungetwc", scope: !735, file: !735, line: 789, type: !819, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!819 = !DISubroutineType(types: !820)
!820 = !{!739, !739, !748}
!821 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !822, line: 162)
!822 = !DISubprogram(name: "vfwprintf", scope: !735, file: !735, line: 612, type: !823, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!823 = !DISubroutineType(types: !824)
!824 = !{!16, !756, !452, !825}
!825 = !DIDerivedType(tag: DW_TAG_typedef, name: "__gnuc_va_list", file: !826, line: 48, baseType: !827)
!826 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/stdarg.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!827 = !DIDerivedType(tag: DW_TAG_typedef, name: "__builtin_va_list", file: !665, baseType: !387)
!828 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !829, line: 164)
!829 = !DISubprogram(name: "vfwscanf", scope: !735, file: !735, line: 689, type: !823, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!830 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !831, line: 167)
!831 = !DISubprogram(name: "vswprintf", scope: !735, file: !735, line: 625, type: !832, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!832 = !DISubroutineType(types: !833)
!833 = !{!16, !406, !360, !452, !825}
!834 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !835, line: 170)
!835 = !DISubprogram(name: "vswscanf", scope: !735, file: !735, line: 701, type: !836, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!836 = !DISubroutineType(types: !837)
!837 = !{!16, !452, !452, !825}
!838 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !839, line: 172)
!839 = !DISubprogram(name: "vwprintf", scope: !735, file: !735, line: 620, type: !840, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!840 = !DISubroutineType(types: !841)
!841 = !{!16, !452, !825}
!842 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !843, line: 174)
!843 = !DISubprogram(name: "vwscanf", scope: !735, file: !735, line: 697, type: !840, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!844 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !845, line: 176)
!845 = !DISubprogram(name: "wcrtomb", scope: !735, file: !735, line: 370, type: !846, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!846 = !DISubroutineType(types: !847)
!847 = !{!360, !451, !408, !785}
!848 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !849, line: 177)
!849 = !DISubprogram(name: "wcscat", scope: !735, file: !735, line: 155, type: !850, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!850 = !DISubroutineType(types: !851)
!851 = !{!407, !406, !452}
!852 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !853, line: 178)
!853 = !DISubprogram(name: "wcscmp", scope: !735, file: !735, line: 163, type: !854, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!854 = !DISubroutineType(types: !855)
!855 = !{!16, !453, !453}
!856 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !857, line: 179)
!857 = !DISubprogram(name: "wcscoll", scope: !735, file: !735, line: 192, type: !854, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!858 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !859, line: 180)
!859 = !DISubprogram(name: "wcscpy", scope: !735, file: !735, line: 147, type: !850, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!860 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !861, line: 181)
!861 = !DISubprogram(name: "wcscspn", scope: !735, file: !735, line: 252, type: !862, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!862 = !DISubroutineType(types: !863)
!863 = !{!360, !453, !453}
!864 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !865, line: 182)
!865 = !DISubprogram(name: "wcsftime", scope: !735, file: !735, line: 855, type: !866, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!866 = !DISubroutineType(types: !867)
!867 = !{!360, !406, !360, !452, !868}
!868 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !869)
!869 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !870, size: 64)
!870 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !871)
!871 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "tm", file: !735, line: 137, flags: DIFlagFwdDecl, identifier: "_ZTS2tm")
!872 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !873, line: 183)
!873 = !DISubprogram(name: "wcslen", scope: !735, file: !735, line: 287, type: !874, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!874 = !DISubroutineType(types: !875)
!875 = !{!360, !453}
!876 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !877, line: 184)
!877 = !DISubprogram(name: "wcsncat", scope: !735, file: !735, line: 158, type: !878, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!878 = !DISubroutineType(types: !879)
!879 = !{!407, !406, !452, !360}
!880 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !881, line: 185)
!881 = !DISubprogram(name: "wcsncmp", scope: !735, file: !735, line: 166, type: !882, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!882 = !DISubroutineType(types: !883)
!883 = !{!16, !453, !453, !360}
!884 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !885, line: 186)
!885 = !DISubprogram(name: "wcsncpy", scope: !735, file: !735, line: 150, type: !878, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!886 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !887, line: 187)
!887 = !DISubprogram(name: "wcsrtombs", scope: !735, file: !735, line: 414, type: !888, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!888 = !DISubroutineType(types: !889)
!889 = !{!360, !451, !890, !360, !785}
!890 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !891)
!891 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !453, size: 64)
!892 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !893, line: 188)
!893 = !DISubprogram(name: "wcsspn", scope: !735, file: !735, line: 256, type: !862, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!894 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !895, line: 189)
!895 = !DISubprogram(name: "wcstod", scope: !735, file: !735, line: 450, type: !896, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!896 = !DISubroutineType(types: !897)
!897 = !{!11, !452, !898}
!898 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !899)
!899 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !407, size: 64)
!900 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !901, line: 191)
!901 = !DISubprogram(name: "wcstof", scope: !735, file: !735, line: 457, type: !902, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!902 = !DISubroutineType(types: !903)
!903 = !{!82, !452, !898}
!904 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !905, line: 193)
!905 = !DISubprogram(name: "wcstok", scope: !735, file: !735, line: 282, type: !906, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!906 = !DISubroutineType(types: !907)
!907 = !{!407, !406, !452, !898}
!908 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !909, line: 194)
!909 = !DISubprogram(name: "wcstol", scope: !735, file: !735, line: 468, type: !910, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!910 = !DISubroutineType(types: !911)
!911 = !{!21, !452, !898, !16}
!912 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !913, line: 195)
!913 = !DISubprogram(name: "wcstoul", scope: !735, file: !735, line: 473, type: !914, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!914 = !DISubroutineType(types: !915)
!915 = !{!362, !452, !898, !16}
!916 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !917, line: 196)
!917 = !DISubprogram(name: "wcsxfrm", scope: !735, file: !735, line: 196, type: !918, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!918 = !DISubroutineType(types: !919)
!919 = !{!360, !406, !452, !360}
!920 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !921, line: 197)
!921 = !DISubprogram(name: "wctob", scope: !735, file: !735, line: 394, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!922 = !DISubroutineType(types: !923)
!923 = !{!16, !739}
!924 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !925, line: 198)
!925 = !DISubprogram(name: "wmemcmp", scope: !735, file: !735, line: 325, type: !882, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!926 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !927, line: 199)
!927 = !DISubprogram(name: "wmemcpy", scope: !735, file: !735, line: 329, type: !878, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!928 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !929, line: 200)
!929 = !DISubprogram(name: "wmemmove", scope: !735, file: !735, line: 334, type: !930, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!930 = !DISubroutineType(types: !931)
!931 = !{!407, !407, !453, !360}
!932 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !933, line: 201)
!933 = !DISubprogram(name: "wmemset", scope: !735, file: !735, line: 338, type: !934, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!934 = !DISubroutineType(types: !935)
!935 = !{!407, !407, !408, !360}
!936 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !937, line: 202)
!937 = !DISubprogram(name: "wprintf", scope: !735, file: !735, line: 601, type: !938, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!938 = !DISubroutineType(types: !939)
!939 = !{!16, !452, null}
!940 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !941, line: 203)
!941 = !DISubprogram(name: "wscanf", scope: !735, file: !735, line: 642, type: !938, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!942 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !943, line: 204)
!943 = !DISubprogram(name: "wcschr", scope: !735, file: !735, line: 227, type: !944, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!944 = !DISubroutineType(types: !945)
!945 = !{!407, !453, !408}
!946 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !947, line: 205)
!947 = !DISubprogram(name: "wcspbrk", scope: !735, file: !735, line: 266, type: !948, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!948 = !DISubroutineType(types: !949)
!949 = !{!407, !453, !453}
!950 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !951, line: 206)
!951 = !DISubprogram(name: "wcsrchr", scope: !735, file: !735, line: 237, type: !944, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!952 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !953, line: 207)
!953 = !DISubprogram(name: "wcsstr", scope: !735, file: !735, line: 277, type: !948, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!954 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !955, line: 208)
!955 = !DISubprogram(name: "wmemchr", scope: !735, file: !735, line: 320, type: !956, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!956 = !DISubroutineType(types: !957)
!957 = !{!407, !453, !408, !360}
!958 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !959, line: 248)
!959 = !DISubprogram(name: "wcstold", scope: !735, file: !735, line: 459, type: !960, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!960 = !DISubroutineType(types: !961)
!961 = !{!496, !452, !898}
!962 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !963, line: 257)
!963 = !DISubprogram(name: "wcstoll", scope: !735, file: !735, line: 483, type: !964, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!964 = !DISubroutineType(types: !965)
!965 = !{!77, !452, !898, !16}
!966 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !967, line: 258)
!967 = !DISubprogram(name: "wcstoull", scope: !735, file: !735, line: 490, type: !968, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!968 = !DISubroutineType(types: !969)
!969 = !{!487, !452, !898, !16}
!970 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !959, line: 264)
!971 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !963, line: 265)
!972 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !967, line: 266)
!973 = !DIImportedEntity(tag: DW_TAG_imported_module, scope: !974, entity: !976, line: 56)
!974 = !DINamespace(name: "__gnu_debug", scope: null, file: !975, line: 54)
!975 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/debug/debug.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!976 = !DINamespace(name: "__debug", scope: !72, file: !975, line: 48)
!977 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !978, line: 53)
!978 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "lconv", file: !979, line: 53, flags: DIFlagFwdDecl, identifier: "_ZTS5lconv")
!979 = !DIFile(filename: "/usr/include/locale.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!980 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !981, line: 54)
!981 = !DISubprogram(name: "setlocale", scope: !979, file: !979, line: 124, type: !982, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!982 = !DISubroutineType(types: !983)
!983 = !{!387, !16, !214}
!984 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !985, line: 55)
!985 = !DISubprogram(name: "localeconv", scope: !979, file: !979, line: 127, type: !986, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!986 = !DISubroutineType(types: !987)
!987 = !{!988}
!988 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !978, size: 64)
!989 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !990, line: 64)
!990 = !DISubprogram(name: "isalnum", scope: !991, file: !991, line: 110, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!991 = !DIFile(filename: "/usr/include/ctype.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!992 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !993, line: 65)
!993 = !DISubprogram(name: "isalpha", scope: !991, file: !991, line: 111, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!994 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !995, line: 66)
!995 = !DISubprogram(name: "iscntrl", scope: !991, file: !991, line: 112, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!996 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !997, line: 67)
!997 = !DISubprogram(name: "isdigit", scope: !991, file: !991, line: 113, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!998 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !999, line: 68)
!999 = !DISubprogram(name: "isgraph", scope: !991, file: !991, line: 115, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1000 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1001, line: 69)
!1001 = !DISubprogram(name: "islower", scope: !991, file: !991, line: 114, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1002 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1003, line: 70)
!1003 = !DISubprogram(name: "isprint", scope: !991, file: !991, line: 116, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1004 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1005, line: 71)
!1005 = !DISubprogram(name: "ispunct", scope: !991, file: !991, line: 117, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1006 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1007, line: 72)
!1007 = !DISubprogram(name: "isspace", scope: !991, file: !991, line: 118, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1008 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1009, line: 73)
!1009 = !DISubprogram(name: "isupper", scope: !991, file: !991, line: 119, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1010 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1011, line: 74)
!1011 = !DISubprogram(name: "isxdigit", scope: !991, file: !991, line: 120, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1012 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1013, line: 75)
!1013 = !DISubprogram(name: "tolower", scope: !991, file: !991, line: 124, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1014 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1015, line: 76)
!1015 = !DISubprogram(name: "toupper", scope: !991, file: !991, line: 127, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1016 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !1017, line: 44)
!1017 = !DIDerivedType(tag: DW_TAG_typedef, name: "size_t", scope: !72, file: !1018, line: 186, baseType: !362)
!1018 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/x86_64-linux-gnu/c++/4.8/bits/c++config.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!1019 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !1020, line: 45)
!1020 = !DIDerivedType(tag: DW_TAG_typedef, name: "ptrdiff_t", scope: !72, file: !1018, line: 187, baseType: !21)
!1021 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1022, line: 82)
!1022 = !DIDerivedType(tag: DW_TAG_typedef, name: "wctrans_t", file: !1023, line: 186, baseType: !1024)
!1023 = !DIFile(filename: "/usr/include/wctype.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!1024 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1025, size: 64)
!1025 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1026)
!1026 = !DIDerivedType(tag: DW_TAG_typedef, name: "__int32_t", file: !1027, line: 40, baseType: !16)
!1027 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/types.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!1028 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1029, line: 83)
!1029 = !DIDerivedType(tag: DW_TAG_typedef, name: "wctype_t", file: !1023, line: 52, baseType: !362)
!1030 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !739, line: 84)
!1031 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1032, line: 86)
!1032 = !DISubprogram(name: "iswalnum", scope: !1023, file: !1023, line: 111, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1033 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1034, line: 87)
!1034 = !DISubprogram(name: "iswalpha", scope: !1023, file: !1023, line: 117, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1035 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1036, line: 89)
!1036 = !DISubprogram(name: "iswblank", scope: !1023, file: !1023, line: 162, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1037 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1038, line: 91)
!1038 = !DISubprogram(name: "iswcntrl", scope: !1023, file: !1023, line: 120, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1039 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1040, line: 92)
!1040 = !DISubprogram(name: "iswctype", scope: !1023, file: !1023, line: 175, type: !1041, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1041 = !DISubroutineType(types: !1042)
!1042 = !{!16, !739, !1029}
!1043 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1044, line: 93)
!1044 = !DISubprogram(name: "iswdigit", scope: !1023, file: !1023, line: 124, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1045 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1046, line: 94)
!1046 = !DISubprogram(name: "iswgraph", scope: !1023, file: !1023, line: 128, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1047 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1048, line: 95)
!1048 = !DISubprogram(name: "iswlower", scope: !1023, file: !1023, line: 133, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1049 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1050, line: 96)
!1050 = !DISubprogram(name: "iswprint", scope: !1023, file: !1023, line: 136, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1051 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1052, line: 97)
!1052 = !DISubprogram(name: "iswpunct", scope: !1023, file: !1023, line: 141, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1053 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1054, line: 98)
!1054 = !DISubprogram(name: "iswspace", scope: !1023, file: !1023, line: 146, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1055 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1056, line: 99)
!1056 = !DISubprogram(name: "iswupper", scope: !1023, file: !1023, line: 151, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1057 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1058, line: 100)
!1058 = !DISubprogram(name: "iswxdigit", scope: !1023, file: !1023, line: 156, type: !922, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1059 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1060, line: 101)
!1060 = !DISubprogram(name: "towctrans", scope: !1023, file: !1023, line: 221, type: !1061, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1061 = !DISubroutineType(types: !1062)
!1062 = !{!739, !739, !1022}
!1063 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1064, line: 102)
!1064 = !DISubprogram(name: "towlower", scope: !1023, file: !1023, line: 194, type: !1065, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1065 = !DISubroutineType(types: !1066)
!1066 = !{!739, !739}
!1067 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1068, line: 103)
!1068 = !DISubprogram(name: "towupper", scope: !1023, file: !1023, line: 197, type: !1065, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1069 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1070, line: 104)
!1070 = !DISubprogram(name: "wctrans", scope: !1023, file: !1023, line: 218, type: !1071, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1071 = !DISubroutineType(types: !1072)
!1072 = !{!1022, !214}
!1073 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !1074, line: 105)
!1074 = !DISubprogram(name: "wctype", scope: !1023, file: !1023, line: 171, type: !1075, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!1075 = !DISubroutineType(types: !1076)
!1076 = !{!1029, !214}
!1077 = !{void (%struct.par_str*, %struct.dim_str*, %struct.box_str*, %struct.FOUR_VECTOR*, double*, %struct.FOUR_VECTOR*)* @_Z15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_, !"kernel", i32 1}
!1078 = !{null, !"align", i32 8}
!1079 = !{null, !"align", i32 8, !"align", i32 65544, !"align", i32 131080}
!1080 = !{null, !"align", i32 16}
!1081 = !{null, !"align", i32 16, !"align", i32 65552, !"align", i32 131088}
!1082 = !{!"clang version 5.0.0 (trunk 294196)"}
!1083 = !{i32 1, i32 2}
!1084 = !{i32 2, !"Dwarf Version", i32 4}
!1085 = !{i32 2, !"Debug Info Version", i32 3}
!1086 = !{i32 4, !"nvvm-reflect-ftz", i32 0}
!1087 = !DIExpression()
!1088 = !DILocation(line: 5, column: 41, scope: !2)
!1089 = !DILocation(line: 6, column: 17, scope: !2)
!1090 = !DILocation(line: 7, column: 18, scope: !2)
!1091 = !DILocation(line: 8, column: 22, scope: !2)
!1092 = !DILocation(line: 9, column: 13, scope: !2)
!1093 = !DILocation(line: 10, column: 22, scope: !2)
!1094 = !DILocation(line: 78, column: 3, scope: !1095, inlinedAt: !1130)
!1095 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !1097, file: !1096, line: 78, type: !1100, isLocal: false, isDefinition: true, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true, unit: !57, declaration: !1099, variables: !59)
!1096 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_builtin_vars.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!1097 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_blockIdx_t", file: !1096, line: 77, size: 8, elements: !1098, identifier: "_ZTS25__cuda_builtin_blockIdx_t")
!1098 = !{!1099, !1102, !1103, !1104, !1115, !1119, !1123, !1126}
!1099 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !1097, file: !1096, line: 78, type: !1100, isLocal: false, isDefinition: false, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true)
!1100 = !DISubroutineType(types: !1101)
!1101 = !{!430}
!1102 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_yEv", scope: !1097, file: !1096, line: 79, type: !1100, isLocal: false, isDefinition: false, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true)
!1103 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_zEv", scope: !1097, file: !1096, line: 80, type: !1100, isLocal: false, isDefinition: false, scopeLine: 80, flags: DIFlagPrototyped, isOptimized: true)
!1104 = !DISubprogram(name: "operator uint3", linkageName: "_ZNK25__cuda_builtin_blockIdx_tcv5uint3Ev", scope: !1097, file: !1096, line: 83, type: !1105, isLocal: false, isDefinition: false, scopeLine: 83, flags: DIFlagPrototyped, isOptimized: true)
!1105 = !DISubroutineType(types: !1106)
!1106 = !{!1107, !1113}
!1107 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "uint3", file: !1108, line: 190, size: 96, elements: !1109, identifier: "_ZTS5uint3")
!1108 = !DIFile(filename: "/usr/local/cuda/include/vector_types.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!1109 = !{!1110, !1111, !1112}
!1110 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !1107, file: !1108, line: 192, baseType: !430, size: 32)
!1111 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !1107, file: !1108, line: 192, baseType: !430, size: 32, offset: 32)
!1112 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !1107, file: !1108, line: 192, baseType: !430, size: 32, offset: 64)
!1113 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1114, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1114 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1097)
!1115 = !DISubprogram(name: "__cuda_builtin_blockIdx_t", scope: !1097, file: !1096, line: 85, type: !1116, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1116 = !DISubroutineType(types: !1117)
!1117 = !{null, !1118}
!1118 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1097, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1119 = !DISubprogram(name: "__cuda_builtin_blockIdx_t", scope: !1097, file: !1096, line: 85, type: !1120, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1120 = !DISubroutineType(types: !1121)
!1121 = !{null, !1118, !1122}
!1122 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1114, size: 64)
!1123 = !DISubprogram(name: "operator=", linkageName: "_ZNK25__cuda_builtin_blockIdx_taSERKS_", scope: !1097, file: !1096, line: 85, type: !1124, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1124 = !DISubroutineType(types: !1125)
!1125 = !{null, !1113, !1122}
!1126 = !DISubprogram(name: "operator&", linkageName: "_ZNK25__cuda_builtin_blockIdx_tadEv", scope: !1097, file: !1096, line: 85, type: !1127, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1127 = !DISubroutineType(types: !1128)
!1128 = !{!1129, !1113}
!1129 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1097, size: 64)
!1130 = distinct !DILocation(line: 17, column: 11, scope: !2)
!1131 = !{i32 0, i32 65535}
!1132 = !DILocation(line: 17, column: 6, scope: !2)
!1133 = !DILocation(line: 67, column: 3, scope: !1134, inlinedAt: !1160)
!1134 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !1135, file: !1096, line: 67, type: !1100, isLocal: false, isDefinition: true, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true, unit: !57, declaration: !1137, variables: !59)
!1135 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_threadIdx_t", file: !1096, line: 66, size: 8, elements: !1136, identifier: "_ZTS26__cuda_builtin_threadIdx_t")
!1136 = !{!1137, !1138, !1139, !1140, !1145, !1149, !1153, !1156}
!1137 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !1135, file: !1096, line: 67, type: !1100, isLocal: false, isDefinition: false, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true)
!1138 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_yEv", scope: !1135, file: !1096, line: 68, type: !1100, isLocal: false, isDefinition: false, scopeLine: 68, flags: DIFlagPrototyped, isOptimized: true)
!1139 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_zEv", scope: !1135, file: !1096, line: 69, type: !1100, isLocal: false, isDefinition: false, scopeLine: 69, flags: DIFlagPrototyped, isOptimized: true)
!1140 = !DISubprogram(name: "operator uint3", linkageName: "_ZNK26__cuda_builtin_threadIdx_tcv5uint3Ev", scope: !1135, file: !1096, line: 72, type: !1141, isLocal: false, isDefinition: false, scopeLine: 72, flags: DIFlagPrototyped, isOptimized: true)
!1141 = !DISubroutineType(types: !1142)
!1142 = !{!1107, !1143}
!1143 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1144, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1144 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1135)
!1145 = !DISubprogram(name: "__cuda_builtin_threadIdx_t", scope: !1135, file: !1096, line: 74, type: !1146, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1146 = !DISubroutineType(types: !1147)
!1147 = !{null, !1148}
!1148 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1135, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1149 = !DISubprogram(name: "__cuda_builtin_threadIdx_t", scope: !1135, file: !1096, line: 74, type: !1150, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1150 = !DISubroutineType(types: !1151)
!1151 = !{null, !1148, !1152}
!1152 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1144, size: 64)
!1153 = !DISubprogram(name: "operator=", linkageName: "_ZNK26__cuda_builtin_threadIdx_taSERKS_", scope: !1135, file: !1096, line: 74, type: !1154, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1154 = !DISubroutineType(types: !1155)
!1155 = !{null, !1143, !1152}
!1156 = !DISubprogram(name: "operator&", linkageName: "_ZNK26__cuda_builtin_threadIdx_tadEv", scope: !1135, file: !1096, line: 74, type: !1157, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1157 = !DISubroutineType(types: !1158)
!1158 = !{!1159, !1143}
!1159 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1135, size: 64)
!1160 = distinct !DILocation(line: 18, column: 11, scope: !2)
!1161 = !{i32 0, i32 1024}
!1162 = !DILocation(line: 18, column: 6, scope: !2)
!1163 = !DILocation(line: 21, column: 6, scope: !2)
!1164 = !DILocation(line: 27, column: 5, scope: !638)
!1165 = !DILocation(line: 27, column: 18, scope: !638)
!1166 = !{!1167, !1171, i64 16}
!1167 = !{!"_ZTS7dim_str", !1168, i64 0, !1168, i64 4, !1168, i64 8, !1168, i64 12, !1171, i64 16, !1171, i64 24, !1171, i64 32, !1171, i64 40, !1171, i64 48}
!1168 = !{!"int", !1169, i64 0}
!1169 = !{!"omnipotent char", !1170, i64 0}
!1170 = !{!"Simple C++ TBAA"}
!1171 = !{!"long", !1169, i64 0}
!1172 = !DILocation(line: 27, column: 7, scope: !638)
!1173 = !DILocation(line: 27, column: 5, scope: !2)
!1174 = !DILocation(line: 35, column: 25, scope: !637)
!1175 = !{!1176, !1177, i64 0}
!1176 = !{!"_ZTS7par_str", !1177, i64 0}
!1177 = !{!"double", !1169, i64 0}
!1178 = !DILocation(line: 35, column: 14, scope: !637)
!1179 = !DILocation(line: 35, column: 30, scope: !637)
!1180 = !DILocation(line: 35, column: 6, scope: !637)
!1181 = !DILocation(line: 45, column: 7, scope: !637)
!1182 = !DILocation(line: 49, column: 7, scope: !637)
!1183 = !DILocation(line: 61, column: 16, scope: !637)
!1184 = !DILocation(line: 72, column: 27, scope: !637)
!1185 = !{!1186, !1171, i64 16}
!1186 = !{!"_ZTS7box_str", !1168, i64 0, !1168, i64 4, !1168, i64 8, !1168, i64 12, !1171, i64 16, !1168, i64 24, !1169, i64 32}
!1187 = !DILocation(line: 75, column: 9, scope: !637)
!1188 = !DILocation(line: 39, column: 16, scope: !637)
!1189 = !DILocation(line: 76, column: 9, scope: !637)
!1190 = !DILocation(line: 40, column: 16, scope: !637)
!1191 = !DILocation(line: 83, column: 12, scope: !1192)
!1192 = !DILexicalBlockFile(scope: !637, file: !3, discriminator: 1)
!1193 = !DILocation(line: 83, column: 3, scope: !1192)
!1194 = !DILocation(line: 84, column: 21, scope: !1195)
!1195 = distinct !DILexicalBlock(scope: !637, file: !3, line: 83, column: 32)
!1196 = !DILocation(line: 84, column: 4, scope: !1195)
!1197 = !DILocation(line: 84, column: 19, scope: !1195)
!1198 = !{i64 0, i64 8, !1199, i64 8, i64 8, !1199, i64 16, i64 8, !1199, i64 24, i64 8, !1199}
!1199 = !{!1177, !1177, i64 0}
!1200 = !DILocation(line: 90, column: 3, scope: !637)
!1201 = !DILocation(line: 97, column: 32, scope: !1202)
!1202 = !DILexicalBlockFile(scope: !1203, file: !3, discriminator: 1)
!1203 = distinct !DILexicalBlock(scope: !1204, file: !3, line: 97, column: 3)
!1204 = distinct !DILexicalBlock(scope: !637, file: !3, line: 97, column: 3)
!1205 = !{!1186, !1168, i64 24}
!1206 = !DILocation(line: 97, column: 14, scope: !1202)
!1207 = !DILocation(line: 97, column: 3, scope: !1208)
!1208 = !DILexicalBlockFile(scope: !1204, file: !3, discriminator: 1)
!1209 = !DILocation(line: 103, column: 8, scope: !1210)
!1210 = distinct !DILexicalBlock(scope: !1211, file: !3, line: 103, column: 7)
!1211 = distinct !DILexicalBlock(scope: !1203, file: !3, line: 97, column: 41)
!1212 = !DILocation(line: 44, column: 7, scope: !637)
!1213 = !DILocation(line: 103, column: 7, scope: !1211)
!1214 = !DILocation(line: 107, column: 34, scope: !1215)
!1215 = distinct !DILexicalBlock(scope: !1210, file: !3, line: 106, column: 8)
!1216 = !DILocation(line: 107, column: 15, scope: !1215)
!1217 = !DILocation(line: 107, column: 38, scope: !1215)
!1218 = !{!1219, !1168, i64 12}
!1219 = !{!"_ZTS7nei_str", !1168, i64 0, !1168, i64 4, !1168, i64 8, !1168, i64 12, !1171, i64 16}
!1220 = !DILocation(line: 115, column: 14, scope: !1211)
!1221 = !DILocation(line: 115, column: 33, scope: !1211)
!1222 = !DILocation(line: 118, column: 10, scope: !1211)
!1223 = !DILocation(line: 47, column: 16, scope: !637)
!1224 = !DILocation(line: 48, column: 7, scope: !637)
!1225 = !DILocation(line: 126, column: 4, scope: !1226)
!1226 = !DILexicalBlockFile(scope: !1211, file: !3, discriminator: 1)
!1227 = !DILocation(line: 119, column: 10, scope: !1211)
!1228 = !DILocation(line: 127, column: 22, scope: !1229)
!1229 = distinct !DILexicalBlock(scope: !1211, file: !3, line: 126, column: 33)
!1230 = !DILocation(line: 127, column: 20, scope: !1229)
!1231 = !DILocation(line: 128, column: 22, scope: !1229)
!1232 = !DILocation(line: 128, column: 20, scope: !1229)
!1233 = !DILocation(line: 134, column: 4, scope: !1211)
!1234 = !DILocation(line: 142, column: 4, scope: !1226)
!1235 = !DILocation(line: 166, column: 30, scope: !1236)
!1236 = distinct !DILexicalBlock(scope: !1237, file: !3, line: 145, column: 41)
!1237 = distinct !DILexicalBlock(scope: !1238, file: !3, line: 145, column: 5)
!1238 = distinct !DILexicalBlock(scope: !1239, file: !3, line: 145, column: 5)
!1239 = distinct !DILexicalBlock(scope: !1211, file: !3, line: 142, column: 33)
!1240 = !{!1241, !1177, i64 0}
!1241 = !{!"_ZTS11FOUR_VECTOR", !1177, i64 0, !1177, i64 8, !1177, i64 16, !1177, i64 24}
!1242 = !DILocation(line: 166, column: 38, scope: !1236)
!1243 = !DILocation(line: 166, column: 51, scope: !1236)
!1244 = !DILocation(line: 166, column: 32, scope: !1236)
!1245 = !DILocation(line: 166, column: 55, scope: !1236)
!1246 = !{!1241, !1177, i64 8}
!1247 = !{!1241, !1177, i64 16}
!1248 = !{!1241, !1177, i64 24}
!1249 = !DILocation(line: 166, column: 53, scope: !1236)
!1250 = !DILocation(line: 54, column: 6, scope: !637)
!1251 = !DILocation(line: 167, column: 13, scope: !1236)
!1252 = !DILocation(line: 55, column: 6, scope: !637)
!1253 = !DILocation(line: 168, column: 15, scope: !1236)
!1254 = !DILocalVariable(name: "a", arg: 1, scope: !1255, file: !1256, line: 245, type: !11)
!1255 = distinct !DISubprogram(name: "exp", linkageName: "_ZL3expd", scope: !1256, file: !1256, line: 245, type: !266, isLocal: true, isDefinition: true, scopeLine: 246, flags: DIFlagPrototyped, isOptimized: true, unit: !57, variables: !1257)
!1256 = !DIFile(filename: "/usr/local/cuda/include/math_functions_dbl_ptx3.hpp", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!1257 = !{!1254}
!1258 = !DILocation(line: 245, column: 52, scope: !1255, inlinedAt: !1259)
!1259 = distinct !DILocation(line: 168, column: 11, scope: !1236)
!1260 = !DILocation(line: 247, column: 10, scope: !1255, inlinedAt: !1259)
!1261 = !DILocation(line: 56, column: 6, scope: !637)
!1262 = !DILocation(line: 169, column: 12, scope: !1236)
!1263 = !DILocation(line: 57, column: 6, scope: !637)
!1264 = !DILocation(line: 171, column: 34, scope: !1236)
!1265 = !DIExpression(DW_OP_LLVM_fragment, 0, 64)
!1266 = !DILocation(line: 172, column: 13, scope: !1236)
!1267 = !DILocation(line: 58, column: 6, scope: !637)
!1268 = !DILocation(line: 173, column: 34, scope: !1236)
!1269 = !DIExpression(DW_OP_LLVM_fragment, 64, 64)
!1270 = !DILocation(line: 174, column: 13, scope: !1236)
!1271 = !DILocation(line: 59, column: 6, scope: !637)
!1272 = !DILocation(line: 175, column: 34, scope: !1236)
!1273 = !DIExpression(DW_OP_LLVM_fragment, 128, 64)
!1274 = !DILocation(line: 176, column: 13, scope: !1236)
!1275 = !DILocation(line: 60, column: 6, scope: !637)
!1276 = !DILocation(line: 178, column: 33, scope: !1236)
!1277 = !DILocation(line: 178, column: 45, scope: !1236)
!1278 = !DILocation(line: 178, column: 16, scope: !1236)
!1279 = !DILocation(line: 179, column: 33, scope: !1236)
!1280 = !DILocation(line: 179, column: 45, scope: !1236)
!1281 = !DILocation(line: 179, column: 16, scope: !1236)
!1282 = !DILocation(line: 180, column: 33, scope: !1236)
!1283 = !DILocation(line: 180, column: 45, scope: !1236)
!1284 = !DILocation(line: 180, column: 16, scope: !1236)
!1285 = !DILocation(line: 181, column: 33, scope: !1236)
!1286 = !DILocation(line: 181, column: 45, scope: !1236)
!1287 = !DILocation(line: 181, column: 16, scope: !1236)
!1288 = !DILocation(line: 145, column: 38, scope: !1289)
!1289 = !DILexicalBlockFile(scope: !1237, file: !3, discriminator: 2)
!1290 = !DILocation(line: 145, column: 16, scope: !1291)
!1291 = !DILexicalBlockFile(scope: !1237, file: !3, discriminator: 1)
!1292 = !DILocation(line: 145, column: 5, scope: !1293)
!1293 = !DILexicalBlockFile(scope: !1238, file: !3, discriminator: 1)
!1294 = distinct !{!1294, !1295, !1296}
!1295 = !DILocation(line: 145, column: 5, scope: !1238)
!1296 = !DILocation(line: 183, column: 5, scope: !1238)
!1297 = !DILocation(line: 194, column: 4, scope: !1211)
!1298 = !DILocation(line: 97, column: 38, scope: !1299)
!1299 = !DILexicalBlockFile(scope: !1203, file: !3, discriminator: 2)
!1300 = distinct !{!1300, !1301, !1302}
!1301 = !DILocation(line: 97, column: 3, scope: !1204)
!1302 = !DILocation(line: 200, column: 3, scope: !1204)
!1303 = !DILocation(line: 214, column: 1, scope: !2)
!1304 = distinct !DISubprogram(name: "mystrcpy", linkageName: "_Z8mystrcpyPcS_", scope: !665, file: !665, line: 59, type: !1305, isLocal: false, isDefinition: true, scopeLine: 60, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1307)
!1305 = !DISubroutineType(types: !1306)
!1306 = !{null, !387, !387}
!1307 = !{!1308, !1309, !1310}
!1308 = !DILocalVariable(name: "dst", arg: 1, scope: !1304, file: !665, line: 59, type: !387)
!1309 = !DILocalVariable(name: "src", arg: 2, scope: !1304, file: !665, line: 59, type: !387)
!1310 = !DILocalVariable(name: "cnt", scope: !1304, file: !665, line: 61, type: !16)
!1311 = !DILocation(line: 59, column: 32, scope: !1304)
!1312 = !DILocation(line: 59, column: 43, scope: !1304)
!1313 = !DILocation(line: 61, column: 6, scope: !1304)
!1314 = !DILocation(line: 62, column: 10, scope: !1315)
!1315 = !DILexicalBlockFile(scope: !1304, file: !665, discriminator: 1)
!1316 = !{!1169, !1169, i64 0}
!1317 = !DILocation(line: 62, column: 19, scope: !1315)
!1318 = !DILocation(line: 62, column: 2, scope: !1319)
!1319 = !DILexicalBlockFile(scope: !1304, file: !665, discriminator: 3)
!1320 = !DILocation(line: 64, column: 12, scope: !1321)
!1321 = distinct !DILexicalBlock(scope: !1304, file: !665, line: 63, column: 2)
!1322 = !DILocation(line: 65, column: 6, scope: !1321)
!1323 = !DILocation(line: 62, column: 34, scope: !1324)
!1324 = !DILexicalBlockFile(scope: !1304, file: !665, discriminator: 2)
!1325 = !DILocation(line: 62, column: 27, scope: !1315)
!1326 = distinct !{!1326, !1327, !1328}
!1327 = !DILocation(line: 62, column: 2, scope: !1304)
!1328 = !DILocation(line: 66, column: 2, scope: !1304)
!1329 = !DILocation(line: 67, column: 11, scope: !1304)
!1330 = !DILocation(line: 69, column: 1, scope: !1304)
!1331 = distinct !DISubprogram(name: "mystrcmp", linkageName: "_Z8mystrcmpPcS_", scope: !665, file: !665, line: 71, type: !1332, isLocal: false, isDefinition: true, scopeLine: 72, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1334)
!1332 = !DISubroutineType(types: !1333)
!1333 = !{!150, !387, !387}
!1334 = !{!1335, !1336, !1337}
!1335 = !DILocalVariable(name: "dst", arg: 1, scope: !1331, file: !665, line: 71, type: !387)
!1336 = !DILocalVariable(name: "src", arg: 2, scope: !1331, file: !665, line: 71, type: !387)
!1337 = !DILocalVariable(name: "cnt", scope: !1331, file: !665, line: 73, type: !16)
!1338 = !DILocation(line: 71, column: 32, scope: !1331)
!1339 = !DILocation(line: 71, column: 43, scope: !1331)
!1340 = !DILocation(line: 73, column: 13, scope: !1331)
!1341 = !DILocation(line: 74, column: 9, scope: !1342)
!1342 = !DILexicalBlockFile(scope: !1331, file: !665, discriminator: 1)
!1343 = !DILocation(line: 81, column: 20, scope: !1344)
!1344 = distinct !DILexicalBlock(scope: !1331, file: !665, line: 75, column: 9)
!1345 = !DILocation(line: 76, column: 8, scope: !1346)
!1346 = distinct !DILexicalBlock(scope: !1344, file: !665, line: 76, column: 8)
!1347 = !DILocation(line: 76, column: 28, scope: !1348)
!1348 = !DILexicalBlockFile(scope: !1346, file: !665, discriminator: 1)
!1349 = !DILocation(line: 76, column: 25, scope: !1346)
!1350 = !DILocation(line: 79, column: 30, scope: !1351)
!1351 = distinct !DILexicalBlock(scope: !1344, file: !665, line: 79, column: 21)
!1352 = !DILocation(line: 79, column: 21, scope: !1344)
!1353 = !DILocation(line: 84, column: 1, scope: !1331)
!1354 = !DILocation(line: 74, column: 21, scope: !1342)
!1355 = distinct !{!1355, !1356, !1357}
!1356 = !DILocation(line: 74, column: 9, scope: !1331)
!1357 = !DILocation(line: 82, column: 9, scope: !1331)
!1358 = distinct !DISubprogram(name: "getFuncID", linkageName: "_Z9getFuncIDPc", scope: !665, file: !665, line: 86, type: !1359, isLocal: false, isDefinition: true, scopeLine: 87, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1361)
!1359 = !DISubroutineType(types: !1360)
!1360 = !{!16, !387}
!1361 = !{!1362, !1363, !1365}
!1362 = !DILocalVariable(name: "func", arg: 1, scope: !1358, file: !665, line: 86, type: !387)
!1363 = !DILocalVariable(name: "i", scope: !1364, file: !665, line: 99, type: !16)
!1364 = distinct !DILexicalBlock(scope: !1358, file: !665, line: 99, column: 2)
!1365 = !DILocalVariable(name: "found", scope: !1366, file: !665, line: 101, type: !150)
!1366 = distinct !DILexicalBlock(scope: !1367, file: !665, line: 100, column: 2)
!1367 = distinct !DILexicalBlock(scope: !1364, file: !665, line: 99, column: 2)
!1368 = !DILocation(line: 86, column: 32, scope: !1358)
!1369 = !DILocation(line: 89, column: 6, scope: !1370)
!1370 = distinct !DILexicalBlock(scope: !1358, file: !665, line: 89, column: 6)
!1371 = !{!1168, !1168, i64 0}
!1372 = !DILocation(line: 89, column: 16, scope: !1370)
!1373 = !DILocation(line: 89, column: 6, scope: !1358)
!1374 = !DILocation(line: 99, column: 10, scope: !1364)
!1375 = !DILocation(line: 99, column: 17, scope: !1376)
!1376 = !DILexicalBlockFile(scope: !1367, file: !665, discriminator: 1)
!1377 = !DILocation(line: 99, column: 2, scope: !1378)
!1378 = !DILexicalBlockFile(scope: !1364, file: !665, discriminator: 1)
!1379 = !DILocation(line: 101, column: 26, scope: !1366)
!1380 = !DILocation(line: 59, column: 32, scope: !1304, inlinedAt: !1381)
!1381 = distinct !DILocation(line: 91, column: 3, scope: !1382)
!1382 = distinct !DILexicalBlock(scope: !1370, file: !665, line: 90, column: 2)
!1383 = !DILocation(line: 59, column: 43, scope: !1304, inlinedAt: !1381)
!1384 = !DILocation(line: 61, column: 6, scope: !1304, inlinedAt: !1381)
!1385 = !DILocation(line: 62, column: 10, scope: !1315, inlinedAt: !1381)
!1386 = !DILocation(line: 62, column: 19, scope: !1315, inlinedAt: !1381)
!1387 = !DILocation(line: 62, column: 2, scope: !1319, inlinedAt: !1381)
!1388 = !DILocation(line: 64, column: 12, scope: !1321, inlinedAt: !1381)
!1389 = !DILocation(line: 65, column: 6, scope: !1321, inlinedAt: !1381)
!1390 = !DILocation(line: 62, column: 34, scope: !1324, inlinedAt: !1381)
!1391 = !DILocation(line: 62, column: 27, scope: !1315, inlinedAt: !1381)
!1392 = !DILocation(line: 71, column: 43, scope: !1331, inlinedAt: !1393)
!1393 = distinct !DILocation(line: 101, column: 16, scope: !1366)
!1394 = !DILocation(line: 73, column: 13, scope: !1331, inlinedAt: !1393)
!1395 = !DILocation(line: 74, column: 9, scope: !1342, inlinedAt: !1393)
!1396 = !DILocation(line: 81, column: 20, scope: !1344, inlinedAt: !1393)
!1397 = !DILocation(line: 76, column: 8, scope: !1346, inlinedAt: !1393)
!1398 = !DILocation(line: 76, column: 28, scope: !1348, inlinedAt: !1393)
!1399 = !DILocation(line: 76, column: 25, scope: !1346, inlinedAt: !1393)
!1400 = !DILocation(line: 79, column: 30, scope: !1351, inlinedAt: !1393)
!1401 = !DILocation(line: 79, column: 21, scope: !1344, inlinedAt: !1393)
!1402 = !DILocation(line: 99, column: 31, scope: !1403)
!1403 = !DILexicalBlockFile(scope: !1367, file: !665, discriminator: 3)
!1404 = distinct !{!1404, !1405, !1406}
!1405 = !DILocation(line: 99, column: 2, scope: !1364)
!1406 = !DILocation(line: 105, column: 2, scope: !1364)
!1407 = !DILocation(line: 109, column: 11, scope: !1358)
!1408 = !DILocation(line: 59, column: 32, scope: !1304, inlinedAt: !1409)
!1409 = distinct !DILocation(line: 109, column: 2, scope: !1358)
!1410 = !DILocation(line: 59, column: 43, scope: !1304, inlinedAt: !1409)
!1411 = !DILocation(line: 61, column: 6, scope: !1304, inlinedAt: !1409)
!1412 = !DILocation(line: 62, column: 10, scope: !1315, inlinedAt: !1409)
!1413 = !DILocation(line: 62, column: 19, scope: !1315, inlinedAt: !1409)
!1414 = !DILocation(line: 62, column: 2, scope: !1319, inlinedAt: !1409)
!1415 = !DILocation(line: 64, column: 12, scope: !1321, inlinedAt: !1409)
!1416 = !DILocation(line: 65, column: 6, scope: !1321, inlinedAt: !1409)
!1417 = !DILocation(line: 62, column: 34, scope: !1324, inlinedAt: !1409)
!1418 = !DILocation(line: 62, column: 27, scope: !1315, inlinedAt: !1409)
!1419 = !DILocation(line: 67, column: 11, scope: !1304, inlinedAt: !1381)
!1420 = !DILocation(line: 113, column: 1, scope: !1358)
!1421 = !DILocation(line: 74, column: 21, scope: !1342, inlinedAt: !1393)
!1422 = distinct !DISubprogram(name: "updateCallStack", linkageName: "_Z15updateCallStackiissi", scope: !665, file: !665, line: 115, type: !1423, isLocal: false, isDefinition: true, scopeLine: 116, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1425)
!1423 = !DISubroutineType(types: !1424)
!1424 = !{null, !16, !16, !673, !673, !16}
!1425 = !{!1426, !1427, !1428, !1429, !1430, !1431, !1432, !1434, !1435, !1436}
!1426 = !DILocalVariable(name: "caller", arg: 1, scope: !1422, file: !665, line: 115, type: !16)
!1427 = !DILocalVariable(name: "callee", arg: 2, scope: !1422, file: !665, line: 115, type: !16)
!1428 = !DILocalVariable(name: "sline", arg: 3, scope: !1422, file: !665, line: 115, type: !673)
!1429 = !DILocalVariable(name: "scolm", arg: 4, scope: !1422, file: !665, line: 115, type: !673)
!1430 = !DILocalVariable(name: "tid", arg: 5, scope: !1422, file: !665, line: 115, type: !16)
!1431 = !DILocalVariable(name: "callStack", scope: !1422, file: !665, line: 118, type: !713)
!1432 = !DILocalVariable(name: "height", scope: !1422, file: !665, line: 119, type: !1433)
!1433 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !16, size: 64)
!1434 = !DILocalVariable(name: "p_caller", scope: !1422, file: !665, line: 137, type: !16)
!1435 = !DILocalVariable(name: "p_callee", scope: !1422, file: !665, line: 138, type: !16)
!1436 = !DILocalVariable(name: "i", scope: !1437, file: !665, line: 167, type: !16)
!1437 = distinct !DILexicalBlock(scope: !1422, file: !665, line: 167, column: 9)
!1438 = !DILocation(line: 115, column: 37, scope: !1422)
!1439 = !DILocation(line: 115, column: 49, scope: !1422)
!1440 = !DILocation(line: 115, column: 63, scope: !1422)
!1441 = !DILocation(line: 115, column: 76, scope: !1422)
!1442 = !DILocation(line: 115, column: 87, scope: !1422)
!1443 = !DILocation(line: 118, column: 26, scope: !1422)
!1444 = !{!1445, !1445, i64 0}
!1445 = !{!"any pointer", !1169, i64 0}
!1446 = !DILocation(line: 118, column: 14, scope: !1422)
!1447 = !DILocation(line: 119, column: 16, scope: !1422)
!1448 = !DILocation(line: 119, column: 7, scope: !1422)
!1449 = !DILocation(line: 121, column: 9, scope: !1422)
!1450 = !DILocation(line: 121, column: 9, scope: !1451)
!1451 = !DILexicalBlockFile(scope: !1422, file: !665, discriminator: 2)
!1452 = !DILocation(line: 125, column: 3, scope: !1453)
!1453 = distinct !DILexicalBlock(scope: !1454, file: !665, line: 124, column: 9)
!1454 = distinct !DILexicalBlock(scope: !1422, file: !665, line: 123, column: 13)
!1455 = !DILocation(line: 126, column: 16, scope: !1453)
!1456 = !DILocation(line: 126, column: 19, scope: !1453)
!1457 = !{!1458, !1168, i64 0}
!1458 = !{!"_ZTS10CallSite_t", !1168, i64 0, !1459, i64 4, !1459, i64 6}
!1459 = !{!"short", !1169, i64 0}
!1460 = !DILocation(line: 127, column: 16, scope: !1453)
!1461 = !DILocation(line: 127, column: 22, scope: !1453)
!1462 = !{!1458, !1459, i64 4}
!1463 = !DILocation(line: 128, column: 16, scope: !1453)
!1464 = !DILocation(line: 128, column: 22, scope: !1453)
!1465 = !{!1458, !1459, i64 6}
!1466 = !DILocation(line: 130, column: 16, scope: !1453)
!1467 = !DILocation(line: 130, column: 19, scope: !1453)
!1468 = !DILocation(line: 131, column: 30, scope: !1453)
!1469 = !DILocation(line: 131, column: 36, scope: !1453)
!1470 = !DILocation(line: 132, column: 30, scope: !1453)
!1471 = !DILocation(line: 132, column: 36, scope: !1453)
!1472 = !DILocation(line: 133, column: 9, scope: !1453)
!1473 = !DILocation(line: 134, column: 17, scope: !1453)
!1474 = !DILocation(line: 137, column: 40, scope: !1422)
!1475 = !DILocation(line: 137, column: 24, scope: !1422)
!1476 = !DILocation(line: 137, column: 44, scope: !1422)
!1477 = !DILocation(line: 137, column: 13, scope: !1422)
!1478 = !DILocation(line: 138, column: 40, scope: !1422)
!1479 = !DILocation(line: 138, column: 24, scope: !1422)
!1480 = !DILocation(line: 138, column: 44, scope: !1422)
!1481 = !DILocation(line: 138, column: 13, scope: !1422)
!1482 = !DILocation(line: 140, column: 23, scope: !1483)
!1483 = distinct !DILexicalBlock(scope: !1422, file: !665, line: 140, column: 14)
!1484 = !DILocation(line: 140, column: 45, scope: !1485)
!1485 = !DILexicalBlockFile(scope: !1483, file: !665, discriminator: 1)
!1486 = !DILocation(line: 140, column: 33, scope: !1483)
!1487 = !DILocation(line: 142, column: 37, scope: !1488)
!1488 = distinct !DILexicalBlock(scope: !1483, file: !665, line: 141, column: 9)
!1489 = !DILocation(line: 142, column: 43, scope: !1488)
!1490 = !DILocation(line: 143, column: 37, scope: !1488)
!1491 = !DILocation(line: 143, column: 43, scope: !1488)
!1492 = !DILocation(line: 144, column: 17, scope: !1488)
!1493 = !DILocation(line: 146, column: 38, scope: !1494)
!1494 = distinct !DILexicalBlock(scope: !1483, file: !665, line: 146, column: 19)
!1495 = !DILocation(line: 148, column: 10, scope: !1496)
!1496 = distinct !DILexicalBlock(scope: !1494, file: !665, line: 147, column: 9)
!1497 = !DILocation(line: 149, column: 27, scope: !1496)
!1498 = !DILocation(line: 149, column: 33, scope: !1496)
!1499 = !DILocation(line: 149, column: 17, scope: !1496)
!1500 = !DILocation(line: 149, column: 37, scope: !1496)
!1501 = !DILocation(line: 149, column: 40, scope: !1496)
!1502 = !DILocation(line: 150, column: 27, scope: !1496)
!1503 = !DILocation(line: 150, column: 33, scope: !1496)
!1504 = !DILocation(line: 150, column: 17, scope: !1496)
!1505 = !DILocation(line: 150, column: 37, scope: !1496)
!1506 = !DILocation(line: 150, column: 43, scope: !1496)
!1507 = !DILocation(line: 151, column: 37, scope: !1496)
!1508 = !DILocation(line: 151, column: 43, scope: !1496)
!1509 = !DILocation(line: 152, column: 17, scope: !1496)
!1510 = !DILocation(line: 154, column: 28, scope: !1511)
!1511 = distinct !DILexicalBlock(scope: !1494, file: !665, line: 154, column: 19)
!1512 = !DILocation(line: 154, column: 19, scope: !1494)
!1513 = !DILocation(line: 156, column: 3, scope: !1514)
!1514 = distinct !DILexicalBlock(scope: !1511, file: !665, line: 155, column: 9)
!1515 = !DILocation(line: 157, column: 27, scope: !1514)
!1516 = !DILocation(line: 157, column: 33, scope: !1514)
!1517 = !DILocation(line: 157, column: 17, scope: !1514)
!1518 = !DILocation(line: 157, column: 37, scope: !1514)
!1519 = !DILocation(line: 157, column: 43, scope: !1514)
!1520 = !DILocation(line: 158, column: 37, scope: !1514)
!1521 = !DILocation(line: 158, column: 43, scope: !1514)
!1522 = !DILocation(line: 160, column: 17, scope: !1514)
!1523 = !DILocation(line: 160, column: 35, scope: !1514)
!1524 = !DILocation(line: 160, column: 38, scope: !1514)
!1525 = !DILocation(line: 161, column: 27, scope: !1514)
!1526 = !DILocation(line: 161, column: 17, scope: !1514)
!1527 = !DILocation(line: 161, column: 35, scope: !1514)
!1528 = !DILocation(line: 161, column: 41, scope: !1514)
!1529 = !DILocation(line: 162, column: 35, scope: !1514)
!1530 = !DILocation(line: 162, column: 41, scope: !1514)
!1531 = !DILocation(line: 163, column: 9, scope: !1514)
!1532 = !DILocation(line: 164, column: 17, scope: !1514)
!1533 = !DILocation(line: 167, column: 18, scope: !1437)
!1534 = !DILocation(line: 167, column: 31, scope: !1535)
!1535 = !DILexicalBlockFile(scope: !1536, file: !665, discriminator: 1)
!1536 = distinct !DILexicalBlock(scope: !1437, file: !665, line: 167, column: 9)
!1537 = !DILocation(line: 167, column: 9, scope: !1538)
!1538 = !DILexicalBlockFile(scope: !1437, file: !665, discriminator: 1)
!1539 = !DILocation(line: 169, column: 22, scope: !1540)
!1540 = distinct !DILexicalBlock(scope: !1541, file: !665, line: 169, column: 22)
!1541 = distinct !DILexicalBlock(scope: !1536, file: !665, line: 168, column: 9)
!1542 = !DILocation(line: 169, column: 35, scope: !1540)
!1543 = !DILocation(line: 169, column: 38, scope: !1540)
!1544 = !DILocation(line: 169, column: 22, scope: !1541)
!1545 = distinct !{!1545, !1546, !1547}
!1546 = !DILocation(line: 167, column: 9, scope: !1437)
!1547 = !DILocation(line: 180, column: 9, scope: !1437)
!1548 = !DILocation(line: 171, column: 11, scope: !1549)
!1549 = distinct !DILexicalBlock(scope: !1540, file: !665, line: 170, column: 17)
!1550 = !DILocation(line: 172, column: 41, scope: !1549)
!1551 = !DILocation(line: 173, column: 38, scope: !1549)
!1552 = !DILocation(line: 174, column: 38, scope: !1549)
!1553 = !DILocation(line: 176, column: 44, scope: !1549)
!1554 = !DILocation(line: 177, column: 44, scope: !1549)
!1555 = !DILocation(line: 183, column: 9, scope: !1422)
!1556 = !DILocation(line: 184, column: 1, scope: !1557)
!1557 = !DILexicalBlockFile(scope: !1422, file: !665, discriminator: 3)
!1558 = distinct !DISubprogram(name: "__assert_fail", linkageName: "_ZL13__assert_failPKcS0_jS0_", scope: !1559, file: !1559, line: 281, type: !1560, isLocal: true, isDefinition: true, scopeLine: 283, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1562)
!1559 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_runtime_wrapper.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!1560 = !DISubroutineType(types: !1561)
!1561 = !{null, !214, !214, !430, !214}
!1562 = !{!1563, !1564, !1565, !1566}
!1563 = !DILocalVariable(name: "__message", arg: 1, scope: !1558, file: !1559, line: 281, type: !214)
!1564 = !DILocalVariable(name: "__file", arg: 2, scope: !1558, file: !1559, line: 282, type: !214)
!1565 = !DILocalVariable(name: "__line", arg: 3, scope: !1558, file: !1559, line: 282, type: !430)
!1566 = !DILocalVariable(name: "__function", arg: 4, scope: !1558, file: !1559, line: 283, type: !214)
!1567 = !DILocation(line: 284, column: 3, scope: !1558)
!1568 = distinct !DISubprogram(name: "printCallStack", linkageName: "_Z14printCallStacki", scope: !665, file: !665, line: 187, type: !377, isLocal: false, isDefinition: true, scopeLine: 188, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1569)
!1569 = !{!1570, !1571, !1572, !1573}
!1570 = !DILocalVariable(name: "tid", arg: 1, scope: !1568, file: !665, line: 187, type: !16)
!1571 = !DILocalVariable(name: "callStack", scope: !1568, file: !665, line: 189, type: !713)
!1572 = !DILocalVariable(name: "height", scope: !1568, file: !665, line: 190, type: !16)
!1573 = !DILocalVariable(name: "i", scope: !1574, file: !665, line: 197, type: !16)
!1574 = distinct !DILexicalBlock(scope: !1568, file: !665, line: 197, column: 9)
!1575 = !DILocation(line: 187, column: 36, scope: !1568)
!1576 = !DILocation(line: 189, column: 26, scope: !1568)
!1577 = !DILocation(line: 189, column: 14, scope: !1568)
!1578 = !DILocation(line: 190, column: 22, scope: !1568)
!1579 = !DILocation(line: 190, column: 13, scope: !1568)
!1580 = !DILocation(line: 191, column: 2, scope: !1568)
!1581 = !DILocation(line: 194, column: 19, scope: !1582)
!1582 = distinct !DILexicalBlock(scope: !1568, file: !665, line: 194, column: 13)
!1583 = !DILocation(line: 197, column: 18, scope: !1574)
!1584 = !DILocation(line: 194, column: 13, scope: !1568)
!1585 = !DILocation(line: 197, column: 9, scope: !1586)
!1586 = !DILexicalBlockFile(scope: !1574, file: !665, discriminator: 1)
!1587 = !DILocation(line: 198, column: 74, scope: !1588)
!1588 = distinct !DILexicalBlock(scope: !1574, file: !665, line: 197, column: 9)
!1589 = !DILocation(line: 198, column: 92, scope: !1588)
!1590 = !DILocation(line: 198, column: 79, scope: !1588)
!1591 = !DILocation(line: 198, column: 113, scope: !1588)
!1592 = !DILocation(line: 198, column: 100, scope: !1588)
!1593 = !DILocation(line: 198, column: 17, scope: !1588)
!1594 = !DILocation(line: 198, column: 61, scope: !1588)
!1595 = !DILocation(line: 197, column: 34, scope: !1596)
!1596 = !DILexicalBlockFile(scope: !1588, file: !665, discriminator: 3)
!1597 = !DILocation(line: 197, column: 24, scope: !1598)
!1598 = !DILexicalBlockFile(scope: !1588, file: !665, discriminator: 1)
!1599 = distinct !{!1599, !1600, !1601}
!1600 = !DILocation(line: 197, column: 9, scope: !1574)
!1601 = !DILocation(line: 198, column: 119, scope: !1574)
!1602 = !DILocation(line: 199, column: 1, scope: !1603)
!1603 = !DILexicalBlockFile(scope: !1568, file: !665, discriminator: 2)
!1604 = distinct !DISubprogram(name: "holdon", linkageName: "_Z6holdonl", scope: !665, file: !665, line: 201, type: !1605, isLocal: false, isDefinition: true, scopeLine: 202, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1610)
!1605 = !DISubroutineType(types: !1606)
!1606 = !{!16, !1607}
!1607 = !DIDerivedType(tag: DW_TAG_typedef, name: "clock_t", file: !1608, line: 59, baseType: !1609)
!1608 = !DIFile(filename: "/usr/include/time.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!1609 = !DIDerivedType(tag: DW_TAG_typedef, name: "__clock_t", file: !1027, line: 135, baseType: !21)
!1610 = !{!1611, !1612, !1613}
!1611 = !DILocalVariable(name: "c", arg: 1, scope: !1604, file: !665, line: 201, type: !1607)
!1612 = !DILocalVariable(name: "start_clock", scope: !1604, file: !665, line: 203, type: !1607)
!1613 = !DILocalVariable(name: "clock_offset", scope: !1604, file: !665, line: 204, type: !1607)
!1614 = !DILocation(line: 201, column: 31, scope: !1604)
!1615 = !DILocation(line: 344, column: 3, scope: !1616, inlinedAt: !1619)
!1616 = distinct !DISubprogram(name: "clock", linkageName: "_ZL5clockv", scope: !528, file: !528, line: 340, type: !420, isLocal: true, isDefinition: true, scopeLine: 342, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1617)
!1617 = !{!1618}
!1618 = !DILocalVariable(name: "r", scope: !1616, file: !528, line: 343, type: !16)
!1619 = distinct !DILocation(line: 203, column: 24, scope: !1604)
!1620 = !{i32 2280603}
!1621 = !DILocation(line: 343, column: 7, scope: !1616, inlinedAt: !1619)
!1622 = !DILocation(line: 203, column: 24, scope: !1604)
!1623 = !DILocation(line: 203, column: 10, scope: !1604)
!1624 = !DILocation(line: 204, column: 14, scope: !1604)
!1625 = !DILocation(line: 205, column: 23, scope: !1626)
!1626 = !DILexicalBlockFile(scope: !1604, file: !665, discriminator: 1)
!1627 = !DILocation(line: 205, column: 3, scope: !1626)
!1628 = !DILocation(line: 344, column: 3, scope: !1616, inlinedAt: !1629)
!1629 = distinct !DILocation(line: 206, column: 25, scope: !1604)
!1630 = !DILocation(line: 343, column: 7, scope: !1616, inlinedAt: !1629)
!1631 = !DILocation(line: 206, column: 25, scope: !1604)
!1632 = !DILocation(line: 206, column: 33, scope: !1604)
!1633 = distinct !{!1633, !1634, !1635}
!1634 = !DILocation(line: 205, column: 3, scope: !1604)
!1635 = !DILocation(line: 206, column: 35, scope: !1604)
!1636 = !DILocation(line: 208, column: 9, scope: !1604)
!1637 = !DILocation(line: 208, column: 2, scope: !1604)
!1638 = distinct !DISubprogram(name: "InitKernel", scope: !665, file: !665, line: 212, type: !330, isLocal: false, isDefinition: true, scopeLine: 213, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !59)
!1639 = !DILocation(line: 214, column: 2, scope: !1638)
!1640 = distinct !DISubprogram(name: "callFunc", scope: !665, file: !665, line: 291, type: !1641, isLocal: false, isDefinition: true, scopeLine: 292, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1643)
!1641 = !DISubroutineType(types: !1642)
!1642 = !{null, !357, !357, !16, !16}
!1643 = !{!1644, !1645, !1646, !1647}
!1644 = !DILocalVariable(name: "er", arg: 1, scope: !1640, file: !665, line: 291, type: !357)
!1645 = !DILocalVariable(name: "ee", arg: 2, scope: !1640, file: !665, line: 291, type: !357)
!1646 = !DILocalVariable(name: "sline", arg: 3, scope: !1640, file: !665, line: 291, type: !16)
!1647 = !DILocalVariable(name: "scolm", arg: 4, scope: !1640, file: !665, line: 291, type: !16)
!1648 = !DILocation(line: 291, column: 32, scope: !1640)
!1649 = !DILocation(line: 291, column: 42, scope: !1640)
!1650 = !DILocation(line: 291, column: 50, scope: !1640)
!1651 = !DILocation(line: 291, column: 61, scope: !1640)
!1652 = !DILocation(line: 293, column: 2, scope: !1640)
!1653 = distinct !DISubprogram(name: "takeString", scope: !665, file: !665, line: 318, type: !1654, isLocal: false, isDefinition: true, scopeLine: 319, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1656)
!1654 = !DISubroutineType(types: !1655)
!1655 = !{null, !357, !16}
!1656 = !{!1657, !1658}
!1657 = !DILocalVariable(name: "p", arg: 1, scope: !1653, file: !665, line: 318, type: !357)
!1658 = !DILocalVariable(name: "action", arg: 2, scope: !1653, file: !665, line: 318, type: !16)
!1659 = !DILocation(line: 318, column: 34, scope: !1653)
!1660 = !DILocation(line: 318, column: 41, scope: !1653)
!1661 = !DILocation(line: 67, column: 3, scope: !1662, inlinedAt: !1663)
!1662 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !1135, file: !1096, line: 67, type: !1100, isLocal: false, isDefinition: true, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true, unit: !664, declaration: !1137, variables: !59)
!1663 = distinct !DILocation(line: 320, column: 6, scope: !1664)
!1664 = distinct !DILexicalBlock(scope: !1653, file: !665, line: 320, column: 6)
!1665 = !DILocation(line: 320, column: 18, scope: !1664)
!1666 = !DILocation(line: 320, column: 23, scope: !1664)
!1667 = !DILocation(line: 78, column: 3, scope: !1668, inlinedAt: !1669)
!1668 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !1097, file: !1096, line: 78, type: !1100, isLocal: false, isDefinition: true, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true, unit: !664, declaration: !1099, variables: !59)
!1669 = distinct !DILocation(line: 320, column: 26, scope: !1670)
!1670 = !DILexicalBlockFile(scope: !1664, file: !665, discriminator: 1)
!1671 = !DILocation(line: 320, column: 37, scope: !1670)
!1672 = !DILocation(line: 320, column: 42, scope: !1670)
!1673 = !DILocation(line: 68, column: 3, scope: !1674, inlinedAt: !1675)
!1674 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_yEv", scope: !1135, file: !1096, line: 68, type: !1100, isLocal: false, isDefinition: true, scopeLine: 68, flags: DIFlagPrototyped, isOptimized: true, unit: !664, declaration: !1138, variables: !59)
!1675 = distinct !DILocation(line: 320, column: 45, scope: !1676)
!1676 = !DILexicalBlockFile(scope: !1664, file: !665, discriminator: 2)
!1677 = !DILocation(line: 320, column: 57, scope: !1676)
!1678 = !DILocation(line: 320, column: 62, scope: !1676)
!1679 = !DILocation(line: 79, column: 3, scope: !1680, inlinedAt: !1681)
!1680 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_yEv", scope: !1097, file: !1096, line: 79, type: !1100, isLocal: false, isDefinition: true, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true, unit: !664, declaration: !1102, variables: !59)
!1681 = distinct !DILocation(line: 320, column: 65, scope: !1682)
!1682 = !DILexicalBlockFile(scope: !1664, file: !665, discriminator: 3)
!1683 = !DILocation(line: 320, column: 76, scope: !1682)
!1684 = !DILocation(line: 320, column: 6, scope: !1685)
!1685 = !DILexicalBlockFile(scope: !1653, file: !665, discriminator: 3)
!1686 = !DILocation(line: 322, column: 6, scope: !1687)
!1687 = distinct !DILexicalBlock(scope: !1653, file: !665, line: 322, column: 6)
!1688 = !{!1689, !1689, i64 0}
!1689 = !{!"bool", !1169, i64 0}
!1690 = !{i8 0, i8 2}
!1691 = !DILocation(line: 322, column: 6, scope: !1653)
!1692 = !DILocation(line: 324, column: 6, scope: !1693)
!1693 = distinct !DILexicalBlock(scope: !1687, file: !665, line: 323, column: 2)
!1694 = !DILocation(line: 325, column: 3, scope: !1695)
!1695 = distinct !DILexicalBlock(scope: !1693, file: !665, line: 324, column: 6)
!1696 = !DILocation(line: 327, column: 3, scope: !1697)
!1697 = distinct !DILexicalBlock(scope: !1695, file: !665, line: 326, column: 11)
!1698 = !DILocation(line: 329, column: 3, scope: !1699)
!1699 = distinct !DILexicalBlock(scope: !1697, file: !665, line: 328, column: 11)
!1700 = !DILocation(line: 331, column: 3, scope: !1699)
!1701 = !DILocation(line: 334, column: 1, scope: !1653)
!1702 = distinct !DISubprogram(name: "cxtprint", linkageName: "_Z8cxtprinti", scope: !665, file: !665, line: 336, type: !377, isLocal: false, isDefinition: true, scopeLine: 337, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1703)
!1703 = !{!1704, !1705}
!1704 = !DILocalVariable(name: "id", arg: 1, scope: !1702, file: !665, line: 336, type: !16)
!1705 = !DILocalVariable(name: "i", scope: !1706, file: !665, line: 344, type: !16)
!1706 = distinct !DILexicalBlock(scope: !1702, file: !665, line: 344, column: 2)
!1707 = !DILocation(line: 336, column: 30, scope: !1702)
!1708 = !DILocation(line: 338, column: 2, scope: !1702)
!1709 = !DILocation(line: 338, column: 2, scope: !1710)
!1710 = !DILexicalBlockFile(scope: !1702, file: !665, discriminator: 2)
!1711 = !DILocation(line: 339, column: 8, scope: !1712)
!1712 = distinct !DILexicalBlock(scope: !1702, file: !665, line: 339, column: 6)
!1713 = !DILocation(line: 339, column: 6, scope: !1702)
!1714 = !DILocation(line: 342, column: 2, scope: !1702)
!1715 = !DILocation(line: 344, column: 11, scope: !1706)
!1716 = !DILocation(line: 344, column: 46, scope: !1717)
!1717 = !DILexicalBlockFile(scope: !1718, file: !665, discriminator: 2)
!1718 = distinct !DILexicalBlock(scope: !1706, file: !665, line: 344, column: 2)
!1719 = !DILocation(line: 344, column: 43, scope: !1720)
!1720 = !DILexicalBlockFile(scope: !1718, file: !665, discriminator: 1)
!1721 = !DILocation(line: 344, column: 64, scope: !1717)
!1722 = !DILocation(line: 344, column: 67, scope: !1717)
!1723 = !DILocation(line: 344, column: 2, scope: !1724)
!1724 = !DILexicalBlockFile(scope: !1706, file: !665, discriminator: 3)
!1725 = !DILocation(line: 346, column: 108, scope: !1726)
!1726 = distinct !DILexicalBlock(scope: !1718, file: !665, line: 345, column: 2)
!1727 = !DILocation(line: 346, column: 90, scope: !1726)
!1728 = !DILocation(line: 346, column: 133, scope: !1726)
!1729 = !DILocation(line: 346, column: 115, scope: !1726)
!1730 = !DILocation(line: 346, column: 3, scope: !1726)
!1731 = !DILocation(line: 344, column: 77, scope: !1732)
!1732 = !DILexicalBlockFile(scope: !1718, file: !665, discriminator: 5)
!1733 = !DILocation(line: 344, column: 19, scope: !1720)
!1734 = distinct !{!1734, !1735, !1736}
!1735 = !DILocation(line: 344, column: 2, scope: !1706)
!1736 = !DILocation(line: 347, column: 2, scope: !1706)
!1737 = !DILocation(line: 350, column: 1, scope: !1702)
!1738 = distinct !DISubprogram(name: "cxtcpy", linkageName: "_Z6cxtcpyP10CallSite_tS0_i", scope: !665, file: !665, line: 353, type: !1739, isLocal: false, isDefinition: true, scopeLine: 354, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1741)
!1739 = !DISubroutineType(types: !1740)
!1740 = !{null, !713, !713, !16}
!1741 = !{!1742, !1743, !1744, !1745}
!1742 = !DILocalVariable(name: "dst", arg: 1, scope: !1738, file: !665, line: 353, type: !713)
!1743 = !DILocalVariable(name: "src", arg: 2, scope: !1738, file: !665, line: 353, type: !713)
!1744 = !DILocalVariable(name: "height", arg: 3, scope: !1738, file: !665, line: 353, type: !16)
!1745 = !DILocalVariable(name: "i", scope: !1738, file: !665, line: 355, type: !16)
!1746 = !DILocation(line: 353, column: 37, scope: !1738)
!1747 = !DILocation(line: 353, column: 54, scope: !1738)
!1748 = !DILocation(line: 353, column: 64, scope: !1738)
!1749 = !DILocation(line: 355, column: 6, scope: !1738)
!1750 = !DILocation(line: 356, column: 13, scope: !1751)
!1751 = !DILexicalBlockFile(scope: !1752, file: !665, discriminator: 1)
!1752 = distinct !DILexicalBlock(scope: !1753, file: !665, line: 356, column: 2)
!1753 = distinct !DILexicalBlock(scope: !1738, file: !665, line: 356, column: 2)
!1754 = !DILocation(line: 356, column: 2, scope: !1755)
!1755 = !DILexicalBlockFile(scope: !1753, file: !665, discriminator: 1)
!1756 = !DILocation(line: 357, column: 12, scope: !1752)
!1757 = !DILocation(line: 357, column: 3, scope: !1752)
!1758 = !DILocation(line: 357, column: 10, scope: !1752)
!1759 = !DILocation(line: 356, column: 24, scope: !1760)
!1760 = !DILexicalBlockFile(scope: !1752, file: !665, discriminator: 2)
!1761 = distinct !{!1761, !1762}
!1762 = !{!"llvm.loop.unroll.disable"}
!1763 = distinct !{!1763, !1764, !1765}
!1764 = !DILocation(line: 356, column: 2, scope: !1753)
!1765 = !DILocation(line: 357, column: 17, scope: !1753)
!1766 = !DILocation(line: 359, column: 2, scope: !1738)
!1767 = !DILocation(line: 359, column: 2, scope: !1768)
!1768 = !DILexicalBlockFile(scope: !1738, file: !665, discriminator: 2)
!1769 = !DILocation(line: 361, column: 2, scope: !1738)
!1770 = !DILocation(line: 361, column: 9, scope: !1738)
!1771 = !DILocation(line: 361, column: 12, scope: !1738)
!1772 = !DILocation(line: 365, column: 1, scope: !1738)
!1773 = distinct !DISubprogram(name: "cxtcmp", linkageName: "_Z6cxtcmpP10CallSite_tS0_i", scope: !665, file: !665, line: 368, type: !1774, isLocal: false, isDefinition: true, scopeLine: 369, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1776)
!1774 = !DISubroutineType(types: !1775)
!1775 = !{!150, !713, !713, !16}
!1776 = !{!1777, !1778, !1779, !1780}
!1777 = !DILocalVariable(name: "dst", arg: 1, scope: !1773, file: !665, line: 368, type: !713)
!1778 = !DILocalVariable(name: "src", arg: 2, scope: !1773, file: !665, line: 368, type: !713)
!1779 = !DILocalVariable(name: "height", arg: 3, scope: !1773, file: !665, line: 368, type: !16)
!1780 = !DILocalVariable(name: "i", scope: !1781, file: !665, line: 370, type: !16)
!1781 = distinct !DILexicalBlock(scope: !1773, file: !665, line: 370, column: 2)
!1782 = !DILocation(line: 368, column: 37, scope: !1773)
!1783 = !DILocation(line: 368, column: 54, scope: !1773)
!1784 = !DILocation(line: 368, column: 63, scope: !1773)
!1785 = !DILocation(line: 370, column: 11, scope: !1781)
!1786 = !DILocation(line: 370, column: 17, scope: !1787)
!1787 = !DILexicalBlockFile(scope: !1788, file: !665, discriminator: 1)
!1788 = distinct !DILexicalBlock(scope: !1781, file: !665, line: 370, column: 2)
!1789 = !DILocation(line: 370, column: 2, scope: !1790)
!1790 = !DILexicalBlockFile(scope: !1781, file: !665, discriminator: 1)
!1791 = !DILocation(line: 371, column: 8, scope: !1792)
!1792 = distinct !DILexicalBlock(scope: !1788, file: !665, line: 371, column: 8)
!1793 = distinct !{!1793, !1794, !1795}
!1794 = !DILocation(line: 370, column: 2, scope: !1781)
!1795 = !DILocation(line: 374, column: 11, scope: !1781)
!1796 = !DILocation(line: 371, column: 15, scope: !1792)
!1797 = !DILocation(line: 371, column: 28, scope: !1792)
!1798 = !DILocation(line: 371, column: 18, scope: !1792)
!1799 = !DILocation(line: 370, column: 28, scope: !1800)
!1800 = !DILexicalBlockFile(scope: !1788, file: !665, discriminator: 3)
!1801 = !DILocation(line: 371, column: 8, scope: !1788)
!1802 = !DILocation(line: 377, column: 1, scope: !1773)
!1803 = distinct !DISubprogram(name: "getContextID", scope: !665, file: !665, line: 380, type: !420, isLocal: false, isDefinition: true, scopeLine: 381, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !59)
!1804 = !DILocation(line: 384, column: 2, scope: !1803)
!1805 = distinct !DISubprogram(name: "passBasicBlock", scope: !665, file: !665, line: 434, type: !1806, isLocal: false, isDefinition: true, scopeLine: 435, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1808)
!1806 = !DISubroutineType(types: !1807)
!1807 = !{null, !357, !16, !16, !16}
!1808 = !{!1809, !1810, !1811, !1812, !1813, !1814, !1815, !1816, !1817, !1818, !1820, !1823}
!1809 = !DILocalVariable(name: "p", arg: 1, scope: !1805, file: !665, line: 434, type: !357)
!1810 = !DILocalVariable(name: "action", arg: 2, scope: !1805, file: !665, line: 434, type: !16)
!1811 = !DILocalVariable(name: "sline", arg: 3, scope: !1805, file: !665, line: 434, type: !16)
!1812 = !DILocalVariable(name: "scolm", arg: 4, scope: !1805, file: !665, line: 434, type: !16)
!1813 = !DILocalVariable(name: "str", scope: !1805, file: !665, line: 446, type: !387)
!1814 = !DILocalVariable(name: "bid", scope: !1805, file: !665, line: 448, type: !16)
!1815 = !DILocalVariable(name: "key", scope: !1805, file: !665, line: 459, type: !487)
!1816 = !DILocalVariable(name: "cnt", scope: !1805, file: !665, line: 460, type: !16)
!1817 = !DILocalVariable(name: "factor", scope: !1805, file: !665, line: 461, type: !77)
!1818 = !DILocalVariable(name: "i", scope: !1819, file: !665, line: 462, type: !16)
!1819 = distinct !DILexicalBlock(scope: !1805, file: !665, line: 462, column: 9)
!1820 = !DILocalVariable(name: "ascii", scope: !1821, file: !665, line: 464, type: !16)
!1821 = distinct !DILexicalBlock(scope: !1822, file: !665, line: 463, column: 2)
!1822 = distinct !DILexicalBlock(scope: !1819, file: !665, line: 462, column: 9)
!1823 = !DILocalVariable(name: "bblog", scope: !1805, file: !665, line: 475, type: !667)
!1824 = !DILocation(line: 434, column: 38, scope: !1805)
!1825 = !DILocation(line: 434, column: 45, scope: !1805)
!1826 = !DILocation(line: 434, column: 57, scope: !1805)
!1827 = !DILocation(line: 434, column: 68, scope: !1805)
!1828 = !DILocation(line: 437, column: 2, scope: !1805)
!1829 = !{!1830, !1830, i64 0}
!1830 = !{!"long long", !1169, i64 0}
!1831 = !DILocation(line: 437, column: 2, scope: !1832)
!1832 = !DILexicalBlockFile(scope: !1805, file: !665, discriminator: 2)
!1833 = !DILocation(line: 78, column: 3, scope: !1668, inlinedAt: !1834)
!1834 = distinct !DILocation(line: 440, column: 8, scope: !1835)
!1835 = distinct !DILexicalBlock(scope: !1805, file: !665, line: 440, column: 7)
!1836 = !DILocation(line: 79, column: 3, scope: !1680, inlinedAt: !1837)
!1837 = distinct !DILocation(line: 440, column: 21, scope: !1838)
!1838 = !DILexicalBlockFile(scope: !1835, file: !665, discriminator: 2)
!1839 = !DILocation(line: 100, column: 3, scope: !1840, inlinedAt: !1882)
!1840 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_xEv", scope: !1841, file: !1096, line: 100, type: !1100, isLocal: false, isDefinition: true, scopeLine: 100, flags: DIFlagPrototyped, isOptimized: true, unit: !664, declaration: !1843, variables: !59)
!1841 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_gridDim_t", file: !1096, line: 99, size: 8, elements: !1842, identifier: "_ZTS24__cuda_builtin_gridDim_t")
!1842 = !{!1843, !1844, !1845, !1846, !1867, !1871, !1875, !1878}
!1843 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_xEv", scope: !1841, file: !1096, line: 100, type: !1100, isLocal: false, isDefinition: false, scopeLine: 100, flags: DIFlagPrototyped, isOptimized: true)
!1844 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_yEv", scope: !1841, file: !1096, line: 101, type: !1100, isLocal: false, isDefinition: false, scopeLine: 101, flags: DIFlagPrototyped, isOptimized: true)
!1845 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_zEv", scope: !1841, file: !1096, line: 102, type: !1100, isLocal: false, isDefinition: false, scopeLine: 102, flags: DIFlagPrototyped, isOptimized: true)
!1846 = !DISubprogram(name: "operator dim3", linkageName: "_ZNK24__cuda_builtin_gridDim_tcv4dim3Ev", scope: !1841, file: !1096, line: 105, type: !1847, isLocal: false, isDefinition: false, scopeLine: 105, flags: DIFlagPrototyped, isOptimized: true)
!1847 = !DISubroutineType(types: !1848)
!1848 = !{!1849, !1865}
!1849 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "dim3", file: !1108, line: 417, size: 96, elements: !1850, identifier: "_ZTS4dim3")
!1850 = !{!1851, !1852, !1853, !1854, !1858, !1862}
!1851 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !1849, file: !1108, line: 419, baseType: !430, size: 32)
!1852 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !1849, file: !1108, line: 419, baseType: !430, size: 32, offset: 32)
!1853 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !1849, file: !1108, line: 419, baseType: !430, size: 32, offset: 64)
!1854 = !DISubprogram(name: "dim3", scope: !1849, file: !1108, line: 421, type: !1855, isLocal: false, isDefinition: false, scopeLine: 421, flags: DIFlagPrototyped, isOptimized: true)
!1855 = !DISubroutineType(types: !1856)
!1856 = !{null, !1857, !430, !430, !430}
!1857 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1849, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1858 = !DISubprogram(name: "dim3", scope: !1849, file: !1108, line: 422, type: !1859, isLocal: false, isDefinition: false, scopeLine: 422, flags: DIFlagPrototyped, isOptimized: true)
!1859 = !DISubroutineType(types: !1860)
!1860 = !{null, !1857, !1861}
!1861 = !DIDerivedType(tag: DW_TAG_typedef, name: "uint3", file: !1108, line: 383, baseType: !1107)
!1862 = !DISubprogram(name: "operator uint3", linkageName: "_ZN4dim3cv5uint3Ev", scope: !1849, file: !1108, line: 423, type: !1863, isLocal: false, isDefinition: false, scopeLine: 423, flags: DIFlagPrototyped, isOptimized: true)
!1863 = !DISubroutineType(types: !1864)
!1864 = !{!1861, !1857}
!1865 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1866, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1866 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !1841)
!1867 = !DISubprogram(name: "__cuda_builtin_gridDim_t", scope: !1841, file: !1096, line: 107, type: !1868, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1868 = !DISubroutineType(types: !1869)
!1869 = !{null, !1870}
!1870 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1841, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!1871 = !DISubprogram(name: "__cuda_builtin_gridDim_t", scope: !1841, file: !1096, line: 107, type: !1872, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1872 = !DISubroutineType(types: !1873)
!1873 = !{null, !1870, !1874}
!1874 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !1866, size: 64)
!1875 = !DISubprogram(name: "operator=", linkageName: "_ZNK24__cuda_builtin_gridDim_taSERKS_", scope: !1841, file: !1096, line: 107, type: !1876, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1876 = !DISubroutineType(types: !1877)
!1877 = !{null, !1865, !1874}
!1878 = !DISubprogram(name: "operator&", linkageName: "_ZNK24__cuda_builtin_gridDim_tadEv", scope: !1841, file: !1096, line: 107, type: !1879, isLocal: false, isDefinition: false, scopeLine: 107, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!1879 = !DISubroutineType(types: !1880)
!1880 = !{!1881, !1865}
!1881 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !1841, size: 64)
!1882 = distinct !DILocation(line: 440, column: 32, scope: !1883)
!1883 = !DILexicalBlockFile(scope: !1835, file: !665, discriminator: 3)
!1884 = !{i32 1, i32 65536}
!1885 = !DILocation(line: 440, column: 31, scope: !1835)
!1886 = !DILocation(line: 440, column: 19, scope: !1835)
!1887 = !DILocation(line: 440, column: 45, scope: !1835)
!1888 = !DILocation(line: 440, column: 43, scope: !1835)
!1889 = !DILocation(line: 440, column: 51, scope: !1835)
!1890 = !DILocation(line: 440, column: 92, scope: !1891)
!1891 = !DILexicalBlockFile(scope: !1835, file: !665, discriminator: 1)
!1892 = !DILocation(line: 440, column: 90, scope: !1891)
!1893 = !DILocation(line: 440, column: 7, scope: !1894)
!1894 = !DILexicalBlockFile(scope: !1805, file: !665, discriminator: 1)
!1895 = !DILocation(line: 446, column: 8, scope: !1805)
!1896 = !DILocalVariable(name: "val", arg: 2, scope: !1897, file: !1898, line: 201, type: !487)
!1897 = distinct !DISubprogram(name: "atomicAdd", linkageName: "_ZL9atomicAddPyy", scope: !1898, file: !1898, line: 201, type: !1899, isLocal: true, isDefinition: true, scopeLine: 202, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1902)
!1898 = !DIFile(filename: "/usr/local/cuda/include/device_atomic_functions.hpp", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!1899 = !DISubroutineType(types: !1900)
!1900 = !{!487, !1901, !487}
!1901 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !487, size: 64)
!1902 = !{!1903, !1896}
!1903 = !DILocalVariable(name: "address", arg: 1, scope: !1897, file: !1898, line: 201, type: !1901)
!1904 = !DILocation(line: 201, column: 123, scope: !1897, inlinedAt: !1905)
!1905 = distinct !DILocation(line: 448, column: 12, scope: !1805)
!1906 = !DILocalVariable(name: "val", arg: 2, scope: !1907, file: !528, line: 1524, type: !487)
!1907 = distinct !DISubprogram(name: "__ullAtomicAdd", linkageName: "_ZL14__ullAtomicAddPyy", scope: !528, file: !528, line: 1524, type: !1899, isLocal: true, isDefinition: true, scopeLine: 1525, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1908)
!1908 = !{!1909, !1906}
!1909 = !DILocalVariable(name: "p", arg: 1, scope: !1907, file: !528, line: 1524, type: !1901)
!1910 = !DILocation(line: 1524, column: 77, scope: !1907, inlinedAt: !1911)
!1911 = distinct !DILocation(line: 203, column: 10, scope: !1897, inlinedAt: !1905)
!1912 = !DILocation(line: 1526, column: 10, scope: !1907, inlinedAt: !1911)
!1913 = !DILocation(line: 459, column: 21, scope: !1805)
!1914 = !DILocation(line: 460, column: 6, scope: !1805)
!1915 = !DILocation(line: 461, column: 12, scope: !1805)
!1916 = !DILocation(line: 462, column: 17, scope: !1819)
!1917 = !DILocation(line: 462, column: 23, scope: !1918)
!1918 = !DILexicalBlockFile(scope: !1822, file: !665, discriminator: 1)
!1919 = !DILocation(line: 462, column: 32, scope: !1918)
!1920 = !DILocation(line: 462, column: 9, scope: !1921)
!1921 = !DILexicalBlockFile(scope: !1819, file: !665, discriminator: 1)
!1922 = !DILocation(line: 465, column: 16, scope: !1923)
!1923 = distinct !DILexicalBlock(scope: !1821, file: !665, line: 465, column: 7)
!1924 = !DILocation(line: 475, column: 11, scope: !1805)
!1925 = !DILocation(line: 476, column: 2, scope: !1805)
!1926 = !DILocation(line: 476, column: 13, scope: !1805)
!1927 = !DILocation(line: 476, column: 17, scope: !1805)
!1928 = !{!1929, !1830, i64 8}
!1929 = !{!"_ZTS7BBlog_t", !1459, i64 0, !1459, i64 2, !1459, i64 4, !1459, i64 6, !1830, i64 8, !1168, i64 16, !1168, i64 20, !1168, i64 24}
!1930 = !DILocation(line: 67, column: 3, scope: !1662, inlinedAt: !1931)
!1931 = distinct !DILocation(line: 478, column: 27, scope: !1805)
!1932 = !DILocation(line: 478, column: 27, scope: !1805)
!1933 = !DILocation(line: 478, column: 20, scope: !1805)
!1934 = !DILocation(line: 478, column: 25, scope: !1805)
!1935 = !{!1929, !1459, i64 4}
!1936 = !DILocation(line: 68, column: 3, scope: !1674, inlinedAt: !1937)
!1937 = distinct !DILocation(line: 479, column: 27, scope: !1805)
!1938 = !DILocation(line: 479, column: 27, scope: !1805)
!1939 = !DILocation(line: 479, column: 20, scope: !1805)
!1940 = !DILocation(line: 479, column: 25, scope: !1805)
!1941 = !{!1929, !1459, i64 6}
!1942 = !DILocation(line: 480, column: 27, scope: !1805)
!1943 = !DILocation(line: 480, column: 20, scope: !1805)
!1944 = !DILocation(line: 480, column: 25, scope: !1805)
!1945 = !{!1929, !1459, i64 0}
!1946 = !DILocation(line: 481, column: 27, scope: !1805)
!1947 = !DILocation(line: 481, column: 20, scope: !1805)
!1948 = !DILocation(line: 481, column: 25, scope: !1805)
!1949 = !{!1929, !1459, i64 2}
!1950 = !DILocation(line: 482, column: 20, scope: !1805)
!1951 = !DILocation(line: 482, column: 26, scope: !1805)
!1952 = !{!1929, !1168, i64 16}
!1953 = !DILocation(line: 483, column: 20, scope: !1805)
!1954 = !DILocation(line: 483, column: 26, scope: !1805)
!1955 = !{!1929, !1168, i64 20}
!1956 = !DILocation(line: 486, column: 13, scope: !1805)
!1957 = !DILocation(line: 486, column: 17, scope: !1805)
!1958 = !{!1929, !1168, i64 24}
!1959 = !DILocation(line: 467, column: 10, scope: !1821)
!1960 = !DILocation(line: 467, column: 15, scope: !1821)
!1961 = !DILocation(line: 467, column: 7, scope: !1821)
!1962 = !DILocation(line: 468, column: 13, scope: !1821)
!1963 = !DILocation(line: 468, column: 10, scope: !1821)
!1964 = !DILocation(line: 471, column: 9, scope: !1822)
!1965 = !DILocation(line: 462, column: 39, scope: !1966)
!1966 = !DILexicalBlockFile(scope: !1822, file: !665, discriminator: 3)
!1967 = !DILocation(line: 462, column: 28, scope: !1918)
!1968 = distinct !{!1968, !1969, !1970}
!1969 = !DILocation(line: 462, column: 9, scope: !1819)
!1970 = !DILocation(line: 471, column: 9, scope: !1819)
!1971 = !DILocation(line: 493, column: 1, scope: !1894)
!1972 = distinct !DISubprogram(name: "storeLines", linkageName: "_Z10storeLinesPvssss", scope: !665, file: !665, line: 496, type: !1973, isLocal: false, isDefinition: true, scopeLine: 497, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !1975)
!1973 = !DISubroutineType(types: !1974)
!1974 = !{null, !357, !673, !673, !673, !673}
!1975 = !{!1976, !1977, !1978, !1979, !1980, !1981, !1982, !1983}
!1976 = !DILocalVariable(name: "p", arg: 1, scope: !1972, file: !665, line: 496, type: !357)
!1977 = !DILocalVariable(name: "size", arg: 2, scope: !1972, file: !665, line: 496, type: !673)
!1978 = !DILocalVariable(name: "line", arg: 3, scope: !1972, file: !665, line: 496, type: !673)
!1979 = !DILocalVariable(name: "colmn", arg: 4, scope: !1972, file: !665, line: 496, type: !673)
!1980 = !DILocalVariable(name: "op", arg: 5, scope: !1972, file: !665, line: 496, type: !673)
!1981 = !DILocalVariable(name: "bid", scope: !1972, file: !665, line: 500, type: !16)
!1982 = !DILocalVariable(name: "buffer_oN_DeViCe_short", scope: !1972, file: !665, line: 508, type: !681)
!1983 = !DILocalVariable(name: "buffer_oN_DeViCe_long", scope: !1972, file: !665, line: 509, type: !682)
!1984 = !DILocation(line: 496, column: 34, scope: !1972)
!1985 = !DILocation(line: 496, column: 43, scope: !1972)
!1986 = !DILocation(line: 496, column: 64, scope: !1972)
!1987 = !DILocation(line: 496, column: 76, scope: !1972)
!1988 = !DILocation(line: 496, column: 89, scope: !1972)
!1989 = !DILocation(line: 498, column: 2, scope: !1972)
!1990 = !DILocation(line: 498, column: 2, scope: !1991)
!1991 = !DILexicalBlockFile(scope: !1972, file: !665, discriminator: 2)
!1992 = !DILocation(line: 201, column: 123, scope: !1897, inlinedAt: !1993)
!1993 = distinct !DILocation(line: 500, column: 12, scope: !1972)
!1994 = !DILocation(line: 1524, column: 77, scope: !1907, inlinedAt: !1995)
!1995 = distinct !DILocation(line: 203, column: 10, scope: !1897, inlinedAt: !1993)
!1996 = !DILocation(line: 1526, column: 10, scope: !1907, inlinedAt: !1995)
!1997 = !DILocation(line: 500, column: 12, scope: !1972)
!1998 = !DILocation(line: 500, column: 6, scope: !1972)
!1999 = !DILocation(line: 508, column: 9, scope: !1972)
!2000 = !DILocation(line: 509, column: 8, scope: !1972)
!2001 = !DILocation(line: 78, column: 3, scope: !1668, inlinedAt: !2002)
!2002 = distinct !DILocation(line: 511, column: 37, scope: !1972)
!2003 = !DILocation(line: 511, column: 37, scope: !1972)
!2004 = !DILocation(line: 511, column: 28, scope: !1972)
!2005 = !DILocation(line: 511, column: 2, scope: !1972)
!2006 = !DILocation(line: 511, column: 35, scope: !1972)
!2007 = !{!1459, !1459, i64 0}
!2008 = !DILocation(line: 79, column: 3, scope: !1680, inlinedAt: !2009)
!2009 = distinct !DILocation(line: 512, column: 37, scope: !1972)
!2010 = !DILocation(line: 512, column: 37, scope: !1972)
!2011 = !DILocation(line: 512, column: 31, scope: !1972)
!2012 = !DILocation(line: 512, column: 2, scope: !1972)
!2013 = !DILocation(line: 512, column: 35, scope: !1972)
!2014 = !DILocation(line: 67, column: 3, scope: !1662, inlinedAt: !2015)
!2015 = distinct !DILocation(line: 513, column: 37, scope: !1972)
!2016 = !DILocation(line: 513, column: 37, scope: !1972)
!2017 = !DILocation(line: 513, column: 31, scope: !1972)
!2018 = !DILocation(line: 513, column: 2, scope: !1972)
!2019 = !DILocation(line: 513, column: 35, scope: !1972)
!2020 = !DILocation(line: 68, column: 3, scope: !1674, inlinedAt: !2021)
!2021 = distinct !DILocation(line: 514, column: 37, scope: !1972)
!2022 = !DILocation(line: 514, column: 37, scope: !1972)
!2023 = !DILocation(line: 514, column: 31, scope: !1972)
!2024 = !DILocation(line: 514, column: 2, scope: !1972)
!2025 = !DILocation(line: 514, column: 35, scope: !1972)
!2026 = !DILocation(line: 515, column: 35, scope: !1972)
!2027 = !DILocation(line: 515, column: 2, scope: !1972)
!2028 = !DILocation(line: 515, column: 33, scope: !1972)
!2029 = !{!1171, !1171, i64 0}
!2030 = !DILocation(line: 516, column: 31, scope: !1972)
!2031 = !DILocation(line: 516, column: 2, scope: !1972)
!2032 = !DILocation(line: 516, column: 35, scope: !1972)
!2033 = !DILocation(line: 517, column: 31, scope: !1972)
!2034 = !DILocation(line: 517, column: 2, scope: !1972)
!2035 = !DILocation(line: 517, column: 35, scope: !1972)
!2036 = !DILocation(line: 518, column: 31, scope: !1972)
!2037 = !DILocation(line: 518, column: 2, scope: !1972)
!2038 = !DILocation(line: 518, column: 36, scope: !1972)
!2039 = !DILocation(line: 519, column: 31, scope: !1972)
!2040 = !DILocation(line: 519, column: 2, scope: !1972)
!2041 = !DILocation(line: 519, column: 36, scope: !1972)
!2042 = !DILocation(line: 526, column: 10, scope: !2043)
!2043 = distinct !DILexicalBlock(scope: !1972, file: !665, line: 526, column: 6)
!2044 = !DILocation(line: 526, column: 6, scope: !1972)
!2045 = !DILocation(line: 527, column: 3, scope: !2043)
!2046 = !DILocation(line: 531, column: 1, scope: !1972)
!2047 = distinct !DISubprogram(name: "dumpLines", linkageName: "_Z9dumpLinesv", scope: !665, file: !665, line: 533, type: !330, isLocal: false, isDefinition: true, scopeLine: 534, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !2048)
!2048 = !{!2049}
!2049 = !DILocalVariable(name: "ii", scope: !2047, file: !665, line: 537, type: !16)
!2050 = !DILocation(line: 67, column: 3, scope: !1662, inlinedAt: !2051)
!2051 = distinct !DILocation(line: 535, column: 6, scope: !2052)
!2052 = distinct !DILexicalBlock(scope: !2047, file: !665, line: 535, column: 6)
!2053 = !DILocation(line: 535, column: 18, scope: !2052)
!2054 = !DILocation(line: 535, column: 23, scope: !2052)
!2055 = !DILocation(line: 78, column: 3, scope: !1668, inlinedAt: !2056)
!2056 = distinct !DILocation(line: 535, column: 26, scope: !2057)
!2057 = !DILexicalBlockFile(scope: !2052, file: !665, discriminator: 1)
!2058 = !DILocation(line: 535, column: 37, scope: !2057)
!2059 = !DILocation(line: 535, column: 42, scope: !2057)
!2060 = !DILocation(line: 68, column: 3, scope: !1674, inlinedAt: !2061)
!2061 = distinct !DILocation(line: 535, column: 45, scope: !2062)
!2062 = !DILexicalBlockFile(scope: !2052, file: !665, discriminator: 2)
!2063 = !DILocation(line: 535, column: 57, scope: !2062)
!2064 = !DILocation(line: 535, column: 62, scope: !2062)
!2065 = !DILocation(line: 79, column: 3, scope: !1680, inlinedAt: !2066)
!2066 = distinct !DILocation(line: 535, column: 65, scope: !2067)
!2067 = !DILexicalBlockFile(scope: !2052, file: !665, discriminator: 3)
!2068 = !DILocation(line: 535, column: 76, scope: !2067)
!2069 = !DILocation(line: 535, column: 6, scope: !2070)
!2070 = !DILexicalBlockFile(scope: !2047, file: !665, discriminator: 3)
!2071 = !DILocation(line: 542, column: 2, scope: !2047)
!2072 = !DILocation(line: 553, column: 1, scope: !2047)
!2073 = !DILocation(line: 553, column: 1, scope: !2074)
!2074 = !DILexicalBlockFile(scope: !2047, file: !665, discriminator: 1)
!2075 = distinct !DISubprogram(name: "print1", linkageName: "_Z6print1i", scope: !665, file: !665, line: 555, type: !377, isLocal: false, isDefinition: true, scopeLine: 556, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !2076)
!2076 = !{!2077}
!2077 = !DILocalVariable(name: "a", arg: 1, scope: !2075, file: !665, line: 555, type: !16)
!2078 = !DILocation(line: 555, column: 28, scope: !2075)
!2079 = !DILocation(line: 67, column: 3, scope: !1662, inlinedAt: !2080)
!2080 = distinct !DILocation(line: 557, column: 6, scope: !2081)
!2081 = distinct !DILexicalBlock(scope: !2075, file: !665, line: 557, column: 6)
!2082 = !DILocation(line: 68, column: 3, scope: !1674, inlinedAt: !2083)
!2083 = distinct !DILocation(line: 557, column: 20, scope: !2084)
!2084 = !DILexicalBlockFile(scope: !2081, file: !665, discriminator: 2)
!2085 = !DILocation(line: 557, column: 18, scope: !2081)
!2086 = !DILocation(line: 78, column: 3, scope: !1668, inlinedAt: !2087)
!2087 = distinct !DILocation(line: 557, column: 34, scope: !2088)
!2088 = !DILexicalBlockFile(scope: !2081, file: !665, discriminator: 3)
!2089 = !DILocation(line: 557, column: 32, scope: !2081)
!2090 = !DILocation(line: 79, column: 3, scope: !1680, inlinedAt: !2091)
!2091 = distinct !DILocation(line: 557, column: 47, scope: !2092)
!2092 = !DILexicalBlockFile(scope: !2081, file: !665, discriminator: 4)
!2093 = !DILocation(line: 557, column: 58, scope: !2081)
!2094 = !DILocation(line: 557, column: 63, scope: !2081)
!2095 = !DILocation(line: 557, column: 66, scope: !2096)
!2096 = !DILexicalBlockFile(scope: !2081, file: !665, discriminator: 1)
!2097 = !DILocation(line: 557, column: 6, scope: !2098)
!2098 = !DILexicalBlockFile(scope: !2075, file: !665, discriminator: 1)
!2099 = !DILocation(line: 559, column: 7, scope: !2100)
!2100 = distinct !DILexicalBlock(scope: !2081, file: !665, line: 558, column: 2)
!2101 = !DILocation(line: 560, column: 4, scope: !2102)
!2102 = distinct !DILexicalBlock(scope: !2100, file: !665, line: 559, column: 7)
!2103 = !DILocation(line: 560, column: 4, scope: !2104)
!2104 = !DILexicalBlockFile(scope: !2102, file: !665, discriminator: 2)
!2105 = !DILocation(line: 562, column: 4, scope: !2106)
!2106 = distinct !DILexicalBlock(scope: !2102, file: !665, line: 561, column: 12)
!2107 = !DILocation(line: 562, column: 4, scope: !2108)
!2108 = !DILexicalBlockFile(scope: !2106, file: !665, discriminator: 2)
!2109 = !DILocation(line: 564, column: 4, scope: !2106)
!2110 = !DILocation(line: 566, column: 1, scope: !2075)
!2111 = distinct !DISubprogram(name: "print2", linkageName: "_Z6print2v", scope: !665, file: !665, line: 568, type: !330, isLocal: false, isDefinition: true, scopeLine: 569, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !59)
!2112 = !DILocation(line: 67, column: 3, scope: !1662, inlinedAt: !2113)
!2113 = distinct !DILocation(line: 570, column: 6, scope: !2114)
!2114 = distinct !DILexicalBlock(scope: !2111, file: !665, line: 570, column: 6)
!2115 = !DILocation(line: 68, column: 3, scope: !1674, inlinedAt: !2116)
!2116 = distinct !DILocation(line: 570, column: 20, scope: !2117)
!2117 = !DILexicalBlockFile(scope: !2114, file: !665, discriminator: 2)
!2118 = !DILocation(line: 570, column: 18, scope: !2114)
!2119 = !DILocation(line: 78, column: 3, scope: !1668, inlinedAt: !2120)
!2120 = distinct !DILocation(line: 570, column: 34, scope: !2121)
!2121 = !DILexicalBlockFile(scope: !2114, file: !665, discriminator: 3)
!2122 = !DILocation(line: 570, column: 32, scope: !2114)
!2123 = !DILocation(line: 79, column: 3, scope: !1680, inlinedAt: !2124)
!2124 = distinct !DILocation(line: 570, column: 47, scope: !2125)
!2125 = !DILexicalBlockFile(scope: !2114, file: !665, discriminator: 4)
!2126 = !DILocation(line: 570, column: 58, scope: !2114)
!2127 = !DILocation(line: 570, column: 63, scope: !2114)
!2128 = !DILocation(line: 570, column: 66, scope: !2129)
!2129 = !DILexicalBlockFile(scope: !2114, file: !665, discriminator: 1)
!2130 = !DILocation(line: 570, column: 6, scope: !2131)
!2131 = !DILexicalBlockFile(scope: !2111, file: !665, discriminator: 1)
!2132 = !DILocation(line: 571, column: 10, scope: !2114)
!2133 = !DILocation(line: 571, column: 10, scope: !2117)
!2134 = !DILocation(line: 572, column: 1, scope: !2111)
!2135 = distinct !DISubprogram(name: "print3", linkageName: "_Z6print3ii", scope: !665, file: !665, line: 574, type: !2136, isLocal: false, isDefinition: true, scopeLine: 575, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !2138)
!2136 = !DISubroutineType(types: !2137)
!2137 = !{null, !16, !16}
!2138 = !{!2139, !2140}
!2139 = !DILocalVariable(name: "line", arg: 1, scope: !2135, file: !665, line: 574, type: !16)
!2140 = !DILocalVariable(name: "col", arg: 2, scope: !2135, file: !665, line: 574, type: !16)
!2141 = !DILocation(line: 574, column: 28, scope: !2135)
!2142 = !DILocation(line: 574, column: 38, scope: !2135)
!2143 = !DILocation(line: 67, column: 3, scope: !1662, inlinedAt: !2144)
!2144 = distinct !DILocation(line: 576, column: 6, scope: !2145)
!2145 = distinct !DILexicalBlock(scope: !2135, file: !665, line: 576, column: 6)
!2146 = !DILocation(line: 68, column: 3, scope: !1674, inlinedAt: !2147)
!2147 = distinct !DILocation(line: 576, column: 20, scope: !2148)
!2148 = !DILexicalBlockFile(scope: !2145, file: !665, discriminator: 2)
!2149 = !DILocation(line: 576, column: 18, scope: !2145)
!2150 = !DILocation(line: 78, column: 3, scope: !1668, inlinedAt: !2151)
!2151 = distinct !DILocation(line: 576, column: 34, scope: !2152)
!2152 = !DILexicalBlockFile(scope: !2145, file: !665, discriminator: 3)
!2153 = !DILocation(line: 576, column: 32, scope: !2145)
!2154 = !DILocation(line: 79, column: 3, scope: !1680, inlinedAt: !2155)
!2155 = distinct !DILocation(line: 576, column: 47, scope: !2156)
!2156 = !DILexicalBlockFile(scope: !2145, file: !665, discriminator: 4)
!2157 = !DILocation(line: 576, column: 58, scope: !2145)
!2158 = !DILocation(line: 576, column: 63, scope: !2145)
!2159 = !DILocation(line: 576, column: 66, scope: !2160)
!2160 = !DILexicalBlockFile(scope: !2145, file: !665, discriminator: 1)
!2161 = !DILocation(line: 576, column: 6, scope: !2162)
!2162 = !DILexicalBlockFile(scope: !2135, file: !665, discriminator: 1)
!2163 = !DILocation(line: 577, column: 10, scope: !2145)
!2164 = !DILocation(line: 577, column: 10, scope: !2148)
!2165 = !DILocation(line: 578, column: 1, scope: !2135)
!2166 = distinct !DISubprogram(name: "print5", scope: !665, file: !665, line: 580, type: !2167, isLocal: false, isDefinition: true, scopeLine: 581, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !2169)
!2167 = !DISubroutineType(types: !2168)
!2168 = !{null, !357, !16, !16, !16, !16}
!2169 = !{!2170, !2171, !2172, !2173, !2174}
!2170 = !DILocalVariable(name: "p", arg: 1, scope: !2166, file: !665, line: 580, type: !357)
!2171 = !DILocalVariable(name: "bits", arg: 2, scope: !2166, file: !665, line: 580, type: !16)
!2172 = !DILocalVariable(name: "sline", arg: 3, scope: !2166, file: !665, line: 580, type: !16)
!2173 = !DILocalVariable(name: "scolm", arg: 4, scope: !2166, file: !665, line: 580, type: !16)
!2174 = !DILocalVariable(name: "op", arg: 5, scope: !2166, file: !665, line: 580, type: !16)
!2175 = !DILocation(line: 580, column: 30, scope: !2166)
!2176 = !DILocation(line: 580, column: 37, scope: !2166)
!2177 = !DILocation(line: 580, column: 47, scope: !2166)
!2178 = !DILocation(line: 580, column: 58, scope: !2166)
!2179 = !DILocation(line: 580, column: 69, scope: !2166)
!2180 = !DILocation(line: 78, column: 3, scope: !1668, inlinedAt: !2181)
!2181 = distinct !DILocation(line: 587, column: 8, scope: !2182)
!2182 = distinct !DILexicalBlock(scope: !2166, file: !665, line: 587, column: 7)
!2183 = !DILocation(line: 79, column: 3, scope: !1680, inlinedAt: !2184)
!2184 = distinct !DILocation(line: 587, column: 21, scope: !2185)
!2185 = !DILexicalBlockFile(scope: !2182, file: !665, discriminator: 2)
!2186 = !DILocation(line: 100, column: 3, scope: !1840, inlinedAt: !2187)
!2187 = distinct !DILocation(line: 587, column: 32, scope: !2188)
!2188 = !DILexicalBlockFile(scope: !2182, file: !665, discriminator: 3)
!2189 = !DILocation(line: 587, column: 31, scope: !2182)
!2190 = !DILocation(line: 587, column: 19, scope: !2182)
!2191 = !DILocation(line: 587, column: 45, scope: !2182)
!2192 = !DILocation(line: 587, column: 43, scope: !2182)
!2193 = !DILocation(line: 587, column: 51, scope: !2182)
!2194 = !DILocation(line: 587, column: 92, scope: !2195)
!2195 = !DILexicalBlockFile(scope: !2182, file: !665, discriminator: 1)
!2196 = !DILocation(line: 587, column: 90, scope: !2195)
!2197 = !DILocation(line: 587, column: 7, scope: !2198)
!2198 = !DILexicalBlockFile(scope: !2166, file: !665, discriminator: 1)
!2199 = !DILocation(line: 496, column: 34, scope: !1972, inlinedAt: !2200)
!2200 = distinct !DILocation(line: 590, column: 9, scope: !2166)
!2201 = !DILocation(line: 496, column: 43, scope: !1972, inlinedAt: !2200)
!2202 = !DILocation(line: 496, column: 64, scope: !1972, inlinedAt: !2200)
!2203 = !DILocation(line: 496, column: 76, scope: !1972, inlinedAt: !2200)
!2204 = !DILocation(line: 496, column: 89, scope: !1972, inlinedAt: !2200)
!2205 = !DILocation(line: 498, column: 2, scope: !1972, inlinedAt: !2200)
!2206 = !DILocation(line: 498, column: 2, scope: !1991, inlinedAt: !2200)
!2207 = !DILocation(line: 590, column: 76, scope: !2166)
!2208 = !DILocation(line: 590, column: 62, scope: !2166)
!2209 = !DILocation(line: 590, column: 47, scope: !2166)
!2210 = !DILocation(line: 590, column: 35, scope: !2166)
!2211 = !DILocation(line: 590, column: 30, scope: !2166)
!2212 = !DILocation(line: 201, column: 123, scope: !1897, inlinedAt: !2213)
!2213 = distinct !DILocation(line: 500, column: 12, scope: !1972, inlinedAt: !2200)
!2214 = !DILocation(line: 1524, column: 77, scope: !1907, inlinedAt: !2215)
!2215 = distinct !DILocation(line: 203, column: 10, scope: !1897, inlinedAt: !2213)
!2216 = !DILocation(line: 1526, column: 10, scope: !1907, inlinedAt: !2215)
!2217 = !DILocation(line: 500, column: 12, scope: !1972, inlinedAt: !2200)
!2218 = !DILocation(line: 500, column: 6, scope: !1972, inlinedAt: !2200)
!2219 = !DILocation(line: 508, column: 9, scope: !1972, inlinedAt: !2200)
!2220 = !DILocation(line: 509, column: 8, scope: !1972, inlinedAt: !2200)
!2221 = !DILocation(line: 511, column: 37, scope: !1972, inlinedAt: !2200)
!2222 = !DILocation(line: 511, column: 28, scope: !1972, inlinedAt: !2200)
!2223 = !DILocation(line: 511, column: 2, scope: !1972, inlinedAt: !2200)
!2224 = !DILocation(line: 511, column: 35, scope: !1972, inlinedAt: !2200)
!2225 = !DILocation(line: 512, column: 37, scope: !1972, inlinedAt: !2200)
!2226 = !DILocation(line: 512, column: 31, scope: !1972, inlinedAt: !2200)
!2227 = !DILocation(line: 512, column: 2, scope: !1972, inlinedAt: !2200)
!2228 = !DILocation(line: 512, column: 35, scope: !1972, inlinedAt: !2200)
!2229 = !DILocation(line: 67, column: 3, scope: !1662, inlinedAt: !2230)
!2230 = distinct !DILocation(line: 513, column: 37, scope: !1972, inlinedAt: !2200)
!2231 = !DILocation(line: 513, column: 37, scope: !1972, inlinedAt: !2200)
!2232 = !DILocation(line: 513, column: 31, scope: !1972, inlinedAt: !2200)
!2233 = !DILocation(line: 513, column: 2, scope: !1972, inlinedAt: !2200)
!2234 = !DILocation(line: 513, column: 35, scope: !1972, inlinedAt: !2200)
!2235 = !DILocation(line: 68, column: 3, scope: !1674, inlinedAt: !2236)
!2236 = distinct !DILocation(line: 514, column: 37, scope: !1972, inlinedAt: !2200)
!2237 = !DILocation(line: 514, column: 37, scope: !1972, inlinedAt: !2200)
!2238 = !DILocation(line: 514, column: 31, scope: !1972, inlinedAt: !2200)
!2239 = !DILocation(line: 514, column: 2, scope: !1972, inlinedAt: !2200)
!2240 = !DILocation(line: 514, column: 35, scope: !1972, inlinedAt: !2200)
!2241 = !DILocation(line: 515, column: 35, scope: !1972, inlinedAt: !2200)
!2242 = !DILocation(line: 515, column: 2, scope: !1972, inlinedAt: !2200)
!2243 = !DILocation(line: 515, column: 33, scope: !1972, inlinedAt: !2200)
!2244 = !DILocation(line: 516, column: 31, scope: !1972, inlinedAt: !2200)
!2245 = !DILocation(line: 516, column: 2, scope: !1972, inlinedAt: !2200)
!2246 = !DILocation(line: 516, column: 35, scope: !1972, inlinedAt: !2200)
!2247 = !DILocation(line: 517, column: 31, scope: !1972, inlinedAt: !2200)
!2248 = !DILocation(line: 517, column: 2, scope: !1972, inlinedAt: !2200)
!2249 = !DILocation(line: 517, column: 35, scope: !1972, inlinedAt: !2200)
!2250 = !DILocation(line: 518, column: 31, scope: !1972, inlinedAt: !2200)
!2251 = !DILocation(line: 518, column: 2, scope: !1972, inlinedAt: !2200)
!2252 = !DILocation(line: 518, column: 36, scope: !1972, inlinedAt: !2200)
!2253 = !DILocation(line: 519, column: 31, scope: !1972, inlinedAt: !2200)
!2254 = !DILocation(line: 519, column: 2, scope: !1972, inlinedAt: !2200)
!2255 = !DILocation(line: 519, column: 36, scope: !1972, inlinedAt: !2200)
!2256 = !DILocation(line: 526, column: 10, scope: !2043, inlinedAt: !2200)
!2257 = !DILocation(line: 526, column: 6, scope: !1972, inlinedAt: !2200)
!2258 = !DILocation(line: 527, column: 3, scope: !2043, inlinedAt: !2200)
!2259 = !DILocation(line: 531, column: 1, scope: !1972, inlinedAt: !2200)
!2260 = !DILocation(line: 594, column: 1, scope: !2166)
!2261 = !DILocation(line: 594, column: 1, scope: !2198)
!2262 = distinct !DISubprogram(name: "RetKernel", scope: !665, file: !665, line: 597, type: !330, isLocal: false, isDefinition: true, scopeLine: 598, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !2263)
!2263 = !{!2264, !2269, !2270, !2271, !2272}
!2264 = !DILocalVariable(name: "buffer_oN_DeViCe_short", scope: !2265, file: !665, line: 603, type: !681)
!2265 = distinct !DILexicalBlock(scope: !2266, file: !665, line: 602, column: 3)
!2266 = distinct !DILexicalBlock(scope: !2267, file: !665, line: 601, column: 7)
!2267 = distinct !DILexicalBlock(scope: !2268, file: !665, line: 600, column: 2)
!2268 = distinct !DILexicalBlock(scope: !2262, file: !665, line: 599, column: 6)
!2269 = !DILocalVariable(name: "buffer_oN_DeViCe_long", scope: !2265, file: !665, line: 611, type: !682)
!2270 = !DILocalVariable(name: "offset1", scope: !2267, file: !665, line: 643, type: !362)
!2271 = !DILocalVariable(name: "offset2", scope: !2267, file: !665, line: 644, type: !362)
!2272 = !DILocalVariable(name: "ptr", scope: !2267, file: !665, line: 654, type: !357)
!2273 = !DILocation(line: 67, column: 3, scope: !1662, inlinedAt: !2274)
!2274 = distinct !DILocation(line: 599, column: 6, scope: !2268)
!2275 = !DILocation(line: 68, column: 3, scope: !1674, inlinedAt: !2276)
!2276 = distinct !DILocation(line: 599, column: 20, scope: !2277)
!2277 = !DILexicalBlockFile(scope: !2268, file: !665, discriminator: 1)
!2278 = !DILocation(line: 599, column: 18, scope: !2268)
!2279 = !DILocation(line: 78, column: 3, scope: !1668, inlinedAt: !2280)
!2280 = distinct !DILocation(line: 599, column: 34, scope: !2281)
!2281 = !DILexicalBlockFile(scope: !2268, file: !665, discriminator: 2)
!2282 = !DILocation(line: 599, column: 32, scope: !2268)
!2283 = !DILocation(line: 79, column: 3, scope: !1680, inlinedAt: !2284)
!2284 = distinct !DILocation(line: 599, column: 47, scope: !2285)
!2285 = !DILexicalBlockFile(scope: !2268, file: !665, discriminator: 3)
!2286 = !DILocation(line: 599, column: 58, scope: !2268)
!2287 = !DILocation(line: 599, column: 6, scope: !2262)
!2288 = !DILocation(line: 603, column: 11, scope: !2265)
!2289 = !DILocation(line: 89, column: 3, scope: !2290, inlinedAt: !2316)
!2290 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_xEv", scope: !2291, file: !1096, line: 89, type: !1100, isLocal: false, isDefinition: true, scopeLine: 89, flags: DIFlagPrototyped, isOptimized: true, unit: !664, declaration: !2293, variables: !59)
!2291 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_blockDim_t", file: !1096, line: 88, size: 8, elements: !2292, identifier: "_ZTS25__cuda_builtin_blockDim_t")
!2292 = !{!2293, !2294, !2295, !2296, !2301, !2305, !2309, !2312}
!2293 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_xEv", scope: !2291, file: !1096, line: 89, type: !1100, isLocal: false, isDefinition: false, scopeLine: 89, flags: DIFlagPrototyped, isOptimized: true)
!2294 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_yEv", scope: !2291, file: !1096, line: 90, type: !1100, isLocal: false, isDefinition: false, scopeLine: 90, flags: DIFlagPrototyped, isOptimized: true)
!2295 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_zEv", scope: !2291, file: !1096, line: 91, type: !1100, isLocal: false, isDefinition: false, scopeLine: 91, flags: DIFlagPrototyped, isOptimized: true)
!2296 = !DISubprogram(name: "operator dim3", linkageName: "_ZNK25__cuda_builtin_blockDim_tcv4dim3Ev", scope: !2291, file: !1096, line: 94, type: !2297, isLocal: false, isDefinition: false, scopeLine: 94, flags: DIFlagPrototyped, isOptimized: true)
!2297 = !DISubroutineType(types: !2298)
!2298 = !{!1849, !2299}
!2299 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !2300, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!2300 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !2291)
!2301 = !DISubprogram(name: "__cuda_builtin_blockDim_t", scope: !2291, file: !1096, line: 96, type: !2302, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!2302 = !DISubroutineType(types: !2303)
!2303 = !{null, !2304}
!2304 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !2291, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!2305 = !DISubprogram(name: "__cuda_builtin_blockDim_t", scope: !2291, file: !1096, line: 96, type: !2306, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!2306 = !DISubroutineType(types: !2307)
!2307 = !{null, !2304, !2308}
!2308 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !2300, size: 64)
!2309 = !DISubprogram(name: "operator=", linkageName: "_ZNK25__cuda_builtin_blockDim_taSERKS_", scope: !2291, file: !1096, line: 96, type: !2310, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!2310 = !DISubroutineType(types: !2311)
!2311 = !{null, !2299, !2308}
!2312 = !DISubprogram(name: "operator&", linkageName: "_ZNK25__cuda_builtin_blockDim_tadEv", scope: !2291, file: !1096, line: 96, type: !2313, isLocal: false, isDefinition: false, scopeLine: 96, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!2313 = !DISubroutineType(types: !2314)
!2314 = !{!2315, !2299}
!2315 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !2291, size: 64)
!2316 = distinct !DILocation(line: 604, column: 34, scope: !2265)
!2317 = !{i32 1, i32 1025}
!2318 = !DILocation(line: 604, column: 34, scope: !2265)
!2319 = !DILocation(line: 604, column: 32, scope: !2265)
!2320 = !DILocation(line: 90, column: 3, scope: !2321, inlinedAt: !2322)
!2321 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockDim_t17__fetch_builtin_yEv", scope: !2291, file: !1096, line: 90, type: !1100, isLocal: false, isDefinition: true, scopeLine: 90, flags: DIFlagPrototyped, isOptimized: true, unit: !664, declaration: !2294, variables: !59)
!2322 = distinct !DILocation(line: 605, column: 34, scope: !2265)
!2323 = !DILocation(line: 605, column: 34, scope: !2265)
!2324 = !DILocation(line: 605, column: 32, scope: !2265)
!2325 = !DILocation(line: 100, column: 3, scope: !1840, inlinedAt: !2326)
!2326 = distinct !DILocation(line: 606, column: 34, scope: !2265)
!2327 = !DILocation(line: 606, column: 34, scope: !2265)
!2328 = !DILocation(line: 606, column: 32, scope: !2265)
!2329 = !DILocation(line: 101, column: 3, scope: !2330, inlinedAt: !2331)
!2330 = distinct !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN24__cuda_builtin_gridDim_t17__fetch_builtin_yEv", scope: !1841, file: !1096, line: 101, type: !1100, isLocal: false, isDefinition: true, scopeLine: 101, flags: DIFlagPrototyped, isOptimized: true, unit: !664, declaration: !1844, variables: !59)
!2331 = distinct !DILocation(line: 607, column: 34, scope: !2265)
!2332 = !DILocation(line: 607, column: 34, scope: !2265)
!2333 = !DILocation(line: 607, column: 32, scope: !2265)
!2334 = !DILocation(line: 608, column: 65, scope: !2265)
!2335 = !DILocation(line: 608, column: 4, scope: !2265)
!2336 = !DILocation(line: 609, column: 65, scope: !2265)
!2337 = !DILocation(line: 609, column: 4, scope: !2265)
!2338 = !DILocation(line: 611, column: 10, scope: !2265)
!2339 = !DILocation(line: 612, column: 33, scope: !2265)
!2340 = !DILocation(line: 612, column: 31, scope: !2265)
!2341 = !DILocation(line: 643, column: 17, scope: !2267)
!2342 = !DILocation(line: 644, column: 17, scope: !2267)
!2343 = !DILocation(line: 646, column: 3, scope: !2267)
!2344 = !DILocation(line: 648, column: 17, scope: !2267)
!2345 = !DILocation(line: 654, column: 9, scope: !2267)
!2346 = !DILocation(line: 392, column: 3, scope: !2347, inlinedAt: !2354)
!2347 = distinct !DISubprogram(name: "memcpy", linkageName: "_ZL6memcpyPvPKvm", scope: !528, file: !528, line: 390, type: !2348, isLocal: true, isDefinition: true, scopeLine: 391, flags: DIFlagPrototyped, isOptimized: true, unit: !664, variables: !2350)
!2348 = !DISubroutineType(types: !2349)
!2349 = !{!357, !357, !358, !360}
!2350 = !{!2351, !2352, !2353}
!2351 = !DILocalVariable(name: "dest", arg: 1, scope: !2347, file: !528, line: 390, type: !357)
!2352 = !DILocalVariable(name: "src", arg: 2, scope: !2347, file: !528, line: 390, type: !358)
!2353 = !DILocalVariable(name: "n", arg: 3, scope: !2347, file: !528, line: 390, type: !360)
!2354 = distinct !DILocation(line: 657, column: 3, scope: !2267)
!2355 = !DILocation(line: 392, column: 3, scope: !2347, inlinedAt: !2356)
!2356 = distinct !DILocation(line: 660, column: 3, scope: !2267)
!2357 = !DILocation(line: 685, column: 10, scope: !2267)
!2358 = !DILocation(line: 686, column: 12, scope: !2267)
!2359 = !DILocation(line: 688, column: 2, scope: !2267)
!2360 = !DILocation(line: 689, column: 1, scope: !2262)
