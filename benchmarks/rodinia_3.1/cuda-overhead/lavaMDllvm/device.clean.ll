; ModuleID = '<stdin>'
source_filename = "./kernel/kernel_gpu_cuda_wrapper.cu"
target datalayout = "e-i64:64-v16:16-v32:32-n16:32:64"
target triple = "nvptx64-nvidia-cuda"

%struct.FOUR_VECTOR = type { double, double, double, double }
%struct.par_str = type { double }
%struct.dim_str = type { i32, i32, i32, i32, i64, i64, i64, i64, i64 }
%struct.box_str = type { i32, i32, i32, i32, i64, i32, [26 x %struct.nei_str] }
%struct.nei_str = type { i32, i32, i32, i32, i64 }
%struct.THREE_VECTOR = type { double, double, double }

@_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9rA_shared = internal unnamed_addr addrspace(3) global [100 x %struct.FOUR_VECTOR] zeroinitializer, align 8, !dbg !0
@_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9rB_shared = internal unnamed_addr addrspace(3) global [100 x %struct.FOUR_VECTOR] zeroinitializer, align 8, !dbg !62
@_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9qB_shared = internal unnamed_addr addrspace(3) global [100 x double] zeroinitializer, align 8, !dbg !67

; Function Attrs: convergent nounwind
define void @_Z15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_(%struct.par_str* byval nocapture readonly align 8 %d_par_gpu, %struct.dim_str* byval nocapture readonly align 8 %d_dim_gpu, %struct.box_str* nocapture readonly %d_box_gpu, %struct.FOUR_VECTOR* nocapture readonly %d_rv_gpu, double* nocapture readonly %d_qv_gpu, %struct.FOUR_VECTOR* nocapture %d_fv_gpu) local_unnamed_addr #0 !dbg !2 {
entry:
  tail call void @llvm.dbg.declare(metadata %struct.par_str* %d_par_gpu, metadata !627, metadata !672), !dbg !673
  tail call void @llvm.dbg.declare(metadata %struct.dim_str* %d_dim_gpu, metadata !628, metadata !672), !dbg !674
  tail call void @llvm.dbg.value(metadata %struct.box_str* %d_box_gpu, i64 0, metadata !629, metadata !672), !dbg !675
  tail call void @llvm.dbg.value(metadata %struct.FOUR_VECTOR* %d_rv_gpu, i64 0, metadata !630, metadata !672), !dbg !676
  tail call void @llvm.dbg.value(metadata double* %d_qv_gpu, i64 0, metadata !631, metadata !672), !dbg !677
  tail call void @llvm.dbg.value(metadata %struct.FOUR_VECTOR* %d_fv_gpu, i64 0, metadata !632, metadata !672), !dbg !678
  %0 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #4, !dbg !679, !range !716
  tail call void @llvm.dbg.value(metadata i32 %0, i64 0, metadata !633, metadata !672), !dbg !717
  %1 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #4, !dbg !718, !range !746
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !634, metadata !672), !dbg !747
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !672), !dbg !748
  %conv231 = zext i32 %0 to i64, !dbg !749
  %number_boxes = getelementptr inbounds %struct.dim_str, %struct.dim_str* %d_dim_gpu, i64 0, i32 4, !dbg !750
  %2 = load i64, i64* %number_boxes, align 8, !dbg !750, !tbaa !751
  %cmp = icmp sgt i64 %2, %conv231, !dbg !757
  br i1 %cmp, label %if.then, label %if.end145, !dbg !758

if.then:                                          ; preds = %entry
  %alpha = getelementptr inbounds %struct.par_str, %struct.par_str* %d_par_gpu, i64 0, i32 0, !dbg !759
  %3 = load double, double* %alpha, align 8, !dbg !759, !tbaa !760
  %mul = fmul double %3, 2.000000e+00, !dbg !763
  %mul3 = fmul double %3, %mul, !dbg !764
  tail call void @llvm.dbg.value(metadata double %mul3, i64 0, metadata !636, metadata !672), !dbg !765
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !643, metadata !672), !dbg !766
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !647, metadata !672), !dbg !767
  tail call void @llvm.dbg.declare(metadata %struct.THREE_VECTOR* undef, metadata !655, metadata !672), !dbg !768
  %offset = getelementptr inbounds %struct.box_str, %struct.box_str* %d_box_gpu, i64 %conv231, i32 4, !dbg !769
  %4 = load i64, i64* %offset, align 8, !dbg !769, !tbaa !770
  %sext = shl i64 %4, 32, !dbg !772
  %idxprom5 = ashr exact i64 %sext, 32, !dbg !772
  tail call void @llvm.dbg.value(metadata %struct.FOUR_VECTOR* %arrayidx6, i64 0, metadata !640, metadata !672), !dbg !773
  %arrayidx8 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %d_fv_gpu, i64 %idxprom5, !dbg !774
  tail call void @llvm.dbg.value(metadata %struct.FOUR_VECTOR* %arrayidx8, i64 0, metadata !641, metadata !672), !dbg !775
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !672), !dbg !748
  %cmp9239 = icmp ult i32 %1, 100, !dbg !776
  br i1 %cmp9239, label %while.end.loopexit, label %while.end, !dbg !778

while.end.loopexit:                               ; preds = %if.then
  %arrayidx6 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %d_rv_gpu, i64 %idxprom5, !dbg !772
  %idxprom10243 = zext i32 %1 to i64, !dbg !779
  %arrayidx11 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx6, i64 %idxprom10243, !dbg !779
  %arrayidx13230 = getelementptr inbounds [100 x %struct.FOUR_VECTOR], [100 x %struct.FOUR_VECTOR] addrspace(3)* @_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9rA_shared, i64 0, i64 %idxprom10243, !dbg !781
  %5 = bitcast %struct.FOUR_VECTOR addrspace(3)* %arrayidx13230 to i8 addrspace(3)*, !dbg !782
  %6 = addrspacecast i8 addrspace(3)* %5 to i8*, !dbg !782
  %7 = bitcast %struct.FOUR_VECTOR* %arrayidx11 to i8*, !dbg !782
  tail call void @llvm.memcpy.p0i8.p0i8.i64(i8* %6, i8* %7, i64 32, i32 8, i1 false), !dbg !782, !tbaa.struct !783
  br label %while.end, !dbg !748

while.end:                                        ; preds = %while.end.loopexit, %if.then
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !672), !dbg !748
  tail call void @llvm.nvvm.barrier0(), !dbg !785
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !643, metadata !672), !dbg !766
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !672), !dbg !748
  tail call void @llvm.dbg.value(metadata i32 0, i64 0, metadata !643, metadata !672), !dbg !766
  %nn = getelementptr inbounds %struct.box_str, %struct.box_str* %d_box_gpu, i64 %conv231, i32 5, !dbg !786
  %8 = load i32, i32* %nn, align 8, !dbg !786, !tbaa !790
  %cmp17237 = icmp slt i32 %8, 0, !dbg !791
  br i1 %cmp17237, label %if.end145, label %for.body.preheader, !dbg !792

for.body.preheader:                               ; preds = %while.end
  %idxprom35242 = zext i32 %1 to i64
  %arrayidx38228 = getelementptr inbounds [100 x %struct.FOUR_VECTOR], [100 x %struct.FOUR_VECTOR] addrspace(3)* @_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9rB_shared, i64 0, i64 %idxprom35242
  %9 = bitcast %struct.FOUR_VECTOR addrspace(3)* %arrayidx38228 to i8 addrspace(3)*
  %10 = addrspacecast i8 addrspace(3)* %9 to i8*
  %arrayidx42229 = getelementptr inbounds [100 x double], [100 x double] addrspace(3)* @_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9qB_shared, i64 0, i64 %idxprom35242
  %11 = bitcast double addrspace(3)* %arrayidx42229 to i64 addrspace(3)*
  %12 = addrspacecast i64 addrspace(3)* %11 to i64*
  %idxprom51241 = zext i32 %1 to i64
  %arrayidx52225 = getelementptr inbounds [100 x %struct.FOUR_VECTOR], [100 x %struct.FOUR_VECTOR] addrspace(3)* @_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9rA_shared, i64 0, i64 %idxprom51241
  %arrayidx52 = addrspacecast %struct.FOUR_VECTOR addrspace(3)* %arrayidx52225 to %struct.FOUR_VECTOR*
  %v = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx52, i64 0, i32 0
  %x = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx52, i64 0, i32 1
  %y = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx52, i64 0, i32 2
  %z = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx52, i64 0, i32 3
  %arrayidx116 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx8, i64 %idxprom51241
  %v117 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx116, i64 0, i32 0
  %x124 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx116, i64 0, i32 1
  %y131 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx116, i64 0, i32 2
  %z138 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx116, i64 0, i32 3
  br label %for.body, !dbg !794

for.body:                                         ; preds = %for.body.preheader, %while.end141
  %k.0238 = phi i32 [ %inc143, %while.end141 ], [ 0, %for.body.preheader ]
  %cmp18 = icmp eq i32 %k.0238, 0, !dbg !794
  tail call void @llvm.dbg.value(metadata i32 %13, i64 0, metadata !642, metadata !672), !dbg !797
  br i1 %cmp18, label %if.end, label %if.else, !dbg !798

if.else:                                          ; preds = %for.body
  %sub = add nsw i32 %k.0238, -1, !dbg !799
  %idxprom22 = sext i32 %sub to i64, !dbg !801
  %number = getelementptr inbounds %struct.box_str, %struct.box_str* %d_box_gpu, i64 %conv231, i32 6, i64 %idxprom22, i32 3, !dbg !802
  %13 = load i32, i32* %number, align 4, !dbg !802, !tbaa !803
  br label %if.end

if.end:                                           ; preds = %for.body, %if.else
  %pointer.0 = phi i32 [ %13, %if.else ], [ %0, %for.body ]
  tail call void @llvm.dbg.value(metadata i32 %pointer.0, i64 0, metadata !642, metadata !672), !dbg !797
  %idxprom24 = sext i32 %pointer.0 to i64, !dbg !805
  %offset26 = getelementptr inbounds %struct.box_str, %struct.box_str* %d_box_gpu, i64 %idxprom24, i32 4, !dbg !806
  %14 = load i64, i64* %offset26, align 8, !dbg !806, !tbaa !770
  %sext224 = shl i64 %14, 32, !dbg !807
  %idxprom28 = ashr exact i64 %sext224, 32, !dbg !807
  tail call void @llvm.dbg.value(metadata %struct.FOUR_VECTOR* %arrayidx29, i64 0, metadata !645, metadata !672), !dbg !808
  tail call void @llvm.dbg.value(metadata double* %arrayidx31, i64 0, metadata !646, metadata !672), !dbg !809
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !672), !dbg !748
  br i1 %cmp9239, label %while.end44.loopexit, label %while.end44, !dbg !810

while.end44.loopexit:                             ; preds = %if.end
  %arrayidx31 = getelementptr inbounds double, double* %d_qv_gpu, i64 %idxprom28, !dbg !812
  %arrayidx29 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %d_rv_gpu, i64 %idxprom28, !dbg !807
  %arrayidx36 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx29, i64 %idxprom35242, !dbg !813
  %15 = bitcast %struct.FOUR_VECTOR* %arrayidx36 to i8*, !dbg !815
  tail call void @llvm.memcpy.p0i8.p0i8.i64(i8* %10, i8* %15, i64 32, i32 8, i1 false), !dbg !815, !tbaa.struct !783
  %arrayidx40 = getelementptr inbounds double, double* %arrayidx31, i64 %idxprom35242, !dbg !816
  %16 = bitcast double* %arrayidx40 to i64*, !dbg !816
  %17 = load i64, i64* %16, align 8, !dbg !816, !tbaa !784
  store i64 %17, i64* %12, align 8, !dbg !817, !tbaa !784
  br label %while.end44, !dbg !748

while.end44:                                      ; preds = %while.end44.loopexit, %if.end
  %cmp9239.pr = phi i1 [ %cmp9239, %while.end44.loopexit ], [ false, %if.end ]
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !672), !dbg !748
  tail call void @llvm.nvvm.barrier0(), !dbg !818
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !672), !dbg !748
  br i1 %cmp9239.pr, label %for.body50.preheader, label %while.end141, !dbg !819

for.body50.preheader:                             ; preds = %while.end44
  br label %for.body50, !dbg !820

for.body50:                                       ; preds = %for.body50.preheader, %_ZL3expd.exit
  %j.0234 = phi i32 [ %inc, %_ZL3expd.exit ], [ 0, %for.body50.preheader ]
  %18 = load double, double* %v, align 8, !dbg !820, !tbaa !825
  %idxprom53 = sext i32 %j.0234 to i64, !dbg !827
  %arrayidx54226 = getelementptr inbounds [100 x %struct.FOUR_VECTOR], [100 x %struct.FOUR_VECTOR] addrspace(3)* @_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9rB_shared, i64 0, i64 %idxprom53, !dbg !827
  %arrayidx54 = addrspacecast %struct.FOUR_VECTOR addrspace(3)* %arrayidx54226 to %struct.FOUR_VECTOR*, !dbg !827
  %v55 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx54, i64 0, i32 0, !dbg !828
  %19 = load double, double* %v55, align 8, !dbg !828, !tbaa !825
  %add56 = fadd double %18, %19, !dbg !829
  %20 = load double, double* %x, align 8, !dbg !830, !tbaa !831
  %x61 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx54, i64 0, i32 1, !dbg !830
  %21 = load double, double* %x61, align 8, !dbg !830, !tbaa !831
  %mul62 = fmul double %20, %21, !dbg !830
  %22 = load double, double* %y, align 8, !dbg !830, !tbaa !832
  %y67 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx54, i64 0, i32 2, !dbg !830
  %23 = load double, double* %y67, align 8, !dbg !830, !tbaa !832
  %mul68 = fmul double %22, %23, !dbg !830
  %add69 = fadd double %mul62, %mul68, !dbg !830
  %24 = load double, double* %z, align 8, !dbg !830, !tbaa !833
  %z74 = getelementptr inbounds %struct.FOUR_VECTOR, %struct.FOUR_VECTOR* %arrayidx54, i64 0, i32 3, !dbg !830
  %25 = load double, double* %z74, align 8, !dbg !830, !tbaa !833
  %mul75 = fmul double %24, %25, !dbg !830
  %add76 = fadd double %add69, %mul75, !dbg !830
  %sub77 = fsub double %add56, %add76, !dbg !834
  tail call void @llvm.dbg.value(metadata double %sub77, i64 0, metadata !648, metadata !672), !dbg !835
  %mul78 = fmul double %mul3, %sub77, !dbg !836
  tail call void @llvm.dbg.value(metadata double %mul78, i64 0, metadata !649, metadata !672), !dbg !837
  %sub79 = fsub double -0.000000e+00, %mul78, !dbg !838
  tail call void @llvm.dbg.value(metadata double %sub79, i64 0, metadata !839, metadata !672) #4, !dbg !843
  %26 = tail call i32 @llvm.nvvm.d2i.hi(double %sub79) #4, !dbg !845
  %27 = bitcast i32 %26 to float, !dbg !845
  %28 = tail call float @llvm.fabs.f32(float %27) #4, !dbg !845
  %29 = fcmp olt float %28, 0x4010E92220000000, !dbg !845
  br i1 %29, label %30, label %62, !dbg !845

; <label>:30:                                     ; preds = %for.body50
  %31 = fmul double %mul78, 0x3FF71547652B82FE, !dbg !845
  %32 = fsub double 0x4338000000000000, %31, !dbg !845
  %33 = tail call i32 @llvm.nvvm.d2i.lo(double %32) #4, !dbg !845
  %34 = fadd double %32, 0xC338000000000000, !dbg !845
  %35 = tail call double @llvm.fma.f64(double %34, double 0xBFE62E42FEFA39EF, double %sub79) #4, !dbg !845
  %36 = tail call double @llvm.fma.f64(double %34, double 0xBC7ABC9E3B39803F, double %35) #4, !dbg !845
  %37 = tail call double @llvm.fma.f64(double %36, double 0x3E5ADE1569CE2BDF, double 0x3E928AF3FCA213EA) #4, !dbg !845
  %38 = tail call double @llvm.fma.f64(double %37, double %36, double 0x3EC71DEE62401315) #4, !dbg !845
  %39 = tail call double @llvm.fma.f64(double %38, double %36, double 0x3EFA01997C89EB71) #4, !dbg !845
  %40 = tail call double @llvm.fma.f64(double %39, double %36, double 0x3F2A01A014761F65) #4, !dbg !845
  %41 = tail call double @llvm.fma.f64(double %40, double %36, double 0x3F56C16C1852B7AF) #4, !dbg !845
  %42 = tail call double @llvm.fma.f64(double %41, double %36, double 0x3F81111111122322) #4, !dbg !845
  %43 = tail call double @llvm.fma.f64(double %42, double %36, double 0x3FA55555555502A1) #4, !dbg !845
  %44 = tail call double @llvm.fma.f64(double %43, double %36, double 0x3FC5555555555511) #4, !dbg !845
  %45 = tail call double @llvm.fma.f64(double %44, double %36, double 0x3FE000000000000B) #4, !dbg !845
  %46 = tail call double @llvm.fma.f64(double %45, double %36, double 1.000000e+00) #4, !dbg !845
  %47 = tail call double @llvm.fma.f64(double %46, double %36, double 1.000000e+00) #4, !dbg !845
  %neg.i.i = sub i32 0, %33, !dbg !845
  %abs.cond.i.i = icmp sgt i32 %33, -1, !dbg !845
  %abs.i.i = select i1 %abs.cond.i.i, i32 %33, i32 %neg.i.i, !dbg !845
  %48 = icmp slt i32 %abs.i.i, 1023, !dbg !845
  br i1 %48, label %49, label %52, !dbg !845

; <label>:49:                                     ; preds = %30
  %50 = shl i32 %33, 20, !dbg !845
  %51 = add nsw i32 %50, 1072693248, !dbg !845
  br label %__internal_exp_kernel.exit.i.i, !dbg !845

; <label>:52:                                     ; preds = %30
  %53 = add nsw i32 %33, 2046, !dbg !845
  %54 = lshr i32 %53, 1, !dbg !845
  %55 = shl i32 %54, 20, !dbg !845
  %56 = shl i32 %53, 20, !dbg !845
  %57 = sub i32 %56, %55, !dbg !845
  %58 = tail call double @llvm.nvvm.lohi.i2d(i32 0, i32 %55) #4, !dbg !845
  %59 = fmul double %47, %58, !dbg !845
  br label %__internal_exp_kernel.exit.i.i, !dbg !845

__internal_exp_kernel.exit.i.i:                   ; preds = %52, %49
  %a.addr.0.i.i.i.i = phi double [ %47, %49 ], [ %59, %52 ], !dbg !845
  %k.0.i.i.i.i = phi i32 [ %51, %49 ], [ %57, %52 ], !dbg !845
  %60 = tail call double @llvm.nvvm.lohi.i2d(i32 0, i32 %k.0.i.i.i.i) #4, !dbg !845
  %61 = fmul double %a.addr.0.i.i.i.i, %60, !dbg !845
  br label %_ZL3expd.exit, !dbg !845

; <label>:62:                                     ; preds = %for.body50
  %63 = icmp slt i32 %26, 0, !dbg !845
  %64 = select i1 %63, double 0.000000e+00, double 0x7FF0000000000000, !dbg !845
  %65 = tail call double @llvm.fabs.f64(double %sub79) #4, !dbg !845
  %66 = fcmp ugt double %65, 0x7FF0000000000000, !dbg !845
  %67 = fsub double %sub79, %mul78, !dbg !845
  %..i.i = select i1 %66, double %67, double %64, !dbg !845
  br label %_ZL3expd.exit, !dbg !845

_ZL3expd.exit:                                    ; preds = %__internal_exp_kernel.exit.i.i, %62
  %t.1.i.i = phi double [ %61, %__internal_exp_kernel.exit.i.i ], [ %..i.i, %62 ], !dbg !845
  tail call void @llvm.dbg.value(metadata double %t.1.i.i, i64 0, metadata !650, metadata !672), !dbg !846
  %mul81 = fmul double %t.1.i.i, 2.000000e+00, !dbg !847
  tail call void @llvm.dbg.value(metadata double %mul81, i64 0, metadata !651, metadata !672), !dbg !848
  %sub88 = fsub double %20, %21, !dbg !849
  tail call void @llvm.dbg.value(metadata double %sub88, i64 0, metadata !655, metadata !850), !dbg !768
  %mul91 = fmul double %mul81, %sub88, !dbg !851
  tail call void @llvm.dbg.value(metadata double %mul91, i64 0, metadata !652, metadata !672), !dbg !852
  %sub98 = fsub double %22, %23, !dbg !853
  tail call void @llvm.dbg.value(metadata double %sub98, i64 0, metadata !655, metadata !854), !dbg !768
  %mul101 = fmul double %mul81, %sub98, !dbg !855
  tail call void @llvm.dbg.value(metadata double %mul101, i64 0, metadata !653, metadata !672), !dbg !856
  %sub108 = fsub double %24, %25, !dbg !857
  tail call void @llvm.dbg.value(metadata double %sub108, i64 0, metadata !655, metadata !858), !dbg !768
  %mul111 = fmul double %mul81, %sub108, !dbg !859
  tail call void @llvm.dbg.value(metadata double %mul111, i64 0, metadata !654, metadata !672), !dbg !860
  %arrayidx113227 = getelementptr inbounds [100 x double], [100 x double] addrspace(3)* @_ZZ15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_E9qB_shared, i64 0, i64 %idxprom53, !dbg !861
  %arrayidx113 = addrspacecast double addrspace(3)* %arrayidx113227 to double*, !dbg !861
  %68 = load double, double* %arrayidx113, align 8, !dbg !861, !tbaa !784
  %mul114 = fmul double %t.1.i.i, %68, !dbg !862
  %69 = load double, double* %v117, align 8, !dbg !863, !tbaa !825
  %add118 = fadd double %69, %mul114, !dbg !863
  store double %add118, double* %v117, align 8, !dbg !863, !tbaa !825
  %70 = load double, double* %arrayidx113, align 8, !dbg !864, !tbaa !784
  %mul121 = fmul double %mul91, %70, !dbg !865
  %71 = load double, double* %x124, align 8, !dbg !866, !tbaa !831
  %add125 = fadd double %71, %mul121, !dbg !866
  store double %add125, double* %x124, align 8, !dbg !866, !tbaa !831
  %72 = load double, double* %arrayidx113, align 8, !dbg !867, !tbaa !784
  %mul128 = fmul double %mul101, %72, !dbg !868
  %73 = load double, double* %y131, align 8, !dbg !869, !tbaa !832
  %add132 = fadd double %73, %mul128, !dbg !869
  store double %add132, double* %y131, align 8, !dbg !869, !tbaa !832
  %74 = load double, double* %arrayidx113, align 8, !dbg !870, !tbaa !784
  %mul135 = fmul double %mul111, %74, !dbg !871
  %75 = load double, double* %z138, align 8, !dbg !872, !tbaa !833
  %add139 = fadd double %75, %mul135, !dbg !872
  store double %add139, double* %z138, align 8, !dbg !872, !tbaa !833
  %inc = add nuw nsw i32 %j.0234, 1, !dbg !873
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !647, metadata !672), !dbg !767
  tail call void @llvm.dbg.value(metadata i32 %inc, i64 0, metadata !647, metadata !672), !dbg !767
  %exitcond = icmp eq i32 %inc, 100, !dbg !875
  br i1 %exitcond, label %while.end141.loopexit, label %for.body50, !dbg !877, !llvm.loop !879

while.end141.loopexit:                            ; preds = %_ZL3expd.exit
  br label %while.end141, !dbg !748

while.end141:                                     ; preds = %while.end141.loopexit, %while.end44
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !672), !dbg !748
  tail call void @llvm.nvvm.barrier0(), !dbg !882
  %inc143 = add nuw nsw i32 %k.0238, 1, !dbg !883
  tail call void @llvm.dbg.value(metadata i32 %inc143, i64 0, metadata !643, metadata !672), !dbg !766
  tail call void @llvm.dbg.value(metadata i32 %1, i64 0, metadata !635, metadata !672), !dbg !748
  tail call void @llvm.dbg.value(metadata i32 %inc143, i64 0, metadata !643, metadata !672), !dbg !766
  %76 = load i32, i32* %nn, align 8, !dbg !786, !tbaa !790
  %cmp17 = icmp slt i32 %k.0238, %76, !dbg !791
  br i1 %cmp17, label %for.body, label %if.end145.loopexit, !dbg !792, !llvm.loop !885

if.end145.loopexit:                               ; preds = %while.end141
  br label %if.end145, !dbg !888

if.end145:                                        ; preds = %if.end145.loopexit, %while.end, %entry
  ret void, !dbg !888
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture writeonly, i8* nocapture readonly, i64, i32, i1) #2

; Function Attrs: convergent nounwind
declare void @llvm.nvvm.barrier0() #3

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.x() #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.d2i.hi(double) #1

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.d2i.lo(double) #1

; Function Attrs: nounwind readnone
declare double @llvm.nvvm.lohi.i2d(i32, i32) #1

; Function Attrs: nounwind readnone
declare void @llvm.dbg.value(metadata, i64, metadata, metadata) #1

; Function Attrs: nounwind readnone
declare float @llvm.fabs.f32(float) #1

; Function Attrs: nounwind readnone
declare double @llvm.fabs.f64(double) #1

; Function Attrs: nounwind readnone
declare double @llvm.fma.f64(double, double, double) #1

attributes #0 = { convergent nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_20" "target-features"="+ptx42,-satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { argmemonly nounwind }
attributes #3 = { convergent nounwind }
attributes #4 = { nounwind }

!llvm.dbg.cu = !{!57}
!nvvm.annotations = !{!662, !663, !664, !663, !665, !665, !665, !665, !666, !666, !665}
!llvm.module.flags = !{!667, !668, !669}
!llvm.ident = !{!670}
!nvvm.internalize.after.link = !{}
!nvvmir.version = !{!671}

!0 = !DIGlobalVariableExpression(var: !1)
!1 = distinct !DIGlobalVariable(name: "rA_shared", scope: !2, file: !3, line: 41, type: !64, isLocal: true, isDefinition: true)
!2 = distinct !DISubprogram(name: "kernel_gpu_cuda", linkageName: "_Z15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_", scope: !3, file: !3, line: 5, type: !4, isLocal: false, isDefinition: true, scopeLine: 11, flags: DIFlagPrototyped, isOptimized: true, unit: !57, variables: !626)
!3 = !DIFile(filename: "./kernel/./kernel_gpu_cuda.cu", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!4 = !DISubroutineType(types: !5)
!5 = !{null, !6, !12, !26, !48, !56, !48}
!6 = !DIDerivedType(tag: DW_TAG_typedef, name: "par_str", file: !7, line: 70, baseType: !8)
!7 = !DIFile(filename: "./kernel/./../main.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!8 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "par_str", file: !7, line: 65, size: 64, elements: !9, identifier: "_ZTS7par_str")
!9 = !{!10}
!10 = !DIDerivedType(tag: DW_TAG_member, name: "alpha", scope: !8, file: !7, line: 68, baseType: !11, size: 64)
!11 = !DIBasicType(name: "double", size: 64, encoding: DW_ATE_float)
!12 = !DIDerivedType(tag: DW_TAG_typedef, name: "dim_str", file: !7, line: 88, baseType: !13)
!13 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "dim_str", file: !7, line: 72, size: 448, elements: !14, identifier: "_ZTS7dim_str")
!14 = !{!15, !17, !18, !19, !20, !22, !23, !24, !25}
!15 = !DIDerivedType(tag: DW_TAG_member, name: "cur_arg", scope: !13, file: !7, line: 76, baseType: !16, size: 32)
!16 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!17 = !DIDerivedType(tag: DW_TAG_member, name: "arch_arg", scope: !13, file: !7, line: 77, baseType: !16, size: 32, offset: 32)
!18 = !DIDerivedType(tag: DW_TAG_member, name: "cores_arg", scope: !13, file: !7, line: 78, baseType: !16, size: 32, offset: 64)
!19 = !DIDerivedType(tag: DW_TAG_member, name: "boxes1d_arg", scope: !13, file: !7, line: 79, baseType: !16, size: 32, offset: 96)
!20 = !DIDerivedType(tag: DW_TAG_member, name: "number_boxes", scope: !13, file: !7, line: 82, baseType: !21, size: 64, offset: 128)
!21 = !DIBasicType(name: "long int", size: 64, encoding: DW_ATE_signed)
!22 = !DIDerivedType(tag: DW_TAG_member, name: "box_mem", scope: !13, file: !7, line: 83, baseType: !21, size: 64, offset: 192)
!23 = !DIDerivedType(tag: DW_TAG_member, name: "space_elem", scope: !13, file: !7, line: 84, baseType: !21, size: 64, offset: 256)
!24 = !DIDerivedType(tag: DW_TAG_member, name: "space_mem", scope: !13, file: !7, line: 85, baseType: !21, size: 64, offset: 320)
!25 = !DIDerivedType(tag: DW_TAG_member, name: "space_mem2", scope: !13, file: !7, line: 86, baseType: !21, size: 64, offset: 384)
!26 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !27, size: 64)
!27 = !DIDerivedType(tag: DW_TAG_typedef, name: "box_str", file: !7, line: 63, baseType: !28)
!28 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "box_str", file: !7, line: 51, size: 5248, elements: !29, identifier: "_ZTS7box_str")
!29 = !{!30, !31, !32, !33, !34, !35, !36}
!30 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !28, file: !7, line: 55, baseType: !16, size: 32)
!31 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !28, file: !7, line: 55, baseType: !16, size: 32, offset: 32)
!32 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !28, file: !7, line: 55, baseType: !16, size: 32, offset: 64)
!33 = !DIDerivedType(tag: DW_TAG_member, name: "number", scope: !28, file: !7, line: 56, baseType: !16, size: 32, offset: 96)
!34 = !DIDerivedType(tag: DW_TAG_member, name: "offset", scope: !28, file: !7, line: 57, baseType: !21, size: 64, offset: 128)
!35 = !DIDerivedType(tag: DW_TAG_member, name: "nn", scope: !28, file: !7, line: 60, baseType: !16, size: 32, offset: 192)
!36 = !DIDerivedType(tag: DW_TAG_member, name: "nei", scope: !28, file: !7, line: 61, baseType: !37, size: 4992, offset: 256)
!37 = !DICompositeType(tag: DW_TAG_array_type, baseType: !38, size: 4992, elements: !46)
!38 = !DIDerivedType(tag: DW_TAG_typedef, name: "nei_str", file: !7, line: 49, baseType: !39)
!39 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "nei_str", file: !7, line: 41, size: 192, elements: !40, identifier: "_ZTS7nei_str")
!40 = !{!41, !42, !43, !44, !45}
!41 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !39, file: !7, line: 45, baseType: !16, size: 32)
!42 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !39, file: !7, line: 45, baseType: !16, size: 32, offset: 32)
!43 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !39, file: !7, line: 45, baseType: !16, size: 32, offset: 64)
!44 = !DIDerivedType(tag: DW_TAG_member, name: "number", scope: !39, file: !7, line: 46, baseType: !16, size: 32, offset: 96)
!45 = !DIDerivedType(tag: DW_TAG_member, name: "offset", scope: !39, file: !7, line: 47, baseType: !21, size: 64, offset: 128)
!46 = !{!47}
!47 = !DISubrange(count: 26)
!48 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !49, size: 64)
!49 = !DIDerivedType(tag: DW_TAG_typedef, name: "FOUR_VECTOR", file: !7, line: 39, baseType: !50)
!50 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !7, line: 35, size: 256, elements: !51, identifier: "_ZTS11FOUR_VECTOR")
!51 = !{!52, !53, !54, !55}
!52 = !DIDerivedType(tag: DW_TAG_member, name: "v", scope: !50, file: !7, line: 37, baseType: !11, size: 64)
!53 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !50, file: !7, line: 37, baseType: !11, size: 64, offset: 64)
!54 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !50, file: !7, line: 37, baseType: !11, size: 64, offset: 128)
!55 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !50, file: !7, line: 37, baseType: !11, size: 64, offset: 192)
!56 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !11, size: 64)
!57 = distinct !DICompileUnit(language: DW_LANG_C_plus_plus, file: !58, producer: "clang version 5.0.0 (trunk 294196)", isOptimized: true, runtimeVersion: 0, emissionKind: FullDebug, enums: !59, retainedTypes: !60, globals: !61, imports: !70)
!58 = !DIFile(filename: "./kernel/kernel_gpu_cuda_wrapper.cu", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!59 = !{}
!60 = !{!11}
!61 = !{!0, !62, !67}
!62 = !DIGlobalVariableExpression(var: !63)
!63 = distinct !DIGlobalVariable(name: "rB_shared", scope: !2, file: !3, line: 50, type: !64, isLocal: true, isDefinition: true)
!64 = !DICompositeType(tag: DW_TAG_array_type, baseType: !49, size: 25600, elements: !65)
!65 = !{!66}
!66 = !DISubrange(count: 100)
!67 = !DIGlobalVariableExpression(var: !68)
!68 = distinct !DIGlobalVariable(name: "qB_shared", scope: !2, file: !3, line: 51, type: !69, isLocal: true, isDefinition: true)
!69 = !DICompositeType(tag: DW_TAG_array_type, baseType: !11, size: 6400, elements: !65)
!70 = !{!71, !78, !83, !85, !87, !89, !91, !95, !97, !99, !101, !103, !105, !107, !109, !111, !113, !115, !117, !119, !121, !123, !127, !129, !131, !133, !137, !142, !144, !146, !151, !155, !157, !159, !161, !163, !165, !167, !169, !171, !175, !179, !181, !183, !187, !189, !191, !193, !195, !197, !201, !203, !205, !210, !217, !221, !223, !225, !229, !231, !233, !237, !239, !241, !245, !247, !249, !251, !253, !255, !257, !259, !261, !263, !268, !270, !272, !276, !278, !280, !282, !284, !286, !288, !290, !294, !298, !300, !302, !306, !308, !310, !312, !314, !316, !318, !322, !328, !332, !336, !341, !344, !348, !352, !367, !371, !375, !379, !383, !388, !390, !394, !398, !402, !410, !414, !418, !422, !426, !431, !437, !441, !445, !447, !455, !459, !467, !469, !471, !475, !479, !483, !488, !492, !497, !498, !499, !500, !503, !504, !505, !506, !507, !508, !509, !512, !514, !516, !518, !520, !522, !524, !526, !529, !531, !533, !535, !537, !539, !541, !543, !545, !547, !549, !551, !553, !555, !557, !559, !561, !563, !565, !567, !569, !571, !573, !575, !577, !579, !581, !583, !585, !587, !589, !591, !593, !597, !598, !600, !602, !604, !606, !608, !610, !612, !614, !616, !618, !620, !622, !624}
!71 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !74, line: 201)
!72 = !DINamespace(name: "std", scope: null, file: !73, line: 195)
!73 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_math_forward_declares.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!74 = !DISubprogram(name: "abs", linkageName: "_ZL3absx", scope: !73, file: !73, line: 44, type: !75, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!75 = !DISubroutineType(types: !76)
!76 = !{!77, !77}
!77 = !DIBasicType(name: "long long int", size: 64, encoding: DW_ATE_signed)
!78 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !79, line: 202)
!79 = !DISubprogram(name: "acos", linkageName: "_ZL4acosf", scope: !73, file: !73, line: 46, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!80 = !DISubroutineType(types: !81)
!81 = !{!82, !82}
!82 = !DIBasicType(name: "float", size: 32, encoding: DW_ATE_float)
!83 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !84, line: 203)
!84 = !DISubprogram(name: "acosh", linkageName: "_ZL5acoshf", scope: !73, file: !73, line: 48, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!85 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !86, line: 204)
!86 = !DISubprogram(name: "asin", linkageName: "_ZL4asinf", scope: !73, file: !73, line: 50, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!87 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !88, line: 205)
!88 = !DISubprogram(name: "asinh", linkageName: "_ZL5asinhf", scope: !73, file: !73, line: 52, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!89 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !90, line: 206)
!90 = !DISubprogram(name: "atan", linkageName: "_ZL4atanf", scope: !73, file: !73, line: 56, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!91 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !92, line: 207)
!92 = !DISubprogram(name: "atan2", linkageName: "_ZL5atan2ff", scope: !73, file: !73, line: 54, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!93 = !DISubroutineType(types: !94)
!94 = !{!82, !82, !82}
!95 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !96, line: 208)
!96 = !DISubprogram(name: "atanh", linkageName: "_ZL5atanhf", scope: !73, file: !73, line: 58, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!97 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !98, line: 209)
!98 = !DISubprogram(name: "cbrt", linkageName: "_ZL4cbrtf", scope: !73, file: !73, line: 60, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!99 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !100, line: 210)
!100 = !DISubprogram(name: "ceil", linkageName: "_ZL4ceilf", scope: !73, file: !73, line: 62, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!101 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !102, line: 211)
!102 = !DISubprogram(name: "copysign", linkageName: "_ZL8copysignff", scope: !73, file: !73, line: 64, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!103 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !104, line: 212)
!104 = !DISubprogram(name: "cos", linkageName: "_ZL3cosf", scope: !73, file: !73, line: 66, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!105 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !106, line: 213)
!106 = !DISubprogram(name: "cosh", linkageName: "_ZL4coshf", scope: !73, file: !73, line: 68, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!107 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !108, line: 214)
!108 = !DISubprogram(name: "erf", linkageName: "_ZL3erff", scope: !73, file: !73, line: 72, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!109 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !110, line: 215)
!110 = !DISubprogram(name: "erfc", linkageName: "_ZL4erfcf", scope: !73, file: !73, line: 70, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!111 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !112, line: 216)
!112 = !DISubprogram(name: "exp", linkageName: "_ZL3expf", scope: !73, file: !73, line: 76, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!113 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !114, line: 217)
!114 = !DISubprogram(name: "exp2", linkageName: "_ZL4exp2f", scope: !73, file: !73, line: 74, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!115 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !116, line: 218)
!116 = !DISubprogram(name: "expm1", linkageName: "_ZL5expm1f", scope: !73, file: !73, line: 78, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!117 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !118, line: 219)
!118 = !DISubprogram(name: "fabs", linkageName: "_ZL4fabsf", scope: !73, file: !73, line: 80, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!119 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !120, line: 220)
!120 = !DISubprogram(name: "fdim", linkageName: "_ZL4fdimff", scope: !73, file: !73, line: 82, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!121 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !122, line: 221)
!122 = !DISubprogram(name: "floor", linkageName: "_ZL5floorf", scope: !73, file: !73, line: 84, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!123 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !124, line: 222)
!124 = !DISubprogram(name: "fma", linkageName: "_ZL3fmafff", scope: !73, file: !73, line: 86, type: !125, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!125 = !DISubroutineType(types: !126)
!126 = !{!82, !82, !82, !82}
!127 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !128, line: 223)
!128 = !DISubprogram(name: "fmax", linkageName: "_ZL4fmaxff", scope: !73, file: !73, line: 88, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!129 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !130, line: 224)
!130 = !DISubprogram(name: "fmin", linkageName: "_ZL4fminff", scope: !73, file: !73, line: 90, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!131 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !132, line: 225)
!132 = !DISubprogram(name: "fmod", linkageName: "_ZL4fmodff", scope: !73, file: !73, line: 92, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!133 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !134, line: 226)
!134 = !DISubprogram(name: "fpclassify", linkageName: "_ZL10fpclassifyf", scope: !73, file: !73, line: 94, type: !135, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!135 = !DISubroutineType(types: !136)
!136 = !{!16, !82}
!137 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !138, line: 227)
!138 = !DISubprogram(name: "frexp", linkageName: "_ZL5frexpfPi", scope: !73, file: !73, line: 96, type: !139, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!139 = !DISubroutineType(types: !140)
!140 = !{!82, !82, !141}
!141 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !16, size: 64)
!142 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !143, line: 228)
!143 = !DISubprogram(name: "hypot", linkageName: "_ZL5hypotff", scope: !73, file: !73, line: 98, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!144 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !145, line: 229)
!145 = !DISubprogram(name: "ilogb", linkageName: "_ZL5ilogbf", scope: !73, file: !73, line: 100, type: !135, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!146 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !147, line: 230)
!147 = !DISubprogram(name: "isfinite", linkageName: "_ZL8isfinitef", scope: !73, file: !73, line: 102, type: !148, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!148 = !DISubroutineType(types: !149)
!149 = !{!150, !82}
!150 = !DIBasicType(name: "bool", size: 8, encoding: DW_ATE_boolean)
!151 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !152, line: 231)
!152 = !DISubprogram(name: "isgreater", linkageName: "_ZL9isgreaterff", scope: !73, file: !73, line: 106, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!153 = !DISubroutineType(types: !154)
!154 = !{!150, !82, !82}
!155 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !156, line: 232)
!156 = !DISubprogram(name: "isgreaterequal", linkageName: "_ZL14isgreaterequalff", scope: !73, file: !73, line: 105, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!157 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !158, line: 233)
!158 = !DISubprogram(name: "isinf", linkageName: "_ZL5isinff", scope: !73, file: !73, line: 108, type: !148, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!159 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !160, line: 234)
!160 = !DISubprogram(name: "isless", linkageName: "_ZL6islessff", scope: !73, file: !73, line: 112, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!161 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !162, line: 235)
!162 = !DISubprogram(name: "islessequal", linkageName: "_ZL11islessequalff", scope: !73, file: !73, line: 111, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!163 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !164, line: 236)
!164 = !DISubprogram(name: "islessgreater", linkageName: "_ZL13islessgreaterff", scope: !73, file: !73, line: 114, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!165 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !166, line: 237)
!166 = !DISubprogram(name: "isnan", linkageName: "_ZL5isnanf", scope: !73, file: !73, line: 116, type: !148, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!167 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !168, line: 238)
!168 = !DISubprogram(name: "isnormal", linkageName: "_ZL8isnormalf", scope: !73, file: !73, line: 118, type: !148, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!169 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !170, line: 239)
!170 = !DISubprogram(name: "isunordered", linkageName: "_ZL11isunorderedff", scope: !73, file: !73, line: 120, type: !153, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!171 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !172, line: 240)
!172 = !DISubprogram(name: "labs", linkageName: "_ZL4labsl", scope: !73, file: !73, line: 121, type: !173, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!173 = !DISubroutineType(types: !174)
!174 = !{!21, !21}
!175 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !176, line: 241)
!176 = !DISubprogram(name: "ldexp", linkageName: "_ZL5ldexpfi", scope: !73, file: !73, line: 123, type: !177, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!177 = !DISubroutineType(types: !178)
!178 = !{!82, !82, !16}
!179 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !180, line: 242)
!180 = !DISubprogram(name: "lgamma", linkageName: "_ZL6lgammaf", scope: !73, file: !73, line: 125, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!181 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !182, line: 243)
!182 = !DISubprogram(name: "llabs", linkageName: "_ZL5llabsx", scope: !73, file: !73, line: 126, type: !75, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!183 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !184, line: 244)
!184 = !DISubprogram(name: "llrint", linkageName: "_ZL6llrintf", scope: !73, file: !73, line: 128, type: !185, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!185 = !DISubroutineType(types: !186)
!186 = !{!77, !82}
!187 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !188, line: 245)
!188 = !DISubprogram(name: "log", linkageName: "_ZL3logf", scope: !73, file: !73, line: 138, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!189 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !190, line: 246)
!190 = !DISubprogram(name: "log10", linkageName: "_ZL5log10f", scope: !73, file: !73, line: 130, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!191 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !192, line: 247)
!192 = !DISubprogram(name: "log1p", linkageName: "_ZL5log1pf", scope: !73, file: !73, line: 132, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!193 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !194, line: 248)
!194 = !DISubprogram(name: "log2", linkageName: "_ZL4log2f", scope: !73, file: !73, line: 134, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!195 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !196, line: 249)
!196 = !DISubprogram(name: "logb", linkageName: "_ZL4logbf", scope: !73, file: !73, line: 136, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!197 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !198, line: 250)
!198 = !DISubprogram(name: "lrint", linkageName: "_ZL5lrintf", scope: !73, file: !73, line: 140, type: !199, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!199 = !DISubroutineType(types: !200)
!200 = !{!21, !82}
!201 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !202, line: 251)
!202 = !DISubprogram(name: "lround", linkageName: "_ZL6lroundf", scope: !73, file: !73, line: 142, type: !199, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!203 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !204, line: 252)
!204 = !DISubprogram(name: "llround", linkageName: "_ZL7llroundf", scope: !73, file: !73, line: 143, type: !185, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!205 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !206, line: 253)
!206 = !DISubprogram(name: "modf", linkageName: "_ZL4modffPf", scope: !73, file: !73, line: 145, type: !207, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!207 = !DISubroutineType(types: !208)
!208 = !{!82, !82, !209}
!209 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !82, size: 64)
!210 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !211, line: 254)
!211 = !DISubprogram(name: "nan", linkageName: "_ZL3nanPKc", scope: !73, file: !73, line: 146, type: !212, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!212 = !DISubroutineType(types: !213)
!213 = !{!11, !214}
!214 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !215, size: 64)
!215 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !216)
!216 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!217 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !218, line: 255)
!218 = !DISubprogram(name: "nanf", linkageName: "_ZL4nanfPKc", scope: !73, file: !73, line: 147, type: !219, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!219 = !DISubroutineType(types: !220)
!220 = !{!82, !214}
!221 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !222, line: 256)
!222 = !DISubprogram(name: "nearbyint", linkageName: "_ZL9nearbyintf", scope: !73, file: !73, line: 149, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!223 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !224, line: 257)
!224 = !DISubprogram(name: "nextafter", linkageName: "_ZL9nextafterff", scope: !73, file: !73, line: 151, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!225 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !226, line: 258)
!226 = !DISubprogram(name: "nexttoward", linkageName: "_ZL10nexttowardfd", scope: !73, file: !73, line: 153, type: !227, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!227 = !DISubroutineType(types: !228)
!228 = !{!82, !82, !11}
!229 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !230, line: 259)
!230 = !DISubprogram(name: "pow", linkageName: "_ZL3powfi", scope: !73, file: !73, line: 158, type: !177, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!231 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !232, line: 260)
!232 = !DISubprogram(name: "remainder", linkageName: "_ZL9remainderff", scope: !73, file: !73, line: 160, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!233 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !234, line: 261)
!234 = !DISubprogram(name: "remquo", linkageName: "_ZL6remquoffPi", scope: !73, file: !73, line: 162, type: !235, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!235 = !DISubroutineType(types: !236)
!236 = !{!82, !82, !82, !141}
!237 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !238, line: 262)
!238 = !DISubprogram(name: "rint", linkageName: "_ZL4rintf", scope: !73, file: !73, line: 164, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!239 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !240, line: 263)
!240 = !DISubprogram(name: "round", linkageName: "_ZL5roundf", scope: !73, file: !73, line: 166, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!241 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !242, line: 264)
!242 = !DISubprogram(name: "scalbln", linkageName: "_ZL7scalblnfl", scope: !73, file: !73, line: 168, type: !243, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!243 = !DISubroutineType(types: !244)
!244 = !{!82, !82, !21}
!245 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !246, line: 265)
!246 = !DISubprogram(name: "scalbn", linkageName: "_ZL6scalbnfi", scope: !73, file: !73, line: 170, type: !177, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!247 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !248, line: 266)
!248 = !DISubprogram(name: "signbit", linkageName: "_ZL7signbitf", scope: !73, file: !73, line: 172, type: !148, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!249 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !250, line: 267)
!250 = !DISubprogram(name: "sin", linkageName: "_ZL3sinf", scope: !73, file: !73, line: 174, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!251 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !252, line: 268)
!252 = !DISubprogram(name: "sinh", linkageName: "_ZL4sinhf", scope: !73, file: !73, line: 176, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!253 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !254, line: 269)
!254 = !DISubprogram(name: "sqrt", linkageName: "_ZL4sqrtf", scope: !73, file: !73, line: 178, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!255 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !256, line: 270)
!256 = !DISubprogram(name: "tan", linkageName: "_ZL3tanf", scope: !73, file: !73, line: 180, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!257 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !258, line: 271)
!258 = !DISubprogram(name: "tanh", linkageName: "_ZL4tanhf", scope: !73, file: !73, line: 182, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!259 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !260, line: 272)
!260 = !DISubprogram(name: "tgamma", linkageName: "_ZL6tgammaf", scope: !73, file: !73, line: 184, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!261 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !262, line: 273)
!262 = !DISubprogram(name: "trunc", linkageName: "_ZL5truncf", scope: !73, file: !73, line: 186, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!263 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !264, line: 102)
!264 = !DISubprogram(name: "acos", scope: !265, file: !265, line: 54, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!265 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/mathcalls.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!266 = !DISubroutineType(types: !267)
!267 = !{!11, !11}
!268 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !269, line: 121)
!269 = !DISubprogram(name: "asin", scope: !265, file: !265, line: 56, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!270 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !271, line: 140)
!271 = !DISubprogram(name: "atan", scope: !265, file: !265, line: 58, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!272 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !273, line: 159)
!273 = !DISubprogram(name: "atan2", scope: !265, file: !265, line: 60, type: !274, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!274 = !DISubroutineType(types: !275)
!275 = !{!11, !11, !11}
!276 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !277, line: 180)
!277 = !DISubprogram(name: "ceil", scope: !265, file: !265, line: 178, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!278 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !279, line: 199)
!279 = !DISubprogram(name: "cos", scope: !265, file: !265, line: 63, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!280 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !281, line: 218)
!281 = !DISubprogram(name: "cosh", scope: !265, file: !265, line: 72, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!282 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !283, line: 237)
!283 = !DISubprogram(name: "exp", scope: !265, file: !265, line: 100, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!284 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !285, line: 256)
!285 = !DISubprogram(name: "fabs", scope: !265, file: !265, line: 181, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!286 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !287, line: 275)
!287 = !DISubprogram(name: "floor", scope: !265, file: !265, line: 184, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!288 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !289, line: 294)
!289 = !DISubprogram(name: "fmod", scope: !265, file: !265, line: 187, type: !274, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!290 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !291, line: 315)
!291 = !DISubprogram(name: "frexp", scope: !265, file: !265, line: 103, type: !292, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!292 = !DISubroutineType(types: !293)
!293 = !{!11, !11, !141}
!294 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !295, line: 334)
!295 = !DISubprogram(name: "ldexp", scope: !265, file: !265, line: 106, type: !296, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!296 = !DISubroutineType(types: !297)
!297 = !{!11, !11, !16}
!298 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !299, line: 353)
!299 = !DISubprogram(name: "log", scope: !265, file: !265, line: 109, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!300 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !301, line: 372)
!301 = !DISubprogram(name: "log10", scope: !265, file: !265, line: 112, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!302 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !303, line: 391)
!303 = !DISubprogram(name: "modf", scope: !265, file: !265, line: 115, type: !304, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!304 = !DISubroutineType(types: !305)
!305 = !{!11, !11, !56}
!306 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !307, line: 403)
!307 = !DISubprogram(name: "pow", scope: !265, file: !265, line: 153, type: !274, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!308 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !309, line: 440)
!309 = !DISubprogram(name: "sin", scope: !265, file: !265, line: 65, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!310 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !311, line: 459)
!311 = !DISubprogram(name: "sinh", scope: !265, file: !265, line: 74, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!312 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !313, line: 478)
!313 = !DISubprogram(name: "sqrt", scope: !265, file: !265, line: 156, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!314 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !315, line: 497)
!315 = !DISubprogram(name: "tan", scope: !265, file: !265, line: 67, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!316 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !317, line: 516)
!317 = !DISubprogram(name: "tanh", scope: !265, file: !265, line: 76, type: !266, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!318 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !319, line: 118)
!319 = !DIDerivedType(tag: DW_TAG_typedef, name: "div_t", file: !320, line: 101, baseType: !321)
!320 = !DIFile(filename: "/usr/include/stdlib.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!321 = !DICompositeType(tag: DW_TAG_structure_type, file: !320, line: 97, flags: DIFlagFwdDecl, identifier: "_ZTS5div_t")
!322 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !323, line: 119)
!323 = !DIDerivedType(tag: DW_TAG_typedef, name: "ldiv_t", file: !320, line: 109, baseType: !324)
!324 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !320, line: 105, size: 128, elements: !325, identifier: "_ZTS6ldiv_t")
!325 = !{!326, !327}
!326 = !DIDerivedType(tag: DW_TAG_member, name: "quot", scope: !324, file: !320, line: 107, baseType: !21, size: 64)
!327 = !DIDerivedType(tag: DW_TAG_member, name: "rem", scope: !324, file: !320, line: 108, baseType: !21, size: 64, offset: 64)
!328 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !329, line: 121)
!329 = !DISubprogram(name: "abort", scope: !320, file: !320, line: 515, type: !330, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!330 = !DISubroutineType(types: !331)
!331 = !{null}
!332 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !333, line: 122)
!333 = !DISubprogram(name: "abs", scope: !320, file: !320, line: 775, type: !334, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!334 = !DISubroutineType(types: !335)
!335 = !{!16, !16}
!336 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !337, line: 123)
!337 = !DISubprogram(name: "atexit", scope: !320, file: !320, line: 519, type: !338, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!338 = !DISubroutineType(types: !339)
!339 = !{!16, !340}
!340 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !330, size: 64)
!341 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !342, line: 129)
!342 = !DISubprogram(name: "atof", scope: !343, file: !343, line: 26, type: !212, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!343 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/stdlib-float.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!344 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !345, line: 130)
!345 = !DISubprogram(name: "atoi", scope: !320, file: !320, line: 278, type: !346, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!346 = !DISubroutineType(types: !347)
!347 = !{!16, !214}
!348 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !349, line: 131)
!349 = !DISubprogram(name: "atol", scope: !320, file: !320, line: 283, type: !350, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!350 = !DISubroutineType(types: !351)
!351 = !{!21, !214}
!352 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !353, line: 132)
!353 = !DISubprogram(name: "bsearch", scope: !354, file: !354, line: 20, type: !355, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!354 = !DIFile(filename: "/usr/include/x86_64-linux-gnu/bits/stdlib-bsearch.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!355 = !DISubroutineType(types: !356)
!356 = !{!357, !358, !358, !360, !360, !363}
!357 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 64)
!358 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !359, size: 64)
!359 = !DIDerivedType(tag: DW_TAG_const_type, baseType: null)
!360 = !DIDerivedType(tag: DW_TAG_typedef, name: "size_t", file: !361, line: 62, baseType: !362)
!361 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/stddef.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!362 = !DIBasicType(name: "long unsigned int", size: 64, encoding: DW_ATE_unsigned)
!363 = !DIDerivedType(tag: DW_TAG_typedef, name: "__compar_fn_t", file: !320, line: 742, baseType: !364)
!364 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !365, size: 64)
!365 = !DISubroutineType(types: !366)
!366 = !{!16, !358, !358}
!367 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !368, line: 133)
!368 = !DISubprogram(name: "calloc", scope: !320, file: !320, line: 468, type: !369, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!369 = !DISubroutineType(types: !370)
!370 = !{!357, !360, !360}
!371 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !372, line: 134)
!372 = !DISubprogram(name: "div", scope: !320, file: !320, line: 789, type: !373, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!373 = !DISubroutineType(types: !374)
!374 = !{!319, !16, !16}
!375 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !376, line: 135)
!376 = !DISubprogram(name: "exit", scope: !320, file: !320, line: 543, type: !377, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!377 = !DISubroutineType(types: !378)
!378 = !{null, !16}
!379 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !380, line: 136)
!380 = !DISubprogram(name: "free", scope: !320, file: !320, line: 483, type: !381, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!381 = !DISubroutineType(types: !382)
!382 = !{null, !357}
!383 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !384, line: 137)
!384 = !DISubprogram(name: "getenv", scope: !320, file: !320, line: 564, type: !385, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!385 = !DISubroutineType(types: !386)
!386 = !{!387, !214}
!387 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !216, size: 64)
!388 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !389, line: 138)
!389 = !DISubprogram(name: "labs", scope: !320, file: !320, line: 776, type: !173, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!390 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !391, line: 139)
!391 = !DISubprogram(name: "ldiv", scope: !320, file: !320, line: 791, type: !392, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!392 = !DISubroutineType(types: !393)
!393 = !{!323, !21, !21}
!394 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !395, line: 140)
!395 = !DISubprogram(name: "malloc", scope: !320, file: !320, line: 466, type: !396, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!396 = !DISubroutineType(types: !397)
!397 = !{!357, !360}
!398 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !399, line: 142)
!399 = !DISubprogram(name: "mblen", scope: !320, file: !320, line: 863, type: !400, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!400 = !DISubroutineType(types: !401)
!401 = !{!16, !214, !360}
!402 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !403, line: 143)
!403 = !DISubprogram(name: "mbstowcs", scope: !320, file: !320, line: 874, type: !404, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!404 = !DISubroutineType(types: !405)
!405 = !{!360, !406, !409, !360}
!406 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !407)
!407 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !408, size: 64)
!408 = !DIBasicType(name: "wchar_t", size: 32, encoding: DW_ATE_signed)
!409 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !214)
!410 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !411, line: 144)
!411 = !DISubprogram(name: "mbtowc", scope: !320, file: !320, line: 866, type: !412, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!412 = !DISubroutineType(types: !413)
!413 = !{!16, !406, !409, !360}
!414 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !415, line: 146)
!415 = !DISubprogram(name: "qsort", scope: !320, file: !320, line: 765, type: !416, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!416 = !DISubroutineType(types: !417)
!417 = !{null, !357, !360, !360, !363}
!418 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !419, line: 152)
!419 = !DISubprogram(name: "rand", scope: !320, file: !320, line: 374, type: !420, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!420 = !DISubroutineType(types: !421)
!421 = !{!16}
!422 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !423, line: 153)
!423 = !DISubprogram(name: "realloc", scope: !320, file: !320, line: 480, type: !424, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!424 = !DISubroutineType(types: !425)
!425 = !{!357, !357, !360}
!426 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !427, line: 154)
!427 = !DISubprogram(name: "srand", scope: !320, file: !320, line: 376, type: !428, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!428 = !DISubroutineType(types: !429)
!429 = !{null, !430}
!430 = !DIBasicType(name: "unsigned int", size: 32, encoding: DW_ATE_unsigned)
!431 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !432, line: 155)
!432 = !DISubprogram(name: "strtod", scope: !320, file: !320, line: 164, type: !433, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!433 = !DISubroutineType(types: !434)
!434 = !{!11, !409, !435}
!435 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !436)
!436 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !387, size: 64)
!437 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !438, line: 156)
!438 = !DISubprogram(name: "strtol", scope: !320, file: !320, line: 183, type: !439, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!439 = !DISubroutineType(types: !440)
!440 = !{!21, !409, !435, !16}
!441 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !442, line: 157)
!442 = !DISubprogram(name: "strtoul", scope: !320, file: !320, line: 187, type: !443, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!443 = !DISubroutineType(types: !444)
!444 = !{!362, !409, !435, !16}
!445 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !446, line: 158)
!446 = !DISubprogram(name: "system", scope: !320, file: !320, line: 717, type: !346, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!447 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !448, line: 160)
!448 = !DISubprogram(name: "wcstombs", scope: !320, file: !320, line: 877, type: !449, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!449 = !DISubroutineType(types: !450)
!450 = !{!360, !451, !452, !360}
!451 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !387)
!452 = !DIDerivedType(tag: DW_TAG_restrict_type, baseType: !453)
!453 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !454, size: 64)
!454 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !408)
!455 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !456, line: 161)
!456 = !DISubprogram(name: "wctomb", scope: !320, file: !320, line: 870, type: !457, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!457 = !DISubroutineType(types: !458)
!458 = !{!16, !387, !408}
!459 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !462, line: 201)
!460 = !DINamespace(name: "__gnu_cxx", scope: null, file: !461, line: 68)
!461 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/bits/cpp_type_traits.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!462 = !DIDerivedType(tag: DW_TAG_typedef, name: "lldiv_t", file: !320, line: 121, baseType: !463)
!463 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !320, line: 117, size: 128, elements: !464, identifier: "_ZTS7lldiv_t")
!464 = !{!465, !466}
!465 = !DIDerivedType(tag: DW_TAG_member, name: "quot", scope: !463, file: !320, line: 119, baseType: !77, size: 64)
!466 = !DIDerivedType(tag: DW_TAG_member, name: "rem", scope: !463, file: !320, line: 120, baseType: !77, size: 64, offset: 64)
!467 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !468, line: 207)
!468 = !DISubprogram(name: "_Exit", scope: !320, file: !320, line: 557, type: !377, isLocal: false, isDefinition: false, flags: DIFlagPrototyped | DIFlagNoReturn, isOptimized: true)
!469 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !470, line: 211)
!470 = !DISubprogram(name: "llabs", scope: !320, file: !320, line: 780, type: !75, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!471 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !472, line: 217)
!472 = !DISubprogram(name: "lldiv", scope: !320, file: !320, line: 797, type: !473, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!473 = !DISubroutineType(types: !474)
!474 = !{!462, !77, !77}
!475 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !476, line: 228)
!476 = !DISubprogram(name: "atoll", scope: !320, file: !320, line: 292, type: !477, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!477 = !DISubroutineType(types: !478)
!478 = !{!77, !214}
!479 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !480, line: 229)
!480 = !DISubprogram(name: "strtoll", scope: !320, file: !320, line: 209, type: !481, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!481 = !DISubroutineType(types: !482)
!482 = !{!77, !409, !435, !16}
!483 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !484, line: 230)
!484 = !DISubprogram(name: "strtoull", scope: !320, file: !320, line: 214, type: !485, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!485 = !DISubroutineType(types: !486)
!486 = !{!487, !409, !435, !16}
!487 = !DIBasicType(name: "long long unsigned int", size: 64, encoding: DW_ATE_unsigned)
!488 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !489, line: 232)
!489 = !DISubprogram(name: "strtof", scope: !320, file: !320, line: 172, type: !490, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!490 = !DISubroutineType(types: !491)
!491 = !{!82, !409, !435}
!492 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !460, entity: !493, line: 233)
!493 = !DISubprogram(name: "strtold", scope: !320, file: !320, line: 175, type: !494, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!494 = !DISubroutineType(types: !495)
!495 = !{!496, !409, !435}
!496 = !DIBasicType(name: "long double", size: 64, encoding: DW_ATE_float)
!497 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !462, line: 241)
!498 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !468, line: 243)
!499 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !470, line: 245)
!500 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !501, line: 246)
!501 = !DISubprogram(name: "div", linkageName: "_ZN9__gnu_cxx3divExx", scope: !460, file: !502, line: 214, type: !473, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!502 = !DIFile(filename: "/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../include/c++/4.8/cstdlib", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!503 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !472, line: 247)
!504 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !476, line: 249)
!505 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !489, line: 250)
!506 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !480, line: 251)
!507 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !484, line: 252)
!508 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !493, line: 253)
!509 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !510, line: 418)
!510 = !DISubprogram(name: "acosf", linkageName: "_ZL5acosff", scope: !511, file: !511, line: 1126, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!511 = !DIFile(filename: "/usr/local/cuda/include/math_functions.hpp", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!512 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !513, line: 419)
!513 = !DISubprogram(name: "acoshf", linkageName: "_ZL6acoshff", scope: !511, file: !511, line: 1154, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!514 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !515, line: 420)
!515 = !DISubprogram(name: "asinf", linkageName: "_ZL5asinff", scope: !511, file: !511, line: 1121, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!516 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !517, line: 421)
!517 = !DISubprogram(name: "asinhf", linkageName: "_ZL6asinhff", scope: !511, file: !511, line: 1159, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!518 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !519, line: 422)
!519 = !DISubprogram(name: "atan2f", linkageName: "_ZL6atan2fff", scope: !511, file: !511, line: 1111, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!520 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !521, line: 423)
!521 = !DISubprogram(name: "atanf", linkageName: "_ZL5atanff", scope: !511, file: !511, line: 1116, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!522 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !523, line: 424)
!523 = !DISubprogram(name: "atanhf", linkageName: "_ZL6atanhff", scope: !511, file: !511, line: 1164, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!524 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !525, line: 425)
!525 = !DISubprogram(name: "cbrtf", linkageName: "_ZL5cbrtff", scope: !511, file: !511, line: 1199, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!526 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !527, line: 426)
!527 = !DISubprogram(name: "ceilf", linkageName: "_ZL5ceilff", scope: !528, file: !528, line: 647, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!528 = !DIFile(filename: "/usr/local/cuda/include/device_functions.hpp", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!529 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !530, line: 427)
!530 = !DISubprogram(name: "copysignf", linkageName: "_ZL9copysignfff", scope: !511, file: !511, line: 973, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!531 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !532, line: 428)
!532 = !DISubprogram(name: "cosf", linkageName: "_ZL4cosff", scope: !511, file: !511, line: 1027, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!533 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !534, line: 429)
!534 = !DISubprogram(name: "coshf", linkageName: "_ZL5coshff", scope: !511, file: !511, line: 1096, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!535 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !536, line: 430)
!536 = !DISubprogram(name: "erfcf", linkageName: "_ZL5erfcff", scope: !511, file: !511, line: 1259, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!537 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !538, line: 431)
!538 = !DISubprogram(name: "erff", linkageName: "_ZL4erfff", scope: !511, file: !511, line: 1249, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!539 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !540, line: 432)
!540 = !DISubprogram(name: "exp2f", linkageName: "_ZL5exp2ff", scope: !528, file: !528, line: 637, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!541 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !542, line: 433)
!542 = !DISubprogram(name: "expf", linkageName: "_ZL4expff", scope: !511, file: !511, line: 1078, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!543 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !544, line: 434)
!544 = !DISubprogram(name: "expm1f", linkageName: "_ZL6expm1ff", scope: !511, file: !511, line: 1169, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!545 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !546, line: 435)
!546 = !DISubprogram(name: "fabsf", linkageName: "_ZL5fabsff", scope: !528, file: !528, line: 582, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!547 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !548, line: 436)
!548 = !DISubprogram(name: "fdimf", linkageName: "_ZL5fdimfff", scope: !511, file: !511, line: 1385, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!549 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !550, line: 437)
!550 = !DISubprogram(name: "floorf", linkageName: "_ZL6floorff", scope: !528, file: !528, line: 572, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!551 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !552, line: 438)
!552 = !DISubprogram(name: "fmaf", linkageName: "_ZL4fmaffff", scope: !511, file: !511, line: 1337, type: !125, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!553 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !554, line: 439)
!554 = !DISubprogram(name: "fmaxf", linkageName: "_ZL5fmaxfff", scope: !528, file: !528, line: 602, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!555 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !556, line: 440)
!556 = !DISubprogram(name: "fminf", linkageName: "_ZL5fminfff", scope: !528, file: !528, line: 597, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!557 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !558, line: 441)
!558 = !DISubprogram(name: "fmodf", linkageName: "_ZL5fmodfff", scope: !511, file: !511, line: 1322, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!559 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !560, line: 442)
!560 = !DISubprogram(name: "frexpf", linkageName: "_ZL6frexpffPi", scope: !511, file: !511, line: 1312, type: !139, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!561 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !562, line: 443)
!562 = !DISubprogram(name: "hypotf", linkageName: "_ZL6hypotfff", scope: !511, file: !511, line: 1174, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!563 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !564, line: 444)
!564 = !DISubprogram(name: "ilogbf", linkageName: "_ZL6ilogbff", scope: !511, file: !511, line: 1390, type: !135, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!565 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !566, line: 445)
!566 = !DISubprogram(name: "ldexpf", linkageName: "_ZL6ldexpffi", scope: !511, file: !511, line: 1289, type: !177, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!567 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !568, line: 446)
!568 = !DISubprogram(name: "lgammaf", linkageName: "_ZL7lgammaff", scope: !511, file: !511, line: 1284, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!569 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !570, line: 447)
!570 = !DISubprogram(name: "llrintf", linkageName: "_ZL7llrintff", scope: !511, file: !511, line: 933, type: !185, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!571 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !572, line: 448)
!572 = !DISubprogram(name: "llroundf", linkageName: "_ZL8llroundff", scope: !511, file: !511, line: 1371, type: !185, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!573 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !574, line: 449)
!574 = !DISubprogram(name: "log10f", linkageName: "_ZL6log10ff", scope: !511, file: !511, line: 1140, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!575 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !576, line: 450)
!576 = !DISubprogram(name: "log1pf", linkageName: "_ZL6log1pff", scope: !511, file: !511, line: 1149, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!577 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !578, line: 451)
!578 = !DISubprogram(name: "log2f", linkageName: "_ZL5log2ff", scope: !511, file: !511, line: 1069, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!579 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !580, line: 452)
!580 = !DISubprogram(name: "logbf", linkageName: "_ZL5logbff", scope: !511, file: !511, line: 1395, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!581 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !582, line: 453)
!582 = !DISubprogram(name: "logf", linkageName: "_ZL4logff", scope: !511, file: !511, line: 1131, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!583 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !584, line: 454)
!584 = !DISubprogram(name: "lrintf", linkageName: "_ZL6lrintff", scope: !511, file: !511, line: 924, type: !199, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!585 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !586, line: 455)
!586 = !DISubprogram(name: "lroundf", linkageName: "_ZL7lroundff", scope: !511, file: !511, line: 1376, type: !199, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!587 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !588, line: 456)
!588 = !DISubprogram(name: "modff", linkageName: "_ZL5modfffPf", scope: !511, file: !511, line: 1317, type: !207, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!589 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !590, line: 457)
!590 = !DISubprogram(name: "nearbyintf", linkageName: "_ZL10nearbyintff", scope: !511, file: !511, line: 938, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!591 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !592, line: 458)
!592 = !DISubprogram(name: "nextafterf", linkageName: "_ZL10nextafterfff", scope: !511, file: !511, line: 1002, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!593 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !594, line: 459)
!594 = !DISubprogram(name: "nexttowardf", scope: !265, file: !265, line: 284, type: !595, isLocal: false, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!595 = !DISubroutineType(types: !596)
!596 = !{!82, !82, !496}
!597 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !594, line: 460)
!598 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !599, line: 461)
!599 = !DISubprogram(name: "powf", linkageName: "_ZL4powfff", scope: !511, file: !511, line: 1352, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!600 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !601, line: 462)
!601 = !DISubprogram(name: "remainderf", linkageName: "_ZL10remainderfff", scope: !511, file: !511, line: 1327, type: !93, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!602 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !603, line: 463)
!603 = !DISubprogram(name: "remquof", linkageName: "_ZL7remquofffPi", scope: !511, file: !511, line: 1332, type: !235, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!604 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !605, line: 464)
!605 = !DISubprogram(name: "rintf", linkageName: "_ZL5rintff", scope: !511, file: !511, line: 919, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!606 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !607, line: 465)
!607 = !DISubprogram(name: "roundf", linkageName: "_ZL6roundff", scope: !511, file: !511, line: 1366, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!608 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !609, line: 466)
!609 = !DISubprogram(name: "scalblnf", linkageName: "_ZL8scalblnffl", scope: !511, file: !511, line: 1299, type: !243, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!610 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !611, line: 467)
!611 = !DISubprogram(name: "scalbnf", linkageName: "_ZL7scalbnffi", scope: !511, file: !511, line: 1294, type: !177, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!612 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !613, line: 468)
!613 = !DISubprogram(name: "sinf", linkageName: "_ZL4sinff", scope: !511, file: !511, line: 1018, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!614 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !615, line: 469)
!615 = !DISubprogram(name: "sinhf", linkageName: "_ZL5sinhff", scope: !511, file: !511, line: 1101, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!616 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !617, line: 470)
!617 = !DISubprogram(name: "sqrtf", linkageName: "_ZL5sqrtff", scope: !528, file: !528, line: 887, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!618 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !619, line: 471)
!619 = !DISubprogram(name: "tanf", linkageName: "_ZL4tanff", scope: !511, file: !511, line: 1060, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!620 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !621, line: 472)
!621 = !DISubprogram(name: "tanhf", linkageName: "_ZL5tanhff", scope: !511, file: !511, line: 1106, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!622 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !623, line: 473)
!623 = !DISubprogram(name: "tgammaf", linkageName: "_ZL7tgammaff", scope: !511, file: !511, line: 1361, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!624 = !DIImportedEntity(tag: DW_TAG_imported_declaration, scope: !72, entity: !625, line: 474)
!625 = !DISubprogram(name: "truncf", linkageName: "_ZL6truncff", scope: !528, file: !528, line: 642, type: !80, isLocal: true, isDefinition: false, flags: DIFlagPrototyped, isOptimized: true)
!626 = !{!627, !628, !629, !630, !631, !632, !633, !634, !635, !636, !639, !640, !641, !642, !643, !644, !645, !646, !647, !648, !649, !650, !651, !652, !653, !654, !655}
!627 = !DILocalVariable(name: "d_par_gpu", arg: 1, scope: !2, file: !3, line: 5, type: !6)
!628 = !DILocalVariable(name: "d_dim_gpu", arg: 2, scope: !2, file: !3, line: 6, type: !12)
!629 = !DILocalVariable(name: "d_box_gpu", arg: 3, scope: !2, file: !3, line: 7, type: !26)
!630 = !DILocalVariable(name: "d_rv_gpu", arg: 4, scope: !2, file: !3, line: 8, type: !48)
!631 = !DILocalVariable(name: "d_qv_gpu", arg: 5, scope: !2, file: !3, line: 9, type: !56)
!632 = !DILocalVariable(name: "d_fv_gpu", arg: 6, scope: !2, file: !3, line: 10, type: !48)
!633 = !DILocalVariable(name: "bx", scope: !2, file: !3, line: 17, type: !16)
!634 = !DILocalVariable(name: "tx", scope: !2, file: !3, line: 18, type: !16)
!635 = !DILocalVariable(name: "wtx", scope: !2, file: !3, line: 21, type: !16)
!636 = !DILocalVariable(name: "a2", scope: !637, file: !3, line: 35, type: !11)
!637 = distinct !DILexicalBlock(scope: !638, file: !3, line: 27, column: 31)
!638 = distinct !DILexicalBlock(scope: !2, file: !3, line: 27, column: 5)
!639 = !DILocalVariable(name: "first_i", scope: !637, file: !3, line: 38, type: !16)
!640 = !DILocalVariable(name: "rA", scope: !637, file: !3, line: 39, type: !48)
!641 = !DILocalVariable(name: "fA", scope: !637, file: !3, line: 40, type: !48)
!642 = !DILocalVariable(name: "pointer", scope: !637, file: !3, line: 44, type: !16)
!643 = !DILocalVariable(name: "k", scope: !637, file: !3, line: 45, type: !16)
!644 = !DILocalVariable(name: "first_j", scope: !637, file: !3, line: 46, type: !16)
!645 = !DILocalVariable(name: "rB", scope: !637, file: !3, line: 47, type: !48)
!646 = !DILocalVariable(name: "qB", scope: !637, file: !3, line: 48, type: !56)
!647 = !DILocalVariable(name: "j", scope: !637, file: !3, line: 49, type: !16)
!648 = !DILocalVariable(name: "r2", scope: !637, file: !3, line: 54, type: !11)
!649 = !DILocalVariable(name: "u2", scope: !637, file: !3, line: 55, type: !11)
!650 = !DILocalVariable(name: "vij", scope: !637, file: !3, line: 56, type: !11)
!651 = !DILocalVariable(name: "fs", scope: !637, file: !3, line: 57, type: !11)
!652 = !DILocalVariable(name: "fxij", scope: !637, file: !3, line: 58, type: !11)
!653 = !DILocalVariable(name: "fyij", scope: !637, file: !3, line: 59, type: !11)
!654 = !DILocalVariable(name: "fzij", scope: !637, file: !3, line: 60, type: !11)
!655 = !DILocalVariable(name: "d", scope: !637, file: !3, line: 61, type: !656)
!656 = !DIDerivedType(tag: DW_TAG_typedef, name: "THREE_VECTOR", file: !7, line: 33, baseType: !657)
!657 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !7, line: 29, size: 192, elements: !658, identifier: "_ZTS12THREE_VECTOR")
!658 = !{!659, !660, !661}
!659 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !657, file: !7, line: 31, baseType: !11, size: 64)
!660 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !657, file: !7, line: 31, baseType: !11, size: 64, offset: 64)
!661 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !657, file: !7, line: 31, baseType: !11, size: 64, offset: 128)
!662 = !{void (%struct.par_str*, %struct.dim_str*, %struct.box_str*, %struct.FOUR_VECTOR*, double*, %struct.FOUR_VECTOR*)* @_Z15kernel_gpu_cuda7par_str7dim_strP7box_strP11FOUR_VECTORPdS4_, !"kernel", i32 1}
!663 = !{null, !"align", i32 8}
!664 = !{null, !"align", i32 8, !"align", i32 65544, !"align", i32 131080}
!665 = !{null, !"align", i32 16}
!666 = !{null, !"align", i32 16, !"align", i32 65552, !"align", i32 131088}
!667 = !{i32 2, !"Dwarf Version", i32 4}
!668 = !{i32 2, !"Debug Info Version", i32 3}
!669 = !{i32 4, !"nvvm-reflect-ftz", i32 0}
!670 = !{!"clang version 5.0.0 (trunk 294196)"}
!671 = !{i32 1, i32 2}
!672 = !DIExpression()
!673 = !DILocation(line: 5, column: 41, scope: !2)
!674 = !DILocation(line: 6, column: 17, scope: !2)
!675 = !DILocation(line: 7, column: 18, scope: !2)
!676 = !DILocation(line: 8, column: 22, scope: !2)
!677 = !DILocation(line: 9, column: 13, scope: !2)
!678 = !DILocation(line: 10, column: 22, scope: !2)
!679 = !DILocation(line: 78, column: 3, scope: !680, inlinedAt: !715)
!680 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !682, file: !681, line: 78, type: !685, isLocal: false, isDefinition: true, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true, unit: !57, declaration: !684, variables: !59)
!681 = !DIFile(filename: "/home/dshen/llvm/build/bin/../lib/clang/5.0.0/include/__clang_cuda_builtin_vars.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!682 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_blockIdx_t", file: !681, line: 77, size: 8, elements: !683, identifier: "_ZTS25__cuda_builtin_blockIdx_t")
!683 = !{!684, !687, !688, !689, !700, !704, !708, !711}
!684 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_xEv", scope: !682, file: !681, line: 78, type: !685, isLocal: false, isDefinition: false, scopeLine: 78, flags: DIFlagPrototyped, isOptimized: true)
!685 = !DISubroutineType(types: !686)
!686 = !{!430}
!687 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_yEv", scope: !682, file: !681, line: 79, type: !685, isLocal: false, isDefinition: false, scopeLine: 79, flags: DIFlagPrototyped, isOptimized: true)
!688 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN25__cuda_builtin_blockIdx_t17__fetch_builtin_zEv", scope: !682, file: !681, line: 80, type: !685, isLocal: false, isDefinition: false, scopeLine: 80, flags: DIFlagPrototyped, isOptimized: true)
!689 = !DISubprogram(name: "operator uint3", linkageName: "_ZNK25__cuda_builtin_blockIdx_tcv5uint3Ev", scope: !682, file: !681, line: 83, type: !690, isLocal: false, isDefinition: false, scopeLine: 83, flags: DIFlagPrototyped, isOptimized: true)
!690 = !DISubroutineType(types: !691)
!691 = !{!692, !698}
!692 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "uint3", file: !693, line: 190, size: 96, elements: !694, identifier: "_ZTS5uint3")
!693 = !DIFile(filename: "/usr/local/cuda/include/vector_types.h", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!694 = !{!695, !696, !697}
!695 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !692, file: !693, line: 192, baseType: !430, size: 32)
!696 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !692, file: !693, line: 192, baseType: !430, size: 32, offset: 32)
!697 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !692, file: !693, line: 192, baseType: !430, size: 32, offset: 64)
!698 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !699, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!699 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !682)
!700 = !DISubprogram(name: "__cuda_builtin_blockIdx_t", scope: !682, file: !681, line: 85, type: !701, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!701 = !DISubroutineType(types: !702)
!702 = !{null, !703}
!703 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !682, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!704 = !DISubprogram(name: "__cuda_builtin_blockIdx_t", scope: !682, file: !681, line: 85, type: !705, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!705 = !DISubroutineType(types: !706)
!706 = !{null, !703, !707}
!707 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !699, size: 64)
!708 = !DISubprogram(name: "operator=", linkageName: "_ZNK25__cuda_builtin_blockIdx_taSERKS_", scope: !682, file: !681, line: 85, type: !709, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!709 = !DISubroutineType(types: !710)
!710 = !{null, !698, !707}
!711 = !DISubprogram(name: "operator&", linkageName: "_ZNK25__cuda_builtin_blockIdx_tadEv", scope: !682, file: !681, line: 85, type: !712, isLocal: false, isDefinition: false, scopeLine: 85, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!712 = !DISubroutineType(types: !713)
!713 = !{!714, !698}
!714 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !682, size: 64)
!715 = distinct !DILocation(line: 17, column: 11, scope: !2)
!716 = !{i32 0, i32 65535}
!717 = !DILocation(line: 17, column: 6, scope: !2)
!718 = !DILocation(line: 67, column: 3, scope: !719, inlinedAt: !745)
!719 = distinct !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !720, file: !681, line: 67, type: !685, isLocal: false, isDefinition: true, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true, unit: !57, declaration: !722, variables: !59)
!720 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "__cuda_builtin_threadIdx_t", file: !681, line: 66, size: 8, elements: !721, identifier: "_ZTS26__cuda_builtin_threadIdx_t")
!721 = !{!722, !723, !724, !725, !730, !734, !738, !741}
!722 = !DISubprogram(name: "__fetch_builtin_x", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_xEv", scope: !720, file: !681, line: 67, type: !685, isLocal: false, isDefinition: false, scopeLine: 67, flags: DIFlagPrototyped, isOptimized: true)
!723 = !DISubprogram(name: "__fetch_builtin_y", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_yEv", scope: !720, file: !681, line: 68, type: !685, isLocal: false, isDefinition: false, scopeLine: 68, flags: DIFlagPrototyped, isOptimized: true)
!724 = !DISubprogram(name: "__fetch_builtin_z", linkageName: "_ZN26__cuda_builtin_threadIdx_t17__fetch_builtin_zEv", scope: !720, file: !681, line: 69, type: !685, isLocal: false, isDefinition: false, scopeLine: 69, flags: DIFlagPrototyped, isOptimized: true)
!725 = !DISubprogram(name: "operator uint3", linkageName: "_ZNK26__cuda_builtin_threadIdx_tcv5uint3Ev", scope: !720, file: !681, line: 72, type: !726, isLocal: false, isDefinition: false, scopeLine: 72, flags: DIFlagPrototyped, isOptimized: true)
!726 = !DISubroutineType(types: !727)
!727 = !{!692, !728}
!728 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !729, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!729 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !720)
!730 = !DISubprogram(name: "__cuda_builtin_threadIdx_t", scope: !720, file: !681, line: 74, type: !731, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!731 = !DISubroutineType(types: !732)
!732 = !{null, !733}
!733 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !720, size: 64, flags: DIFlagArtificial | DIFlagObjectPointer)
!734 = !DISubprogram(name: "__cuda_builtin_threadIdx_t", scope: !720, file: !681, line: 74, type: !735, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!735 = !DISubroutineType(types: !736)
!736 = !{null, !733, !737}
!737 = !DIDerivedType(tag: DW_TAG_reference_type, baseType: !729, size: 64)
!738 = !DISubprogram(name: "operator=", linkageName: "_ZNK26__cuda_builtin_threadIdx_taSERKS_", scope: !720, file: !681, line: 74, type: !739, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!739 = !DISubroutineType(types: !740)
!740 = !{null, !728, !737}
!741 = !DISubprogram(name: "operator&", linkageName: "_ZNK26__cuda_builtin_threadIdx_tadEv", scope: !720, file: !681, line: 74, type: !742, isLocal: false, isDefinition: false, scopeLine: 74, flags: DIFlagPrivate | DIFlagPrototyped, isOptimized: true)
!742 = !DISubroutineType(types: !743)
!743 = !{!744, !728}
!744 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !720, size: 64)
!745 = distinct !DILocation(line: 18, column: 11, scope: !2)
!746 = !{i32 0, i32 1024}
!747 = !DILocation(line: 18, column: 6, scope: !2)
!748 = !DILocation(line: 21, column: 6, scope: !2)
!749 = !DILocation(line: 27, column: 5, scope: !638)
!750 = !DILocation(line: 27, column: 18, scope: !638)
!751 = !{!752, !756, i64 16}
!752 = !{!"_ZTS7dim_str", !753, i64 0, !753, i64 4, !753, i64 8, !753, i64 12, !756, i64 16, !756, i64 24, !756, i64 32, !756, i64 40, !756, i64 48}
!753 = !{!"int", !754, i64 0}
!754 = !{!"omnipotent char", !755, i64 0}
!755 = !{!"Simple C++ TBAA"}
!756 = !{!"long", !754, i64 0}
!757 = !DILocation(line: 27, column: 7, scope: !638)
!758 = !DILocation(line: 27, column: 5, scope: !2)
!759 = !DILocation(line: 35, column: 25, scope: !637)
!760 = !{!761, !762, i64 0}
!761 = !{!"_ZTS7par_str", !762, i64 0}
!762 = !{!"double", !754, i64 0}
!763 = !DILocation(line: 35, column: 14, scope: !637)
!764 = !DILocation(line: 35, column: 30, scope: !637)
!765 = !DILocation(line: 35, column: 6, scope: !637)
!766 = !DILocation(line: 45, column: 7, scope: !637)
!767 = !DILocation(line: 49, column: 7, scope: !637)
!768 = !DILocation(line: 61, column: 16, scope: !637)
!769 = !DILocation(line: 72, column: 27, scope: !637)
!770 = !{!771, !756, i64 16}
!771 = !{!"_ZTS7box_str", !753, i64 0, !753, i64 4, !753, i64 8, !753, i64 12, !756, i64 16, !753, i64 24, !754, i64 32}
!772 = !DILocation(line: 75, column: 9, scope: !637)
!773 = !DILocation(line: 39, column: 16, scope: !637)
!774 = !DILocation(line: 76, column: 9, scope: !637)
!775 = !DILocation(line: 40, column: 16, scope: !637)
!776 = !DILocation(line: 83, column: 12, scope: !777)
!777 = !DILexicalBlockFile(scope: !637, file: !3, discriminator: 1)
!778 = !DILocation(line: 83, column: 3, scope: !777)
!779 = !DILocation(line: 84, column: 21, scope: !780)
!780 = distinct !DILexicalBlock(scope: !637, file: !3, line: 83, column: 32)
!781 = !DILocation(line: 84, column: 4, scope: !780)
!782 = !DILocation(line: 84, column: 19, scope: !780)
!783 = !{i64 0, i64 8, !784, i64 8, i64 8, !784, i64 16, i64 8, !784, i64 24, i64 8, !784}
!784 = !{!762, !762, i64 0}
!785 = !DILocation(line: 90, column: 3, scope: !637)
!786 = !DILocation(line: 97, column: 32, scope: !787)
!787 = !DILexicalBlockFile(scope: !788, file: !3, discriminator: 1)
!788 = distinct !DILexicalBlock(scope: !789, file: !3, line: 97, column: 3)
!789 = distinct !DILexicalBlock(scope: !637, file: !3, line: 97, column: 3)
!790 = !{!771, !753, i64 24}
!791 = !DILocation(line: 97, column: 14, scope: !787)
!792 = !DILocation(line: 97, column: 3, scope: !793)
!793 = !DILexicalBlockFile(scope: !789, file: !3, discriminator: 1)
!794 = !DILocation(line: 103, column: 8, scope: !795)
!795 = distinct !DILexicalBlock(scope: !796, file: !3, line: 103, column: 7)
!796 = distinct !DILexicalBlock(scope: !788, file: !3, line: 97, column: 41)
!797 = !DILocation(line: 44, column: 7, scope: !637)
!798 = !DILocation(line: 103, column: 7, scope: !796)
!799 = !DILocation(line: 107, column: 34, scope: !800)
!800 = distinct !DILexicalBlock(scope: !795, file: !3, line: 106, column: 8)
!801 = !DILocation(line: 107, column: 15, scope: !800)
!802 = !DILocation(line: 107, column: 38, scope: !800)
!803 = !{!804, !753, i64 12}
!804 = !{!"_ZTS7nei_str", !753, i64 0, !753, i64 4, !753, i64 8, !753, i64 12, !756, i64 16}
!805 = !DILocation(line: 115, column: 14, scope: !796)
!806 = !DILocation(line: 115, column: 33, scope: !796)
!807 = !DILocation(line: 118, column: 10, scope: !796)
!808 = !DILocation(line: 47, column: 16, scope: !637)
!809 = !DILocation(line: 48, column: 7, scope: !637)
!810 = !DILocation(line: 126, column: 4, scope: !811)
!811 = !DILexicalBlockFile(scope: !796, file: !3, discriminator: 1)
!812 = !DILocation(line: 119, column: 10, scope: !796)
!813 = !DILocation(line: 127, column: 22, scope: !814)
!814 = distinct !DILexicalBlock(scope: !796, file: !3, line: 126, column: 33)
!815 = !DILocation(line: 127, column: 20, scope: !814)
!816 = !DILocation(line: 128, column: 22, scope: !814)
!817 = !DILocation(line: 128, column: 20, scope: !814)
!818 = !DILocation(line: 134, column: 4, scope: !796)
!819 = !DILocation(line: 142, column: 4, scope: !811)
!820 = !DILocation(line: 166, column: 30, scope: !821)
!821 = distinct !DILexicalBlock(scope: !822, file: !3, line: 145, column: 41)
!822 = distinct !DILexicalBlock(scope: !823, file: !3, line: 145, column: 5)
!823 = distinct !DILexicalBlock(scope: !824, file: !3, line: 145, column: 5)
!824 = distinct !DILexicalBlock(scope: !796, file: !3, line: 142, column: 33)
!825 = !{!826, !762, i64 0}
!826 = !{!"_ZTS11FOUR_VECTOR", !762, i64 0, !762, i64 8, !762, i64 16, !762, i64 24}
!827 = !DILocation(line: 166, column: 38, scope: !821)
!828 = !DILocation(line: 166, column: 51, scope: !821)
!829 = !DILocation(line: 166, column: 32, scope: !821)
!830 = !DILocation(line: 166, column: 55, scope: !821)
!831 = !{!826, !762, i64 8}
!832 = !{!826, !762, i64 16}
!833 = !{!826, !762, i64 24}
!834 = !DILocation(line: 166, column: 53, scope: !821)
!835 = !DILocation(line: 54, column: 6, scope: !637)
!836 = !DILocation(line: 167, column: 13, scope: !821)
!837 = !DILocation(line: 55, column: 6, scope: !637)
!838 = !DILocation(line: 168, column: 15, scope: !821)
!839 = !DILocalVariable(name: "a", arg: 1, scope: !840, file: !841, line: 245, type: !11)
!840 = distinct !DISubprogram(name: "exp", linkageName: "_ZL3expd", scope: !841, file: !841, line: 245, type: !266, isLocal: true, isDefinition: true, scopeLine: 246, flags: DIFlagPrototyped, isOptimized: true, unit: !57, variables: !842)
!841 = !DIFile(filename: "/usr/local/cuda/include/math_functions_dbl_ptx3.hpp", directory: "/home/dshen/benchmarks/rodinia_3.1/cuda-overhead/lavaMDllvm")
!842 = !{!839}
!843 = !DILocation(line: 245, column: 52, scope: !840, inlinedAt: !844)
!844 = distinct !DILocation(line: 168, column: 11, scope: !821)
!845 = !DILocation(line: 247, column: 10, scope: !840, inlinedAt: !844)
!846 = !DILocation(line: 56, column: 6, scope: !637)
!847 = !DILocation(line: 169, column: 12, scope: !821)
!848 = !DILocation(line: 57, column: 6, scope: !637)
!849 = !DILocation(line: 171, column: 34, scope: !821)
!850 = !DIExpression(DW_OP_LLVM_fragment, 0, 64)
!851 = !DILocation(line: 172, column: 13, scope: !821)
!852 = !DILocation(line: 58, column: 6, scope: !637)
!853 = !DILocation(line: 173, column: 34, scope: !821)
!854 = !DIExpression(DW_OP_LLVM_fragment, 64, 64)
!855 = !DILocation(line: 174, column: 13, scope: !821)
!856 = !DILocation(line: 59, column: 6, scope: !637)
!857 = !DILocation(line: 175, column: 34, scope: !821)
!858 = !DIExpression(DW_OP_LLVM_fragment, 128, 64)
!859 = !DILocation(line: 176, column: 13, scope: !821)
!860 = !DILocation(line: 60, column: 6, scope: !637)
!861 = !DILocation(line: 178, column: 33, scope: !821)
!862 = !DILocation(line: 178, column: 45, scope: !821)
!863 = !DILocation(line: 178, column: 16, scope: !821)
!864 = !DILocation(line: 179, column: 33, scope: !821)
!865 = !DILocation(line: 179, column: 45, scope: !821)
!866 = !DILocation(line: 179, column: 16, scope: !821)
!867 = !DILocation(line: 180, column: 33, scope: !821)
!868 = !DILocation(line: 180, column: 45, scope: !821)
!869 = !DILocation(line: 180, column: 16, scope: !821)
!870 = !DILocation(line: 181, column: 33, scope: !821)
!871 = !DILocation(line: 181, column: 45, scope: !821)
!872 = !DILocation(line: 181, column: 16, scope: !821)
!873 = !DILocation(line: 145, column: 38, scope: !874)
!874 = !DILexicalBlockFile(scope: !822, file: !3, discriminator: 2)
!875 = !DILocation(line: 145, column: 16, scope: !876)
!876 = !DILexicalBlockFile(scope: !822, file: !3, discriminator: 1)
!877 = !DILocation(line: 145, column: 5, scope: !878)
!878 = !DILexicalBlockFile(scope: !823, file: !3, discriminator: 1)
!879 = distinct !{!879, !880, !881}
!880 = !DILocation(line: 145, column: 5, scope: !823)
!881 = !DILocation(line: 183, column: 5, scope: !823)
!882 = !DILocation(line: 194, column: 4, scope: !796)
!883 = !DILocation(line: 97, column: 38, scope: !884)
!884 = !DILexicalBlockFile(scope: !788, file: !3, discriminator: 2)
!885 = distinct !{!885, !886, !887}
!886 = !DILocation(line: 97, column: 3, scope: !789)
!887 = !DILocation(line: 200, column: 3, scope: !789)
!888 = !DILocation(line: 214, column: 1, scope: !2)
