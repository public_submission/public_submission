#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Instruction.h"
#include "llvm/ADT/ilist.h"
#include "llvm/IR/SymbolTableListTraits.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/DebugLoc.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Metadata.h"
#include "llvm/ADT/StringRef.h"
#include <iostream>

#include "common.h"

#define BUFFERSIZE_INT BUFFERSIZE/4 // number of integers, rather than bytes // do not use 24<<20, errors
#define TRACESIZE_INT TRACESIZE/4 // number of integers, rather than bytes

const int AFsize = 22;
std::string AF[AFsize] = {	"print1",
				"print2",
				"print3",
				"print5",
				"takeString",
				"RetKernel",
				"storeLines",
				"dumpLines",
				"atomicAdd", //this is needed for -O0
				"passBasicBlock",
				"callFunc",
			"mystrcpy",
			"mystrcmp",
			"getFuncID",
			"updateCallStack",
			"printCallStack",
			"getContextID",
			"cxtcpy",
			"myholdon",
			"cxtcmp",
			"cxtprint",
			"InitKernel"
			};

std::string CUDAKN[20];
int KNcnt=0;



using namespace llvm;
namespace{
	// pass: first pass
	// for memory load/store, function call and return
    struct instru_host : public ModulePass{
        static char ID;   
        Function *hook1;
        Function *hook2;
        Function *hook3;
        Function *hook4;
        Function *hookEA;
	Function *hookRetMain;
	Function *hookString;
	Function *hookCMFS;
	Function* hookDumpTrace;
	Function* hookFCall;
	bool flag1; // tracks whether a caller name global Variable created or not
	Value* str_fname=NULL; 
	Value* ptr_fname=NULL;

        instru_host() : ModulePass(ID) {}

        virtual bool runOnModule(Module &M)
        {
		LLVMContext &C = M.getContext();
		Type* VoidTy = Type::getVoidTy(C); 
                errs() << "\n================== 1st pass: instru_host ==============\n\n";

            	Constant *hookFunc1; Constant *hookFunc2;
		Constant *hookFunc3; Constant *hookFunc4; Constant *hookFuncEA;
		Constant *hookFuncRetMain;
		Constant *hookFuncString;
		Constant *hookFuncDumpTrace;
		Constant *hookFuncFCall;

            	hookFunc1 = M.getOrInsertFunction("print1", VoidTy, (Type*)0);
            	hookFunc2 = M.getOrInsertFunction("print2", VoidTy, (Type*)0);
		hookFunc3 = M.getOrInsertFunction("print3", VoidTy, Type::getInt32Ty(C),  NULL);
		hookFunc4 = M.getOrInsertFunction("print4", VoidTy, Type::getInt64Ty(C),  NULL);
		hookFuncEA = M.getOrInsertFunction("printea", VoidTy, Type::getInt8PtrTy(C), Type::getInt32Ty(C), NULL);
		hookFuncRetMain = M.getOrInsertFunction("RetMain", VoidTy, (Type*)0 );
		hookFuncString = M.getOrInsertFunction("takeString", VoidTy, Type::getInt8PtrTy(C), Type::getInt32Ty(C), NULL);
		hookFuncFCall = M.getOrInsertFunction("callFunc", VoidTy, Type::getInt8PtrTy(C), Type::getInt8PtrTy(C), Type::getInt32Ty(C), Type::getInt32Ty(C), NULL);

//		Type::getFloatTy(M.getContext())  for float
		hook1= cast<Function>(hookFunc1); hook2= cast<Function>(hookFunc2); hook3= cast<Function>(hookFunc3); 
		hook4= cast<Function>(hookFunc4); hookEA= cast<Function>(hookFuncEA);
                hookRetMain = cast<Function>(hookFuncRetMain);
                hookString = cast<Function>(hookFuncString);
                hookFCall = cast<Function>(hookFuncFCall);

            	for(Module::iterator F = M.begin(), E = M.end(); F!= E; ++F)
               	{
		// 	errs() << F->getName().str() << "\n";
			flag1 = false;
	                for(Function::iterator BB = F->begin(), E = F->end(); BB != E; ++BB)
			{
                //	errs() << F->getName().str() << "\n";
		//	errs() << BB->getName().str() << "\n";
				instru_host::runOnBasicBlock(BB);//TODO: for overhead testing
			}
            	}
		return false;
        }


        virtual bool runOnBasicBlock(Function::iterator &BB)
        {
        	LLVMContext &C = BB->getContext();
		Value* ZERO = ConstantInt::get(Type::getInt32Ty(C), 0);
		Value* ONE = ConstantInt::get(Type::getInt32Ty(C), 1);
		Value* TWO = ConstantInt::get(Type::getInt32Ty(C), 2);
		Value* THREE = ConstantInt::get(Type::getInt32Ty(C), 3);

		Function* bb_func = BB->getParent();
		StringRef f_name = bb_func->getName();	
		std::string func_name = f_name.str();	
		std::string bb_name = BB->getName().str();
		//errs() << func_name << " : " << bb_name << "\n" ;
		int cnt = 0;
	
		IRBuilder<> bbbuilder(&(*BB));
		if(!flag1)
		{
			str_fname = bbbuilder.CreateGlobalStringPtr(f_name);
			ptr_fname = bbbuilder.CreatePointerCast(str_fname, Type::getInt8PtrTy(C) );
			flag1=true;
		}

	    	for(BasicBlock::iterator BI = BB->begin(), BE = BB->end(); BI != BE; ++BI)
            	{
			cnt++;
		    	if (cnt==1 ) 
			{
				if( (bb_name.find("entry") != std::string::npos)  &&  (func_name.find("main") != std::string::npos)  )
				{      // insert AllocaInst into entry block of main function.
					errs() << "I am in main() function's entry block\n";
				}
				
				if( bb_name.find("kcall.end") != std::string::npos  &&  (bb_name.find("1") == std::string::npos) )
				{
					errs() << "I am in kcall.end block \n";
				}	
			}

			if ( auto *op = dyn_cast<ResumeInst>( &(*BI) ) )
			{
                                errs() << " [[[\nI am Resume Inst!  :    "  << *BI <<  "\n ]]]\n";
				continue;
			}	
			if ( auto *op = dyn_cast<ReturnInst>( &(*BI) ) )
                        {
                        //      errs() << " [[[\nI am ReturnInst !  :    "  << *BI <<  "\n";
			//	errs() << op->getFunction() -> getName().str() <<  "\n";

				StringRef fn = op->getFunction() -> getName();
				std::string fname = fn.str();
			//	if( (fname.find("cudaMalloc") == std::string::npos)  || (fname.find("_") != std::string::npos)  )
				//const char* ffname;
				//ffname = fname.c_str();

				IRBuilder<> builder(op);
				Value* args[] = { ptr_fname, THREE};
                               	CallInst *MI = builder.CreateCall(hookString, args);
				
				if (fname=="main")
				{
					errs() << "!!!!!!!!! main\n";
					Instruction *newInst = CallInst::Create(hookRetMain,"");
                                	newInst->insertBefore( &(*BI));		
				}
                        //       errs() << " ]]]\n\n" ;
				continue;
                        }

			if ( auto *op = dyn_cast<CallInst>( &(*BI) ) )
			{
				Function* f = op->getCalledFunction();
				if (!f) //this check is important for: %call =  call deferenceble bla bla...
					continue;	
				StringRef callee = op->getCalledFunction() -> getName();
                                std::string calleeName = callee.str();

				if( (calleeName.find("llvm.dbg") != std::string::npos)  || (calleeName.find("llvm.stack") != std::string::npos)  
				   || (calleeName.find("llvm.lifetime") != std::string::npos) || (calleeName.find("llvm.memcpy") != std::string::npos)
				   || (calleeName.find("dim3") != std::string::npos) || (calleeName.find("basic_ostream") != std::string::npos) 
				   || (calleeName.find("_ZNS") != std::string::npos)  )
					continue;

				StringRef caller = op->getFunction() -> getName();
				std::string callerName = caller.str();
			
				if( (callerName.find("__cuda_register_global") != std::string::npos)  || (callerName.find("__cuda_module_") != std::string::npos) )
					continue;

				errs() << " [[[\nI am an CallInst !  :    "  << *BI <<  "\n";
				errs() << "from : " << op->getFunction() -> getName().str() <<  "\n";
		//			errs() << op->getCalledFunction() -> getName().str() <<  "\n";

					IRBuilder<> builder(op);

				//	Value *Str, *strPtr;
		//			Str = builder.CreateGlobalStringPtr(f_name); // pass caller name
		//			strPtr = builder.CreatePointerCast(Str, Type::getInt8PtrTy(C) );
					Value *args[] = {ptr_fname, ONE};
					CallInst *MI = builder.CreateCall(hookString, args);

					Value *Str, *strPtr;
					Str = builder.CreateGlobalStringPtr(callee); // pass callee name
					strPtr = builder.CreatePointerCast(Str, Type::getInt8PtrTy(C) );
					Value *args2[] = {strPtr, TWO};
					builder.CreateCall(hookString, args2);

					

                                	const DebugLoc &loc = BI->getDebugLoc();
                                	if (loc)
                                	{
                                        	int l =  loc.getLine();
                                        	int cl =  loc.getCol();
						builder.CreateCall(hook3, builder.getInt32(l) );

						builder.CreateCall(hookFCall, {ptr_fname, strPtr, builder.getInt32(l), builder.getInt32(cl) } );

					}
	
					if (calleeName=="main")
					{
						errs() << "!!!!!!!!! main\n";
						Instruction *newInst = CallInst::Create(hookRetMain,"");
						newInst->insertBefore( &(*BI));
					}
				
	
				if( (calleeName.find("cudaMalloc") != std::string::npos)  && (calleeName.find("_") == std::string::npos)  )
				{
		//			errs() <<  op->getNumArgOperands() << "\n";
					Value* arg1 = op->getArgOperand(0);
					Value* ptr1 = builder.CreatePointerCast(arg1, Type::getInt8PtrTy(C) );
	//TODO
	//		do something to ptr1
	//				arg1 ->dump(); errs() << "\n";
	//				ptr1 ->dump(); errs() << "\n";
	//				ptr2 ->dump(); errs() << "\n";
	//				builder.CreateCall(hookEA, {arg1});
					builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() ); // insert after op
					builder.CreateCall(hookEA, {ptr1,TWO});

					Value* arg2 = op->getArgOperand(1);
	//				arg2 ->dump(); errs() << "\n";
					builder.CreateCall(hook4, {arg2}); //Value* argsxy[] = {arg2};
					errs() << " ]]]\n\n" ;

				} 
			}

	continue;
			if ( auto *op = dyn_cast<BinaryOperator>( &(*BI) ) )
			{
				errs()  << " [[[\nI am an binary operator!  :    "  << *BI <<  "\n";
/*				IRBuilder<> builder(op);
				builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() );
			//	Value *TWO = ConstantInt::get(Type::getInt32Ty(C), 2);
			//	Value *Two = ConstantFP::get(Type::getFloatTy(C), 2.0);
				
				Value* args[] = {op};
				builder.CreateCall(hook3, args);			
*/
				errs() << " ]]]\n\n" ;
			}
	continue;
			if ( auto *op = dyn_cast<LoadInst>( &(*BI) ) )
                        {
                                errs()  << " [[[\nI am a load!  :    "  << *BI <<  "\n";

                                IRBuilder<> builder(op); //insert before op
                                builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() ); // insert after op
				Instruction *newInst = CallInst::Create(hook1,"");
                                newInst->insertBefore( &(*BI));			

                                const DebugLoc &loc = BI->getDebugLoc();
                                if (loc)
                                {
                                        int l =  loc.getLine();
                      //                  errs() <<"getLine  : " << loc.getLine() << "\n";
		//			errs() << "\nprint:\n";
		//			loc.print(errs());
			//		errs() << "\ndump: \n";
			//		loc.dump();
				//	errs() << "getScope : " << loc.getScope() << "\n";
				//	errs() << Scope->getFilename() << "\n";

                                        Value* L = ConstantInt::get(Type::getInt32Ty(C), l);
                                        Value* args[] = {L};
                                        builder.CreateCall(hook3,args);
                                }
                                LoadInst *CI = dyn_cast<LoadInst>(BI);
                                Value* addr = op->getPointerOperand();//TODO
//				addr->print( errs(), true);
//				addr->printAsOperand( errs(), true);
				Value *voidptr = builder.CreatePointerCast(addr, Type::getInt8PtrTy(C) );
//				addr->dump(); errs() << "\n";
//				voidptr->dump(); errs() << "\n";
  				CallInst *MI = builder.CreateCall(hookEA, {voidptr,ONE});

                                errs() << " ]]]\n\n" ;
                        }

			if ( auto *op = dyn_cast<StoreInst>( &(*BI) ) )
                        {
                                errs()  << " [[[[\nI am a store!  :    "  << *BI <<  "\n";
				
				Instruction *newInst = CallInst::Create(hook2,"");
                                newInst->insertBefore( &(*BI));	

				IRBuilder<> builder(op); //insert before op
                                builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() ); // insert after op

                                const DebugLoc &loc = BI->getDebugLoc();
				if (loc)
				{
					int l =  loc.getLine();
					errs() <<"getLine  : " << loc.getLine() << "\n";
					Value *L = ConstantInt::get(Type::getInt32Ty(C), l);
					Value* args[] = {L};
					builder.CreateCall(hook3,args);
				}

				
				StoreInst *CI = dyn_cast<StoreInst>(BI);
				Value  *addr = CI->getPointerOperand();
                        	Value *voidptr = builder.CreatePointerCast(addr, Type::getInt8PtrTy(C) );
                                CallInst *MI = builder.CreateCall(hookEA, {voidptr,ONE});        

				errs() << " ]]]\n\n" ;
                        }

            	}
            	return true;
        }
    };


	// second pass for CPU, host
	// for Global Variables
    struct instru_globalvar : public ModulePass{
        static char ID;   
        Function* hook1;
        Function* hook2;
        Function* hook3;
        Function* hook4;
        Function* hookEA;
	Function* hookRetMain;
	Function* hookString;
	Function* hookCMFS;
	Function* hookCRV;
	Function* hookDumpTrace;
	Function* hookAppend;
	Function* hookAppendBB;
	Function* hookMyMalloc;
	Function* hookGetHandle;

	GlobalVariable* bufferOnDevice; 
	GlobalVariable* gv_1;
	GlobalVariable* gv_2;
	GlobalVariable* gv_test;
 
        instru_globalvar() : ModulePass(ID) {}

        virtual bool runOnModule(Module &M)
        {
		LLVMContext &C = M.getContext();
		Type* VoidTy = Type::getVoidTy(C); 
                errs() << "\n================== 2nd pass: instru_globalvar ==============\n\n";

		gv_1 = new GlobalVariable(M, 
        		Type::getInt32Ty(C),
        		false,
        		GlobalValue::InternalLinkage,
        		0, // has initializer, specified below
        		"gv1");
		gv_1->setAlignment(16);
		gv_1->setInitializer(ConstantInt::get(Type::getInt32Ty(C), 0));
	/////////
		gv_2= new GlobalVariable(M,
        		ArrayType::get(Type::getInt32Ty(C), 20),
        		false,
        		GlobalValue::InternalLinkage,
        		0, // has initializer, specified below
        		"gv2");
		gv_2->setAlignment(16);
		ConstantAggregateZero* const_array_2 = ConstantAggregateZero::get(  ArrayType::get(Type::getInt32Ty(C), 20) );
		gv_2->setInitializer( const_array_2);
	/////////
		bufferOnDevice  = new GlobalVariable(M,
                        ArrayType::get(Type::getInt32Ty(C), BUFFERSIZE_INT),
                        false,
                        GlobalValue::InternalLinkage,
                        0, // has initializer, specified below
                        "buffer_oN_DeViCe"); // you need an awkward name to avoid conflicts with applications's variables
                bufferOnDevice->setAlignment(16);
                ConstantAggregateZero* const_array_tmp = ConstantAggregateZero::get(  ArrayType::get(Type::getInt32Ty(C), BUFFERSIZE_INT) );
                bufferOnDevice->setInitializer( const_array_tmp);
	//////////
        //////////

		gv_test = M.getGlobalVariable("test",true);

		Constant *hookFunc1; Constant *hookFunc2;
                Constant *hookFunc3; Constant *hookFunc4; Constant *hookFuncEA;
                Constant *hookFuncRetMain;
                Constant* hookFuncCMFS;
                Constant* hookFuncCRV;
                Constant *hookFuncString;
                Constant *hookFuncDumpTrace;
                Constant* hookFuncAppendTrace;
                Constant* hookFuncAppendBBlog;
                Constant* hookFuncMyMalloc;
                Constant* hookFuncGetHandle;

		hookCMFS = M.getFunction("cudaMemcpyFromSymbol");
		if(hookCMFS==NULL)
		{
			errs() << "cudaMemcpyFromSymbol not found, need to insert it!\n";
			hookFuncCMFS = M.getOrInsertFunction("cudaMemcpyFromSymbol", Type::getInt32Ty(C), 
					Type::getInt8PtrTy(C), Type::getInt8PtrTy(C) ,Type::getInt64Ty(C), Type::getInt64Ty(C), Type::getInt32Ty(C), NULL);
                	hookCMFS = cast<Function>(hookFuncCMFS);
		
		} else
		{
			errs() << "cudaMemcpyFromSymbol found, no need to insert it.\n";
		}

		hookCRV = M.getFunction("__cudaRegisterVar");
                if(hookCRV==NULL)
                {
                        errs() << "cudaRegisterVar not found, need to insert it!\n";
                        hookFuncCRV = M.getOrInsertFunction("__cudaRegisterVar", Type::getInt32Ty(C),// this line is NOT actually used
                                        PointerType::get(Type::getInt8PtrTy(C), 0), // namely, i8** pointer  
					Type::getInt8PtrTy(C) , Type::getInt8PtrTy(C), Type::getInt8PtrTy(C), 
					Type::getInt32Ty(C), Type::getInt32Ty(C), Type::getInt32Ty(C), Type::getInt32Ty(C),  NULL);
                        hookCRV = cast<Function>(hookFuncCRV);

                } else
                {
		//	errs() << *( PointerType::get(Type::getInt8PtrTy(C), 0 ) ) ;
                        errs() << "cudaRegisterVar found, no need to insert it.\n";
                }

            	hookFunc1 = M.getOrInsertFunction("print1", VoidTy, (Type*)0);
            	hookFunc2 = M.getOrInsertFunction("print2", VoidTy, (Type*)0);
		hookFunc3 = M.getOrInsertFunction("print3", VoidTy, Type::getInt32Ty(C),  NULL);
		hookFunc4 = M.getOrInsertFunction("print4", VoidTy, Type::getInt64Ty(C),  NULL);
		hookFuncEA = M.getOrInsertFunction("printea", VoidTy, Type::getInt8PtrTy(C), Type::getInt32Ty(C), NULL);
		hookFuncRetMain = M.getOrInsertFunction("RetMain", VoidTy, (Type*)0 );
		hookFuncString = M.getOrInsertFunction("takeString", VoidTy, Type::getInt8PtrTy(C), Type::getInt32Ty(C), NULL);
		hookFuncDumpTrace = M.getOrInsertFunction("dumpTrace", VoidTy, Type::getInt8PtrTy(C), NULL);
		hookFuncAppendTrace = M.getOrInsertFunction("appendTrace", VoidTy, Type::getInt8PtrTy(C), Type::getInt8PtrTy(C), NULL);
		hookFuncAppendBBlog = M.getOrInsertFunction("appendBBlog", VoidTy, Type::getInt8PtrTy(C), Type::getInt8PtrTy(C), NULL);
		hookFuncMyMalloc = M.getOrInsertFunction("mymalloc", VoidTy, Type::getInt32Ty(C), NULL);
		hookFuncGetHandle = M.getOrInsertFunction("getHandle", Type::getInt8PtrTy(C), Type::getInt32Ty(C), NULL);

//		Type::getFloatTy(M.getContext())  for float
		hook1= cast<Function>(hookFunc1); hook2= cast<Function>(hookFunc2); hook3= cast<Function>(hookFunc3); 
		hook4= cast<Function>(hookFunc4); hookEA= cast<Function>(hookFuncEA);
                hookRetMain = cast<Function>(hookFuncRetMain);
                hookString = cast<Function>(hookFuncString);
                hookDumpTrace = cast<Function>(hookFuncDumpTrace);
                hookAppend = cast<Function>(hookFuncAppendTrace);
                hookAppendBB = cast<Function>(hookFuncAppendBBlog);
                hookMyMalloc = cast<Function>(hookFuncMyMalloc);
                hookGetHandle = cast<Function>(hookFuncGetHandle);

            	for(Module::iterator F = M.begin(), E = M.end(); F!= E; ++F)
               	{
		// 	errs() << F->getName().str() << "\n";
			for(Function::iterator BB = F->begin(), E = F->end(); BB != E; ++BB)
			{
			//	errs() << F->getName().str() << "\n";
			//	errs() << BB->getName().str() << "\n";
				instru_globalvar::runOnBasicBlock(BB); //TODO: 4 overhead
			}
            	}
		return false;
        }


        virtual bool runOnBasicBlock(Function::iterator &BB)
        {
        	LLVMContext &C = BB->getContext();
		Value* ZERO = ConstantInt::get(Type::getInt32Ty(C), 0);
		Value* ONE = ConstantInt::get(Type::getInt32Ty(C), 1);
		Value* TWO = ConstantInt::get(Type::getInt32Ty(C), 2);
		Value* THREE = ConstantInt::get(Type::getInt32Ty(C), 3);

		Function* bb_func = BB->getParent();
		std::string func_name = bb_func->getName().str();	
		std::string bb_name = BB->getName().str();
		//errs() << func_name << " : " << bb_name << "\n" ;
		int cnt = 0;
		AllocaInst* hxyz;
		AllocaInst* hgv2;
		Value* str_gv2;
		Value* str_bufferOnDevice;
		Value* str_BBbufferOnDevice;
	
		bool alreadyCopied = false;

	    	for(BasicBlock::iterator BI = BB->begin(), BE = BB->end(); BI != BE; ++BI)
            	{
			cnt++;
			
		    	if (cnt==1 ) 
			{
				if( (bb_name.find("entry") != std::string::npos) &&  (func_name.find("main") != std::string::npos)  )
				{      // insert AllocaInst into entry block of main function.
					errs() << "I am about to insert [hookMyMalloc] to main() .entry block \n";
				//	hxyz = new AllocaInst(Type::getInt32Ty(C), ONE, 16/*alignment*/, "xxxyyyzzz", &(*BI)); 
					hgv2 = new AllocaInst( ArrayType::get(Type::getInt32Ty(C), 20) , NULL /*initial value*/, 16/*alignment*/, "h_gv2", &(*BI)); 
				//	bufferOnHost = new AllocaInst( ArrayType::get(Type::getInt32Ty(C), BUFFERSIZE_INT) , NULL, 16/*alignment*/, "buffer_oN_HoST", &(*BI)); 
					IRBuilder<> builder(&(*BI));
                                        builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() ); // insert after op
					CallInst* mi = builder.CreateCall(hookMyMalloc, builder.getInt32(101) );	//this 101 is overwritten	
					CallInst* ci1 = builder.CreateCall(hookGetHandle, ONE);
					CallInst* ci2 = builder.CreateCall(hookGetHandle, TWO);
                                	
					const DebugLoc &loc = BI->getDebugLoc();
					if( loc )
					{
						mi-> setDebugLoc(  loc );

					}

                                        builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() ); // insert after op
				        builder.CreateCall(hookEA, {ci1,ONE});
				        builder.CreateCall(hookEA, {ci2,ONE});
				}

				if( bb_name.find("kcall.end") != std::string::npos  &&  true )
				{
					errs() << "I am about to insert [hookCMFS] and [hookAppend] to kcall.end << \n=============\n\n";
						
					IRBuilder<> builder(&(*BI));
//					builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() ); // insert after op
					
				//        Value* h_pool = builder.CreatePointerCast(hgv2, Type::getInt8PtrTy(C) );
				//        Value* d_pool = builder.CreatePointerCast(gv_2, Type::getInt8PtrTy(C) );
					Value* ptr2BufferOnDevice = builder.CreatePointerCast(bufferOnDevice, Type::getInt8PtrTy(C) );

				//        builder.CreateCall(hookEA, {h_pool,THREE});
				//        builder.CreateCall(hookEA, {d_pool,THREE});
					str_gv2 = builder.CreateGlobalStringPtr("gv2"); //create a global string for gv2;
					str_bufferOnDevice = builder.CreateGlobalStringPtr("buffer_oN_DeViCe"); //create a global string for bufferOnDevice;
					
			       //         Value* args[] = { h_pool, d_pool,
				//		ConstantInt::get(Type::getInt64Ty(C), 20*4), /*number of bytes*/
				 //               ConstantInt::get(Type::getInt64Ty(C), 0), /*offset*/
				  //              TWO /*direction: device to host*/};
				
			       //     	CallInst *MI = builder.CreateCall(hookCMFS, args);

					CallInst* p2buffer = builder.CreateCall(hookGetHandle, ONE);
					CallInst* p2trace = builder.CreateCall(hookGetHandle, TWO);
					const DebugLoc &loc = BI->getDebugLoc();
//		
					builder.CreateCall( hookCMFS, { p2buffer, ptr2BufferOnDevice,
									ConstantInt::get(Type::getInt64Ty(C), BUFFERSIZE_INT*4), /*number of bytes*/
									ConstantInt::get(Type::getInt64Ty(C), 0), /*offset*/
									TWO /*direction: device to host*/} 	);
					builder.CreateCall( hookAppend, { p2trace, p2buffer } );
			////
			////

                       //                 builder.CreateCall( hookCMFS, { p2buffer, ptr2bbBufferOnDevice,
                       //                                                 ConstantInt::get(Type::getInt64Ty(C), BUFFERSIZE_INT*4), /*number of bytes*/
                       //                                                ConstantInt::get(Type::getInt64Ty(C), 0), /*offset*/
                       //                                                 TWO /*direction: device to host*/}      );
                       //                 builder.CreateCall( hookAppendBB, { p2trace, p2buffer } );

					continue;
					Value* h_t = builder.CreatePointerCast(hxyz, Type::getInt8PtrTy(C) );
					Value* d_t = builder.CreatePointerCast(gv_1, Type::getInt8PtrTy(C) );

					builder.CreateCall(hookEA, {h_t,THREE});
					builder.CreateCall(hookEA, {d_t,THREE});
		
					Value* args1[] = {h_t, d_t,
						ConstantInt::get(Type::getInt32Ty(C), 4),
						ConstantInt::get(Type::getInt32Ty(C), 0),
						TWO}; 
					errs() << "end << \n=============\n\n";
	//                              CallInst *MI = builder.CreateCall(hookCMFS, args);
				}	
			}

				
			if ( auto *op = dyn_cast<CallInst>( &(*BI) ) )
			{
				Function* f = op->getCalledFunction();
				if (!f) //this check is important for: %call =  call deferenceble bla bla...
					continue;	
				StringRef callee = op->getCalledFunction() -> getName();
                                std::string calleeName = callee.str();
				if( (calleeName.find("cudaMalloc") != std::string::npos)  && (calleeName.find("_") == std::string::npos)  )
				{
				} 
				else  if( (calleeName.find("cudaMemcpyFrom") != std::string::npos)  && (calleeName.find("_") == std::string::npos)  )
				{
					errs() << " [[[\nI am an CallInst !  :    "  << *BI <<  "\n";
			//		op->getArgOperand(0) -> dump(); errs() << "\n";
			//		op->getArgOperand(1) -> dump(); errs() << "\n";
			//		op->getArgOperand(2) -> dump(); errs() << "\n";
			//		op->getArgOperand(3) -> dump(); errs() << "\n";

					IRBuilder<> builder(op);

			//		builder.CreateCall(hookDumpTrace, {op->getArgOperand(0) } );
					builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() ); // insert after op
			//		builder.CreateCall(hookDumpTrace, {op->getArgOperand(0) } );
					StringRef caller = op->getFunction() -> getName();
					std::string callerName = caller.str();

					errs() << op->getFunction() -> getName().str() <<  "\n";
					errs() << op->getCalledFunction() -> getName().str() <<  "\n";

					errs() << " ]]]\n\n" ;
				}
				else  if( (calleeName.find("cudaRegisterFunction") != std::string::npos)  && true)
				{
					if (alreadyCopied)
                                                continue;

					IRBuilder<> builder(op);
					builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() ); // insert after op
                                        //op ->getArgOperand(0) -> dump(); errs() << "\n";

					errs() << " [[[\nI am a CudaRegisterFunction !  :  \n  "  << *BI <<  "\n";
					alreadyCopied=true;
					Value* ptr2bufferOnDevice = builder.CreatePointerCast(bufferOnDevice, Type::getInt8PtrTy(C) );
					str_bufferOnDevice = builder.CreateGlobalStringPtr("buffer_oN_DeViCe"); //create a global string for bufferOnDevice;

				//	errs() << " 1 \n";
					Value* args[] ={	op ->getArgOperand(0),
							ptr2bufferOnDevice, str_bufferOnDevice, str_bufferOnDevice,
							ZERO, builder.getInt32(BUFFERSIZE_INT*4), ZERO, ZERO};
				//	errs() << *( op ->getArgOperand(0) )<<  "\n" ;
				//	errs() << *( op ->getArgOperand(0) -> getType() )<<  "\n" ;
				//	ptr2bufferOnDevice ->dump(); errs()<<  "\n";
				//	str_bufferOnDevice ->dump(); errs()<<  "\n";
					builder.CreateCall(hookCRV, args);

			/*		Value* ptr2BBbufferOnDevice = builder.CreatePointerCast(bb_bufferOnDevice, Type::getInt8PtrTy(C) );
                                        str_BBbufferOnDevice = builder.CreateGlobalStringPtr("BB_buffer_oN_DeViCe"); //create a global string for bufferOnDevice;
					Value* args2[] ={        op ->getArgOperand(0),
                                                        ptr2BBbufferOnDevice, str_BBbufferOnDevice, str_BBbufferOnDevice,
                                                        ZERO, builder.getInt32(BUFFERSIZE_INT*4), ZERO, ZERO};
					builder.CreateCall(hookCRV, args2);
			*/
					errs() << " out of CudaRegisterFunction !  :  \n  " ;
				}
			}

			if ( auto *op = dyn_cast<ReturnInst>( &(*BI) ) )
                        {
				StringRef fn = op->getFunction() -> getName();
				std::string fname = fn.str();
				if (fname!="main")
					continue;

				IRBuilder<> builder(op);
				CallInst* ptr2trace = builder.CreateCall( hookGetHandle, TWO );		

			//		errs() << "!!!!!!!!! main returns\n";
			//		Instruction* newInst = CallInst::Create(hookDumpTrace,{ptr2trace } );
                        //`        	newInst->insertBefore( &(*BI));		
				continue;
                        }
			continue;
            	}
            	return true;
        }
    };
///
///
///
/// pass:  GPU pass
// third pass
// 3rd pass
// memory accesses only
    struct instru_kernel_memory : public ModulePass{
        static char ID;   
        Function *hook1;
        Function *hook2;
        Function *hook3;
        Function *hook4;
        Function *hook5;
	Function *hookMain;
	Function *hookString;
	Function *hookRet;
	bool flag1;
	Value* str_fname;
	Value* ptr_fname;
	Type* VoidTy; 

        instru_kernel_memory() : ModulePass(ID) {}

        virtual bool runOnModule(Module &M)
        {

		LLVMContext &C = M.getContext();
		VoidTy = Type::getVoidTy(C); 

                errs() << "\n ======== 3rd pass: GPU kernel =============== \n\n";
                errs() << "\nDeclare new Global Variables:\n\n";

//// auto-detect CUDA kernels
		NamedMDNode* MDannotatios = M.getNamedMetadata("nvvm.annotations");
            	for(unsigned i=0; i<MDannotatios->getNumOperands(); i++)
		{
			MDNode* nn = MDannotatios->getOperand(i);
			////////////////errs()<<" I have operands " <<  nn->getNumOperands() << "\n" ;
			for(unsigned j=0; j<nn->getNumOperands(); j++)
			{
				if(! nn->getOperand(j))
					continue;
				//MDOperand  op = ( nn->getOperand(j));		
				Metadata*  op = ( nn->getOperand(j));
				//errs() << *op  << "\n";
				if( auto* str = dyn_cast<MDString>(nn->getOperand(j) ) )
				{
					std::string ss = str->getString().str();
					if (ss=="kernel")
					{
						errs() << "I found a CUDA kernel:\t";
						if( auto* v = dyn_cast<ValueAsMetadata>(nn->getOperand(j-1) ) )
							if (auto* F = dyn_cast<Function> (v->getValue())  )
							{
								errs() << F->getName() << "\n";
								StringRef fk = F->getName();
								std::string fkname = fk.str();
								CUDAKN[KNcnt].assign(fkname);
								KNcnt++;
							}
					}
				}
//		Metadata md =  *( nn->getOperand(j) );
 			}
		//	nn-> dump(); errs() << "\n";
		}

////	
                errs() << "\nLinking Analysis Functions:\n\n";
		for(Module::iterator F = M.begin(), E = M.end(); F!= E; ++F)
		{	// it is all about mangling
			StringRef fn = F->getName();
            		std::string fname = fn.str();
			errs() << fname << "\n";
			
			int ii;
			for (ii=0; ii<AFsize; ii++)
			if ( fname.find(AF[ii]) != std::string::npos ) 
			{	
				errs() << AF[ii] << "  is changed to "  ;
				AF[ii].assign(fname);
				errs() << AF[ii] <<  "\n";
			}
		}

            	Constant *hookFunc1; Constant *hookFunc2;
		Constant *hookFunc3; Constant *hookFunc4; Constant *hookFunc5;
		Constant *hookFuncMain;
		Constant *hookFuncString;
		Constant *hookFuncRet;

//            	hookFunc1 = M.getOrInsertFunction(AF[0], VoidTy, (Type*)0);
		hookFunc1 = M.getOrInsertFunction(AF[0], VoidTy, Type::getInt32Ty(C),  NULL);
            	hookFunc2 = M.getOrInsertFunction(AF[1], VoidTy, (Type*)0);
		hookFunc3 = M.getOrInsertFunction(AF[2], VoidTy, Type::getInt32Ty(C), Type::getInt32Ty(C),  NULL);
//		hookFunc4 = M.getOrInsertFunction("print4", VoidTy, Type::getInt64Ty(C),  NULL);
		hookFunc5 = M.getOrInsertFunction(AF[3], VoidTy, Type::getInt8PtrTy(C), Type::getInt32Ty(C),  Type::getInt32Ty(C),  Type::getInt32Ty(C), Type::getInt32Ty(C), NULL);
//		hookFuncMain = M.getOrInsertFunction("fini", VoidTy, (Type*)0 );
		hookFuncString = M.getOrInsertFunction(AF[4], VoidTy, Type::getInt8PtrTy(C), Type::getInt32Ty(C), NULL);
            	hookFuncRet = M.getOrInsertFunction(AF[5], VoidTy, (Type*)0);
//		Type::getFloatTy(M.getContext())  for float
		hook1= cast<Function>(hookFunc1); hook2= cast<Function>(hookFunc2); hook3= cast<Function>(hookFunc3); 
//		hook4= cast<Function>(hookFunc4); 
		hook5= cast<Function>(hookFunc5);
//		hookMain = cast<Function>(hookFuncMain);
                hookString = cast<Function>(hookFuncString);
                hookRet = cast<Function>(hookFuncRet);

                errs() << "\nInstrument:\n\n";
            	for(Module::iterator F = M.begin(), E = M.end(); F!= E; ++F)
		{
			bool native = true;
			std::string fname = F->getName().str();
			errs() << fname << "\n";
			int ii;
                        for (ii=0; ii<AFsize; ii++)
                        	if ( fname.find(AF[ii]) != std::string::npos )
                        	{	native = false; 
                       		//	errs() << "found !" << fname << " \n";
				}

			if (!native)
			{
				errs() << "skipped. "<< "\n\n";
                                continue;
			}

			flag1 = false;			
			for(Function::iterator BB = F->begin(), E = F->end(); BB != E; ++BB)
 				instru_kernel_memory::runOnBasicBlock32(BB);
		//	errs() <<  "next\n";
            	}
		return false;
        }
// 

        virtual bool runOnBasicBlock32(Function::iterator &BB)
        {
        	LLVMContext &C = BB->getContext();
		Value *ONE = ConstantInt::get(Type::getInt32Ty(C), 1);
		Value *TWO = ConstantInt::get(Type::getInt32Ty(C), 2);
		Value *THREE = ConstantInt::get(Type::getInt32Ty(C), 3);
	    	for(BasicBlock::iterator BI = BB->begin(), BE = BB->end(); BI != BE; ++BI)
            	{
			if ( auto *op = dyn_cast<LoadInst>( &(*BI) ) )
                        {
                                errs()  << " [[[\nI am a load!  :    "  << *BI <<  "\n";

				IRBuilder<> builder(op); //insert before op
				
                                const DebugLoc &loc = BI->getDebugLoc();
				if (!loc)
					printf(" I don't have a !dgb tag\n");

                                builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() ); // insert after op	
                                if (loc)
                                {
                                        int l =  loc.getLine();
                                        int col =  loc.getCol();
                                        Value* L = builder.getInt32(l); 
					Value* sColm = builder.getInt32(col);
                                        Value* args[] = {L, builder.getInt32(col) };
                                        CallInst* tmp = builder.CreateCall(hook3,args);
					CallInst* newInst = builder.CreateCall(hook1, ONE);
					tmp->setDebugLoc( loc);
					newInst->setDebugLoc( loc);

                        	        LoadInst* CI = dyn_cast<LoadInst>(BI);
                                	Value* addr = CI->getPointerOperand();//TODO
					Value* voidptr = builder.CreatePointerCast(addr, Type::getInt8PtrTy(C) );

					Type* tp = CI->getType();
                               		// errs() << *tp << " : " << tp->getPrimitiveSizeInBits() <<  " : " << tp->getScalarSizeInBits() ;
					int sizebits = (int) tp->getPrimitiveSizeInBits();

					CallInst* MI = builder.CreateCall(hook5, {voidptr, builder.getInt32(sizebits), L, sColm,  ONE });
					MI->setDebugLoc(loc);
				}

                                errs() << " ]]]\n\n" ;
				continue;
                        }
			if ( auto *op = dyn_cast<StoreInst>( &(*BI) ) )
                        {
                                errs()  << " [[[\nI am a load!  :    "  << *BI <<  "\n";

		//		Instruction *newInst = CallInst::Create(hook1,"");
				IRBuilder<> builder(op); //insert before op
				
                                const DebugLoc &loc = BI->getDebugLoc();
				if (!loc)
					printf(" I don't have a !dgb tag\n");

			//	newInst->insertAfter( &(*BI));
		
                                builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() ); // insert after op	
                                if (loc)
                                {
                                        int l =  loc.getLine();
                                        int col =  loc.getCol();
                                        Value* L = ConstantInt::get(Type::getInt32Ty(C), l);
                                        Value* scolm = builder.getInt32(col);
                                        Value* args[] = {L, scolm };
                                        CallInst* tmp = builder.CreateCall(hook3,args);
					CallInst* newInst = builder.CreateCall(hook1, TWO);
					tmp->setDebugLoc( loc);
					newInst->setDebugLoc( loc);
                                
                                	StoreInst* CI = dyn_cast<StoreInst>(BI);
                                	Value* addr = CI->getPointerOperand();//TODO
					Value* voidptr = builder.CreatePointerCast(addr, Type::getInt8PtrTy(C) );

					Type* tp = CI->getValueOperand() -> getType();
                               // errs() << *tp << " : " << tp->getPrimitiveSizeInBits() <<  " : " << tp->getScalarSizeInBits() ;
					int sizebits = (int) tp->getPrimitiveSizeInBits();

  				//CallInst* MI = builder.CreateCall(hook5, {voidptr, L });
  					CallInst* MI = builder.CreateCall(hook5, {voidptr, builder.getInt32(sizebits), L, scolm, TWO });
					MI->setDebugLoc(loc);
				}

                                errs() << " ]]]\n\n" ;
				continue;
                        }
			
			continue;
            	}
            	return true;
        }
    };



/// pass:  GPU pass
// fourth pass
// 4th pass
    struct instru_kernel_branch : public ModulePass{
        static char ID;   
        Function *hook1;
        Function *hook2;
        Function *hook3;
        Function *hook4;
        Function *hook5;
	Function *hookMain;
	Function *hookBB;
	Function *hookRet;
	Function *hookInit;
	Function *hookFCall;
	Function* hookString;
	bool flag1;
	Value* str_fname;
	Value* ptr_fname;
	Type* VoidTy; 

        instru_kernel_branch() : ModulePass(ID) {}

        virtual bool runOnModule(Module &M)
        {

		LLVMContext &C = M.getContext();
		VoidTy = Type::getVoidTy(C); 

                errs() << "\n ======== 4rd pass: GPU kernel branch =============== \n\n";
                errs() << "\nDeclare new Global Variables:\n\n";

		NamedMDNode* MDannotatios = M.getNamedMetadata("nvvm.annotations");
		//MDannotatios -> dump(); errs() << "\n";

            	for(unsigned i=0; i<MDannotatios->getNumOperands(); i++)
		{//auto detect GPU kernels
			MDNode* nn = MDannotatios->getOperand(i);
			////////////////errs()<<" I have operands " <<  nn->getNumOperands() << "\n" ;
			for(unsigned j=0; j<nn->getNumOperands(); j++)
			{
				if(! nn->getOperand(j))
					continue;
				//MDOperand  op = ( nn->getOperand(j));		
				Metadata*  op = ( nn->getOperand(j));
				//errs() << *op  << "\n";
				if( auto* str = dyn_cast<MDString>(nn->getOperand(j) ) )
				{
					std::string ss = str->getString().str();
					if (ss=="kernel")
					{
						errs() << "I found a CUDA kernel:\t";
						if( auto* v = dyn_cast<ValueAsMetadata>(nn->getOperand(j-1) ) )
							if (auto* F = dyn_cast<Function> (v->getValue())  )
							{
								errs() << F->getName() << "\n";
								StringRef fk = F->getName();
								std::string fkname = fk.str();
								CUDAKN[KNcnt].assign(fkname);
								KNcnt++;
							}
							
					}
				}
//		Metadata md =  *( nn->getOperand(j) );
 			}
		//	nn-> dump(); errs() << "\n";
		}

		GlobalVariable* bufferOnDevice = M.getGlobalVariable("buffer_oN_DeViCe",false); //TODO//DEBUG
                if(bufferOnDevice!=NULL)
		{
			bufferOnDevice ->setExternallyInitialized(true);
			bufferOnDevice ->setLinkage(GlobalValue::ExternalLinkage) ;
			ConstantAggregateZero* const_array = ConstantAggregateZero::get(  ArrayType::get(Type::getInt32Ty(C), BUFFERSIZE_INT) );
			bufferOnDevice -> setInitializer( const_array);
		//	bufferOnDevice -> dump();
		}
////	
                errs() << "\nLinking Analysis Functions:\n\n";
		for(Module::iterator F = M.begin(), E = M.end(); F!= E; ++F)
		{	// it is all about mangling
			StringRef fn = F->getName();
            		std::string fname = fn.str();
			errs() << fname << "\n";
			
			int ii;
			for (ii=0; ii<AFsize; ii++)
			if ( fname.find(AF[ii]) != std::string::npos ) 
			{	
				errs() << AF[ii] << "  is changed to "  ;
				AF[ii].assign(fname);
				errs() << AF[ii] <<  "\n";
			}
		}

            	Constant *hookFunc1; Constant *hookFunc2;
		Constant *hookFunc3; Constant *hookFunc4; Constant *hookFunc5;
		Constant *hookFuncMain;
		Constant *hookFuncBB;
		Constant* hookFuncFCall;
		Constant* hookFuncRet;
		Constant* hookFuncInit;
		Constant* hookFuncString;

//            	hookFunc1 = M.getOrInsertFunction(AF[0], VoidTy, (Type*)0);
		hookFunc1 = M.getOrInsertFunction(AF[0], VoidTy, Type::getInt32Ty(C),  NULL);
            	hookFunc2 = M.getOrInsertFunction(AF[1], VoidTy, (Type*)0);
		hookFunc3 = M.getOrInsertFunction(AF[2], VoidTy, Type::getInt32Ty(C), Type::getInt32Ty(C),  NULL);
//		hookFunc4 = M.getOrInsertFunction("print4", VoidTy, Type::getInt64Ty(C),  NULL);
		hookFunc5 = M.getOrInsertFunction(AF[3], VoidTy, Type::getInt8PtrTy(C), Type::getInt32Ty(C),  Type::getInt32Ty(C),  Type::getInt32Ty(C), Type::getInt32Ty(C), NULL);
		hookFuncString = M.getOrInsertFunction(AF[4], VoidTy, Type::getInt8PtrTy(C), Type::getInt32Ty(C), NULL);
//		hookFuncMain = M.getOrInsertFunction("fini", VoidTy, (Type*)0 );
		hookFuncBB = M.getOrInsertFunction("passBasicBlock", VoidTy, Type::getInt8PtrTy(C), Type::getInt32Ty(C),Type::getInt32Ty(C),Type::getInt32Ty(C), NULL);
		hookFuncFCall = M.getOrInsertFunction("callFunc", VoidTy, Type::getInt8PtrTy(C), Type::getInt8PtrTy(C), Type::getInt32Ty(C),Type::getInt32Ty(C), NULL);
            	hookFuncRet = M.getOrInsertFunction(AF[5], VoidTy, (Type*)0);
            	hookFuncInit = M.getOrInsertFunction("InitKernel", VoidTy, (Type*)0);
//		Type::getFloatTy(M.getContext())  for float
		hook1= cast<Function>(hookFunc1); hook2= cast<Function>(hookFunc2); hook3= cast<Function>(hookFunc3); 
//		hook4= cast<Function>(hookFunc4); 
		hook5= cast<Function>(hookFunc5);
//		hookMain = cast<Function>(hookFuncMain);
                hookBB = cast<Function>(hookFuncBB);
                hookFCall = cast<Function>(hookFuncFCall);
                hookRet = cast<Function>(hookFuncRet);
                hookInit = cast<Function>(hookFuncInit);
		hookString = cast<Function> (hookFuncString);

                errs() << "\nInstrument:\n\n";
            	for(Module::iterator F = M.begin(), E = M.end(); F!= E; ++F)
		{
			bool native = true;
			std::string fname = F->getName().str();
			errs() << fname << "\n";
			int ii;
                        for (ii=0; ii<AFsize; ii++)
                        	if ( fname.find(AF[ii]) != std::string::npos )
                        	{	native = false; 
                       		//	errs() << "found !" << fname << " \n";
				}

			if (!native)
			{
				errs() << "skipped. "<< "\n\n";
                                continue;
			}

			flag1 = false;			
//                	for(Function::iterator BB = F->begin(), E = F->end(); BB != E; ++BB)
 //               		instru_kernel_branch::runOnBasicBlock41(BB);
			for(Function::iterator BB = F->begin(), E = F->end(); BB != E; ++BB)
                                instru_kernel_branch::runOnBasicBlock42(BB);
		//	errs() <<  "next\n";
            	}
		return false;
        }
// 
        virtual bool runOnBasicBlock41(Function::iterator &BB)
        { //instrument at kernel return;
        	LLVMContext &C = BB->getContext();
		Value *ONE = ConstantInt::get(Type::getInt32Ty(C), 1);
		Value *TWO = ConstantInt::get(Type::getInt32Ty(C), 2);
		Value *THREE = ConstantInt::get(Type::getInt32Ty(C), 3);

		Function* bb_func = BB->getParent();
		StringRef f_name = bb_func->getName();	

		IRBuilder<> bbbuilder(&(*BB));
		if(!flag1)
		{
			str_fname = bbbuilder.CreateGlobalStringPtr(f_name);
			ptr_fname = bbbuilder.CreatePointerCast(str_fname, Type::getInt8PtrTy(C) );
			flag1 = true;
		}

	    	for(BasicBlock::iterator BI = BB->begin(), BE = BB->end(); BI != BE; ++BI)
            	{
			if ( auto *op = dyn_cast<ReturnInst>( &(*BI) ) )
                        {
                                errs() << " [[[\nI am ReturnInst !  :    "  << *BI <<  "\n";
			//	errs() << op->getFunction() -> getName().str();

                                const DebugLoc &loc = BI->getDebugLoc();

				StringRef fn = op->getFunction() -> getName();
				std::string fname = fn.str();
				//const char* ffname;
				//ffname = fname.c_str();

				IRBuilder<> builder(op); //IMPORTANT: you can't insert a call after a call, you will get dead loop
	
				bool yeskernel=false;
				for (int ii=0; ii<KNcnt; ii++)
				{
				//	errs() << CUDAKN[ii] << " and  " << fname << "\n";
					if ( fname.find(CUDAKN[ii])!= std::string::npos)
						yeskernel=true;
				}	
                 		//if ( fname.find("axpy_kernel")!= std::string::npos )
				if (yeskernel)
				{
					CallInst* tmpI = builder.CreateCall(hookRet, {});
					if(loc)
                                        	tmpI->setDebugLoc(loc);
				}
				
                                errs() << " ]]]\n\n" ;
				continue;
                        }
//			continue;//Debug
			if ( auto *op = dyn_cast<CallInst>( &(*BI) ) )
			{	
			//	errs() << op->getCalledFunction() -> getName().str();

                                StringRef callee = op->getCalledFunction() -> getName();
                                std::string calleeName = callee.str();
			
				if ( calleeName.find("llvm.dbg.")!= std::string::npos || calleeName.find("ptx.sreg")!= std::string::npos
					|| calleeName.find("nvvm.barrier")!= std::string::npos || calleeName.find("llvm.nvvm.")!= std::string::npos 
					|| calleeName.find("llvm.fabs")!= std::string::npos || calleeName.find("llvm.fma")!= std::string::npos	) 
					continue; //skip functions containing these keywords

				errs() << " [[[\nI am an CallInst !  :    "  << *BI <<  "\n";
				StringRef caller = op->getFunction() -> getName();
                                std::string callerName = caller.str();

                                IRBuilder<> builder(op);
                                const DebugLoc &loc = BI->getDebugLoc();

//                                builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() ); // insert after op	
				Value *args[] = {ptr_fname, ONE};
                                CallInst* MI = builder.CreateCall(hookString, args);

                                Value *Str, *strPtr;
				Str = builder.CreateGlobalStringPtr(callee); // pass callee name
                                strPtr = builder.CreatePointerCast(Str, Type::getInt8PtrTy(C) );
                                Value *args2[] = {strPtr, TWO};
                                CallInst* tmpI = builder.CreateCall(hookString, args2);
				
				if(loc)
				{
					int ln = loc.getLine();
                                        int cl = loc.getCol();
					//MI -> setDebugLoc(loc);
					// tmpI -> setDebugLoc(loc);
                                	CallInst* ci1 = builder.CreateCall( hookFCall, {ptr_fname, strPtr, builder.getInt32(ln), builder.getInt32(cl)} );
                                	ci1 -> setDebugLoc(loc);
				}
				else
					printf(" I don't have a !dgb tag\n");
			
				errs() << " ]]]\n\n" ;
				continue;
			}

            	}
            	return true;
	}

        virtual bool runOnBasicBlock42(Function::iterator &BB)
        {
        	LLVMContext &C = BB->getContext();
		Value *ONE = ConstantInt::get(Type::getInt32Ty(C), 1);
		Value *TWO = ConstantInt::get(Type::getInt32Ty(C), 2);
		Value *THREE = ConstantInt::get(Type::getInt32Ty(C), 3);

		Function* bb_func = BB->getParent();
		StringRef f_name = bb_func->getName();	

		Instruction* inst = BB->getFirstNonPHI();
		const DebugLoc &loc = inst->getDebugLoc();
		std::string bb_name = BB->getName().str();
		IRBuilder<> builder(inst);

		if(!flag1)
		{
			str_fname = builder.CreateGlobalStringPtr(f_name);
			ptr_fname = builder.CreatePointerCast(str_fname, Type::getInt8PtrTy(C) );
			flag1 = true;
		}

//              errs() << "\n ==== ===== next pair ==== ==== \n";

                if ( bb_name.length() == 0)
		{
			errs() << "length = 0!!!\n";
//			return true;
		}

		Value* gv_bbname = builder.CreateGlobalStringPtr(bb_name);
		Value* str_bbname = builder.CreatePointerCast(gv_bbname, Type::getInt8PtrTy(C) );

		if (loc)
		{
//	                errs() << f_name << "\n";
//			errs() << bb_name << "\n";
			int ln = loc.getLine();
			int cl = loc.getCol();
			CallInst* ci = builder.CreateCall(hookBB, {str_bbname, ONE, builder.getInt32(ln), builder.getInt32(cl) } );
			ci->setDebugLoc(loc);
		} 
		else 	
		{
                	for(BasicBlock::iterator BI = BB->begin(), BE = BB->end(); BI != BE; ++BI)
			{
				const DebugLoc &loc2 = BI->getDebugLoc();
				if (loc2)
				{
					int ln = loc2.getLine();
                        		int cl = loc2.getCol();
                        		CallInst *ci = builder.CreateCall(hookBB, {str_bbname, ONE, builder.getInt32(ln), builder.getInt32(cl) } );
                        		ci->setDebugLoc(loc2);
					break;
				}
			}
		}

		return true;
        }
    };



/// pass:  GPU pass
// fifth pass
// 5th pass
// this is path is for constructing call path, allocating GPU buffer,  kernel initialization and return.
    struct instru_kernel_basic : public ModulePass{
        static char ID;   
        Function *hook1;
        Function *hook2;
        Function *hook3;
        Function *hook4;
        Function *hook5;
	Function *hookMain;
	Function *hookBB;
	Function *hookRet;
	Function *hookInit;
	Function *hookFCall;
	Function* hookString;
	bool flag1;
	Value* str_fname;
	Value* ptr_fname;
	Type* VoidTy; 

        instru_kernel_basic() : ModulePass(ID) {}

        virtual bool runOnModule(Module &M)
        {

		LLVMContext &C = M.getContext();
		VoidTy = Type::getVoidTy(C); 

                errs() << "\n ======== 5th pass: GPU kernel branch =============== \n\n";
                errs() << "\nDeclare new Global Variables:\n\n";

		NamedMDNode* MDannotatios = M.getNamedMetadata("nvvm.annotations");
		//MDannotatios -> dump(); errs() << "\n";

            	for(unsigned i=0; i<MDannotatios->getNumOperands(); i++)
		{//auto detect GPU kernels
			MDNode* nn = MDannotatios->getOperand(i);
			////////////////errs()<<" I have operands " <<  nn->getNumOperands() << "\n" ;
			for(unsigned j=0; j<nn->getNumOperands(); j++)
			{
				if(! nn->getOperand(j))
					continue;
				//MDOperand  op = ( nn->getOperand(j));		
				Metadata*  op = ( nn->getOperand(j));
				//errs() << *op  << "\n";
				if( auto* str = dyn_cast<MDString>(nn->getOperand(j) ) )
				{
					std::string ss = str->getString().str();
					if (ss=="kernel")
					{
						errs() << "I found a CUDA kernel:\t";
						if( auto* v = dyn_cast<ValueAsMetadata>(nn->getOperand(j-1) ) )
							if (auto* F = dyn_cast<Function> (v->getValue())  )
							{
								errs() << F->getName() << "\n";
								StringRef fk = F->getName();
								std::string fkname = fk.str();
								CUDAKN[KNcnt].assign(fkname);
								KNcnt++;
							}
							
					}
				}
//		Metadata md =  *( nn->getOperand(j) );
 			}
		//	nn-> dump(); errs() << "\n";
		}

		GlobalVariable* bufferOnDevice = M.getGlobalVariable("buffer_oN_DeViCe",false); //TODO//DEBUG
                if(bufferOnDevice!=NULL)
		{
			bufferOnDevice ->setExternallyInitialized(true);
			bufferOnDevice ->setLinkage(GlobalValue::ExternalLinkage) ;
			ConstantAggregateZero* const_array = ConstantAggregateZero::get(  ArrayType::get(Type::getInt32Ty(C), BUFFERSIZE_INT) );
			bufferOnDevice -> setInitializer( const_array);
		//	bufferOnDevice -> dump();
		}
////	
                errs() << "\nLinking Analysis Functions:\n\n";
		for(Module::iterator F = M.begin(), E = M.end(); F!= E; ++F)
		{	// it is all about mangling
			StringRef fn = F->getName();
            		std::string fname = fn.str();
			errs() << fname << "\n";
			
			int ii;
			for (ii=0; ii<AFsize; ii++)
			if ( fname.find(AF[ii]) != std::string::npos ) 
			{	
				errs() << AF[ii] << "  is changed to "  ;
				AF[ii].assign(fname);
				errs() << AF[ii] <<  "\n";
			}
		}

            	Constant *hookFunc1; Constant *hookFunc2;
		Constant *hookFunc3; Constant *hookFunc4; Constant *hookFunc5;
		Constant *hookFuncMain;
		Constant *hookFuncBB;
		Constant* hookFuncFCall;
		Constant* hookFuncRet;
		Constant* hookFuncInit;
		Constant* hookFuncString;

//            	hookFunc1 = M.getOrInsertFunction(AF[0], VoidTy, (Type*)0);
		hookFunc1 = M.getOrInsertFunction(AF[0], VoidTy, Type::getInt32Ty(C),  NULL);
            	hookFunc2 = M.getOrInsertFunction(AF[1], VoidTy, (Type*)0);
		hookFunc3 = M.getOrInsertFunction(AF[2], VoidTy, Type::getInt32Ty(C), Type::getInt32Ty(C),  NULL);
//		hookFunc4 = M.getOrInsertFunction("print4", VoidTy, Type::getInt64Ty(C),  NULL);
		hookFunc5 = M.getOrInsertFunction(AF[3], VoidTy, Type::getInt8PtrTy(C), Type::getInt32Ty(C),  Type::getInt32Ty(C),  Type::getInt32Ty(C), Type::getInt32Ty(C), NULL);
		hookFuncString = M.getOrInsertFunction(AF[4], VoidTy, Type::getInt8PtrTy(C), Type::getInt32Ty(C), NULL);
//		hookFuncMain = M.getOrInsertFunction("fini", VoidTy, (Type*)0 );
		hookFuncBB = M.getOrInsertFunction("passBasicBlock", VoidTy, Type::getInt8PtrTy(C), Type::getInt32Ty(C),Type::getInt32Ty(C),Type::getInt32Ty(C), NULL);
		hookFuncFCall = M.getOrInsertFunction("callFunc", VoidTy, Type::getInt8PtrTy(C), Type::getInt8PtrTy(C), Type::getInt32Ty(C),Type::getInt32Ty(C), NULL);
            	hookFuncRet = M.getOrInsertFunction(AF[5], VoidTy, (Type*)0);
            	hookFuncInit = M.getOrInsertFunction("InitKernel", VoidTy, (Type*)0);
//		Type::getFloatTy(M.getContext())  for float
		hook1= cast<Function>(hookFunc1); hook2= cast<Function>(hookFunc2); hook3= cast<Function>(hookFunc3); 
//		hook4= cast<Function>(hookFunc4); 
		hook5= cast<Function>(hookFunc5);
//		hookMain = cast<Function>(hookFuncMain);
                hookBB = cast<Function>(hookFuncBB);
                hookFCall = cast<Function>(hookFuncFCall);
                hookRet = cast<Function>(hookFuncRet);
                hookInit = cast<Function>(hookFuncInit);
		hookString = cast<Function> (hookFuncString);

                errs() << "\nInstrument:\n\n";
            	for(Module::iterator F = M.begin(), E = M.end(); F!= E; ++F)
		{
			bool native = true;
			std::string fname = F->getName().str();
			errs() << fname << "\n";
			int ii;
                        for (ii=0; ii<AFsize; ii++)
                        	if ( fname.find(AF[ii]) != std::string::npos )
                        	{	native = false; 
                       		//	errs() << "found !" << fname << " \n";
				}

			if (!native)
			{
				errs() << "skipped. "<< "\n\n";
                                continue;
			}

			flag1 = false;			
                	for(Function::iterator BB = F->begin(), E = F->end(); BB != E; ++BB)
                		instru_kernel_basic::runOnBasicBlock41(BB);
			for(Function::iterator BB = F->begin(), E = F->end(); BB != E; ++BB)
                                instru_kernel_basic::runOnBasicBlock42(BB);
		//	errs() <<  "next\n";
            	}
		return false;
        }
// 
        virtual bool runOnBasicBlock41(Function::iterator &BB)
        { //instrument at kernel return;
        	LLVMContext &C = BB->getContext();
		Value *ONE = ConstantInt::get(Type::getInt32Ty(C), 1);
		Value *TWO = ConstantInt::get(Type::getInt32Ty(C), 2);
		Value *THREE = ConstantInt::get(Type::getInt32Ty(C), 3);

		Function* bb_func = BB->getParent();
		StringRef f_name = bb_func->getName();	

		IRBuilder<> bbbuilder(&(*BB));
		if(!flag1)
		{
			str_fname = bbbuilder.CreateGlobalStringPtr(f_name);
			ptr_fname = bbbuilder.CreatePointerCast(str_fname, Type::getInt8PtrTy(C) );
			flag1 = true;
		}

	    	for(BasicBlock::iterator BI = BB->begin(), BE = BB->end(); BI != BE; ++BI)
            	{
			if ( auto *op = dyn_cast<ReturnInst>( &(*BI) ) )
                        {
                                errs() << " [[[\nI am ReturnInst !  :    "  << *BI <<  "\n";
			//	errs() << op->getFunction() -> getName().str();

                                const DebugLoc &loc = BI->getDebugLoc();

				StringRef fn = op->getFunction() -> getName();
				std::string fname = fn.str();
				//const char* ffname;
				//ffname = fname.c_str();

				IRBuilder<> builder(op); //IMPORTANT: you can't insert a call after a call, you will get dead loop
	
				bool yeskernel=false;
				for (int ii=0; ii<KNcnt; ii++)
				{
				//	errs() << CUDAKN[ii] << " and  " << fname << "\n";
					if ( fname.find(CUDAKN[ii])!= std::string::npos)
						yeskernel=true;
				}	
                 		//if ( fname.find("axpy_kernel")!= std::string::npos )
				if (yeskernel)
				{
					CallInst* tmpI = builder.CreateCall(hookRet, {});
					if(loc)
                                        	tmpI->setDebugLoc(loc);
				}
				
                                errs() << " ]]]\n\n" ;
				continue;
                        }
//			continue;//Debug
			if ( auto *op = dyn_cast<CallInst>( &(*BI) ) )
			{	
			//	errs() << op->getCalledFunction() -> getName().str();
				Function* f = op->getCalledFunction();
                                if (!f) //this check is important for: %call =  call deferenceble bla bla...
                                        continue;

                                StringRef callee = f -> getName();
                                std::string calleeName = callee.str();
			
				if ( calleeName.find("llvm.dbg.")!= std::string::npos || calleeName.find("ptx.sreg")!= std::string::npos
					|| calleeName.find("nvvm.barrier")!= std::string::npos || calleeName.find("llvm.nvvm.")!= std::string::npos 
					|| calleeName.find("llvm.fabs")!= std::string::npos || calleeName.find("llvm.fma")!= std::string::npos	) 
					continue; //skip functions containing these keywords

				errs() << " [[[\nI am an CallInst !  :    "  << *BI <<  "\n";
				StringRef caller = op->getFunction() -> getName();
                                std::string callerName = caller.str();

                                IRBuilder<> builder(op);
                                const DebugLoc &loc = BI->getDebugLoc();

//                                builder.SetInsertPoint( &(*BB), ++builder.GetInsertPoint() ); // insert after op	
				Value *args[] = {ptr_fname, ONE};
                                CallInst* MI = builder.CreateCall(hookString, args);

                                Value *Str, *strPtr;
				Str = builder.CreateGlobalStringPtr(callee); // pass callee name
                                strPtr = builder.CreatePointerCast(Str, Type::getInt8PtrTy(C) );
                                Value *args2[] = {strPtr, TWO};
                                CallInst* tmpI = builder.CreateCall(hookString, args2);
				
				if(loc)
				{
					int ln = loc.getLine();
                                        int cl = loc.getCol();
					//MI -> setDebugLoc(loc);
					// tmpI -> setDebugLoc(loc);
                                	CallInst* ci1 = builder.CreateCall( hookFCall, {ptr_fname, strPtr, builder.getInt32(ln), builder.getInt32(cl)} );
                                	ci1 -> setDebugLoc(loc);
				}
				else
				errs() << " ]]]\n\n" ;
				continue;
			}

            	}
            	return true;
	}

        virtual bool runOnBasicBlock42(Function::iterator &BB)
        {
        	LLVMContext &C = BB->getContext();
		Value *ONE = ConstantInt::get(Type::getInt32Ty(C), 1);
		Value *TWO = ConstantInt::get(Type::getInt32Ty(C), 2);
		Value *THREE = ConstantInt::get(Type::getInt32Ty(C), 3);

		Function* bb_func = BB->getParent();
		StringRef f_name = bb_func->getName();	

		Instruction* inst = BB->getFirstNonPHI();
		const DebugLoc &loc = inst->getDebugLoc();
		std::string bb_name = BB->getName().str();
		IRBuilder<> builder(inst);

		if(!flag1)
		{
			str_fname = builder.CreateGlobalStringPtr(f_name);
			ptr_fname = builder.CreatePointerCast(str_fname, Type::getInt8PtrTy(C) );
			flag1 = true;
		}

		//insert intialization into Kernel's entry block		
	       	bool yeskernel=false;
		for (int ii=0; ii<KNcnt; ii++)
		{
			if ( f_name.str().find(CUDAKN[ii])!= std::string::npos)
				yeskernel=true;
		}
		if (yeskernel && bb_name.find("entry")!= std::string::npos ) //insert intialization into Kernel's entry block
		{
			CallInst* tmpI = builder.CreateCall(hookInit, {});
			if(loc)
				tmpI->setDebugLoc(loc);
			else
				for(BasicBlock::iterator BI = BB->begin(), BE = BB->end(); BI != BE; ++BI)
                        {
                                const DebugLoc &loc2 = BI->getDebugLoc();
                                if (loc2)
				{
					tmpI->setDebugLoc(loc2);
                                        break;
                                }
			}
		} // end of insertion

		return true;
        }
    };
}


char instru_host::ID = 0;
static RegisterPass<instru_host> X("instru-host", "load/store and call path instrumentation", false, false);

char instru_kernel_memory::ID = 0;
static RegisterPass<instru_kernel_memory> XX("instru-kernel-memory", "CUDA kernel memory instrumentation", false, false);

char instru_globalvar::ID = 0;
static RegisterPass<instru_globalvar> XXX("instru-global-var", "global variables instrumentation on host", false, false);

char instru_kernel_branch::ID = 0;
static RegisterPass<instru_kernel_branch> XXXX("instru-kernel-branch", "CUDA kernel branch instrumentation", false, false);

char instru_kernel_basic::ID = 0;
static RegisterPass<instru_kernel_basic> XXXXX("instru-kernel-basic", "CUDA kernel general instrumentation", false, false);

//char instru_globalvar2::ID = 0;
//static RegisterPass<instru_globalvar2> XXXX("instru-global-var2", "global variables instrumentation on host", false, false);

