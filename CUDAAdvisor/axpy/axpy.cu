#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <cuda.h>
#include <cuda_runtime.h>

__device__ int test1[20];
__device__ int test2[20];
//extern __device__ int testxyz[1000];
//int localtrace[10000];
//__device__ float* tracehandle;

__device__ int* aaa;

__device__ double AAAABBBB(float a)
{
	return a*1.9;
}

__device__ int foo_AA(float a)
{
	if (threadIdx.x < 4)
		return (int) a;
	else 
		return 1024;
}

__device__ int foo( float a, float b)
{

	return 1;
	if (threadIdx.x < 8)
		return foo_AA(a);
	else 
		return (int)AAAABBBB(b);
}

__global__ void axpy_kernel2(float a, float* x, float* y, float* newbu)
{
	//tracehandle = newbu;
	int index = blockIdx.x*blockDim.x + threadIdx.x;

//	if (threadIdx.x==0)
//		aaa = (int*)malloc(1024*1024*1024*5);


	float aa = y[index] + x[index];
	float b = 0.3*y[index] + 0.7* x[index];
	y[index] *= (int)(x[index])%4 +  foo(a, x[index]);
}

__global__ void axpy_kernel(float a, float* x, float* y, float* newbu) 
{
	//tracehandle = newbu;

	int index = blockIdx.x*blockDim.x + threadIdx.x;
	y[index] = x[index]*0.3;

	if (index>2)
		y[index] += 99;
	else
		y[index] += 999;

}

int main(int argc, char* argv[]) {
  	const int kDataLen = 12;
  	float a = 2.0f;
	int blocks = 10;
	
//	tracetest = (int*)malloc( 1234);

//  	float host_x[blocks*kDataLen];
	float host_y[blocks*kDataLen];
	float* host_x = (float*) malloc( blocks*kDataLen*sizeof(float));
	float host_newbu[10000];
	int ii;
	for( ii=0; ii<kDataLen*blocks; ii++)
		host_x[ii] = ii%8;
 	for( ii=0; ii<kDataLen*blocks; ii++)
                host_y[ii] = ii%5;
/*	int x[5];
	x[0] = 13;
	printf("%p\n",x);
	printf("%p\n",&x);
	printf("%d\n",*x);
	printf("%d\n",*(x+1));
*/

  // Copy input data to device.
  float* device_x;
  float* device_y;
  float* newbu;
	cudaSetDevice(6);

  cudaMalloc((void**)&device_x, blocks* kDataLen * sizeof(float));
  cudaMalloc((void**)&device_y, blocks* kDataLen * sizeof(float) + 18);
////  cudaMalloc(&newbu, 1024*1024*4);
/*	std::cout << &(device_x) << "\n";
        std::cout << &(device_y) << "\n";
	std::cout << &(*device_x) << "\n";
        std::cout << &(*device_y) << "\n";
	std::cout << (device_x) << "\n";
        std::cout << (device_y) << "\n";
*/
	cudaMemcpy(device_x, host_x, blocks* kDataLen * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(device_y, host_y, blocks* kDataLen * sizeof(float), cudaMemcpyHostToDevice);

	std::cout << "launching kernel...\n";
  	axpy_kernel<<<blocks, dim3(kDataLen,2)>>>(a, device_x, device_y, newbu);
	cudaDeviceSynchronize();
  	axpy_kernel2<<<blocks, kDataLen>>>(a, device_x, device_y, newbu);
		
	cudaDeviceSynchronize();
	cudaMemcpy(host_y, device_y, blocks* kDataLen * sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(host_newbu, newbu, 1000, cudaMemcpyDeviceToHost);


  	for (int i = 0; i < 8; ++i) {
    		std::cout << "y[" << i << "] = " << host_y[i] << "\n";
//    std::cout << & (host_y[i] )<< "\n";
  	}

//	for (int i = 0; i < 20; ++i) {
//                std::cout << "newtrace [" << i << "] = " << host_newbu[i] << "\n";
//    std::cout << & (host_y[i] )<< "\n";
//        }

/*        cudaMemcpyFromSymbol(localtrace, testxyz, 40*sizeof(int), 0, cudaMemcpyDeviceToHost);
        for (int i = 0; i < 20; ++i)
                printf("%d\t", localtrace[i] );
        std::cout << std::endl;

	cudaMemcpyFromSymbol(localtrace+8, testxyz, 40*sizeof(int), 0, cudaMemcpyDeviceToHost);
	for (int i = 0; i < 20; ++i)
		printf("%d\t", localtrace[i] );
	std::cout << std::endl;
*/
//	int* show_h;
//	cudaMemcpyFromSymbol(show_h, show, sizeof(int), 0, cudaMemcpyDeviceToHost);
//	msg = cudaGetSymbolAddress((void **)&d_G, test);
//		printf("the address is %p\n", d_G);
//	if (msg == cudaSuccess)
	{
	//	int tmp[4];
	//	printf("before %d %d %d %d@ %p\n", *tmp,  *(tmp+1), *(tmp+2), *(tmp+3), tmp);
	//	cudaMemcpyFromSymbol(tracetest, test1, 4*sizeof(int), 0, cudaMemcpyDeviceToHost);
	//	cudaMemcpyFromSymbol(tmp, test2, 4*sizeof(int), 0, cudaMemcpyDeviceToHost);
	//	printf("copy %d %d %d %d@ %p\n",  *tmp, *(tmp+1), *(tmp+2), *(tmp+3), tmp);
	//	cudaMemcpyFromSymbol(tmp, test2, 4*sizeof(int), 0, cudaMemcpyDeviceToHost);
	//	printf("after %d %d %d %d@ %p\n",  tmp[0], tmp[1], tmp[2], tmp[3], tmp);
	}
	//else
	//	std::cout << cudaGetErrorString(msg)  <<  "\n\n";


	cudaFree(device_x);
	cudaFree(device_y);
  	cudaDeviceReset();
  	return 0;
}
